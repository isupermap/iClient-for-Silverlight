﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace SuperMap.Web.Controls
{
    /// <summary>
    /// 	<para>${controls_FeatureDataGrid_Title}</para>
    /// 	<para>${controls_FeatureDataGrid_Description}</para>
    /// </summary>
    [TemplatePart(Name = "opbtn", Type = typeof(Button))]
    [TemplatePart(Name = "clearmbtn", Type = typeof(Button))]
    [TemplatePart(Name = "switchmbtn", Type = typeof(Button))]
    [TemplatePart(Name = "lastbtn", Type = typeof(Button))]
    [TemplatePart(Name = "numrecordtb", Type = typeof(TextBlock))]
    [TemplatePart(Name = "zoomtombtn", Type = typeof(Button))]
    [TemplatePart(Name = "delrowmbtn", Type = typeof(Button))]
    [TemplatePart(Name = "menu", Type = typeof(Popup))]
    [TemplatePart(Name = "selectallmbtn", Type = typeof(Button))]
    [TemplatePart(Name = "changedmapviewbounds", Type = typeof(CheckBox))]
    [TemplatePart(Name = "firstbtn", Type = typeof(Button))]
    [TemplatePart(Name = "previousbtn", Type = typeof(Button))]
    [TemplatePart(Name = "recordtb", Type = typeof(TextBox))]
    [TemplatePart(Name = "nextbtn", Type = typeof(Button))]
    public class FeatureDataGrid : DataGrid
    {
        private const double ex_bounds_ra = 0.05;

        private Button firstbtn = null;
        private Button previousbtn = null;
        private TextBox recordtb = null;
        private Button nextbtn = null;
        private Button lastbtn = null;
        private TextBlock numrecordtb = null;
        private Popup menu = null;
        private Button opbtn = null;
        private Button clearmbtn = null;
        private Button switchmbtn = null;
        private Button selectallmbtn = null;
        private Button zoomtombtn = null;
        private Button delrowmbtn = null;
        private CheckBox changedmapviewbounds = null;

        private int selectedRecordsNum = 0;//选中了几个
        private int totalRecordsNum = 0;//总共几个
        private int currentRecordNum = 0; //当前的index
        private bool isFD = false;
        private bool isFL = false;
        private FeaturesSelectionList sf = null;
        private string tNum = "";
        private Type obt;

        /// <summary>${controls_FeatureDataGrid_constructor_None_D}</summary>
        public FeatureDataGrid()
        {
            DefaultStyleKey = typeof(FeatureDataGrid);
        }

        /// <summary>${controls_FeatureDataGrid_attribute_Map_D}</summary>
        public Map Map
        {
            get { return (Map)GetValue(MapProperty); }
            set { SetValue(MapProperty, value); }
        }
        /// <summary>${controls_FeatureDataGrid_field_MapProperty_D}</summary>
        public static readonly DependencyProperty MapProperty =
            DependencyProperty.Register("Map", typeof(Map), typeof(FeatureDataGrid), null);

        /// <summary>${controls_FeatureDataGrid_attribute_featuresLayer_D}</summary>
        public FeaturesLayer FeaturesLayer
        {
            get { return (FeaturesLayer)GetValue(FeaturesLayerProperty); }
            set { SetValue(FeaturesLayerProperty, value); }
        }
        /// <summary>${controls_FeatureDataGrid_field_featuresLayerProperty_D}</summary>
        public static readonly DependencyProperty FeaturesLayerProperty =
            DependencyProperty.Register("FeaturesLayer", typeof(FeaturesLayer), typeof(FeatureDataGrid), new PropertyMetadata(null, OnFeaturesLayerPropertyChanged));
        private static void OnFeaturesLayerPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataGrid g = d as FeatureDataGrid;
            FeaturesLayer ov = e.OldValue as FeaturesLayer;
            FeaturesLayer nv = e.NewValue as FeaturesLayer;
            if (ov != null)
            {
                ov.Features.CollectionChanged -= g.Features_CollectionChanged;
                foreach (Feature f in ov.Features)
                {
                    f.PropertyChanged -= g.Feature_PropertyChanged;
                }

                g.ItemsSource = null;
            }
            if (nv != null)
            {
                nv.Features.CollectionChanged += g.Features_CollectionChanged;
                foreach (Feature f in nv.Features)
                {
                    f.PropertyChanged += g.Feature_PropertyChanged;
                }

                IEnumerable<IDictionary<string, object>> attr = GetFEStatic(nv.Features);
                g.SetItemsSource(attr);
            }
        }

        private static IEnumerable<IDictionary<string, object>> GetFEStatic(FeatureCollection features)
        {
            return (from a in features select a.Attributes).AsEnumerable<IDictionary<string, object>>();
        }

        /// <summary>${controls_FeatureDataGrid_method_onApplyTemplate_D}</summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            opbtn = GetTemplateChild("opbtn") as Button;
            menu = GetTemplateChild("menu") as Popup;
            clearmbtn = GetTemplateChild("clearmbtn") as Button;
            switchmbtn = GetTemplateChild("switchmbtn") as Button;
            selectallmbtn = GetTemplateChild("selectallmbtn") as Button;
            zoomtombtn = GetTemplateChild("zoomtombtn") as Button;
            delrowmbtn = GetTemplateChild("delrowmbtn") as Button;
            changedmapviewbounds = GetTemplateChild("changedmapviewbounds") as CheckBox;
            firstbtn = GetTemplateChild("firstbtn") as Button;
            previousbtn = GetTemplateChild("previousbtn") as Button;
            recordtb = GetTemplateChild("recordtb") as TextBox;
            nextbtn = GetTemplateChild("nextbtn") as Button;
            lastbtn = GetTemplateChild("lastbtn") as Button;
            numrecordtb = GetTemplateChild("numrecordtb") as TextBlock;

            Initdatagrid();
        }

        private void Initdatagrid()
        {
            SelectionChanged += (s, e) =>
            {
                if (ItemsSource == null || ItemsSource.AsList().Count == 0)
                {
                    return;
                }

                if (FeaturesLayer != null && FeaturesLayer.Features != null)
                {
                    isFD = true;
                    SelectFeatures(e.AddedItems, true);
                    SelectFeatures(e.RemovedItems, false);
                    isFD = false;

                    selectedRecordsNum = SelectedFeatures.Count;

                    if (changedmapviewbounds != null && changedmapviewbounds.IsChecked.Value && !isFL)
                    {
                        if (selectedRecordsNum < 10)
                        {
                            RoamToSel(false);
                        }
                        else
                        {
                            RoamToSel(true);
                        }

                    }

                    ShowNumberOfRecords();
                    CurRecNumtb();
                }
            };
            RowEditEnding += (s1, e1) =>
            {
                if (e1.EditAction == DataGridEditAction.Commit)
                {
                    int idx = GetRowIndexInRows(e1.Row.DataContext);
                    Feature relatedFeature = FeaturesLayer.Features[idx];

                    if (relatedFeature != null)
                    {
                        try
                        {
                            e1.Row.DoFeatureRefresh(relatedFeature, obt);
                        }
                        catch (Exception)
                        {
                            throw new ArgumentException(SuperMap.Web.Controls.Resources.Resource.FeatureDataGrid_Row_Editing_Exception);
                        }
                    }
                }
            };

            if (firstbtn != null)
            {
                firstbtn.Click += (s, e) =>
                {
                    currentRecordNum = 0;
                    SelCurRecord();
                };
            }

            if (previousbtn != null)
            {
                previousbtn.Click += (s, e) =>
                {
                    int idx = GetRowIndexInRows(CurrentItem);
                    if (idx > 0)
                    {
                        currentRecordNum = idx - 1;
                    }
                    SelCurRecord();
                };
            }

            if (recordtb != null)
            {
                recordtb.KeyDown += (sender, e) =>
                {
                    TextBox textBox = sender as TextBox;
                    if (textBox != null)
                    {
                        if (e.Key == Key.Enter)
                        {
                            int convertedNo = int.Parse(textBox.Text);
                            currentRecordNum = convertedNo - 1;
                            ValCurRecordNum();
                            CurRecNumtb();
                            SelCurRecord();
                        }
                        else if ((e.PlatformKeyCode >= 48 && e.PlatformKeyCode <= 57) || (e.PlatformKeyCode >= 96 && e.PlatformKeyCode <= 105))
                        {
                            if (textBox.SelectedText.Length > 0)
                            {
                                textBox.Text = textBox.Text.Remove(textBox.SelectionStart, textBox.SelectedText.Length);
                            }

                            textBox.Text += e.Key.ToString().Substring(e.Key.ToString().Length - 1);
                            textBox.SelectionStart = textBox.Text.Length;
                        }
                        e.Handled = true;
                    }
                };
            }

            if (nextbtn != null)
            {
                nextbtn.Click += (s, e) =>
                {
                    int idx = GetRowIndexInRows(CurrentItem);
                    if (idx < totalRecordsNum - 1)
                    {
                        if (idx == 0 && SelectedItem != CurrentItem)
                        {
                            currentRecordNum = 0;
                        }
                        else
                        {
                            currentRecordNum = idx + 1;
                        }
                    }
                    SelCurRecord();
                };
            }

            if (lastbtn != null)
            {
                lastbtn.Click += (s, e) =>
                {
                    currentRecordNum = totalRecordsNum - 1;
                    SelCurRecord();
                };
            }

            if (numrecordtb != null)
            {
                if (numrecordtb.Text.IndexOf("{0}") > -1 || numrecordtb.Text.IndexOf("{1}") > -1)
                {
                    tNum = numrecordtb.Text;
                }
                else
                {
                    tNum = SuperMap.Web.Controls.Resources.Resource.FeatureDataGrid_RecordMsg;
                }

                numOfRecordstb(0, 0);
            }

            if (opbtn != null)
            {
                opbtn.Click += (s, e) =>
                {
                    if (menu != null && menu.Child is FrameworkElement)
                    {
                        double opacity = menu.Opacity;
                        menu.Opacity = 0;
                        menu.IsOpen = true;
                        Dispatcher.BeginInvoke((Action)delegate()
                        {
                            menu.VerticalOffset = -(menu.Child as FrameworkElement).ActualHeight + opbtn.ActualHeight;
                            menu.Opacity = opacity;
                        });
                    }
                };
            }

            if (menu != null && menu.Child != null)
            {
                menu.Child.MouseLeave += (s, e) => { menu.IsOpen = false; };
            }

            if (clearmbtn != null)
            {
                clearmbtn.Click += (s, e) =>
                {
                    ClearSel();
                    if (menu != null)
                    {
                        menu.IsOpen = false;
                    }
                };
            }

            if (switchmbtn != null)
            {
                switchmbtn.Click += (s, e) =>
                {
                    SwitchSel();
                    if (menu != null)
                    {
                        menu.IsOpen = false;
                    }
                };
            }

            if (selectallmbtn != null)
            {
                selectallmbtn.Click += (s, e) =>
                {
                    SelAll();
                    if (menu != null)
                    {
                        menu.IsOpen = false;
                    }
                };
            }

            if (zoomtombtn != null)
            {
                zoomtombtn.Click += (s, e) =>
                {
                    RoamToSel(true);
                    if (menu != null)
                    {
                        menu.IsOpen = false;
                    }
                };
            }

            if (delrowmbtn != null)
            {
                delrowmbtn.Click += (s, e) =>
                {
                    DelSelectedRows();
                    if (menu != null)
                    {
                        menu.IsOpen = false;
                    }
                };
            }
        }

        private void ValCurRecordNum()
        {
            if (currentRecordNum >= totalRecordsNum)
            {
                currentRecordNum = totalRecordsNum - 1;
            }
            else if (currentRecordNum < 0)
            {
                currentRecordNum = 0;
            }
        }

        private void SelCurRecord()
        {
            if (totalRecordsNum == 0)
            {
                return;
            }
            ValCurRecordNum();
            SelectedItem = ItemsSource.AsList()[currentRecordNum];
            ScrollGridRowIntoView(SelectedItem);
        }

        private void ClearSel()
        {
            if (ItemsSource == null || selectedRecordsNum == 0)
            {
                return;
            }

            if (SelectedItems != null)
            {
                SelectedItems.Clear();
            }
        }

        private void SwitchSel()
        {
            if (ItemsSource != null)
            {
                if (totalRecordsNum == selectedRecordsNum)
                {
                    ClearSel();
                }
                else if (selectedRecordsNum == 0)
                {
                    SelAll();
                }
                else
                {
                    foreach (object gridRow in ItemsSource)
                    {
                        bool isFound = false;
                        foreach (object selectedRow in SelectedItems)
                        {
                            if (selectedRow.Equals(gridRow))
                            {
                                isFound = true;
                                break;
                            }
                        }
                        if (isFound)
                        {
                            SelectedItems.Remove(gridRow);
                        }
                        else
                        {
                            SelectedItems.Add(gridRow);
                        }
                    }
                }
            }
        }

        private void SelAll()
        {
            if (ItemsSource == null || totalRecordsNum == selectedRecordsNum)
            {
                return;
            }

            foreach (object gridRow in ItemsSource)
            {
                SelectedItems.Add(gridRow);
            }


        }

        private void RoamToSel(bool isZ)
        {
            if (selectedRecordsNum == 0)
            {
                return;
            }

            if (Map != null && SelectedFeatures != null)
            {
                Rectangle2D newMapviewbounds = Rectangle2D.Empty;
                foreach (Feature f in SelectedFeatures)
                {
                    if (f.Geometry != null && f.Geometry.Bounds != Rectangle2D.Empty)
                        newMapviewbounds.Union(f.Geometry.Bounds);
                }
                if (newMapviewbounds != Rectangle2D.Empty)
                {
                    if (newMapviewbounds.Width > 0 || newMapviewbounds.Height > 0)
                    {
                        newMapviewbounds = new Rectangle2D(
                            newMapviewbounds.Left - newMapviewbounds.Width * ex_bounds_ra,
                            newMapviewbounds.Bottom - newMapviewbounds.Height * ex_bounds_ra,
                            newMapviewbounds.Right + newMapviewbounds.Width * ex_bounds_ra,
                            newMapviewbounds.Top + newMapviewbounds.Height * ex_bounds_ra);

                        Map.ZoomTo(newMapviewbounds);
                    }
                    else
                    {
                        Map.PanTo(newMapviewbounds.Center);
                    }
                }
            }
        }

        private void DelSelectedRows()
        {
            List<object> selectedItems = new List<object>();
            selectedItems.AddRange(SelectedItems.OfType<object>());
            while (selectedItems.Count > 0)
            {
                int idxToRemove = GetRowIndexInRows(selectedItems[0]);
                if (FeaturesLayer != null)
                {
                    FeaturesLayer.Features.RemoveAt(idxToRemove);
                }
                else
                {
                    ItemsSource.DataSourceRemove(idxToRemove, obt);
                }
                selectedItems.RemoveAt(0);
            }
            ShowNumberOfRecords();
            SelCurRecord();
            CurRecNumtb();
        }

        private void CurRecNumtb()
        {
            if (recordtb != null)
            {
                recordtb.Text = (currentRecordNum + 1).ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        private void numOfRecordstb(int _countSelected, int _countTotal)
        {
            if (numrecordtb != null)
            {
                numrecordtb.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, tNum, _countSelected, _countTotal);
            }
        }

        private IEnumerable<IDictionary<string, object>> GetFEnumerable(FeatureCollection features)
        {
            return (from a in features select a.Attributes).AsEnumerable<IDictionary<string, object>>();
        }

        private void SetRecordsnum()
        {
            if (ItemsSource == null || ItemsSource.AsList().Count == 0)
            {
                totalRecordsNum = 0;
            }
            else
            {
                totalRecordsNum = ItemsSource.AsList().Count;
            }
        }

        private void ShowNumberOfRecords()
        {
            SetRecordsnum();

            if (ItemsSource == null || ItemsSource.AsList().Count == 0)
            {
                currentRecordNum = -1;
                CurRecNumtb();
                numOfRecordstb(0, 0);
            }
            else
            {
                numOfRecordstb(selectedRecordsNum, totalRecordsNum);
            }
        }

        private void SetItemsSource(IEnumerable<IDictionary<string, object>> source)
        {
            ItemsSource = source.ToDs(out obt);
            ShowNumberOfRecords();
        }

        private object GetCorrespondingGridRow(Feature feature)
        {
            if (feature != null)
            {
                int idx = GetFeatureIndexInFeatures(feature);
                if (idx > -1)
                {
                    IList gridRows = ItemsSource.AsList();
                    if (idx < gridRows.Count)
                    {
                        return gridRows[idx];
                    }
                }
            }

            return null;
        }

        private Feature GetCorrespondingFeature(object dataGridRow)
        {
            if (dataGridRow != null)
            {
                int idx = GetRowIndexInRows(dataGridRow);
                if (idx > -1 && idx < FeaturesLayer.Features.Count)
                {
                    return FeaturesLayer.Features[idx];
                }
            }

            return null;
        }

        private void Feature_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (isFD)
            {
                return;
            }

            if (e.PropertyName == "Selected")
            {
                Feature feature = sender as Feature;
                if (feature != null)
                {
                    object correspondingGridRow = GetCorrespondingGridRow(feature);
                    if (correspondingGridRow != null)
                    {
                        isFL = true;
                        if (feature.Selected)
                        {
                            SelectedItems.Add(correspondingGridRow);
                        }
                        else
                        {
                            SelectedItems.Remove(correspondingGridRow);
                        }
                        isFL = false;
                    }
                }
            }
        }

        private void Features_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            IEnumerable<IDictionary<string, object>> attr = GetFEnumerable(sender as FeatureCollection);
            if (ItemsSource == null || e.Action == NotifyCollectionChangedAction.Reset)
            {
                SetItemsSource(attr);
            }
            else if (ItemsSource.AsList().Count == 0 && e.Action == NotifyCollectionChangedAction.Add &&
                     e.NewItems != null && e.NewItems.Count > 0)
            {
                SetItemsSource(attr);
                IEnumerator enumAddedFeatures = e.NewItems.GetEnumerator();
                while (enumAddedFeatures.MoveNext())
                {
                    Feature feature = enumAddedFeatures.Current as Feature;
                    if (feature != null)
                    {
                        feature.PropertyChanged += Feature_PropertyChanged;
                        SetRecordsnum();
                    }
                }
            }
            else
            {
                if (e.Action != NotifyCollectionChangedAction.Reset)
                {
                    if (e.Action == NotifyCollectionChangedAction.Add &&
                        e.NewItems != null && e.NewItems.Count > 0)
                    {
                        IEnumerator enumAddedFeatures = e.NewItems.GetEnumerator();
                        int idxLastNewFeature = -1;

                        while (enumAddedFeatures.MoveNext())
                        {
                            Feature feature = enumAddedFeatures.Current as Feature;
                            if (feature != null)
                            {
                                bool isNewFeature = (feature.Attributes.Count == 0);
                                ItemsSource.AddToDataSource(feature, obt);
                                feature.PropertyChanged += Feature_PropertyChanged;
                                SetRecordsnum();
                                if (isNewFeature)
                                {
                                    idxLastNewFeature = totalRecordsNum - 1;
                                }
                            }
                        }
                        if (idxLastNewFeature > -1)
                        {
                            ScrollGridRowIntoView(ItemsSource.AsList()[idxLastNewFeature]);
                        }
                    }
                    if (e.Action == NotifyCollectionChangedAction.Remove &&
                        e.OldItems != null && e.OldItems.Count > 0)
                    {
                        int selCount = SelectedItems.Count;
                        List<object> selItems = new List<object>(selCount);
                        for (int i = 0; i < selCount; i++)
                        {
                            selItems.Add(SelectedItems[i]);
                        }
                        IEnumerator enumRemovedFeatures = e.OldItems.GetEnumerator();
                        while (enumRemovedFeatures.MoveNext())
                        {
                            Feature feature = enumRemovedFeatures.Current as Feature;
                            if (feature != null)
                            {
                                object gridRow = ItemsSource.AsList()[e.OldStartingIndex];
                                if (gridRow != null)
                                {
                                    selItems.Remove(gridRow);
                                }
                                ItemsSource.DataSourceRemove(e.OldStartingIndex, obt);
                                feature.PropertyChanged -= Feature_PropertyChanged;

                                SelectedFeatures.Remove(feature);
                            }
                        }
                        if (selItems.Count > 0)
                        {
                            SelectedItems.Clear();
                            for (int i = 0; i < selItems.Count; i++)
                            {
                                SelectedItems.Add(selItems[i]);
                            }
                        }
                    }
                }
                else
                {
                    SetItemsSource(attr);
                }
            }

            ShowNumberOfRecords();
        }

        private void ScrollGridRowIntoView(object item)
        {
            ScrollIntoView(item, null);
        }

        private int GetFeatureIndexInFeatures(Feature feature)
        {
            if (FeaturesLayer != null)
            {
                return FeaturesLayer.Features.IndexOf(feature);
            }
            else
            {
                return -1;
            }
        }

        private void SelectFeatures(IList ro, bool sh)
        {
            foreach (object objRow in ro)
            {
                if (GetRowIndexInRows(objRow) > -1)
                {
                    FeaturesLayer.Features[GetRowIndexInRows(objRow)].Selected = sh;

                    if (sh)
                    {
                        currentRecordNum = GetRowIndexInRows(objRow);
                    }
                    else
                    {
                        if (SelectedFeatures.Count == 0)
                        {
                            currentRecordNum = -1;
                        }
                    }
                }
            }
        }

        private int GetRowIndexInRows(object g)
        {
            if (g == null)
            {
                return -1;
            }

            int idx = -1;
            foreach (object obj in ItemsSource)
            {
                idx++;
                if (obj.Equals(g))
                {
                    return idx;
                }
            }

            return -1;
        }

        /// <summary>${controls_FeatureDataGrid_attribute_selectedfeatures_D}</summary>
        public IList<Feature> SelectedFeatures
        {
            get
            {
                if (sf == null)
                {
                    sf = new FeaturesSelectionList(this);
                }

                return sf;
            }
        }

        /// <summary>${controls_FeatureDataGrid_method_Clear_D}</summary>
        public void Clear()
        {
            if (ItemsSource != null && ItemsSource.AsList().Count > 0)
            {
                ItemsSource.AsList().Clear();
            }
        }


        /// <summary>${controls_FeatureDataGrid_method_refreshRow_D}</summary>
        /// <param name="feature">${controls_FeatureDataGrid_method_refreshRow_param_feature}</param>
        public void RefreshRow(Feature feature)
        {
            changedmapviewbounds.IsChecked = false;
            if (GetFeatureIndexInFeatures(feature) > -1 && GetFeatureIndexInFeatures(feature) < ItemsSource.AsList().Count)
            {
                try
                {
                    int[] selIndexes = new int[SelectedItems.Count];
                    for (int i = 0; i < SelectedItems.Count; i++)
                    {
                        selIndexes[i] = GetRowIndexInRows(SelectedItems[i]);
                    }
                    feature.DoRowRefresh(ItemsSource, GetFeatureIndexInFeatures(feature), obt);
                    for (int i = 0; i < SelectedItems.Count; i++)
                    {
                        SelectedItems.Add(ItemsSource.AsList()[selIndexes[i]]);
                    }
                }
                catch (Exception)
                {
                    throw new ArgumentException(SuperMap.Web.Controls.Resources.Resource.FeatureDataGrid_No_Update_Exception);
                }
            }
        }


        /// <summary>${controls_FeatureDataGrid_method_scrollIntoView_D}</summary>
        /// <param name="feature">${controls_FeatureDataGrid_method_refreshRow_param_feature}</param>
        /// <param name="column">${controls_FeatureDataGrid_method_scrollIntoView_param_column}</param>
        public void ScrollIntoView(Feature feature, DataGridColumn column)
        {
            if (GetFeatureIndexInFeatures(feature) > -1)
            {
                if (FeaturesLayer != null && FeaturesLayer.Features.Contains(feature))
                {
                    IList gridRows = ItemsSource.AsList();
                    ScrollIntoView(gridRows[GetFeatureIndexInFeatures(feature)], column);
                }
            }
        }


        private class FeaturesSelectionList : IList<Feature>
        {
            private FeatureDataGrid FeatureDataGridowner;

            public FeaturesSelectionList(FeatureDataGrid o)
            {
                this.FeatureDataGridowner = o;
            }

            public int Add(Feature value)
            {
                if (FeatureDataGridowner.GetCorrespondingGridRow(value) != null)
                {
                    return FeatureDataGridowner.SelectedItems.Add(FeatureDataGridowner.GetCorrespondingGridRow(value));
                }

                return -1;
            }

            public void Clear()
            {
                FeatureDataGridowner.SelectedItems.Clear();
            }

            public bool Contains(Feature v)
            {
                if (FeatureDataGridowner.GetCorrespondingGridRow(v) != null)
                {
                    return FeatureDataGridowner.SelectedItems.Contains(FeatureDataGridowner.GetCorrespondingGridRow(v));
                }

                return false;
            }

            public int IndexOf(Feature v)
            {
                if (FeatureDataGridowner.GetCorrespondingGridRow(v) != null)
                {
                    return FeatureDataGridowner.SelectedItems.IndexOf(FeatureDataGridowner.GetCorrespondingGridRow(v));
                }
                return -1;
            }

            public void Insert(int index, Feature value)
            {
                if (FeatureDataGridowner.GetCorrespondingGridRow(value) != null)
                {
                    FeatureDataGridowner.SelectedItems.Insert(index, FeatureDataGridowner.GetCorrespondingGridRow(value));
                }
            }

            public bool IsFixedSize
            {
                get { return FeatureDataGridowner.SelectedItems.IsFixedSize; }
            }

            public bool IsReadOnly
            {
                get { return FeatureDataGridowner.SelectedItems.IsReadOnly; }
            }

            public void Remove(Feature v)
            {
                if (FeatureDataGridowner.GetCorrespondingGridRow(v) != null)
                {
                    FeatureDataGridowner.SelectedItems.Remove(FeatureDataGridowner.GetCorrespondingGridRow(v));
                }
            }

            public void RemoveAt(int i)
            {
                FeatureDataGridowner.SelectedItems.RemoveAt(i);
            }

            public Feature this[int index]
            {
                get
                {
                    if (FeatureDataGridowner.GetRowIndexInRows(FeatureDataGridowner.SelectedItems[index]) > -1)
                    {
                        return FeatureDataGridowner.FeaturesLayer.Features[FeatureDataGridowner.GetRowIndexInRows(FeatureDataGridowner.SelectedItems[index])];
                    }
                    else
                    {
                        if (FeatureDataGridowner.FeaturesLayer.Features.Count == 0)
                        {
                            throw new ArgumentOutOfRangeException(SuperMap.Web.Controls.Resources.Resource.FeatureDataGrid_No_Item_Exception);
                        }
                        else
                        {
                            throw new ArgumentOutOfRangeException(SuperMap.Web.Controls.Resources.Resource.FeatureDataGrid_Index_Range_Exception);
                        }
                    }
                }
                set
                {
                    if (FeatureDataGridowner.GetFeatureIndexInFeatures(value) > -1)
                    {
                        FeatureDataGridowner.SelectedItems[index] = FeatureDataGridowner.ItemsSource.AsList()[FeatureDataGridowner.GetFeatureIndexInFeatures(value)];
                    }
                }
            }


            public IEnumerator GetEnumerator()
            {
                return ((IEnumerable<Feature>)this).GetEnumerator();
            }

            IEnumerator<Feature> IEnumerable<Feature>.GetEnumerator()
            {
                foreach (object item in FeatureDataGridowner.SelectedItems)
                {
                    yield return FeatureDataGridowner.GetCorrespondingFeature(item);
                }
            }

            public void CopyTo(Array array, int index)
            {
                ((ICollection<Feature>)this).CopyTo((Feature[])array, index);
            }

            public int Count
            {
                get { return FeatureDataGridowner.SelectedItems.Count; }
            }

            public bool IsSynchronized
            {
                get { return FeatureDataGridowner.SelectedItems.IsSynchronized; }
            }

            public object SyncRoot
            {
                get { return FeatureDataGridowner.SelectedItems.SyncRoot; }
            }


            void ICollection<Feature>.Add(Feature item)
            {
                int i = this.Add(item);
            }

            public void CopyTo(Feature[] a, int aIndex)
            {
                List<object> cors = new List<object>(a.Length);
                foreach (Feature feature in a)
                {
                    object cor = FeatureDataGridowner.GetCorrespondingGridRow(feature);
                    if (cor != null)
                    {
                        cors.Add(cor);
                    }
                }
                if (cors.Count > 0)
                {
                    FeatureDataGridowner.SelectedItems.CopyTo(cors.ToArray(), aIndex);
                }
            }

            bool ICollection<Feature>.Remove(Feature item)
            {
                try
                {
                    this.Remove(item);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

    }
}