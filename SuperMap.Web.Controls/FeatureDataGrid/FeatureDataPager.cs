﻿using SuperMap.Web.Mapping;
using System.Windows.Controls.Primitives;
using System;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Data;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Collections.Specialized;
using System.Windows.Input;
using SuperMap.Web.Core;
using System.Collections;

//默认隐藏最后一行那些选项?
namespace SuperMap.Web.Controls
{  /// <summary>
    /// 	<para>${controls_FeatureDataPager_Title}</para>
    /// 	<para>${controls_FeatureDataPager_Description}</para>
    /// </summary>
    [TemplatePart(Name = "DataPagerControl", Type = typeof(DataPager))]

    [TemplatePart(Name = "CurrentRecordNumberTextBox", Type = typeof(TextBox))]
    //[TemplatePart(Name = "NumberOfRecordsTextBlock", Type = typeof(TextBlock))]

    [TemplatePart(Name = "PopupMenu", Type = typeof(Popup))]
    [TemplatePart(Name = "OptionsButton", Type = typeof(Button))]
    [TemplatePart(Name = "ClearSelectionMenuButton", Type = typeof(Button))]
    [TemplatePart(Name = "SwitchSelectionMenuButton", Type = typeof(Button))]
    [TemplatePart(Name = "SelectAllMenuButton", Type = typeof(Button))]
    [TemplatePart(Name = "ZoomToSelectionMenuButton", Type = typeof(Button))]
    [TemplatePart(Name = "DeleteSelectedRowsMenuButton", Type = typeof(Button))]
    [TemplatePart(Name = "AutoChangeMapExtentCheckBox", Type = typeof(CheckBox))]
  
    public class FeatureDataPager : DataGrid
    {
        //TODO:资源，XAML中也有
        #region Constant Variables
        private const double EXPAND_BOUNDS_RATIO = .05;
        
        #endregion

        private DataPager dataPagerControl = null;
        private PagedCollectionView PagedDataProvider = null;
        private TextBox currentRecordNumberTextBox = null;
        //private TextBlock numberOfRecordsTextBlock = null;
        private TextBlock totalRecordsTextBox = null;

        private Popup popupMenu = null;
        private Button optionsButton = null;
        private Button clearSelectionMenuButton = null;
        private Button switchSelectionMenuButton = null;
        private Button selectAllMenuButton = null;
        private Button zoomToSelectionMenuButton = null;
        private Button deleteSelectedRowsMenuButton = null;
        private CheckBox autoChangeMapViewBoundsCheckBox = null;

        private int selectedRecordsCount = 0;//选中了几个
        private int totalRecordsCount = 0;//总共几个
        private int currentRecordNumber = 0; //当前的index
        private bool isSelectionChangedFromFeatureDataPager = false;
        private bool isSelectionChangedFromFeaturesLayer = false;
        //private IList<Feature> selectedFeatures = null;
        private int firstItemIndexInCurrentView;
        private int lastItemIndexInCurrentView;
        private Type objectType;
        private IEnumerable itemsSource;
        private bool isSelectionChangedFromDataPagerControl = false;
        /// <summary>${controls_FeatureDataPager_constructor_None_D}</summary>
        public FeatureDataPager()
        {
            DefaultStyleKey = typeof(FeatureDataPager);
            //selectedFeatures = new FeaturesSelectionList1(this);
            //selectedFeatures = new List<Feature>();
        }
        /// <summary>${controls_FeatureDataPager_attribute_Map_D}</summary>
        public Map Map
        {
            get { return (Map)GetValue(MapProperty); }
            set { SetValue(MapProperty, value); }
        }
        /// <summary>${controls_FeatureDataPager_attribute_MapProperty_D}</summary>
        public static readonly DependencyProperty MapProperty =
            DependencyProperty.Register("Map", typeof(Map), typeof(FeatureDataPager), null);
        /// <summary>${controls_FeatureDataPager_attribute_FeaturesLayer_D}</summary>
        public FeaturesLayer FeaturesLayer
        {
            get { return (FeaturesLayer)GetValue(FeaturesLayerProperty); }
            set { SetValue(FeaturesLayerProperty, value); }
        }
        /// <summary>${controls_FeatureDataPager_attribute_FeaturesLayerProperty_D}</summary>
        public static readonly DependencyProperty FeaturesLayerProperty = DependencyProperty.Register("FeaturesLayer", typeof(FeaturesLayer), typeof(FeatureDataPager), new PropertyMetadata(null, OnFeaturesLayerPropertyChanged));

        private static void OnFeaturesLayerPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataPager grid = d as FeatureDataPager;
            FeaturesLayer previousFeaturesLayer = e.OldValue as FeaturesLayer;
            FeaturesLayer newFeaturesLayer = e.NewValue as FeaturesLayer;

            if (previousFeaturesLayer != null)
            {
                previousFeaturesLayer.Features.CollectionChanged -= grid.Features_CollectionChanged;
                foreach (Feature feature in previousFeaturesLayer.Features)
                {
                    feature.PropertyChanged -= grid.Feature_PropertyChanged;
                }

                grid.ItemsSource = null;
            }

            if (newFeaturesLayer != null)
            {
                newFeaturesLayer.Features.CollectionChanged += grid.Features_CollectionChanged;
                foreach (Feature feature in newFeaturesLayer.Features)
                {
                    feature.PropertyChanged += grid.Feature_PropertyChanged;
                }

                IEnumerable<IDictionary<string, object>> attr = GetFeaturesEnumerableStatic(newFeaturesLayer.Features);
                grid.SetItemsSource(attr);
            }
        }
        /// <summary>${controls_FeatureDataPager_attribute_PageSize_D}</summary>
        public int PageSize
        {
            get { return (int)GetValue(PageSizeProperty); }
            set { SetValue(PageSizeProperty, value); }
        }
        /// <summary>${controls_FeatureDataPager_attribute_PageSizeProperty_D}</summary>
        public static readonly DependencyProperty PageSizeProperty =
            DependencyProperty.Register("PageSize", typeof(int), typeof(FeatureDataPager), new PropertyMetadata(6, OnPageSizePropertyChanged));

        private static void OnPageSizePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataPager pager = d as FeatureDataPager;
            if (pager != null && pager.dataPagerControl != null)
            {
                pager.dataPagerControl.PageSize = (int)e.NewValue;
            }
        }

        private static IEnumerable<IDictionary<string, object>> GetFeaturesEnumerableStatic(FeatureCollection features)
        {
            return (from a in features select a.Attributes).AsEnumerable<IDictionary<string, object>>();
        }
        /// <summary>${controls_FeatureDataPager_attribute_OnApplyTemplate_D}</summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            dataPagerControl = GetTemplateChild("DataPagerControl") as DataPager;
            if (dataPagerControl != null)
            {
                dataPagerControl.PageSize = PageSize;
            }
            currentRecordNumberTextBox = GetTemplateChild("CurrentRecordNumberTextBox") as TextBox;
            //numberOfRecordsTextBlock = GetTemplateChild("NumberOfRecordsTextBlock") as TextBlock;

            optionsButton = GetTemplateChild("OptionsButton") as Button;
            popupMenu = GetTemplateChild("PopupMenu") as Popup;
            autoChangeMapViewBoundsCheckBox = GetTemplateChild("AutoChangeMapExtentCheckBox") as CheckBox;
            clearSelectionMenuButton = GetTemplateChild("ClearSelectionMenuButton") as Button;
            switchSelectionMenuButton = GetTemplateChild("SwitchSelectionMenuButton") as Button;
            selectAllMenuButton = GetTemplateChild("SelectAllMenuButton") as Button;
            zoomToSelectionMenuButton = GetTemplateChild("ZoomToSelectionMenuButton") as Button;
            deleteSelectedRowsMenuButton = GetTemplateChild("DeleteSelectedRowsMenuButton") as Button;
            totalRecordsTextBox = GetTemplateChild("TotalRecordsTextBlock") as TextBlock;
            InitializeFeatureDataPager();

        }

        private void InitializeFeatureDataPager()
        {
            this.SelectionChanged += FeatureDataPager_SelectionChanged;
            this.RowEditEnding += FeatureDataPager_RowEditEnding;
            this.dataPagerControl.PageIndexChanged += dataPagerControl_PageIndexChanged;

            if (currentRecordNumberTextBox != null)
            {
                currentRecordNumberTextBox.KeyDown += CurrentRecordNumberTextBox_KeyDown;
            }

            //if (numberOfRecordsTextBlock != null)
            //{
                //if (numberOfRecordsTextBlock.Text.IndexOf("{0}") > -1 || numberOfRecordsTextBlock.Text.IndexOf("{1}") > -1)
                //{
                //    textForNumberOfRecordsTextBlock = numberOfRecordsTextBlock.Text;
                //}
                //else
                //{
                //    textForNumberOfRecordsTextBlock = "TXT_SELECTED_TOTAL";
                //}

                //SetNumberOfRecordsTextBlock(0, 0);
            //}

            if (optionsButton != null)
            {
                optionsButton.Click += OptionsButton_Click;
            }

            if (popupMenu != null && popupMenu.Child != null)
            {
                popupMenu.Child.MouseLeave += PopupChild_MouseLeave;
            }

            if (clearSelectionMenuButton != null)
            {
                clearSelectionMenuButton.Click += ClearSelectionMenuButton_Click;
            }

            if (switchSelectionMenuButton != null)
            {
                switchSelectionMenuButton.Click += SwitchSelectionMenuButton_Click;
            }

            if (selectAllMenuButton != null)
            {
                selectAllMenuButton.Click += SelectAllMenuButton_Click;
            }

            if (zoomToSelectionMenuButton != null)
            {
                zoomToSelectionMenuButton.Click += ZoomToSelectionMenuButton_Click;
            }

            if (deleteSelectedRowsMenuButton != null)
            {
                deleteSelectedRowsMenuButton.Click += DeleteSelectedRowsMenuButton_Click;
            }
        }

        private void dataPagerControl_PageIndexChanged(object sender, EventArgs e)
        {
            isSelectionChangedFromDataPagerControl = true;
            PagedDataProvider.MoveCurrentToPosition(-1);
            GetFirstAndLastIndexInCurrentPage();
            //SelectedItems.Clear();
            //SelectedIndex = -1;

            //翻页时，检索该页中记录对应的已经选中的地物，并将地物添加到SelectedItems集合中
            foreach (Feature item in SelectedFeatures) //这里报错，原因是SelectedItems改变，触发了事件。
            {
                int indexInitemsSource = -1;
                //indexInitemsSource = GetMatchFeatureIndex(item, indexInitemsSource);

                //if (firstItemIndexInCurrentView <= indexInitemsSource && lastItemIndexInCurrentView >= indexInitemsSource)
                //{
                //    SelectedItems.Add(itemsSource.AsList()[indexInitemsSource]);
                //}

                for (int i = firstItemIndexInCurrentView; i <= lastItemIndexInCurrentView; i++)
                {
                    if (FeaturesLayer.Features[i].Equals(item))
                    {
                        indexInitemsSource = FeaturesLayer.Features.IndexOf(item);
                        SelectedItems.Add(itemsSource.AsList()[indexInitemsSource]);
                    }
                }
            }
        }

        //private int GetMatchFeatureIndex(Feature item, int indexInitemsSource)
        //{
        //    foreach (var targetFeature in FeaturesLayer.Features)
        //    {
        //        if (targetFeature.Equals(item))
        //        {
        //            indexInitemsSource = FeaturesLayer.Features.IndexOf(item);
        //            return indexInitemsSource;
        //        }
        //    }
        //    return -1;
        //}

        private void GetFirstAndLastIndexInCurrentPage()
        {
            firstItemIndexInCurrentView = dataPagerControl.PageIndex * dataPagerControl.PageSize;
            if (dataPagerControl.PageIndex + 1 < dataPagerControl.PageCount)
            {
                lastItemIndexInCurrentView = (dataPagerControl.PageIndex + 1) * dataPagerControl.PageSize - 1;
            }
            else
            {
                lastItemIndexInCurrentView = itemsSource.AsList().Count - 1;
            }
        }

        private void FeatureDataPager_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isSelectionChangedFromDataPagerControl)
            {
                isSelectionChangedFromDataPagerControl = false;
                return;
            }

            if (itemsSource == null || itemsSource.AsList().Count == 0)
            {
                return;
            }

            if (FeaturesLayer != null && FeaturesLayer.Features != null)
            {
                isSelectionChangedFromFeatureDataPager = true;
                SelectFeatures(e.AddedItems, true);
                SelectFeatures(e.RemovedItems, false);
                isSelectionChangedFromFeatureDataPager = false;

                selectedRecordsCount = SelectedFeatures.Count;

                //设置currentRecordNumberTextBox控件的当前值
                currentRecordNumber = this.PagedDataProvider.PageIndex * this.PageSize + this.PagedDataProvider.CurrentPosition;
                SetCurrentRecordNumberTextBox();

                if (autoChangeMapViewBoundsCheckBox != null && autoChangeMapViewBoundsCheckBox.IsChecked.Value && !isSelectionChangedFromFeaturesLayer)
                {
                    if (selectedRecordsCount < 10)
                    {
                        RoamToSelection(false);
                    }
                    else
                    {
                        RoamToSelection(true);
                    }
                }
            }
        }

        private void FeatureDataPager_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                int idx = GetRowIndexInRowsCollection(e.Row.DataContext);
                Feature relatedFeature = FeaturesLayer.Features[idx];

                if (relatedFeature != null)
                {
                    try
                    {
                        e.Row.DoFeatureRefresh(relatedFeature, objectType);
                    }
                    catch (Exception)
                    {
                        throw new ArgumentException(SuperMap.Web.Controls.Resources.Resource.FeatureDataGrid_No_Update_Exception);
                    }
                }
            }
        }

        private void CurrentRecordNumberTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox != null)
            {
                if (e.Key == Key.Enter)
                {
                    int convertedNo;
                    if (int.TryParse(textBox.Text, out convertedNo))
                    {
                        currentRecordNumber = convertedNo - 1;
                        ValidateCurrentRecordNumber();
                        SetCurrentRecordNumberTextBox();

                        /*当这两个函数合为一个函数时不起作用*/
                        SelectCurrentPage();
                        SelectCurrentRecord();
                    }
                    textBox.SelectAll();
                }
                else if ((e.PlatformKeyCode >= 48 && e.PlatformKeyCode <= 57) || (e.PlatformKeyCode >= 96 && e.PlatformKeyCode <= 105))
                {
                    if (textBox.SelectedText.Length > 0)
                    {
                        textBox.Text = textBox.Text.Remove(textBox.SelectionStart, textBox.SelectedText.Length);
                    }

                    textBox.Text += e.Key.ToString().Substring(e.Key.ToString().Length - 1);
                    textBox.SelectionStart = textBox.Text.Length;
                }
                e.Handled = true;
            }
        }

        private void ValidateCurrentRecordNumber()
        {
            if (currentRecordNumber >= totalRecordsCount)
            {
                currentRecordNumber = totalRecordsCount - 1;
            }
            else if (currentRecordNumber < 0)
            {
                currentRecordNumber = 0;
            }
        }

        private void OptionsButton_Click(object sender, RoutedEventArgs e)
        {
            if (popupMenu != null && popupMenu.Child is FrameworkElement)
            {
                double opacity = popupMenu.Opacity;
                popupMenu.Opacity = 0;
                popupMenu.IsOpen = true;
                Dispatcher.BeginInvoke((Action)delegate()
                {
                    popupMenu.VerticalOffset = -(popupMenu.Child as FrameworkElement).ActualHeight + optionsButton.ActualHeight;
                    popupMenu.HorizontalOffset = -(popupMenu.Child as FrameworkElement).ActualWidth;
                    popupMenu.Opacity = opacity;
                });
            }
        }

        private void PopupChild_MouseLeave(object sender, MouseEventArgs e)
        {
            popupMenu.IsOpen = false;
        }

        private void ClearSelectionMenuButton_Click(object sender, RoutedEventArgs e)
        {
            ClearSelection();
            if (popupMenu != null)
            {
                popupMenu.IsOpen = false;
            }
        }

        private void SwitchSelectionMenuButton_Click(object sender, RoutedEventArgs e)
        {
            SwitchSelection();
            if (popupMenu != null)
            {
                popupMenu.IsOpen = false;
            }
        }

        private void SelectAllMenuButton_Click(object sender, RoutedEventArgs e)
        {
            SelectAllRows();
            if (popupMenu != null)
            {
                popupMenu.IsOpen = false;
            }
        }

        private void ZoomToSelectionMenuButton_Click(object sender, RoutedEventArgs e)
        {
            RoamToSelection(true);
            if (popupMenu != null)
            {
                popupMenu.IsOpen = false;
            }
        }

        private void DeleteSelectedRowsMenuButton_Click(object sender, RoutedEventArgs e)
        {
            DeleteSelectedRows();
            if (popupMenu != null)
            {
                popupMenu.IsOpen = false;
            }
        }

        private void SetCurrentRecordNumberTextBox()
        {
            if (currentRecordNumberTextBox != null)
            {
                currentRecordNumberTextBox.Text = (currentRecordNumber + 1).ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        //private void SetNumberOfRecordsTextBlock(int countSelected, int countTotal)
        //{
        //    if (numberOfRecordsTextBlock != null)
        //    {
        //        numberOfRecordsTextBlock.Text = string.Format(textForNumberOfRecordsTextBlock, countSelected, countTotal);
        //    }
        //}

        private void ClearSelection()
        {
            if (itemsSource == null || selectedRecordsCount == 0)
            {
                return;
            }

            if (SelectedItems != null)
            {
                SelectedItems.Clear();
            }
        }

        private void SwitchSelection()
        {
            if (itemsSource != null)
            {
                if (dataPagerControl.PageSize == selectedRecordsCount)
                {
                    ClearSelection();
                }
                else if (selectedRecordsCount == 0)
                {
                    SelectAllRows();
                }
                else
                {
                    for (int i = firstItemIndexInCurrentView; i <= lastItemIndexInCurrentView; i++)
                    {
                        bool isFound = false;
                        foreach (object selectedRow in SelectedItems)
                        {
                            if (selectedRow.Equals(itemsSource.AsList()[i]))
                            {
                                isFound = true;
                                break;
                            }
                        }
                        if (isFound)
                        {
                            SelectedItems.Remove(itemsSource.AsList()[i]);
                        }
                        else
                        {
                            SelectedItems.Add(itemsSource.AsList()[i]);
                        }
                    }
                }
            }
        }

        private void SelectAllRows()
        {
            if (ItemsSource == null || dataPagerControl.PageCount == selectedRecordsCount)
            {
                return;
            }

            //foreach (object gridRow in ItemsSource)
            //{
            //    SelectedItems.Add(gridRow);
            //}
            for (int i = firstItemIndexInCurrentView; i <= lastItemIndexInCurrentView; i++)
            {
                SelectedItems.Add(itemsSource.AsList()[i]);
            }

        }

        private void DeleteSelectedRows()
        {
            List<object> currentSelectedItems = new List<object>();
            currentSelectedItems.AddRange(SelectedItems.OfType<object>());
            while (currentSelectedItems.Count > 0)
            {
                int idxToRemove = GetRowIndexInRowsCollection(currentSelectedItems[0]);
                if (FeaturesLayer != null)
                {
                    FeaturesLayer.Features.RemoveAt(idxToRemove);
                }
                else
                {
                    ItemsSource.DataSourceRemove(idxToRemove, objectType);
                }
                currentSelectedItems.RemoveAt(0);
            }
            ShowNumberOfRecords();
            ValidateCurrentRecordNumber();
            SelectCurrentPage();
            SelectCurrentRecord();
            SetCurrentRecordNumberTextBox();
        }

        private void SelectCurrentPage()
        {
            if (totalRecordsCount == 0)
            {
                return;
            }
            if (dataPagerControl != null)
            {
                dataPagerControl.PageIndex = currentRecordNumber / dataPagerControl.PageSize;
            }
        }

        private void SelectCurrentRecord()
        {
            if (totalRecordsCount == 0)
            {
                return;
            }

            IList gridRows = itemsSource.AsList();
            SelectedItems.Add(gridRows[currentRecordNumber]);
            ScrollGridRowIntoView(gridRows[currentRecordNumber]);
        }

        private void ShowNumberOfRecords()
        {
            SetRecordsCount();

            if (itemsSource == null || itemsSource.AsList().Count == 0)
            {
                currentRecordNumber = -1;
                SetCurrentRecordNumberTextBox();
                //SetNumberOfRecordsTextBlock(0, 0);
            }
            else
            {
                //SetNumberOfRecordsTextBlock(selectedRecordsCount, totalRecordsCount);
            }
        }

        private void SetRecordsCount()
        {
            if (itemsSource == null || itemsSource.AsList().Count == 0)
            {
                totalRecordsCount = 0;
            }
            else
            {
                totalRecordsCount = itemsSource.AsList().Count;
            }
        }

        //漫游到所选项
        private void RoamToSelection(bool isZoom)
        {
            if (selectedRecordsCount == 0)
            {
                return;
            }

            if (Map != null && SelectedFeatures != null)
            {
                double xMin = 0.0, yMin = 0.0, xMax = 0.0, yMax = 0.0;
                foreach (Feature feature in SelectedFeatures)
                {
                    Rectangle2D featureBounds = feature.Geometry.Bounds;
                    if (xMin == 0.0 && yMin == 0.0 && xMax == 0.0 && yMax == 0.0)
                    {
                        xMin = featureBounds.Left;
                        yMin = featureBounds.Bottom;
                        xMax = featureBounds.Right;
                        yMax = featureBounds.Top;
                    }
                    else
                    {
                        xMin = Math.Min(xMin, featureBounds.Left);
                        yMin = Math.Min(yMin, featureBounds.Bottom);
                        xMax = Math.Max(xMax, featureBounds.Right);
                        yMax = Math.Max(yMax, featureBounds.Top);
                    }
                }

                Rectangle2D newMapViewBounds = new Rectangle2D(xMin, yMin, xMax, yMax);
                if (isZoom)
                {
                    newMapViewBounds.Inflate(newMapViewBounds.Width * EXPAND_BOUNDS_RATIO, newMapViewBounds.Height);
                    Map.ZoomTo(newMapViewBounds);
                }
                else
                {
                    Map.PanTo(newMapViewBounds.Center);
                }

            }
        }

        private IEnumerable<IDictionary<string, object>> GetFeaturesEnumerable(FeatureCollection features)
        {
            return (from a in features select a.Attributes).AsEnumerable<IDictionary<string, object>>();
        }

        private void SetItemsSource(IEnumerable<IDictionary<string, object>> source)
        {
            itemsSource = source.ToDs(out objectType);
            PagedDataProvider = new PagedCollectionView(itemsSource);
            PagedDataProvider.MoveCurrentToPosition(-1);
            ItemsSource = PagedDataProvider;

            if (dataPagerControl != null)
            {
                dataPagerControl.Source = PagedDataProvider;
            }

            SetTotalRecordInTextblock();
        }

        private void SetTotalRecordInTextblock()
        {
            if (totalRecordsTextBox != null)
            {
                totalRecordsTextBox.Text = itemsSource.AsList().Count.ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        private object GetCorrespondingGridRow(Feature feature)
        {
            if (feature != null)
            {
                //该代码被替代
                //int idx = GetFeatureIndexInFeaturesCollection(feature);
                //if (idx > -1)
                //{
                //    IList gridRows = itemsSource.AsList();
                //    if (idx < gridRows.Count)
                //    {
                //        return gridRows[idx];
                //    }
                //}

                currentRecordNumber = GetFeatureIndexInFeaturesCollection(feature);
                if (currentRecordNumber > -1)
                {
                    IList gridRows = itemsSource.AsList();
                    if (currentRecordNumber < gridRows.Count)
                    {
                        return gridRows[currentRecordNumber];
                    }
                }
            }

            return null;
        }

        private Feature GetCorrespondingFeature(object dataGridRow)
        {
            if (dataGridRow != null)
            {
                int idx = GetRowIndexInRowsCollection(dataGridRow);
                if (idx > -1 && idx < FeaturesLayer.Features.Count)
                {
                    return FeaturesLayer.Features[idx];
                }
            }

            return null;
        }

        private void Feature_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (isSelectionChangedFromFeatureDataPager)
            {
                return;
            }

            if (e.PropertyName == "Selected")
            {
                Feature feature = sender as Feature;
                if (feature != null)
                {
                    object correspondingGridRow = GetCorrespondingGridRow(feature);
                    if (correspondingGridRow != null)
                    {
                        isSelectionChangedFromFeaturesLayer = true;

                        if (feature.Selected)
                        {
                            //如果选择的地物所对应的记录没有在当前页上，那么将翻页，
                            SelectCurrentPage();
                            SelectCurrentRecord();
                            SelectedItems.Add(correspondingGridRow);
                            //selectedFeatures.Add(feature);
                        }
                        else
                        {
                            SelectedItems.Remove(correspondingGridRow);
                            //selectedFeatures.Remove(feature);
                        }
                        isSelectionChangedFromFeaturesLayer = false;
                    }
                }
            }
        }

        private void Features_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            IEnumerable<IDictionary<string, object>> attr = GetFeaturesEnumerable(sender as FeatureCollection);
            if (itemsSource == null || e.Action == NotifyCollectionChangedAction.Reset)
            {
                SetItemsSource(attr);
            }
            else if (itemsSource.AsList().Count == 0 && e.Action == NotifyCollectionChangedAction.Add && e.NewItems != null && e.NewItems.Count > 0)
            {
                SetItemsSource(attr);
                IEnumerator enumAddedFeatures = e.NewItems.GetEnumerator();
                while (enumAddedFeatures.MoveNext())
                {
                    Feature feature = enumAddedFeatures.Current as Feature;
                    if (feature != null)
                    {
                        feature.PropertyChanged += Feature_PropertyChanged;
                        SetRecordsCount();
                    }
                }
            }
            else
            {
                if (e.Action != NotifyCollectionChangedAction.Reset)
                {
                    if (e.Action == NotifyCollectionChangedAction.Add &&
                        e.NewItems != null && e.NewItems.Count > 0)
                    {
                        IEnumerator enumAddedFeatures = e.NewItems.GetEnumerator();
                        int idxLastNewFeature = -1;

                        while (enumAddedFeatures.MoveNext())
                        {
                            Feature feature = enumAddedFeatures.Current as Feature;
                            if (feature != null)
                            {
                                bool isNewFeature = (feature.Attributes.Count == 0);
                                itemsSource.AddToDataSource(feature, objectType);
                                feature.PropertyChanged += Feature_PropertyChanged;
                                SetRecordsCount();
                                if (isNewFeature)
                                {
                                    idxLastNewFeature = totalRecordsCount - 1;
                                }
                            }
                        }
                        if (idxLastNewFeature > -1)
                        {
                            ScrollGridRowIntoView(itemsSource.AsList()[idxLastNewFeature]);
                        }

                        SetTotalRecordInTextblock();
                    }
                    if (e.Action == NotifyCollectionChangedAction.Remove &&
                        e.OldItems != null && e.OldItems.Count > 0)
                    {
                        int selCount = SelectedItems.Count;
                        List<object> selItems = new List<object>(selCount);
                        for (int i = 0; i < selCount; i++)
                        {
                            selItems.Add(SelectedItems[i]);
                        }
                        IEnumerator enumRemovedFeatures = e.OldItems.GetEnumerator();
                        while (enumRemovedFeatures.MoveNext())
                        {
                            Feature feature = enumRemovedFeatures.Current as Feature;
                            if (feature != null)
                            {
                                object gridRow = itemsSource.AsList()[e.OldStartingIndex];
                                if (gridRow != null)
                                {
                                    selItems.Remove(gridRow);
                                }
                                itemsSource.DataSourceRemove(e.OldStartingIndex, objectType);
                                feature.PropertyChanged -= Feature_PropertyChanged;

                                SelectedFeatures.Remove(feature);
                            }
                        }
                        if (selItems.Count > 0)
                        {
                            SelectedItems.Clear();
                            for (int i = 0; i < selItems.Count; i++)
                            {
                                SelectedItems.Add(selItems[i]);
                            }
                        }

                        SetTotalRecordInTextblock();
                    }
                }
                else
                {
                    SetItemsSource(attr);
                }
            }
            GetFirstAndLastIndexInCurrentPage();
        }

        private void ScrollGridRowIntoView(object item)
        {
            ScrollIntoView(item, null);
        }

        private int GetFeatureIndexInFeaturesCollection(Feature feature)
        {
            if (FeaturesLayer != null)
            {
                return FeaturesLayer.Features.IndexOf(feature);
            }
            else
            {
                return -1;
            }
        }

        private void SelectFeatures(IList rowsToLookup, bool shouldSelectFeatures)
        {
            foreach (object objRow in rowsToLookup)
            {
                int idx = GetRowIndexInRowsCollection(objRow);
                if (idx > -1)
                {
                    Feature feature = FeaturesLayer.Features[idx];
                    feature.Selected = shouldSelectFeatures;

                    if (shouldSelectFeatures)
                    {
                        currentRecordNumber = idx;
                        //selectedFeatures.Add(FeaturesLayer.Features[idx]);
                    }
                    else
                    {
                        if (SelectedFeatures.Count == 0)
                        {
                            currentRecordNumber = -1;
                        }
                        //selectedFeatures.Remove(FeaturesLayer.Features[idx]);
                    }
                }
            }
        }

        private int GetRowIndexInRowsCollection(object gridRow)
        {
            if (gridRow == null)
            {
                return -1;
            }

            int idx = -1;
            foreach (object obj in itemsSource)
            {
                idx++;
                if (obj.Equals(gridRow))
                {
                    return idx;
                }
            }

            return -1;
        }
        /// <summary>${controls_FeatureDataPager_attribute_SelectedFeatures_D}</summary>
        public IList<Feature> SelectedFeatures
        {
            get
            {
                return getSelectedFeatures();
            }
        }

        private IList<Feature> getSelectedFeatures()
        {
            List<Feature> selectedFeatures = new List<Feature>();
            if (FeaturesLayer != null && FeaturesLayer.Features != null)
            {
                foreach (var item in FeaturesLayer.Features)
                {
                    if (item.Selected)
                    {
                        selectedFeatures.Add(item);
                    }
                }
            }
            return selectedFeatures;
        }
        /// <summary>${controls_FeatureDataPager_method_Clear_D}</summary>
        public void Clear()
        {
            if (itemsSource != null && itemsSource.AsList().Count > 0)
            {
                itemsSource.AsList().Clear();
            }
        }
        /// <summary>${controls_FeatureDataPager_method_RefreshRow_D}</summary>
        public void DoRowRefresh(Feature feature)
        {
            autoChangeMapViewBoundsCheckBox.IsChecked = false;

            int idx = GetFeatureIndexInFeaturesCollection(feature);
            IList gridRows = itemsSource.AsList();
            if (idx > -1 && idx < gridRows.Count)
            {
                try
                {
                    int selCount = SelectedItems.Count;
                    int[] selIndexes = new int[selCount];
                    for (int i = 0; i < selCount; i++)
                    {
                        selIndexes[i] = GetRowIndexInRowsCollection(SelectedItems[i]);
                    }
                    feature.DoRowRefresh(itemsSource, idx, objectType);
                    for (int i = 0; i < selCount; i++)
                    {
                        SelectedItems.Add(itemsSource.AsList()[selIndexes[i]]);
                    }

                }
                catch (Exception)
                {
                    throw new ArgumentException(SuperMap.Web.Controls.Resources.Resource.FeatureDataGrid_Row_Editing_Exception);
                }
            }
        }
        /// <summary>${controls_FeatureDataPager_method_ScrollIntoView_D}</summary>
        public void ScrollIntoView(Feature feature, DataGridColumn column)
        {
            int idx = GetFeatureIndexInFeaturesCollection(feature);
            if (idx > -1)
            {
                if (FeaturesLayer != null && FeaturesLayer.Features.Contains(feature))
                {
                    IList gridRows = itemsSource.AsList();
                    ScrollIntoView(gridRows[idx], column);
                }
            }
        }


        private class FeaturesSelectionList1 : IList<Feature>
        {
            private FeatureDataPager owner;

            public FeaturesSelectionList1(FeatureDataPager owner)
            {
                this.owner = owner;
            }

            public int Add(Feature value)
            {
                object correspondingGridRow = owner.GetCorrespondingGridRow(value);
                if (correspondingGridRow != null)
                {
                    return owner.SelectedItems.Add(correspondingGridRow);
                }

                return -1;
            }

            public void Clear()
            {
                owner.SelectedItems.Clear();
            }

            public bool Contains(Feature value)
            {
                object correspondingGridRow = owner.GetCorrespondingGridRow(value);
                if (correspondingGridRow != null)
                {
                    return owner.SelectedItems.Contains(correspondingGridRow);
                }

                return false;
            }

            public int IndexOf(Feature value)
            {
                object correspondingGridRow = owner.GetCorrespondingGridRow(value);
                if (correspondingGridRow != null)
                {
                    return owner.SelectedItems.IndexOf(correspondingGridRow);
                }

                return -1;
            }

            public void Insert(int index, Feature value)
            {
                object correspondingGridRow = owner.GetCorrespondingGridRow(value);
                if (correspondingGridRow != null)
                {
                    owner.SelectedItems.Insert(index, correspondingGridRow);
                }
            }

            public bool IsFixedSize
            {
                get { return owner.SelectedItems.IsFixedSize; }
            }

            public bool IsReadOnly
            {
                get { return owner.SelectedItems.IsReadOnly; }
            }

            public void Remove(Feature value)
            {
                object correspondingGridRow = owner.GetCorrespondingGridRow(value);
                if (correspondingGridRow != null)
                {
                    owner.SelectedItems.Remove(correspondingGridRow);
                }
            }

            public void RemoveAt(int index)
            {
                owner.SelectedItems.RemoveAt(index);
            }

            public Feature this[int index]
            {
                get
                {
                    int idxInItemsSource = owner.GetRowIndexInRowsCollection(owner.SelectedItems[index]);
                    if (idxInItemsSource > -1)
                    {
                        return owner.FeaturesLayer.Features[idxInItemsSource];
                    }
                    else
                    {
                        int featuresCount = owner.FeaturesLayer.Features.Count;
                        if (featuresCount == 0)
                        {
                            throw new ArgumentOutOfRangeException(SuperMap.Web.Controls.Resources.Resource.FeatureDataGrid_No_Item_Exception);
                        }
                        else
                        {
                            throw new ArgumentOutOfRangeException(SuperMap.Web.Controls.Resources.Resource.FeatureDataGrid_Index_Range_Exception);
                        }
                    }
                }
                set
                {
                    int idxInFeaturesCollection = owner.GetFeatureIndexInFeaturesCollection(value);
                    if (idxInFeaturesCollection > -1)
                    {
                        owner.SelectedItems[index] = owner.itemsSource.AsList()[idxInFeaturesCollection];
                    }
                }
            }

            public IEnumerator GetEnumerator()
            {
                return ((IEnumerable<Feature>)this).GetEnumerator();
            }

            IEnumerator<Feature> IEnumerable<Feature>.GetEnumerator()
            {
                foreach (Feature item in owner.FeaturesLayer.Features)
                {
                    yield return GetSelectedFeature(item);
                }
            }

            private Feature GetSelectedFeature(Feature f)
            {
                if (f.Selected)
                {
                    return f;
                }
                return null;
            }

            public void CopyTo(Array array, int index)
            {
                ((ICollection<Feature>)this).CopyTo((Feature[])array, index);
            }

            public int Count
            {
                get { return owner.SelectedItems.Count; }
            }

            public bool IsSynchronized
            {
                get { return owner.SelectedItems.IsSynchronized; }
            }

            public object SyncRoot
            {
                get { return owner.SelectedItems.SyncRoot; }
            }

            void ICollection<Feature>.Add(Feature item)
            {
                int i = this.Add(item);
            }

            public void CopyTo(Feature[] array, int arrayIndex)
            {
                List<object> correspondingGridRows = new List<object>(array.Length);
                foreach (Feature feature in array)
                {
                    object correspondingGridRow = owner.GetCorrespondingGridRow(feature);
                    if (correspondingGridRow != null)
                    {
                        correspondingGridRows.Add(correspondingGridRow);
                    }
                }
                if (correspondingGridRows.Count > 0)
                {
                    owner.SelectedItems.CopyTo(correspondingGridRows.ToArray(), arrayIndex);
                }
            }

            bool ICollection<Feature>.Remove(Feature item)
            {
                try
                {
                    this.Remove(item);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}