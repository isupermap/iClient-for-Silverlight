﻿using SuperMap.Web.Core;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using System.Reflection.Emit;
using System.Text;
using System;
using System.Windows.Controls;

namespace SuperMap.Web.Controls
{
    internal static class DataSourceCreator
    {
        private const string NULL_EXCEPTION_MSG_IDICT = "The Entry of IDictionary Type cannot be null.";
        private const string TYPE_EXCEPTION_MSG_OBJECT = "Please Invalid Object Type.";

        private static readonly Dictionary<string , Type> signatureType = new Dictionary<string , Type>();
        private static Dictionary<string , string> mappingfield = null;
        
        private static readonly Regex badNameRegex = new Regex(@"[^A-Z^a-z^0-9^_^\u4e00-\u9fa5^ ]", RegexOptions.Singleline);

        private static string GetSignatureType(IDictionary fd)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string s in fd.Keys)
            {
                stringBuilder.AppendFormat("_{0}_{1}" , new object[] { s , fd[s] });
            }
            return stringBuilder.ToString().GetHashCode().ToString(System.Globalization.CultureInfo.InvariantCulture).Replace("-", "Minus");
        }

        private static IEnumerable GenEnumerable(Type obj , IEnumerable<IDictionary<string , object>> listDic , IDictionary fd)
        {
            Type listTypes = typeof(ObservableCollection<>).MakeGenericType(new[] { obj });
            object listCustom = Activator.CreateInstance(listTypes);

            foreach (var cd in listDic)
            {
                if (cd == null)
                {
                    throw new ArgumentException(NULL_EXCEPTION_MSG_IDICT);
                }

                var row = Activator.CreateInstance(obj);
                foreach (string pair in fd.Keys)
                {
                    if (cd.ContainsKey(pair))
                    {
                        PropertyInfo prop = obj.GetProperty(mappingfield[pair]);
                        prop.SetValue(row , Convert.ChangeType(cd[pair] , prop.PropertyType , null) , null);
                    }
                }
                listTypes.GetMethod("Add").Invoke(listCustom , new[] { row });
            }
            return listCustom as IEnumerable;
        }

        private static TypeBuilder GetBuilderType(string ts)
        {
            AssemblyName assemblyName = new AssemblyName("FeatureDataGrid." + ts);
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName , AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("MainModule");

            TypeBuilder typeBuilder = moduleBuilder.DefineType("TempType" + ts
                                , TypeAttributes.Public | TypeAttributes.Class | TypeAttributes.AutoClass | TypeAttributes.AnsiClass | TypeAttributes.BeforeFieldInit | TypeAttributes.AutoLayout
                                , typeof(object));
            return typeBuilder;
        }

        private static void CreateProp(TypeBuilder tb , string propName , Type propType)
        {
            FieldBuilder fieldBld = tb.DefineField("_" + propName , propType , FieldAttributes.Private);

            PropertyBuilder propertyBld =
                tb.DefineProperty(
                    propName , PropertyAttributes.HasDefault , propType , null);
            MethodBuilder getPropMthdBld = tb.DefineMethod("get_" + propName ,
                    MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Public ,
                    propType , Type.EmptyTypes);

            ILGenerator getILgg = getPropMthdBld.GetILGenerator();

            getILgg.Emit(OpCodes.Ldarg_0);
            getILgg.Emit(OpCodes.Ldfld , fieldBld);
            getILgg.Emit(OpCodes.Ret);

            MethodBuilder setPropMthdBld = tb.DefineMethod("set_" + propName ,
                MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Public ,
                  null , new Type[] { propType });

            ILGenerator setILgg = setPropMthdBld.GetILGenerator();

            setILgg.Emit(OpCodes.Ldarg_0);
            setILgg.Emit(OpCodes.Ldarg_1);
            setILgg.Emit(OpCodes.Stfld , fieldBld);
            setILgg.Emit(OpCodes.Ret);

            propertyBld.SetGetMethod(getPropMthdBld);
            propertyBld.SetSetMethod(setPropMthdBld);
        }

        private static FieldInfo[] GetFldInfo(Type obj)
        {
            return obj.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
        }

        private static void AddToEnum(Type obj , IDictionary fd , IEnumerable items)
        {
            if (fd == null)
            {
                throw new ArgumentException(NULL_EXCEPTION_MSG_IDICT);
            }

            var listType = typeof(ObservableCollection<>).MakeGenericType(new[] { obj });

            listType.GetMethod("Add").Invoke(items , new[] { EntityToObject(obj , fd) });
        }

        private static void InsertIntoEn(Type objectType , IDictionary firstDict , IEnumerable itemsSource , int atIndex)
        {
            if (firstDict == null)
            {
                throw new ArgumentException(NULL_EXCEPTION_MSG_IDICT);
            }

            var listType = typeof(ObservableCollection<>).MakeGenericType(new[] { objectType });
            var row = EntityToObject(objectType , firstDict);

            listType.GetMethod("Insert").Invoke(itemsSource , new[] { atIndex , row });
        }

        private static string GetUnique(Dictionary<string , string> dic , string value)
        {
            int i = 0;
            string prefixion = "";
            while (dic.ContainsValue(value + prefixion))
            {
                prefixion = string.Format(System.Globalization.CultureInfo.InvariantCulture, "_{0}", ++i);
            }

            string result = string.Format("{0}{1}" , value , prefixion);
            return result;
        }

        private static string GetKey(string targetKey)
        {
            string mapKey = targetKey;
            MatchCollection improNames = badNameRegex.Matches(targetKey , 0);

            if (improNames.Count > 0)
            {
                foreach (Match improperName in improNames)
                {
                    mapKey = targetKey.Replace(improperName.Value , "_");
                }
            }
            if (mappingfield.ContainsValue(mapKey))
            {
                mapKey = GetUnique(mappingfield , mapKey);
            }

            mappingfield.Add(targetKey , mapKey);

            return mapKey;
        }

        private static object EntityToObject(Type obj , IDictionary fd)
        {
            var row = Activator.CreateInstance(obj);

            foreach (string pair in fd.Keys)
            {
                PropertyInfo prop = obj.GetProperty(pair);

                try
                {
                    prop.SetValue(row , Convert.ChangeType(fd[pair] , prop.PropertyType , null) , null);
                }
                catch (Exception ex)
                {
                    throw new ArgumentException(TYPE_EXCEPTION_MSG_OBJECT , ex);
                }
            }
            return row;
        }

        //扩展方法
        private static string KV(this Dictionary<string , string> dict , string v)
        {
            if (dict.ContainsValue(v))
            {
                foreach (string key in dict.Keys)
                {
                    if (dict[key] == v)
                    {
                        return key;
                    }
                }
            }
            return null;
        }

        internal static IEnumerable ToDs(this IEnumerable<IDictionary<string , object>> list , out Type obj)
        {
            obj = null;
            IDictionary fd = null;
            bool hasData = false;
            foreach (IDictionary currentDict in list)
            {
                hasData = true;
                fd = currentDict;
                break;
            }
            if (!hasData)
            {
                return new object[] { };
            }
            if (fd == null)
            {
                throw new ArgumentException(NULL_EXCEPTION_MSG_IDICT);
            }

            string typeSignature = GetSignatureType(fd);

            Type type;
            obj = signatureType.TryGetValue(typeSignature , out type) ? type : null;
            if (obj == null)
            {
                TypeBuilder tb = GetBuilderType(typeSignature);

                ConstructorBuilder constructor = tb.DefineDefaultConstructor(MethodAttributes.Public |
                                                                             MethodAttributes.SpecialName |
                                                                             MethodAttributes.RTSpecialName);

                mappingfield = new Dictionary<string , string>();
                foreach (string str in fd.Keys)
                {
                    string keyToUse = GetKey(str);
                    CreateProp(tb , keyToUse , fd[str] == null ? typeof(object) : fd[str].GetType());
                }
                obj = tb.CreateType();
                signatureType.Add(typeSignature , obj);
            }
            else
            {
                mappingfield = new Dictionary<string , string>();
                foreach (string str in fd.Keys)
                {
                    string keyToUse = GetKey(str);//等于是给_fieldMapping拿到原来的值
                }
            }

            return GenEnumerable(obj , list , fd);
        }

        internal static IList AsList(this IEnumerable itemsSource)
        {
            return (IList)itemsSource;
        }

        internal static void AddToDataSource(this IEnumerable itemsSource , Feature feature , Type objectType)
        {
            Dictionary<string , object> dictionary = new Dictionary<string , object>();

            if (feature.Attributes.Count == 0)
            {
                if (objectType != null)
                {
                    FieldInfo[] fieldInfo = GetFldInfo(objectType);
                    foreach (FieldInfo fldInfo in fieldInfo)
                    {
                        Type fldType = fldInfo.FieldType;
                        string fldName = fldInfo.Name.Substring(1);

                        object defaultValue = fldType.IsValueType ? Activator.CreateInstance(fldType) : null;
                        dictionary.Add(fldName , defaultValue);

                        feature.Attributes.Add(mappingfield.KV(fldName) , defaultValue);
                    }
                }
            }
            else
            {
                foreach (KeyValuePair<string , object> keyVal in feature.Attributes)
                {
                    dictionary.Add(mappingfield[keyVal.Key] , keyVal.Value);
                }
            }

            if (dictionary.Keys.Count == 0)
            {
                throw new ArgumentException(NULL_EXCEPTION_MSG_IDICT);

            }

            if (objectType != null)
            {
                AddToEnum(objectType , dictionary , itemsSource);
            }
        }

        internal static void DataSourceRemove(this IEnumerable source , int index , Type type)
        {
            //var typeLists = typeof(ObservableCollection<>).MakeGenericType(new[] { type });
            //typeLists.GetMethod("RemoveAt").Invoke(source , new[] { (object)index });
            typeof(ObservableCollection<>).MakeGenericType(new[] { type }).GetMethod("RemoveAt").Invoke(source , new[] { (object)index });
        }

        internal static void DoFeatureRefresh(this DataGridRow row , Feature targetFeature , Type type)
        {
            object dataContext = row.DataContext;

            if (dataContext != null && type != null)
            {
                FieldInfo[] fldInfos = GetFldInfo(type);
                foreach (FieldInfo fldInfo in fldInfos)
                {
                    string key = fldInfo.Name.Substring(1);
                    try
                    {
                        targetFeature.Attributes[mappingfield.KV(key)] = dataContext.GetType().GetProperty(key).GetValue(dataContext , null);
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException(TYPE_EXCEPTION_MSG_OBJECT + key , ex);
                    }
                }
            }
            else
            {
                throw new NullReferenceException(TYPE_EXCEPTION_MSG_OBJECT);
            }
        }

        internal static void DoRowRefresh(this Feature targetFeature , IEnumerable source , int itemIndex , Type type)
        {
            if (type != null)
            {
                Dictionary<string , object> dic = new Dictionary<string , object>();

                foreach (KeyValuePair<string , object> keyValue in targetFeature.Attributes)
                {
                    dic.Add(mappingfield[keyValue.Key] , keyValue.Value);
                }

                source.AsList()[itemIndex] = EntityToObject(type , dic);
            }
        }
    }
}