﻿using System;
using System.Windows;
using System.Windows.Input;
using SuperMap.Web.Utilities;
using SuperMap.Web.Mapping;

namespace SuperMap.Web.Actions
{
    internal class OvPan : MapAction
    {
        private bool isMouseCaptured;
        private double oldMouseX;
        private double oldMouseY;

        private Map mapBig;


        public OvPan(Map map, Map mapBig)
            : this(map, mapBig, Cursors.Arrow)
        {
        }

        public OvPan(Map map, Map mapBig, Cursor cursor)
            : base(map, "OvPan", cursor)
        {
            this.mapBig = mapBig;
        }

        public override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            oldMouseX = e.GetPosition(Map).X;
            oldMouseY = e.GetPosition(Map).Y;
            isMouseCaptured = true;
            Map.CaptureMouse();
            e.Handled = true;
            base.OnMouseLeftButtonDown(e);
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (isMouseCaptured)
            {
                double newMouseX = e.GetPosition(Map).X;
                double newMouseY = e.GetPosition(Map).Y;
                double deltaX;
                double deltaY;
                deltaX = newMouseX - oldMouseX;
                deltaY = newMouseY - oldMouseY;

                Map.PanByPixel(-deltaX, -deltaY);

                oldMouseY = e.GetPosition(Map).Y;
                oldMouseX = e.GetPosition(Map).X;
            }
            base.OnMouseMove(e);
        }

        public override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            isMouseCaptured = false;
            Map.ReleaseMouseCapture();
            oldMouseY = -1;
            oldMouseX = -1;

            mapBig.PanTo(Map.ViewBounds.Center);

            e.Handled = true;
            base.OnMouseLeftButtonUp(e);
        }

        public override void Deactivate()
        {

        }

    }
}