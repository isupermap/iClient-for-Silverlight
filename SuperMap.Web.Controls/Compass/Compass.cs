﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using SuperMap.Web.Mapping;
namespace SuperMap.Web.Controls
{
    /// <summary>
    /// 	<para>${controls_Compass_Title}</para>
    /// 	<para>${controls_Compass_Description}</para>
    /// 	<para><img src="compass.png"/></para>
    /// </summary>
    [TemplatePart(Name = "PanUpElement", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "PanDownElement", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "PanLeftElement", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "PanRightElement", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "ViewEntireElement", Type = typeof(Button))]

    [TemplatePart(Name = "RotateRingElement", Type = typeof(FrameworkElement))]
    [TemplatePart(Name = "TransformRotateElement", Type = typeof(RotateTransform))]

    public class Compass : Control
    {
        private RepeatButton panDownElement;
        private RepeatButton panLeftElement;
        private RepeatButton panRightElement;
        private RepeatButton panUpElement;
        private Button viewEntireElement;

        private FrameworkElement rotateRingElement;
        private RotateTransform transformRotateElement;

        private Map.AngleChangedEventHandler angleChangedHandler;
        /// <summary>${controls_Compass_field_MapProperty_D}</summary>
        public static readonly DependencyProperty MapProperty = DependencyProperty.Register("Map", typeof(Map), typeof(Compass), new PropertyMetadata(new PropertyChangedCallback(OnMapPropertyChanged)));

        /// <summary>${controls_Compass_attribute_map_D}</summary>
        public Map Map
        {
            get
            {
                return (base.GetValue(MapProperty) as Map);
            }
            set
            {
                base.SetValue(MapProperty, value);
            }
        }

        private static void OnMapPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Compass com = d as Compass;
            Map oldValue = e.OldValue as Map;
            Map newValue = e.NewValue as Map;

            if (oldValue != null)
            {
                oldValue.AngleChanged -= com.angleChangedHandler;
            }
            if (com.angleChangedHandler == null)
            {
                com.angleChangedHandler = new Map.AngleChangedEventHandler(com.Map_RotationChanged);
            }
            if (newValue != null)
            {
                newValue.AngleChanged += com.angleChangedHandler;
            }
            com.ChangeAngle();
        }

        /// <summary>${controls_Compass_constructor_None_D}</summary>
        public Compass()
        {
            base.DefaultStyleKey = typeof(Compass);
        }

        /// <summary>${controls_Compass_method_onApplyTemplate_D}</summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.rotateRingElement = base.GetTemplateChild("RotateRingElement") as FrameworkElement;
            this.transformRotateElement = base.GetTemplateChild("TransformRotateElement") as RotateTransform;

            this.panLeftElement = base.GetTemplateChild("PanLeftElement") as RepeatButton;
            this.panRightElement = base.GetTemplateChild("PanRightElement") as RepeatButton;
            this.panUpElement = base.GetTemplateChild("PanUpElement") as RepeatButton;
            this.panDownElement = base.GetTemplateChild("PanDownElement") as RepeatButton;
            this.viewEntireElement = base.GetTemplateChild("ViewEntireElement") as Button;

            #region 上下左右
            if (this.panLeftElement != null)
            {
                this.panLeftElement.Click += (sender, args) =>
                           {
                               if ((this.Map != null) && (sender != null))
                               {
                                   this.Map.Pan(-this.Map.ViewBounds.Width * this.Map.PanFactor, 0);
                               }
                           };
            }

            if (this.panRightElement != null)
            {

                this.panRightElement.Click += (sender, args) =>
                {
                    if ((this.Map != null) && (sender != null))
                    {
                        this.Map.Pan(this.Map.ViewBounds.Width * this.Map.PanFactor, 0);
                    }
                };
            }
            if (this.panDownElement != null)
            {
                this.panDownElement.Click += (sender, args) =>
                {
                    if ((this.Map != null) && (sender != null))
                    {
                        this.Map.Pan(0, -this.Map.ViewBounds.Height * this.Map.PanFactor);
                    }
                };
            }

            if (this.panUpElement != null)
            {
                this.panUpElement.Click += (sender, args) =>
                {
                    if ((this.Map != null) && (sender != null))
                    {
                        this.Map.Pan(0, this.Map.ViewBounds.Height * this.Map.PanFactor);
                    }
                };
            }
            #endregion

            if (this.rotateRingElement != null && this.transformRotateElement != null)
            {
                this.rotateRingElement.MouseLeftButtonDown += new MouseButtonEventHandler(RotateRing_MouseLeftButtonDown);
                this.rotateRingElement.MouseMove += new MouseEventHandler(RotateRing_MouseMove);
                this.rotateRingElement.MouseLeftButtonUp += new MouseButtonEventHandler(RotateRing_MouseLeftButtonUp);
            }
            if (this.viewEntireElement != null)
            {
                this.viewEntireElement.Click += new RoutedEventHandler(this.ZoomViewEntire_Click);
            }
            ChangeAngle();
        }

        private void ChangeAngle()
        {
            if (this.transformRotateElement != null && this.Map != null)
            {
                this.transformRotateElement.Angle = this.Map.Angle;
            }
        }

        private bool GoToState(bool useTransitions, string stateName)
        {
            return VisualStateManager.GoToState(this, stateName, useTransitions);
        }

        #region 旋转
        private double angle;
        private bool isRotating;
        private double lastXPosition;
        private double lastYPosition;
        private const double DoubleClickTime = 222.0;
        private long lastMouseClick = 0L;

        private void RotateRing_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            var ui = sender as FrameworkElement;
            if (ui != null)
            {
                isRotating = true;
                ui.CaptureMouse();
                lastXPosition = e.GetPosition(ui).X - (ui.ActualWidth / 2);
                lastYPosition = e.GetPosition(ui).Y - (ui.ActualHeight / 2);
            }
            angle = transformRotateElement.Angle;

        }
        private void RotateRing_MouseMove(object sender, MouseEventArgs e)
        {
            if (isRotating)
            {
                var ui = sender as FrameworkElement;
                if (ui != null)
                {
                    double x = e.GetPosition(ui).X - (ui.ActualWidth / 2);
                    double y = e.GetPosition(ui).Y - (ui.ActualWidth / 2);
                    double theta = Math.Atan2(y, x) - Math.Atan2(lastYPosition, lastXPosition);
                    double angleInDegrees = Math.Round(theta * 180 / Math.PI);

                    angle = angle + angleInDegrees;

                    if (Map != null)
                    {
                        Map.Angle = angle;
                    }

                    transformRotateElement.Angle = angle;
                }
            }

        }
        private void RotateRing_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            long num = DateTime.Now.Ticks - this.lastMouseClick;
            if (TimeSpan.FromTicks(num).TotalMilliseconds < DoubleClickTime)
            {
                ResetNorth();
                lastMouseClick = 0L;
            }
            else
            {
                lastMouseClick = DateTime.Now.Ticks;
            }
            e.Handled = true;
            var ui = sender as FrameworkElement;
            if (ui != null)
            {
                isRotating = false;
                ui.ReleaseMouseCapture();
            }

        }

        private void Map_RotationChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            double newValue = (double)e.NewValue;
            if ((this.transformRotateElement != null) && (this.transformRotateElement.Angle != newValue))
            {
                this.transformRotateElement.Angle = newValue;
            }
            this.angle = newValue;
        }
        private void ResetNorth()
        {
            Storyboard storyboard = new Storyboard();
            storyboard.Duration = TimeSpan.FromMilliseconds(500.0);
            DoubleAnimationUsingKeyFrames frames = new DoubleAnimationUsingKeyFrames();
            SplineDoubleKeyFrame frame2 = new SplineDoubleKeyFrame();
            frame2.KeyTime = storyboard.Duration.TimeSpan;
            frame2.Value = 0.0;
            KeySpline spline = new KeySpline();
            spline.ControlPoint1 = new Point(0.0, 0.1);
            spline.ControlPoint2 = new Point(0.1, 1.0);
            frame2.KeySpline = spline;
            SplineDoubleKeyFrame frame = frame2;
            frames.KeyFrames.Add(frame);
            if (this.Map.Angle > 180)
            {
                frame.Value = 360.0;
            }//正转到北
            if (this.Map.Angle <= 180)
            {
                frame.Value = 0.0;
            }//转回去
            frames.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("Angle"));
            storyboard.Children.Add(frames);
            Storyboard.SetTarget(frames, this.Map);
            storyboard.Begin();
        }
        #endregion

        private void ZoomViewEntire_Click(object sender, RoutedEventArgs e)
        {
            if (this.Map != null)
            {
                this.Map.ViewEntire();
            }
        }
    }
}
