﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using System.Globalization;

namespace SuperMap.Web.Controls
{
    /// <summary>
    /// 	<para>${controls_BookMark_Title}</para>
    /// 	<para>${controls_BookMark_Description}</para>
    /// 	<para><img src="bookMark.png"/></para>
    /// </summary>
    [TemplatePart(Name = "AddBookmark", Type = typeof(Button))]
    [TemplatePart(Name = "ClearBookmarks", Type = typeof(Button))]
    [TemplatePart(Name = "OneByOneClear", Type = typeof(Button))]
    [TemplatePart(Name = "BookmarkList", Type = typeof(DataGrid))]
    [TemplatePart(Name = "AddBookmarkName", Type = typeof(TextBox))]
    public class Bookmark : Control
    {
        private TextBox addBookmarkName;
        private Button addBookmarkButton;
        private Button clearBookmarksButton;
        private Button oneByOneClear;
        private DataGrid bookmarkList;
        private BookmarkInfo bookmark;
        private const string isolatedStorageKey = "SuperMap.Bookmarks";

        /// <summary>${controls_BookMark_constructor_None_D}</summary>
        public Bookmark()
        {
            DefaultStyleKey = typeof(Bookmark);
            Bookmarks = new ObservableCollection<BookmarkInfo>();
            EnableIsolatedStorage = true;
            this.Title = SuperMap.Web.Controls.Resources.Resource.Bookmark_Title;
            this.Loaded += new RoutedEventHandler(Bookmark_Loaded);
        }

        /// <summary>${controls_BookMark_method_onApplyTemplate_D}</summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            addBookmarkName = GetTemplateChild("AddBookmarkName") as TextBox;
            addBookmarkButton = GetTemplateChild("AddBookmark") as Button;
            bookmarkList = GetTemplateChild("BookmarkList") as DataGrid;
            clearBookmarksButton = GetTemplateChild("ClearBookmarks") as Button;
            oneByOneClear = GetTemplateChild("OneByOneClear") as Button;

            if (addBookmarkButton != null)
            {
                addBookmarkButton.Click += AddBookmarkButton_Click;
            }

            if (bookmarkList != null)
            {
                bookmarkList.ItemsSource = Bookmarks;
                bookmarkList.SelectionChanged += BookmarkList_SelectionChanged;
                bookmarkList.BeginningEdit += new EventHandler<DataGridBeginningEditEventArgs>(bookmarkList_BeginningEdit);
            }

            if (oneByOneClear != null)
            {
                oneByOneClear.Content = SuperMap.Web.Controls.Resources.Resource.Bookmark_Remove;
                oneByOneClear.Click += (o, e) => { ClearBookmarksOneByOne(); };
            }

            if (clearBookmarksButton != null)
            {
                clearBookmarksButton.Content = SuperMap.Web.Controls.Resources.Resource.Bookmark_Clear;
                clearBookmarksButton.Click += (o, e) => { Clear(); };
            }
        }

        private void bookmarkList_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            ZoomToViewBounds();
        }

        private void ZoomToViewBounds()
        {
            if (bookmark != null && Map != null && !Double.IsNaN(Map.Resolution))
            {
                Map.ZoomTo(bookmark.ViewBounds);
            }
        }

        private void Bookmark_Loaded(object sender, RoutedEventArgs e)
        {
            LoadBookmarks();
        }

        private void SaveBookmarks()
        {
            if (DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }
            try
            {
                if (IsolatedStorageSettings.ApplicationSettings.Contains(Key))
                {
                    IsolatedStorageSettings.ApplicationSettings.Remove(Key);
                }
                if (EnableIsolatedStorage && Bookmarks != null && Bookmarks.Count > 0)
                {
                    IsolatedStorageSettings.ApplicationSettings.Add(Key, Bookmarks);
                }
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
            catch
            {
            }
        }

        private bool isLoaded;
        private void LoadBookmarks()
        {
            if (EnableIsolatedStorage && !DesignerProperties.GetIsInDesignMode(this)
                && IsolatedStorageSettings.ApplicationSettings.Contains(Key))
            {
                ObservableCollection<BookmarkInfo> storedMarks = IsolatedStorageSettings.ApplicationSettings[Key] as ObservableCollection<BookmarkInfo>;
                if (storedMarks != null)
                {
                    int i = storedMarks.Count;
                    for (int k = 0; k < i; k++)
                    {
                        if (!isLoaded)
                            Bookmarks.Add(storedMarks[k]);
                    }
                }
            }
        }

        private void ClearBookmarksOneByOne()
        {
            if (bookmarkList != null && bookmarkList.SelectedIndex != -1)
            {
                Bookmarks.RemoveAt(bookmarkList.SelectedIndex);
            }
            else if (Bookmarks.Count > 0)
            {
                Bookmarks.RemoveAt(Bookmarks.Count - 1);
            }
            SaveBookmarks();
        }

        private void BookmarkList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bookmark = bookmarkList.SelectedItem as BookmarkInfo;
            ZoomToViewBounds();
        }

        private void AddBookmarkButton_Click(object sender, RoutedEventArgs e)
        {
            if (Map != null)
            {
                string name = null;
                if (addBookmarkName != null)
                {
                    name = addBookmarkName.Text;
                }
                AddBookmark(name, Map.ViewBounds);
                addBookmarkName.Text = "";
            }
        }

        /// <summary>${controls_BookMark_method_addBookMark_D}</summary>
        /// <param name="name">${controls_BookMark_method_addBookMark_param_name}</param>
        /// <param name="viewbounds">${controls_BookMark_method_addBookMark_param_viewBounds}</param>
        public void AddBookmark(string name, Rectangle2D viewbounds)
        {
            name = name.Trim();
            if (string.IsNullOrEmpty(name))
            {
                name = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            }
            BookmarkInfo bookmarkInfo = new BookmarkInfo
            {
                Name = name,
                ViewBounds = viewbounds
            };
            Bookmarks.Add(bookmarkInfo);
            isLoaded = true;

            if (bookmarkList != null && bookmarkList.Columns.Count > 0)
            {
                bookmarkList.ScrollIntoView(bookmarkInfo, bookmarkList.Columns[0]);
            }
            SaveBookmarks();
        }

        /// <summary>${controls_BookMark_method_deleteBookMarkAt_D}</summary>
        /// <param name="index">${controls_BookMark_method_deleteBookMarkAt_param_index}</param>
        public void RemoveAt(int index)
        {
            Bookmarks.RemoveAt(index);
            SaveBookmarks();
        }

        /// <summary>${controls_BookMark_method_clearBookMarks_D}</summary>
        public void Clear()
        {
            Bookmarks.Clear();
            SaveBookmarks();
        }


        //属性
        /// <summary>${controls_BookMark_attribute_useIsolatedStorage_D}</summary>
        public bool EnableIsolatedStorage { get; set; }

        /// <summary>${controls_BookMark_attribute_title_D}</summary>
        public string Title
        {
            get
            {
                return GetValue(TitleProperty) as string;
            }
            set
            {
                SetValue(TitleProperty, value);
            }
        }
        /// <summary>${controls_BookMark_field_titleProperty_D}</summary>
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(Bookmark), null);

        /// <summary>${controls_BookMark_attribute_bookMarks_D}</summary>
        public ObservableCollection<BookmarkInfo> Bookmarks { get; private set; }

        private string Key
        {
            get { return isolatedStorageKey + "_" + this.Name; }
        }

        #region Map属性
        /// <summary>${controls_BookMark_field_MapProperty_D}</summary>
        public static readonly DependencyProperty MapProperty = DependencyProperty.Register("Map", typeof(Map), typeof(Bookmark), new PropertyMetadata(OnMapPropertyChanged));
        private static void OnMapPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Bookmark).Map = e.NewValue as Map;
        }
        /// <summary>${controls_BookMark_attribute_Map_D}</summary>
        public Map Map
        {
            get { return (Map)GetValue(MapProperty); }
            set { SetValue(MapProperty, value); }
        }


        #endregion

        //记录的一些信息
        /// <summary>
        /// 	<para>${controls_BookMark_BookMarkInfo_Title}</para>
        /// 	<para>${controls_BookMark_BookMarkInfo_Description}</para>
        /// </summary>      
        #region 嵌套类
        public class BookmarkInfo
        {
            /// <summary>${controls_BookMark_BookMarkInfo_attribute_name_D}</summary>
            public string Name { get; set; }
            /// <summary>${controls_BookMark_BookMarkInfo_attribute_viewBounds_D}</summary>
            public Rectangle2D ViewBounds { get; set; }
            /// <summary>${controls_BookMark_BookMarkInfo_method_toString_D}</summary>
            /// <returns>${controls_BookMark_BookMarkInfo_method_toString_returns}</returns>
            public override string ToString()
            {
                return Name;
            }
        }
        #endregion

    }
}
