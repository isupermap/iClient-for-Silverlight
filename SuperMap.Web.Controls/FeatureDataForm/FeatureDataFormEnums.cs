﻿namespace SuperMap.Web.Controls
{
    /// <summary>
    /// 	<para>${controls_FeatureDataFormLabelPosition_Title}</para>
    /// </summary>
    public enum FeatureDataFormLabelPosition
    {
        /// <summary>${controls_FeatureDataFormLabelPosition_attribute_Left_D}</summary>
        Left = 0,
        /// <summary>${controls_FeatureDataFormLabelPosition_attribute_Top_D}</summary>
        Top = 1
    }
}