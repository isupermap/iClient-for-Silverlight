﻿using System;
using System.ComponentModel;


namespace SuperMap.Web.Controls
{

    internal interface IKeyValue : INotifyPropertyChanged
    {
        string Key { get; }
        object Value { get; }
    }
    /// <summary>
    /// 	<para>${controls_FeatureDataField_Title}</para>
    /// 	<para>${controls_FeatureDataField_Description}</para>
    /// </summary>
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public sealed class FeatureDataField<T> : IKeyValue
    {
        private FeatureDataForm _featureDataForm;
        private string _fieldName;
        private T _propertyValue;

        internal FeatureDataField(FeatureDataForm featureDataForm, string fieldName, T propertyValue)
        {
            this._featureDataForm = featureDataForm;
            this._fieldName = fieldName;
            this._propertyValue = propertyValue;
        }
        /// <summary>${controls_FeatureDataField_attribute_Key_D}</summary>
        public string Key
        {
            get { return this._fieldName; }
        }
        /// <summary>${controls_FeatureDataField_attribute_Value_D}</summary>
        public T Value
        {
            get { return this._propertyValue; }
            set
            {
                string propertyName = this._fieldName;
                this._propertyValue = value;
                NotifyPropertyChanged(propertyName);
                this._featureDataForm.IsValid = true;
            }
        }
        /// <summary>${controls_FeatureDataField_event_PropertyChanged_D}</summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
       
        object IKeyValue.Value
        {
            get { return this.Value; }
        }
    }
}