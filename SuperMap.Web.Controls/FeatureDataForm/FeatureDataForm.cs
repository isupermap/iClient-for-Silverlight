﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.ComponentModel;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;
using System.Globalization;

namespace SuperMap.Web.Controls
{
    /// <summary>
    /// 	<para>${controls_FeatureDataForm_Title}</para>
    /// 	<para>${controls_FeatureDataForm_Description}</para>
    /// </summary>
    [TemplatePart(Name = "ContentPresenter", Type = typeof(ContentPresenter))]
    [TemplatePart(Name = "CommitButton", Type = typeof(ButtonBase))]
    [StyleTypedProperty(Property = "LabelStyle", StyleTargetType = typeof(ContentControl))]
    [StyleTypedProperty(Property = "TextBoxStyle", StyleTargetType = typeof(TextBox))]
    public sealed class FeatureDataForm : Control, INotifyPropertyChanged
    {
        private const int SIZEOF_SPACING_COLUMN = 8;
        private Thickness PANEL_MARGIN = new Thickness(15, 10, 15, 10);
        private Thickness CONTROLS_MARGIN = new Thickness(0, 2.5, 0, 2.5);
        private ContentPresenter contentPresenter = null;
        private ButtonBase commitButton = null;

        private Dictionary<string, Control> attributeFrameworkElements = null;
        private Dictionary<string, bool> attributeValidationStatus = null;
        private Dictionary<string, IKeyValue> dataFields = null;

        private bool isUpdatedByCommitButton = false;
        private bool isBindingData = false;
        private bool isVerifyingAfterLosingFocus = false;
        private bool isValid;
        private bool hasEdits;
        /// <summary>${controls_FeatureDataForm_constructor_None_D}</summary>
        public FeatureDataForm()
        {
            DefaultStyleKey = typeof(FeatureDataForm);
        }
        /// <summary>${controls_FeatureDataForm_attribute_IsValid_D}</summary>
        public bool IsValid
        {
            get { return isValid; }
            internal set
            {
                if (value != isValid)
                {
                    isValid = value;
                    OnPropertyChanged("IsValid");
                }
            }
        }
        /// <summary>${controls_FeatureDataForm_attribute_HasEdits_D}</summary>
        public bool HasEdits
        {
            get { return hasEdits; }
            internal set
            {
                if (value != hasEdits)
                {
                    hasEdits = value;
                    OnPropertyChanged("HasEdits");
                }
            }
        }

        private static readonly DependencyProperty AssociatedFieldNameProperty =
            DependencyProperty.RegisterAttached("AssociatedFieldName", typeof(string), typeof(FeatureDataForm), null);

        /// <summary>${controls_FeatureDataForm_attribute_FeaturesLayer_D}</summary>
        public FeaturesLayer FeaturesLayer
        {
            get { return (FeaturesLayer)GetValue(FeaturesLayerProperty); }
            set { SetValue(FeaturesLayerProperty, value); }
        }
        /// <summary>${controls_FeatureDataForm_attribute_FeaturesLayerProperty_D}</summary>
        public static readonly DependencyProperty FeaturesLayerProperty =
            DependencyProperty.Register("FeaturesLayer", typeof(FeaturesLayer), typeof(FeatureDataForm), new PropertyMetadata(null, OnFeaturesLayerPropertyChanged));
        
        private static void OnFeaturesLayerPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataForm dataForm = d as FeatureDataForm;
            FeaturesLayer oldValue = e.OldValue as FeaturesLayer;
            FeaturesLayer newValue = e.NewValue as FeaturesLayer;
            if (oldValue != null)
            {
                oldValue.Initialized -= dataForm.FeaturesLayer_Initialized;
            }        
            if (newValue != null)
            {
                if (newValue.IsInitialized)
                {
                    dataForm.GenerateUI(true);
                }
                else
                {
                    newValue.Initialized += dataForm.FeaturesLayer_Initialized;
                }
            }
        }
        /// <summary>${controls_FeatureDataForm_attribute_FeatureSource_D}</summary>
        public Feature FeatureSource
        {
            get { return (Feature)GetValue(FeatureSourceProperty); }
            set { SetValue(FeatureSourceProperty, value); }
        }
        /// <summary>${controls_FeatureDataForm_attribute_FeatureSourceProperty_D}</summary>
        public static readonly DependencyProperty FeatureSourceProperty =
            DependencyProperty.Register("FeatureSource", typeof(Feature), typeof(FeatureDataForm), new PropertyMetadata(null, OnFeatureSourcePropertyChanged));
        private static void OnFeatureSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataForm dataForm = d as FeatureDataForm;
            Feature oldValue = e.OldValue as Feature;
            Feature newValue = e.NewValue as Feature;
            if (oldValue != null)
            {           
                oldValue.AttributeValueChanged -= dataForm.Feature_AttributeValueChanged;
            }
            if (newValue != null)
            {
                newValue.AttributeValueChanged += dataForm.Feature_AttributeValueChanged;
            }

            dataForm.GenerateUI(true);
        }
        /// <summary>${controls_FeatureDataForm_attribute_IsReadOnly_D}</summary>
        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }
        /// <summary>${controls_FeatureDataForm_attribute_IsReadOnlyProperty_D}</summary>
        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(FeatureDataForm), new PropertyMetadata(false, OnIsReadOnlyPropertyChanged));
        private static void OnIsReadOnlyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataForm dataForm = d as FeatureDataForm;
            dataForm.GenerateUI(true);
        }
        /// <summary>${controls_FeatureDataForm_attribute_CommitButtonContent_D}</summary>
        public object CommitButtonContent
        {
            get { return (object)GetValue(CommitButtonContentProperty); }
            set { SetValue(CommitButtonContentProperty, value); }
        }
        /// <summary>${controls_FeatureDataForm_attribute_CommitButtonContentProperty_D}</summary>
        public static readonly DependencyProperty CommitButtonContentProperty =
            DependencyProperty.Register("CommitButtonContent", typeof(object), typeof(FeatureDataForm), new PropertyMetadata("Apply", OnCommitButtonContentPropertyChanged));
        private static void OnCommitButtonContentPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataForm dataForm = d as FeatureDataForm;
            if (dataForm.commitButton != null)
            {
                dataForm.commitButton.Content = e.NewValue;
            }
        }
        /// <summary>${controls_FeatureDataForm_attribute_LabelPosition_D}</summary>
        public FeatureDataFormLabelPosition LabelPosition
        {
            get { return (FeatureDataFormLabelPosition)GetValue(LabelPositionProperty); }
            set { SetValue(LabelPositionProperty, value); }
        }
        /// <summary>${controls_FeatureDataForm_attribute_LabelPositionProperty_D}</summary>
        public static readonly DependencyProperty LabelPositionProperty =
            DependencyProperty.Register("LabelPosition", typeof(FeatureDataFormLabelPosition), typeof(FeatureDataForm), new PropertyMetadata(OnLabelPositionPropertyChanged));
        private static void OnLabelPositionPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataForm dataForm = d as FeatureDataForm;
            dataForm.GenerateUI(false);
        }
        /// <summary>${controls_FeatureDataForm_attribute_CommitButtonStyle_D}</summary>
        public System.Windows.Style CommitButtonStyle
        {
            get { return (System.Windows.Style)GetValue(CommitButtonStyleProperty); }
            set { SetValue(CommitButtonStyleProperty, value); }
        }
        /// <summary>${controls_FeatureDataForm_attribute_CommitButtonStyleProperty_D}</summary>
        public static readonly DependencyProperty CommitButtonStyleProperty =
            DependencyProperty.Register("CommitButtonStyle", typeof(System.Windows.Style), typeof(FeatureDataForm), new PropertyMetadata(OnCommitButtonStylePropertyChanged));
        private static void OnCommitButtonStylePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataForm dataForm = d as FeatureDataForm;
            if (dataForm.commitButton != null)
            {
                dataForm.commitButton.Style = e.NewValue as System.Windows.Style;
            }
        }
        /// <summary>${controls_FeatureDataForm_attribute_LabelStyle_D}</summary>
        public System.Windows.Style LabelStyle
        {
            get { return (System.Windows.Style)GetValue(LabelStyleProperty); }
            set { SetValue(LabelStyleProperty, value); }
        }
        /// <summary>${controls_FeatureDataForm_attribute_LabelStyleProperty_D}</summary>
        public static readonly DependencyProperty LabelStyleProperty =
            DependencyProperty.Register("LabelStyle", typeof(System.Windows.Style), typeof(FeatureDataForm), new PropertyMetadata(OnLabelStylePropertyChanged));
        private static void OnLabelStylePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataForm dataForm = d as FeatureDataForm;
            dataForm.GenerateUI(false);
        }
        /// <summary>${controls_FeatureDataForm_attribute_TextBoxStyle_D}</summary>
        public System.Windows.Style TextBoxStyle
        {
            get { return (System.Windows.Style)GetValue(TextBoxStyleProperty); }
            set { SetValue(TextBoxStyleProperty, value); }
        }
        /// <summary>${controls_FeatureDataForm_attribute_TextBoxStyleProperty_D}</summary>
        public static readonly DependencyProperty TextBoxStyleProperty =
            DependencyProperty.Register("TextBoxStyle", typeof(System.Windows.Style), typeof(FeatureDataForm), new PropertyMetadata(OnTextBoxStylePropertyChanged));
        private static void OnTextBoxStylePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FeatureDataForm dataForm = d as FeatureDataForm;
            dataForm.GenerateUI(false);
        }
        /// <summary>${controls_FeatureDataForm_attribute_OnApplyTemplate_D}</summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (this.contentPresenter != null)
            {
                this.contentPresenter.Content = null;
            }
            this.contentPresenter = GetTemplateChild("ContentPresenter") as ContentPresenter;

            if (this.commitButton != null)
            {
                this.commitButton.Click -= CommitButton_Click;
            }
            this.commitButton = GetTemplateChild("CommitButton") as ButtonBase;
            if (this.commitButton != null)
            {
                SetFrameworkElementBinding(this.commitButton, BindingMode.OneWay, this.CommitButtonContent, ButtonBase.ContentProperty);
                SetFrameworkElementBinding(this.commitButton, BindingMode.OneWay, this.CommitButtonStyle, ButtonBase.StyleProperty);
                this.commitButton.Click += CommitButton_Click;
            }
            this.GenerateUI(true);
        }

        private void SetFrameworkElementBinding(FrameworkElement frameworkElement, BindingMode bindingMode, object bindingSource, DependencyProperty dependencyProperty)
        {
            Binding binding = new Binding();
            binding.Mode = bindingMode;
            binding.Source = bindingSource;
            this.commitButton.SetBinding(dependencyProperty, binding);
        }

        private void FeaturesLayer_Initialized(object sender, EventArgs e)
        {
            GenerateUI(true);
        }

        private bool CheckFeatureParent()
        {
            if (this.FeaturesLayer != null && this.FeatureSource != null)
            {
                return this.FeaturesLayer.Features.Contains(this.FeatureSource);
            }

            return false;
        }

        private void GenerateUI(bool resetInternalVariables)
        {
            if (resetInternalVariables)
            {
                this.isVerifyingAfterLosingFocus = false;
                this.attributeFrameworkElements = null;
                this.isUpdatedByCommitButton = false;
                this.attributeValidationStatus = null;
                this.isBindingData = false;
                IsValid = true;
                HasEdits = false;
                this.dataFields = null;

                if (!this.IsReadOnly && this.commitButton != null)
                {
                    this.commitButton.IsEnabled = false;
                }
            }
            if (this.FeaturesLayer == null || this.FeatureSource == null ||
                (this.FeaturesLayer != null && this.FeatureSource != null && !CheckFeatureParent()))
            {
                if (this.contentPresenter != null)
                {
                    this.contentPresenter.DataContext = null;
                    this.contentPresenter.Content = null;
                }
                UpdateStates(); 
                return;
            }

            AutoGenerateColumns();
            UpdateStates();
        }

        private void UpdateDataForm(string key, object newValue)
        {
            if (this.contentPresenter != null && this.contentPresenter.Content != null)
            {
                foreach (KeyValuePair<string, Control> pair in this.attributeFrameworkElements)
                {
                    if (key == pair.Key)
                    {
                        Control fieldControl = pair.Value;

                        (fieldControl as TextBox).TextChanged -= FrameworkElement_PropertyChanged;
                        (fieldControl as TextBox).SetValue(TextBox.TextProperty, newValue == null ? "" : newValue.ToString());

                        Dispatcher.BeginInvoke((Action)delegate()
                        {
                            (fieldControl as TextBox).TextChanged += FrameworkElement_PropertyChanged;
                        });

                        break;
                    }
                }
                UpdateStates();
            }
        }

        private void Feature_AttributeValueChanged(object sender, DictionaryChangedEventArgs e)
        {
            if (!this.isUpdatedByCommitButton &&
                e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Replace)
            {
                UpdateDataForm(e.Key, e.NewValue);
            }
        }

        private void FeatureDataField_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (this.isVerifyingAfterLosingFocus)
            {
                return;
            }

            IKeyValue newValue = sender as IKeyValue;
            object valueInFeatures = this.FeatureSource.Attributes[newValue.Key];
            object value = newValue.Value;

            bool hasChange = (valueInFeatures == null && value != null) ||
                             (valueInFeatures != null && value == null) ||
                             (valueInFeatures != null && value != null && !valueInFeatures.Equals(value));
            if (hasChange)
            {
                this.FeatureSource.Attributes[newValue.Key] = value;
            }
        }

        private void FrameworkElement_PropertyChanged(object sender, EventArgs e)
        {
            if (this.isBindingData)
            {
                return;
            }

            object associatedField = (sender as FrameworkElement).GetValue(AssociatedFieldNameProperty);

            if (associatedField != null && !string.IsNullOrEmpty(associatedField.ToString()))
            {
                UpdateDictionary(ref this.attributeValidationStatus, associatedField.ToString(), true);
                UpdateStates();
            }
        }
  
        private TextBox GetTextFieldControl()
        {
            TextBox textBox = new TextBox();
            textBox.VerticalAlignment = VerticalAlignment.Center;
            textBox.Margin = CONTROLS_MARGIN;

            if (this.TextBoxStyle != null)
            {
                textBox.Style = this.TextBoxStyle;
            }

            textBox.Loaded += (sender, e) =>
            {
                (sender as TextBox).TextChanged += FrameworkElement_PropertyChanged;
            };
            return textBox;
        }

        private Control GetControlFromType(string fieldName, BindingMode bindingMode)
        {
            object fieldValue = this.FeatureSource.Attributes[fieldName];

            Control control = GetTextFieldControl();//目前是只支持TextBox，假如有Type，则可以支持更多类型的控件，且可以判断输入值的合法性等，后续可以完善。
            control.DataContext = fieldName;

            Binding binding = new Binding("Value");
            binding.Mode = bindingMode;
         // binding.Converter = new FeatureDataFieldValueConverter(fieldValue);
            binding.ConverterParameter = fieldName;
            if (bindingMode == BindingMode.TwoWay)
            {
                binding.UpdateSourceTrigger = UpdateSourceTrigger.Explicit;
                binding.ValidatesOnExceptions = true;
                binding.NotifyOnValidationError = true;
            }

            IKeyValue datafield = null;
            try 
            {
                datafield = new FeatureDataField<string>(this, fieldName, fieldValue != null ? Convert.ToString(fieldValue, CultureInfo.InvariantCulture) : (string)null);
            }
            catch 
            { 
                datafield = new FeatureDataField<string>(this, fieldName, (string)null);
            }

            if (datafield != null)
            {
                if (this.dataFields == null)
                {
                    this.dataFields = new Dictionary<string, IKeyValue>();
                }
                if (this.dataFields.ContainsKey(fieldName))
                {
                    this.dataFields[fieldName].PropertyChanged -= FeatureDataField_PropertyChanged;
                    this.dataFields[fieldName] = datafield;
                }
                else
                {
                    this.dataFields.Add(fieldName, datafield);
                }

                datafield.PropertyChanged += FeatureDataField_PropertyChanged;
                binding.Source = datafield;
            }
            control.SetBinding(TextBox.TextProperty, binding);
            return control;
        }

        private void GenerateField(string fieldName, Grid grid, BindingMode bindingMode)
        {
            int gridRow = grid.RowDefinitions.Count;
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });

            ContentControl fieldLabel = new ContentControl();
            if (this.LabelStyle != null)
            {
                fieldLabel.Style = this.LabelStyle;
            }
            else
            {
                fieldLabel.Content = fieldName;
            }

            fieldLabel.VerticalAlignment = VerticalAlignment.Center;
            if (this.LabelPosition == FeatureDataFormLabelPosition.Top)
            {
                fieldLabel.HorizontalAlignment = HorizontalAlignment.Left;
            }
            else
            {
                fieldLabel.HorizontalAlignment = HorizontalAlignment.Right;
            }

            fieldLabel.DataContext = fieldName;
            fieldLabel.SetValue(Grid.RowProperty, gridRow);
            if (this.LabelPosition == FeatureDataFormLabelPosition.Top)
            {
                fieldLabel.SetValue(Grid.ColumnProperty, 1);
                grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });
                gridRow++;
            }
            else
            {
                fieldLabel.SetValue(Grid.ColumnProperty, 0);
            }
            grid.Children.Add(fieldLabel);

            object typeIdFieldValue = (fieldName != null && this.FeatureSource.Attributes.ContainsKey(fieldName)) ? this.FeatureSource.Attributes[fieldName] : null;
            PopulateFieldControl(fieldName, grid, bindingMode, gridRow);
        }

        private Control PopulateFieldControl(string fieldName, Grid grid, BindingMode bindingMode, int gridRow)
        {
            Control fieldControl = GetControlFromType(fieldName, bindingMode);
            fieldControl.SetValue(AssociatedFieldNameProperty, fieldName);

            if (!this.IsReadOnly)
            {
                fieldControl.LostFocus += FieldControl_LostFocus;
            }
        
            if (fieldControl is TextBox)
            {
                TextBox tb = (fieldControl as TextBox);
                tb.IsReadOnly = this.IsReadOnly;
            }

            fieldControl.VerticalAlignment = VerticalAlignment.Center;
            fieldControl.SetValue(Grid.RowProperty, gridRow);
            if (this.LabelPosition == FeatureDataFormLabelPosition.Left)
            {
                fieldControl.SetValue(Grid.ColumnProperty, 2);
            }
            else
            {
                fieldControl.SetValue(Grid.ColumnProperty, 1);
            }

            if (this.attributeFrameworkElements.ContainsKey(fieldName))
            {
                Control previousControl = this.attributeFrameworkElements[fieldName];
                if (previousControl != null && previousControl.Parent == grid)
                    grid.Children.Remove(previousControl);
                grid.Children.Add(fieldControl);
                this.attributeFrameworkElements[fieldName] = fieldControl;
            }
            else
            {
                grid.Children.Add(fieldControl);
                this.attributeFrameworkElements.Add(fieldName, fieldControl);
            }

            return fieldControl;
        }

        private FrameworkElement GenerateFields()
        {
            if (this.FeaturesLayer == null || this.FeatureSource == null || this.contentPresenter == null ||
                this.FeaturesLayer.Features == null || this.FeatureSource.Attributes == null ||
                !this.FeatureSource.Attributes.GetEnumerator().MoveNext())
            {
                return null;
            }

            BindingMode bindingMode = this.IsReadOnly ? BindingMode.OneWay : BindingMode.TwoWay;
            this.contentPresenter.DataContext = this.FeatureSource;

            Grid grid = new Grid();
            grid.Margin = PANEL_MARGIN;

            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });
            if (this.LabelPosition == FeatureDataFormLabelPosition.Left)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(SIZEOF_SPACING_COLUMN, GridUnitType.Pixel), MinWidth = SIZEOF_SPACING_COLUMN });
            }

            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            this.attributeFrameworkElements = new Dictionary<string, Control>();
            this.isBindingData = true;

            foreach (string fieldName in this.FeatureSource.Attributes.Keys)
            {
                GenerateField(fieldName, grid, bindingMode);
            }

            Dispatcher.BeginInvoke((Action)delegate()
            {
                this.isBindingData = false;
            });

            return grid;
        }
    
        private void AutoGenerateColumns()
        {
            if (this.contentPresenter != null)
            {
                this.contentPresenter.Content = GenerateFields();
            }
        }

        private void UpdateStates()
        {
            if (this.contentPresenter != null && this.contentPresenter.Content != null)
            {
                if (!this.IsReadOnly)
                {
                    bool oldValue = HasEdits;
                    bool newValue = false;
                    if (HasChange())
                    {
                        newValue = true;
                        if (this.commitButton != null)
                        {
                            if (HasInvalidField() || !this.IsValid)
                            {
                                this.commitButton.IsEnabled = false;
                            }
                            else
                            {
                                this.commitButton.IsEnabled = true;
                            }
                        }
                    }
                    else
                    {
                        newValue = false;
                        if (this.commitButton != null)
                        {
                            this.commitButton.IsEnabled = false;
                        }
                    }
                    HasEdits = newValue;

                    if (this.commitButton != null)
                    {
                        this.commitButton.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    IsValid = true;
                    HasEdits = false;

                    if (this.commitButton != null)
                    {
                        this.commitButton.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }

        private void UpdateDictionary(ref Dictionary<string, bool> dictionary, string key, bool value)
        {
            if (dictionary == null)
            {
                dictionary = new Dictionary<string, bool>();
            }
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, value);
            }
            else
            {
                dictionary[key] = value;
            }
        }

        private void RemoveFromDictionary(ref Dictionary<string, bool> dictionary, string key)
        {
            if (dictionary != null && dictionary.ContainsKey(key))
            {
                dictionary.Remove(key);
            }
        }

        private bool HasChange()
        {
            if (this.attributeValidationStatus == null ||
                (this.attributeValidationStatus != null && this.attributeValidationStatus.Count == 0))
            {
                return false;
            }

            return true;
        }
       
        private bool HasInvalidField()
        {
            if (this.attributeValidationStatus == null ||
                (this.attributeValidationStatus != null && this.attributeValidationStatus.Count == 0))
            {
                return false;
            }
            else
            {
                if (this.attributeValidationStatus.ContainsValue(false))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private void CommitButton_Click(object sender, RoutedEventArgs e)
        {
            if (!this.IsReadOnly)
            {
                ApplyChanges();
            }
        }

        private bool ApplyChanges(Control fieldControl, bool shouldUpdateFeatureSource)
        {
            if (this.FeaturesLayer == null || this.FeatureSource == null ||
                (this.FeaturesLayer != null && this.FeatureSource != null && !CheckFeatureParent()))
            {
                return false;
            }

            BindingExpression bindingExpression = null;
            bindingExpression = (fieldControl as TextBox).GetBindingExpression(TextBox.TextProperty);
            bool hasError = false;

            if (bindingExpression != null && bindingExpression.ParentBinding != null)
            {
                string fieldName = bindingExpression.ParentBinding.ConverterParameter.ToString();
                if (FeatureSource.Attributes.ContainsKey(fieldName))
                {
                    object currentValueInFeature = this.FeatureSource.Attributes[fieldName];
                    bool hasChange = HasChange(fieldControl, currentValueInFeature, out hasError);
                    if (hasChange || (this.attributeValidationStatus != null &&
                        this.attributeValidationStatus.ContainsKey(fieldName)))
                    {
                        if (shouldUpdateFeatureSource)
                        {
                            this.isUpdatedByCommitButton = true;
                        }
                        else
                        {
                            this.isVerifyingAfterLosingFocus = true;
                        }
                        try
                        {
                            bindingExpression.UpdateSource();
                            if (!hasChange && this.attributeValidationStatus != null &&
                                this.attributeValidationStatus.ContainsKey(fieldName) &&
                                this.attributeValidationStatus[fieldName])
                            {
                                RemoveFromDictionary(ref this.attributeValidationStatus, fieldName);
                            }
                        }
                        catch
                        {
                            UpdateDictionary(ref this.attributeValidationStatus, fieldName, false);
                            hasError = true;
                        }
                        if (shouldUpdateFeatureSource)
                        {
                            this.isUpdatedByCommitButton = false;
                        }
                        else
                        {
                            this.isVerifyingAfterLosingFocus = false;
                        }

                        if (!hasChange && this.attributeValidationStatus != null &&
                            this.attributeValidationStatus.ContainsKey(fieldName) &&
                            this.attributeValidationStatus[fieldName])
                        {
                            return hasError;
                        }

                        if (!hasError)
                        {
                            if (shouldUpdateFeatureSource)
                            {
                                UpdateDictionary(ref this.attributeValidationStatus, fieldName, true);
                            }
                        }
                        else
                        {
                            UpdateDictionary(ref this.attributeValidationStatus, fieldName, false);
                        }
                    }
                    else
                    {
                        RemoveFromDictionary(ref this.attributeValidationStatus, fieldName);
                    }
                }
            }

            return hasError;
        }

        private bool HasChange(Control control, object valueInFeature, out bool hasError)
        {
            hasError = false;
            try
            {
                string input = (control as TextBox).Text;
                if ((valueInFeature as string) == null && input.Length == 0)
                {
                    return false;
                }
                return valueInFeature as string != input;
            }
            catch
            {
                hasError = true;
            }
            return true;
        }

        private void FieldControl_LostFocus(object sender, RoutedEventArgs e)
        {
            string associatedField = (sender as FrameworkElement).GetValue(AssociatedFieldNameProperty) as string;
            if (!string.IsNullOrEmpty(associatedField))
            {
                this.isBindingData = true;
                RepopulateDomains(sender as TextBox);
                Dispatcher.BeginInvoke((Action)delegate()
                {
                    this.isBindingData = false;
                });
            }

            bool hasError = false;
            foreach (Control control in this.attributeFrameworkElements.Values)
            {
                if (ApplyChanges(control, false))
                {
                    hasError = true;
                }
            }
            if (hasError)
            {
                IsValid = false;
            }

            UpdateStates();
        }

        private void RepopulateDomains(TextBox textBox)
        {
            if (textBox == null)
                return;

            Grid grid = this.contentPresenter.Content as Grid;
            if (grid != null)
            {
                List<string> fields = new List<string>();
                if (this.FeaturesLayer != null && this.FeatureSource != null)
                {
                    foreach(string key in this.FeatureSource.Attributes.Keys)
                    {
                        fields.Add(key);
                    }
                }

                if (fields != null)
                {
                    object typeIdFieldValue = null;
                    try
                    {                 
                        typeIdFieldValue = textBox.Text;
                    }
                    catch { }
                    foreach (string fieldName in fields)
                    {
                        if (this.attributeFrameworkElements.ContainsKey(fieldName))
                        {
                            Control previousFieldControl = this.attributeFrameworkElements[fieldName];
                            object previousFieldControlValue = null;
                            if (previousFieldControl is TextBox)
                            {
                                previousFieldControlValue = (previousFieldControl as TextBox).Text;
                            }

                            if (!this.IsReadOnly)
                            {
                                previousFieldControl.LostFocus -= FieldControl_LostFocus;
                            }

                            BindingMode bindingMode = this.IsReadOnly ? BindingMode.OneWay : BindingMode.TwoWay;
                            int? gridRow = previousFieldControl.GetValue(Grid.RowProperty) as int?;
                         
                            Control newFieldControl = PopulateFieldControl(fieldName, grid, bindingMode, gridRow.Value);

                            if (newFieldControl is TextBox)
                            {
                                (newFieldControl as TextBox).SetValue(TextBox.TextProperty, previousFieldControlValue == null ? "" : previousFieldControlValue.ToString());
                            }

                        }
                    }
                }
            }
        }
        /// <summary>${controls_FeatureDataForm_attribute_ApplyChanges_D}</summary>
        public bool ApplyChanges()
        {
            if (!HasEdits || !IsValid)
            {
                return false;
            }
            if (this.IsReadOnly)
            {
                throw new InvalidOperationException(SuperMap.Web.Controls.Resources.Resource.FeatureDataForm_Cannot_Update);
            }

            foreach (KeyValuePair<string, Control> pair in this.attributeFrameworkElements)
            {
                ApplyChanges(pair.Value, true); 
            }

            bool retVal = HasInvalidField();
            if (retVal)
            {
                IsValid = false;
            }
            else
            {
                this.attributeValidationStatus = null;
            }

            UpdateStates();

            if (IsValid && !HasEdits)
            {
                OnEditEnded(EventArgs.Empty);
            }

            return retVal;
        }
        /// <summary>${controls_FeatureDataForm_attribute_EditEnded_D}</summary>
        public event EventHandler<EventArgs> EditEnded;
        private void OnEditEnded(EventArgs e)
        {
            if (EditEnded != null)
            {
                EditEnded(this, e);
            }
        }
        /// <summary>${controls_FeatureDataForm_attribute_PropertyChanged_D}</summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}