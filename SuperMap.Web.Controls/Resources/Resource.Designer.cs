﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.239
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace SuperMap.Web.Controls.Resources {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SuperMap.Web.Controls.Resources.Resource", typeof(Resource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   使用此强类型资源类，为所有资源查找
        ///   重写当前线程的 CurrentUICulture 属性。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查找类似 全部删除 的本地化字符串。
        /// </summary>
        internal static string Bookmark_Clear {
            get {
                return ResourceManager.GetString("Bookmark_Clear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 删除 的本地化字符串。
        /// </summary>
        internal static string Bookmark_Remove {
            get {
                return ResourceManager.GetString("Bookmark_Remove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 书签 的本地化字符串。
        /// </summary>
        internal static string Bookmark_Title {
            get {
                return ResourceManager.GetString("Bookmark_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 当FeatureDataForm属性IsReadOnly为True时，不能更新 的本地化字符串。
        /// </summary>
        internal static string FeatureDataForm_Cannot_Update {
            get {
                return ResourceManager.GetString("FeatureDataForm_Cannot_Update", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 清除选择 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_Clear {
            get {
                return ResourceManager.GetString("FeatureDataGrid_Clear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 第一条记录 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_FirstRcd {
            get {
                return ResourceManager.GetString("FeatureDataGrid_FirstRcd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 索引必须大于等于0 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_Index_Range_Exception {
            get {
                return ResourceManager.GetString("FeatureDataGrid_Index_Range_Exception", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 最后一条记录 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_LastRcd {
            get {
                return ResourceManager.GetString("FeatureDataGrid_LastRcd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 下一条记录 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_NextRcd {
            get {
                return ResourceManager.GetString("FeatureDataGrid_NextRcd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 集合中无元素 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_No_Item_Exception {
            get {
                return ResourceManager.GetString("FeatureDataGrid_No_Item_Exception", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 不能更新 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_No_Update_Exception {
            get {
                return ResourceManager.GetString("FeatureDataGrid_No_Update_Exception", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似  选项... 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_Opt {
            get {
                return ResourceManager.GetString("FeatureDataGrid_Opt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 上一条记录 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_PreviousRcd {
            get {
                return ResourceManager.GetString("FeatureDataGrid_PreviousRcd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 记录： 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_Rcd {
            get {
                return ResourceManager.GetString("FeatureDataGrid_Rcd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 共选中{1}条记录中的{0}条 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_RecordMsg {
            get {
                return ResourceManager.GetString("FeatureDataGrid_RecordMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 删除所选项 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_RemoveSlt {
            get {
                return ResourceManager.GetString("FeatureDataGrid_RemoveSlt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 更新要素失败 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_Row_Editing_Exception {
            get {
                return ResourceManager.GetString("FeatureDataGrid_Row_Editing_Exception", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 选中全部 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_SltAll {
            get {
                return ResourceManager.GetString("FeatureDataGrid_SltAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 反向选择 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_SwitchSlt {
            get {
                return ResourceManager.GetString("FeatureDataGrid_SwitchSlt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 自动漫游到所选项 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_ZoomFree {
            get {
                return ResourceManager.GetString("FeatureDataGrid_ZoomFree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 缩放到所选项 的本地化字符串。
        /// </summary>
        internal static string FeatureDataGrid_ZoomToSlt {
            get {
                return ResourceManager.GetString("FeatureDataGrid_ZoomToSlt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 共： 的本地化字符串。
        /// </summary>
        internal static string FeatureDataPager_Total {
            get {
                return ResourceManager.GetString("FeatureDataPager_Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 map 参数不能为空 的本地化字符串。
        /// </summary>
        internal static string InfoWindow_MapParam {
            get {
                return ResourceManager.GetString("InfoWindow_MapParam", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 缩放到后一视图 的本地化字符串。
        /// </summary>
        internal static string MapHistoryManager_NextView {
            get {
                return ResourceManager.GetString("MapHistoryManager_NextView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 缩放到前一视图 的本地化字符串。
        /// </summary>
        internal static string MapHistoryManager_PreView {
            get {
                return ResourceManager.GetString("MapHistoryManager_PreView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 英尺 的本地化字符串。
        /// </summary>
        internal static string ScaleBar_Foot {
            get {
                return ResourceManager.GetString("ScaleBar_Foot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 公里 的本地化字符串。
        /// </summary>
        internal static string ScaleBar_Kilometer {
            get {
                return ResourceManager.GetString("ScaleBar_Kilometer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 米 的本地化字符串。
        /// </summary>
        internal static string ScaleBar_Meter {
            get {
                return ResourceManager.GetString("ScaleBar_Meter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 英里 的本地化字符串。
        /// </summary>
        internal static string ScaleBar_Mile {
            get {
                return ResourceManager.GetString("ScaleBar_Mile", resourceCulture);
            }
        }
    }
}
