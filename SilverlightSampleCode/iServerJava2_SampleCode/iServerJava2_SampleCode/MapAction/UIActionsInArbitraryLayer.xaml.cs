﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
namespace iServerJava2_SampleCode
{
    public partial class UIActionsInArbitraryLayer : UserControl
    {
        private ElementsLayer elementsLayer;

        public UIActionsInArbitraryLayer()
        {
            InitializeComponent();
            elementsLayer = MyMap.Layers["MyArbitraryLayer"] as ElementsLayer;
        }

        //绘制点
        private void btn_Point_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint point = new DrawPoint(this.MyMap);
            this.MyMap.Action = point;
            point.DrawCompleted += new EventHandler<DrawEventArgs>(point_DrawCompleted);
        }

        //绘制点结束后将所绘制的点转化为图钉并加载到ElementsLayer中，在加载前先设置点要素样式
        void point_DrawCompleted(object sender, DrawEventArgs e)
        {
            this.elementsLayer.AddChild(e.Element as Pushpin);
        }

        //绘制线
        private void btn_Line_Click(object sender, RoutedEventArgs e)
        {
            DrawLine line = new DrawLine(this.MyMap);
            this.MyMap.Action = line;
            line.DrawCompleted += new EventHandler<DrawEventArgs>(line_DrawCompleted);
        }

        //绘制线结束后将所绘制的线转化为线元素并加载到ElementsLayer中，在加载前先设置线元素样式
        void line_DrawCompleted(object sender, DrawEventArgs e)
        {
            //设置线元素样式
            PolylineElement polyline = e.Element as PolylineElement;
            polyline.Stroke = new SolidColorBrush(Colors.Purple);
            polyline.StrokeThickness = 2;
            polyline.Opacity = 0.25;

            //将线元素加载到ElementsLayer中
            this.elementsLayer.Children.Add(polyline);
        }

        //绘制多边形
        private void btn_Region_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon region = new DrawPolygon(this.MyMap);
            this.MyMap.Action = region;
            region.DrawCompleted += new EventHandler<DrawEventArgs>(region_DrawCompleted);
        }

        //绘制多边形结束后将所绘制的多边形转化为面元素并加载到ElementsLayer中，在加载前先设置该元素样式
        void region_DrawCompleted(object sender, DrawEventArgs e)
        {
            PolygonElement polyRegion = e.Element as PolygonElement;

            //设置面元素样式
            polyRegion.Fill = new SolidColorBrush(Colors.Green);
            polyRegion.FillRule = FillRule.Nonzero;
            polyRegion.Opacity = 0.85;
            polyRegion.Stroke = new SolidColorBrush(Colors.Orange);
            polyRegion.StrokeThickness = 1;

            //将面元素加载到ElementsLayer中
            this.elementsLayer.Children.Add(polyRegion);
        }

        //绘制自由线
        private void btn_FreeHand_Click(object sender, RoutedEventArgs e)
        {
            DrawFreeLine freehand = new DrawFreeLine(this.MyMap);
            this.MyMap.Action = freehand;
            freehand.DrawCompleted += new EventHandler<DrawEventArgs>(freehand_DrawCompleted);
        }

        //绘制自由线结束后将所绘制的自由线转化为线元素并加载到ElementsLayer中，在加载前先设置该元素样式
        void freehand_DrawCompleted(object sender, DrawEventArgs e)
        {
            PolylineElement freeline = e.Element as PolylineElement;

            //设置线元素样式
            freeline.Stroke = new SolidColorBrush(Colors.Purple);
            freeline.StrokeThickness = 3;
            freeline.Opacity = 0.75;

            //将线元素加载到ElementsLayer中
            this.elementsLayer.Children.Add(freeline);
        }

        //绘制圆
        private void btn_Circle_Click(object sender, RoutedEventArgs e)
        {
            DrawCircle circle = new DrawCircle(this.MyMap);
            this.MyMap.Action = circle;
            circle.DrawCompleted += new EventHandler<DrawEventArgs>(circle_DrawCompleted);
        }

        //绘制圆结束后将所绘制的圆转化为椭圆（silverlight的元素）并加载到ElementsLayer中，在加载前先设置该元素样式
        void circle_DrawCompleted(object sender, DrawEventArgs e)
        {
            Ellipse ellipse = e.Element as Ellipse;

            //设置椭圆样式
            ellipse.Fill = new SolidColorBrush(Colors.Green);
            ellipse.Opacity = 0.75;
            ellipse.Stroke = new SolidColorBrush(Colors.Brown);
            ellipse.StrokeThickness = 1;

            //将椭圆加载到ElementsLayer中
            this.elementsLayer.Children.Add(ellipse);
        }

        //绘制矩形
        private void btn_Rectangle_Click(object sender, RoutedEventArgs e)
        {
            DrawRectangle rectangle = new DrawRectangle(this.MyMap);
            this.MyMap.Action = rectangle;
            rectangle.DrawCompleted += new EventHandler<DrawEventArgs>(rectangle_DrawCompleted);
        }

        //绘制矩形结束后将所绘制的矩形转化为面元素并加载到ElementsLayer中，在加载前先设置该元素样式
        void rectangle_DrawCompleted(object sender, DrawEventArgs e)
        {
            Rectangle rectangle = e.Element as Rectangle;

            //设置面元素样式
            rectangle.Stroke = new SolidColorBrush(Colors.Blue);
            rectangle.StrokeThickness = 1;
            rectangle.Fill = new SolidColorBrush(Colors.Orange);
            rectangle.Opacity = 0.75;

            //将矩形加载到ElementsLayer中
            this.elementsLayer.Children.Add(rectangle);
        }
        
        //绘制自由面
        private void btn_FreeRegion_Click(object sender, RoutedEventArgs e)
        {
            DrawFreePolygon polygon = new DrawFreePolygon(this.MyMap);
            this.MyMap.Action = polygon;
            polygon.DrawCompleted += new EventHandler<DrawEventArgs>(polygon_DrawCompleted);
        }

        //绘制自由面结束后将所绘制的自由面转化为面元素并加载到ElementsLayer中
        void polygon_DrawCompleted(object sender, DrawEventArgs e)
        {
            this.elementsLayer.AddChild(e.Element as PolygonElement);
        }

        //平移
        private void btn_Pan_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap.Action = new Pan(this.MyMap,Cursors.Hand);
        }

        //清除ElementsLayer中全部元素
        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            this.elementsLayer.Children.Clear();
        }

        //拉框缩小
        private void btn_ZoomOut_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap.Action = new ZoomOut(this.MyMap);
        }

        //拉框放大，并设置拉框的样式
        private void btn_ZoomIn_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap.Action = new ZoomIn(this.MyMap) { Fill = new SolidColorBrush(Colors.Red) };
        }
    }
}
