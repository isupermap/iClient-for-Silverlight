﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iServerJava2_SampleCode
{
    public partial class DataBinding : UserControl
    {
        private Random random = new Random();
        private const double ChinaLeft = 70;
        private const double ChinaRight = 130;
        private const double ChinaBottom = 4;
        private const double ChinaTop = 50;

        public DataBinding()
        {
            InitializeComponent();
            this.LayoutRoot.Loaded += new RoutedEventHandler(LayoutRoot_Loaded);
        }

        //设置椭圆的颜色
        private Brush GetRandomColor()
        {
            byte r = (byte)random.Next(255);
            byte g = (byte)random.Next(255);
            byte b = (byte)random.Next(255);
            return new SolidColorBrush(Color.FromArgb(255, r, g, b));
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            FeaturesLayer featureslayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            MarkerStyle style = this.LayoutRoot.Resources["customStyle"] as MarkerStyle;

            //将feature的ID、Fill和Size属性绑定到随机生成50个椭圆
            for (int i = 0; i < 50; i++)
            {
                double x = random.NextDouble() * (ChinaRight - ChinaLeft) + ChinaLeft;
                double y = random.NextDouble() * (ChinaTop - ChinaBottom) + ChinaBottom;
                Feature feature = new Feature() { Geometry = new GeoPoint(x, y), Style = style };
                feature.Attributes.Add("ID", i);
                feature.Attributes.Add("Size", random.NextDouble() * 20);
                feature.Attributes.Add("Fill", GetRandomColor());
                featureslayer.Features.Add(feature);
            }
        }
    }
}
