﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;

namespace iServerJava2_SampleCode
{
    public partial class RendererTest : UserControl
    {
        private Random random = new Random();

        public RendererTest()
        {
            InitializeComponent();
            MyMap.Loaded += new RoutedEventHandler(MyMapControl_Loaded);
        }

        private void MyMapControl_Loaded(object sender, RoutedEventArgs e)
        {
            FeaturesLayer layer = MyMap.Layers["MyFeaturesLayer1"] as FeaturesLayer;
            for (int i = 0; i < 100; i++)
            {
                double x = random.Next(2000, 7600);
                double y = random.Next(-7600, -2000);

                Feature f = new Feature
                {
                    Geometry = new GeoPoint(x, y),
                };
                f.Attributes.Add("Ranking", random.NextDouble());
                f.ToolTip = new TextBlock
                {
                    Text = "Ranking:" + f.Attributes["Ranking"].ToString(),
                    Foreground = new SolidColorBrush(Colors.Red)
                };
                layer.Features.Add(f);
            }

            List<QueryLayerParam> queryLayerParams = new List<QueryLayerParam>() 
            { 
                new QueryLayerParam
                { 
                    Name = "WaterPoly@changchun" 
                } 
            };

            QueryBySqlParameters parameters = new QueryBySqlParameters
            {
                MapName = "Changchun",
                QueryParam = new QueryParam { QueryLayerParams = queryLayerParams },
            };
            QueryBySqlService query = new QueryBySqlService("http://localhost:7080/demo/");
            query.ProcessAsync(parameters);
            query.Failed += (s, args) => { MessageBox.Show(args.Error.ToString()); };
            query.ProcessCompleted += new EventHandler<QueryEventArgs>(query_ProcessCompleted);
        }

        private void query_ProcessCompleted(object sender, QueryEventArgs e)
        {
            if (e.ResultSet == null)
            {
                MessageBox.Show("没结果!");
                return;
            }
            FeaturesLayer featuresLayer = MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            foreach (RecordSet item in e.ResultSet.RecordSets)
            {
                foreach (Feature feature in item.ToFeatureSet())
                {
                    featuresLayer.Features.Add(feature);
                }
            }
        }
    }
}
