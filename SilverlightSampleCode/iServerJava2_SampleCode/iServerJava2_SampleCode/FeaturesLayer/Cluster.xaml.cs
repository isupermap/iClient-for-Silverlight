﻿using System;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iServerJava2_SampleCode
{
    public partial class Cluster : UserControl
    {
        private const double ChinaLeft = 70;
        private const double ChinaRight = 130;
        private const double ChinaBottom = 4;
        private const double ChinaTop = 50;
        private Random random = new Random();
        private FeaturesLayer featuresLayer;

        public Cluster()
        {
            InitializeComponent();
            this.MyMap.ViewBoundsChanged += new EventHandler<SuperMap.Web.Mapping.ViewBoundsEventArgs>(Map_ViewBoundsChanged);
        }

        //根据地图缩放，实现点的聚类功能
        private void Map_ViewBoundsChanged(object sender, SuperMap.Web.Mapping.ViewBoundsEventArgs e)
        {
            if (e.OldViewBounds.IsEmpty)
            {
                featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
                featuresLayer.Clusterer = myFlareClusterer;

                //随机生成点
                for (int i = 0; i < 500; i++)
                {
                    double x = random.NextDouble() * (ChinaRight - ChinaLeft) + ChinaLeft;
                    double y = random.NextDouble() * (ChinaTop - ChinaBottom) + ChinaBottom;
                    Feature feature = new Feature();
                    feature.Geometry = new GeoPoint(x, y);
                    feature.Style = MediumMarkerStyle;
                    feature.ToolTip = new TextBlock() { Text = i.ToString() };
                    featuresLayer.Features.Add(feature);
                }
            }
        }

        //取消聚类功能
        private void myCheck_Checked(object sender, RoutedEventArgs e)
        {
            featuresLayer.Clusterer = null;
        }

        private void myCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            featuresLayer.Clusterer = myFlareClusterer;
        }
    }
}

