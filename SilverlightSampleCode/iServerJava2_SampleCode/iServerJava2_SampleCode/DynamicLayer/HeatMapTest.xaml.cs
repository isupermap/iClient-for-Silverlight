﻿using System;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Mapping;

namespace iServerJava2_SampleCode
{
    public partial class HeatMapTest : UserControl
    {
        HeatMapLayer layer;
        public HeatMapTest()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(HeatMapTest_Loaded);
        }

        //加载热点图
        private void HeatMapTest_Loaded(object sender, RoutedEventArgs e)
        {
            layer = MyMap.Layers["heatMap"] as HeatMapLayer;
            Random rand = new Random();
            for (int i = 0; i < 100; i++)
            {
                double x = rand.NextDouble() * 360 - 180;
                double y = rand.NextDouble() * 180 - 90;
                HeatPoint heatPoint = new HeatPoint();
                heatPoint.X = x;
                heatPoint.Y = y;
                heatPoint.Value = 10;
                layer.HeatPoints.Add(heatPoint);
            }
        }

        //半径选择滑动条触发事件
        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (MyMap == null) return;
            layer = MyMap.Layers["heatMap"] as HeatMapLayer;
            layer.Radius = (int)e.NewValue;
        }

        //强度选择滑动条触发事件
        private void Slider2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (MyMap == null) return;
            layer = MyMap.Layers["heatMap"] as HeatMapLayer;
            layer.Intensity = (double)e.NewValue;
        }
    }
}
