﻿using System.Windows.Controls;
using SuperMap.Web.Mapping;

namespace iServerJava2_SampleCode
{
    public partial class IServerAndWMS : UserControl
    {
        private DynamicWMSLayer myWMSLayer;

        public IServerAndWMS()
        {
            InitializeComponent();
        }

        private void Slider_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            if (MyMap != null)
            {
                myWMSLayer.Opacity = layerOpacity.Value;
            }
        }

        private void MyMap_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            myWMSLayer = MyMap.Layers["MyWMSLayer"] as DynamicWMSLayer;
            myWMSLayer.Opacity = layerOpacity.Value;
        }
    }
}
