﻿using System.Windows.Controls;

namespace iServerJava2_SampleCode
{
    public partial class Magnifier : UserControl
    {
        public Magnifier()
        {
            InitializeComponent();
            MyMap.Loaded += new System.Windows.RoutedEventHandler(MyMap_Loaded);
        }

        private void MyMap_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            double t;
            if (!double.TryParse(myFactor.Text, out t))
            {
                t = 5;
            }
            MyMagnifier.ZoomFactor = t;
            myFactor.TextChanged += new TextChangedEventHandler(myFactor_TextChanged);
        }

        private void myFactor_TextChanged(object sender, TextChangedEventArgs e)
        {
            double t;
            if (!double.TryParse(myFactor.Text, out t))
            {
                t = 5;
            }
            MyMagnifier.ZoomFactor = t;
        }
    }
}
