﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace iServerJava2_SampleCode
{
    public partial class MainPage : UserControl
    {
        private List<Category> _categoryList;
        private UserControl _control;
        private CategoryItem _item;
        private string _target;
        private ListBoxItem _targetListBoxItem;
        private string _xmlFile;


        public MainPage()
        {
            this._control = null;
            this.InitializeComponent();
            this.myStory.Begin();
        }

        public MainPage(string xmlFile)
            : this()
        {
            string url = HtmlPage.Document.DocumentUri.AbsoluteUri;
            int i = url.LastIndexOf('/');
            this._xmlFile = url.Substring(0, i) + "/" + xmlFile;
        }
        private void client_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                XDocument document = null;
                using (Stream stream = e.Result)
                {
                    document = XDocument.Load(stream);
                }
                this._categoryList = document.Root.Elements("Category").Select<XElement, Category>(delegate(XElement f)
                {
                    return new Category { Name = (string)f.Element("name"), CategoryItems = f.Elements("items").Elements<XElement>("item").Select<XElement, CategoryItem>(((Func<XElement, CategoryItem>)(o => new CategoryItem { ID = (string)o.Element("id"), XAML = (string)o.Element("xaml"), Source = (string)o.Element("source"), Code = (string)o.Element("code"), Explain = (string)o.Element("explain") }))).ToArray<CategoryItem>() };
                }).ToList<Category>();
                this.CreateList();
                this.ProcessTarget();
            }
        }

        public void copyToClipboard_Click(object sender, RoutedEventArgs e)
        {
            string text = string.Empty;
            switch (this.tabPanel.SelectedIndex)
            {
                case 1:
                    if (this.txtXaml != null)
                    {
                        if (string.IsNullOrEmpty(this.txtXaml.SelectedText))
                        {
                            //this.txtXaml.SelectAll();
                            text = this.txtXaml.SourceCode;
                        }
                        else
                        {
                            text = this.txtXaml.SelectedText;
                        }
                        break;
                    }
                    return;

                case 2:
                    if (this.txtSrc == null)
                    {
                        return;
                    }
                    if (string.IsNullOrEmpty(this.txtSrc.SelectedText))
                    {
                        //this.txtSrc.SelectAll();
                        text = this.txtSrc.SourceCode;
                    }
                    else
                    {
                        text = this.txtSrc.SelectedText;
                    }
                    break;

                case 3:
                    if (this.txtSrcExplain == null)
                    {
                        return;
                    }
                    if (string.IsNullOrEmpty(this.txtSrcExplain.SelectedText))
                    {
                        this.txtSrcExplain.SelectAll();
                    }
                    text = this.txtSrcExplain.SelectedText;
                    break;
            }
            Clipboard.SetText(text);
        }

        private void CreateList()
        {
            int count = this.ListOfSamples.Children.Count;
            foreach (Category category in this._categoryList)
            {
                Grid grid = new Grid();
                grid.Margin = new Thickness(0.0, 5.0, 0.0, 0.0);
                Rectangle rectangle = new Rectangle();
                rectangle.Fill = this.ExpanderGradient;
                rectangle.Stroke = new SolidColorBrush(Color.FromArgb(0x55, 0x00, 0x00, 0x00));
                rectangle.Margin = new Thickness(0.0);
                rectangle.RadiusX = 5.0;
                rectangle.RadiusY = 5.0;
                grid.Children.Add(rectangle);
                Expander expander = new Expander();
                expander.IsExpanded = false;
                expander.Name = string.Format("Category_{0}", count);
                expander.Foreground = new SolidColorBrush(Color.FromArgb(0xFE, 0x00, 0x00, 0x00));
                expander.Background = new SolidColorBrush(Colors.Transparent);
                expander.FontWeight = FontWeights.Bold;
                expander.FontSize = 11.0;
                expander.Header = category.Name;
                expander.Tag = count;
                expander.Cursor = Cursors.Hand;
                expander.Margin = new Thickness(4.0);
                expander.Expanded += new RoutedEventHandler(this.exp_Expanded);

                ListBox box = new ListBox();
                box.Name = string.Format("List_{0}", count);
                box.Background = new SolidColorBrush(Colors.Transparent);
                box.BorderBrush = new SolidColorBrush(Colors.Transparent);
                box.Foreground = new SolidColorBrush(Colors.Black);
                box.Margin = new Thickness(5.0, 0.0, 5.0, 5.0);
                box.FontWeight = FontWeights.Normal;
                box.ItemContainerStyle = this.SDKListBoxtItemStyle;
                int num2 = 0;
                foreach (CategoryItem item in category.CategoryItems)
                {
                    StackPanel panel = new StackPanel();
                    panel.Orientation = Orientation.Horizontal;
                    TextBlock block = new TextBlock();
                    block.FontSize = 11.0;
                    block.Text = item.ID;
                    panel.Children.Add(block);
                    ListBoxItem item3 = new ListBoxItem();
                    item3.Content = panel;
                    item3.Background = new SolidColorBrush(Colors.Transparent);
                    item3.BorderBrush = new SolidColorBrush(Colors.Transparent);
                    item3.Name = string.Format("Item_{0}_{1}", count, num2);
                    item3.Tag = item;
                    item3.Cursor = Cursors.Hand;
                    ListBoxItem item2 = item3;
                    box.Items.Add(item2);
                    num2++;
                }
                box.SelectionChanged += new SelectionChangedEventHandler(this.lb_SelectionChanged);
                expander.Content = box;
                count++;
                grid.Children.Add(expander);
                this.ListOfSamples.Children.Add(grid);
            }
        }

        private void exp_Expanded(object sender, RoutedEventArgs e)
        {
            Expander expander = sender as Expander;
            int count = this.ListOfSamples.Children.Count;
            for (int i = 0; i < count; i++)
            {

                Expander expander2 = base.FindName(string.Format("Category_{0}", i)) as Expander;
                if (expander2.Name != expander.Name)
                {
                    expander2.IsExpanded = false;
                }
            }
            int num3 = Convert.ToInt32(expander.Tag);
            string name = string.Format("List_{0}", num3);
            ListBox box = base.FindName(name) as ListBox;
            if (box != null)
            {
                box.SelectedIndex = -1;
            }
        }

        private void lb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this._targetListBoxItem != null)
            {
                this._targetListBoxItem.IsSelected = false;
                this._targetListBoxItem = null;
            }
            if ((e.AddedItems != null) && (e.AddedItems.Count > 0))
            {
                ListBoxItem item = e.AddedItems[0] as ListBoxItem;
                if (item != null)
                {
                    CategoryItem tag = item.Tag as CategoryItem;
                    this._item = tag;
                    this.processitem(tag);
                }
            }
        }

        private void pline_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Polygon polygon = sender as Polygon;
            string str = Convert.ToString(polygon.Tag);
            string name = this._categoryList[Convert.ToInt32(str)].Name;
        }

        private void processitem(CategoryItem item)
        {
            this._control = null;
            this.tabSample.Content = null;
            //this.txtSrc.Text = "";
            //this.txtXaml.Text = "";
            this.txtSrc.SourceCode = "";
            this.txtXaml.SourceCode = "";
            this.txtSrcExplain.Text = "";
            this.tabPanel.SelectedIndex = 0;
            Type type = Type.GetType(item.XAML);
            if (type != null)
            {
                this._control = Activator.CreateInstance(type) as UserControl;
                this.tabSample.Content = this._control;

            }
            this.SampleCaption.Text = item.ID;
            HtmlPage.Window.Invoke("setParentHash", new object[] { item.XAML.Split(new char[] { '.' })[1] });
        }

        public void ProcessTarget()
        {
            if (!string.IsNullOrEmpty(this._target))
            {
                string str = "iServerJava2_SampleCode." + this._target;
                int num = 0;
                foreach (Category category in this._categoryList)
                {
                    int num2 = 0;
                    foreach (CategoryItem item in category.CategoryItems)
                    {
                        if (item.XAML.ToLower() == str.ToLower())
                        {
                            this._item = item;
                            this.processitem(item);
                            Expander expander = base.FindName(string.Format("Category_{0}", num)) as Expander;
                            expander.IsExpanded = true;
                            this._targetListBoxItem = base.FindName(string.Format("Item_{0}_{1}", num, num2)) as ListBoxItem;
                            this._targetListBoxItem.IsSelected = true;
                            return;
                        }
                        num2++;
                    }
                    num++;
                }
            }
        }

        [ScriptableMember]
        public void SetTargetSample(string hash)
        {
            string str = hash.Trim().TrimStart(new char[] { '#' });
            if ((str.Length > 0) && (str.Length < 30))
            {
                this._target = str;
            }
        }

        private void sideBar_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double num = this.sideBar.ActualHeight - 10.0;
            double num2 = this.sideBar.ActualWidth - 10.0;
        }

        private void sourceView_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                string str = null;
                using (Stream stream = e.Result)
                {
                    str = new StreamReader(stream).ReadToEnd();
                }
                switch (this.tabPanel.SelectedIndex)
                {
                    case 1:
                        //this.txtXaml.Text = str;
                        this.txtXaml.SourceCode = str;

                        break;

                    case 2:
                        //this.txtSrc.Text = str;
                        this.txtSrc.SourceCode = str;
                        break;

                    case 3:
                        this.txtSrcExplain.Text = str;
                        break;
                }
            }
        }

        private void sourceViewer(string srcFile)
        {
            string url = HtmlPage.Document.DocumentUri.AbsoluteUri;
            int i = url.LastIndexOf('/');
            string ur = url.Substring(0, i) + "/" + srcFile;

            //string url = string.Concat("http://", HtmlPage.Document.DocumentUri.Host, ":", HtmlPage.Document.DocumentUri.Port, "/", srcFile);

            WebClient client = new WebClient();
            client.OpenReadCompleted += new OpenReadCompletedEventHandler(this.sourceView_OpenReadCompleted);
            client.OpenReadAsync(new Uri(ur, UriKind.RelativeOrAbsolute));
        }

        private void tabPanel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((this.tabPanel != null) && (this._item != null))
            {
                switch (this.tabPanel.SelectedIndex)
                {
                    case 1:
                        this.sourceViewer(this._item.Source);
                        this.copyToClipboard.Visibility = Visibility.Visible;
                        return;

                    case 2:
                        this.sourceViewer(this._item.Code);
                        this.copyToClipboard.Visibility = Visibility.Visible;
                        return;

                    case 3:
                        this.sourceViewer(this._item.Explain);
                        this.copyToClipboard.Visibility = Visibility.Visible;
                        return;
                }
                this.copyToClipboard.Visibility = Visibility.Collapsed;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            client.OpenReadCompleted += new OpenReadCompletedEventHandler(this.client_OpenReadCompleted);
            Uri uri = new Uri(this._xmlFile, UriKind.RelativeOrAbsolute);
            base.Dispatcher.BeginInvoke(delegate
            {
                client.OpenReadAsync(uri);
            });
        }
    }
}

