﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iServerJava2_SampleCode
{
    public partial class Code : UserControl
    {
        private ElementsLayer arbitrarylayer;

        public Code()
        {
            InitializeComponent();
            arbitrarylayer = this.MyMap.Layers["MyArbitraryLayer"] as ElementsLayer;
            MyMap.ViewBoundsChanged += new System.EventHandler<ViewBoundsEventArgs>(MyMap_ViewBoundsChanged);
        }

        private void MyMap_ViewBoundsChanged(object sender, ViewBoundsEventArgs e)
        {
            #region Ellipse
            Ellipse myellipse = new Ellipse();
            myellipse.Fill = new SolidColorBrush(Colors.Red);
            Rectangle2D ellbounds = new Rectangle2D(119.5, 21, 122.5, 26);
            this.arbitrarylayer.AddChild(myellipse, ellbounds);
            MyEllipseStory.Stop();

            Storyboard.SetTarget(myDoubleAnimation, myellipse);
            MyEllipseStory.Begin();

            #endregion

            #region Pushpin
            Pushpin beijing = new Pushpin();
            beijing.Location = new Point2D(116.2, 39.6);
            this.arbitrarylayer.AddChild(beijing);

            Pushpin ulanBator = new Pushpin();
            ulanBator.Location = new Point2D(106.5, 47.6);
            ulanBator.Background = new SolidColorBrush(Colors.Blue);
            this.arbitrarylayer.AddChild(ulanBator);

            Pushpin moscow = new Pushpin();
            moscow.Location = new Point2D(37.4, 55.5);
            moscow.Background = new SolidColorBrush(Colors.Yellow);
            this.arbitrarylayer.AddChild(moscow);


            Pushpin prague = new Pushpin();
            prague.Location = new Point2D(14.3, 50.1);
            prague.Background = new SolidColorBrush(Colors.Orange);
            this.arbitrarylayer.AddChild(prague);

            Pushpin berlin = new Pushpin();
            berlin.Location = new Point2D(13.3, 52.3);
            berlin.Background = new SolidColorBrush(Colors.Purple);
            this.arbitrarylayer.AddChild(berlin);
            #endregion

            #region PolygonElement
            PolygonElement line = new PolygonElement();
            line.Stroke = new SolidColorBrush(Colors.Yellow);
            line.StrokeThickness = 3;
            line.Opacity = 1;
            line.StrokeDashArray = new DoubleCollection { 3, 3 };
            line.StrokeEndLineCap = PenLineCap.Triangle;
            Point2DCollection points = new Point2DCollection();
            points.Add(new Point2D(121.3, 25));
            points.Add(new Point2D(116.2, 39.6));
            points.Add(new Point2D(106.5, 47.6));
            points.Add(new Point2D(37.4, 55.5));
            points.Add(new Point2D(14.3, 50.1));
            points.Add(new Point2D(13.3, 52.3));
            points.Add(new Point2D(0.1, 51.3));
            line.Point2Ds = points;
            this.arbitrarylayer.AddChild(line);
            #endregion

            #region PolygonElement
            PolygonElement polygon = new PolygonElement();
            polygon.Stroke = new SolidColorBrush(Colors.Purple);
            polygon.Opacity = 0.5;
            polygon.Fill = this.British;
            Point2DCollection gonpoints = new Point2DCollection();
            gonpoints.Add(new Point2D(-8, 61));
            gonpoints.Add(new Point2D(-6, 55));
            gonpoints.Add(new Point2D(-8, 50));
            gonpoints.Add(new Point2D(2, 50));
            gonpoints.Add(new Point2D(1, 61));
            polygon.Point2Ds = gonpoints;
            this.arbitrarylayer.AddChild(polygon);
            #endregion

            #region Button
            Button btn = new Button();
            btn.Height = 32;
            btn.Width = 32;
            btn.Content = "点击";
            btn.Click += new RoutedEventHandler(btn_Click);
            Rectangle2D btnbounds = new Rectangle2D(95, 29, 115, 38);
            btn.SetValue(ElementsLayer.BBoxProperty, btnbounds);
            this.arbitrarylayer.AddChild(btn);
            #endregion
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("欢迎来到中国！");
        }
    }
}