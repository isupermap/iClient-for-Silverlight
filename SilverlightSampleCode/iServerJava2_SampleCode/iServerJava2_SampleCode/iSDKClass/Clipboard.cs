﻿using System.Windows.Browser;

namespace iServerJava2_SampleCode
{
    public class Clipboard
    {
        private const string BeforeFlashCopy = "即将复制该内容...";
        private const string ClipboardFailure = "该内容不能复制到剪贴板中。";
        private const string HostNoClipboard = "当前主机的剪贴板不可用。";

        public static void SetText(string text)
        {
            ScriptObject property = (ScriptObject)HtmlPage.Window.GetProperty("clipboardData");
            if (property != null)
            {
                if (!((bool)property.Invoke("setData", new object[] { "text", text })))
                {
                    HtmlPage.Window.Alert("");
                }
            }
            else
            {
                HtmlPage.Window.Alert("即将复制该内容...");
            }
        }
    }
}
