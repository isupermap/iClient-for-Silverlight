﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava2_SampleCode
{
    public partial class Query : UserControl
    {
        private const string PointLayerName = "School@changchun";
        private const string LineLayerName = "Railway@changchun";
        private const string AreaLayerName = "ResidentialArea@changchun";
        private const string AllLayer = "全部图层";
        private FeaturesLayer featuresLayer;
        private ComboBox comboBox;

        public Query()
        {
            InitializeComponent();

            //通过图层的索引号来获取图层
            featuresLayer = MyMap.Layers[1] as FeaturesLayer;

            //设置显示查询图层的下拉框
            #region ComboBox
            comboBox = new ComboBox();
            comboBox.Width = 160;
            comboBox.VerticalAlignment = VerticalAlignment.Top;
            comboBox.HorizontalAlignment = HorizontalAlignment.Right;

            ComboBoxItem itemPoint = new ComboBoxItem();
            itemPoint.Content = PointLayerName;
            ComboBoxItem itemLine = new ComboBoxItem();
            itemLine.Content = LineLayerName;
            ComboBoxItem itemRegion = new ComboBoxItem();
            itemRegion.Content = AreaLayerName;
            ComboBoxItem itemAll = new ComboBoxItem();
            itemAll.Content = AllLayer;
            comboBox.Items.Add(itemPoint);
            comboBox.Items.Add(itemLine);
            comboBox.Items.Add(itemRegion);
            comboBox.Items.Add(itemAll);
            MyStackPanel.Children.Add(comboBox);
            comboBox.SelectedIndex = 1;
            #endregion
        }

        //自定义SchoolData类，用于存储"School@changchun"图层的属性值
        public class SchoolData
        {
            public string SmID { get; set; }
            public string SmUserID { get; set; }
            public string name { get; set; }
            public string X { get; set; }
            public string Y { get; set; }
            public string SmX { get; set; }
            public string SmY { get; set; }
        }

        //自定义RoadData类，用于存储"Railway@changchun"图层的属性值
        public class RoadData
        {
            public string SmID { get; set; }
            public string SmUserID { get; set; }
            public string SmLength { get; set; }
            public string Name { get; set; }
        }

        //自定义ResidentialData类，用于存储"ResidentialArea@changchun"图层的属性值
        public class ResidentialData
        {
            public string SmID { get; set; }
            public string SmUserID { get; set; }
            public string SmArea { get; set; }
            public string SmPerimeter { get; set; }
            public string ClassID { get; set; }
        }

        //showResult 函数将查询结果在DataGrid控件和地图中显示
        private void ShowResult(ResultSet result)
        {
            //显示或隐藏相关控件
            DataStackPanel.Visibility = Visibility.Visible;
            SchoolScroll.Visibility = Visibility.Collapsed;
            dgSchool.Visibility = Visibility.Collapsed;
            ResidentialScroll.Visibility = Visibility.Collapsed;
            dgResidential.Visibility = Visibility.Collapsed;
            dgRoad.Visibility = Visibility.Collapsed;
            RoadScroll.Visibility = Visibility.Collapsed;

            //结果为空
            if (result == null)
            {
                MessageBox.Show("No Result!");
                return;
            }

            //遍历全部结果记录集
            foreach (RecordSet item in result.RecordSets)
            {
                //获取“School@changchun”图层上查询的结果并进行显示
                if (item.LayerName == PointLayerName)
                {
                    SchoolScroll.Visibility = Visibility.Visible;
                    dgSchool.Visibility = Visibility.Visible;
                    List<SchoolData> listSchool = new List<SchoolData>();

                    //将记录集中的每条记录转换成矢量要素，对其进行遍历，并将矢量要素在地图和DataGrid控件中显示
                    foreach (Feature feature in item.ToFeatureSet())
                    {
                        featuresLayer.Features.Add(feature);
                        listSchool.Add(new SchoolData
                        {
                            SmID = feature.Attributes["SmID"].ToString(),
                            SmUserID = feature.Attributes["SmUserID"].ToString(),
                            name = feature.Attributes["name"].ToString(),
                            X = feature.Attributes["X"].ToString(),
                            Y = feature.Attributes["Y"].ToString(),
                            SmX = feature.Attributes["SmX"].ToString(),
                            SmY = feature.Attributes["SmY"].ToString()
                        });
                    }
                    dgSchool.ItemsSource = listSchool;
                }

                //获取"Railway@changchun"图层上查询的结果并进行显示
                if (item.LayerName == LineLayerName)
                {
                    RoadScroll.Visibility = Visibility.Visible;
                    dgRoad.Visibility = Visibility.Visible;
                    List<RoadData> listData = new List<RoadData>();

                    //将记录集中的每条记录转换成矢量要素，对其进行遍历，并将矢量要素在地图和DataGrid控件中显示
                    foreach (Feature feature in item.ToFeatureSet())
                    {
                        featuresLayer.Features.Add(feature);
                        listData.Add(new RoadData
                        {
                            SmID = feature.Attributes["SmID"].ToString(),
                            SmUserID = feature.Attributes["SmUserID"].ToString(),
                            Name = feature.Attributes["Name"].ToString(),
                            SmLength = feature.Attributes["SmLength"].ToString(),

                        });
                    }
                    dgRoad.ItemsSource = listData;
                }

                //获取"ResidentialArea@changchun"图层上查询的结果并进行显示
                if (item.LayerName == AreaLayerName)
                {
                    ResidentialScroll.Visibility = Visibility.Visible;
                    dgResidential.Visibility = Visibility.Visible;
                    List<ResidentialData> listResidential = new List<ResidentialData>();

                    //将记录集中的每条记录转换成矢量要素，对其进行遍历，并将矢量要素在地图和DataGrid控件中显示
                    foreach (Feature feature in item.ToFeatureSet())
                    {
                        featuresLayer.Features.Add(feature);
                        listResidential.Add(new ResidentialData
                        {
                            SmID = feature.Attributes["SmID"].ToString(),
                            SmUserID = feature.Attributes["SmUserID"].ToString(),
                            SmArea = feature.Attributes["SmArea"].ToString(),
                            SmPerimeter = feature.Attributes["SmPerimeter"].ToString(),
                            ClassID = feature.Attributes["ClassID"].ToString(),
                        });
                    }
                    dgResidential.ItemsSource = listResidential;
                }
            }
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }


        //绘制点要素
        private void Point_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint point = new DrawPoint(MyMap);
            MyMap.Action = point;

            //绘制结束调用point_DrawCompleted函数
            point.DrawCompleted += new System.EventHandler<DrawEventArgs>(point_DrawCompleted);
        }

        //中心点查询
        private void point_DrawCompleted(object sender, DrawEventArgs e)
        {

            List<QueryLayerParam> queryLayerParams = new List<QueryLayerParam>();
            if (comboBox.SelectedIndex != 3)
            {   //在一个图层中查询，获取要查询的图层名
                QueryLayerParam queryLayerParam = new QueryLayerParam();
                queryLayerParam.Name = comboBox.SelectionBoxItem.ToString();

                //查询图层参数列表
                queryLayerParams = new List<QueryLayerParam>() { queryLayerParam };
            }
            else
            {
                //在三个图层中查询，获取要查询的图层名
                QueryLayerParam queryLayerParamPoint = new QueryLayerParam();
                queryLayerParamPoint.Name = PointLayerName;
                QueryLayerParam queryLayerParamLine = new QueryLayerParam();
                queryLayerParamLine.Name = LineLayerName;
                QueryLayerParam queryLayerParamPolygon = new QueryLayerParam();
                queryLayerParamPolygon.Name = AreaLayerName;

                //查询图层参数列表
                queryLayerParams = new List<QueryLayerParam>() 
                {   queryLayerParamPoint, 
                    queryLayerParamLine, 
                    queryLayerParamPolygon 
                };
            }

            //将文本中读取的容限值转换为Double类型，如果值为0则将其设为100
            double tolerance;
            if (!double.TryParse(MyTextBox.Text, out tolerance))
            {
                tolerance = 100;
            }

            //设置中心点查询参数，其中四个属性都为必设属性
            QueryByCenterParameters parameters = new QueryByCenterParameters
            {
                CenterPoint = e.Geometry.Bounds.Center,
                Tolerance = tolerance,
                MapName = "Changchun",

                // ExceptCount属性小于等于0时表示返回全部查询结果
                QueryParam = new QueryParam { QueryLayerParams = queryLayerParams, ExpectCount =1000 }
            };

            //是否查找最近地物
            if (FindNearest.IsChecked == true)
            {
                parameters.IsNearest = true;
            }

            //与指定服务器交互
            QueryByCenterService queryByCenterService = new QueryByCenterService("http://localhost:7080/demo");
            queryByCenterService.ProcessAsync(parameters);
            queryByCenterService.Failed += new System.EventHandler<ServiceFailedEventArgs>(queryByCenterService_Failed);
            queryByCenterService.ProcessCompleted += new System.EventHandler<QueryEventArgs>(queryByCenterService_ProcessCompleted);
        }

        //服务器返回查询结果，记录在QueryServiceEventArgs中
        private void queryByCenterService_ProcessCompleted(object sender, QueryEventArgs e)
        {
            ShowResult(e.ResultSet);
        }

        //与服务器交互失败，提示错误信息
        private void queryByCenterService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.ToString());
        }

        //清除全部查询到的矢量要素
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            //清除要素
            featuresLayer.Features.Clear();

            //隐藏相关控件
            dgSchool.Visibility = Visibility.Collapsed;
            SchoolScroll.Visibility = Visibility.Collapsed;
            ResidentialScroll.Visibility = Visibility.Collapsed;
            dgResidential.Visibility = Visibility.Collapsed;
            dgRoad.Visibility = Visibility.Collapsed;
            RoadScroll.Visibility = Visibility.Collapsed;
            DataStackPanel.Visibility = Visibility.Collapsed;
        }
    }
}
