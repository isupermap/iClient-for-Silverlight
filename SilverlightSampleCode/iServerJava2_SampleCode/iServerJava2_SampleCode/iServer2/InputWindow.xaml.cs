﻿using System.Windows;
using System.Windows.Controls;

namespace iServerJava2_SampleCode
{
    public partial class InputWindow : ChildWindow
    {
        public InputWindow()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.Tag = Radius.Text;

            this.Close();
        }
    }
}
