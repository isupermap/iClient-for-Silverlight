﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava2_SampleCode
{
    public partial class ServerMeasure : UserControl
    {
        private FeaturesLayer featuresLayer;
        private ElementsLayer elementsLayer;

        public ServerMeasure()
        {
            InitializeComponent();
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            elementsLayer = this.MyMap.Layers["MyArbitraryLayer"] as ElementsLayer;
        }

        //绘制距离量算线段
        private void MDistance_Click(object sender, RoutedEventArgs e)
        {
            DrawLine line = new DrawLine(MyMap);
            MyMap.Action = line;
            line.DrawCompleted += new EventHandler<DrawEventArgs>(line_DrawCompleted);
        }

        //距离量算
        private void line_DrawCompleted(object sender, DrawEventArgs e)
        {
            //将线标绘在客户端要素图层
            PredefinedLineStyle lineStyle = new PredefinedLineStyle { Stroke = new SolidColorBrush(Colors.Red), StrokeThickness = 3 };
            Feature feature = new Feature
                {
                    Geometry = e.Geometry,
                    Style = lineStyle
                };
            featuresLayer.Features.Add(feature);

            MeasureParameters parameters = new MeasureParameters
            {
                Geometry = e.Geometry,
                MapName = "Changchun"
            };
            MeasureService measureDistance = new MeasureService("http://localhost:7080/demo/");
            measureDistance.ProcessAsync(parameters);
            measureDistance.ProcessCompleted += new EventHandler<MeasureEventArgs>(measureDistance_ProcessCompleted);
            measureDistance.Failed += new EventHandler<ServiceFailedEventArgs>(measure_Failed);
        }

        //得到量算距离
        private void measureDistance_ProcessCompleted(object sender, MeasureEventArgs e)
        {
            MessageBox.Show(e.Result.Distance.ToString() + " 米");
        }

        //绘制面
        private void MArea_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon polygon = new DrawPolygon(MyMap);
            MyMap.Action = polygon;
            polygon.DrawCompleted += new EventHandler<DrawEventArgs>(polygon_DrawCompleted);
        }

        //面积量算
        private void polygon_DrawCompleted(object sender, DrawEventArgs e)
        {
            //将面标绘在客户端任意图层
            PolygonElement polygon = e.Element as PolygonElement;
            polygon.Opacity = 0.618;
            polygon.StrokeThickness = 1;
            polygon.Fill = new SolidColorBrush(Colors.LightGray);
            this.elementsLayer.Children.Add(polygon);

            MeasureParameters parameters = new MeasureParameters
            {
                Geometry = e.Geometry,
                MapName = "Changchun"
            };

            //与服务器交互
            MeasureService measureRegion = new MeasureService("http://localhost:7080/demo/");
            measureRegion.ProcessAsync(parameters);
            measureRegion.ProcessCompleted += new EventHandler<MeasureEventArgs>(measureRegion_ProcessCompleted);
            measureRegion.Failed += new EventHandler<ServiceFailedEventArgs>(measure_Failed);
        }

        //服务器处理失败提示错误信息
        private void measure_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //得到量算面积
        private void measureRegion_ProcessCompleted(object sender, MeasureEventArgs e)
        {
            MessageBox.Show(e.Result.Area.ToString() + " 平方米");
        }

        //清除要素
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }
    }
}
