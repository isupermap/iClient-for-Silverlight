﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;

namespace iServerJava2_SampleCode
{
    public partial class SpatialOperateTest : UserControl
    {
        private QueryLayerParam qlpVegetable = new QueryLayerParam { Name = "Vegetable@changchun" }; // SqlParam = new SqlParam { WhereClause = "smid>50" }
        private QueryLayerParam qlpResidentialArea = new QueryLayerParam { Name = "ResidentialArea@changchun" };
        private ServerGeometry sgVegetable;
        private ServerGeometry sgResidentialArea;
        private FeaturesLayer paramLayer;
        private FeaturesLayer resultLayer;

        public SpatialOperateTest()
        {
            InitializeComponent();

            paramLayer = MyMap.Layers["ParamLayer"] as FeaturesLayer;
            resultLayer = MyMap.Layers["ResultLayer"] as FeaturesLayer;
        }

        private void vegetable_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint point = new DrawPoint(MyMap);
            MyMap.Action = point;
            point.DrawCompleted += new EventHandler<DrawEventArgs>(point_DrawCompleted);
        }

        private void point_DrawCompleted(object sender, DrawEventArgs e)
        {
            QueryByCenterParameters parameters = new QueryByCenterParameters
                        {
                            CenterPoint = e.Geometry.Bounds.Center,
                            Tolerance = 5,
                            MapName = "Changchun",
                            QueryParam = new QueryParam { QueryLayerParams = new List<QueryLayerParam>() { qlpVegetable } }
                        };
            QueryByCenterService query = new QueryByCenterService("http://localhost:7080/demo");
            query.ProcessAsync(parameters);
            query.Failed += (s, args) => { MessageBox.Show(args.Error.ToString()); };
            query.ProcessCompleted += new EventHandler<QueryEventArgs>(query_ProcessCompleted);
        }

        private void query_ProcessCompleted(object sender, QueryEventArgs e)
        {
            if (e.ResultSet == null)
            {
                MessageBox.Show("No Result!");
                return;
            }
            sgVegetable = e.ResultSet.RecordSets[0].Records[0].Shape;
            FeatureCollection fc = e.ResultSet.RecordSets[0].ToFeatureSet();
            Feature f = new Feature { Geometry = fc[0].Geometry, Style = this.GreenFillStyle };
            paramLayer.Features.Add(f);
        }

        private void residential_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint point = new DrawPoint(MyMap);
            MyMap.Action = point;
            point.DrawCompleted += (s2, e2) =>
            {
                QueryByCenterParameters parameters = new QueryByCenterParameters
                {
                    CenterPoint = e2.Geometry.Bounds.Center,
                    Tolerance = 5,
                    MapName = "Changchun",
                    QueryParam = new QueryParam { QueryLayerParams = new List<QueryLayerParam>() { qlpResidentialArea } }
                };
                QueryByCenterService query = new QueryByCenterService("http://localhost:7080/demo");
                query.ProcessAsync(parameters);
                query.Failed += (s, args) => { MessageBox.Show(args.Error.ToString()); };
                query.ProcessCompleted += (s3, e3) =>
                {
                    if (e3.ResultSet == null)
                    {
                        MessageBox.Show("No Result!");
                        return;
                    }
                    sgResidentialArea = e3.ResultSet.RecordSets[0].Records[0].Shape;
                    FeatureCollection fc = e3.ResultSet.RecordSets[0].ToFeatureSet();
                    Feature f = new Feature { Geometry = fc[0].Geometry, Style = this.RedFillStyle };
                    paramLayer.Features.Add(f);
                };
            };
        }

        private void union_Click(object sender, RoutedEventArgs e)
        {
            SpatialOperateParameters sop = new SpatialOperateParameters
            {
                SourceGeometry = sgVegetable,
                OperatorGeometry = sgResidentialArea,
                SpatialOperationType = SpatialOperationType.Union,
                MapName = "Changchun"
            };
            SpatialOperateService sos = new SpatialOperateService("http://localhost:7080/demo");
            sos.ProcessAsync(sop);
            sos.Failed += (s, args) => { MessageBox.Show(args.Error.ToString()); };
            sos.ProcessCompleted += new EventHandler<SpatialOperateEventArgs>(sos_ProcessCompleted);

        }

        private void sos_ProcessCompleted(object sender, SpatialOperateEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("No Result!");
                return;
            }
            ServerGeometry r = e.Result;
            SuperMap.Web.Core.Geometry g = r.ToGeometry();
            Feature f = new Feature { Geometry = g, Style = (SuperMap.Web.Core.Style)App.Current.Resources["CustomDiagonalBlackHatchFillSymbol"] };
            resultLayer.Features.Add(f);

        }

        private void pan_Click(object sender, RoutedEventArgs e)
        {
            MyMap.Action = new Pan(MyMap);
        }

        private void param_Click(object sender, RoutedEventArgs e)
        {
            paramLayer.Features.Clear();
        }

        private void result_Click(object sender, RoutedEventArgs e)
        {
            resultLayer.Features.Clear();
        }
    }
}


