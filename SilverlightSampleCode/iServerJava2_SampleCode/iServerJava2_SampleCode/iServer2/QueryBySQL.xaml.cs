﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;

namespace iServerJava2_SampleCode
{
    public partial class QueryBySQL : UserControl
    {
        private const string PointLayerName = "Capital@world";
        private const string LineLayerName = "Continent_Lable@world";
        private const string AreaLayerName = "World@world";
        private FeaturesLayer featuresLayer;
        private ComboBox comboBox;

        public QueryBySQL()
        {
            InitializeComponent();
            featuresLayer = MyMap.Layers[1] as FeaturesLayer;

            //设置显示查询图层的下拉框
            #region ComboBox
            comboBox = new ComboBox();
            comboBox.Width = 160;
            comboBox.Height = 25;
            comboBox.VerticalAlignment = VerticalAlignment.Top;
            comboBox.HorizontalAlignment = HorizontalAlignment.Right;

            ComboBoxItem itemPoint = new ComboBoxItem();
            itemPoint.Content = PointLayerName;
            ComboBoxItem itemLine = new ComboBoxItem();
            itemLine.Content = LineLayerName;
            ComboBoxItem itemRegion = new ComboBoxItem();
            itemRegion.Content = AreaLayerName;
            ComboBoxItem itemAll = new ComboBoxItem();
            itemAll.Content = "全部图层";
            comboBox.Items.Add(itemPoint);
            comboBox.Items.Add(itemLine);
            comboBox.Items.Add(itemRegion);
            comboBox.Items.Add(itemAll);
            MyStackPanel.Children.Add(comboBox);
            comboBox.SelectedIndex = 2;
            #endregion

            MyTextBox.Text = "smid<10 and smid>3";
        }

        //自定义WorldData类，用于存储"World@world"图层的属性值
        public class WorldData
        {
            public string SmID { get; set; }
            public string SmUserID { get; set; }
            public string SmArea { get; set; }
            public string SmPerimeter { get; set; }
            public string COLOR_MAP { get; set; }
            public string Country { get; set; }
        }

        //自定义CapitalData类，用于存储"Capital@world"图层的属性值
        public class CapitalData
        {
            public string SmID { get; set; }
            public string SmX { get; set; }
            public string SmY { get; set; }
            public string Capital { get; set; }
            public string Country { get; set; }
            public string Cap_Pop { get; set; }
        }

        //自定义ContinentData类，用于存储"Continent_Lable@world"图层的属性值
        public class ContinentData
        {
            public string SmID { get; set; }
            public string SmLength { get; set; }
            public string CONTINENT { get; set; }
            public string id { get; set; }
        }

        // showResult 函数将查询结果在DataGrid控件和地图中显示
        private void ShowResult(ResultSet result)
        {
            // 显示或隐藏相关控件
            DataStackPanel.Visibility = Visibility.Visible;
            worldScroll.Visibility = Visibility.Collapsed;
            world.Visibility = Visibility.Collapsed;
            continentScroll.Visibility = Visibility.Collapsed;
            continent.Visibility = Visibility.Collapsed;
            capital.Visibility = Visibility.Collapsed;
            capitalScroll.Visibility = Visibility.Collapsed;

            //结果为空
            if (result == null)
            {
                MessageBox.Show("No Result!");
                return;
            }

            //遍历全部结果记录集
            foreach (RecordSet item in result.RecordSets)
            {
                //获取"Capital@world"图层上查询的结果并进行显示
                if (item.LayerName == PointLayerName)
                {
                    capitalScroll.Visibility = Visibility.Visible;
                    capital.Visibility = Visibility.Visible;
                    List<CapitalData> listCapital = new List<CapitalData>();

                    //将记录集中的每条记录转换成矢量要素，对其进行遍历，并将矢量要素在地图和DataGrid控件中显示
                    foreach (Feature feature in item.ToFeatureSet())
                    {
                        featuresLayer.Features.Add(feature);

                        listCapital.Add(new CapitalData
                        {
                            SmID = feature.Attributes["SmID"].ToString(),
                            SmX = feature.Attributes["SmX"].ToString(),
                            SmY = feature.Attributes["SmY"].ToString(),
                            Capital = feature.Attributes["Capital"].ToString(),
                            Country = feature.Attributes["Country"].ToString(),
                            Cap_Pop = feature.Attributes["Cap_Pop"].ToString()
                        });
                    }
                    capital.ItemsSource = listCapital;
                }

                //获取"Continent_Lable@world"图层上查询的结果并进行显示
                if (item.LayerName == LineLayerName)
                {
                    continentScroll.Visibility = Visibility.Visible;
                    continent.Visibility = Visibility.Visible;
                    List<ContinentData> listContinent = new List<ContinentData>();

                    //将记录集中的每条记录转换成矢量要素，对其进行遍历，并将矢量要素在地图和DataGrid控件中显示
                    foreach (Feature feature in item.ToFeatureSet())
                    {
                        featuresLayer.Features.Add(feature);

                        listContinent.Add(new ContinentData
                        {
                            SmID = feature.Attributes["SmID"].ToString(),
                            SmLength = feature.Attributes["SmLength"].ToString(),
                            CONTINENT = feature.Attributes["CONTINENT"].ToString(),
                            id = feature.Attributes["id"].ToString()

                        });
                    }
                    continent.ItemsSource = listContinent;
                }

                //获取"World@world"图层上查询的结果并进行显示
                if (item.LayerName == AreaLayerName)
                {
                    worldScroll.Visibility = Visibility.Visible;
                    world.Visibility = Visibility.Visible;
                    List<WorldData> listWorld = new List<WorldData>();

                    //将记录集中的每条记录转换成矢量要素，对其进行遍历，并将矢量要素在地图和DataGrid控件中显示
                    foreach (Feature feature in item.ToFeatureSet())
                    {
                        featuresLayer.Features.Add(feature);

                        listWorld.Add(new WorldData
                        {
                            SmID = feature.Attributes["SmID"].ToString(),
                            SmUserID = feature.Attributes["SmUserID"].ToString(),
                            SmArea = feature.Attributes["SmArea"].ToString(),
                            SmPerimeter = feature.Attributes["SmPerimeter"].ToString(),
                            COLOR_MAP = feature.Attributes["COLOR_MAP"].ToString(),
                            Country = feature.Attributes["Country"].ToString()
                        });
                    }
                    world.ItemsSource = listWorld;
                }
            }
        }

        //SQL查询
        private void QueryBySQL_Click(object sender, RoutedEventArgs e)
        {
            List<QueryLayerParam> queryLayerParams = new List<QueryLayerParam>();
            string str = MyTextBox.Text;
            if (str == "")
            {
                MessageBox.Show("请输入SQL条件！");
            }

            //在一个图层中查询，设置要查询的图层名和SQL语句
            if (comboBox.SelectedIndex != 3)
            {
                QueryLayerParam queryLayerParam = new QueryLayerParam();
                queryLayerParam.Name = comboBox.SelectionBoxItem.ToString();
                queryLayerParam.SqlParam = new SqlParam() { WhereClause = str };
                queryLayerParams = new List<QueryLayerParam>() { queryLayerParam };
            }

            // 在三个图层中查询，设置要查询的图层名和SQL语句
            else
            {

                QueryLayerParam queryLayerParamPoint = new QueryLayerParam();
                queryLayerParamPoint.Name = PointLayerName;
                queryLayerParamPoint.SqlParam = new SqlParam() { WhereClause = str };

                QueryLayerParam queryLayerParamLine = new QueryLayerParam();
                queryLayerParamLine.Name = LineLayerName;
                queryLayerParamLine.SqlParam = new SqlParam() { WhereClause = str };

                QueryLayerParam queryLayerParamArea = new QueryLayerParam();
                queryLayerParamArea.Name = AreaLayerName;
                queryLayerParamArea.SqlParam = new SqlParam() { WhereClause = str };

                queryLayerParams = new List<QueryLayerParam>() { queryLayerParamPoint, queryLayerParamLine, queryLayerParamArea };
            }

            // 设置SQL查询参数类，以下属性为必设属性
            QueryBySqlParameters parameters = new QueryBySqlParameters
            {
                MapName = "World",

                //ExpectCount小于等于0时返回全部查询结果
                QueryParam = new QueryParam { QueryLayerParams = queryLayerParams, ExpectCount = 0 }
            };

            //与指定服务器交互
            QueryBySqlService query = new QueryBySqlService("http://localhost:7080/demo/");
            query.ProcessAsync(parameters);
            query.Failed += (s, args) => { MessageBox.Show(args.Error.ToString()); };
            query.ProcessCompleted += (s, args) => { ShowResult(args.ResultSet); };
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            //清除要素
            featuresLayer.Features.Clear();

            //隐藏相关控件
            world.Visibility = Visibility.Collapsed;
            worldScroll.Visibility = Visibility.Collapsed;
            continent.Visibility = Visibility.Collapsed;
            continentScroll.Visibility = Visibility.Collapsed;
            capital.Visibility = Visibility.Collapsed;
            capitalScroll.Visibility = Visibility.Collapsed;
            DataStackPanel.Visibility = Visibility.Collapsed;
        }
    }
}
