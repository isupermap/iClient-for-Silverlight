﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava2_SampleCode
{
    public partial class ClosestFacility : UserControl
    {
        private ElementsLayer arbitraryLayerE;
        private ElementsLayer arbitraryLayerF;
        private Point2DCollection points = new Point2DCollection();
        private Point2D eventp = new Point2D();
        private FeaturesLayer featuresLayer;
        private bool flag = false;

        public ClosestFacility()
        {
            InitializeComponent();
            arbitraryLayerE = this.MyMap.Layers["MyArbitraryLayerE"] as ElementsLayer;
            arbitraryLayerF = this.MyMap.Layers["MyArbitraryLayerF"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        //选择事件点或设施点
        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            Pushpin pushpin = new Pushpin();

            //将事件点以图钉样式加到ArbitraryLayerE上
            if (eventPoint.IsChecked == true)
            {
                //因为事件点只有一个，因此判断是否存在事件点，如果存在就先将以前的事件点删除
                if (flag == true)
                {
                    arbitraryLayerE.Children.Clear();
                }

                pushpin.Location = e.Geometry.Bounds.Center;
                pushpin.Content = "E";
                pushpin.Background = new SolidColorBrush(Colors.Red);
                arbitraryLayerE.AddChild(pushpin);
                flag = true;

                //记录事件点坐标
                eventp = pushpin.Location;
            }

            //将设施点以图钉样式加到ElementsLayer上
            if (FacilityPoint.IsChecked == true)
            {
                pushpin.Location = e.Geometry.Bounds.Center;
                pushpin.Content = "F";
                pushpin.Background = new SolidColorBrush(Colors.Purple);
                arbitraryLayerF.AddChild(pushpin);

                //用points数组记录设施点坐标
                points.Add(pushpin.Location);
            }

        }


        //最近设施查找
        private void FindClosestFacility_Click(object sender, RoutedEventArgs e)
        {
            //设置权重字段列表
            WeightFieldInfo weightFieldInfo = new WeightFieldInfo { Name = "length", TFWeightField = "SmLength", FTWeightField = "SmLength" };
            List<WeightFieldInfo> list = new List<WeightFieldInfo>();
            list.Add(weightFieldInfo);

            //设置网络分析模型
            NetworkModelSetting networkModelSetting = new NetworkModelSetting
            {
                NetworkDatasetName = "RoadNet",
                NetworkDataSourceName = "changchun",
                Tolerance = 30,
                NodeIDField = "SMNODEID",
                EdgeIDField = "SmID",
                WeightFieldInfos = list,
                TNodeIDField = "SMTNODE",
                FNodeIDField = "SMFNODE",

            };

            //设置网络分析参数
            NetworkAnalystParam networkAnalystParam = new NetworkAnalystParam
            {
                Point2Ds = points,
                IsPathsReturn = true,
                WeightName = "length"
            };

            //设置最近设施查找参数
            ProximityParam proximityParam = new ProximityParam
            {
                FacilityCount = 1,
                MaxImpedance = 100000,
                NetworkAnalystParam = networkAnalystParam
            };

            //设置最近设施分析参数
            ClosestFacilityParameters paramters = new ClosestFacilityParameters
            {
                MapName = "Changchun",
                EventPoint = eventp,
                NetworkSetting = networkModelSetting,
                ProximityParam = proximityParam
            };

            //与服务器交换
            ClosestFacilityService service = new ClosestFacilityService("http://localhost:7080/demo");
            service.ProcessAsync(paramters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<NetworkAnalystEventArgs>(service_ProcessCompleted);
        }

        //服务器返回结果
        private void service_ProcessCompleted(object sender, NetworkAnalystEventArgs e)
        {
            foreach (ServerGeometry p in e.Result.Paths)
            {
                PredefinedLineStyle lineStyle = new PredefinedLineStyle();
                lineStyle.Stroke = new SolidColorBrush(Colors.Blue);
                lineStyle.StrokeThickness = 2;

                Feature path = new Feature();
                path.Geometry = p.ToGeometry();
                path.Style = lineStyle;
                featuresLayer.AddFeature(path);
            }
        }

        //服务器计算失败提示失败信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //清除所有图层上全部元素并清空点坐标记录
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            arbitraryLayerF.Children.Clear();
            arbitraryLayerE.Children.Clear();
            featuresLayer.ClearFeatures();
            points.Clear();
            eventp = Point2D.Empty;
            flag = false;
        }
    }
}
