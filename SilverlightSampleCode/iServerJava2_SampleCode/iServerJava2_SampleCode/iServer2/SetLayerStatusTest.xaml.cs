﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;

namespace iServerJava2_SampleCode
{
    public partial class SetLayerStatusTest : UserControl
    {
        private bool isAdded;

        public SetLayerStatusTest()
        {
            InitializeComponent();
            Load();
        }

        private DynamicIServerLayer layer = new DynamicIServerLayer()
        {
            Url = "http://localhost:7080/demo",
            MapName = "World",
            MapServiceAddress = "localhost",
            MapServicePort = 8600,
            AdjustFactor = 1.00025
        };

        private void mybtn_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void Load()
        {
            LayerStatus ls1 = new LayerStatus { LayerName = "World@world#1", IsVisible = (bool)ch1.IsChecked };
            LayerStatus ls2 = new LayerStatus { LayerName = "Ocean@world", IsVisible = (bool)ch2.IsChecked };
            List<LayerStatus> list = new List<LayerStatus>();
            list.Add(ls1);
            list.Add(ls2);
            SetLayerStatusParameters parameters = new SetLayerStatusParameters
                      {
                          MapName = "World",
                          LayerStatusList = list
                      };
            SetLayerStatusService service = new SetLayerStatusService { Url = "http://localhost:7080/demo", DisableClientCaching = true };
            service.ProcessAsync(parameters);
            service.Failed += (s1, e1) => { MessageBox.Show(e1.Error.ToString()); };
            service.ProcessCompleted += new System.EventHandler<SetLayerStatusEventArgs>(service_ProcessCompleted);

        }

        private void service_ProcessCompleted(object sender, SetLayerStatusEventArgs e)
        {
            layer.LayersKey = e.Result.LayersKey;
            if (!isAdded)
            {
                this.MyMap.Layers.Add(layer);
                isAdded = true;
            }
        }
    }
}
