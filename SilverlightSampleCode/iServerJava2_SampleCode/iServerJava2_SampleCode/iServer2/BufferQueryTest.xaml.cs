﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;
using System.Windows.Media;

namespace iServerJava2_SampleCode
{
    public partial class BufferQueryTest : UserControl
    {
        private const string SchoolLayerName = "School@changchun";
        private const string RailLayerName = "Railway@changchun";
        private const string ResidentialLayerName = "ResidentialArea@changchun";
        private const string AllLayer = "全部图层";
        private FeaturesLayer featuresLayer;
        public BufferQueryTest()
        {
            InitializeComponent();
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;

            //设置被查询图层的下拉框
            #region ComboBox
            QueriedComboBox.VerticalAlignment = VerticalAlignment.Top;
            ComboBoxItem itemPoint = new ComboBoxItem();
            itemPoint.Content = SchoolLayerName;
            ComboBoxItem itemLine = new ComboBoxItem();
            itemLine.Content = RailLayerName;
            ComboBoxItem itemRegion = new ComboBoxItem();
            itemRegion.Content = ResidentialLayerName;
            ComboBoxItem itemAll = new ComboBoxItem();
            itemAll.Content = AllLayer;
            QueriedComboBox.Items.Add(itemPoint);
            QueriedComboBox.Items.Add(itemLine);
            QueriedComboBox.Items.Add(itemRegion);
            QueriedComboBox.Items.Add(itemAll);
            QueriedComboBox.SelectedIndex = 0;
            #endregion
        }

        //自定义SchoolData类，用于存储"School@changchun"图层的属性值
        public class SchoolData
        {
            public string SmID { get; set; }
            public string SmUserID { get; set; }
            public string name { get; set; }
            public string X { get; set; }
            public string Y { get; set; }
            public string SmX { get; set; }
            public string SmY { get; set; }
        }

        //自定义RoadData类，用于存储"Railway@changchun"图层的属性值
        public class RoadData
        {
            public string SmID { get; set; }
            public string SmUserID { get; set; }
            public string SmLength { get; set; }
            public string Name { get; set; }
        }

        //自定义ResidentialData类，用于存储"ResidentialArea@changchun"图层的属性值
        public class ResidentialData
        {
            public string SmID { get; set; }
            public string SmUserID { get; set; }
            public string SmArea { get; set; }
            public string SmPerimeter { get; set; }
            public string ClassID { get; set; }
        }

        //缓冲区查询时隐藏地物图层选择下拉框
        private void BufferQuery_Click(object sender, RoutedEventArgs e)
        {
            MyBorder.Visibility = Visibility.Collapsed;
        }

        //地物缓冲区查询时显示地物图层选择下拉框
        private void EntityBufferQuery_Checked(object sender, RoutedEventArgs e)
        {
            MyBorder.Visibility = Visibility.Visible;
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //点缓冲区查询
        private void Point_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint point = new DrawPoint(MyMap);
            MyMap.Action = point;

            //绘制结束调用DrawCompleted函数
            point.DrawCompleted += new EventHandler<DrawEventArgs>(DrawCompleted);
        }

        //线缓冲区查询
        private void Line_Click(object sender, RoutedEventArgs e)
        {
            DrawLine line = new DrawLine(MyMap);
            MyMap.Action = line;

            //绘制结束调用DrawCompleted函数
            line.DrawCompleted += new EventHandler<DrawEventArgs>(DrawCompleted);
        }

        //面缓冲区查询
        private void Region_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon polygon = new DrawPolygon(MyMap);
            MyMap.Action = polygon;

            //绘制结束调用DrawCompleted函数
            polygon.DrawCompleted += new EventHandler<DrawEventArgs>(DrawCompleted);
        }

        //几何对象绘制结束触发事件
        private void DrawCompleted(object sender, DrawEventArgs e)
        {
            LoadParam(e.Geometry);
        }

        //设置缓冲区查询参数类
        private void LoadParam(SuperMap.Web.Core.Geometry geo)
        {
            List<QueryLayerParam> queryLayerParams = new List<QueryLayerParam>();
            if (QueriedComboBox.SelectedIndex != 3)
            {
                //在一个图层中查询，获取要查询的图层名
                QueryLayerParam queryLayerParam = new QueryLayerParam();
                queryLayerParam.Name = QueriedComboBox.SelectionBoxItem.ToString();

                //查询图层参数列表
                queryLayerParams = new List<QueryLayerParam>() { queryLayerParam };
            }
            else
            {
                //在三个图层中查询，获取要查询的图层名
                QueryLayerParam queryLayerParamLine = new QueryLayerParam();
                queryLayerParamLine.Name = SchoolLayerName;
                QueryLayerParam queryLayerParamPolygon = new QueryLayerParam();
                queryLayerParamPolygon.Name = RailLayerName;
                QueryLayerParam queryLayerParamPoint = new QueryLayerParam();
                queryLayerParamPoint.Name = ResidentialLayerName;

                //查询图层参数列表
                queryLayerParams = new List<QueryLayerParam>() 
                    {   queryLayerParamPoint, 
                        queryLayerParamLine, 
                        queryLayerParamPolygon 
                    };
            }

            //缓冲区参数类
            BufferAnalystParam param = new BufferAnalystParam
            {
                LeftDistance = 100,
                SemicircleLineSegment = 10,
            };

            //如果为缓冲区查询，则设置缓冲区查询参数
            if (BufferQuery.IsChecked == true)
            {
                BufferQueryParameters parameters = new BufferQueryParameters
                {
                    BufferParam = param,
                    Geometry = geo,
                    QueryMode = SpatialQueryMode.Intersect,
                    MapName = "Changchun",
                    QueryParam = new QueryParam { QueryLayerParams = queryLayerParams, ExpectCount = 0 }
                };
                BufferQueryService query = new BufferQueryService("http://localhost:7080/demo/");
                query.ProcessAsync(parameters);
                query.ProcessCompleted += new EventHandler<BufferQueryEventArgs>(query_ProcessCompleted);
                query.Failed += new EventHandler<ServiceFailedEventArgs>(query_Failed);
            }

            //如果为地物缓冲区查询，则设置地物缓冲区查询参数
            else
            {
                EntityBufferQueryParameters parameters = new EntityBufferQueryParameters
                {
                    BufferParam = param,
                    Geometry = geo,
                    QueryMode = SpatialQueryMode.Intersect,
                    MapName = "Changchun",
                    QueryParam = new QueryParam { QueryLayerParams = queryLayerParams, ExpectCount = 0 },
                    FromLayer = QueryComboBox.SelectionBoxItem.ToString()
                };
                EntityBufferQueryService query = new EntityBufferQueryService("http://localhost:7080/demo/");
                query.ProcessAsync(parameters);
                query.ProcessCompleted += new EventHandler<BufferQueryEventArgs>(query_ProcessCompleted);
                query.Failed += new EventHandler<ServiceFailedEventArgs>(query_Failed);
            }
        }

        //查询出错，提示错误信息
        private void query_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //查询成果，调用ShowResult函数显示结果
        private void query_ProcessCompleted(object sender, BufferQueryEventArgs e)
        {
            ShowResult(e.BufferResultSet);
        }

        // showResult 函数将查询结果在DataGrid控件和地图中显示
        private void ShowResult(BufferResultSet result)
        {
            //显示或隐藏相关控件
            DataStackPanel.Visibility = Visibility.Visible;
            SchoolScroll.Visibility = Visibility.Collapsed;
            dgSchool.Visibility = Visibility.Collapsed;
            ResidentialScroll.Visibility = Visibility.Collapsed;
            dgResidential.Visibility = Visibility.Collapsed;
            dgRoad.Visibility = Visibility.Collapsed;
            RoadScroll.Visibility = Visibility.Collapsed;

            //结果为空
            if (result == null || result.BufferRegion == null || result.ResultSet == null)
            {
                MessageBox.Show("No Result!");
                return;
            }

            //遍历全部结果记录集            
            foreach (RecordSet item in result.ResultSet.RecordSets)
            {
                //获取“School@changchun”图层上查询的结果并进行显示
                if (item.LayerName == SchoolLayerName)
                {
                    SchoolScroll.Visibility = Visibility.Visible;
                    dgSchool.Visibility = Visibility.Visible;
                    List<SchoolData> listSchool = new List<SchoolData>();

                    //将记录集中的每条记录转换成矢量要素，对其进行遍历，并将矢量要素在地图和DataGrid控件中显示
                    foreach (Feature feature in item.ToFeatureSet())
                    {
                        featuresLayer.Features.Add(feature);

                        listSchool.Add(new SchoolData
                        {
                            SmID = feature.Attributes["SmID"].ToString(),
                            SmUserID = feature.Attributes["SmUserID"].ToString(),
                            name = feature.Attributes["name"].ToString(),
                            X = feature.Attributes["X"].ToString(),
                            Y = feature.Attributes["Y"].ToString(),
                            SmX = feature.Attributes["SmX"].ToString(),
                            SmY = feature.Attributes["SmY"].ToString()
                        });
                    }
                    dgSchool.ItemsSource = listSchool;
                }

                //获取"Railway@changchun"图层上查询的结果并进行显示
                if (item.LayerName == RailLayerName)
                {
                    RoadScroll.Visibility = Visibility.Visible;
                    dgRoad.Visibility = Visibility.Visible;
                    List<RoadData> listData = new List<RoadData>();

                    //将记录集中的每条记录转换成矢量要素，对其进行遍历，并将矢量要素在地图和DataGrid控件中显示
                    foreach (Feature feature in item.ToFeatureSet())
                    {
                        featuresLayer.Features.Add(feature);

                        listData.Add(new RoadData
                        {
                            SmID = feature.Attributes["SmID"].ToString(),
                            SmUserID = feature.Attributes["SmUserID"].ToString(),
                            Name = feature.Attributes["Name"].ToString(),
                            SmLength = feature.Attributes["SmLength"].ToString(),

                        });
                    }
                    dgRoad.ItemsSource = listData;
                }

                //获取"ResidentialArea@changchun"图层上查询的结果并进行显示
                if (item.LayerName == ResidentialLayerName)
                {
                    ResidentialScroll.Visibility = Visibility.Visible;
                    dgResidential.Visibility = Visibility.Visible;
                    List<ResidentialData> listResidential = new List<ResidentialData>();

                    //将记录集中的每条记录转换成矢量要素，对其进行遍历，并将矢量要素在地图和DataGrid控件中显示
                    foreach (Feature feature in item.ToFeatureSet())
                    {
                        featuresLayer.Features.Add(feature);

                        listResidential.Add(new ResidentialData
                        {
                            SmID = feature.Attributes["SmID"].ToString(),
                            SmUserID = feature.Attributes["SmUserID"].ToString(),
                            SmArea = feature.Attributes["SmArea"].ToString(),
                            SmPerimeter = feature.Attributes["SmPerimeter"].ToString(),
                            ClassID = feature.Attributes["ClassID"].ToString(),
                        });
                    }
                    dgResidential.ItemsSource = listResidential;
                }
            }

            featuresLayer.AddFeature(new Feature
            {
                Geometry = result.BufferRegion,
                Style = new PredefinedFillStyle
                {
                    Fill = new SolidColorBrush
                    {
                        Color = Colors.Yellow,
                        Opacity = 0.5,
                    },
                },
            });
        }

        //清除地图上的矢量要素
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            //清除要素
            featuresLayer.Features.Clear();

            //隐藏相关控件
            dgSchool.Visibility = Visibility.Collapsed;
            SchoolScroll.Visibility = Visibility.Collapsed;
            ResidentialScroll.Visibility = Visibility.Collapsed;
            dgResidential.Visibility = Visibility.Collapsed;
            dgRoad.Visibility = Visibility.Collapsed;
            RoadScroll.Visibility = Visibility.Collapsed;
            DataStackPanel.Visibility = Visibility.Collapsed;
        }
    }
}

