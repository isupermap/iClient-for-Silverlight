﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava2_SampleCode
{
    public partial class GraphTheme : UserControl
    {
        private bool isAdded = false;
        private List<string> listLayerName = new List<string>();
        private string url="http://localhost:7080/demo";
        //定义专题图层
        private TiledDynamicIServerLayer themeLayer = new TiledDynamicIServerLayer()
        {
            Url = "http://localhost:7080/demo" ,
            MapName = "World",
            MapServiceAddress = "localhost" ,
            MapServicePort = 8600
        };

        public GraphTheme()
        {
            InitializeComponent();
        }

        //制作统计专题图
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            //设置了二个子项
            ThemeGraphItem item1 = new ThemeGraphItem()
            {
                Caption = "面积",
                GraphExpression = "SmArea",
                UniformStyle = new ServerStyle() { FillForeColor = new ServerColor(255, 209, 173) },
            };

            ThemeGraphItem item2 = new ThemeGraphItem()
            {
                Caption = "人口",
                GraphExpression = "Pop_1994",
                UniformStyle = new ServerStyle() { FillForeColor = new ServerColor(233, 150, 127) },
            };

            SuperMap.Web.iServerJava2.ThemeGraph themeGraph = new SuperMap.Web.iServerJava2.ThemeGraph()
            {
                Items = new List<ThemeGraphItem>() { item1, item2 },
                AxesColor = new ServerColor(255, 0, 0),
                GraduatedMode = GraduatedMode.Constant,
                GraphTextFormat = GraphTextFormat.Caption,
                GraphType = GraphType.Bar3D,
                IsGraphTextDisplayed = true,
                IsFlowEnabled = false,
                IsLeaderLineDisplayed = true,
                LeaderLineStyle = new ServerStyle
                {
                    LineColor = new ServerColor { Red = 0, Green = 255, Blue = 0 }
                },
            };

            ThemeParameters parameters = new ThemeParameters()
             {
                 LayerName = "World@world",
                 MapName = "World",
                 Theme = themeGraph,
             };

            //与服务器交互
            ThemeService themeService = new ThemeService(url) { DisableClientCaching = true };
            themeService.ProcessAsync(parameters);
            themeService.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeService_ProcessCompleted);
            themeService.Failed += new EventHandler<ServiceFailedEventArgs>(themeService_Failed);
        }

        //服务器处理失败提示错误信息
        private void themeService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        private void themeService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            //服务器处理成功获取传回的专题图key值，唯一对应一个专题图的状态
            themeLayer.LayersKey = e.Result.Key;

            //用于控制只加载一层专题图图层，如果已经加载，就由key值控制该图层专题图的状态
            if (!isAdded)
            {
                this.MyMap.Layers.Add(themeLayer);
                isAdded = true;
            }

            //记录已加载的专题图的名字，为删除专题图做准备
            string layername = e.Result.Name;
            listLayerName.Add(e.Result.Name);
        }

        //清除专题图现有状态
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            RemoveThemesParameters parameters = new RemoveThemesParameters()
           {
               MapName = "World",
               LayerNames = listLayerName
           };

            RemoveThemesService removeService = new RemoveThemesService(url) { DisableClientCaching = true };
            removeService.ProcessAsync(parameters);
            removeService.ProcessCompleted += new EventHandler<RemoveThemesEventArgs>(removeService_ProcessCompleted);
            removeService.Failed += new EventHandler<ServiceFailedEventArgs>(removeService_Failed);
        }

        //移除成功则获取服务器返回的新的key值，从而专题图图层状态变为空专题图,即只有底图
        private void removeService_ProcessCompleted(object sender, RemoveThemesEventArgs e)
        {
            themeLayer.LayersKey = e.Result.LayerKey;
        }

        //移除失败提示错误信息
        private void removeService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }
    }
}
