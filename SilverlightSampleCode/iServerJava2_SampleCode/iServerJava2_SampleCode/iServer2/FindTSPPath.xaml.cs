﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava2_SampleCode
{
    public partial class FindTSPPath : UserControl
    {
        private ElementsLayer elementsLayer;
        private Point2DCollection points = new Point2DCollection();
        private FeaturesLayer featuresLayer;
        private int i = 0;

        public FindTSPPath()
        {
            InitializeComponent();
            elementsLayer = this.MyMap.Layers["MyArbitraryLayer"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        //选择路径途经结点
        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            //标记途径结点顺序
            i++;

            //将结点以图钉样式加到ElementsLayer上
            Pushpin pushpin = new Pushpin();
            pushpin.Location = e.Geometry.Bounds.Center;
            pushpin.Content = i.ToString();
            elementsLayer.AddChild(pushpin);

            //用points数组记录结点坐标
            points.Add(pushpin.Location);
        }

        private void PathAnalyst_Click(object sender, RoutedEventArgs e)
        {
            TSPPathParam pathParam = new TSPPathParam();

            //设置权重字段列表
            WeightFieldInfo weightFieldInfo = new WeightFieldInfo { Name = "length", TFWeightField = "SmLength", FTWeightField = "SmLength" };
            List<WeightFieldInfo> list = new List<WeightFieldInfo>();
            list.Add(weightFieldInfo);

            //设置网络分析模型
            NetworkModelSetting networkModelSetting = new NetworkModelSetting
            {
                NetworkDatasetName = "RoadNet",
                NetworkDataSourceName = "changchun",
                Tolerance = 300,
                NodeIDField = "SMNODEID",
                EdgeIDField = "SmID",
                WeightFieldInfos = list,
                TNodeIDField = "SMTNODE",
                FNodeIDField = "SMFNODE",
            };

            //设置网络分析参数
            NetworkAnalystParam networkAnalystParam = new NetworkAnalystParam
            {
                Point2Ds = points,
                IsPathsReturn = true,
                WeightName = "length",

            };

            //设置旅行商参数，判断是否指定终点  
            if (terminal.IsChecked == true)
            {
                pathParam = new TSPPathParam
                {
                    NetworkAnalystParam = networkAnalystParam,
                    IsEndNodeAssigned = true

                };
            }
            else
            {
                pathParam = new TSPPathParam
                {
                    NetworkAnalystParam = networkAnalystParam,

                };
            }


            //设置旅行商分析参数
            FindTSPPathParameters parameters = new FindTSPPathParameters
            {
                MapName = "Changchun",
                NetworkSetting = networkModelSetting,
                TSPPathParam = pathParam
            };

            //与服务器交换
            FindTSPPathService service = new FindTSPPathService("http://localhost:7080/demo");
            service.ProcessAsync(parameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<NetworkAnalystEventArgs>(service_ProcessCompleted);
        }

        //服务器返回结果，将最佳路径显示在客户端
        private void service_ProcessCompleted(object sender, NetworkAnalystEventArgs e)
        {
            foreach (ServerGeometry p in e.Result.Paths)
            {
                PredefinedLineStyle simpleLineStyle = new PredefinedLineStyle();
                simpleLineStyle.Stroke = new SolidColorBrush(Colors.Blue);
                simpleLineStyle.StrokeThickness = 2;

                //显示返回的路径
                Feature path = new Feature();
                path.Geometry = p.ToGeometry();
                path.Style = simpleLineStyle;
                featuresLayer.AddFeature(path);
            }

        }

        //服务器计算失败提示失败信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //清除ElementsLayer和FeaturesLayer图层上全部元素
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();
            points.Clear();
            i = 0;
        }
    }
}
