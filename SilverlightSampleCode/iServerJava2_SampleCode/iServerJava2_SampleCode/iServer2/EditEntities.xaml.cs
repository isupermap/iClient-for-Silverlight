﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava2_SampleCode
{
    public partial class EditEntities : UserControl
    {
        private FeaturesLayer drawLayer;

        private QueryLayerParam qureyLayerParam = new QueryLayerParam { Name = "Vegetable@changchun" };
        private int entityID;
        private List<VegetableData> listVegetable;
        private ServerGeometry shape;
        private IList<string> getEntityFieldNames;
        private List<int> ids = new List<int>();
        private List<ServerGeometry> unionGeometry = new List<ServerGeometry>();

        //Editflag 用来判断是否编辑过地物的逻辑参数
        private bool Editflag = false;

        //该类与"Vegetable"数据集的属性数据字段对应，用于记录属性值，并实现与DataGrid绑定
        public class VegetableData
        {
            public string SmID { get; set; }
            public string SmUserID { get; set; }
            public string SmArea { get; set; }
            public string SmPerimeter { get; set; }
            public string ClassID { get; set; }
        }

        public EditEntities()
        {
            InitializeComponent();
            drawLayer = MyMap.Layers["DrawLayer"] as FeaturesLayer;
        }

        //在图层中添加新地物
        private void AddEntity_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon polygon = new DrawPolygon(MyMap);
            MyMap.Action = polygon;
            polygon.DrawCompleted += new EventHandler<DrawEventArgs>(polygon_DrawCompleted);
        }

        //将地物添加到图层中
        private void polygon_DrawCompleted(object sender, DrawEventArgs e)
        {
            //将绘制的地物显示在FeaturesLayer中
            GeoRegion region = e.Geometry as GeoRegion;
            Feature f = new Feature();
            f.Geometry = region;
            f.Style = new PredefinedFillStyle() { Fill = new SolidColorBrush(Colors.Green) };
            drawLayer.Features.Add(f);

            //记录所绘制地物的点坐标
            Point2DCollection ps = new Point2DCollection();
            for (int i = 0; i < region.Parts.Count; i++)
            {
                for (int j = 0; j < region.Parts[i].Count; j++)
                {
                    ps.Add(new Point2D(region.Parts[i][j].X, region.Parts[i][j].Y));
                }
            }

            //将客户端地物转换成服务端几何类型
            ServerGeometry Addshape = new ServerGeometry
           {
               Feature = ServerFeatureType.Polygon,
               Point2Ds = ps
           };

            Entity entity = new Entity
            {
                Shape = Addshape
            };

            AddEntityParameters parameters = new AddEntityParameters
            {
                MapName = "Changchun",
                LayerName = "Vegetable@Changchun",
                Entity = entity
            };

            //与服务器交互
            AddEntityService editService = new AddEntityService("http://localhost:7080/demo");
            editService.ProcessAsync(parameters);
            editService.Failed += new EventHandler<ServiceFailedEventArgs>(Service_Failed);
            editService.ProcessCompleted += new EventHandler<EditEventArgs>(Service_ProcessCompleted);
        }

        //与服务器交换成功
        private void Service_ProcessCompleted(object sender, EditEventArgs e)
        {
            MessageBox.Show("成功编辑地物");
            MyMap.Layers["MyLayer"].Refresh();
        }

        //与服务器交换失败提示失败信息
        private void Service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //选择地物
        private void SelectEntity_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint selectEntity = new DrawPoint(MyMap, Cursors.Arrow);
            MyMap.Action = selectEntity;
            selectEntity.DrawCompleted += new EventHandler<DrawEventArgs>(selectEntity_DrawCompleted);
        }

        private void selectEntity_DrawCompleted(object sender, DrawEventArgs e)
        {
            //如果存在编辑过的地物，则不显示编辑过的地物
            if (Editflag == true)
            {
                drawLayer.ClearFeatures();
                dgVegetable.Visibility = Visibility.Collapsed;
                Editflag = false;
            }

            //查询鼠标点击处地物
            QueryByCenterParameters parameters = new QueryByCenterParameters
            {
                CenterPoint = e.Geometry.Bounds.Center,
                Tolerance = 5,
                MapName = "Changchun",
                IsNearest = true,
                QueryParam = new QueryParam { QueryLayerParams = new List<QueryLayerParam>() { qureyLayerParam } }
            };
            QueryByCenterService selectQuery = new QueryByCenterService("http://localhost:7080/demo");
            selectQuery.ProcessAsync(parameters);
            selectQuery.Failed += (s, args) => { MessageBox.Show(args.Error.ToString()); };
            selectQuery.ProcessCompleted += new EventHandler<QueryEventArgs>(selectQuery_ProcessCompleted);
        }

        private void selectQuery_ProcessCompleted(object sender, QueryEventArgs e)
        {
            //点击处没有地物的情况
            if (e.ResultSet == null)
            {
                MessageBox.Show("查询结果为空！");
                // dgVegetable.Visibility = Visibility.Collapsed;
                return;
            }

            //点击处存在地物时，用蓝色边显示该地物
            PredefinedFillStyle selectStyle = new PredefinedFillStyle();
            selectStyle.Stroke = new SolidColorBrush(Colors.Blue);
            FeatureCollection fc = e.ResultSet.RecordSets[0].ToFeatureSet();
            Feature f = new Feature { Geometry = fc[0].Geometry, Style = selectStyle };
            drawLayer.Features.Add(f);

            //记录所选地物的id号，为删除地物准备
            ids.Add(e.ResultSet.RecordSets[0].Records[0].Shape.Id);

            //记录所选地物的几何形状，为合并地物准备
            unionGeometry.Add(e.ResultSet.RecordSets[0].Records[0].Shape);
        }

        //编辑地物
        private void EditEntity_Click(object sender, RoutedEventArgs e)
        {
            Editflag = true;
            DrawPoint selectEntity = new DrawPoint(MyMap, Cursors.Arrow);
            MyMap.Action = selectEntity;
            selectEntity.DrawCompleted += new EventHandler<DrawEventArgs>(ModifyEntity_DrawCompleted);
        }

        private void ModifyEntity_DrawCompleted(object sender, DrawEventArgs e)
        {
            //清除前一次选中地物
            drawLayer.Features.Clear();

            //查询当次鼠标点击地物
            queryEntity(e.Geometry);
        }

        //中心点查询功能查询地物
        private void queryEntity(SuperMap.Web.Core.Geometry entity)
        {
            QueryByCenterParameters parameters = new QueryByCenterParameters
            {
                CenterPoint = entity.Bounds.Center,
                Tolerance = 5,
                MapName = "Changchun",
                IsNearest = true,
                QueryParam = new QueryParam { QueryLayerParams = new List<QueryLayerParam>() { qureyLayerParam } }
            };
            QueryByCenterService query = new QueryByCenterService("http://localhost:7080/demo");
            query.ProcessAsync(parameters);
            query.Failed += (s, args) => { MessageBox.Show(args.Error.ToString()); };
            query.ProcessCompleted += new EventHandler<QueryEventArgs>(query_ProcessCompleted);
        }

        //查询成功高亮显示选中地物，并显示相关属性信息
        private void query_ProcessCompleted(object sender, QueryEventArgs e)
        {
            //点击处没有地物的情况
            if (e.ResultSet == null)
            {
                MessageBox.Show("查询结果为空！");
                dgVegetable.Visibility = Visibility.Collapsed;
                return;
            }

            //点击处存在地物时，用蓝色边显示该地物
            PredefinedFillStyle selectStyle = new PredefinedFillStyle();
            selectStyle.Stroke = new SolidColorBrush(Colors.Blue);
            FeatureCollection fc = e.ResultSet.RecordSets[0].ToFeatureSet();
            Feature f = new Feature { Geometry = fc[0].Geometry, Style = selectStyle };
            drawLayer.Features.Add(f);

            //记录地物ID号，为设置GetEntityParameters做准备
            entityID = e.ResultSet.RecordSets[0].Records[0].Shape.Id;

            GetEntityParameters parameters = new GetEntityParameters
            {
                ID = entityID,
                MapName = "Changchun",
                LayerName = "Vegetable@changchun"
            };
            GetEntityService getEntityService = new GetEntityService("http://localhost:7080/demo");
            getEntityService.ProcessAsync(parameters);
            getEntityService.ProcessCompleted += new EventHandler<GetEntityEventArgs>(getEntityService_ProcessCompleted);
        }

        //获取选中地物属性
        private void getEntityService_ProcessCompleted(object sender, GetEntityEventArgs e)
        {
            //隐藏DataGrid
            dgVegetable.Visibility = Visibility.Collapsed;

            //将得到的地物当前属性与DataGrid绑定
            listVegetable = new List<VegetableData>();
            listVegetable.Add(new VegetableData()
            {
                SmID = e.Result.FieldValues[0],
                SmUserID = e.Result.FieldValues[1],
                SmArea = e.Result.FieldValues[2],
                SmPerimeter = e.Result.FieldValues[3],
                ClassID = e.Result.FieldValues[4]
            });
            dgVegetable.ItemsSource = listVegetable;

            //显示DataGrid，由于SmID、SmArea（面积）、SmPerimeter（周长）不能修改故设置这三项为只读
            dgVegetable.Visibility = Visibility.Visible;
            dgVegetable.Columns[0].IsReadOnly = true;
            dgVegetable.Columns[2].IsReadOnly = true;
            dgVegetable.Columns[3].IsReadOnly = true;

            //获取选中地物的几何信息、属性表字段和地物ID号，为更新地物做准备
            entityID = e.Result.ID;
            getEntityFieldNames = e.Result.FieldNames;
            shape = e.Result.Shape;

            //编辑地物形状
            SuperMap.Web.Actions.Edit editEntity = new SuperMap.Web.Actions.Edit(MyMap, drawLayer);
            MyMap.Action = editEntity;
            editEntity.GeometryEdit += new EventHandler<SuperMap.Web.Actions.Edit.GeometryEditEventArgs>(editEntity_GeometryEdit);
        }

        //编辑地物形状
        private void editEntity_GeometryEdit(object sender, SuperMap.Web.Actions.Edit.GeometryEditEventArgs e)
        {

            if (e.Action == SuperMap.Web.Actions.Edit.GeometryEditAction.EditCompleted)
            {
                Point2DCollection ps = new Point2DCollection();

                //记录编辑后的地物结点
                GeoRegion region = e.Feature.Geometry as GeoRegion;
                for (int i = 0; i < region.Parts.Count; i++)
                {
                    for (int j = 0; j < region.Parts[i].Count; j++)
                    {
                        ps.Add(new Point2D(region.Parts[i][j].X, region.Parts[i][j].Y));
                    }
                }

                shape = new ServerGeometry
                {
                    Feature = ServerFeatureType.Polygon,
                    Point2Ds = ps
                };
                Entity entity = new Entity
                {
                    Shape = shape,
                    ID = entityID
                };
                UpdateEntityParameters parameters = new UpdateEntityParameters
                {
                    MapName = "Changchun",
                    LayerName = "Vegetable@changchun",
                    Entity = entity
                };

                //与服务器交互更新地物形状
                UpdateEntityService updateEntityService = new UpdateEntityService("http://localhost:7080/demo");
                updateEntityService.ProcessAsync(parameters);
                updateEntityService.Failed += new EventHandler<ServiceFailedEventArgs>(Service_Failed);
                updateEntityService.ProcessCompleted += new EventHandler<EditEventArgs>(Service_ProcessCompleted);
            }
        }

        //属性编辑结束，提交新的属性信息
        private void dgVegetable_RowEditEnded(object sender, DataGridRowEditEndedEventArgs e)
        {
            List<string> getEntityFieldValues = new List<string>();
            if (dgVegetable != null)
            {
                getEntityFieldValues.Add(listVegetable[0].SmID);
                getEntityFieldValues.Add(listVegetable[0].SmUserID);
                getEntityFieldValues.Add(listVegetable[0].SmPerimeter);
                getEntityFieldValues.Add(listVegetable[0].SmArea);
                getEntityFieldValues.Add(listVegetable[0].ClassID);
            }

            //FieldNames与FieldValues必须一一对应
            Entity entity = new Entity
            {
                Shape = shape,
                ID = entityID,
                FieldNames = getEntityFieldNames,
                FieldValues = getEntityFieldValues
            };
            UpdateEntityParameters parameters = new UpdateEntityParameters
            {
                MapName = "Changchun",
                LayerName = "Vegetable@changchun",
                Entity = entity
            };

            //与服务器交互更新地物属性
            UpdateEntityService updateEntityService = new UpdateEntityService("http://localhost:7080/demo");
            updateEntityService.ProcessAsync(parameters);
            updateEntityService.Failed += new EventHandler<ServiceFailedEventArgs>(Service_Failed);
            updateEntityService.ProcessCompleted += new EventHandler<EditEventArgs>(Service_ProcessCompleted);
        }

        //删除选中地物
        private void DeleteEntity_Click(object sender, RoutedEventArgs e)
        {
            DeleteEntityParameters parameters = new DeleteEntityParameters
            {
                MapName = "Changchun",
                LayerName = "Vegetable@changchun",
                IDs = ids
            };
            DeleteEntityService deleteEntityService = new DeleteEntityService("http://localhost:7080/demo");
            deleteEntityService.ProcessAsync(parameters);
            deleteEntityService.Failed += new EventHandler<ServiceFailedEventArgs>(Service_Failed);
            deleteEntityService.ProcessCompleted += new EventHandler<EditEventArgs>(deleteService_ProcessCompleted);
        }

        //删除成功返回提示信息，并清除featuresLayer上的要素
        private void deleteService_ProcessCompleted(object sender, EditEventArgs e)
        {
            MessageBox.Show(e.Result.Succeed.ToString());
            drawLayer.ClearFeatures();

            //隐藏DataGrid
            dgVegetable.Visibility = Visibility.Collapsed;
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan p = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = p;
        }

        //清除选中地物
        private void clear_Click(object sender, RoutedEventArgs e)
        {
            drawLayer.ClearFeatures();
            dgVegetable.Visibility = Visibility.Collapsed;
            unionGeometry.Clear();
            ids.Clear();

        }

        //合并选中地物
        private void UnionEntity_Click(object sender, RoutedEventArgs e)
        {
            SpatialOperateParameters spatialOper = new SpatialOperateParameters
            {
                SpatialOperationType = SpatialOperationType.Union,
                MapName = "Changchun"
            };

            //所选地物只有一个时
            if (unionGeometry.Count < 2)
            {
                MessageBox.Show("请选择两个地物进行合并！");
            }
            //选中多个地物时，只将最后两个被选择地物合并
            else
            {
                spatialOper.SourceGeometry = unionGeometry[unionGeometry.Count - 2];
                spatialOper.OperatorGeometry = unionGeometry[unionGeometry.Count - 1];
            }

            //与服务器交互合并地物
            SpatialOperateService spatialService = new SpatialOperateService("http://localhost:7080/demo");
            spatialService.ProcessAsync(spatialOper);
            spatialService.Failed += (s, args) => { MessageBox.Show(args.Error.ToString()); };
            spatialService.ProcessCompleted += new EventHandler<SpatialOperateEventArgs>(spatialService_ProcessCompleted);
        }

        private void spatialService_ProcessCompleted(object sender, SpatialOperateEventArgs e)
        {
            //清除图层中选中要素
            drawLayer.ClearFeatures();

            if (e.Result == null)
            {
                MessageBox.Show("No Result!");
                return;
            }

            //将合并后的形状绘制到图层中
            Feature f = new Feature
            {
                Geometry = e.Result.ToGeometry(),
                Style = (SuperMap.Web.Core.Style)App.Current.Resources["CustomDiagonalBlackHatchFillSymbol"]
            };
            drawLayer.Features.Add(f);

            //将合并后的形状更新到服务器
            Entity entity = new Entity
            {
                Shape = e.Result,
                ID = ids[ids.Count - 2]
            };
            UpdateEntityParameters parameters = new UpdateEntityParameters
            {
                MapName = "Changchun",
                LayerName = "Vegetable@changchun",
                Entity = entity
            };
            UpdateEntityService updateEntityService = new UpdateEntityService("http://localhost:7080/demo");
            updateEntityService.ProcessAsync(parameters);
            updateEntityService.Failed += new EventHandler<ServiceFailedEventArgs>(Service_Failed);
            updateEntityService.ProcessCompleted += new EventHandler<EditEventArgs>(Service_ProcessCompleted);
        }
    }
}
