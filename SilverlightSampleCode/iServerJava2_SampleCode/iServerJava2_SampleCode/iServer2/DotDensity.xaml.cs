﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava2_SampleCode
{
    public partial class DotDensity : UserControl
    {
        private List<string> listLayerName = new List<string>();

        //定义专题图层
        private TiledDynamicIServerLayer themeLayer;
        public DotDensity()
        {
            InitializeComponent();
            themeLayer = MyMap.Layers["IServerLayer"] as TiledDynamicIServerLayer;
        }

        //制作点密度专题图
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            //如果字段表达式为空则用下拉列表中选择字段来制作单值专题图
            ThemeDotDensity theme = new ThemeDotDensity()
            {
                DotExpression = "Pop_1994",
                Value = 11281396.89000,
                Style = new ServerStyle()
                {
                    MarkerSize = 3,
                    LineColor = new ServerColor { Blue = 0, Green = 255, Red = 0 }
                }
            };

            ThemeParameters parameters = new ThemeParameters
            {
                MapName = "World",
                LayerName = "World@world" ,
                Theme = theme,
            };

            //与服务器交互
            ThemeService themeService = new ThemeService("http://localhost:7080/demo");
            themeService.ProcessAsync(parameters);
            themeService.Failed += new EventHandler<ServiceFailedEventArgs>(themeService_Failed);
            themeService.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeService_ProcessCompleted);
        }

        private void themeService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            //服务器处理成功获取传回的专题图key值，唯一对应一个专题图的状态
            themeLayer.LayersKey = e.Result.Key;
            //记录已加载的专题图的名字，为删除专题图做准备
            string layername = e.Result.Name;
            listLayerName.Add(layername);
        }

        //服务器处理失败提示错误信息
        private void themeService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //清除专题图现有状态
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            RemoveThemesParameters parameters = new RemoveThemesParameters
            {
                MapName = "World",
                LayerNames = listLayerName
            };
            RemoveThemesService removeThemeLayerService = new RemoveThemesService("http://localhost:7080/demo");
            removeThemeLayerService.ProcessAsync(parameters);
            removeThemeLayerService.Failed += new EventHandler<ServiceFailedEventArgs>(removeThemeLayerService_Failed);
            removeThemeLayerService.ProcessCompleted += new EventHandler<RemoveThemesEventArgs>(removeThemeLayerService_ProcessCompleted);
        }

        //移除成功则获取服务器返回的新的key值，从而专题图图层状态变为空专题图,即只有底图
        private void removeThemeLayerService_ProcessCompleted(object sender, RemoveThemesEventArgs e)
        {
            themeLayer.LayersKey = e.Result.LayerKey;
        }

        //移除失败提示错误信息
        private void removeThemeLayerService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }
    }
}
