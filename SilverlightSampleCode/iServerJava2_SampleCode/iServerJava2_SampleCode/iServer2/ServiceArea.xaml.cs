﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava2_SampleCode
{
    public partial class ServiceArea : UserControl
    {
        private ElementsLayer elementsLayer;
        private Point2DCollection points = new Point2DCollection();
        private FeaturesLayer featuresLayer;
        private int i = 0;
        List<int> listweights = new List<int>();

        public ServiceArea()
        {
            InitializeComponent();
            elementsLayer = this.MyMap.Layers["MyArbitraryLayer"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        //选择服务区站点
        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            //标记服务区站点
            i++;

            //将站点以图钉样式加到ElementsLayer上
            Pushpin pushpin = new Pushpin();
            pushpin.Location = e.Geometry.Bounds.Center;
            pushpin.Content = i.ToString();
            pushpin.Background = new SolidColorBrush(Colors.Red);
            elementsLayer.AddChild(pushpin);
            //用points数组记录结点坐标
            points.Add(pushpin.Location);

            //用户输入服务区半径
            InputWindow weightWindow = new InputWindow();
            weightWindow.Show();
            weightWindow.Closed += new EventHandler(weightWindow_Closed);

        }

        private void weightWindow_Closed(object sender, EventArgs e)
        {
            InputWindow weightWindow = sender as InputWindow;
            listweights.Add(Convert.ToInt32(weightWindow.Tag));
        }

        private void PathAnalyst_Click(object sender, RoutedEventArgs e)
        {
            //设置权重字段列表
            WeightFieldInfo weightFieldInfo = new WeightFieldInfo { Name = "length", TFWeightField = "SmLength", FTWeightField = "SmLength" };
            List<WeightFieldInfo> list = new List<WeightFieldInfo>();
            list.Add(weightFieldInfo);

            //设置网络分析模型
            NetworkModelSetting networkModelSetting = new NetworkModelSetting
            {
                NetworkDatasetName = "RoadNet",
                NetworkDataSourceName = "changchun",
                Tolerance = 300,
                NodeIDField = "SMNODEID",
                EdgeIDField = "SmID",
                WeightFieldInfos = list,
                TNodeIDField = "SMTNODE",
                FNodeIDField = "SMFNODE",
            };

            //设置网络分析参数
            NetworkAnalystParam networkAnalystParam = new NetworkAnalystParam
            {
                Point2Ds = points,
                //IsPathsReturn = true,
                WeightName = "length",
            };

            //设置服务区分析子参数
            ServiceAreaParam serviceArea = new ServiceAreaParam
            {
                NetworkAnalystParam = networkAnalystParam,
                Weights = listweights
            };

            //设置服务区分析参数
            ServiceAreaParameters parameters = new ServiceAreaParameters
            {
                MapName = "Changchun",
                NetworkSetting = networkModelSetting,
                ServiceAreaParam = serviceArea
            };

            //与服务器交互
            ServiceAreaService service = new ServiceAreaService("http://localhost:7080/demo");
            service.ProcessAsync(parameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ServiceAreaEventArgs>(service_ProcessCompleted);
        }

        private void service_ProcessCompleted(object sender, ServiceAreaEventArgs e)
        {
            foreach (ServerGeometry p in e.Result.AreaRegions)
            {
                //显示服务区域
                FillStyle fillStyle = new FillStyle();
                fillStyle.Fill = new SolidColorBrush(Color.FromArgb(120, 179, 235, 246));
                Feature area = new Feature();
                area.Geometry = p.ToGeometry();
                area.Style = fillStyle;
                featuresLayer.AddFeature(area);
            }
        }

        //服务器计算失败提示失败信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //清除ElementsLayer和FeaturesLayer图层上全部元素
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();
            points.Clear();
            listweights.Clear();
            i = 0;
        }
    }
}
