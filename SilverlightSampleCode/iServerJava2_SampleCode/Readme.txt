   注意：如果您尚未安装 Microsoft Silverlight 5 Toolkit，在运行 SampleCode 时，会出现缺少引用的错误，所缺少的 dll 显示为黄色叹号。
   解决上述问题：
     方法一：打开工程“添加引用”，引用内容为该工程文件夹下的 Lib 文件夹中对应黄色叹号的 dll。
     方法二：安装 Microsoft Silverlight 5 Toolkit 。
