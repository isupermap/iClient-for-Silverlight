﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class GetFeaturesBySQLTest : Page
    {
        private const string url = "http://localhost:8090/iserver/services/data-world/rest/data/featureResults";
        private FeaturesLayer flayer;

        public GetFeaturesBySQLTest( )
        {
            InitializeComponent();
            MyTextBox.Text = "SMID<10";
            flayer = MyMap.Layers["FeaturesLayer"] as FeaturesLayer;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        private void GetFeaturesBySQLTest_Click(object sender , RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(MyTextBox.Text) || string.IsNullOrWhiteSpace(MyTextBox.Text))
            {
                MessageBox.Show("请输入查询条件");
                return;
            }
            GetFeaturesBySQLParameters param = new GetFeaturesBySQLParameters
            {
                DatasetNames = new List<string> { "World:Capitals" } , 
                FilterParameter = new SuperMap.Web.iServerJava6R.FilterParameter
                {
                    AttributeFilter = MyTextBox.Text ,
                }
            };
            GetFeaturesBySQLService ser = new GetFeaturesBySQLService(url);
            ser.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(ser_Failed);
            ser.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(ser_ProcessCompleted);
            ser.ProcessAsync(param);
        }

        private void ser_Failed(object sender , SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
        }

        private void ser_ProcessCompleted(object sender , GetFeaturesEventArgs e)
        {
            flayer.ClearFeatures();
            if (e.Result != null)
            {
                flayer.AddFeatureSet(e.Result.Features);
            }
            else
            {
                MessageBox.Show("查询结果为空");
            }
        }
    }
}
