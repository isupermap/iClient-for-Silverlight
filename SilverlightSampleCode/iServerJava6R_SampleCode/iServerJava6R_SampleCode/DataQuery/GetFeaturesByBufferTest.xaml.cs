﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using SuperMap.Web.Actions;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class GetFeaturesByBufferTest : Page
    {
        private FeaturesLayer featureslayer;
        private const string url = "http://localhost:8090/iserver/services/data-world/rest/data/featureResults";

        public GetFeaturesByBufferTest( )
        {
            InitializeComponent();
            featureslayer = MyMap.Layers["FeaturesLayer"] as FeaturesLayer;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        private void Pan_Click(object sender , RoutedEventArgs e)
        {
            MyMap.Action = new Pan(MyMap);
        }

        private void Region_Click(object sender , RoutedEventArgs e)
        {
            DrawPolygon dp = new DrawPolygon(MyMap);
            dp.DrawCompleted += new EventHandler<DrawEventArgs>(dp_DrawCompleted);
            MyMap.Action = dp;
        }

        void dp_DrawCompleted(object sender , DrawEventArgs e)
        {
            if (e.Geometry == null)
            {
                return;
            }
            GetFeaturesByBufferParameters param = new GetFeaturesByBufferParameters
            {
                DatasetNames = new List<string> { "World:Countries" } ,
                BufferDistance = 10 ,
                Geometry = e.Geometry ,
            };
            GetFeaturesByBufferService ser = new GetFeaturesByBufferService(url);
            ser.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(ser_ProcessCompleted);
            ser.ProcessAsync(param);
        }

        void ser_ProcessCompleted(object sender , GetFeaturesEventArgs e)
        {
            if (e.Result != null)
            {
                featureslayer.AddFeatureSet(e.Result.Features);
            }
        }
    }
}
