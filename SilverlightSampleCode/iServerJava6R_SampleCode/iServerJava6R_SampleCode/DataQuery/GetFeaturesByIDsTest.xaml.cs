﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class GetFeaturesByIDsTest : Page
    {
        private const string url = "http://localhost:8090/iserver/services/data-world/rest/data/featureResults";
        private FeaturesLayer flayer;

        public GetFeaturesByIDsTest( )
        {
            InitializeComponent();
            flayer = MyMap.Layers["FeaturesLayer"] as FeaturesLayer;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void GetFeaturesByIDsBtn_Click(object sender , RoutedEventArgs e)
        {
            //用逗号和空格分开的都行。
            string[] str = MyTextBox.Text.Split(',' , ' ');
            List<int> ids = new List<int>();
            for (int i = 0 ; i < str.Length ; i++)
            {
                ids.Add(Convert.ToInt32(str[i]));
            }

            GetFeaturesByIDsParameters param = new GetFeaturesByIDsParameters
            {
                DatasetNames = new List<string> { "World:Capitals" } ,
                IDs = ids
            };
            GetFeaturesByIDsService ser = new GetFeaturesByIDsService(url);
            ser.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(ser_ProcessCompleted);
            ser.ProcessAsync(param);
        }

        void ser_ProcessCompleted(object sender , GetFeaturesEventArgs e)
        {
            flayer.ClearFeatures();
            if (e.Result != null)
            {
                flayer.AddFeatureSet(e.Result.Features);
            }
        }

    }
}
