﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using SuperMap.Web.Actions;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class GetFeaturesByGeometryTest : Page
    {
        private const string url = "http://localhost:8090/iserver/services/data-world/rest/data/featureResults";
        private FeaturesLayer featureslayer;

        public GetFeaturesByGeometryTest( )
        {
            InitializeComponent();
            featureslayer = MyMap.Layers["FeaturesLayer"] as FeaturesLayer;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Point_Click(object sender , RoutedEventArgs e)
        {
            DrawPoint dp = new DrawPoint(MyMap);
            dp.DrawCompleted += DrawCompleted;
            MyMap.Action = dp;
        }

        void DrawCompleted(object sender , DrawEventArgs e)
        {
            if (e.Geometry == null)
            {
                return;
            }

            GetFeaturesByGeometryParameters param = new GetFeaturesByGeometryParameters
            {
                DatasetNames = new List<string> { "World:Countries" } ,
                SpatialQueryMode = SuperMap.Web.iServerJava6R.SpatialQueryMode.INTERSECT ,
                Geometry = e.Geometry
            };

            GetFeaturesByGeometryService service = new GetFeaturesByGeometryService(url);
            service.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(service_ProcessCompleted);
            service.ProcessAsync(param);
        }

        void service_ProcessCompleted(object sender , GetFeaturesEventArgs e)
        {
            featureslayer.ClearFeatures();
            if (e.Result != null)
            {
                featureslayer.AddFeatureSet(e.Result.Features);
            }
            else
            {
                MessageBox.Show("查询结果为空!");
            }
        }

        private void Line_Click(object sender , RoutedEventArgs e)
        {
            DrawLine dl = new DrawLine(MyMap);
            dl.DrawCompleted += DrawCompleted;
            MyMap.Action = dl;
        }

        private void Region_Click(object sender , RoutedEventArgs e)
        {
            DrawPolygon dpl = new DrawPolygon(MyMap);
            dpl.DrawCompleted += DrawCompleted;
            MyMap.Action = dpl;
        }

        private void Pan_Click(object sender , RoutedEventArgs e)
        {
            MyMap.Action = new Pan(MyMap);
        }

        private void Clear_Click(object sender , RoutedEventArgs e)
        {
            featureslayer.ClearFeatures();
        }

    }
}
