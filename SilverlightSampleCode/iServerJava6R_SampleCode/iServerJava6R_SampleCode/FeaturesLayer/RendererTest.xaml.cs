﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;
using SuperMap.Web.Rendering;

namespace iServerJava6R_SampleCode
{
    public partial class RendererTest : Page
    {
        private Random random = new Random();
        private string url = "http://localhost:8090/iserver/services/map-world/rest/maps/World Map";
        FeaturesLayer uniqueRenderLayer;
        FeaturesLayer rangeRendererLayer;
        FeaturesLayer uniformRendererLayer;

        public RendererTest( )
        {
            InitializeComponent();
            MyMap.Loaded += new RoutedEventHandler(MyMapControl_Loaded);

            uniqueRenderLayer = MyMap.Layers["uniqueRenderLayer"] as FeaturesLayer;
            rangeRendererLayer = MyMap.Layers["rangeRendererLayer"] as FeaturesLayer;
            uniformRendererLayer = MyMap.Layers["uniformRendererLayer"] as FeaturesLayer;
        }

        private void MyMapControl_Loaded(object sender , RoutedEventArgs e)
        {
            for (int i = 0 ; i < 100 ; i++)
            {
                double x = this.random.Next(-180 , 0);
                double y = this.random.Next(-90 , 90);
                Feature feature2 = new Feature();
                feature2.Geometry = new GeoPoint(x , y);
                Feature feature = feature2;
                feature.Attributes.Add("Ranking" , this.random.NextDouble());
                TextBlock block = new TextBlock();
                block.Text = "Ranking:" + feature.Attributes["Ranking"].ToString();
                block.Foreground = new SolidColorBrush(Colors.Red);
                feature.ToolTip = block;
                rangeRendererLayer.Features.Add(feature);
            }

            DouUiqueRenderSearch();
            DoUniformRendererSearch();
        }

        private void DouUiqueRenderSearch( )
        {
            List<FilterParameter> list = new List<FilterParameter>();

            list.Add(new FilterParameter
            {
                Name = "Countries@World" ,
                AttributeFilter = "SmID=39 OR SmID=45 OR SmID=54"
            });

            QueryBySQLParameters parameters = new QueryBySQLParameters
            {
                FilterParameters = list
            };

            QueryBySQLService service = new QueryBySQLService(this.url);
            service.ProcessAsync(parameters);
            service.ProcessCompleted += new EventHandler<QueryEventArgs>(this.service_ProcessCompleted);
            service.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(service_Failed);
        }

        private void DoUniformRendererSearch( )
        {
            List<FilterParameter> list = new List<FilterParameter>();

            list.Add(new FilterParameter
            {
                Name = "Countries@World" ,
                AttributeFilter = "SmID=43 OR SmID=78 OR SmID=71"
            });

            QueryBySQLParameters parameters = new QueryBySQLParameters
            {
                FilterParameters = list
            };

            QueryBySQLService service1 = new QueryBySQLService(this.url);
            service1.ProcessAsync(parameters);
            service1.ProcessCompleted += new EventHandler<QueryEventArgs>(service1_ProcessCompleted);
            service1.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(service_Failed);
        }

        private void service_ProcessCompleted(object sender , QueryEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("没有查询到结果!");
                return;
            }

            int i = 0;
            foreach (Recordset item in e.Result.Recordsets)
            {
                foreach (Feature feature in item.Features)
                {
                    feature.ToolTip = new TextBlock { Text = i++.ToString(System.Globalization.CultureInfo.InvariantCulture) };
                    uniqueRenderLayer.Features.Add(feature);
                }
            }
        }

        private void service1_ProcessCompleted(object sender , QueryEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("没有查询到结果!");
                return;
            }

            foreach (Recordset item in e.Result.Recordsets)
            {
                foreach (Feature feature in item.Features)
                {
                    uniformRendererLayer.Features.Add(feature);
                }
            }

            uniformRendererLayer.Renderer = new UniformRenderer
            {
                FillStyle = new FillStyle
                {
                    Fill = new SolidColorBrush(Color.FromArgb(0xff , 0xe5 , 0xe5 , 0x45)) ,
                    Stroke = new SolidColorBrush(Color.FromArgb(0xff , 0x7f , 0x80 , 0x13))
                }
            };
        }

        private void service_Failed(object sender , SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
            MessageBox.Show("没有查询到结果!");
        }
    }
}
