﻿using System;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using System.Collections.ObjectModel;

namespace iServerJava6R_SampleCode
{
    public partial class Cluster : Page
    {
        private const double ChinaLeft = 70;
        private const double ChinaRight = 130;
        private const double ChinaBottom = 4;
        private const double ChinaTop = 50;
        private Random random = new Random();
        private FeaturesLayer featuresLayer;
        private FeaturesLayer featuresLayer1;
        private Point2DCollection point2Ds = new Point2DCollection();
        private Feature region;

        public Cluster()
        {
            InitializeComponent();
            this.MyMap.ViewBoundsChanged += new EventHandler<SuperMap.Web.Mapping.ViewBoundsEventArgs>(Map_ViewBoundsChanged);
            for (int i = -175; i <= 175; i += 10)
            {
                for (int j = -85; j <= 85; j += 10)
                {
                    point2Ds.Add(new Point2D(i, j));
                }
            }
        }

        //根据地图缩放，实现点的聚类功能
        private void Map_ViewBoundsChanged(object sender, SuperMap.Web.Mapping.ViewBoundsEventArgs e)
        {
            if (e.OldViewBounds.IsEmpty)
            {
                featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
                featuresLayer1 = this.MyMap.Layers["MyFeaturesLayer1"] as FeaturesLayer;
                featuresLayer.Clusterer = myScatterClusterer;

                //随机生成点
                for (int i = 0; i < 500; i++)
                {
                    double x = random.NextDouble() * (ChinaRight - ChinaLeft) + ChinaLeft;
                    double y = random.NextDouble() * (ChinaTop - ChinaBottom) + ChinaBottom;
                    Feature feature = new Feature();
                    feature.Geometry = new GeoPoint(x, y);
                    feature.Style = MediumMarkerStyle;
                    feature.ToolTip = new TextBlock() { Text = i.ToString(System.Globalization.CultureInfo.InvariantCulture) };
                    featuresLayer.Features.Add(feature);
                }
                featuresLayer.Clusterer = myScatterClusterer;

                region = new Feature
                {
                    Geometry = new GeoRegion
                    {
                        Parts = new ObservableCollection<Point2DCollection>
                           {
                               new Point2DCollection
                               {
                                   new Point2D{ X=100,Y=20},
                                   new Point2D{ X=120,Y=20},
                                   new Point2D{ X=120,Y=30},
                                   new Point2D{ X=100,Y=30},
                                   new Point2D{ X=100,Y=20},
                               }
                           }
                    }
                };
            }
        }
        private void scatterClusterer_Checked(object sender, RoutedEventArgs e)
        {
            if (myScatterClusterer != null)
            {
                featuresLayer.Clusterer = myScatterClusterer;
                AddFeature();
            }
        }

        private void sparkClusterer_Checked(object sender, RoutedEventArgs e)
        {
            if (mySparkClusterer != null)
            {
                featuresLayer.Clusterer = mySparkClusterer;
                AddFeature();
            }
        }

        private void NonClusterer_Checked(object sender, RoutedEventArgs e)
        {
            featuresLayer.Clusterer = null;
            featuresLayer1.Features.Clear();
        }
        //区域聚散
        private void regioncluster_Checked(object sender, RoutedEventArgs e)
        {
            if (mySparkClusterer != null && myScatterClusterer != null)
            {
                //设置区域聚散的几何面积
                mySparkClusterer.RegionCollection = new ObservableCollection<GeoRegion>{ new GeoRegion
                {
                    Parts = new ObservableCollection<Point2DCollection>
                            {
                                new Point2DCollection
                                {
                                    new Point2D{ X=100,Y=20},
                                    new Point2D{ X=120,Y=20},
                                    new Point2D{ X=120,Y=30},
                                    new Point2D{ X=100,Y=30},
                                    new Point2D{ X=100,Y=20},
                                }
                            }
                }};
                myScatterClusterer.RegionCollection = new ObservableCollection<GeoRegion>{ new GeoRegion
                {
                    Parts = new ObservableCollection<Point2DCollection>
                            {
                                new Point2DCollection
                                {
                                    new Point2D{ X=100,Y=20},
                                    new Point2D{ X=120,Y=20},
                                    new Point2D{ X=120,Y=30},
                                    new Point2D{ X=100,Y=30},
                                    new Point2D{ X=100,Y=20},
                                }
                            }
                }};
                AddFeature();
            }
        }

        private void regioncluster_unChecked(object sender, RoutedEventArgs e)
        {
            if (mySparkClusterer != null && myScatterClusterer != null)
            {
                mySparkClusterer.RegionCollection = null;
                myScatterClusterer.RegionCollection = null;
                this.featuresLayer1.Features.Clear();
            }
        }

        private void AddFeature()
        {
            if (!featuresLayer1.Features.Contains(region) && regioncluster.IsChecked == true)
            {
                featuresLayer1.Features.Add(region);
            }
        }
    }
}

