﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RFindTSPPath : Page
    {
        private ElementsLayer elementsLayer;
        private FeaturesLayer featuresLayer;
        private List<Point2D> points = new List<Point2D>();
        private int i = 0;

        public iServerJava6RFindTSPPath()
        {
            InitializeComponent();
            elementsLayer = this.MyMap.Layers["MyElementsLayer"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            //标记途经结点顺序
            i++;

            //将结点以图钉样式加到ElementsLayer上
            Pushpin pushpin = new Pushpin();
            pushpin.Location = e.Geometry.Bounds.Center;
            pushpin.Content = i.ToString(System.Globalization.CultureInfo.InvariantCulture);
            elementsLayer.AddChild(pushpin);

            //用points数组记录结点坐标
            points.Add(pushpin.Location);
        }

        private void PathAnalyst_Click(object sender, RoutedEventArgs e)
        {
            if (points.Count == 0)
            {
                MessageBox.Show("请指定途经点");
                return;
            }

            FindTSPPathsParameters<Point2D> paramPoint2D = new FindTSPPathsParameters<Point2D>
            {
                EndNodeAssigned = (bool)terminal.IsChecked,
                Nodes = points,
                Parameter = new TransportationAnalystParameter
                {
                    TurnWeightField = "TurnCost",
                    WeightFieldName = "length",
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true,
                    }
                },                 
            };
            //与服务器交互
            FindTSPPathsService findTSPPathsService = new FindTSPPathsService("http://localhost:8090/iserver/services/components-rest/rest/networkanalyst/RoadNet@Changchun");
            findTSPPathsService.ProcessAsync(paramPoint2D);
            findTSPPathsService.ProcessCompleted += new EventHandler<FindTSPPathsEventArgs>(findTSPPathsService_ProcessCompleted);
            findTSPPathsService.Failed += new EventHandler<ServiceFailedEventArgs>(findTSPPathsService_Failed);
        }

        //服务器返回结果，将最佳路径显示在客户端
        private void findTSPPathsService_ProcessCompleted(object sender, FindTSPPathsEventArgs e)
        {
            //路径样式
            PredefinedLineStyle simpleLineStyle = new PredefinedLineStyle();
            simpleLineStyle.Stroke = new SolidColorBrush(Colors.Blue);
            simpleLineStyle.StrokeThickness = 2;

            if (e.Result != null && e.Result.TSPPathList != null)
            {
                foreach (ServerPath p in e.Result.TSPPathList)
                {
                    //将要素添加到图层上
                    featuresLayer.Features.Add(new Feature { Geometry = p.Route, Style = simpleLineStyle });
                }
            }
        }

        //服务器计算失败提示失败信息
        private void findTSPPathsService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //清除 ElementsLayer 和 FeaturesLayer 图层上全部元素
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();
            points.Clear();
            i = 0;
        }
    }
}
