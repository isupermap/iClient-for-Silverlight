﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using SuperMap.Web.Mapping;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.Service;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.iServerJava6R.SpatialAnalyst;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RRouteLocatorLine : Page
    {
        TiledDynamicRESTLayer layer;
        FeaturesLayer _routeLayer;
        ElementsLayer _elementsLayer;

        string _dataUrl = "http://localhost:8090/iserver/services/data-changchun/rest/data/featureResults";
        string _serviceUrl = "http://localhost:8090/iserver/services/spatialanalyst-changchun/restjsr/spatialanalyst/";

        public iServerJava6RRouteLocatorLine()
        {
            InitializeComponent();

            _routeLayer = MyMap.Layers["RouteLayer"] as FeaturesLayer;
            _elementsLayer = MyMap.Layers["MyElementsLayer"] as ElementsLayer;

            //获取路由对象
            GetFeaturesBySQLParameters param = new GetFeaturesBySQLParameters
            {
                DatasetNames = new List<string> { "Changchun:RouteDT_road" },
                FilterParameter = new SuperMap.Web.iServerJava6R.FilterParameter
                {
                    AttributeFilter = "RouteID = 1690",
                }
            };
            GetFeaturesBySQLService ser = new GetFeaturesBySQLService(_dataUrl);
            ser.Failed += new EventHandler<ServiceFailedEventArgs>(ser_Failed);
            ser.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(ser_ProcessCompleted);
            ser.ProcessAsync(param);

            //起始点以图钉形式显示
            Pushpin pushpin1 = new Pushpin();
            pushpin1.IsEnabled = false;
            pushpin1.Location = new Point2D(4020.0045, -4377.0273);
            pushpin1.Content = "A";
            this._elementsLayer.AddChild(pushpin1);

            Pushpin pushpin2 = new Pushpin();
            pushpin2.IsEnabled = false;
            pushpin2.Location = new Point2D(6214.1838, -4209.9278);
            pushpin2.Content = "B";
            this._elementsLayer.AddChild(pushpin2);
        }

        // 当用户导航到此页面时执行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void ser_ProcessCompleted(object sender, GetFeaturesEventArgs e)
        {
            _routeLayer.Features.Clear();
            foreach (var f in e.Result.Features)
            {
                PredefinedLineStyle style = new PredefinedLineStyle();
                style.StrokeThickness = 5;
                style.Stroke = new SolidColorBrush(Colors.Red);
                style.Symbol = PredefinedLineStyle.LineSymbol.Dash;
                f.Style = style;
                _routeLayer.AddFeature(f);

            }
        }

        void ser_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("查询路由对象失败！");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (_routeLayer.Features.Count > 1)
            {
                _routeLayer.Features.RemoveAt(1);
            }
            if (string.IsNullOrEmpty(StartValue.Text) || string.IsNullOrEmpty(EndValue.Text))
            {
                MessageBox.Show("请输入里程值！");
                return;
            }

            Route route = new Route();
            if (_routeLayer.Features.Count > 0)
            {
                Feature f = _routeLayer.Features[0];
                route = f.Geometry as Route;

            }

            double sValue = 0;
            double.TryParse(StartValue.Text, out sValue);
            double eValue = 0;
            double.TryParse(EndValue.Text, out eValue);

            if (_routeLayer.Features.Count > 0)
            {
                Feature f = _routeLayer.Features[0];
                route = f.Geometry as Route;
            }
            RouteLocatorParameters param = new RouteLocatorParameters();

            param.SourceRoute = route;
            param.Type = LocateType.LINE;
            param.StartMeasure = sValue;
            param.EndMeasure = eValue;
            param.IsIgnoreGap = false;

            RouteLocatorService service = new RouteLocatorService(_serviceUrl);
            service.ProcessAsync(param);
            service.ProcessCompleted += new EventHandler<RouteLocatorEventArgs>(service_ProcessCompleted);

            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
        }

        void service_ProcessCompleted(object sender, RouteLocatorEventArgs e)
        {
            if (e.Result.ResultGeometry is GeoLine)
            {
                Feature f = new Feature();
                PredefinedLineStyle style = new PredefinedLineStyle();
                style.Symbol = PredefinedLineStyle.LineSymbol.Solid;
                style.Stroke = new SolidColorBrush(Colors.Blue);
                style.StrokeThickness = 5;
                f.Geometry = e.Result.ResultGeometry;
                f.Style = style;
                _routeLayer.AddFeature(f);

            }
        }
        void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.ErrorMsg);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            _routeLayer.Features.Remove(_routeLayer.Features[1]);
        }

    }
}
