﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.SpatialAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class OverlayAnalyst : Page
    {
        private FeaturesLayer featuresLayer;
        private const string url1 = "http://localhost:8090/iserver/services/map-jingjin/rest/maps/京津地区土地利用现状图";
        private const string url2 = "http://localhost:8090/iserver/services/spatialanalyst-sample/restjsr/spatialanalyst";
        private TiledDynamicRESTLayer overlayLayer = new TiledDynamicRESTLayer()
        {
            Url = url1,
            //设置图层是否透明,true 表示透明。
            Transparent = true
        };
        public OverlayAnalyst()
        {
            InitializeComponent();
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            featuresLayer.ClearFeatures();
        }

        private void DatasetOverlayAnalyst_Click(object sender, RoutedEventArgs e)
        {
            DatasetOverlayAnalystParameters param = new DatasetOverlayAnalystParameters
            {
                Operation = OverlayOperationType.CLIP ,
                OperateDataset = "Lake_R@Jingjin" ,
                SourceDataset = "Landuse_R@Jingjin" ,
              
            };
            DatasetOverlayAnalystService datasetOverlayAnalystService = new DatasetOverlayAnalystService(url2);
            datasetOverlayAnalystService.ProcessAsync(param);
            datasetOverlayAnalystService.ProcessCompleted += new EventHandler<DatasetOverlayAnalystArgs>(datasetOverlayAnalystService_ProcessCompleted);
            datasetOverlayAnalystService.Failed += new EventHandler<ServiceFailedEventArgs>(datasetOverlayAnalystService_Failed);
        }

        void datasetOverlayAnalystService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("叠加失败！");
        }

        void datasetOverlayAnalystService_ProcessCompleted(object sender, DatasetOverlayAnalystArgs e)
        {
            foreach (Feature feature in e.Result.Recordset.Features)
            {
                feature.Style = new PredefinedFillStyle
                {
                    StrokeThickness = 1,
                    Fill = new SolidColorBrush
                    {
                        Color = Colors.Magenta,
                        Opacity = 0.8
                    }
                };
            }
            featuresLayer.AddFeatureSet(e.Result.Recordset.Features);
        }
    }
}
