﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RGeometryQueryTest : Page
    {
        private const string url = "http://localhost:8090/iserver/services/components-rest/rest/maps/世界地图";

        //定义一个矢量图层，用于显示查询结果
        private FeaturesLayer flayer;

        //创建一个查询结果高亮图层
        HighlightLayer highlayer = new HighlightLayer(url);

        //判断是否已经加载了高亮图层
        private bool isAdded;

        //判断是否使用高亮图层
        private bool notHighlight;

        public iServerJava6RGeometryQueryTest()
        {
            InitializeComponent();
            flayer = MyMap.Layers["FeaturesLayer"] as FeaturesLayer;
        }

        //绘制点操作，用于点查询
        private void Point_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint point = new DrawPoint(MyMap, Cursors.Arrow);
            MyMap.Action = point;

            //绘制结束调用 DrawCompleted 函数
            point.DrawCompleted += new EventHandler<DrawEventArgs>(DrawCompleted);

        }

        //绘制线操作，用于线查询
        private void Line_Click(object sender, RoutedEventArgs e)
        {
            DrawLine line = new DrawLine(MyMap);
            MyMap.Action = line;

            //绘制结束调用 DrawCompleted 函数
            line.DrawCompleted += new EventHandler<DrawEventArgs>(DrawCompleted);
        }

        //绘制面操作，用于面查询
        private void Region_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon polygon = new DrawPolygon(MyMap);
            MyMap.Action = polygon;

            //绘制结束调用 DrawCompleted 函数
            polygon.DrawCompleted += new EventHandler<DrawEventArgs>(DrawCompleted);
        }

        //几何对象绘制结束触发事件
        private void DrawCompleted(object sender, DrawEventArgs e)
        {
            //隐藏 FeatureDataGrid 控件
            MyDataGrid.Visibility = Visibility.Collapsed;

            //设置几何查询参数，FilterParameters、Geometry 和 SpatialQueryMode 为必设属性
            QueryByGeometryParameters parameter = new QueryByGeometryParameters
            {
                FilterParameters = new List<FilterParameter>() 
                {             
                        new FilterParameter()
                       {
                           Name = "Countries@World",                          
                       }
                },
                Geometry = e.Geometry,
                SpatialQueryMode = SpatialQueryMode.INTERSECT,

                //设置是返回查询结果资源（false）还是返回查询结果记录集（true）
                ReturnContent = notHighlight
            };

            //与服务器交互
            QueryByGeometryService service = new QueryByGeometryService(url);
            service.ProcessAsync(parameter);
            service.ProcessCompleted += new EventHandler<QueryEventArgs>(service_ProcessCompleted);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);

            //当在 FeaturesLayer 上通过鼠标左键点击触发的事件
            flayer.MouseLeftButtonDown += flayer_MouseLeftButtonDown;
        }

        private void flayer_MouseLeftButtonDown(object sender, FeatureMouseButtonEventArgs args)
        {
            //如果矢量已选中，再点击变为非选中状态，反之亦然
            args.Feature.Selected = !args.Feature.Selected;

            //FeatureDataGrid 移动到选中地物记录
            if (args.Feature.Selected)
            {
                MyDataGrid.ScrollIntoView(args.Feature, null);
            }
            args.Handled = true;
        }

        //服务器查询失败
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        private void service_ProcessCompleted(object sender, QueryEventArgs e)
        {
            //无查询结果的情况
            if (e.Result == null)
            {
                MessageBox.Show("查询结果为空！");
                return;
            }

            //如果当前矢量图层中存在上一次的查询地物，则清除
            if (flayer.Features.Count > 0)
            {
                flayer.Features.Clear();
            }

            //隐藏高亮图层
            highlayer.IsVisible = false;

            //当 ReturnContent = false 且有查询结果时，返回结果中的 ResourceInfo 不为空
            //当 ReturnContent = true 且有查询结果时，返回结果中的 Recordsets 不为空，ResourceInfo 为空
            if (e.Result.ResourceInfo != null)
            {
                highlayer.IsVisible = true;

                //获取高亮图片在服务器上存储的资源 ID 号
                highlayer.QueryResultID = e.Result.ResourceInfo.NewResourceID;

                //设置服务端高亮图层的样式
                highlayer.Style = new ServerStyle() { FillForeColor = new Color { R = 0, G = 160, B = 0 }, LineWidth = 0, FillOpaqueRate = 90 };

                //该判断控制只加载一个高亮图层
                if (!isAdded)
                {
                    this.MyMap.Layers.Add(highlayer);
                    isAdded = true;
                }
            }
            else
            {
                //显示 FeatureDataGrid 控件
                MyDataGrid.Visibility = Visibility.Visible;

                //将查询结果记录集中的数据转换为矢量要素并在矢量图层中显示
                foreach (Recordset recordset in e.Result.Recordsets)
                {
                    foreach (Feature f in recordset.Features)
                    {
                        flayer.Features.Add(f);
                    }
                }

                //将查询到的地物赋予 XMAL 中自定义样式
                foreach (Feature item in flayer.Features)
                {
                    if (item.Geometry is GeoLine)
                    {
                        item.Style = LineSelectStyle;
                    }
                    else if (item.Geometry is GeoRegion)
                    {
                        item.Style = SelectStyle;
                    }
                    else
                    {
                        item.Style = new PredefinedMarkerStyle() { Color = new SolidColorBrush(Colors.Blue), Size = 20, Symbol = PredefinedMarkerStyle.MarkerSymbol.Diamond };
                    }
                }
            }
        }

        //界面中下拉框发生改变时触发
        private void mycb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch ((sender as ComboBox).SelectedIndex)
            {
                //当选择"使用高亮图层显示结果"项时
                case 0:
                    notHighlight = false;
                    break;

                //当选择"使用矢量图层显示结果"项时
                case 1:
                    notHighlight = true;
                    break;
            }
        }

        //平移操作
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan p = new Pan(MyMap);
            MyMap.Action = p;
        }

        //清除所有要素、隐藏高亮图层和 FeatureDataGrid 控件
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            flayer.Features.Clear();
            highlayer.IsVisible = false;
            MyDataGrid.Visibility = Visibility.Collapsed;
        }
    }
}
