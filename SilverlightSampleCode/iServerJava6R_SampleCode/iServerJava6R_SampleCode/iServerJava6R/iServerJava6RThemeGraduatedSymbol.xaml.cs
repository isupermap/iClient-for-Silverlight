﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RThemeGraduatedSymbol : Page
    {
        private bool isAdded;
        //世界图层服务地址
        private const string url = "http://localhost:8090/iserver/services/map-world/rest/maps/World Map";

        //定义专题图层
        private TiledDynamicRESTLayer themeLayer = new TiledDynamicRESTLayer()
        {
            Url = url,
            //设置图层是否透明,true 表示透明。
            Transparent = true
        };

        public iServerJava6RThemeGraduatedSymbol()
        {
            InitializeComponent();
        }

        //点击生成专题图触发事件
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            ThemeGraduatedSymbol themeGraduatedSymbol = new ThemeGraduatedSymbol
            {
                BaseValue = 1128139680,
                GraduatedMode = GraduatedMode.SQUAREROOT,
                Flow = new ThemeFlow
                {
                    FlowEnabled = true,
                },
                Expression = "Pop_1994",
                Style = new ThemeGraduatedSymbolStyle
                {
                    PositiveStyle = new ServerStyle
                    {
                        LineColor = Colors.White,
                        MarkerSize = 20,
                        MarkerSymbolID = 5
                    }

                }
            };
            //专题图参数设置
            ThemeParameters themeGraduatedSymbolParams = new ThemeParameters
            {
                //数据集名称
                DatasetName = "Countries",
                //数据源名称
                DataSourceName = "World",
                Themes = new List<Theme> { themeGraduatedSymbol }
            };
            //与服务器交互
            ThemeService themeGraduatedSymbolService = new ThemeService(url);
            themeGraduatedSymbolService.ProcessAsync(themeGraduatedSymbolParams);
            themeGraduatedSymbolService.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeGraduatedSymbolService_ProcessCompleted);
            themeGraduatedSymbolService.Failed += new EventHandler<ServiceFailedEventArgs>(themeGraduatedSymbolService_Failed);
        }

        //与服务器交互失败
        private void themeGraduatedSymbolService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("生成专题图失败！");
        }

        //与服务器交互成功
        private void themeGraduatedSymbolService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            //显示专题图。专题图在服务端为一个资源，每个资源都有一个 ID 号和一个 url
            //要显示专题图即将资源结果的 ID 号赋值给图层的 layersID 属性即可
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
            if (!isAdded)
            {
                //加载专题图图层
                this.MyMap.Layers.Add(themeLayer);
                isAdded = true;
            }
        }

        //移除专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            if (isAdded == false)
            {
                MessageBox.Show("请先添加一幅专题图！");
            }
            else
            {
                themeLayer.ClearTheme();
                isAdded = false;
            }
        }
    }
}
