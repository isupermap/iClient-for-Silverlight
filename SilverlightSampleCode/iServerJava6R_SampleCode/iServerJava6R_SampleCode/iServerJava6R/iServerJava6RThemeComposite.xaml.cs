﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RThemeComposite : Page
    {
        private bool isAdded;
        //世界图层服务地址
        private const string url = "http://localhost:8090/iserver/services/map-world/rest/maps/World Map";
        //定义专题图层
        private TiledDynamicRESTLayer themeLayer = new TiledDynamicRESTLayer()
        {
            Url = url,
            //设置图层是否透明,true 表示透明。 
            Transparent = true
        };

        public iServerJava6RThemeComposite()
        {
            InitializeComponent();
        }

        private void commit_Click(object sender, RoutedEventArgs e)
        {
            //专题图子项数组
            List<ThemeUniqueItem> items = new List<ThemeUniqueItem> 
            {
                //专题图子项
                 new ThemeUniqueItem
                 {
                     //SmID字段值
                     Unique="1", 
                     Style=new ServerStyle
                     {
                         FillForeColor = new Color {R=1,G=128,B=171},
                         LineWidth = 0.1                       
                     },
                     Visible=true,
                 },
                 new ThemeUniqueItem
                 {
                     //SmID字段值
                     Unique="247",
                     Style=new ServerStyle
                     {
                         FillForeColor= new Color {R=192,G=214,B=54},
                         LineWidth = 0.1
                     },
                     Visible=true,
                 }
            };
            //设置其他 SmID 字段显示风格
            ThemeUnique themeUnique = new ThemeUnique
            {
                Items = items,
                UniqueExpression = "SmID",
                DefaultStyle = new ServerStyle
                {
                    FillOpaqueRate = 100,
                    FillForeColor = new Color { R = 250, G = 237, B = 195 },
                    LineWidth = 0.1,
                    FillBackOpaque = true,
                    FillBackColor = Colors.Transparent
                }
            };

            ThemeDotDensity themeDotDensity = new ThemeDotDensity
            {
                //专题图中每一个点所代表的数值
                Value = 10000000,
                //1994年人口字段
                DotExpression = "Pop_1994",
                Style = new ServerStyle
                {
                    //设置点符号
                    MarkerSize = 2,
                    MarkerSymbolID = 1,
                    FillForeColor = System.Windows.Media.Color.FromArgb(255, 0, 180, 150),
                }
            };
            //专题图参数设置
            ThemeParameters compostionThemeParameters = new ThemeParameters
            {
                DatasetName = "Countries",
                DataSourceName = "World",
                Themes = new List<Theme> { themeDotDensity, themeUnique }
            };
            //与服务器交互
            ThemeService compositeService = new ThemeService(url);
            compositeService.ProcessAsync(compostionThemeParameters);
            compositeService.Failed += new EventHandler<ServiceFailedEventArgs>(compositeService_Failed);
            compositeService.ProcessCompleted += new EventHandler<ThemeEventArgs>(compositeService_ProcessCompleted);

        }

        //与服务器交互成功
        private void compositeService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            //显示专题图。专题图在服务端为一个资源，每个资源都有一个 ID 号和一个 url
            //要显示专题图即将资源结果的 ID 号赋值给图层的 layersID 属性即可
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
            if (!isAdded)
            {
                //加载专题图图层
                this.MyMap.Layers.Add(themeLayer);
                isAdded = true;
            }
        }
        //与服务器交互失败
        private void compositeService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("生成专题图失败！");
        }

        //移除专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            if (isAdded == false)
            {
                MessageBox.Show("请先添加一幅专题图！");
            }
            else
            {
                themeLayer.ClearTheme();
                isAdded = false;
            }
        }
    }
}
