﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RServiceArea : Page
    {
        private ElementsLayer elementsLayer;
        private Point2DCollection points = new Point2DCollection();
        private FeaturesLayer featuresLayer;
        private int i = 0;
        List<double> listweights = new List<double>();

        public iServerJava6RServiceArea()
        {
            InitializeComponent();
            elementsLayer = this.MyMap.Layers["MyElementsLayer"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //选择服务区站点
        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            //标记服务区站点;
            i++;

            //将站点以图钉样式加到ElementsLayer上
            Pushpin pushpin = new Pushpin();
            pushpin.Location = e.Geometry.Bounds.Center;
            pushpin.Content = i.ToString(System.Globalization.CultureInfo.InvariantCulture);
            pushpin.Background = new SolidColorBrush(Colors.Red);
            elementsLayer.AddChild(pushpin);

            //用points数组记录结点坐标
            points.Add(pushpin.Location);

            //用户输入服务区半径
            InputWindow weightWindows = new InputWindow();
            weightWindows.Show();
            weightWindows.Closed += new EventHandler(weightWindows_Closed);
        }

        private void weightWindows_Closed(object sender, EventArgs e)
        {
            InputWindow weightWindow = sender as InputWindow;
            listweights.Add(Convert.ToDouble(weightWindow.Tag));
        }

        private void PathAnalyst_Click(object sender, RoutedEventArgs e)
        {
            if (points.Count == 0)
            {
                MessageBox.Show("请指定服务点");
                return;
            }

            //定义 Point2D 类型的参数
            FindServiceAreasParameters<Point2D> paramPoint2D = new FindServiceAreasParameters<Point2D>
            {
                Centers = points,
                Weights = listweights,
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true,
                    },
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost",
                }
            };

            //与服务器交互
            FindServiceAreasService findServiceAreaService = new FindServiceAreasService("http://localhost:8090/iserver/services/components-rest/rest/networkanalyst/RoadNet@Changchun");
            findServiceAreaService.ProcessAsync(paramPoint2D);
            findServiceAreaService.ProcessCompleted += new EventHandler<FindServiceAreasEventArgs>(findServiceAreaService_ProcessCompleted);
            findServiceAreaService.Failed += new EventHandler<ServiceFailedEventArgs>(findServiceAreaService_Failed);
        }

        private void findServiceAreaService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        private void findServiceAreaService_ProcessCompleted(object sender, FindServiceAreasEventArgs e)
        {
            if (e.Result != null && e.Result.ServiceAreaList != null)
            {

                foreach (SuperMap.Web.iServerJava6R.NetworkAnalyst.ServiceArea p in e.Result.ServiceAreaList)
                {
                    //将要素添加到图层
                    PredefinedFillStyle style = new PredefinedFillStyle();
                    style.Fill = new SolidColorBrush(Color.FromArgb(120, 179, 235, 246));
                    Feature area = new Feature();
                    area.Geometry = p.ServiceRegion;
                    area.Style = style;
                    featuresLayer.AddFeature(area);
                }
            }
        }

        //清除 ElementsLayer 和 FeaturesLayer 图层上的全部元素
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();
            points.Clear();
            listweights.Clear();
            i = 0;
        }
    }
}
