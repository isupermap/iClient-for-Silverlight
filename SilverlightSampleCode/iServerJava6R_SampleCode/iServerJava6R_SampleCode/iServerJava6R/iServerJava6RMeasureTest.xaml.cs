﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RMeasureTest : Page
    {
        private const string url = "http://localhost:8090/iserver/services/map-world/rest/maps/世界地图";
        private FeaturesLayer featuresLayer;
        private ElementsLayer elementsLayer;

        public iServerJava6RMeasureTest()
        {
            InitializeComponent();
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            elementsLayer = this.MyMap.Layers["MyElementsLayer"] as ElementsLayer;
        }

        private void mybtn_Click(object sender, RoutedEventArgs e)
        {
            this.featuresLayer.ClearFeatures();
            this.elementsLayer.Children.Clear();

            DrawLine line = new DrawLine(MyMap);
            MyMap.Action = line;
            line.DrawCompleted += new EventHandler<DrawEventArgs>(line_DrawCompleted);
        }
        private void line_DrawCompleted(object sender, DrawEventArgs e)
        {
            if (e.Geometry == null)
            {
                return;
            }

            //将线标绘在客户端要素图层
            PredefinedLineStyle lineStyle = new PredefinedLineStyle { Stroke = new SolidColorBrush(Colors.Red), StrokeThickness = 3 };
            Feature feature = new Feature
            {
                Geometry = e.Geometry,
                Style = lineStyle
            };
            featuresLayer.Features.Add(feature);

            Measure(e.Geometry);
        }

        private void mybtn2_Click(object sender, RoutedEventArgs e)
        {
            this.featuresLayer.ClearFeatures();
            this.elementsLayer.Children.Clear();

            DrawPolygon polygon = new DrawPolygon(MyMap);
            MyMap.Action = polygon;
            polygon.DrawCompleted += new EventHandler<DrawEventArgs>(polygon_DrawCompleted);
        }

        private void polygon_DrawCompleted(object sender, DrawEventArgs e)
        {
            if (e.Geometry == null)
            {
                return;
            }

            //将面标绘在客户端任意图层
            PolygonElement polygon = e.Element as PolygonElement;
            polygon.Opacity = 0.618;
            polygon.StrokeThickness = 1;
            polygon.Fill = new SolidColorBrush(Colors.LightGray);
            this.elementsLayer.Children.Add(polygon);

            Measure(e.Geometry);
        }

        private void Measure(SuperMap.Web.Core.Geometry geo)
        {
            MeasureParameters parameters = new MeasureParameters { Geometry = geo, Unit = Unit.Kilometer };
            MeasureService measureService = new MeasureService(url);
            measureService.ProcessAsync(parameters);
            measureService.ProcessCompleted += new EventHandler<MeasureEventArgs>(measureService_ProcessCompleted);
            measureService.Failed += new EventHandler<ServiceFailedEventArgs>(measureService_Failed);
        }

        private void measureService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        private void measureService_ProcessCompleted(object sender, MeasureEventArgs e)
        {
            if (e.Result.Distance == -1)
            {
                MessageBox.Show(e.Result.Area.ToString(System.Globalization.CultureInfo.InvariantCulture) + "平方千米");
            }
            else if (e.Result.Area == -1)
            {
                MessageBox.Show(e.Result.Distance.ToString(System.Globalization.CultureInfo.InvariantCulture) + "千米");
            }
            else
            {
                MessageBox.Show("量算没有结果！");
            }
        }

        private void pan_Click(object sender, RoutedEventArgs e)
        {

            Pan pan = new Pan(MyMap);
            MyMap.Action = pan;
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();
        }
    }
}
