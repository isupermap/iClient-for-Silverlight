﻿using System.Windows.Controls;
using SuperMap.Web.Mapping;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.Core;
using System.Windows.Navigation;
using System.Windows;
using SuperMap.Web.iServerJava6R.SpatialAnalyst;
using SuperMap.Web.Service;
using System;
using SuperMap.Web.iServerJava6R;
using System.Collections.Generic;
namespace iServerJava6R_SampleCode
{
    public partial class InterpolationAnalyst : Page
    {

        string layerid;
        TiledDynamicRESTLayer layer;
        private List<Point2D> inputPoints;
        private Random random;
        private string mapUrl = "http://localhost:8090/iserver/services/map-temperature/rest/maps/全国温度变化图";
        private const string url = "http://localhost:8090/iserver/services/spatialanalyst-sample/restjsr/spatialanalyst/";

        public InterpolationAnalyst()
        {
            InitializeComponent();
            inputPoints = new List<Point2D>();
            random = new Random();
            layer = MyMap.Layers["Tilelayer"] as TiledDynamicRESTLayer;
        }


        //执行反距离加权插值
        private void InterpolationIDW_Click(object sender, RoutedEventArgs e)
        {
            ClearLayers();
            MyProgressBar.Storyboard1.Begin();
            MyProgressBar.Visibility = Visibility.Visible;
            InterpolationIDWAnalystParameters param = new InterpolationIDWAnalystParameters();
            param.Dataset = "SamplesP@Interpolation";
            param.Bounds = new Rectangle2D(-2640403.6321084504, 1873792.1034850003, 3247669.390292245, 5921501.395578556);
            param.OutputDataset = "IDW";
            param.Resolution = 7923.84989108;
            param.PixelFormat = PixelFormat.DOUBLE;
            param.SearchMode = SearchMode.KDTREE_FIXED_COUNT;
            param.SearchRadius = 0;
            param.ZValueFieldName = "AVG_TMP";
            InterpolationAnalystService service = new InterpolationAnalystService(url);
            service.ProcessCompleted += new EventHandler<InterpolateAnalystArgs>(processCompleted);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(excuteErrors);
            service.ProcessAsync(param);

        }

        //执行样条（径向基函数）插值
        private void InterpolationRBF_Click(object sender, RoutedEventArgs e)
        {
            ClearLayers();
            MyProgressBar.Storyboard1.Begin();
            MyProgressBar.Visibility = Visibility.Visible;
            InterpolationRBFAnalystParameters param = new InterpolationRBFAnalystParameters();
            param.Dataset = "SamplesP@Interpolation";
            param.Bounds = new Rectangle2D(-2640403.6321084504, 1873792.1034850003, 3247669.390292245, 5921501.395578556);
            param.OutputDataset = "RBF";
            param.PixelFormat = PixelFormat.DOUBLE;
            param.SearchMode = SearchMode.KDTREE_FIXED_COUNT;
            param.SearchRadius = 0;
            param.ZValueFieldName = "AVG_TMP";
            InterpolationAnalystService service = new InterpolationAnalystService(url);
            service.ProcessCompleted += new EventHandler<InterpolateAnalystArgs>(processCompleted);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(excuteErrors);
            service.ProcessAsync(param);

        }
        //执行简单克吕金插值
        private void InterpolationSimpleKriging_Click(object sender, RoutedEventArgs e)
        {
            ClearLayers();
            MyProgressBar.Storyboard1.Begin();
            MyProgressBar.Visibility = Visibility.Visible;
            InterpolationKrigingAnalystParameters param = new InterpolationKrigingAnalystParameters();
            param.Dataset = "SamplesP@Interpolation";
            param.Bounds = new Rectangle2D(-2640403.6321084504, 1873792.1034850003, 3247669.390292245, 5921501.395578556);
            param.OutputDataset = "SimpleKriging";
            param.Type = InterpolationAlgorithmType.SimpleKriging;
            param.PixelFormat = PixelFormat.DOUBLE;
            param.SearchMode = SearchMode.KDTREE_FIXED_COUNT;
            param.SearchRadius = 0;
            param.Mean = 11.6005;
            param.ZValueFieldName = "AVG_TMP";
            InterpolationAnalystService service = new InterpolationAnalystService(url);
            service.ProcessCompleted += new EventHandler<InterpolateAnalystArgs>(processCompleted);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(excuteErrors);
            service.ProcessAsync(param);

        }
        //执行普通克吕金插值
        private void InterpolationKriging_Click(object sender, RoutedEventArgs e)
        {
            ClearLayers();
            MyProgressBar.Storyboard1.Begin();
            MyProgressBar.Visibility = Visibility.Visible;
            InterpolationKrigingAnalystParameters param = new InterpolationKrigingAnalystParameters();
            param.Dataset = "SamplesP@Interpolation";
            param.Bounds = new Rectangle2D(-2640403.6321084504, 1873792.1034850003, 3247669.390292245, 5921501.395578556);
            param.OutputDataset = "KRIGING";
            param.Type = InterpolationAlgorithmType.KRIGING;
            param.PixelFormat = PixelFormat.DOUBLE;
            param.SearchMode = SearchMode.KDTREE_FIXED_COUNT;
            param.SearchRadius = 0;
            param.ZValueFieldName = "AVG_TMP";
            InterpolationAnalystService service = new InterpolationAnalystService(url);
            service.ProcessCompleted += new EventHandler<InterpolateAnalystArgs>(processCompleted);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(excuteErrors);
            service.ProcessAsync(param);

        }
        //执行泛吕金插值
        private void InterpolationUniversalKriging_Click(object sender, RoutedEventArgs e)
        {
            ClearLayers();
            MyProgressBar.Storyboard1.Begin();
            MyProgressBar.Visibility = Visibility.Visible;
            InterpolationKrigingAnalystParameters param = new InterpolationKrigingAnalystParameters();
            param.Dataset = "SamplesP@Interpolation";
            param.Bounds = new Rectangle2D(-2640403.6321084504, 1873792.1034850003, 3247669.390292245, 5921501.395578556);
            param.OutputDataset = "UniversalKriging";
            param.Type = InterpolationAlgorithmType.UniversalKriging;
            param.PixelFormat = PixelFormat.DOUBLE;
            param.SearchMode = SearchMode.KDTREE_FIXED_COUNT;
            param.SearchRadius = 0;
            param.ExpectedCount = 12;
            param.Exponent = Exponent.EXP1;
            param.VariogramMode = VariogramMode.SPHERICAL;
            param.Nugget = 0;
            param.Angle = 0;
            param.Range = 0;

            param.ZValueFieldName = "AVG_TMP";
            param.Sill = 0;
            InterpolationAnalystService service = new InterpolationAnalystService(url);
            service.ProcessCompleted += new EventHandler<InterpolateAnalystArgs>(processCompleted);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(excuteErrors);
            service.ProcessAsync(param);

        }

        //分析成功,利用生成的插值数据制作范围分段专题图
        private void processCompleted(object sender, InterpolateAnalystArgs e)
        {
            List<ThemeGridRangeItem> themeRangeItems = new List<ThemeGridRangeItem>
                {
                     new ThemeGridRangeItem
                     {
                         Start =-5,
				         End =-3.4,
				         Color = new ServerColor(170,240,233)
                     },
					
				    new ThemeGridRangeItem
                    {
                        Start =-3.4,
				        End =-1.8,
				        Color =new ServerColor(176,243,196)
                    },

                     new ThemeGridRangeItem
                    {
                        Start =-1.8,
				        End =-0.2,
				        Color =new ServerColor(198,249,178)
                    },

                      new ThemeGridRangeItem
                    {
                        Start =-0.2,
				        End =1.4,
				        Color =new ServerColor(235,249,174)
                    },

				      new ThemeGridRangeItem
                    {
                        Start =1.4,
				        End =3,
				        Color =new ServerColor(188,224,123)
                    },

                    new ThemeGridRangeItem
                    {
                        Start =3,
				        End =4.6,
				        Color =new ServerColor(88,185,63)
                    },

                    new ThemeGridRangeItem
                    {
                        Start =4.6,
				        End =6.2,
				        Color =new ServerColor(25,147,52)
                    },

		           new ThemeGridRangeItem
                    {
                        Start =6.2,
				        End =7.8,
				        Color =new ServerColor(54,138,58)
                    },

				  new ThemeGridRangeItem
                    {
                        Start =7.8,
				        End =9.4,
				        Color =new ServerColor(131,158,47)
                    },
			     
                    new ThemeGridRangeItem
                    {
                        Start =9.4,
				        End =11,
				        Color =new ServerColor(201,174,28)
                    },
			
		            new ThemeGridRangeItem
                    {
                        Start =11,
				        End =12.6,
				        Color =new ServerColor(232,154,7)
                    },
			
			        new ThemeGridRangeItem
                    {
                        Start =12.6,
				        End =14.2,
				        Color =new ServerColor(204,91,2)
                    },

                    new ThemeGridRangeItem
                    {
                        Start =14.2,
				        End =15.8,
				        Color =new ServerColor(174,54,1)
                    },

                     new ThemeGridRangeItem
                    {
                        Start =15.8,
				        End =17.4,
				        Color =new ServerColor(127,13,1)
                    },

			        new ThemeGridRangeItem
                    {
                        Start =17.4,
				        End =19,
				        Color =new ServerColor(115,23,6)
                    },
				
				    new ThemeGridRangeItem
                    {
                        Start =19,
				        End =20.6,
				        Color =new ServerColor(111,36,8)
                    },
				
				   new ThemeGridRangeItem
                    {
                        Start =20.6,
				        End =22.2,
				        Color =new ServerColor(107,47,14)
                    },

                    new ThemeGridRangeItem
                    {
                        Start =22.2,
				        End =23.8,
				        Color =new ServerColor(125,75,44)
                    },

				   new ThemeGridRangeItem
                    {
                        Start =23.8,
				        End =25.4,
				        Color =new ServerColor(146,110,88)
                    },

                     new ThemeGridRangeItem
                    {
                        Start =25.4,
				        End =27,
				        Color =new ServerColor(166,153,146)
                    },
							
            };

            //设置栅格分段专题图分段模式、反序显示参数和子项数组
            ThemeGridRange themeGridRange = new ThemeGridRange
            {
                RangeMode = RangeMode.EQUALINTERVAL,
                ReverseColor = false,
                Items = themeRangeItems,
            };

            //设置专题图参数
            ThemeParameters parameter = new ThemeParameters
            {
                DatasetName = e.Result.Dataset.Split('@')[0],
                DataSourceName = "Interpolation",
                Themes = new List<Theme> { themeGridRange },
            };
            //与服务器交互
            ThemeService themeService = new ThemeService(mapUrl);
            themeService.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeServie_ProcessCompleted);
            themeService.Failed += new EventHandler<ServiceFailedEventArgs>(themeServie_Failed);
            themeService.ProcessAsync(parameter);


        }

        //展示插值分析专题图
        private void themeServie_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            TiledDynamicRESTLayer themeLayer = new TiledDynamicRESTLayer();
            themeLayer.Url = mapUrl;
            //themeLayer.CRS = new CoordinateReferenceSystem(3857);
            layerid = e.Result.ResourceInfo.NewResourceID;
            themeLayer.LayersID = layerid;
            themeLayer.Transparent = true;
            themeLayer.EnableServerCaching = false;
            //加载专题图图层
            this.MyMap.Layers.Add(themeLayer);
            MyProgressBar.Storyboard1.Stop();
            MyProgressBar.Visibility = Visibility.Collapsed;
            //currentStateTest ="插值分析完毕";
        }

        //生成专题图失败调用的处理函数
        private void themeServie_Failed(Object sender, ServiceFailedEventArgs e)
        {
            MyProgressBar.Storyboard1.Stop();
            MyProgressBar.Visibility = Visibility.Collapsed;
            MessageBox.Show("生成专题图失败");

        }


        //与服务端交互失败时调用的处理函数
        private void excuteErrors(Object sender, ServiceFailedEventArgs e)
        {
            MyProgressBar.Storyboard1.Stop();
            MyProgressBar.Visibility = Visibility.Collapsed;
            MessageBox.Show("插值分析失败！" + e.Error);
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            ClearLayers();
        }

        private void ClearLayers()
        {
            for (int i = MyMap.Layers.Count - 1; i > 0; i--)
            {
                MyMap.Layers.RemoveAt(i);
            }
        }

        //离散点插值分析
        private void cretePointsAnalyst_Click(object sender, RoutedEventArgs e)
        {
            ClearLayers();
            MyProgressBar.Storyboard1.Begin();
            MyProgressBar.Visibility = Visibility.Visible;
            //设置 SQL 查询参数，FilterParameters为必设属性
            QueryBySQLParameters param = new QueryBySQLParameters()
            {
                FilterParameters = new List<FilterParameter>() 
                {
                    new FilterParameter()
                    {
                        //Name 为必设属性
                        Name = "SamplesP@Interpolation", 
                        
                        //SQL 查询条件，从文本框中获取用户输入的查询条件
                        AttributeFilter = "SMID>0"
                    }
                },

                //设置是返回查询结果资源（false）还是返回查询结果记录集（true）
                ReturnContent = true
            };

            //与服务器交互
            QueryBySQLService sqlService = new QueryBySQLService(mapUrl);
            sqlService.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(excuteErrors);
            sqlService.ProcessCompleted += new EventHandler<QueryEventArgs>(sqlService_ProcessCompleted);
            sqlService.ProcessAsync(param);
        }
        private void sqlService_ProcessCompleted(object sender, QueryEventArgs e)
        {
            int zMin = Convert.ToInt32(zMinValue.Text);
            int zMax = Convert.ToInt32(zMaxValue.Text);

            FeatureCollection fc = e.Result.Recordsets[0].Features;
            for (int i = 0; i < fc.Count; i++)
            {
                GeoPoint gp = fc[i].Geometry as GeoPoint;
                var z = random.Next(zMin, zMax);
                Point2D point = new Point2D()
                {
                    X = gp.X,
                    Y = gp.Y,
                    Tag = z
                };
                inputPoints.Add(point);
            }
            InterpolationIDWAnalystParameters idwParams = new InterpolationIDWAnalystParameters()
            {
                Bounds = new Rectangle2D(-2640403.6321084504, 1873792.1034850003, 3247669.390292245, 5921501.395578556),
                OutputDataset = "idwcretepoints",
                OutputDataSource = "Interpolation",
                SearchMode=SearchMode.KDTREE_FIXED_RADIUS,
                InterpolationAnalystType=InterpolationAnalystType.GEOMETRY,
                InputPoints = inputPoints
            };
            InterpolationAnalystService service = new InterpolationAnalystService(url);
            service.ProcessCompleted += new EventHandler<InterpolateAnalystArgs>(processCompleted);
            service.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(excuteErrors);
            service.ProcessAsync(idwParams);
        }
    }
}
