﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using SuperMap.Web.iServerJava6R.SpatialAnalyst;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode.iServerJava6R
{
    public partial class iServerJava6RGenerateSpatialData : Page
    {
        TiledDynamicRESTLayer layer;
        string _mapUrl = "http://localhost:8090/iserver/services/map-changchun/rest/maps/长春市区图";
        string _serviceUrl = "http://localhost:8090/iserver/services/spatialanalyst-changchun/restjsr/spatialanalyst";

        public iServerJava6RGenerateSpatialData()
        {
            InitializeComponent();
        }

        // 当用户导航到此页面时执行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GenerateSpatialDataParameters param = new GenerateSpatialDataParameters();
            param.EventRouteIDField = "RouteID";
            param.EventTable = "LinearEventTabDT@Changchun";
            param.MeasureEndField = "LineMeasureTo";
            param.MeasureStartField = "LineMeasureFrom";
            param.RouteDataset = "RouteDT_road@Changchun";
            param.RouteIDField = "RouteID";
            param.DataReturnOption = new DataReturnOption();
            param.DataReturnOption.DataReturnMode = DataReturnMode.DATASET_ONLY;
            param.DataReturnOption.Dataset = "generateSpatialData";
            param.DataReturnOption.DeleteExistResultDataset = true;
            param.DataReturnOption.ExpectCount = 1000;

            GenerateSpatialDataService service = new GenerateSpatialDataService(_serviceUrl);
            service.ProcessCompleted += new EventHandler<GenerateSpatialDataArgs>(service_ProcessCompleted);
            service.ProcessAsync(param);
        }

        void service_ProcessCompleted(object sender, GenerateSpatialDataArgs e)
        {
            ThemeUniqueItem item1 = new ThemeUniqueItem();
            ServerStyle style1 = new ServerStyle();
            style1.FillForeColor = Color.FromArgb(255, 242, 48, 48);
            style1.LineColor = Color.FromArgb(255, 242, 48, 48);
            item1.Unique = "拥堵";
            item1.Style = style1;

            ThemeUniqueItem item2 = new ThemeUniqueItem();
            ServerStyle style2 = new ServerStyle();
            style2.FillForeColor = Color.FromArgb(255, 255, 159, 25);
            style2.LineColor = Color.FromArgb(255, 255, 159, 25);
            item2.Unique = "缓行";
            item2.Style = style2;

            ThemeUniqueItem item3 = new ThemeUniqueItem();
            ServerStyle style3 = new ServerStyle();
            style3.FillForeColor = Color.FromArgb(255, 91, 195, 69);
            style3.LineColor = Color.FromArgb(255, 91, 195, 69);
            item3.Unique = "畅通";
            item3.Style = style3;

            ThemeUnique themeUnique = new ThemeUnique();
            themeUnique.UniqueExpression = "TrafficStatus";
            themeUnique.Items = new List<ThemeUniqueItem>() { item1,item2,item3};

            ThemeParameters param = new ThemeParameters();
            param.DatasetName = "generateSpatialData";
            param.DataSourceName = "Changchun";
            param.Themes=new List<Theme>(){themeUnique};

            ThemeService themeService = new ThemeService(_mapUrl);
            themeService.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeService_ProcessCompleted);
            themeService.ProcessAsync(param);
        }

        void themeService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            ClearLayer();
            layer = new TiledDynamicRESTLayer();
            layer.Url = _mapUrl;

            layer.LayersID = e.Result.ResourceInfo.NewResourceID;
            layer.Transparent = true;
            layer.EnableServerCaching = false;
            MyMap.Layers.Add(layer);
        }

        private void ClearLayer()
        {
            if (layer != null)
            {
                if (MyMap.Layers.Contains(layer))
                {
                    MyMap.Layers.Remove(layer);
                }
                layer = null;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ClearLayer();
        }
    }
}
