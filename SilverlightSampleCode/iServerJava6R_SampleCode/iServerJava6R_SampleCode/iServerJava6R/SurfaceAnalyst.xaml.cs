﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.SpatialAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class SurfaceAnalyst : Page
    {
        private FeaturesLayer featuresLayer;
        private const string url1 = "http://localhost:8090/iserver/services/map-temperature/rest/maps/全国温度变化图";
        private const string url2 = "http://localhost:8090/iserver/services/spatialanalyst-sample/restjsr/spatialanalyst";

        public SurfaceAnalyst()
        {
            InitializeComponent();
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            featuresLayer.ClearFeatures();
        }

        private void datasetsIsoline_Click(object sender, RoutedEventArgs e)
        {
            DatasetSurfaceAnalystParameters param = new DatasetSurfaceAnalystParameters
            {
                Dataset = "SamplesP@Interpolation",
                SurfaceAnalystMethod = SurfaceAnalystMethod.ISOLINE,
                ZValueFieldName = "AVG_TMP",
                Resolution = 3000,
                ParametersSetting= new SurfaceAnalystParametersSetting
                { 
                    ResampleTolerance = 0.7,
                    SmoothMethod = SmoothMethod.BSPLINE,
                    DatumValue = Convert.ToDouble(MyTextBox1.Text),
                    Interval = Convert.ToDouble(MyTextBox2.Text),
                    Smoothness = 3
                },
            };
            SurfaceAnalystService datasetIsolineService = new SurfaceAnalystService(url2);
            datasetIsolineService.ProcessAsync(param);
            datasetIsolineService.ProcessCompleted += new EventHandler<SurfaceAnalystEventArgs>(datasetIsolineService_ProcessCompleted);
            datasetIsolineService.Failed += new EventHandler<ServiceFailedEventArgs>(datasetIsolineService_Failed);
        }

        void datasetIsolineService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("等值线提取失败！");
        }

        void datasetIsolineService_ProcessCompleted(object sender, SurfaceAnalystEventArgs e)
        {
            if (e.Result.Recordset.Features == null || e.Result.Recordset.Features.Count == 0)
            {
                MessageBox.Show("获取等值线数目为零！");
            }
            else
            {
                foreach (var item in e.Result.Recordset.Features)
                {
                    item.Style = new PredefinedLineStyle
                    {
                        Stroke = new SolidColorBrush
                        {
                            Color = Colors.Magenta,
                            Opacity = 0.6
                        },
                        StrokeThickness = 2
                    };
                    featuresLayer.AddFeature(item);
                }
            }
        }
    }
}
