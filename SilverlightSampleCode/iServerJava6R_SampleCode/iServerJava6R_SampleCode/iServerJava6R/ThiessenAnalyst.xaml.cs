﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Net;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.iServerJava6R.SpatialAnalyst;
using SuperMap.Web.Service;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;

namespace iServerJava6R_SampleCode.iServerJava6R
{
    public partial class ThiessenAnalyst : Page
    {
        private FeaturesLayer featureLayer;
        private const string url2 = "http://localhost:8090/iserver/services/spatialanalyst-changchun/restjsr/spatialanalyst";

        public ThiessenAnalyst()
        {
            InitializeComponent();
            this.featureLayer = this.MyMap.Layers["MyFeatureLayer"] as FeaturesLayer;
        }

        private void DatasetThiessenAnalyst_Click(object sender, RoutedEventArgs e)
        {
            DatasetThiessenAnalystParameters thiessenParams = new DatasetThiessenAnalystParameters();
            thiessenParams.CreateResultDataset = true;
            thiessenParams.DataSet = "Factory@Changchun";
            thiessenParams.ResultDatasetName = "";
            thiessenParams.ResultDatasourceName = "";
            thiessenParams.ReturnResultRegion = true;
            thiessenParams.FilterQueryParameter = null;
            thiessenParams.ClipRegion = null;
            ThiessenAnalystService thiessenService = new ThiessenAnalystService(url2);
            thiessenService.ProcessAsync(thiessenParams);
            thiessenService.ProcessCompleted += new EventHandler<ThiessenAnalystEventArgs>(ThiessenAnalyst_ProcessCompleted);
            thiessenService.Failed += new EventHandler<ServiceFailedEventArgs>(ThiessenAnalyst_Failed);
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            featureLayer.ClearFeatures();
        }

        private void ThiessenAnalyst_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("分析失败！原因："+e.UserState.ToString());
        }

        private void ThiessenAnalyst_ProcessCompleted(object sender, ThiessenAnalystEventArgs e)
        {
            featureLayer.ClearFeatures();
            ThiessenAnalystResult result = e.Result;
            featureLayer.AddFeatureSet(result.Recorset.Features);
        }

        private void GeometryThiessenAnalyst_Click(object sender, RoutedEventArgs e)
        {
            Point2DCollection points = new Point2DCollection();
            points.Add(new Point2D(5472.712382, -2189.15344));
            points.Add(new Point2D(4707.299652, -1229.476725));
            points.Add(new Point2D(5450.34263, -2070.794081));
            points.Add(new Point2D(5447.671615, -2255.928819));
            points.Add(new Point2D(5357.895026, -1965.022579));
            points.Add(new Point2D(5317.70775, -2521.162355));
            GeometryThiessenAnalystParameters thiessenParams = new GeometryThiessenAnalystParameters();
            thiessenParams.Points = points;
            thiessenParams.CreateResultDataset = true;
            thiessenParams.ResultDatasetName = "";
            thiessenParams.ResultDatasourceName = "Changchun";
            thiessenParams.ReturnResultRegion = true;
            thiessenParams.ClipRegion = null;
            ThiessenAnalystService thiessenService = new ThiessenAnalystService(url2);
            thiessenService.ProcessAsync(thiessenParams);
            thiessenService.ProcessCompleted += new EventHandler<ThiessenAnalystEventArgs>(ThiessenAnalyst_ProcessCompleted);
            thiessenService.Failed += new EventHandler<ServiceFailedEventArgs>(ThiessenAnalyst_Failed);
        }
    }
}
