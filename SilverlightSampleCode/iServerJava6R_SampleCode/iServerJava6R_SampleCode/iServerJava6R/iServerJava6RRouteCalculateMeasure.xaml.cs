﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using SuperMap.Web.Mapping;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Service;
using SuperMap.Web.iServerJava6R.SpatialAnalyst;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.Utilities;
using System.Globalization;
using SuperMap.Web.Actions;
using System.Windows.Input;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RRouteCalculateMeasure : Page
    {
        TiledDynamicRESTLayer layer;
        FeaturesLayer _routeLayer;
        ElementsLayer _elementsLayer;
        Pushpin pushpin = new Pushpin();

        string _dataUrl = "http://localhost:8090/iserver/services/data-changchun/rest/data/featureResults";
        string _serviceUrl = "http://localhost:8090/iserver/services/spatialanalyst-changchun/restjsr/spatialanalyst/";
        Point2D _queryPoint = Point2D.Empty;
       

        public iServerJava6RRouteCalculateMeasure()
        {
            InitializeComponent();

            _routeLayer = MyMap.Layers["RouteLayer"] as FeaturesLayer;
            _elementsLayer = MyMap.Layers["MyElementsLayer"] as ElementsLayer;

            GetFeaturesBySQLParameters param = new GetFeaturesBySQLParameters
            {
                DatasetNames = new List<string> { "Changchun:RouteDT_road" },
                FilterParameter = new SuperMap.Web.iServerJava6R.FilterParameter
                {
                    AttributeFilter = "RouteID = 1690",
                }
            };
            GetFeaturesBySQLService ser = new GetFeaturesBySQLService(_dataUrl);
            ser.Failed += new EventHandler<ServiceFailedEventArgs>(ser_Failed);
            ser.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(ser_ProcessCompleted);
            ser.ProcessAsync(param);

            Pushpin pushpin1 = new Pushpin();
            pushpin1.IsEnabled = false;
            pushpin1.Location = new Point2D(4020.0045, -4377.0273);
            pushpin1.Content = "A";
            this._elementsLayer.AddChild(pushpin1);

            Pushpin pushpin2 = new Pushpin();
            pushpin2.IsEnabled = false;
            pushpin2.Location = new Point2D(6214.1838, -4209.9278);
            pushpin2.Content = "B";
            this._elementsLayer.AddChild(pushpin2);
            
        }
        // 当用户导航到此页面时执行。
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            _elementsLayer.Children.Remove(pushpin);
            this.boder.Visibility = Visibility.Collapsed;
            this.measure.Text = "";
            DrawPoint action = new DrawPoint(MyMap);
            MyMap.Action = action;
            action.DrawCompleted += new EventHandler<DrawEventArgs>(action_DrawCompleted);

        }

        void action_DrawCompleted(object sender, DrawEventArgs e)
        {
            _elementsLayer.Children.Remove(pushpin);
            //this.boder.Visibility = Visibility.Collapsed;
            Route route = new Route();
            if (_routeLayer.Features.Count > 0)
            {
                Feature f = _routeLayer.Features[0];
                route = f.Geometry as Route;
                
            }

            pushpin.IsEnabled = false;
            pushpin.Location = e.Geometry.Bounds.Center;
            pushpin.Background = new SolidColorBrush(Colors.Red);
            pushpin.Content = "O";

            _elementsLayer.AddChild(pushpin);
            

           _queryPoint = (e.Geometry as GeoPoint).Location;

            RouteCalculateMeasureParameters param = new RouteCalculateMeasureParameters();
            param.IsIgnoreGap = true;
            param.Point = _queryPoint;
            param.SourceRoute = route;
            param.Tolerance = 100;
            RouteCalculateMeasureService service = new RouteCalculateMeasureService(_serviceUrl);
            service.ProcessComplated += service_ProcessComplated;
            service.Failed += service_Failed;
            service.ProcessAsync(param);
        }

        void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("请在AB线上选择点！");
        }

        void service_ProcessComplated(object sender, RouteCalculateMeasureEventArgs e)
        {
            this.boder.Visibility = Visibility.Visible;
            this.measure.Text = "该点与起点A的距离为：" + e.Result.Measure.ToStringEx() + " 米";
        }

        //private void Button_Click_1(object sender, RoutedEventArgs e)
        //{
            
            
        //    GetFeaturesBySQLParameters param = new GetFeaturesBySQLParameters
        //    {
        //        DatasetNames = new List<string> { "Changchun:RouteDT_road" },
        //        FilterParameter = new SuperMap.Web.iServerJava6R.FilterParameter
        //        {
        //            AttributeFilter = "RouteID = 1690",
        //        }
        //    };
        //    GetFeaturesBySQLService ser = new GetFeaturesBySQLService(_dataUrl);
        //    ser.Failed += new EventHandler<ServiceFailedEventArgs>(ser_Failed);
        //    ser.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(ser_ProcessCompleted);
        //    ser.ProcessAsync(param);

        //    Pushpin pushpin1 = new Pushpin();
        //    pushpin1.IsEnabled = false;
        //    pushpin1.Location = new Point2D(4020.0045, -4377.0273);
        //    pushpin1.Content = "起";
        //    this._elementsLayer.AddChild(pushpin1);

        //    Pushpin pushpin2 = new Pushpin();
        //    pushpin2.IsEnabled = false;
        //    pushpin2.Location = new Point2D(6214.1838, -4209.9278);
        //    pushpin2.Content = "终";
        //    this._elementsLayer.AddChild(pushpin2);
        //}

        void ser_ProcessCompleted(object sender, GetFeaturesEventArgs e)
        {
            _routeLayer.Features.Clear();
            foreach (var f in e.Result.Features)
            {
                PredefinedLineStyle style = new PredefinedLineStyle();
                style.StrokeThickness = 5;
                style.Stroke = new SolidColorBrush(Colors.Red);
                style.Symbol = PredefinedLineStyle.LineSymbol.Dash;
                f.Style = style;
                _routeLayer.AddFeature(f);
            }
        }

        void ser_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("查询路由对象失败！");
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap);
            MyMap.Action = pan;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            _elementsLayer.Children.Remove(pushpin);
            this.boder.Visibility = Visibility.Collapsed;
            this.measure.Text = "";
        }
     
    }
}
