﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RThemeRange : Page
    {
        private bool isAdded;
        //世界图层服务地址
        private const string url = "http://localhost:8090/iserver/services/map-world/rest/maps/World Map";

        //定义专题图层
        private TiledDynamicRESTLayer themeLayer = new TiledDynamicRESTLayer()
        {
            Url = url,
            //设置图层是否透明,true 表示透明。
            Transparent = true
        };

        public iServerJava6RThemeRange()
        {
            InitializeComponent();
        }

        //点击生成专题图触发事件
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            //设置专题图子项
            List<ThemeRangeItem> themeRangeItems = new List<ThemeRangeItem>
            {
                new ThemeRangeItem
                {
                    Start = 0.0,
                    End = 59973,
                    Visible = true,
                    Style = new ServerStyle 
                    {
                        FillForeColor = new Color {R=216,G=244,B=254},
                        LineWidth = 0.1,
                    }
                },
                
                new ThemeRangeItem
                {
                    Start = 59973,
                    End = 1097234,
                    Visible = true ,
                    Style = new ServerStyle 
                    {
                         FillForeColor = new Color {R=131,G=232,B=252},
                         LineWidth = 0.1 ,
                    }
                },

                new ThemeRangeItem
                {
                    Start = 1097234,
                    End = 5245515,
                    Visible = true ,
                    Style = new ServerStyle 
                    {
                        FillForeColor = new Color {R=112,G=212,B=243},
                        LineWidth = 0.1,
                    }
                },
                 
                new ThemeRangeItem
                {
                    Start = 5245515,
                    End = 17250390,
                    Visible = true,
                    Style = new ServerStyle 
                    {
                        FillForeColor = new Color {R=23,G=198,B=238},
                        LineWidth = 0.1,
                    }
                },

                new ThemeRangeItem
                {
                    Start = 17250390,
                    End = 894608700,
                    Visible = true,
                    Style = new ServerStyle 
                    {
                        FillForeColor = new Color {R=0,G=187,B=236},
                        LineWidth = 0.1 ,
                    }
                },

                new ThemeRangeItem
                {
                    Start = 894608700,
                    End =  12E+8,
                    Visible = true ,
                    Style = new ServerStyle 
                    {
                        FillForeColor = new Color {R=0,G=133,B=236},
                        LineWidth = 0.1 ,
                    }
                }
            };
            //设置范围分段专题图字段、分段模式、分段参数和子项数组
            ThemeRange themeRange = new ThemeRange
            {
                RangeExpression = "Pop_1994",
                RangeMode = RangeMode.EQUALINTERVAL,
                RangeParameter = 6,
                Items = themeRangeItems
            };
            //设置专题图参数
            ThemeParameters themeRangeParams = new ThemeParameters
            {
                //数据集名称
                DatasetName = "Countries",
                //数据源名称
                DataSourceName = "World",
                Themes = new List<Theme> { themeRange }
            };
            //与服务器交互
            ThemeService themeRangeServie = new ThemeService(url);
            themeRangeServie.ProcessAsync(themeRangeParams);
            themeRangeServie.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeRangeServie_ProcessCompleted);
            themeRangeServie.Failed += new EventHandler<ServiceFailedEventArgs>(themeRangeServie_Failed);
        }

        //与服务器交互失败
        private void themeRangeServie_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("生成专题图失败！");
        }

        //与服务器交互成功
        private void themeRangeServie_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            //显示专题图。专题图在服务端为一个资源，每个资源都有一个 ID 号和一个 url
            //要显示专题图即将资源结果的 ID 号赋值给图层的 layersID 属性即可
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
            if (!isAdded)
            {
                //加载专题图图层
                this.MyMap.Layers.Add(themeLayer);
                isAdded = true;
            }
        }

        //移除专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            if (isAdded == false)
            {
                MessageBox.Show("请先添加一幅专题图！");
            }
            else
            {
                themeLayer.ClearTheme();
                isAdded = false;
            }
        }
    }
}
