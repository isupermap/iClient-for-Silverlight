﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class SetLayerStatusTest : Page
    {
        private List<LayerStatus> layersStatus;
        private const string url = "http://localhost:8090/iserver/services/map-world/rest/maps/World Map";
        private string tempLayerID = string.Empty;
        private TiledDynamicRESTLayer layer;

        public SetLayerStatusTest( )
        {
            InitializeComponent();

            layersStatus = new List<LayerStatus>();
            layer = new TiledDynamicRESTLayer
            {
                Url = url ,
                EnableServerCaching = false ,
            };
            this.MyMap.Layers.Add(layer);

            //获取图层信息
            GetLayersInfoService getInfoServer = new GetLayersInfoService(url);
            getInfoServer.ProcessAsync();
            getInfoServer.ProcessCompleted += new EventHandler<GetLayersInfoEventArgs>(getInfoServer_ProcessCompleted);
            getInfoServer.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(getInfoServer_Failed);

        }

        private void getInfoServer_Failed(object sender , SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
            MessageBox.Show("获取图层状态失败。");
        }

        private void getInfoServer_ProcessCompleted(object sender , GetLayersInfoEventArgs e)
        {
            if (e.Result.LayersInfo.Count > 0)
            {
                foreach (var layer in e.Result.LayersInfo)
                {
                    layersStatus.Add(new LayerStatus { IsVisible = layer.IsVisible , LayerName = layer.Name });
                }
                layersList.ItemsSource = layersStatus;
            }
        }

        private void setLayersStatus_Failed(object sender , SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
            MessageBox.Show("设置图层状态失败。");
        }

        private void setLayersStatus_ProcessCompleted(object sender , SetLayerEventArgs e)
        {
            if (e.Result.IsSucceed)
            {
                tempLayerID = e.Result.NewResourceID;
                layer.LayersID = e.Result.NewResourceID;
                layer.Refresh();

            }
        }
        private void isVisible_Click(object sender , RoutedEventArgs e)
        {
            SetLayerStatusParameters parameters = new SetLayerStatusParameters
            {
                HoldTime = 30 ,
                LayerStatusList = layersStatus ,
                ResourceID = tempLayerID
            };

            SetLayerStatusService setLayersStatus = new SetLayerStatusService(url);
            setLayersStatus.ProcessAsync(parameters);
            setLayersStatus.ProcessCompleted += new EventHandler<SetLayerEventArgs>(setLayersStatus_ProcessCompleted);
            setLayersStatus.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(setLayersStatus_Failed);
        }
    }
}