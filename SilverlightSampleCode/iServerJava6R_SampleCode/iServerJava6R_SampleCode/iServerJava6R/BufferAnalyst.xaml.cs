﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.SpatialAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class BufferAnalyst : Page
    {
        //京津图层服务地址
        private const string url1 = "http://localhost:8090/iserver/services/map-jingjin/rest/maps/京津地区土地利用现状图";
        private const string url2 = "http://localhost:8090/iserver/services/spatialanalyst-sample/restjsr/spatialanalyst";
        private  FeaturesLayer resultLayer = new FeaturesLayer();
        private TiledDynamicRESTLayer bufferLayer = new TiledDynamicRESTLayer()
        {
            Url = url1,
            //设置图层是否透明,true 表示透明。
            Transparent = true
        };
        public BufferAnalyst()
        {
            InitializeComponent();
            this.MyMap.Layers.Add(resultLayer);
        }
        private void DatasetBufferAnalyst_Click(object sender, RoutedEventArgs e)
        {
            DatasetBufferAnalystParameters param = new DatasetBufferAnalystParameters
            {
                BufferSetting = new BufferSetting
                {
                    EndType = BufferEndType.ROUND,
                    LeftDistance = new BufferDistance 
                    {
                        Value =Convert.ToDouble(MyTextBox.Text)
                    },
                    SemicircleLineSegment = 12,
                    
                },
                FilterQueryParameter = new SuperMap.Web.iServerJava6R.FilterParameter
                {
                    AttributeFilter = "SmID=19"
                },
                Dataset = "Landuse_R@Jingjin",
                
            };
            DatasetBufferAnalystService datasetBufferAnalyst = new DatasetBufferAnalystService(url2);
            datasetBufferAnalyst.ProcessAsync(param);
            datasetBufferAnalyst.Failed += new EventHandler<ServiceFailedEventArgs>(datasetBufferAnalyst_Failed);
            datasetBufferAnalyst.ProcessCompleted += new EventHandler<DatasetBufferAnalystArgs>(datasetBufferAnalyst_ProcessCompleted);
        }

        void datasetBufferAnalyst_ProcessCompleted(object sender, DatasetBufferAnalystArgs e)
        {
            foreach (Feature feature in e.Result.Recordset.Features)
            {
                feature.Style = new PredefinedFillStyle
                {
                    StrokeThickness = 1,
                    Fill = new SolidColorBrush
                    {
                         Color=Colors.Red,
                         Opacity=0.3
                    }                    
                };
            }
            resultLayer.AddFeatureSet(e.Result.Recordset.Features);
        }

        void datasetBufferAnalyst_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("缓冲区分析失败！");
        }
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            resultLayer.ClearFeatures();
        }
    }
}
