﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RFindMTSPPath : Page
    {
        private ElementsLayer elementsLayer;
        private FeaturesLayer featuresLayer;
        private FeaturesLayer featuresLayerCenterPoints;
        private Point2DCollection points = new Point2DCollection();
        private List<Point2D> centers = new List<Point2D>();
        private int i = 0;

        public iServerJava6RFindMTSPPath()
        {
            InitializeComponent();

            elementsLayer = this.MyMap.Layers["MyElementsLayer"] as ElementsLayer;

            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            featuresLayerCenterPoints = this.MyMap.Layers["MyFeaturesLayer1"] as FeaturesLayer;

            Feature feature1 = new Feature
            {
                Geometry = new GeoPoint(4100, -4100),
                Style = new PredefinedMarkerStyle
                {
                    Color = new SolidColorBrush(Colors.Red),
                    Size = 20,
                    Symbol = PredefinedMarkerStyle.MarkerSymbol.Star
                }
            };

            featuresLayerCenterPoints.Features.Add(feature1);

            Feature feature2 = new Feature
            {
                Geometry = new GeoPoint(4500, -3000),
                Style = new PredefinedMarkerStyle
                {
                    Color = new SolidColorBrush(Colors.Red),
                    Size = 20,
                    Symbol = PredefinedMarkerStyle.MarkerSymbol.Star
                }
            };
            featuresLayerCenterPoints.AddFeature(feature2);

            Feature feature3 = new Feature
            {
                Geometry = new GeoPoint(5000, -3500),
                Style = new PredefinedMarkerStyle
                {
                    Color = new SolidColorBrush(Colors.Red),
                    Size = 20,
                    Symbol = PredefinedMarkerStyle.MarkerSymbol.Star
                }
            };
            featuresLayerCenterPoints.AddFeature(feature3);
        }

        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            //标记途经结点顺序
            i++;

            //将结点以图钉样式加到ElementsLayer上
            Pushpin pushpin = new Pushpin();
            pushpin.Location = e.Geometry.Bounds.Center;
            pushpin.Content = i.ToString(System.Globalization.CultureInfo.InvariantCulture);
            elementsLayer.AddChild(pushpin);

            //用points数组记录结点坐标
            points.Add(pushpin.Location);
        }

        private void PathAnalyst_Click(object sender, RoutedEventArgs e)
        {
            if (points.Count == 0)
            {
                MessageBox.Show("请选择配送目标");
                return;
            }

            FindMTSPPathsParameters<Point2D> paramPoint2D = new FindMTSPPathsParameters<Point2D>
            {
                HasLeastTotalCost = false,

                //已选定的中心站点
                Centers = new List<Point2D> { new Point2D(4100, -4100), new Point2D(4500, -3000), new Point2D(5000, -3500) },
                Nodes = points,
                Parameter = new TransportationAnalystParameter
                {
                    BarrierEdgeIDs = null,
                    BarrierNodeIDs = null,
                    TurnWeightField = "TurnCost",
                    WeightFieldName = "length",
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    }
                }
            };

            //与服务器交互
            FindMTSPPathsService findMTSPPathsService = new FindMTSPPathsService("http://localhost:8090/iserver/services/transportationanalyst-sample/rest/networkanalyst/RoadNet@Changchun");
            findMTSPPathsService.ProcessAsync(paramPoint2D);
            findMTSPPathsService.ProcessCompleted += new EventHandler<FindMTSPPathsEventArgs>(findMTSPPathsService_ProcessCompleted);
            findMTSPPathsService.Failed += new EventHandler<ServiceFailedEventArgs>(findMTSPPathsService_Failed);
        }

        private void findMTSPPathsService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        private void findMTSPPathsService_ProcessCompleted(object sender, FindMTSPPathsEventArgs e)
        {
            //路径样式
            PredefinedLineStyle simpleLineStyle = new PredefinedLineStyle();
            simpleLineStyle.Stroke = new SolidColorBrush(Colors.Blue);
            simpleLineStyle.StrokeThickness = 2;

            if (e.Result != null && e.Result.MTSPathList != null)
            {
                foreach (ServerPath p in e.Result.MTSPathList)
                {
                    //将要素添加到图层上
                    featuresLayer.Features.Add(new Feature { Geometry = p.Route, Style = simpleLineStyle });
                }
            }
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //清除 ElementsLayer 和 FeaturesLayer 图层上全部元素
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();
            points.Clear();
            i = 0;
        }
    }
}
