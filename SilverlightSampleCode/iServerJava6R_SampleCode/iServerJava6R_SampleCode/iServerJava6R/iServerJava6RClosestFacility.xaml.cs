﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RClosestFacility : Page
    {
        private ElementsLayer elementsLayerE;
        private ElementsLayer elementsLayerF;
        private FeaturesLayer featuresLayer;
        private Point2DCollection points = new Point2DCollection();
        private Point2D eventp = new Point2D();
        private bool flag = false;

        public iServerJava6RClosestFacility()
        {
            InitializeComponent();
            elementsLayerE = this.MyMap.Layers["MyElementsLayerE"] as ElementsLayer;
            elementsLayerF = this.MyMap.Layers["MyElementsLayerF"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            Pushpin pushpin = new Pushpin();

            //将事件点以图钉样式加到ElementsLayerE上
            if (eventPonit.IsChecked == true)
            {
                //因为事件点只有一个，因此判断是否存在事件点，如果存在就先将以前的事件点删除
                if (flag == true)
                {
                    elementsLayerE.Children.Clear();
                }
                pushpin.Location = e.Geometry.Bounds.Center;
                pushpin.Content = "E";
                pushpin.Background = new SolidColorBrush(Colors.Red);
                elementsLayerE.AddChild(pushpin);
                flag = true;

                //记录事件点坐标
                eventp = pushpin.Location;
            }

            //将设施点以图钉样式加到ElementsLayer上
            if (FacilityPoint.IsChecked == true)
            {
                pushpin.Location = e.Geometry.Bounds.Center;
                pushpin.Content = "F";
                pushpin.Background = new SolidColorBrush(Colors.Purple);
                elementsLayerF.AddChild(pushpin);

                //用 points 数组记录设施点坐标
                points.Add(pushpin.Location);
            }
        }

        //最近设施查找
        private void FindClosetFacility_Click(object sender, RoutedEventArgs e)
        {
            if (featuresLayer.Features.Count > 0)
            {
                featuresLayer.Features.Clear();
            }

            if (eventp.IsEmpty)
            {
                MessageBox.Show("请指定事件点");
                return;
            }

            if (points.Count == 0)
            {
                MessageBox.Show("请指设施点");
                return;
            }

            FindClosestFacilitiesParameters<Point2D> paramPoint2D = new FindClosestFacilitiesParameters<Point2D>
            {
                Event = eventp,
                Facilities = points,
                FromEvent = true,
                MaxWeight = 0,
                ExpectFacilityCount = 1,
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"
                }
            };

            //与服务器交互
            FindClosestFacilitiesService findclosestFacilitiesService = new FindClosestFacilitiesService("http://localhost:8090/iserver/services/transportationanalyst-sample/rest/networkanalyst/RoadNet@Changchun");
            findclosestFacilitiesService.ProcessAsync(paramPoint2D);
            findclosestFacilitiesService.Failed += new EventHandler<ServiceFailedEventArgs>(findclosestFacilitiesService_Failed);
            findclosestFacilitiesService.ProcessCompleted += new EventHandler<FindClosestFacilitiesEventArgs>(findclosestFacilitiesService_ProcessCompleted);
        }

        private void findclosestFacilitiesService_ProcessCompleted(object sender, FindClosestFacilitiesEventArgs e)
        {
            //路径样式
            PredefinedLineStyle simpleLineStyle = new PredefinedLineStyle();
            simpleLineStyle.Stroke = new SolidColorBrush(Colors.Blue);
            simpleLineStyle.StrokeThickness = 2;

            if (e.Result != null && e.Result.FacilityPathList != null)
            {
                foreach (ServerPath path in e.Result.FacilityPathList)
                {
                    featuresLayer.Features.Add(new Feature { Geometry = path.Route, Style = simpleLineStyle  });
                }
            }
        }

        private void findclosestFacilitiesService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //清除图层上的全部元素并清空点坐标记录
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayerF.Children.Clear();
            elementsLayerE.Children.Clear();
            featuresLayer.ClearFeatures();
            points.Clear();
            eventp = Point2D.Empty;
            flag = false;
        }
    }
}
