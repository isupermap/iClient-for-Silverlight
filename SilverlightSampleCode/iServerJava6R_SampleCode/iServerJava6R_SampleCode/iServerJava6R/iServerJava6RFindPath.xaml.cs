﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RFindPath : Page
    {
        private ElementsLayer elementsLayer;
        private List<Point2D> points = new List<Point2D>();
        private List<Point2D> barrierPoints = new List<Point2D>();
        private FeaturesLayer featuresLayer;
        private int i = 0;

        public iServerJava6RFindPath()
        {
            InitializeComponent();
            elementsLayer = this.MyMap.Layers["MyElementsLayer"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        //清除 ElementsLayer 和 FeaturesLayer 图层上全部元素
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();
            points.Clear();
            barrierPoints.Clear();
            resultPanel.Visibility = Visibility.Collapsed;
            i = 0;
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //选择路径途经结点或障碍点
        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            if (this.pathPonit.IsChecked.Value)
            {
                //标记途经结点顺序
                i++;
                //将结点以图钉样式加到ElementsLayer上
                Pushpin pushpin = new Pushpin()
                {
                    Location = e.Geometry.Bounds.Center,
                    Content = i.ToString(System.Globalization.CultureInfo.InvariantCulture),
                };
                elementsLayer.AddChild(pushpin);
                //用points数组记录结点坐标
                points.Add(pushpin.Location);
            }

            if (this.barrierPoint.IsChecked.Value)
            {
                Pushpin pushpin = new Pushpin()
                {
                    Location = e.Geometry.Bounds.Center,
                    Content = "Stop",
                    FontSize = 8,
                    Background = new SolidColorBrush(Colors.Black),
                };
                elementsLayer.AddChild(pushpin);
                //用MyBarrierPoint数组记录结点坐标
                barrierPoints.Add(pushpin.Location);
            }
        }

        //最佳路径分析
        private void PathAnalyst_Click(object sender, RoutedEventArgs e)
        {
            if (points.Count < 2)
            {
                MessageBox.Show("请指定途经点");
                return;
            }

            if (featuresLayer.Features.Count > 0)
            {
                featuresLayer.Features.Clear();
            }
            //定义 Point2D 类型的参数
            FindPathParameters<Point2D> paramPoint2D = new FindPathParameters<Point2D>
            {
                Nodes = points,
                HasLeastEdgeCount = true,
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost",
                    BarrierPoints = barrierPoints,
                }
            };

            //与服务器交互
            FindPathService findPathService = new FindPathService("http://localhost:8090/iserver/services/transportationanalyst-sample/rest/networkanalyst/RoadNet@Changchun");
            findPathService.ProcessAsync(paramPoint2D);
            findPathService.ProcessCompleted += new EventHandler<FindPathEventArgs>(findPathService_ProcessCompleted);
            findPathService.Failed += new EventHandler<ServiceFailedEventArgs>(findPathService_Failed);
        }

        //服务器返回结果，将最佳路径显示在客户端
        private void findPathService_ProcessCompleted(object sender, FindPathEventArgs e)
        {
            //路径样式
            PredefinedLineStyle simpleLineStyle = new PredefinedLineStyle();
            simpleLineStyle.Stroke = new SolidColorBrush(Colors.Blue);
            simpleLineStyle.StrokeThickness = 2;

            if (e.Result != null)
            {
                if (e.Result.PathList != null)
                {
                    foreach (ServerPath p in e.Result.PathList)
                    {
                        //将要素添加到图层
                        featuresLayer.Features.Add(new Feature { Geometry = p.Route, Style = simpleLineStyle });
                        this.length.Text = "路线长度：" + p.Route.Length.ToString(System.Globalization.CultureInfo.InvariantCulture);
                        this.cost.Text = "花费:" + p.Weight.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    }
                    resultPanel.Visibility = Visibility.Visible;
                }
                else
                {
                    MessageBox.Show("此设置下无可用路径");
                }
            }
        }

        //服务器计算失败提示失败信息
        private void findPathService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }
    }
}
