﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using SuperMap.Web.OGC;

namespace iServerJava6R_SampleCode
{
    public partial class TDWMSLayer : Page
    {
        private FeaturesLayer featuresLayer;
        string url = "http://localhost:8090/iserver/services/data-world/wfs100";
        public TDWMSLayer()
        {
            InitializeComponent();
            featuresLayer = this.MyMap.Layers["featuresLayer"] as FeaturesLayer;
        }

        private void getWFS_Click(object sender, RoutedEventArgs e)
        {
            GetWFSCapabilities getCapabilities = new GetWFSCapabilities(url);
            getCapabilities.ProcessCompleted += new EventHandler<ResultEventArgs<List<WFSFeatureType>>>(getCapabilities_ProcessCompleted);
            getCapabilities.ProcessAsync();
            getCapabilities.Failed += new EventHandler<FailedEventArgs>(ServiceError_Failed);
        }

        private void getCapabilities_ProcessCompleted(object sender, ResultEventArgs<List<WFSFeatureType>> e)
        {
            if (e.Result != null && e.Result.Count > 9)
            {
                GetWFSDescribeFeatureType featureType = new GetWFSDescribeFeatureType(url);
                featureType.TypeNames.Add("World:Countries");
                featureType.ProcessAsync();
                featureType.ProcessCompleted += new EventHandler<ResultEventArgs<Dictionary<string, List<WFSFeatureDescription>>>>(featureType_ProcessCompleted);
                featureType.Failed += new EventHandler<FailedEventArgs>(ServiceError_Failed);
            }
        }

        private void featureType_ProcessCompleted(object sender, ResultEventArgs<Dictionary<string, List<WFSFeatureDescription>>> e)
        {
            foreach (var item in e.Result)
            {
                if (item.Key == "http://www.supermap.com/World")
                {
                    GetWFSFeature getWFSFeature = new GetWFSFeature(url)
                    {
                        MaxFeatures = 5,
                        FeatureNS = item.Key
                    };
                    WFSFeatureDescription typeCountries = new WFSFeatureDescription
                    {
                        TypeName = item.Value[0].TypeName,
                        SpatialProperty = item.Value[0].SpatialProperty,
                    };
                    typeCountries.Properties.Add("COUNTRY");
                    typeCountries.Properties.Add("CAPITAL");
                    getWFSFeature.FeatureDescriptions.Add(typeCountries);
                    getWFSFeature.ProcessAsync();
                    getWFSFeature.ProcessCompleted += new EventHandler<ResultEventArgs<GetWFSFeatureResult>>(getWFSFeature_ProcessCompleted);
                    getWFSFeature.Failed += new EventHandler<FailedEventArgs>(ServiceError_Failed);
                }
            }
        }

        private void getWFSFeature_ProcessCompleted(object sender, ResultEventArgs<GetWFSFeatureResult> e)
        {
            foreach (var key in e.Result.FeaturePair)
            {
                featuresLayer.AddFeatureSet(key.Value);
                foreach (Feature feature in key.Value)
                {
                    feature.ToolTip = new TextBlock { Text = "Country: " + feature.Attributes["COUNTRY"] + "\n" + "Capital: " + feature.Attributes["CAPITAL"] };
                }
            }
        }

        private void ServiceError_Failed(object sender, FailedEventArgs e)
        {
            if (e.Error != null)
                MessageBox.Show(e.Error.Message + e.Error.OtherMessage.Message);
        }
    }
}
