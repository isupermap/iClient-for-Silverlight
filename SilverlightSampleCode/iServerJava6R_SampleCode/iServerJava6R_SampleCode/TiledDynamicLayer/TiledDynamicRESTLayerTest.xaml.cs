﻿using System.Windows.Controls;

namespace iServerJava6R_SampleCode
{
    public partial class TiledDynamicRESTLayerTest : Page
    {
        public TiledDynamicRESTLayerTest()
        {
            InitializeComponent();
            MyMap.ViewBoundsChanged += new System.EventHandler<SuperMap.Web.Mapping.ViewBoundsEventArgs>(MyMap_ViewBoundsChanged);
            MyMap.MouseMove += new System.Windows.Input.MouseEventHandler(MyMap_MouseMove);
        }

        void MyMap_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.cor.Text = MyMap.ScreenToMap(e.GetPosition(MyMap)).ToString();
        }

        void MyMap_ViewBoundsChanged(object sender, SuperMap.Web.Mapping.ViewBoundsEventArgs e)
        {
            this.scale.Text = "Scale：" + MyMap.Scale.ToString(System.Globalization.CultureInfo.InvariantCulture);
            this.center.Text = "Center：" + MyMap.Center.ToString();
            this.viewbounds.Text = "ViewBounds：" + MyMap.ViewBounds.ToString();
        }
    }
}
