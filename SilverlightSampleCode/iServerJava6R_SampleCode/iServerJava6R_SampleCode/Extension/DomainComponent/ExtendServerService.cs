﻿using System;
using SuperMap.Web.iServerJava6R.Resources;
using SuperMap.Web.Service;

namespace serverExtend
{
    public class ExtendServerService : ServiceBase
    {
        public ExtendServerService()
        {
        }

        public ExtendServerService(string url)
            : base(url)
        {
        }

        public void ProcessAsync(ExtendServerParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        public void ProcessAsync(ExtendServerParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            base.Url += string.Format(".json?arg0={0}", parameters.Arg);

            base.SubmitRequest(base.Url, null,
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            ExtendServerResult result = ExtendServerResult.FromJson(e.Result);
            lastResult = result;
            ExtendServerEventArgs args = new ExtendServerEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(ExtendServerEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

        public event EventHandler<ExtendServerEventArgs> ProcessCompleted;

        private ExtendServerResult lastResult;
        public ExtendServerResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
