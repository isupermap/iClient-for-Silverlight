﻿using SuperMap.Web.Service;

namespace serverExtend
{
    public class ExtendServerEventArgs : ServiceEventArgs
    {
        public ExtendServerEventArgs(ExtendServerResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }

        public ExtendServerResult Result { get; private set; }
        public string OriginResult { get; private set; }
    }
}
