﻿using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class CustomLineStyle : Page
    {
        public CustomLineStyle()
        {
            InitializeComponent();
            FeaturesLayer featureslayer = MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            for (int i = 1; i <= 10; i++)
            {
                int j = i * 7 + 70;
                Feature f = new Feature();
                f.Geometry =
                    new GeoLine
                    {
                        Parts = new System.Collections.ObjectModel.ObservableCollection<Point2DCollection>
                        {
                            new Point2DCollection
                            {
                                new Point2D(j,10),
                                new Point2D(j,50)
                            }
                        }
                    };
                f.Style = LayoutRoot.Resources[string.Format(System.Globalization.CultureInfo.InvariantCulture, "MyLineStyle{0}", i)] as LineStyle;
                featureslayer.Features.Add(f);
            }
        }
    }
}
