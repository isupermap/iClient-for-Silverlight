﻿using SuperMap.Web.Core;

namespace iServerJava6R_SampleCode
{
    public static class CheckInRegion
    {
        public static bool CheckRegion(GeoRegion region, Point2D point)
        {
            Point2DCollection points = region.Parts[0];

            double pValue = double.NaN;
            int i = 0;
            int j = 0;

            double yValue = double.NaN;
            int m = 0;
            int n = 0;

            double iPointX = double.NaN;
            double iPointY = double.NaN;
            double jPointX = double.NaN;
            double jPointY = double.NaN;

            int k = 0;
            int p = 0;

            yValue = points[0].Y - points[(points.Count - 1)].Y;
            if (yValue < 0)
            {
                p = 1;
            }
            else if (yValue > 0)
            {
                p = 0;
            }
            else
            {
                m = points.Count - 2;
                n = m + 1;
                while (points[m].Y == points[n].Y)
                {
                    m--;
                    n--;
                    if (m == 0)
                    {
                        return true;
                    }
                }
                yValue = points[n].Y - points[m].Y;
                if (yValue < 0)
                {
                    p = 1;
                }
                else if (yValue > 0)
                {
                    p = 0;
                }
            }


            //使多边形封闭
            int count = points.Count;
            i = 0;
            j = count - 1;
            while (i < count)
            {
                iPointX = points[j].X;
                iPointY = points[j].Y;
                jPointX = points[i].X;
                jPointY = points[i].Y;
                if (point.Y > iPointY)
                {
                    if (point.Y < jPointY)
                    {
                        pValue = (point.Y - iPointY) * (jPointX - iPointX) / (jPointY - iPointY) + iPointX;
                        if (point.X < pValue)
                        {
                            k++;
                        }
                        else if (point.X == pValue)
                        {
                            return true;
                        }
                    }
                    else if (point.X == jPointY)
                    {
                        p = 0;
                    }
                }
                else if (point.Y < iPointY)
                {
                    if (point.Y > jPointY)
                    {
                        pValue = (point.Y - iPointY) * (jPointX - iPointX) / (jPointY - iPointY) + iPointX;
                        if (point.X < pValue)
                        {
                            k++;
                        }
                        else if (point.X == pValue)
                        {
                            return true;
                        }
                    }
                    else if (point.Y == jPointY)
                    {
                        p = 1;
                    }
                }
                else
                {
                    if (point.X == iPointX)
                    {
                        return true;
                    }
                    if (point.Y < jPointY)
                    {
                        if (p != 1)
                        {
                            if (point.X < iPointX)
                            {
                                k++;
                            }
                        }
                    }
                    else if (point.Y > jPointY)
                    {
                        if (p > 0)
                        {
                            if (point.X < iPointX)
                            {
                                k++;
                            }
                        }
                    }
                    else
                    {
                        if (point.X > iPointX && point.X <= jPointX)
                        {
                            return true;
                        }
                        if (point.X < iPointX && point.X >= jPointX)
                        {
                            return true;
                        }
                    }
                }
                j = i;
                i++;
            }

            if (k % 2 != 0)
            {
                return true;
            }
            return false;
        }
    }
}
