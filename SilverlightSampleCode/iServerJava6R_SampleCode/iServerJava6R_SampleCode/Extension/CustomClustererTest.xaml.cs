﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using SuperMap.Web.Clustering;
using SuperMap.Web.Utilities;
using System.Windows.Markup;

namespace iServerJava6R_SampleCode
{
    public partial class CustomClustererTest : Page
    {
        public CustomClustererTest()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(CustomClustererTest_Loaded);
        }

        void CustomClustererTest_Loaded(object sender, RoutedEventArgs e)
        {

            FeaturesLayer featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            featuresLayer.Clusterer = new CustomClusterer() { AggregateColumn = "id", StyleScale = 0.01 };

            for (int i = 0; i < 1000; i++)
            {
                Feature feature = new Feature();
                feature.Geometry = new GeoPoint(random.NextDouble() * (ChinaRight - ChinaLeft) + ChinaLeft, random.NextDouble() * (ChinaTop - ChinaBottom) + ChinaBottom);
                PredefinedMarkerStyle ps = new PredefinedMarkerStyle();
                ps.Color = new SolidColorBrush(Colors.Red);
                ps.Size = 10;
                feature.Style = ps;
                feature.ToolTip = new TextBlock() { Text = i.ToString(System.Globalization.CultureInfo.InvariantCulture) };
                feature.Attributes.Add("id", i);
                featuresLayer.Features.Add(feature);
            }
        }

        private Random random = new Random();
        private const double ChinaLeft = 70;
        private const double ChinaRight = 130;
        private const double ChinaBottom = 4;
        private const double ChinaTop = 50;

    }

    public class CustomClusterer : FeaturesClusterer
    {
        public CustomClusterer()
        {
            StyleScale = 1;
            Radius = 50;
        }

        protected override Feature OnCreateFeature(FeatureCollection cluster, GeoPoint center, int maxClusterCount)
        {
            double sum = 0;
            Feature feature = null;
            foreach (Feature item in cluster)
            {
                if (item.Attributes.ContainsKey(AggregateColumn))
                {
                    sum += Convert.ToDouble(item.Attributes[AggregateColumn]);
                }
            }
            double size = (sum + 450) / 30;
            size = Math.Log(sum * StyleScale / 10) * 10 + 20;
            if (size < 12)
            {
                size = 12;
            }
            CustomClusterStyle s = new CustomClusterStyle();
            feature = new Feature() { Style = new CustomClusterStyle() { Size = size }, Geometry = center };
            feature.Attributes.Add("Color", InterPlateColor(size - 12, 100));
            feature.Attributes.Add("Size", size);
            feature.Attributes.Add("Count", cluster.Count);

            return feature;
        }

        private static Brush InterPlateColor(double value, double max)
        {
            value = (int)Math.Round(value * 255.0 / max);
            if (value > 255)
            {
                value = 255;
            }
            else if (value < 0)
            {
                value = 0;
            }
            return new SolidColorBrush(Color.FromArgb(127, 255, (byte)value, 0));

        }

        public string AggregateColumn { get; set; }
        public double StyleScale { get; set; }
    }

    public class CustomClusterStyle : MarkerStyle
    {
        public CustomClusterStyle()
        {
            string xaml = StyleUtility.XamlFileToString("/iServerJava6R_SampleCode;component/Extension/CustomClusterStyle.xaml");
            base.ControlTemplate = XamlReader.Load(xaml) as ControlTemplate;
        }

        public double Size { get; set; }
    }


}
