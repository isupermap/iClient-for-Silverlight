﻿using System.Windows.Controls;
using System.Windows.Markup;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;

namespace iServerJava6R_SampleCode
{
    public class CustomClusterStyle : MarkerStyle
    {
        public CustomClusterStyle()
        {
            string xaml = StyleUtility.XamlFileToString("/iServerJava6R_SampleCode;component/Extension/CustomClusterStyle.xaml");
            base.ControlTemplate = XamlReader.Load(xaml) as ControlTemplate;
        }

        public double Size { get; set; }
    }
}
