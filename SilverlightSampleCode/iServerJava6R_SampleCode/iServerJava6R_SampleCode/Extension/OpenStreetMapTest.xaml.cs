﻿using System.Windows.Controls;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;
using System.Windows;

namespace iServerJava6R_SampleCode
{
    public partial class OpenStreetMapTest : Page
    {
        public OpenStreetMapTest()
        {
            InitializeComponent();
        }
    }

    public enum OMapsType
    {
        Mapnik,
        Osmarender,
        CycleMap,
        NoName
    }

    public class TiledOpenStreetMapsLayer : TiledCachedLayer
    {
        private string[] subDomains = { "a", "b", "c" };
        private const string MapnikUri = "http://{0}.tile.openstreetmap.org/{1}/{2}/{3}.png";
        private const string OsmarenderUri = "http://{0}.tah.openstreetmap.org/Tiles/tile/{1}/{2}/{3}.png";
        private const string CycleMapUri = "http://{0}.andy.sandbox.cloudmade.com/tiles/cycle/{1}/{2}/{3}.png";
        private const string NoNameUri = "http://{0}.tile.cloudmade.com/fd093e52f0965d46bb1c6c6281022199/3/256/{1}/{2}/{3}.png";

        private const double CornerCoordinate = 20037508.3427892;

        public TiledOpenStreetMapsLayer()
        {
        }

        public override string GetTileUrl(int indexX, int indexY, int level)
        {
            string subDomain = subDomains[(indexX + indexY + level) % subDomains.Length];

            return string.Format(System.Globalization.CultureInfo.InvariantCulture, this.Url, subDomain, level, indexX, indexY);
        }

        public override void Initialize()
        {
            this.Bounds = new Rectangle2D(-CornerCoordinate, -CornerCoordinate, CornerCoordinate, CornerCoordinate);
            this.TileSize = 256;

            double res = CornerCoordinate * 2 / 256;
            double[] resolutions = new double[8];
            for (int i = 0; i < resolutions.Length; i++)
            {
                resolutions[i] = res;
                res *= 0.5;
            }
            this.Resolutions = resolutions;

            switch (this.MapType)
            {
                case OMapsType.Mapnik:
                    this.Url = MapnikUri;
                    break;
                case OMapsType.Osmarender:
                    this.Url = OsmarenderUri;
                    break;
                case OMapsType.CycleMap:
                    this.Url = CycleMapUri;
                    break;
                case OMapsType.NoName:
                    this.Url = NoNameUri;
                    break;
            }
            base.Initialize();
        }

        public OMapsType MapType
        {
            get { return (OMapsType)GetValue(MapTypeProperty); }
            set { SetValue(MapTypeProperty, value); }
        }

        public static readonly DependencyProperty MapTypeProperty =
            DependencyProperty.Register("MapType", typeof(OMapsType), typeof(TiledOpenStreetMapsLayer), new PropertyMetadata((new PropertyChangedCallback(OnMapTypePropertyChanged))));

        private static void OnMapTypePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TiledOpenStreetMapsLayer layer = d as TiledOpenStreetMapsLayer;
            if (layer.IsInitialized)
            {
                layer.ChangeTileSource();
            }
        }

        private void ChangeTileSource()
        {
            switch (this.MapType)
            {
                case OMapsType.Mapnik:
                    this.Url = MapnikUri;
                    break;
                case OMapsType.Osmarender:
                    this.Url = OsmarenderUri;
                    break;
                case OMapsType.CycleMap:
                    this.Url = CycleMapUri;
                    break;
                case OMapsType.NoName:
                    this.Url = NoNameUri;
                    break;
            }
            if (!base.IsInitialized)
            {
                base.Initialize();
            }
            else
            {
                base.Refresh();
            }
        }
    }
}
