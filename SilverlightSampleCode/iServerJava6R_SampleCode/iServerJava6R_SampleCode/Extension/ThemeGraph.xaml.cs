﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;
using SuperMap.Web.Core;
using Visifire.Charts;
using Visifire.Commons;

namespace iServerJava6R_SampleCode
{
    public partial class iServerJava6RThemeGraph : UserControl
    {
        //初始化元素图层
        private ElementsLayer elementsLayer;

        //设置饼状图所在位置数组
        List<Point2D> rects = new List<Point2D>
        {
           new Point2D(112.12,58.52), 
           new Point2D(100.85,36.19),
           new Point2D(5.46,23.45),
           new Point2D(-110.55,40.61),
           new Point2D(-55.53,-15.15),
           new Point2D(133.48,-25.20),
           new Point2D(30.88,15.02),
           new Point2D(-45.55,70.76)
        };
        //设置人口类型
        String[] pop = { "成人", "老人", "儿童" };
        //设置成人、老人、儿童分布百分比数组
        int[,] points = new int[,] { { 50, 30, 20 }, { 60, 15, 25 }, { 65, 20, 15 }, { 50, 20, 30 }, { 80, 10, 10 }, { 70, 10, 20 }, { 75, 15, 10 }, { 55, 20, 25 } };
        public iServerJava6RThemeGraph()
        {
            InitializeComponent();
            elementsLayer = this.MyMap.Layers["MyElementsLayer"] as ElementsLayer;
        }
        Random random = new Random();
        //饼状图
        private void PieChart_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 8; i++)
            {
                Chart chart = new Chart();
                //背景不显示
                chart.LightingEnabled = false;
                //图表边线不显示
                chart.BorderThickness = new Thickness(0, 0, 0, 0);
                //去除水印
                chart.Watermark = false;
                //设置背景颜色为透明
                chart.Background = new SolidColorBrush(Colors.Transparent);
                //设置图表宽度
                chart.Width = 170;
                chart.Height = 170;
                DataSeries dataSeries = new DataSeries();
                //设置图表类型为饼状图
                dataSeries.RenderAs = RenderAs.Pie;
                dataSeries.ShadowEnabled = false;
                int pointsNumber = 3;
                for (int loopIndex = 0; loopIndex < pointsNumber; loopIndex++)
                {
                    DataPoint dataPoint = new DataPoint();
                    dataPoint.AxisXLabel = pop[loopIndex];
                    dataPoint.YValue = points[i, loopIndex];
                    dataSeries.DataPoints.Add(dataPoint);
                }

                chart.Series.Add(dataSeries);
                elementsLayer.AddChild(chart, rects[i]);
            }
        }
        //柱状图
        private void barGraph_Click(object sender, RoutedEventArgs e)
        {
            graphContainer.Children.Clear();
            Chart chart = new Chart();
            chart.Watermark = false;
            chart.View3D = true;
            chart.Width = 300;
            chart.Height = 200;
            Title title = new Title();
            title.Text = "人口类别统计图";
            chart.Titles.Add(title);
            for (int i = 0; i < 8; i++)
            {
                DataSeries dataSeries = new DataSeries();
                dataSeries.ShowInLegend = false;
                dataSeries.RenderAs = RenderAs.Column;             
                for (int loopIndex = 0; loopIndex < 3; loopIndex++)
                {
                    DataPoint dataPoint = new DataPoint();
                    dataPoint.AxisXLabel = pop[loopIndex];
                    dataPoint.YValue = points[i, loopIndex];
                    dataSeries.DataPoints.Add(dataPoint);
                }
                chart.Series.Add(dataSeries);
            }
            //将柱状图添加到 Grid 控件以固定位置
            graphContainer.VerticalAlignment = VerticalAlignment.Top;
            graphContainer.HorizontalAlignment = HorizontalAlignment.Left;
            graphContainer.Children.Add(chart);
        }
        //清除统计图
        private void clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            graphContainer.Children.Clear();
        }
    }
}



