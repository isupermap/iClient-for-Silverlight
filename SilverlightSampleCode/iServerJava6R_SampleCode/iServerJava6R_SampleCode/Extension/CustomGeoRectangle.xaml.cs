﻿using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using System;

namespace iServerJava6R_SampleCode
{
    public partial class CustomGeoRectangle : Page
    {
        public CustomGeoRectangle()
        {
            InitializeComponent();

            FeaturesLayer featuresLayer = MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            Feature feature = new Feature();

            feature.Geometry = new GeoRectangle(new Point2D(10, -10), new Point2D(30, 30));
            featuresLayer.Features.Add(feature);
        }

        public class GeoRectangle : GeoRegion
        {
            public GeoRectangle(Point2D point1, Point2D point2)
            {
                CreateRectangle(point1, point2);
            }

            private void CreateRectangle(Point2D p1, Point2D p2)
            {
                double xmin = Math.Min(p1.X, p2.X);
                double xmax = Math.Max(p1.X, p2.X);
                double ymin = Math.Min(p1.Y, p2.Y);
                double ymax = Math.Max(p1.Y, p2.Y);

                Point2DCollection ps = new Point2DCollection();
                ps.Add(new Point2D(xmin, ymin));
                ps.Add(new Point2D(xmin, ymax));
                ps.Add(new Point2D(xmax, ymax));
                ps.Add(new Point2D(xmax, ymin));
                ps.Add(new Point2D(xmin, ymin));

                this.Parts.Add(ps);
            }
        }

    }
}
