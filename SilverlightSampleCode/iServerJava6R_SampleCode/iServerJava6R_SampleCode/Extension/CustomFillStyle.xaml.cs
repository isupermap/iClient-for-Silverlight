﻿using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class CustomFillStyle : Page
    {
        public CustomFillStyle()
        {
            InitializeComponent();
            FeaturesLayer featureslayer = MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            for (int i = 1; i <= 10; i++)
            {
                int j = i * 8 + 60;
                Feature f = new Feature();
                f.Geometry =
                    new GeoRegion
                    {
                        Parts = new System.Collections.ObjectModel.ObservableCollection<Point2DCollection>
                        {
                            new Point2DCollection
                            {
                                new Point2D(j,20),
                                new Point2D(j,40),
                                new Point2D(j+5,40),
                                new Point2D(j+5,20),
                                 new Point2D(j,20)
                            }
                        }
                    };
                f.Style = LayoutRoot.Resources[string.Format(System.Globalization.CultureInfo.InvariantCulture, "MyFillStyle{0}", i)] as FillStyle;
                featureslayer.Features.Add(f);
            }
        }
    }
}
