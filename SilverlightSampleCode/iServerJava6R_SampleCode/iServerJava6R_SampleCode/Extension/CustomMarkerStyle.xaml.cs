﻿using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class CustomMarkerStyle : Page
    {
        public CustomMarkerStyle()
        {
            InitializeComponent();
            FeaturesLayer featureslayer = MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;

            for (int i = 1; i <= 27; i++)
            {
                double x;
                double y;
                if (i <= 9)
                {
                    x = 80 + 5 * i;
                    y = 40;
                }
                else if (i > 9 && i <= 18)
                {
                    x = 80 + 5 * (i - 9);
                    y = 30;
                }
                else
                {
                    x = 80 + 5 * (i - 18);
                    y = 20;
                }

                Feature f = new Feature();
                f.Geometry = new GeoPoint(x, y);
                f.Style = LayoutRoot.Resources[string.Format(System.Globalization.CultureInfo.InvariantCulture, "MyMarkerStyle{0}", i)] as MarkerStyle;
                featureslayer.Features.Add(f);
            }
        }
    }
}
