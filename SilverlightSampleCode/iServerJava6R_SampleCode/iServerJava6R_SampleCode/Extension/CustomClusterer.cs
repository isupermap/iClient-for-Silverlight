﻿using System;
using System.Windows.Media;
using SuperMap.Web.Clustering;
using SuperMap.Web.Core;

namespace iServerJava6R_SampleCode
{
    public class CustomClusterer : FeaturesClusterer
    {
        public CustomClusterer()
        {
            StyleScale = 1;
            Radius = 50;
        }

        protected override Feature OnCreateFeature(FeatureCollection cluster, GeoPoint center, int maxClusterCount)
        {
            double sum = 0;
            Feature feature = null;
            foreach (Feature item in cluster)
            {
                if (item.Attributes.ContainsKey(AggregateColumn))
                {
                    sum += Convert.ToDouble(item.Attributes[AggregateColumn]);
                }
            }
            double size = (sum + 450) / 30;
            size = Math.Log(sum * StyleScale / 10) * 10 + 20;
            if (size < 12)
            {
                size = 12;
            }
            CustomClusterStyle s = new CustomClusterStyle();
            feature = new Feature() { Style = new CustomClusterStyle() { Size = size }, Geometry = center };
            feature.Attributes.Add("Color", InterPlateColor(size - 12, 100));
            feature.Attributes.Add("Size", size);
            feature.Attributes.Add("Count", cluster.Count);

            return feature;
        }

        private static Brush InterPlateColor(double value, double max)
        {
            value = (int)Math.Round(value * 255.0 / max);
            if (value > 255)
            {
                value = 255;
            }
            else if (value < 0)
            {
                value = 0;
            }
            return new SolidColorBrush(Color.FromArgb(127, 255, (byte)value, 0));

        }

        public string AggregateColumn { get; set; }
        public double StyleScale { get; set; }
    }
}
