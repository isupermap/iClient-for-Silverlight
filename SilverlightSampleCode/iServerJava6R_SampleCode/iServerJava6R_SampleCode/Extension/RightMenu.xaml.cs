﻿using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SuperMap.Web.Actions;


namespace iServerJava6R_SampleCode
{
    public partial class RightMenu : Page
    {
        public RightMenu()
        {
            InitializeComponent();
            this.ShowVersion();
        }
        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap.Action = new Pan(this.MyMap, Cursors.Hand);
        }
        //拉框缩小
        private void ZoomIn_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap.Action = new ZoomIn(this.MyMap);
        }
        //拉框放大
        private void ZoomOut_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap.Action = new ZoomOut(this.MyMap);
        }
        //版本号
        private void ShowVersion()
        {
            this.sv.Header = "SuperMap iClient 7C for Silverlight";
        }
    }
}
