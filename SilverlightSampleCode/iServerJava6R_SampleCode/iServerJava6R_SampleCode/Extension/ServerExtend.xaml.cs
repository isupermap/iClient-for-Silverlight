﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using serverExtend;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class ServerExtend : Page
    {
        public ServerExtend()
        {
            InitializeComponent();

            string url = "http://localhost:8090/iserver/services/Temperature/rest/domainComponents/Temperature/getTemperatureResult";
            ExtendServerParameters param = new ExtendServerParameters() { Arg = "北京" };
            ExtendServerService service = new ExtendServerService(url);
            service.ProcessCompleted += new EventHandler<ExtendServerEventArgs>(service_ProcessCompleted);
            service.ProcessAsync(param);
        }

        private void service_ProcessCompleted(object sender, ExtendServerEventArgs e)
        {
            Feature feature = new Feature();
            feature.Geometry = new GeoPoint(116, 40);
            feature.Style = MyMarkerStyle;

            Grid grid = new Grid();
            grid.Background = new SolidColorBrush(Color.FromArgb(99, 00, 00, 00));
            StackPanel sp = new StackPanel();
            sp.Margin = new Thickness(5);
            sp.Children.Add(new TextBlock { Text = e.Result.Extendresult, Foreground = new SolidColorBrush(Colors.Red), FontSize = 20 });
            grid.Children.Add(sp);

            feature.ToolTip = grid;
            FeaturesLayer layer = new FeaturesLayer();
            layer.Features.Add(feature);
            MyMap.Layers.Add(layer);
        }
    }
}
