﻿using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Actions;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;
using System.Collections.Generic;
using System;
using System.Windows.Input;
using System.Windows.Media;

namespace iServerJava6R_SampleCode
{
    public partial class CustomDrawStar : Page
    {
        private ElementsLayer elementslayer;

        public CustomDrawStar()
        {
            InitializeComponent();
            elementslayer = MyMap.Layers["MyElementsLayer"] as ElementsLayer;
        }

        private void pan_Click(object sender, RoutedEventArgs e)
        {
            MyMap.Action = new Pan(MyMap);
        }

        private void Mybtn_Click(object sender, RoutedEventArgs e)
        {
            DrawStar star = new DrawStar(MyMap);
            MyMap.Action = star;
            star.DrawCompleted += new System.EventHandler<DrawEventArgs>(star_DrawCompleted);
        }

        private void star_DrawCompleted(object sender, DrawEventArgs e)
        {
            elementslayer.AddChild(e.Element);
        }
    }

    public class DrawStar : MapAction, IDrawStyle
    {
        private Point2D startPt = Point2D.Empty;
        private PolygonElement pentagram;
        private Point2DCollection points;

        private bool isActivated;
        private bool isDrawing;

        private List<PolygonElement> oldPentagrams;
        public event EventHandler<DrawEventArgs> DrawCompleted;

        #region 变量
        private double om;
        private double a1m;
        private double a0x;
        private double a0y;
        private double ox;
        private double oy;
        private Point2D o;
        private Point2D a0;
        private Point2D a1;
        private Point2D a2;
        private Point2D a3;
        private Point2D a4;

        private double maxX;
        private double minX;
        private double maxY;
        private double minY;
        #endregion

        public DrawStar(Map map)
            : this(map, Cursors.Hand)
        {
        }

        public DrawStar(Map map, Cursor cursor)
        {

            Name = "DrawStar";
            Map = map;

            Stroke = new SolidColorBrush(Colors.Red);
            StrokeThickness = 2;
            Fill = new SolidColorBrush(Colors.Red);
            FillRule = FillRule.Nonzero;
            Opacity = 1;

            Map.Cursor = cursor;

        }

        private void Activate()
        {
            pentagram = new PolygonElement();
            #region 所有风格的控制
            pentagram.Stroke = this.Stroke;
            pentagram.StrokeThickness = this.StrokeThickness;
            pentagram.Fill = this.Fill;
            pentagram.FillRule = this.FillRule;
            pentagram.Opacity = this.Opacity;
            pentagram.StrokeMiterLimit = this.StrokeMiterLimit;
            pentagram.StrokeDashOffset = this.StrokeDashOffset;
            pentagram.StrokeDashArray = this.StrokeDashArray;
            pentagram.StrokeDashCap = this.StrokeDashCap;
            pentagram.StrokeEndLineCap = this.StrokeEndLineCap;
            pentagram.StrokeLineJoin = this.StrokeLineJoin;
            pentagram.StrokeStartLineCap = this.StrokeStartLineCap;
            #endregion
            a0 = new Point2D();
            a1 = new Point2D();
            a2 = new Point2D();
            a3 = new Point2D();
            a4 = new Point2D();

            points = new Point2DCollection();
            pentagram.Point2Ds = points;
            oldPentagrams = new List<PolygonElement>();

            DrawLayer = new ElementsLayer();
            Map.Layers.Add(DrawLayer);

            isActivated = true;
            isDrawing = true;
        }

        public override void Deactivate()
        {
            isActivated = false;
            isDrawing = false;
            oldPentagrams = null;
            pentagram = null;
            points = null;
            if (DrawLayer != null)
            {
                DrawLayer.Children.Clear();
            }
            if (Map != null)
            {
                Map.Layers.Remove(DrawLayer);
            }
        }

        public override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            startPt = Map.ScreenToMap(e.GetPosition(Map));
            if (!isActivated)
            {
                Activate();
            }
            e.Handled = true;
            base.OnMouseLeftButtonDown(e);
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (isDrawing)
            {
                for (int i = 0; i < oldPentagrams.Count - 1; i++)
                {
                    DrawLayer.Children.Remove(oldPentagrams[i]);
                }

                Point2D item = Map.ScreenToMap(e.GetPosition(Map));

                #region 五角星算法
                maxX = Math.Max(startPt.X, item.X);
                minX = Math.Min(startPt.X, item.X);
                maxY = Math.Max(startPt.Y, item.Y);
                minY = Math.Min(startPt.Y, item.Y);

                om = Math.Cos((2 * Math.PI) / 5) * (maxY - minY) / 2;
                a1m = Math.Sin((2 * Math.PI) / 5) * (maxY - minY) / 2;
                a0x = (maxY - minY) / 2 + minX;
                a0y = maxY;
                ox = (maxY - minY) / 2 + minX;
                oy = maxY + (minY - maxY) / 2;
                o = new Point2D(ox, oy);

                a0 = new Point2D(a0x, a0y);
                a1 = new Point2D(ox - a1m, oy + om);
                a2 = new Point2D(ox + a1m, oy + om);
                a3 = new Point2D(ox - Math.Sin(Math.PI / 5) * (maxY - minY) / 2, oy - Math.Cos(Math.PI / 5) * (maxY - minY) / 2);
                a4 = new Point2D(ox + Math.Sin(Math.PI / 5) * (maxY - minY) / 2, oy - Math.Cos(Math.PI / 5) * (maxY - minY) / 2);

                points.Clear();
                points.Add(a0);
                points.Add(a3);
                points.Add(a2);
                points.Add(a1);
                points.Add(a4);

                #endregion
                //pentagram.Point2Ds = points;
                DrawLayer.Children.Add(pentagram);
                oldPentagrams.Add(pentagram);//等待删除
            }

            base.OnMouseMove(e);
        }

        public override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (this.points != null)
            {
                GeoRegion geoRegion = new GeoRegion();
                points.Add(a0);
                geoRegion.Parts.Add(points);

                PolygonElement pPen = new PolygonElement()
                {
                    Point2Ds = points,
                    #region 所有风格的控制
                    Stroke = this.Stroke,
                    StrokeThickness = this.StrokeThickness,
                    StrokeMiterLimit = this.StrokeMiterLimit,
                    StrokeDashOffset = this.StrokeDashOffset,
                    StrokeDashArray = this.StrokeDashArray,
                    StrokeDashCap = this.StrokeDashCap,
                    StrokeEndLineCap = this.StrokeEndLineCap,
                    StrokeLineJoin = this.StrokeLineJoin,
                    StrokeStartLineCap = this.StrokeStartLineCap,
                    Opacity = this.Opacity,
                    Fill = this.Fill,
                    FillRule = this.FillRule
                    #endregion
                };

                DrawEventArgs args2 = new DrawEventArgs
                {
                    DrawName = Name,
                    Element = pPen,
                    Geometry = geoRegion
                };

                Deactivate();
                OnDrawComplete(args2);
            }
            e.Handled = true;
            base.OnMouseLeftButtonUp(e);
        }

        public override void OnDblClick(MouseButtonEventArgs e)
        {
            e.Handled = true;
            base.OnDblClick(e);
        }

        private void OnDrawComplete(DrawEventArgs args)
        {
            if (DrawCompleted != null)
            {
                DrawCompleted(this, args);
            }
        }

        internal ElementsLayer DrawLayer { get; private set; }
        public FillRule FillRule { get; set; }

        #region IDrawStyle 成员
        public Brush Fill { get; set; }
        public Brush Stroke { get; set; }
        public double StrokeThickness { get; set; }

        public double StrokeMiterLimit { get; set; }
        public double StrokeDashOffset { get; set; }
        public DoubleCollection StrokeDashArray { get; set; }

        public PenLineCap StrokeDashCap { get; set; }
        public PenLineCap StrokeEndLineCap { get; set; }
        public PenLineCap StrokeStartLineCap { get; set; }

        public PenLineJoin StrokeLineJoin { get; set; }

        public double Opacity { get; set; }
        #endregion
    }

}
