﻿using System.Windows.Controls;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class DynamicRESTLayerTest : Page
    {
        private DynamicRESTLayer restLayer;

        public DynamicRESTLayerTest()
        {
            InitializeComponent();
            restLayer = this.MyMap.Layers["restLayer"] as DynamicRESTLayer;
        }

        private void clipRegion_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DrawPolygon region = new DrawPolygon(this.MyMap);
            this.MyMap.Action = region;
            region.DrawCompleted += new System.EventHandler<DrawEventArgs>(region_DrawCompleted);
        }

        void region_DrawCompleted(object sender, DrawEventArgs e)
        {
            Feature feature = new Feature();
            feature.Geometry = e.Geometry;
            restLayer.ClipRegion = e.Geometry as GeoRegion;
            restLayer.Refresh();
            this.MyMap.Action = new Pan(this.MyMap);
        }
    }
}
