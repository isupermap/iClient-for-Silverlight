﻿using System.Windows.Controls;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class IServerAndWMS : Page
    {
        private TiledWMSLayer myTiledWMSLayer;

        public IServerAndWMS()
        {
            InitializeComponent();
        }

        private void Slider_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            if (MyMap != null)
            {
                myTiledWMSLayer.Opacity = layerOpacity.Value;
            }
        }

        private void MyMap_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            myTiledWMSLayer = MyMap.Layers["MyTiledWMSLayer"] as TiledWMSLayer;
            myTiledWMSLayer.Opacity = layerOpacity.Value;
        }
    }
}
