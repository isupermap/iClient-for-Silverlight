﻿using System.Windows.Controls;

namespace iServerJava6R_SampleCode
{
    public partial class Home : Page
    {
        public Home()
        {
            InitializeComponent();
            this.myStory.Begin();
        }
    }
}