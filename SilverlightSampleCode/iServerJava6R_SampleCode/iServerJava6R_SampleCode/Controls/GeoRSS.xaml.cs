﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml.Linq;
using SuperMap.Web.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iServerJava6R_SampleCode
{
    public partial class GeoRSS : Page
    {
        private ElementsLayer elementsLayer;
        private Pushpin slectedPP;
        private const string url = "http://earthquake.usgs.gov/eqcenter/catalogs/eqs7day-M2.5.xml";
        private InfoWindow window;

        public GeoRSS()
        {
            InitializeComponent();

            elementsLayer = MyMap.Layers["MyElementsLayer"] as ElementsLayer;
            window = new InfoWindow(this.MyMap);
            this.MyMap.ViewBoundsChanged += new System.EventHandler<ViewBoundsEventArgs>(MyMap_ViewBoundsChanged);
            LoadRSS(url);
            DispatcherTimer updateTimer = new DispatcherTimer();
            updateTimer.Interval = new TimeSpan(0, 0, 0, 60000);
            updateTimer.Tick += (s, args) =>
            {
                LoadRSS(url); 
            };
            updateTimer.Start();

            progress.Storyboard1.Begin();
        }

        private void MyMap_ViewBoundsChanged(object sender, ViewBoundsEventArgs e)
        {
            if (e.OldViewBounds.IsEmpty)
            {
                this.MyMap.ScreenContainer.Children.Add(window);
            }
        }

        private void LoadRSS(string uri)
        {
            WebClient wc = new WebClient();
            wc.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);
            Uri feedUri = new Uri(uri, UriKind.Absolute);
            wc.OpenReadAsync(feedUri);
        }
        private void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
          
            if (e.Error != null)
            {
                return;
            }
            elementsLayer.Children.Clear();

            XDocument doc = XDocument.Load(e.Result);
            XNamespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";
            var temp = from tt in doc.Descendants("item")
                       select new RssClass
                       {

                           Lon = Convert.ToDouble(tt.Element(geo + "long") != null ? tt.Element(geo + "long").Value : "0"),
                           Lat = Convert.ToDouble(tt.Element(geo + "lat") != null ? tt.Element(geo + "lat").Value : "0"), 
                           Title = tt.Element("title").Value,
                           Desc = tt.Element("description").Value
                       };

            foreach (var item in temp)
            {
                Point2D lonlat = new Point2D(item.Lon, item.Lat);
                Pushpin pushpin = new Pushpin() { Location = lonlat, Tag = item };
                pushpin.MouseLeftButtonDown += new MouseButtonEventHandler(pp_MouseLeftButtonDown);
                elementsLayer.AddChild(pushpin);
            }
            progress.Storyboard1.Stop();
            progress.Visibility = Visibility.Collapsed;
        }

        private void pp_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            slectedPP = sender as Pushpin;
            RssClass info = (RssClass)slectedPP.Tag;

            window.Content = info.Desc;
            window.Title = info.Title;
            window.Location = new Point2D(info.Lon, info.Lat);
            window.ShowInfoWindow();
        }

        private class RssClass
        {
            public double Lon { get; set; }
            public double Lat { get; set; }
            public string Title { get; set; }
            public string Desc { get; set; }
        }
    }
}
