﻿using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace iServerJava6R_SampleCode
{
    public partial class LegendTest : UserControl
    {
        public LegendTest()
        {
            InitializeComponent();
            legend1.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(legend1_PropertyChanged);
        }

        private void legend1_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "LayerItems" && legend1.LayerItems.Count > 0)
            {
                legend1.LayerItems[0].ImageSource = new BitmapImage
                {
                    UriSource = new Uri("/iServerJava6R_SampleCode;component/images/world.png", UriKind.RelativeOrAbsolute)
                };
            }
        }
    }
}
