﻿本例随机生成了100个要素点,当小于一定缩放级别的时候,100个点不会全部分布在地图中,而是根据设定的聚类半径以聚合的方式呈现在地图上。
当比例尺进一步增大的时候，地图上的点会越来越分散。
用户可以设置 Clusterer 是否进行聚合和聚合的风格。
SuperMap iClient  for Silverlight 提供一个预定义的聚合风格（ScatterClusterer和SparkClusterer）。
勾选“区域聚散”，可以设置聚散方式是否是在设定的区域面积中进行聚散显示。
需要注意的是 ScatterClusterer和 SparkClusterer 中有一个 Gradient 属性来设置渐变色。
