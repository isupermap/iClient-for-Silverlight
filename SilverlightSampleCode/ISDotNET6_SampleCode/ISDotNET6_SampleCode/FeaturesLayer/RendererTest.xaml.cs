﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;

namespace ISDotNET6_SampleCode
{
    public partial class RendererTest : UserControl
    {
        private Random random = new Random();
        private FeaturesLayer layer;

        public RendererTest()
        {
            InitializeComponent();
            MyMap.Loaded += new RoutedEventHandler(MyMapControl_Loaded);
        }

        private void MyMapControl_Loaded(object sender, RoutedEventArgs e)
        {
            layer = MyMap.Layers["MyFeaturesLayer1"] as FeaturesLayer;
            for (int i = 0; i < 100; i++)
            {
                double x = random.Next(2000, 7600);
                double y = random.Next(-7600, -2000);

                Feature f = new Feature
                {
                    Geometry = new GeoPoint(x, y),
                };
                f.Attributes.Add("Ranking", random.NextDouble());
                f.ToolTip = new TextBlock
                {
                    Text = "Ranking:" + f.Attributes["Ranking"].ToString(),
                    Foreground = new SolidColorBrush(Colors.Red)
                };
                layer.Features.Add(f);
            }

            List<QueryLayer> queryLayerParams = new List<QueryLayer>() 
            { 
                new QueryLayer
                { 
                    LayerName = "WaterPoly@changchun"  
                } 
            };

            QueryBySqlParameters parameters = new QueryBySqlParameters
            {
                MapName = "changchun",
                QueryParam = new QueryParam { QueryLayers = queryLayerParams },
            };
            QueryBySqlService query = new QueryBySqlService("http://localhost/IS/AjaxDemo");
            query.ProcessAsync(parameters);
            query.Failed += (s, args) => { MessageBox.Show("查询失败!"); };
            query.ProcessCompleted += new EventHandler<QueryServiceEventArgs>(query_ProcessCompleted);
        }

        private void query_ProcessCompleted(object sender, QueryServiceEventArgs e)
        {
            if (e.ResultSet == null)
            {
                MessageBox.Show("没结果!");
                return;
            }
            FeaturesLayer featuresLayer = MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
            foreach (RecordSet item in e.ResultSet.RecordSets)
            {
                foreach (Feature feature in item.ToFeatureSet())
                {
                    featuresLayer.Features.Add(feature);
                }
            }
        }
    }
}
