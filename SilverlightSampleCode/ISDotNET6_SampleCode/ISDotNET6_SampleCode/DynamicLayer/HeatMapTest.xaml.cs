﻿using System;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Mapping;

namespace ISDotNET6_SampleCode
{
    public partial class HeatMapTest : UserControl
    {
        private HeatMapLayer layer;

        public HeatMapTest()
        {
            InitializeComponent();

            layer = MyMap.Layers["heatMap"] as HeatMapLayer;
            this.Loaded += new RoutedEventHandler(HeatMapTest_Loaded);
        }

        //加载热点图
        private void HeatMapTest_Loaded(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            for (int i = 0; i < 100; i++)
            {
                double x = rand.NextDouble() * 360 - 180;
                double y = rand.NextDouble() * 180 - 90;
                HeatPoint heatPoint = new HeatPoint();
                heatPoint.X = x;
                heatPoint.Y = y;
                heatPoint.Value = 10;
                layer.HeatPoints.Add(heatPoint);
            }
            layer.Radius = (int)sliderRadius.Value;
            layer.Intensity = sliderIntensity.Value;
        }

        //半径选择滑动条触发事件
        private void sliderRadius_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (MyMap == null) return;

            layer.Radius = (int)e.NewValue;
        }

        //强度选择滑动条触发事件
        private void sliderIntensity_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (MyMap == null) return;
            layer.Intensity = (double)e.NewValue;
        }
    }
}
