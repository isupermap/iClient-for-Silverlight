﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Mapping;

namespace ISDotNET6_SampleCode
{
    public partial class DynamicWMS : UserControl
    {
        DynamicWMSLayer wmsLayer;

        public DynamicWMS()
        {
            InitializeComponent();
            wmsLayer = this.MyMap.Layers["MyWMS"] as DynamicWMSLayer;
        }

        private void MyCheckBox1_Click(object sender, RoutedEventArgs e)
        {
            List<string> layers = new List<string>();
            foreach (CheckBox item in this.MyStackPanel.Children)
            {
                if ((bool)item.IsChecked)
                {
                    layers.Add(item.Tag.ToString());
                }
            }
            wmsLayer.Layers = layers.ToArray();
        }
    }
}
