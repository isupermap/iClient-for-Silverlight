﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace ISDotNET6_SampleCode
{
    public partial class UIActions : UserControl
    {
        private FeaturesLayer featuresLayer;

        public UIActions()
        {
            InitializeComponent();
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        //绘制点，并且设置绘制时的样式
        private void btn_Point_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint point = new DrawPoint(this.MyMap);
            point.Color = new SolidColorBrush(Colors.Red);
            point.Size = 12;
            this.MyMap.Action = point;
            point.DrawCompleted += new EventHandler<DrawEventArgs>(point_DrawCompleted);
        }

        //绘制点结束后将所绘制的点转化为矢量要素并加载到矢量要素图层中，在加载前先设置点要素样式
        private void point_DrawCompleted(object sender, DrawEventArgs e)
        {
            //设置预定义样式
            PredefinedMarkerStyle style = new PredefinedMarkerStyle();
            style.Color = new SolidColorBrush(Colors.Red);
            style.Size = 30;
            style.Symbol = PredefinedMarkerStyle.MarkerSymbol.Star;

            //将绘制的点转化为具有预定义样式的点要素并加载到矢量要素图层中
            Feature feature = new Feature() { Geometry = e.Geometry as GeoPoint, Style = style };
            this.featuresLayer.Features.Add(feature);
        }

        //绘制线
        private void btn_Line_Click(object sender, RoutedEventArgs e)
        {
            DrawLine line = new DrawLine(this.MyMap);
            this.MyMap.Action = line;
            line.DrawCompleted += new EventHandler<DrawEventArgs>(line_DrawCompleted);

        }

        //绘制线结束后将所绘制的线转化为矢量要素并加载到矢量要素图层中，在加载前先设置线要素样式
        private void line_DrawCompleted(object sender, DrawEventArgs e)
        {
            Feature feature = new Feature();

            //设置预定义样式
            PredefinedLineStyle style = new PredefinedLineStyle();
            style.Stroke = new SolidColorBrush(Colors.Blue);
            style.StrokeThickness = 3;

            //将绘制的线转化为具有预定义样式的线要素
            feature.Style = style;
            feature.Geometry = e.Geometry as GeoLine;

            //将线要素加载到矢量要素图层中
            this.featuresLayer.Features.Add(feature);
        }

        //绘制多边形，并且设置绘制时的样式
        private void btn_Region_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon region = new DrawPolygon(this.MyMap);
            region.Fill = new SolidColorBrush(Colors.Cyan);
            region.FillRule = FillRule.Nonzero;
            region.Opacity = 0.75;
            region.Stroke = new SolidColorBrush(Colors.Brown);
            region.StrokeThickness = 1;
            this.MyMap.Action = region;
            region.DrawCompleted += new EventHandler<DrawEventArgs>(region_DrawCompleted);
        }

        //绘制多边形结束后将所绘制的多边形转化为矢量要素并加载到矢量要素图层中，在加载前先设置该要素样式
        private void region_DrawCompleted(object sender, DrawEventArgs e)
        {
            Feature feature = new Feature();

            //设置预定义样式
            PredefinedFillStyle style = new PredefinedFillStyle();
            style.Stroke = new SolidColorBrush(Colors.Orange);
            style.StrokeThickness = 1;
            style.Fill = new SolidColorBrush(Colors.Green);

            //将绘制的多边形转化为具有预定义样式的面要素
            feature.Style = style;
            feature.Geometry = e.Geometry as GeoRegion;

            //将面要素加载到矢量要素图层中
            this.featuresLayer.Features.Add(feature);
        }

        //绘制自由线，并且设置绘制时的样式
        private void btn_FreeHand_Click(object sender, RoutedEventArgs e)
        {
            DrawFreeLine freehand = new DrawFreeLine(this.MyMap);
            freehand.Opacity = 0.5;
            freehand.Stroke = new SolidColorBrush(Colors.Orange);
            freehand.StrokeThickness = 3;
            this.MyMap.Action = freehand;
            freehand.DrawCompleted += new EventHandler<DrawEventArgs>(freehand_DrawCompleted);
        }

        //绘制自由线结束后将所绘制的自由线转化为矢量要素并加载到矢量要素图层中，在加载前先设置该要素样式
        private void freehand_DrawCompleted(object sender, DrawEventArgs e)
        {
            Feature feature = new Feature();

            //设置预定义样式
            PredefinedLineStyle style = new PredefinedLineStyle();
            style.StrokeThickness = 3;
            style.Symbol = PredefinedLineStyle.LineSymbol.DashDotDot;
            style.Stroke = new SolidColorBrush(Colors.Purple);

            //将绘制的自由线转化为具有预定义样式的线要素
            feature.Style = style;
            feature.Geometry = e.Geometry as GeoLine;

            //将线要素加载到矢量要素图层中
            this.featuresLayer.Features.Add(feature);
        }

        //绘制矩形，并且设置绘制时的样式
        private void btn_Rectangle_Click(object sender, RoutedEventArgs e)
        {
            DrawRectangle rectangle = new DrawRectangle(this.MyMap) { Fill = new SolidColorBrush(Colors.Blue) };
            this.MyMap.Action = rectangle;
            rectangle.DrawCompleted += new EventHandler<DrawEventArgs>(rectangle_DrawCompleted);
        }

        //绘制矩形结束后将所绘制的矩形转化为矢量要素并加载到矢量要素图层中，在加载前先设置该要素样式
        private void rectangle_DrawCompleted(object sender, DrawEventArgs e)
        {
            Feature feature = new Feature();

            //设置预定义样式
            PredefinedFillStyle style = new PredefinedFillStyle();
            style.Stroke = new SolidColorBrush(Colors.Green);
            style.StrokeThickness = 1;
            style.Fill = new SolidColorBrush(Colors.Gray);

            //将绘制的矩形转化为具有预定义样式的面要素
            feature.Style = style;
            feature.Geometry = e.Geometry as GeoRegion;

            //将面要素加载到矢量要素图层中
            this.featuresLayer.Features.Add(feature);
        }

        //绘制自由面
        private void btn_FreeRegion_Click(object sender, RoutedEventArgs e)
        {
            DrawFreePolygon polygon = new DrawFreePolygon(this.MyMap);
            this.MyMap.Action = polygon;
            polygon.DrawCompleted += new EventHandler<DrawEventArgs>(polygon_DrawCompleted);
        }

        //绘制自由面结束后将所绘制的自由面转化为矢量要素并加载到矢量要素图层中
        private void polygon_DrawCompleted(object sender, DrawEventArgs e)
        {
            Feature feature = new Feature() { Geometry = e.Geometry as GeoRegion };
            featuresLayer.Features.Add(feature);
        }

        //平移
        private void btn_Pan_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap.Action = new Pan(this.MyMap, Cursors.Hand);
        }

        //清除矢量要素图层中全部元素
        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            if (this.featuresLayer != null)
            {
                this.featuresLayer.Features.Clear();
            }
        }

        //拉框缩小
        private void btn_ZoomOut_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap.Action = new ZoomOut(this.MyMap);
        }

        //拉框放大，并设置拉框的样式
        private void btn_ZoomIn_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap.Action = new ZoomIn(this.MyMap) { Fill = new SolidColorBrush(Colors.Red) };
        }

        //编辑
        private void EditionAction_Click(object sender, RoutedEventArgs e)
        {
            SuperMap.Web.Actions.Edit editEntity = new Edit(this.MyMap, featuresLayer);
            this.MyMap.Action = editEntity;
        }
    }
}
