﻿using System.Windows.Controls;
using SuperMap.Web.Mapping;

namespace ISDotNET6_SampleCode
{
    public partial class ISAndWMS : UserControl
    {
        private DynamicWMSLayer myWMSLayer;

        public ISAndWMS()
        {
            InitializeComponent();
        }

        private void Slider_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            if (MyMap != null)
            {
                myWMSLayer.Opacity = layerOpacity.Value;
            }
        }

        private void MyMap_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            myWMSLayer = MyMap.Layers["WMSLayer"] as DynamicWMSLayer;
            myWMSLayer.Opacity = layerOpacity.Value;
        }
    }
}
