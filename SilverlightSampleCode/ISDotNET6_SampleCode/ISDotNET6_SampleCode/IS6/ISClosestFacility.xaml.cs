﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISClosestFacility : UserControl
    {
        private ElementsLayer arbitraryLayerE;
        private ElementsLayer arbitraryLayerF;
        private FeaturesLayer featuresLayer;
        private Point2D eventp;

        public ISClosestFacility()
        {
            InitializeComponent();
            eventp = new Point2D();

            arbitraryLayerE = this.MyMap.Layers["MyArbitraryLayerE"] as ElementsLayer;
            arbitraryLayerF = this.MyMap.Layers["MyArbitraryLayerF"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        //为最近设施分析提供点
        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        //将点添加到任意图层中
        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            Pushpin pushpin = new Pushpin();

            pushpin.Location = e.Geometry.Bounds.Center;
            pushpin.Background = new SolidColorBrush(Colors.Red);

            arbitraryLayerE.AddChild(pushpin);
            eventp = pushpin.Location;
        }

        private void FindClosestFacility_Click(object sender, RoutedEventArgs e)
        {
            //为最近设施分析设置参数
            NetworkSetting networkSetting = new NetworkSetting
            {
                NodeIDField = "SmNodeID",
                NetworkLayerName = "RoadNet@changchun",
                EdgeIDField = "SmEdgeID",
                FTWeightField = "SmLength",
                TFWeightField = "SmLength"
            };

            NetworkParams networkParams = new NetworkParams
            {
                Tolerance = 1000,
                NetworkSetting = networkSetting
            };
            ProximityParam proximityParam = new ProximityParam
            {
                FacilityCount = 2,
                MaxRadius = 100000,
                FacilityLayer = "School@changchun",
                IsFromEvent = true,
                NetworkParams = networkParams,
            };

            ClosestFacilityParameters closestFacilityParamters = new ClosestFacilityParameters
            {
                MapName = "changchun",
                Point = eventp,
                ProximityParam = proximityParam
            };

            //最近设施分析
            ClosestFacilityService service = new ClosestFacilityService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(closestFacilityParamters);
            service.ProcessCompleted += new EventHandler<ProximityEventArgs>(service_ProcessCompleted);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
        }

        //最近设施分析失败时的操作
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //最近设施分析完成时的操作
        private void service_ProcessCompleted(object sender, ProximityEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("查询无结果");
                return;
            }

            if (e.Result.ReturnRecordSet == null)
            {
                MessageBox.Show("查询无结果");
                return;
            }

            if (e.Result.ReturnRecordSet.Records == null)
            {
                MessageBox.Show("查询无结果");
                return;
            }
            for (int i = 0; i < e.Result.ReturnRecordSet.Records.Count; i++)
            {
                Feature feature = new Feature
                {
                    Geometry = new GeoPoint(e.Result.ReturnRecordSet.Records[i].Center.X, e.Result.ReturnRecordSet.Records[i].Center.Y),
                    Style = new PredefinedMarkerStyle { Color = new SolidColorBrush(Colors.Red) },
                    ToolTip = new TextBlock { Text = e.Result.ReturnRecordSet.Records[i].FieldValues[2] }
                };

                featuresLayer.AddFeature(feature);
            }
        }

        //鼠标平移操作
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //清除图层
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            arbitraryLayerF.Children.Clear();
            arbitraryLayerE.Children.Clear();
            featuresLayer.ClearFeatures();
            eventp = Point2D.Empty;
        }
    }
}
