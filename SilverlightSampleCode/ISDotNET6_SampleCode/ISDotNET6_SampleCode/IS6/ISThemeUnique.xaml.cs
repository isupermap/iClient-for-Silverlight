﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISThemeUnique : UserControl
    {
        private List<String> layerNames = new List<String>();
        private DynamicISLayer themeLayer;
        public ISThemeUnique()
        {
            InitializeComponent();
            themeLayer = MyMap.Layers["ISLayer"] as DynamicISLayer;
            //从服务器获取地图中所有图层信息
            GetMapStatusParameters mapStatus = new GetMapStatusParameters() { MapName = "World" };
            GetMapStatusService mapStatusService = new GetMapStatusService("http://localhost/IS/AjaxDemo");
            mapStatusService.ProcessAsync(mapStatus);
            mapStatusService.ProcessCompleted += new EventHandler<GetMapStatusEventArgs>(mapStatusService_ProcessCompleted);
            mapStatusService.Failed += new EventHandler<ServiceFailedEventArgs>(mapStatusService_Failed);
        }

        //获取服务器地图图层失败提示错误信息
        private void mapStatusService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //获取服务器地图中图层名
        private void mapStatusService_ProcessCompleted(object sender, GetMapStatusEventArgs e)
        {
            foreach (ServerLayer layer in e.Result.ServerLayers)
            {
                layerNames.Add(layer.Name);
            }
        }

        //单值专题图
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            List<ServerStyle> themeStyle = new List<ServerStyle>();
            ServerStyle themeStyle1 = new ServerStyle() { BrushColor = new ServerColor(157, 127, 255), PenColor = new ServerColor(197, 17, 21) };
            ServerStyle themeStyle2 = new ServerStyle() { BrushColor = new ServerColor(250, 237, 195), PenColor = new ServerColor(33, 180, 40) };
            ServerStyle themeStyle3 = new ServerStyle() { BrushColor = new ServerColor(59, 188, 230), PenColor = new ServerColor(116, 122, 235) };
            ServerStyle themeStyle4 = new ServerStyle() { BrushColor = new ServerColor(1, 128, 171), PenColor = new ServerColor(16, 122, 235) };
            ServerStyle themeStyle5 = new ServerStyle() { BrushColor = new ServerColor(167, 219, 232), PenColor = new ServerColor(116, 122, 25) };
            ServerStyle themeStyle6 = new ServerStyle() { BrushColor = new ServerColor(192, 214, 54), PenColor = new ServerColor(116, 12, 235) };

            themeStyle.Add(themeStyle1);
            themeStyle.Add(themeStyle2);
            themeStyle.Add(themeStyle3);
            themeStyle.Add(themeStyle4);
            themeStyle.Add(themeStyle5);
            themeStyle.Add(themeStyle6);

            SuperMap.Web.ISDotNET6.ThemeUnique unique = new SuperMap.Web.ISDotNET6.ThemeUnique()
            {
                Caption = "制作国家的单值专题图",
                Expression = "Country",
                Displays = themeStyle,
                Values = new List<string>() { "俄罗斯", "中华人民共和国", "印度", "美国", "巴西", "澳大利亚" },
            };

            ThemeParameters parameters = new ThemeParameters()
            {
                MapName = "World",
                Theme = unique,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(parameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }

        //成功返回专题图
        private void service_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersKey = e.Result.LayerKey;
        }

        //服务器计算失败提示错误信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //移除专题图，即生成一个空的专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            ThemeParameters clearThemeParameters = new ThemeParameters()
            {
                MapName = "World",
                Theme = null,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            //与服务器交互
            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(clearThemeParameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }
    }
}
