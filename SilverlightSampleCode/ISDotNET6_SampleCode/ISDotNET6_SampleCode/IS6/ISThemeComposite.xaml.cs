﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISThemeComposite : UserControl
    {
        private List<String> layerNames = new List<String>();
        private TiledDynamicISLayer themeLayer;
        public ISThemeComposite()
        {
            InitializeComponent();
            themeLayer = MyMap.Layers["ISLayer"] as TiledDynamicISLayer;
            //从服务器获取地图中所有图层信息
            GetMapStatusParameters mapStatus = new GetMapStatusParameters() { MapName = "World" };
            GetMapStatusService mapStatusService = new GetMapStatusService("http://localhost/IS/AjaxDemo");
            mapStatusService.ProcessAsync(mapStatus);
            mapStatusService.ProcessCompleted += new EventHandler<GetMapStatusEventArgs>(mapStatusService_ProcessCompleted);
            mapStatusService.Failed += new EventHandler<ServiceFailedEventArgs>(mapStatusService_Failed);
        }

        //获取服务器地图图层失败提示错误信息
        private void mapStatusService_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //获取服务器地图中图层名
        private void mapStatusService_ProcessCompleted(object sender, GetMapStatusEventArgs e)
        {
            foreach (ServerLayer layer in e.Result.ServerLayers)
            {
                layerNames.Add(layer.Name);
            }
        }

        //制作满足条件国家的人口密度专题图，并显示国家标签
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            //标签专题图
            ThemeLabel label = new ThemeLabel()
            {
                Caption = "标签专题图",
                Expression = "Country",
                Display = new ServerTextStyle()
                {
                    FontHeight = 10,
                    FontWidth = 0, 
                    Color = new ServerColor(0, 0, 0),
                    FontName = "微软雅黑" 
                },
                Filter = "SmID > 220",
            };

            ThemeDotDensity dotDensity = new ThemeDotDensity()
            {
                Caption = "点密度专题图",
                Expression = "Pop_1994",
                DotStyle = new ServerStyle()
                { 
                    SymbolSize = 20,
                    SymbolStyle = 1,
                },
                DotValue = 11281396.89000,
                Filter = "SmID > 220",
            };

            CompositeParam composite = new CompositeParam()
            {
                ThemeLayer = "World@world",
                Themes = new List<Theme> { label, dotDensity },
            };

            CompositeThemeParameters parameters = new CompositeThemeParameters()
            {
                LayerNames = layerNames,
                MapName = "World",
                ParamList = new List<CompositeParam>(){composite},
            };

            //与服务器交互
            CompositeThemeService service = new CompositeThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(parameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }

        //成功返回专题图
        private void service_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersKey = e.Result.LayerKey;
        }

        //服务器计算失败提示错误信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //移除专题图，即生成一个空的专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            CompositeThemeParameters clearThemeParameters = new CompositeThemeParameters()
            {
                LayerNames = layerNames,
                MapName = "World",
                ParamList = null,
            };

            //与服务器交互
            CompositeThemeService service = new CompositeThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(clearThemeParameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }
    }
}
