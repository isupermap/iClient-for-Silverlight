﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISThemeRange : UserControl
    {
        private List<String> layerNames = new List<String>();
        private TiledDynamicISLayer themeLayer;
        public ISThemeRange()
        {
            InitializeComponent();
            themeLayer = MyMap.Layers["ISLayer"] as TiledDynamicISLayer;
            //从服务器获取地图中所有图层信息
            GetMapStatusParameters mapStatus = new GetMapStatusParameters() { MapName = "World" };
            GetMapStatusService mapStatusService = new GetMapStatusService("http://localhost/IS/AjaxDemo");
            mapStatusService.ProcessAsync(mapStatus);
            mapStatusService.ProcessCompleted += new EventHandler<GetMapStatusEventArgs>(mapStatusService_ProcessCompleted);
            mapStatusService.Failed += new EventHandler<ServiceFailedEventArgs>(mapStatusService_Failed);

        }

        //获取服务器地图图层失败提示错误信息
        private void mapStatusService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //获取服务器地图中图层名
        private void mapStatusService_ProcessCompleted(object sender, GetMapStatusEventArgs e)
        {
            foreach (ServerLayer layer in e.Result.ServerLayers)
            {
                layerNames.Add(layer.Name);
            }
        }

        //范围分段专题图
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            List<ServerStyle> themeStyles = new List<ServerStyle>();
            ServerStyle themeStyle1 = new ServerStyle() { BrushColor = new ServerColor(216, 244, 254) };
            ServerStyle themeStyle2 = new ServerStyle() { BrushColor = new ServerColor(131, 232, 252) };
            ServerStyle themeStyle3 = new ServerStyle() { BrushColor = new ServerColor(112, 212, 243) };
            ServerStyle themeStyle4 = new ServerStyle() { BrushColor = new ServerColor(23, 198, 238) };
            ServerStyle themeStyle5 = new ServerStyle() { BrushColor = new ServerColor(0, 187, 236) };
            ServerStyle themeStyle6 = new ServerStyle() { BrushColor = new ServerColor(0, 133, 236) };

            themeStyles.Add(themeStyle1);
            themeStyles.Add(themeStyle2);
            themeStyles.Add(themeStyle3);
            themeStyles.Add(themeStyle4);
            themeStyles.Add(themeStyle5);
            themeStyles.Add(themeStyle6);

            ThemeRange range = new ThemeRange()
            {
                BreakValues = new List<double>() { 6000000, 10000000, 50000000, 100000000, 500000000 },
                Caption = "范围分段专题图",
                Displays = themeStyles,
                Expression = "Pop_1994",
            };

            ThemeParameters parameters = new ThemeParameters
            {
                MapName = "World",
                Theme = range,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            //与服务器交互
            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(parameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }

        //成功返回专题图
        private void service_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersKey = e.Result.LayerKey;
        }

        //服务器计算失败提示错误信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //移除专题图，即生成一个空的专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            ThemeParameters clearParameters = new ThemeParameters
            {
                MapName = "World",
                Theme = null,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            //与服务器交互
            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(clearParameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }
    }
}
