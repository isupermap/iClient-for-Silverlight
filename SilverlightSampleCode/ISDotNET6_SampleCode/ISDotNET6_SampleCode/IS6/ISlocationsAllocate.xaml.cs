﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISlocationsAllocate : UserControl
    {
        private ElementsLayer elementsLayer;
        private FeaturesLayer featuresLayer;
        private List<ServerCenter> centers = new List<ServerCenter>();

        private List<Point2D> points = new List<Point2D>();

        public ISlocationsAllocate()
        {
            InitializeComponent();
            elementsLayer = this.MyMap.Layers["MyArbitraryLayer"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        private void LocationsAllocateService_Click(object sender, RoutedEventArgs e)
        {
            //给选址分析服务设置参数
            LocationsAllocateParameters parameters = new LocationsAllocateParameters()
            {
                MapName = "changchun",
                LocationsAllocateParam = new LocationsAllocateParam()
                {
                    FromCenter = true,
                    LocationAllocateMode = LocationAllocateType.MinDistance,
                    LocationsCount = 0,
                    NetworkParams = new NetworkParams()
                    {
                        NetworkSetting = new NetworkSetting()
                        {
                            NetworkLayerName = "RoadNet@changchun",
                            NodeIDField = "SmNodeID",
                            EdgeIDField = "SmEdgeID",
                            FTWeightField = "SmLength",
                            TFWeightField = "SmLength",
                        },
                        LocationAllocateSetting = new LocationAllocateSetting()
                        {
                            Centers = centers,
                            NodeDemandField = "SmNodeID",
                        },
                        Tolerance = 124.615617,
                    },
                    ResultCenterNodesTableOption = new DataReturnOption()
                    {
                        MaxRecordCount = 10000,
                        OverwriteIfExists = true,
                        ReturnMode = DataReturnMode.AsRecordset
                    },
                    ResultCenterOption = new DataReturnOption()
                    {
                        MaxRecordCount = 10000,
                        OverwriteIfExists = true,
                        ReturnMode = DataReturnMode.AsRecordset
                    }
                }
            };

            //执行选址服务
            LocationsAllocateService locationsAllocateService = new LocationsAllocateService("http://localhost/IS/AjaxDemo");
            locationsAllocateService.ProcessAsync(parameters);
            locationsAllocateService.ProcessCompleted += new EventHandler<LocationsAllocateEventArgs>(service_ProcessCompleted);
            locationsAllocateService.Failed += new EventHandler<ServiceFailedEventArgs>(locationsAllocateService_Failed);
        }

        private void locationsAllocateService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message.ToString());
        }

        private void service_ProcessCompleted(object sender, LocationsAllocateEventArgs e)
        {
            //对结果进行判断，增强程序健壮性
            if (e.Result == null)
            {
                MessageBox.Show("查询无结果");
                return;
            }
            if (e.Result.ResultCenterData == null)
            {
                MessageBox.Show("查询无结果");
                return;
            }

            if (e.Result.ResultCenterData.RecordSet == null)
            {
                MessageBox.Show("查询无结果");
                return;
            }

            //显示结果面板
            DataStackPanel.Visibility = Visibility.Visible;
            List<ResultData> listResultData = new List<ResultData>();
            FeatureCollection features = e.Result.ResultCenterData.RecordSet.ToFeatureSet();
            int i = 0;

            //遍历结果，listResultData变量
            foreach (Feature item in features)
            {
                Scroll.Visibility = Visibility.Visible;
                ResultGrid.Visibility = Visibility.Visible;

                listResultData.Add(new ResultData
                {
                    SmID = item.Attributes["SmID"].ToString(),
                    AltCost = item.Attributes["AltCost"].ToString(),
                    AltDemand = item.Attributes["AltDemand"].ToString(),
                    AltResource = item.Attributes["AltResource"].ToString(),
                    AveCost = item.Attributes["AveCost"].ToString(),
                });
                i++;
            }
            //给datagrid控件赋值。
            ResultGrid.ItemsSource = listResultData;
        }

        //服务失败时执行的方法
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //鼠标平移操作
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //清除结果数据
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();

            DataStackPanel.Visibility = Visibility.Collapsed;
            Scroll.Visibility = Visibility.Collapsed;
            ResultGrid.Visibility = Visibility.Collapsed;
            ResultGrid.ItemsSource = null;
            centers.Clear();
        }

        //自定义ResultData类，用于存储返回的结果
        public class ResultData
        {
            public string SmID { get; set; }
            public string AltCost { get; set; }
            public string AltDemand { get; set; }
            public string AltResource { get; set; }
            public string AveCost { get; set; }
        }

        private void select_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint drawPoint = new DrawPoint(MyMap);
            MyMap.Action = drawPoint;
            drawPoint.DrawCompleted += new EventHandler<DrawEventArgs>(point_DrawCompleted);
        }

        private void point_DrawCompleted(object sender, DrawEventArgs e)
        {
            Pushpin pushPin = new Pushpin();
            pushPin.Location = e.Geometry.Bounds.Center;
            elementsLayer.AddChild(pushPin);
            QueryByCenterParameters queryParameters = new QueryByCenterParameters()
            {
                MapName = "changchun",
                Tolerance = 100,
                IsNearest = true,
                QueryParam = new QueryParam()
                {
                    QueryAllLayer = false,
                    QueryLayers = new List<QueryLayer>() { new QueryLayer() { LayerName = "RoadNet@changchun" } },
                    HasGeometry = true,
                    ReturnShape = true,
                    ExpectCount = 1,
                }
            };
            queryParameters.CenterPoint = e.Geometry.Bounds.Center;
            QueryByCenterService queryService = new QueryByCenterService("http://localhost/IS/AjaxDemo");
            queryService.ProcessAsync(queryParameters);
            queryService.ProcessCompleted += new EventHandler<QueryServiceEventArgs>(queryService_ProcessCompleted);
        }

        private void queryService_ProcessCompleted(object sender, QueryServiceEventArgs e)
        {
            if (e.ResultSet != null && e.ResultSet.RecordSets != null && e.ResultSet.CurrentCount != 0 && e.ResultSet.RecordSets != null)
            {
                centers.Add(new ServerCenter
                {
                    ID = Convert.ToInt32(e.ResultSet.RecordSets[0].Records[0].FieldValues[2]),
                    CandidateType = CenterCandidateType.Mobile,
                    MaxImpedance = 1000,
                    Resource = 8000
                });
            }
        }
    }
}