﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISQueryBySQL : UserControl
    {
        private const string PointLayerName = "School@changchun";
        private const string LineLayerName = "Railway@changchun";
        private const string AreaLayerName = "ResidentialArea@changchun";
        private FeaturesLayer featuresLayer;
        private ComboBox comboBox;

        public ISQueryBySQL()
        {
            InitializeComponent();
            featuresLayer = MyMap.Layers[1] as FeaturesLayer;

            //设置显示查询图层的下拉框
            #region ComboBox
            comboBox = new ComboBox();
            comboBox.Width = 160;
            comboBox.Height = 25;
            comboBox.VerticalAlignment = VerticalAlignment.Top;
            comboBox.HorizontalAlignment = HorizontalAlignment.Right;

            ComboBoxItem itemPoint = new ComboBoxItem();
            itemPoint.Content = PointLayerName;
            ComboBoxItem itemLine = new ComboBoxItem();
            itemLine.Content = LineLayerName;
            ComboBoxItem itemRegion = new ComboBoxItem();
            itemRegion.Content = AreaLayerName;
            comboBox.Items.Add(itemPoint);
            comboBox.Items.Add(itemLine);
            comboBox.Items.Add(itemRegion);
            MyStackPanel.Children.Add(comboBox);
            comboBox.SelectedIndex = 2;
            comboBox.SelectionChanged += new SelectionChangedEventHandler(comboBox_SelectionChanged);
            #endregion
        }

        //当查询图层改变时
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //清除要素
            featuresLayer.Features.Clear();

            //隐藏FeatureDataGrid控件
            MyDataGrid.ItemsSource = null;
            MyDataGrid.Visibility = Visibility.Collapsed;
        }

        //SQL查询
        private void Query_Click(object sender, RoutedEventArgs e)
        {
            string str = MyTextBox.Text;
            if (str == "")
            {
                MessageBox.Show("请输入SQL条件！");
            }

            QueryLayer querylayer = new QueryLayer();
            querylayer.LayerName = comboBox.SelectionBoxItem.ToString();
            querylayer.WhereClause = str;
            List<QueryLayer> queryLayers = new List<QueryLayer>() { querylayer };

            QueryParam queryParam = new QueryParam()
            {
                ExpectCount = 0,
                StartRecord = 0,
                QueryLayers = queryLayers
            };

            QueryBySqlParameters parameters = new QueryBySqlParameters
            {
                MapName = "changchun",
                QueryParam = queryParam
            };

            QueryBySqlService queryBySqlService = new QueryBySqlService("http://localhost/IS/AjaxDemo");
            queryBySqlService.ProcessAsync(parameters);
            queryBySqlService.Failed += new EventHandler<ServiceFailedEventArgs>(queryBySqlService_Failed);
            queryBySqlService.ProcessCompleted += new EventHandler<QueryServiceEventArgs>(queryBySqlService_ProcessCompleted);

            featuresLayer.MouseLeftButtonDown +=new EventHandler<FeatureMouseButtonEventArgs>(featuresLayer_MouseLeftButtonDown);
        }

        //在地图中通过鼠标左键选择地物
        private void featuresLayer_MouseLeftButtonDown(object sender, FeatureMouseButtonEventArgs args)
        {
            //如果矢量已选中，再点击变为非选中状态；反之亦然
            args.Feature.Selected = !args.Feature.Selected;

            //FeatureDataGrid 移动到选中地物的记录
            if (args.Feature.Selected)
            {
                MyDataGrid.ScrollIntoView(args.Feature, null);
            }
            args.Handled = true;
        }

        //处理服务器返回结果
        private void queryBySqlService_ProcessCompleted(object sender, QueryServiceEventArgs e)
        {
            if (e.ResultSet == null || e.ResultSet.RecordSets == null || e.ResultSet.RecordSets.Count < 0)
            {
                MessageBox.Show("查询结果为空!");
                return;
            }
            else
            {
                MyDataGrid.Visibility = Visibility.Visible;

                //将指定查询图层的查询结果加载到矢量图层中
                foreach (RecordSet item in e.ResultSet.RecordSets)
                {
                    featuresLayer.AddFeatureSet(item.ToFeatureSet());
                }

                //将查询到的地物赋予用户自定义的样式
                foreach (Feature item in featuresLayer.Features)
                {
                    if (item.Geometry is GeoLine)
                    {
                        item.Style = LineSelectStyle;
                    }
                    else if (item.Geometry is GeoRegion)
                    {
                        item.Style = SelectStyle;
                    }
                    else
                    {
                        item.Style = new PredefinedMarkerStyle() { Color = new SolidColorBrush(Colors.Blue), Size = 20, Symbol = PredefinedMarkerStyle.MarkerSymbol.Diamond };
                    }
                }
            }
        }

        //服务器提示失败信息
        private void queryBySqlService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            //清除要素
            featuresLayer.Features.Clear();

            //隐藏FeatureDataGrid控件
            MyDataGrid.ItemsSource = null;
            MyDataGrid.Visibility = Visibility.Collapsed;
        }
    }
}
