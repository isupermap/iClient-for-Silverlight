﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISQueryByCenter : UserControl
    {
        private const string PointLayerName = "School@changchun";
        private const string LineLayerName = "Railway@changchun";
        private const string AreaLayerName = "ResidentialArea@changchun";
        private FeaturesLayer featuresLayer;
        private ComboBox comboBox;

        public ISQueryByCenter()
        {
            InitializeComponent();

            //通过图层的索引号来获取图层
            featuresLayer = MyMap.Layers[1] as FeaturesLayer;

            //设置显示查询图层的下拉框
            #region ComboBox
            comboBox = new ComboBox();
            comboBox.Width = 160;
            comboBox.VerticalAlignment = VerticalAlignment.Top;
            comboBox.HorizontalAlignment = HorizontalAlignment.Right;

            ComboBoxItem itemPoint = new ComboBoxItem();
            itemPoint.Content = PointLayerName;
            ComboBoxItem itemLine = new ComboBoxItem();
            itemLine.Content = LineLayerName;
            ComboBoxItem itemRegion = new ComboBoxItem();
            itemRegion.Content = AreaLayerName;
            comboBox.Items.Add(itemPoint);
            comboBox.Items.Add(itemLine);
            comboBox.Items.Add(itemRegion);
            MyStackPanel.Children.Add(comboBox);
            comboBox.SelectedIndex = 2;
            comboBox.SelectionChanged += new SelectionChangedEventHandler(comboBox_SelectionChanged);
            #endregion
        }

        //当查询图层改变时
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //清除要素
            featuresLayer.Features.Clear();

            //隐藏FeatureDataGrid控件
            MyDataGrid.ItemsSource = null;
            MyDataGrid.Visibility = Visibility.Collapsed;
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //指定中心点
        private void Point_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint point = new DrawPoint(MyMap);
            MyMap.Action = point;
            point.DrawCompleted += new EventHandler<DrawEventArgs>(point_DrawCompleted);
        }

        //中心点查询
        private void point_DrawCompleted(object sender, DrawEventArgs e)
        {
            QueryParam queryParam = new QueryParam();
            QueryLayer queryLayer = new QueryLayer();
            queryLayer.LayerName = comboBox.SelectionBoxItem.ToString();
            List<QueryLayer> queryLayers = new List<QueryLayer> { queryLayer };
            queryParam = new QueryParam()
            {
                ExpectCount = 0,
                QueryLayers = queryLayers,
                StartRecord = 0
            };

            //将文本中读取的容限值转换为Double类型，如果值为0则将其设为100
            double tolerance;
            if (!double.TryParse(MyTextBox.Text, out tolerance))
            {
                tolerance = 100;
            }

            QueryByCenterParameters parameters = new QueryByCenterParameters
            {
                MapName = "changchun",
                CenterPoint = e.Geometry.Bounds.Center,
                Tolerance = tolerance,
                QueryParam = queryParam
            };

            if (FindNearest.IsChecked == true)
            {
                parameters.IsNearest = true;
            }

            //与服务器交互 
            QueryByCenterService query = new QueryByCenterService("http://localhost/IS/AjaxDemo");
            query.ProcessAsync(parameters);
            query.Failed += new EventHandler<ServiceFailedEventArgs>(query_Failed);
            query.ProcessCompleted += new EventHandler<QueryServiceEventArgs>(query_ProcessCompleted);

            featuresLayer.MouseLeftButtonDown+=new EventHandler<FeatureMouseButtonEventArgs>(featuresLayer_MouseLeftButtonDown);
        }

        //在地图中通过鼠标左键选择地物
        private void featuresLayer_MouseLeftButtonDown(object sender, FeatureMouseButtonEventArgs args)
        {
            //如果矢量已选中，再点击变为非选中状态；反之亦然
            args.Feature.Selected = !args.Feature.Selected;

            //FeatureDataGrid 移动到选中地物的记录
            if (args.Feature.Selected)
            {
                MyDataGrid.ScrollIntoView(args.Feature, null);
            }
            args.Handled = true;
        }

        //服务器返回查询结果
        private void query_ProcessCompleted(object sender, QueryServiceEventArgs e)
        {
            if (e.ResultSet == null || e.ResultSet.RecordSets == null || e.ResultSet.RecordSets.Count < 0)
            {
                MessageBox.Show("查询结果为空!");
                return;
            }
            else
            {
                MyDataGrid.Visibility = Visibility.Visible;

                //将指定查询图层的查询结果加载到矢量图层中
                foreach (RecordSet item in e.ResultSet.RecordSets)
                {
                    featuresLayer.AddFeatureSet(item.ToFeatureSet());
                }

                //将查询到的地物赋予用户自定义的样式
                foreach (Feature item in featuresLayer.Features)
                {
                    if (item.Geometry is GeoLine)
                    {
                        item.Style = LineSelectStyle;
                    }
                    else if (item.Geometry is GeoRegion)
                    {
                        item.Style = SelectStyle;
                    }
                    else
                    {
                        item.Style = new PredefinedMarkerStyle() { Color = new SolidColorBrush(Colors.Blue), Size = 20, Symbol = PredefinedMarkerStyle.MarkerSymbol.Diamond };
                    }
                }

                //将FeatureDataGrid与矢量图层绑定
                this.MyDataGrid.FeaturesLayer = featuresLayer;
            }
        }

        //与服务器交互失败提示错误信息
        private void query_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //清除图上全部矢量要素
        private void clearFeatures_Click(object sender, RoutedEventArgs e)
        {
            //清除要素
            featuresLayer.Features.Clear();

            //隐藏FeatureDataGrid控件
            MyDataGrid.ItemsSource = null;
            MyDataGrid.Visibility = Visibility.Collapsed;
        }
    }
}
