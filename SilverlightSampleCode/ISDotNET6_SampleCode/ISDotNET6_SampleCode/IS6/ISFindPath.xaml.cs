﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISFindPath : UserControl
    {
        private ElementsLayer elementsLayer;
        private Point2DCollection points = new Point2DCollection();
        private FeaturesLayer featuresLayer;
        private int i = 0;

        public ISFindPath()
        {
            InitializeComponent();
            elementsLayer = this.MyMap.Layers["MyArbitraryLayer"] as ElementsLayer;
            featuresLayer = this.MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;
        }

        private void PathAnalyst_Click(object sender, RoutedEventArgs e)
        {
            //为最佳路径分析服务设置参数
            NetworkSetting networkSetting = new NetworkSetting
            {
                EdgeIDField = "SmEdgeID",
                NodeIDField = "SmNodeID",
                NetworkLayerName = "RoadNet@changchun",
                FTWeightField = "SmLength",
                TFWeightField = "SmLength",
                FromNodeIDField = "SmFNode",
                ToNodeIDField = "SmTNode",
            };

            NetworkParams networkParams = new NetworkParams
            {
                NetworkSetting = networkSetting,
                Tolerance = 30,
            };

            RouteParam routeParam = new RouteParam
            {
                NetworkParams = networkParams,
                ReturnNodePositions = true,
                ReturnEdgeIDsAndNodeIDs = true,
                ReturnCosts = true,
                ReturnPathInfo = new ReturnPathInfoParam()
                {
                    EdgeNameField = "SmEdgeID",
                    NodeNameField = "SmNodeID",
                    ReturnPathGuide = true,
                    ReturnPathTable = false,
                },
            };

            SuperMap.Web.ISDotNET6.FindPathByPointsParameters findPathParameters = new FindPathByPointsParameters
            {
                MapName = "changchun",
                Points = points,
                RouteParam = routeParam,
            };

            //最佳路径分析服务
            FindPathByPointsService service = new FindPathByPointsService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(findPathParameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<RouteEventArgs>(service_ProcessCompleted);
        }

        private void service_ProcessCompleted(object sender, RouteEventArgs e)
        {
            //预定义线样式
            PredefinedLineStyle style = new PredefinedLineStyle { Stroke = new SolidColorBrush(Colors.Red), StrokeThickness = 3 };
            //对结果进行判断，增强程序健壮性

            if (e.Result == null)
            {
                MessageBox.Show("查询无结果");
                return;
            }

            if (e.Result.NodePositions == null)
            {
                MessageBox.Show("查询无结果");
                return;
            }

            if (e.Result.NodePositions.Count == 0)
            {
                MessageBox.Show("查询无结果");
                return;
            }

            GeoLine line = new GeoLine();
            line.Parts.Add(e.Result.NodePositions);
            //将feature 添加到图层中
            Feature feature = new Feature { Geometry = line, Style = style };
            featuresLayer.AddFeature(feature);
        }

        //最佳路径分析服务失败时的操作
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //鼠标平移操作
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        //清除图层
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            elementsLayer.Children.Clear();
            featuresLayer.ClearFeatures();
            points.Clear();
            i = 0;
        }

        //为路径分析提供点
        private void SelectPoint_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint node = new DrawPoint(MyMap);
            MyMap.Action = node;
            node.DrawCompleted += new EventHandler<DrawEventArgs>(node_DrawCompleted);
        }

        //将点添加到任意图层中
        private void node_DrawCompleted(object sender, DrawEventArgs e)
        {
            i++;
            Pushpin pushPin = new Pushpin()
            {
                Location = e.Geometry.Bounds.Center,
                Content = i.ToString()
            };
            elementsLayer.Children.Add(pushPin);
            points.Add(pushPin.Location);
        }
    }
}
