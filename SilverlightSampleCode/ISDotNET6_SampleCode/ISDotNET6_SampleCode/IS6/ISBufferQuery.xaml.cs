﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISBufferQuery : UserControl
    {
        private const string SchoolLayerName = "School@changchun";
        private const string RailLayerName = "Railway@changchun";
        private const string ResidentialLayerName = "ResidentialArea@changchun";
        private FeaturesLayer resultLayer;
        private FeaturesLayer drawLayer;
        private List<SuperMap.Web.Core.Geometry> geometries = new List<SuperMap.Web.Core.Geometry>();

        public ISBufferQuery()
        {
            InitializeComponent();
            drawLayer = this.MyMap.Layers["drawLayer"] as FeaturesLayer;
            resultLayer = this.MyMap.Layers["resultLayer"] as FeaturesLayer;

            //设置被查询图层的下拉框
            #region ComboBox
            QueriedComboBox.VerticalAlignment = VerticalAlignment.Top;
            ComboBoxItem itemPoint = new ComboBoxItem();
            itemPoint.Content = SchoolLayerName;
            ComboBoxItem itemLine = new ComboBoxItem();
            itemLine.Content = RailLayerName;
            ComboBoxItem itemRegion = new ComboBoxItem();
            itemRegion.Content = ResidentialLayerName;
            QueriedComboBox.Items.Add(itemPoint);
            QueriedComboBox.Items.Add(itemLine);
            QueriedComboBox.Items.Add(itemRegion);
            QueriedComboBox.SelectedIndex = 0;
            QueriedComboBox.SelectionChanged += new SelectionChangedEventHandler(comboBox_SelectionChanged);
            #endregion
        }

        //当查询图层改变时
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //清除要素
            resultLayer.Features.Clear();
            drawLayer.Features.Clear();
            geometries.Clear();

            //隐藏FeatureDataGrid控件
            MyDataGrid.ItemsSource = null;
            MyDataGrid.Visibility = Visibility.Collapsed;
        }

        //地物缓冲区查询时显示地物图层选择下拉框和查询条件输入框
        private void EntityBufferQuery_Checked(object sender, RoutedEventArgs e)
        {
            //控制绘制几何对象按钮不能使用
            Point.IsEnabled = false;
            Line.IsEnabled = false;
            Region.IsEnabled = false;

            textBorder.Visibility = Visibility.Visible;
            queryLayerBorder.Visibility = Visibility.Visible;

            //清除要素
            resultLayer.Features.Clear();
            drawLayer.Features.Clear();
            geometries.Clear();

            //隐藏FeatureDataGrid控件
            MyDataGrid.ItemsSource = null;
            MyDataGrid.Visibility = Visibility.Collapsed;
        }

        //几何对象缓冲区查询时隐藏地物图层选择下拉框和查询条件输入框
        private void BufferQuery_Click(object sender, RoutedEventArgs e)
        {
            //控制绘制几何对象按钮可以使用
            Point.IsEnabled = true;
            Line.IsEnabled = true;
            Region.IsEnabled = true;

            textBorder.Visibility = Visibility.Collapsed;
            queryLayerBorder.Visibility = Visibility.Collapsed;

            //清除要素
            resultLayer.Features.Clear();
            drawLayer.Features.Clear();
            geometries.Clear();

            //隐藏FeatureDataGrid控件
            MyDataGrid.ItemsSource = null;
            MyDataGrid.Visibility = Visibility.Collapsed;
        }

        //平移
        private void Pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap, Cursors.Hand);
            MyMap.Action = pan;
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            //清除要素
            resultLayer.Features.Clear();
            drawLayer.Features.Clear();
            geometries.Clear();

            //隐藏FeatureDataGrid控件
            MyDataGrid.ItemsSource = null;
            MyDataGrid.Visibility = Visibility.Collapsed;
        }

        //点缓冲区查询
        private void Point_Click(object sender, RoutedEventArgs e)
        {
            DrawPoint point = new DrawPoint(MyMap);
            MyMap.Action = point;
            point.DrawCompleted += new EventHandler<DrawEventArgs>(DrawCompleted);
        }

        //线缓冲区查询
        private void Line_Click(object sender, RoutedEventArgs e)
        {
            DrawLine line = new DrawLine(MyMap);
            MyMap.Action = line;
            line.DrawCompleted += new EventHandler<DrawEventArgs>(DrawCompleted);
        }

        //面缓冲区查询
        private void Region_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon polygon = new DrawPolygon(MyMap);
            MyMap.Action = polygon;
            polygon.DrawCompleted += new EventHandler<DrawEventArgs>(DrawCompleted);
        }

        //记录绘制的几何对象并将其添加到图层上
        private void DrawCompleted(object sender, DrawEventArgs e)
        {
            //将画的图形标绘到地图上；
            Feature bufferFeature = new Feature();
            bufferFeature.Geometry = e.Geometry;
            // bufferFeature.Style = mystyle;
            drawLayer.Features.Add(bufferFeature);
            geometries.Add(bufferFeature.Geometry);
        }

        //设置缓冲区查询参数
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BufferQueryParameters parameters = new BufferQueryParameters();

            //几何对象缓冲区查询
            if (BufferQuery.IsChecked == true)
            {
                BufferParam bufferParam = new BufferParam()
                {
                    Distance = 500,
                    FromCustomGeo = true,
                    Geometries = geometries,
                    Smoothness = 12,
                    QueryMode = SpatialQueryMode.AreaIntersect,
                    ReturnBufferResult = true
                };

                QueryLayer queriedLayer = new QueryLayer();
                queriedLayer.LayerName = QueriedComboBox.SelectionBoxItem.ToString();
                QueryParam queryParam = new QueryParam() { QueryLayers = new List<QueryLayer> { queriedLayer } };
                parameters = new BufferQueryParameters() { MapName = "changchun", QueryParam = queryParam, BufferParam = bufferParam };
            }

            //地物缓冲区查询
            else
            {
                QueryLayer queryLayer = new QueryLayer();
                queryLayer.LayerName = QueryComboBox.SelectionBoxItem.ToString();
                queryLayer.WhereClause = queryText.Text;

                BufferParam bufferParam = new BufferParam()
                {
                    FromLayer = queryLayer,
                    Distance = 500,
                    FromCustomGeo = false,
                    Smoothness = 12,
                    QueryMode = SpatialQueryMode.AreaIntersect,
                    ReturnBufferResult = true
                };

                QueryLayer queriedLayer = new QueryLayer();
                queriedLayer.LayerName = QueriedComboBox.SelectionBoxItem.ToString();
                QueryParam queryParam = new QueryParam() { QueryLayers = new List<QueryLayer> { queriedLayer } };
                parameters = new BufferQueryParameters() { MapName = "changchun", QueryParam = queryParam, BufferParam = bufferParam };
            }
            BufferQueryService bufferQueryService = new BufferQueryService("http://localhost/IS/AjaxDemo");
            bufferQueryService.ProcessAsync(parameters);
            bufferQueryService.Failed += new EventHandler<ServiceFailedEventArgs>(Service_Failed);
            bufferQueryService.ProcessCompleted += new EventHandler<BufferQueryEventArgs>(bufferQueryService_ProcessCompleted);

            resultLayer.MouseLeftButtonDown += new EventHandler<FeatureMouseButtonEventArgs>(resultLayer_MouseLeftButtonDown);
        }

        //服务器返回查询结果
        private void bufferQueryService_ProcessCompleted(object sender, BufferQueryEventArgs e)
        {
            if (e.BufferResultSet == null || e.BufferResultSet.ResultSet.RecordSets == null || e.BufferResultSet.ResultSet.RecordSets.Count < 0)
            {
                MessageBox.Show("查询结果为空!");
                return;
            }
            else
            {
                MyDataGrid.Visibility = Visibility.Visible;

                //将指定查询图层的查询结果加载到矢量图层中
                foreach (RecordSet item in e.BufferResultSet.ResultSet.RecordSets)
                {
                    resultLayer.AddFeatureSet(item.ToFeatureSet());
                }

                //将查询到的地物赋予用户自定义的样式
                foreach (Feature item in resultLayer.Features)
                {
                    if (item.Geometry is GeoLine)
                    {
                        item.Style = LineSelectStyle;
                    }
                    else if (item.Geometry is GeoRegion)
                    {
                        item.Style = SelectStyle;
                    }
                    else
                    {
                        item.Style = new PredefinedMarkerStyle() { Color = new SolidColorBrush(Colors.Blue), Size = 20, Symbol = PredefinedMarkerStyle.MarkerSymbol.Diamond };
                    }
                }

                //将指定查询图层的查询缓冲区域加载到矢量图层中
                Feature buffer = new Feature
                {
                    Geometry = e.BufferResultSet.BufferRegion,
                };
                resultLayer.AddFeature(buffer);
            }
        }

        //在地图中通过鼠标左键选择地物
        private void resultLayer_MouseLeftButtonDown(object sender, FeatureMouseButtonEventArgs args)
        {
            //如果矢量已选中，再点击变为非选中状态；反之亦然
            args.Feature.Selected = !args.Feature.Selected;

            //FeatureDataGrid 移动到选中地物的记录
            if (args.Feature.Selected)
            {
                MyDataGrid.ScrollIntoView(args.Feature, null);
            }
            args.Handled = true;
        }

        //查询出错，提示错误信息
        void Service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }
    }
}
