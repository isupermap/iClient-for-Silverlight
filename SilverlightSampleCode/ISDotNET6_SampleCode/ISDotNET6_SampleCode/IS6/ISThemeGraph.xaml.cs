﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISThemeGraph : UserControl
    {
        private List<String> layerNames = new List<String>();
        private TiledDynamicISLayer themeLayer;
        public ISThemeGraph()
        {
            InitializeComponent();
            themeLayer = MyMap.Layers["ISLayer"] as TiledDynamicISLayer;
            //可选统计专题图符号形状
            setGraphType.Items.Add(GraphType.Bar3D);
            setGraphType.Items.Add(GraphType.Area);
            setGraphType.Items.Add(GraphType.Bar);
            setGraphType.Items.Add(GraphType.Line);
            setGraphType.Items.Add(GraphType.Pie);
            setGraphType.Items.Add(GraphType.Pie3D);
            setGraphType.Items.Add(GraphType.Point);
            setGraphType.Items.Add(GraphType.Doughnut);
            setGraphType.Items.Add(GraphType.Rose);
            setGraphType.Items.Add(GraphType.Rose3D);
            setGraphType.Items.Add(GraphType.StackedBar);
            setGraphType.Items.Add(GraphType.Step);
            setGraphType.Items.Add(GraphType.StackedBar3D);
            setGraphType.SelectedIndex = 0;

            //从服务器获取地图中所有图层信息
            GetMapStatusParameters mapStatus = new GetMapStatusParameters() { MapName = "World" };
            GetMapStatusService mapStatusService = new GetMapStatusService("http://localhost/IS/AjaxDemo");
            mapStatusService.ProcessAsync(mapStatus);
            mapStatusService.ProcessCompleted += new EventHandler<GetMapStatusEventArgs>(mapStatusService_ProcessCompleted);
            mapStatusService.Failed += new EventHandler<ServiceFailedEventArgs>(mapStatusService_Failed);
        }

        //获取服务器地图图层失败提示错误信息
        private void mapStatusService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //获取服务器地图中图层名
        private void mapStatusService_ProcessCompleted(object sender, GetMapStatusEventArgs e)
        {
            foreach (ServerLayer layer in e.Result.ServerLayers)
            {
                layerNames.Add(layer.Name);
            }
        }

        //当选中子项文本时，可设置文本格式
        private void isItemText_Checked(object sender, RoutedEventArgs e)
        {
            setGraphTextFormat.IsEnabled = true;
        }

        private void isItemText_Unchecked(object sender, RoutedEventArgs e)
        {
            setGraphTextFormat.IsEnabled = false;
        }

        //当选中坐标轴时，可设坐标轴的文本和坐标线
        private void isAxes_Checked(object sender, RoutedEventArgs e)
        {
            isAxesGrid.IsEnabled = true;
        }

        private void isAxes_Unchecked(object sender, RoutedEventArgs e)
        {
            isAxesGrid.IsChecked = false;
            isAxesGrid.IsEnabled = false;
        }

        //制作统计专题图
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            List<ServerStyle> themeStyles = new List<ServerStyle>();
            themeStyles.Add(new ServerStyle() { BrushColor = new ServerColor(125, 0, 0) });
            themeStyles.Add(new ServerStyle() { BrushColor = new ServerColor(220, 50, 127) });

            ThemeGraph graph = new ThemeGraph()
            {
                Caption = "统计专题图",
                Expressions = new List<string>() { "SmArea", "Pop_1994" },
                GraduatedMode = (GraduatedMode)setGradutedMode.SelectedIndex,
                GraphStyles = themeStyles,
                GraphType = (GraphType)setGraphType.SelectedItem,
                OnTop = true,
                ShowItemText = (bool)isItemText.IsChecked,
                ItemTextFormat = (GraphTextFormat)(setGraphTextFormat.SelectedIndex + 1),
                ItemCaptions = new List<string>() { "面积", "人口" },
                ShowAxes = (bool)isAxes.IsChecked,
                ShowAxisGrid = (bool)isAxesGrid.IsChecked,
                MaxSumSize = 100,
                MinSumSize = 20,   
            };

            ThemeParameters parameters = new ThemeParameters()
            {
                MapName = "World",
                Theme = graph,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(parameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }

        //成功返回专题图
        private void service_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersKey = e.Result.LayerKey;
        }

        //服务器计算失败提示错误信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //移除专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            ThemeParameters clearThemeParameters = new ThemeParameters()
            {
                MapName = "World",
                Theme = null,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            //与服务器交互
            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(clearThemeParameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }
    }
}
