﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISThemeDotDensity : UserControl
    {
        private List<String> layerNames = new List<String>();
        private TiledDynamicISLayer themeLayer;
        public ISThemeDotDensity()
        {
            InitializeComponent();
            themeLayer = MyMap.Layers["ISLayer"] as TiledDynamicISLayer;
            //在选择字段下拉列表中设置World数据集的属性字段
            setField.Items.Add("SmArea");
            setField.Items.Add("SmPerimeter");
            setField.Items.Add("Pop_1994");
            setField.SelectedIndex = 2;

            //从服务器获取地图中所有图层信息
            GetMapStatusParameters mapStatus = new GetMapStatusParameters() { MapName = "World" };
            GetMapStatusService mapStatusService = new GetMapStatusService("http://localhost/IS/AjaxDemo");
            mapStatusService.ProcessAsync(mapStatus);
            mapStatusService.ProcessCompleted += new EventHandler<GetMapStatusEventArgs>(mapStatusService_ProcessCompleted);
            mapStatusService.Failed += new EventHandler<ServiceFailedEventArgs>(mapStatusService_Failed);
        }

        //选择的字段不同基准值也不同（点密度基准值设置）
        private void setField_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (setField.SelectedIndex)
            {
                case 0:
                    baseValue.Text = "60.32928";
                    break;
                case 1:
                    baseValue.Text = "91.74652";
                    break;
                case 2:
                    baseValue.Text = "11281396.89000";
                    break;

            }
        }

        //获取服务器地图图层失败提示错误信息
        private void mapStatusService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //获取服务器地图中图层名
        private void mapStatusService_ProcessCompleted(object sender, GetMapStatusEventArgs e)
        {
            foreach (ServerLayer layer in e.Result.ServerLayers)
            {
                layerNames.Add(layer.Name);
            }
        }

        //点密度专题图
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            //如果字段表达式为空则用下拉列表中选择字段来制作单值专题图
            string str;
            if (!string.IsNullOrEmpty(filterField.Text) && !(filterField.Text == ""))
            {
                str = filterField.Text;
            }
            else
            {
                str = setField.SelectedItem.ToString();
            }

            ThemeDotDensity dotDensity = new ThemeDotDensity()
            {
                Caption = "点密度专题图",
                Expression = str,
                DotStyle = new ServerStyle() 
                {                    
                    SymbolSize = 20, 
                    SymbolStyle = 1, 
                },
                DotValue = Convert.ToDouble(baseValue.Text)
            };

            ThemeParameters parameters = new ThemeParameters()
            {
                MapName = "World",
                Theme = dotDensity,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            //与服务器交互
            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(parameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }

        //成功返回专题图
        private void service_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersKey = e.Result.LayerKey;
        }

        //服务器计算失败提示错误信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //移除专题图，即生成一个空专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            ThemeParameters clearDotDensityParameters = new ThemeParameters()
            {
                MapName = "World",
                Theme = null,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            //与服务器交互
            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(clearDotDensityParameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }
    }
}
