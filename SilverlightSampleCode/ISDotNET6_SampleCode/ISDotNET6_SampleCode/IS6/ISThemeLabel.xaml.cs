﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISThemeLabel : UserControl
    {
        private List<String> layerNames = new List<String>();
        private TiledDynamicISLayer themeLayer;
        public ISThemeLabel()
        {
            InitializeComponent();
            themeLayer = MyMap.Layers["ISLayer"] as TiledDynamicISLayer;
            //从服务器获取地图中所有图层信息
            GetMapStatusParameters mapStatus = new GetMapStatusParameters() { MapName = "World" };
            GetMapStatusService mapStatusService = new GetMapStatusService("http://localhost/IS/AjaxDemo");
            mapStatusService.ProcessAsync(mapStatus);
            mapStatusService.ProcessCompleted += new EventHandler<GetMapStatusEventArgs>(mapStatusService_ProcessCompleted);
            mapStatusService.Failed += new EventHandler<ServiceFailedEventArgs>(mapStatusService_Failed);
        }

        //获取服务器地图图层失败提示错误信息
        private void mapStatusService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //获取服务器地图中图层名
        private void mapStatusService_ProcessCompleted(object sender, GetMapStatusEventArgs e)
        {
            foreach (ServerLayer layer in e.Result.ServerLayers)
            {
                layerNames.Add(layer.Name);
            }
        }

        //对1994年人口字段制作标签专题图
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            ThemeLabel label = new ThemeLabel()
            {
                Caption = "标签专题图",
                Expression = "Country",           
                Display = new ServerTextStyle() 
                { 
                    FontHeight = 5, 
                    FontWidth = 0, 
                    Color = new ServerColor(100, 100, 220), 
                    FontName = "微软雅黑" ,
                },  
                AutoAvoidOverlapped = true,                 
            };

            ThemeParameters parameters = new ThemeParameters()
            {
                MapName = "World",
                Theme = label,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(parameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }

        //成功返回专题图
        private void service_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersKey = e.Result.LayerKey;
        }

        //服务器计算失败提示错误信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //移除专题图，即生成一个空的专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            ThemeParameters clearThemeParameters = new ThemeParameters()
            {
                MapName = "World",
                Theme = null,
                LayerNames = layerNames,
                ThemeLayer = "World@world"
            };

            //与服务器交互
            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(clearThemeParameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }
    }
}
