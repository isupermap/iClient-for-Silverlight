﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace ISDotNET6_SampleCode
{
    public partial class ISThemeGridRange : UserControl
    {
        private List<String> layerNames = new List<String>();
        private DynamicISLayer themeLayer;
        public ISThemeGridRange()
        {
            InitializeComponent();
            themeLayer = MyMap.Layers["ISLayer"] as DynamicISLayer;
            //从服务器获取地图中所有图层信息
            GetMapStatusParameters mapStatus = new GetMapStatusParameters() { MapName = "DEM" };
            GetMapStatusService mapStatusService = new GetMapStatusService("http://localhost/IS/AjaxDemo");
            mapStatusService.ProcessAsync(mapStatus);
            mapStatusService.ProcessCompleted += new EventHandler<GetMapStatusEventArgs>(mapStatusService_ProcessCompleted);
            mapStatusService.Failed += new EventHandler<ServiceFailedEventArgs>(mapStatusService_Failed);
        }

        //获取服务器地图图层失败提示错误信息
        private void mapStatusService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //获取服务器地图中图层名
        private void mapStatusService_ProcessCompleted(object sender, GetMapStatusEventArgs e)
        {
            foreach (ServerLayer layer in e.Result.ServerLayers)
            {
                layerNames.Add(layer.Name);
            }
        }

        //栅格范围分段专题图
        private void commit_Click(object sender, RoutedEventArgs e)
        {
            List<ServerColor> colors = new List<ServerColor>();
            ServerColor color1 = new ServerColor(255, 0, 0);
            ServerColor color2 = new ServerColor(0, 255, 0);
            ServerColor color3 = new ServerColor(0, 0, 255);
            ServerColor color4 = new ServerColor(255, 255, 0);
            ServerColor color5 = new ServerColor(255, 0, 255);
            ServerColor color6 = new ServerColor(0, 255, 255);

            colors.Add(color1);
            colors.Add(color2);
            colors.Add(color3);
            colors.Add(color4);
            colors.Add(color5);
            colors.Add(color6);

            ThemeGridRange gridRange = new ThemeGridRange()
            {
                BreakValues = new List<double>() { 1200, 1250, 1300, 1350, 1400 },
                Caption = "栅格范围分段专题图",
                Displays = colors
            };

            ThemeParameters parameters = new ThemeParameters()
            {
                MapName = "DEM",
                Theme = gridRange,
                LayerNames = layerNames,
                ThemeLayer = "DEM@dem"
            };

            //与服务器交互
            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(parameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }

        //成功返回专题图
        private void service_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersKey = e.Result.LayerKey;
        }

        //服务器计算失败提示错误信息
        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        //移除专题图，即生成一个空的专题图
        private void remove_Click(object sender, RoutedEventArgs e)
        {
            ThemeParameters clearParameters = new ThemeParameters
            {
                MapName = "DEM",
                Theme = null,
                LayerNames = layerNames,
                ThemeLayer = "DEM@dem"
            };

            //与服务器交互
            ThemeService service = new ThemeService("http://localhost/IS/AjaxDemo");
            service.ProcessAsync(clearParameters);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
            service.ProcessCompleted += new EventHandler<ThemeEventArgs>(service_ProcessCompleted);
        }
    }
}
