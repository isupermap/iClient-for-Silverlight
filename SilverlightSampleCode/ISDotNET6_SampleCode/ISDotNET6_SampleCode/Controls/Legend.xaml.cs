﻿using System.Windows.Controls;
using SuperMap.Web.Mapping;

namespace ISDotNET6_SampleCode
{
    public partial class Legend : UserControl
    {
        private TiledCachedISLayer layer;

        public Legend()
        {
            InitializeComponent();
            layer = this.MyMap.Layers["myTiledCachedISLayer"] as TiledCachedISLayer;
            legend.Layer = layer;
        }
    }
}
