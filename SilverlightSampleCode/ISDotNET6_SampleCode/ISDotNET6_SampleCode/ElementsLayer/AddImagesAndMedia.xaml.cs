﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ISDotNET6_SampleCode
{
    public partial class AddImagesAndMedia : UserControl
    {
        public AddImagesAndMedia()
        {
            InitializeComponent();
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            MediaElement media = sender as MediaElement;
            media.Position = TimeSpan.FromSeconds(0);
            media.Play();
        }
    }
}
