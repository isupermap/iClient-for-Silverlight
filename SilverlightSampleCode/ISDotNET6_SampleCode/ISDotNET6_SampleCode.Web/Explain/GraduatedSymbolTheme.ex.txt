﻿等级符号专题图是利用符号的大小来表示图层的数值型的某一字段或字段表达式的值。
等级符号专题图多用于具有数量特征的地图，因此参与制作专题图的字段或字段表达式应为数值型。