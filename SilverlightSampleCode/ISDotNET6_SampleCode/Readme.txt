   注意：如果您尚未安装 Microsoft Silverlight 5 Toolkit，在运行 SampleCode 时，会出现缺少引用的错误，所缺少的 dll 显示为黄色叹号。
   解决上述问题：
     方法一：打开工程“添加引用”，引用内容为该工程文件夹下的 Lib 文件夹中对应黄色叹号的 dll。
     方法二：安装 Microsoft Silverlight 5 Toolkit 。
    
   SampleCode 中“专题图”下拉列表中的“栅格范围分段专题图”所用数据需要更换工作空间为 SuperMap IS .NET 6安装目录文件夹下SampleCodeLibrary\SampleData.smw ，否则无法显示。关于如何更换工作空间，请参见《SuperMap IS .NET 联机帮助》。
