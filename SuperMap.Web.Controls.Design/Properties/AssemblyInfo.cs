﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Controls.Design;

[assembly: AssemblyTitle("SuperMap.Web.Controls.Design")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SuperMap")]
[assembly: AssemblyProduct("SuperMap.Web.Controls.Design")]
[assembly: AssemblyCopyright("Copyright ©  2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(
    ResourceDictionaryLocation.None,
    ResourceDictionaryLocation.SourceAssembly
)]
[assembly: AssemblyVersion("6.1.2.9215")]
[assembly: AssemblyFileVersion("6.1.2.9215")]
[assembly: ProvideMetadata(typeof(MetadataRegistration))]