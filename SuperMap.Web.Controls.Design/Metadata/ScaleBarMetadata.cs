﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using Microsoft.Windows.Design;
using SuperMap.Web.Controls.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Controls.Design
{
    internal class ScaleBarMetadata : AttributeTableBuilder
    {
        public ScaleBarMetadata()
            : base()
        {
            AddCallback(typeof(ScaleBar), b =>
               {
                   b.AddCustomAttributes(new Attribute[]
                          {
                              new ToolboxCategoryAttribute("SuperMap.Web/Controls", false) 
                          });
                   b.AddCustomAttributes(
                Extensions.GetMemberName<ScaleBar>(x => x.Map),
               new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.ScaleBarSetting)
                        });
                   b.AddCustomAttributes(
               Extensions.GetMemberName<ScaleBar>(x => x.LatitudeSpan),
              new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.ScaleBarSetting)
                        });
                   b.AddCustomAttributes(
               Extensions.GetMemberName<ScaleBar>(x => x.TargetHeight),
              new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.ScaleBarSetting)
                        });
                   b.AddCustomAttributes(
               Extensions.GetMemberName<ScaleBar>(x => x.TargetWidth),
              new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.ScaleBarSetting)
                        });
               });
        }
    }
}
