﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using Microsoft.Windows.Design;
using SuperMap.Web.Controls.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Controls.Design
{
    internal class OverviewMapMetadata : AttributeTableBuilder
    {
        public OverviewMapMetadata()
            : base()
        {
            AddCallback(typeof(OverviewMap), b =>
               {
                   b.AddCustomAttributes(new Attribute[]
                          {
                              new ToolboxCategoryAttribute("SuperMap.Web/Controls", false) 
                          });
                   b.AddCustomAttributes(
                  Extensions.GetMemberName<OverviewMap>(x => x.Map),
                 new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.OverviewMapSetting)
                        });
                   b.AddCustomAttributes(
                Extensions.GetMemberName<OverviewMap>(x => x.Layer),
               new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.OverviewMapSetting)
                        });
                   b.AddCustomAttributes(
                Extensions.GetMemberName<OverviewMap>(x => x.DisableOverviewMapZoom),
               new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.OverviewMapSetting)
                        });
               });
        }
    }
}
