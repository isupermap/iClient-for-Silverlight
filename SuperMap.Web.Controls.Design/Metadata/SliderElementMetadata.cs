﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using Microsoft.Windows.Design;

namespace SuperMap.Web.Controls.Design
{
    internal class SliderElementMetadata : AttributeTableBuilder
    {
        public SliderElementMetadata()
            : base()
        {
            AddCallback(typeof(SliderElement), b =>
            {
                b.AddCustomAttributes(new ToolboxBrowsableAttribute(false));
            });
        }
    }
}
