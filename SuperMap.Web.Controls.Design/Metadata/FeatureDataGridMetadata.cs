﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using Microsoft.Windows.Design;
using SuperMap.Web.Controls.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Controls.Design
{
    internal class FeatureDataGridMetadata : AttributeTableBuilder
    {
        public FeatureDataGridMetadata()
            : base()
        {
            AddCallback(typeof(FeatureDataGrid), b =>
               {
                   b.AddCustomAttributes(new Attribute[]
                          {
                              new ToolboxCategoryAttribute("SuperMap.Web/Controls", false) 
                          });
                   b.AddCustomAttributes(
              Extensions.GetMemberName<FeatureDataGrid>(x => x.Map),
              new Attribute[] 
                        { 
                             new CategoryAttribute(Properties.Resources.FeatureDataGridSetting)
                        });
                   b.AddCustomAttributes(
             Extensions.GetMemberName<FeatureDataGrid>(x => x.ItemsSource),
             new Attribute[] 
                        { 
                             new CategoryAttribute(Properties.Resources.FeatureDataGridSetting)
                        });
                   b.AddCustomAttributes(
                               Extensions.GetMemberName<FeatureDataGrid>(x => x.FeaturesLayer),
                               new Attribute[] 
                        { 
                             new CategoryAttribute(Properties.Resources.FeatureDataGridSetting)
                        });
               });
        }
    }
}
