﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using Microsoft.Windows.Design;
using SuperMap.Web.Controls.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Controls.Design
{
    internal class MapProgressBarMetadata : AttributeTableBuilder
    {
        public MapProgressBarMetadata()
            : base()
        {
            AddCallback(typeof(MapProgressBar), b =>
               {
                   b.AddCustomAttributes(new Attribute[]
                          {
                              new ToolboxCategoryAttribute("SuperMap.Web/Controls", false) 
                          });
                   b.AddCustomAttributes(
                   Extensions.GetMemberName<MapProgressBar>(x => x.Map),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.MapProgressBarSetting)
                                            });
               });
        }
    }
}
