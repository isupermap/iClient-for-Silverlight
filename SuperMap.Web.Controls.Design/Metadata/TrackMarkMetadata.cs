﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using Microsoft.Windows.Design;

namespace SuperMap.Web.Controls.Design.Metadata
{
    internal class TrackMarkMetadata : AttributeTableBuilder
    {
        public TrackMarkMetadata()
            : base()
        {
            AddCallback(typeof(TrackMark), b =>
            {
                b.AddCustomAttributes(new ToolboxBrowsableAttribute(false));
            });
        }
    }
}
