﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Controls.Design.Common;
using System.ComponentModel;
using Microsoft.Windows.Design;

namespace SuperMap.Web.Controls.Design
{
    internal class BookmarkMetadata : AttributeTableBuilder
    {
        public BookmarkMetadata()
            : base()
        {
            AddCallback(typeof(Bookmark), b =>
            {
                b.AddCustomAttributes(new Attribute[]
                      {
                          new ToolboxCategoryAttribute("SuperMap.Web/Controls", false) 
                      });

                b.AddCustomAttributes(
                      Extensions.GetMemberName<Bookmark>(x => x.Bookmarks),
                      new Attribute[] 
                        { 
                            new EditorBrowsableAttribute( EditorBrowsableState.Never)
                        });
                b.AddCustomAttributes(
                  Extensions.GetMemberName<Bookmark>(x => x.Map),
                  new Attribute[] 
                        { 
                             new CategoryAttribute(Properties.Resources.BookmarkSetting)
                        });
                b.AddCustomAttributes(
              Extensions.GetMemberName<Bookmark>(x => x.Title),
              new Attribute[] 
                        { 
                             new CategoryAttribute(Properties.Resources.BookmarkSetting)
                        });
            });
        }
    }
}
