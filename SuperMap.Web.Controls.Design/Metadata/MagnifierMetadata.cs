﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Controls.Design.Common;
using System.ComponentModel;
using Microsoft.Windows.Design;

namespace SuperMap.Web.Controls.Design
{
    internal class MagnifierMetadata : AttributeTableBuilder
    {
        public MagnifierMetadata()
            : base()
        {
            AddCallback(typeof(Magnifier), b =>
            {
                b.AddCustomAttributes(new Attribute[]
                      {
                          new ToolboxCategoryAttribute("SuperMap.Web/Controls", false) 
                      });

                b.AddCustomAttributes(
                    Extensions.GetMemberName<Magnifier>(x => x.Map),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.MagnifierSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<Magnifier>(x => x.Layer),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.MagnifierSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<Magnifier>(x => x.ZoomFactor),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.MagnifierSetting)
                                            });
            });
        }
    }
}
