﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using Microsoft.Windows.Design;
using SuperMap.Web.Controls.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Controls.Design
{
    internal class ZoomSliderMetadata : AttributeTableBuilder
    {
        public ZoomSliderMetadata()
            : base()
        {
            AddCallback(typeof(ZoomSlider), b =>
               {
                   b.AddCustomAttributes(new Attribute[]
                          {
                              new ToolboxCategoryAttribute("SuperMap.Web/Controls", false) 
                          });
                   b.AddCustomAttributes(
               Extensions.GetMemberName<ZoomSlider>(x => x.Map),
              new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.ZoomSliderSetting)
                        });
               });
        }
    }
}
