﻿
using System.Collections.Generic;
using SuperMap.Web.Core;
using System.Json;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ServerGeometry_Title}</para>
    /// 	<para>${IS6_ServerGeometry_Description}</para>
    /// </summary>
    public class ServerGeometry
    {
        /// <summary>${IS6_ServerGeometry_constructor_None_D}</summary>
        public ServerGeometry()
        {
            ID = -1;
        }

        /// <summary>${IS6_ServerGeometry_attribute_id_D}</summary>
        public int ID { get; set; }
        /// <summary>${IS6_ServerGeometry_attribute_feature_D}</summary>
        public ServerFeatureType Feature { get; set; }

        //第n部分的点数！
        /// <summary>${IS6_ServerGeometry_attribute_parts_D}</summary>
        public IList<int> Parts { get; set; }
        /// <summary>${IS6_ServerGeometry_attribute_point2Ds_D}</summary>
        public Point2DCollection Point2Ds { get; set; }

        /// <summary>${IS6_ServerGeometry_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_ServerGeometry_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_ServerGeometry_method_FromJson_return}</returns>
        public static ServerGeometry FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ServerGeometry geometry = new ServerGeometry();
            geometry.Feature = (ServerFeatureType)(int)jsonObject["feature"];
            geometry.ID = (int)jsonObject["id"];
            JsonArray parts = (JsonArray)jsonObject["parts"];
            if (parts != null && parts.Count > 0)
            {
                geometry.Parts = new List<int>();
                for (int i = 0; i < parts.Count; i++)
                {
                    geometry.Parts.Add((int)parts[i]);
                }
            }
            JsonArray point2Ds = (JsonArray)jsonObject["points"];
            if (point2Ds != null && point2Ds.Count > 0)
            {
                geometry.Point2Ds = new Point2DCollection();
                for (int i = 0; i < point2Ds.Count; i++)
                {
                    geometry.Point2Ds.Add(JsonHelper.ToPoint2D((JsonObject)point2Ds[i]));
                }
            }
            return geometry;
        }

        internal static string ToJson(ServerGeometry serverGeometry)
        {
            if (serverGeometry == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"feature\":{0}", (int)serverGeometry.Feature));
            list.Add(string.Format("\"id\":{0}", serverGeometry.ID));

            if (serverGeometry.Parts != null && serverGeometry.Parts.Count > 0)
            {
                List<string> parts = new List<string>();
                foreach (int i in serverGeometry.Parts)
                {
                    parts.Add(i.ToString());
                }
                list.Add(string.Format("\"parts\":[{0}]", string.Join(",", parts.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"parts\":[]"));
            }
            if (serverGeometry.Point2Ds != null && serverGeometry.Point2Ds.Count > 0)
            {
                List<string> ps = new List<string>();
                foreach (Point2D p in serverGeometry.Point2Ds)
                {
                    ps.Add(JsonHelper.FromPoint2D(p));
                }
                list.Add(string.Format("\"points\":[{0}]", string.Join(",", ps.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"points\":null"));
            }
            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal GeoPoint ToGeoPoint()
        {
            if (this.Feature == ServerFeatureType.Point)
            {
                return new GeoPoint(this.Point2Ds[0].X, this.Point2Ds[0].Y);
            }
            return null;
        }

        internal GeoLine ToGeoLine()
        {
            if (this.Parts != null)
            {
                List<Point2DCollection> pss = new List<Point2DCollection>();
                Point2DCollection copy = new Point2DCollection();

                foreach (Point2D item in this.Point2Ds)
                {
                    copy.Add(item);
                }
                for (int i = 0; i < this.Parts.Count; i++)
                {
                    Point2DCollection temp = new Point2DCollection();
                    for (int j = 0; j < this.Parts[i]; j++)
                    {
                        temp.Add(copy[j]);
                    }
                    pss.Add(temp);

                    copy.RemoveRange(0, this.Parts[i]);//把前面的删除
                } //把Point2Ds根据Parts分成一段一段的

                GeoLine line = new GeoLine();
                foreach (Point2DCollection item in pss)
                {
                    line.Parts.Add(item);
                }
                return line;
            }
            return null;
        }

        internal GeoRegion ToGeoRegion()
        {
            if (this.Parts != null)
            {
                List<Point2DCollection> pss = new List<Point2DCollection>();
                Point2DCollection copy = new Point2DCollection();
                foreach (Point2D item in this.Point2Ds)
                {
                    copy.Add(item);
                }
                for (int i = 0; i < this.Parts.Count; i++)
                {
                    Point2DCollection temp = new Point2DCollection();
                    for (int j = 0; j < this.Parts[i]; j++)
                    {
                        temp.Add(copy[j]);
                    }
                    temp.Add(copy[0]); //面的话把第一个点作为最后一个点
                    pss.Add(temp);
                    copy.RemoveRange(0, this.Parts[i]);//把前面的删除
                } //把Point2Ds根据Parts分成一段一段的

                GeoRegion region = new GeoRegion();
                foreach (Point2DCollection item in pss)
                {
                    region.Parts.Add(item);
                }
                return region;
            }
            return null;
        }



    }
}
