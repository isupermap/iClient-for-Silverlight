﻿
using System;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_ServerColor_Title}</summary>
    public class ServerColor
    {
        /// <summary>${IS6_ServerColor_constructor_None_D}</summary>
        /// <overloads>${IS6_ServerColor_constructor_overloads_D}</overloads>
        public ServerColor()
        { }
        /// <summary>${IS6_ServerColor_constructor_Int_D}</summary>
        /// <param name="red">${IS6_ServerColor_constructor_Int_param_r}</param>
        /// <param name="green">${IS6_ServerColor_constructor_Int_param_g}</param>
        /// <param name="blue">${IS6_ServerColor_constructor_Int_param_b}</param>
        public ServerColor(int red, int green, int blue)
        {
            Red = red;
            Green = green;
            Blue = blue;
        }
        /// <summary>${IS6_ServerColor_attribute_Red_D}</summary>
        public int Red { get; set; }
        /// <summary>${IS6_ServerColor_attribute_Green_D}</summary>
        public int Green { get; set; }
        /// <summary>${IS6_ServerColor_attribute_Blue_D}</summary>
        public int Blue { get; set; }

        //ServerColor返回的int：    
        // byte[] brg = new byte[4] { c.B, c.G, c.R, c.A };
        // int pixel = BitConverter.ToInt32(brg, 0);

        internal static int ToJson(ServerColor param)
        {
            if (param == null)
            {
                return 0;
            }

            //byte[] brg = new byte[4] { (byte)param.Blue, (byte)param.Green, (byte)param.Red, (byte)0 };
            //int pixel = BitConverter.ToInt32(brg, 0);
            int pixel = (param.Blue << 16) | (param.Green << 8) | param.Red;
            return pixel;
        }

        /// <summary>${IS6_ServerColor_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_ServerColor_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_ServerColor_method_FromJson_return}</returns>
        public static ServerColor FromJson(int jsonObject)
        {
            int pixel = jsonObject;
            byte mask = byte.MaxValue;
            int a = pixel >> 24;
            int b = (pixel >> 16) & (mask);
            int g = (pixel >> 8) & (mask);
            int r = pixel & mask;
            ServerColor result = new ServerColor { Blue = b, Red = r, Green = g };

            return result;
        }
    }
}
