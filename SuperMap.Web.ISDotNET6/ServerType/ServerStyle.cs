﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ServerStyle_Title}</para>
    /// 	<para>${IS6_ServerStyle_Description}</para>
    /// </summary>
    /// <remarks>${IS6_ServerStyle_Remarks}</remarks>
    public class ServerStyle
    {
        /// <summary>${IS6_ServerStyle_constructor_None_D}</summary>
        public ServerStyle()
        {
            BrushOpaqueRate = 100;
            BrushBackColor = new ServerColor(255, 255, 255);
            BrushColor = new ServerColor(255, 255, 0);

            PenColor = new ServerColor(197, 198, 201);
            PenWidth = 1;
        }

        /// <summary>${IS6_ServerStyle_attribute_brushBackColor_D}</summary>
        public ServerColor BrushBackColor { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_brushBackTransparent_D}</summary>
        public bool BrushBackTransparent { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_brushColor_D}</summary>
        public ServerColor BrushColor { get; set; }

        //渐变填充的角度，以度为单位。圆形渐变该参数不起作用。
        /// <summary>
        /// 	<para>${IS6_ServerStyle_attribute_brushGradientAngle_D_1}</para>
        /// 	<para><img style="WIDTH: 136px; HEIGHT: 83px" height="90" src="fillGradientAngleLinear0.bmp" width="159"/></para>
        /// 	<para>${IS6_ServerStyle_attribute_brushGradientAngle_D_2}</para>
        /// 	<para><img style="WIDTH: 139px; HEIGHT: 81px" height="90" src="fillGradientAngleLinear180.bmp" width="159"/></para>
        /// 	<para>${IS6_ServerStyle_attribute_brushGradientAngle_D_3}</para>
        /// 	<para><img style="WIDTH: 142px; HEIGHT: 82px" height="82" src="fillGradientAngleLinear90.bmp" width="145"/></para>
        /// 	<para>${IS6_ServerStyle_attribute_brushGradientAngle_D_4}</para>
        /// 	<para><img style="WIDTH: 144px; HEIGHT: 89px" height="91" src="fillGradientAngleLinear270.bmp" width="158"/></para>
        /// 	<para>${IS6_ServerStyle_attribute_brushGradientAngle_D_5}</para>
        /// 	<para><img src="fillGradientAngleConical0.bmp"/><img src="fillGradientAngleConical90.bmp"/></para>
        /// 	<para>${IS6_ServerStyle_attribute_brushGradientAngle_D_6}</para>
        /// </summary>
        public double BrushGradientAngle { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_brushGradientCenterOffsetX_D}</summary>
        public int BrushGradientCenterOffsetX { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_brushGradientCenterOffsetY_D}</summary>
        public int BrushGradientCenterOffsetY { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_brushGradientMode_D}</summary>
        public BrushGradientMode BrushGradientMode { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_brushOpaqueRate_D}</summary>
        public int BrushOpaqueRate { get; set; }

        //表示填充风格的ID。这里的ID是指工作空间的填充库中填充的编号。 
        /// <summary>${IS6_ServerStyle_attribute_brushStyle_D}</summary>
        public int BrushStyle { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_penColor_D}</summary>
        public ServerColor PenColor { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_penStyle_D}</summary>
        public int PenStyle { get; set; }

        //边线宽度，单位为0.1毫米。
        /// <summary>${IS6_ServerStyle_attribute_penWidth_D}</summary>
        public int PenWidth { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_symbolRotation_D}</summary>
        public double SymbolRotation { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_symbolSize_D}</summary>
        public int SymbolSize { get; set; }

        /// <summary>${IS6_ServerStyle_attribute_symbolStyle_D}</summary>
        public int SymbolStyle { get; set; }

        internal static string ToJson(ServerStyle param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"brushBackColor\":{0}", ServerColor.ToJson(param.BrushBackColor)));

            list.Add(string.Format("\"brushBackTransparent\":{0}", param.BrushBackTransparent.ToString().ToLower()));

            list.Add(string.Format("\"brushColor\":{0}", ServerColor.ToJson(param.BrushColor)));

            list.Add(string.Format("\"brushGradientAngle\":{0}", param.BrushGradientAngle));

            list.Add(string.Format("\"brushGradientCenterOffsetX\":{0}", param.BrushGradientCenterOffsetX));

            list.Add(string.Format("\"brushGradientCenterOffsetY\":{0}", param.BrushGradientCenterOffsetY));

            list.Add(string.Format("\"brushGradientMode\":{0}", (int)param.BrushGradientMode));

            list.Add(string.Format("\"brushOpaqueRate\":{0}", param.BrushOpaqueRate));

            list.Add(string.Format("\"brushStyle\":{0}", param.BrushStyle));

            list.Add(string.Format("\"penColor\":{0}", ServerColor.ToJson(param.PenColor)));

            list.Add(string.Format("\"penStyle\":{0}", param.PenStyle));

            list.Add(string.Format("\"penWidth\":{0}", param.PenWidth));

            list.Add(string.Format("\"symbolRotation\":{0}", param.SymbolRotation));

            list.Add(string.Format("\"symbolSize\":{0}", param.SymbolSize));

            list.Add(string.Format("\"symbolStyle\":{0}", param.SymbolStyle));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }

        /// <summary>${IS6_ServerStyle_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_ServerStyle_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_ServerStyle_method_FromJson_return}</returns>
        public static ServerStyle FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ServerStyle result = new ServerStyle
            {
                BrushBackColor = ServerColor.FromJson((int)jsonObject["brushBackColor"]),
                BrushBackTransparent = (bool)jsonObject["brushBackTransparent"],
                BrushColor = ServerColor.FromJson((int)jsonObject["brushColor"]),
                BrushGradientAngle = (int)jsonObject["brushGradientAngle"],
                BrushGradientCenterOffsetX = (int)jsonObject["brushGradientCenterOffsetX"],
                BrushGradientCenterOffsetY = (int)jsonObject["brushGradientCenterOffsetY"],
                BrushGradientMode = (BrushGradientMode)(int)jsonObject["brushGradientMode"],
                BrushOpaqueRate = (int)jsonObject["brushOpaqueRate"],
                BrushStyle = (int)jsonObject["brushStyle"],
                PenColor = ServerColor.FromJson((int)jsonObject["penColor"]),
                PenStyle = (int)jsonObject["penStyle"],
                PenWidth = (int)jsonObject["penWidth"],
                SymbolRotation = (double)jsonObject["symbolRotation"],
                SymbolSize = (int)jsonObject["symbolSize"],
                SymbolStyle = (int)jsonObject["symbolStyle"],
            };


            return result;
        }
    }
}
