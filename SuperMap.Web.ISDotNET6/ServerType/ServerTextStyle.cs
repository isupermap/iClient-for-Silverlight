﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{

    /// <summary>${IS6_ServerTextStyle_Title}</summary>
    public class ServerTextStyle
    {
        /// <summary>${IS6_ServerTextStyle_constructor_None_D}</summary>
        public ServerTextStyle()
        {
            BgColor = new ServerColor() { Red = 255, Green = 0, Blue = 0 };
            Color = new ServerColor() { Red = 0, Green = 255, Blue = 0 };
            FontHeight = 39;
            FontWidth = 0;
            FontName = "宋体";
            Transparent = true;
        }
        /// <summary>${IS6_ServerTextStyle_attribute_align_D}</summary>
        public ServerTextAlign Align { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_bgColor_D}</summary>
        public ServerColor BgColor { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_bold_D}</summary>
        public bool Bold { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_color_D}</summary>
        public ServerColor Color { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_fixedSize_D}</summary>
        public bool FixedSize { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_fixedTextSize_D}</summary>
        public int FixedTextSize { get; set; }

        /// <summary>${IS6_ServerTextStyle_attribute_fontHeight_D}</summary>
        public double FontHeight { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_fontWidth_D}</summary>
        public double FontWidth { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_italicAngle_D}</summary>
        public double ItalicAngle { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_fontName_D}</summary>
        public string FontName { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_italic_D}</summary>
        public bool Italic { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_shadow_D}</summary>
        public bool Shadow { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_stroke_D}</summary>
        public bool Stroke { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_outline_D}</summary>
        public bool Outline { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_transparent_D}</summary>
        public bool Transparent { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_underline_D}</summary>
        public bool Underline { get; set; }
        /// <summary>${IS6_ServerTextStyle_attribute_rotation_D}</summary>
        public double Rotation { get; set; }


        internal static string ToJson(ServerTextStyle serverTextStyle)
        {
            if (serverTextStyle == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"align\":{0}", (int)serverTextStyle.Align));


            list.Add(string.Format("\"bgColor\":{0}", ServerColor.ToJson(serverTextStyle.BgColor)));


            list.Add(string.Format("\"bold\":{0}", serverTextStyle.Bold.ToString().ToLower()));

            list.Add(string.Format("\"color\":{0}", ServerColor.ToJson(serverTextStyle.Color)));

            list.Add(string.Format("\"fixedSize\": {0}", serverTextStyle.FixedSize.ToString().ToLower()));

            list.Add(string.Format("\"fixedTextSize\":{0}", serverTextStyle.FixedTextSize));
            list.Add(string.Format("\"fontHeight\":{0}", serverTextStyle.FontHeight));
            list.Add(string.Format("\"italicAngle\":{0}", serverTextStyle.ItalicAngle));
            if (!string.IsNullOrEmpty(serverTextStyle.FontName))
            {
                list.Add(string.Format("\"fontName\":\"{0}\"", serverTextStyle.FontName));
            }
            list.Add(string.Format("\"fontWidth\":{0}", serverTextStyle.FontWidth));
            list.Add(string.Format("\"italic\":{0}", serverTextStyle.Italic.ToString().ToLower()));
            list.Add(string.Format("\"outline\":{0}", serverTextStyle.Outline.ToString().ToLower()));
            list.Add(string.Format("\"rotation\":{0}", serverTextStyle.Rotation));
            list.Add(string.Format("\"shadow\":{0}", serverTextStyle.Shadow.ToString().ToLower()));
            list.Add(string.Format("\"stroke\":{0}", serverTextStyle.Stroke.ToString().ToLower()));
            list.Add(string.Format("\"transparent\":{0}", serverTextStyle.Transparent.ToString().ToLower()));
            list.Add(string.Format("\"underline\":{0}", serverTextStyle.Underline.ToString().ToLower()));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
        /// <summary>${IS6_ServerTextStyle_method_FromJson_D}</summary>
        /// <returns>${IS6_ServerTextStyle_method_FromJson_return}</returns>
        /// <param name="jsonObject">${IS6_ServerTextStyle_method_FromJson_param_jsonObject}</param>
        public static ServerTextStyle FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ServerTextStyle result = new ServerTextStyle
            {
                Align = (ServerTextAlign)(int)jsonObject["align"],
                BgColor = ServerColor.FromJson((int)jsonObject["bgColor"]),
                Color = ServerColor.FromJson((int)jsonObject["color"]),
                FixedSize = (bool)jsonObject["fixedSize"],
                FixedTextSize = (int)jsonObject["fixedTextSize"],
                FontHeight = (double)jsonObject["fontHeight"],
                FontWidth = (double)jsonObject["fontWidth"],
                FontName = (string)jsonObject["fontName"],
                Bold = (bool)jsonObject["bold"],
                Italic = (bool)jsonObject["italic"],
                Shadow = (bool)jsonObject["shadow"],
                Stroke = (bool)jsonObject["stroke"],
                Outline = (bool)jsonObject["outline"],
                Transparent = (bool)jsonObject["transparent"],
                Underline = (bool)jsonObject["underline"],
                ItalicAngle = (double)jsonObject["italicAngle"],
                Rotation = (int)jsonObject["rotation"]
            };

            return result;
        }

    }
}

