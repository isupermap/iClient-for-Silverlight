﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ServerCenter_Title}</para>
    /// 	<para>${IS6_ServerCenter_Description}</para>
    /// </summary>
    public class ServerCenter
    {
        /// <summary>${IS6_ServerCenter_constructor_None_D}</summary>
        public ServerCenter()
        { }
        /// <summary>${IS6_ServerCenter_attribute_candidateType_D}</summary>
        public CenterCandidateType CandidateType { get; set; }
        /// <summary>${IS6_ServerCenter_attribute_id_D}</summary>
        public int ID { get; set; }
        /// <summary>${IS6_ServerCenter_attribute_maxImpedance_D}</summary>
        public double MaxImpedance { get; set; }
        /// <summary>${IS6_ServerCenter_attribute_resource_D}</summary>
        public double Resource { get; set; }

        internal static string ToJson(ServerCenter param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"candidateType\":{0}", (int)param.CandidateType));
            list.Add(string.Format("\"id\":{0}", param.ID));
            list.Add(string.Format("\"maxImpedance\":{0}", param.MaxImpedance));
            list.Add(string.Format("\"resource\":{0}", param.Resource));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
