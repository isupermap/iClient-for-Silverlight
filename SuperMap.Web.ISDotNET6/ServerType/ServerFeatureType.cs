﻿

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ServerFeatureType_Title}</para>
    /// 	<para>${IS6_ServerFeatureType_Description}</para>
    /// </summary>
    public enum ServerFeatureType
    {
        /// <summary>${IS6_ServerFeatureType_attribute_unknown_D}</summary>
        Unknown = 0,
        /// <summary>${IS6_ServerFeatureType_attribute_point_D}</summary>
        Point = 1,
        /// <summary>${IS6_ServerFeatureType_attribute_line_D}</summary>
        Line = 3,
        /// <summary>${IS6_ServerFeatureType_attribute_polygon_D}</summary>
        Polygon = 5,
        /// <summary>${IS6_ServerFeatureType_attribute_text_D}</summary>
        Text = 7,
        /// <summary>${IS6_ServerFeatureType_attribute_circle_D}</summary>
        Circle = 15,
        /// <summary>${IS6_ServerFeatureType_attribute_image_D}</summary>
        Image = 81
    }
}
