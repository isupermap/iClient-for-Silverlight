﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_ServerFieldType_Title}</summary>
    public enum ServerFieldType
    {
        /// <summary>${IS6_ServerFieldType_attribute_undefined_D}</summary>
        Undefined = 0,
        /// <summary>${IS6_ServerFieldType_attribute_boolean_D}</summary>
        Boolean = 1,
        /// <summary>${IS6_ServerFieldType_attribute_byten_D}</summary>
        Byte = 2,
        /// <summary>${IS6_ServerFieldType_attribute_integer_D}</summary>
        Integer = 3,
        /// <summary>${IS6_ServerFieldType_attribute_long_D}</summary>
        Long = 4,
        /// <summary>${IS6_ServerFieldType_attribute_currency_D}</summary>
        Currency = 5,
        /// <summary>${IS6_ServerFieldType_attribute_single_D}</summary>
        Single = 6,
        /// <summary>${IS6_ServerFieldType_attribute_double_D}</summary>
        Double = 7,
        /// <summary>${IS6_ServerFieldType_attribute_date_D}</summary>
        Date = 8,
        /// <summary>${IS6_ServerFieldType_attribute_binary_D}</summary>
        Binary = 9,
        /// <summary>${IS6_ServerFieldType_attribute_text_D}</summary>
        Text = 10,
        /// <summary>${IS6_ServerFieldType_attribute_longBinary_D}</summary>
        LongBinary = 11,
        /// <summary>${IS6_ServerFieldType_attribute_memo_D}</summary>
        Memo = 12,
        /// <summary>${IS6_ServerFieldType_attribute_char_D}</summary>
        Char = 18,
        /// <summary>${IS6_ServerFieldType_attribute_numeric_D}</summary>
        Numeric = 19,
        /// <summary>${IS6_ServerFieldType_attribute_time_D}</summary>
        Time = 22,
        /// <summary>${IS6_ServerFieldType_attribute_nchar_D}</summary>
        Nchar = 127,
        /// <summary>${IS6_ServerFieldType_attribute_geometry_D}</summary>
        Geometry = 128,
        /// <summary>${IS6_ServerFieldType_attribute_dgnLink_D}</summary>
        DgnLink = 129
    }
}
