﻿
using System.Collections.Generic;
using SuperMap.Web.Core;
using System.Json;
using System;
using System.Windows.Browser;
using System.Windows;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ClearCacheService_Title}</para>
    /// 	<para>${IS6_ClearCacheService_Description}</para>
    /// </summary>
    public class ClearCacheService : ServiceBase
    {
        /// <summary>${IS6_ClearCacheService_constructor_None_D}</summary>
        /// <overloads>${IS6_ClearCacheService_constructor_overloads}</overloads>
        public ClearCacheService()
        { }
        /// <summary>${IS6_ClearCacheService_constructor_String_D}</summary>
        /// <param name="url">${IS6_ClearCacheService_constructor_String_param_url}</param>
        public ClearCacheService(string url)
            : base(url)
        {
        }

        /// <summary>${IS6_ClearCacheService_method_processAsync_D}</summary>
        /// <overloads>${IS6_ClearCacheService_method_processAsync_overloads}</overloads>
        /// <param name="parameters">${IS6_ClearCacheService_method_processAsync_param_parameters}</param>
        public void ProcessAsync(ClearCacheParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_ClearCacheService_method_processAsync_D}</summary>
        /// <param name="parameters">${IS6_ClearCacheService_method_processAsync_param_parameters}</param>
        /// <param name="state">${IS6_ClearCacheService_method_processAsync_param_state}</param>
        public void ProcessAsync(ClearCacheParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("ClearCacheParameters is Null");
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }
            base.SubmitRequest(base.Url + "/common.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(ClearCacheParameters parameters)
        {

            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (parameters.MapName == null)
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.MapNameIsNull);
                //throw new InvalidOperationException("MapName is not set");
            }

            string method = "ClearCache";
            dictionary.Add("method", method);
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("mapName", parameters.MapName);

            if (parameters.Rectangle2D.Width != 0 && parameters.Rectangle2D.Height != 0)
            {
                dictionary.Add("rect", JsonHelper.FromRectangle2D(parameters.Rectangle2D));
            }
            return dictionary;
        }


        private ClearCacheResult lastResult;

        /// <summary>${IS6_ClearCacheService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ClearCacheEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonValue jsonValue = JsonObject.Parse(e.Result);
            ClearCacheResult result = ClearCacheResult.FromJson(jsonValue);
            LastResult = result;
            ClearCacheEventArgs args = new ClearCacheEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(ClearCacheEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_ClearCacheService_attribute_lastResult_D}</summary>
        public ClearCacheResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}

