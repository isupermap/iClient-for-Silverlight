﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ClearCacheEventArgs_Title}</para>
    /// 	<para>${IS6_ClearCacheEventArgs_Description}</para>
    /// </summary>
    public class ClearCacheEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_ClearCacheEventArgs_constructor_D}</summary>
        public ClearCacheEventArgs(ClearCacheResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_ClearCacheEventArgs_attribute_result_D}</summary>
        public ClearCacheResult Result { get; private set; }
        /// <summary>${IS6_ClearCacheEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
