﻿using System.Json;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ClearCacheResult_Title}</para>
    /// 	<para>${IS6_ClearCacheResult_Description}</para>
    /// </summary>
    public class ClearCacheResult
    {
        internal ClearCacheResult()
        { }
        /// <summary>${IS6_ClearCacheResult_attribute_succeed_D}</summary>
        public bool Succeed { get; private set; }

        /// <summary>${IS6_ClearCacheResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_ClearCacheResult_method_FromJson_param_jsonPrimitive}</param>
        /// <returns>${IS6_ClearCacheResult_method_FromJson_return}</returns>
        public static ClearCacheResult FromJson(JsonValue jsonValue)
        {
            if (jsonValue == null)
            {
                return null;
            }

            ClearCacheResult result = new ClearCacheResult();
            result.Succeed = (bool)jsonValue;
            return result;
        }
    }
}
