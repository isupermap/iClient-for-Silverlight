﻿
using SuperMap.Web.Core;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ClearCacheParameters_Title}</para>
    /// 	<para>${IS6_ClearCacheParameters_Description}</para>
    /// </summary>
    public class ClearCacheParameters : ParametersBase
    {
        /// <summary>${IS6_ClearCacheParameters_constructor_None_D}</summary>
        public ClearCacheParameters()
        { }
        /// <summary>${IS6_ClearCacheParameters_attribute_Rectangle2D_D}</summary>
        public Rectangle2D Rectangle2D { get; set; }
    }
}
