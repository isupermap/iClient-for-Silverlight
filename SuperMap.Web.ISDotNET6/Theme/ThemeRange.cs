﻿
using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.ISDotNET6
{

    /// <summary>
    /// 	<para>${IS6_ThemeRange_Title}</para>
    /// 	<para>${IS6_ThemeRange_Description}</para>
    /// 	<para><img src="ThemeDotDensity.png"/></para>
    /// 	<para>${IS6_ThemeRange_Description_1}</para>
    /// </summary>
    public class ThemeRange : Theme
    {
        //---ScottXu  2010/1/11---
        /// <summary>${IS6_ThemeRange_constructor_None_D}</summary>
        public ThemeRange()
        {
            ForeignDataParam = new ForeignDataParam();
            Displays = new List<ServerStyle>();
        }

        /// <summary>${IS6_ThemeRange_attribute_breakValues_D}</summary>
        public IList<double> BreakValues { get; set; }
        /// <summary>${IS6_ThemeRange_attribute_displays_D}</summary>
        public IList<ServerStyle> Displays { get; set; }
        /// <summary>${IS6_ThemeRange_attribute_expression_D}</summary>
        public string Expression { get; set; }


        internal static string ToJson(ThemeRange param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (param.BreakValues != null && param.BreakValues.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < param.BreakValues.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", param.BreakValues[i]));
                }
                list.Add(string.Format("\"breakValues\":[{0}]", string.Join(",", temp.ToArray())));
            }

            if (param.Displays.Count > 0 && param.Displays != null)
            {
                List<string> l = new List<string>();
                for (int i = 0; i < param.Displays.Count; i++)
                {
                    l.Add(ServerStyle.ToJson(param.Displays[i]));
                }
                list.Add(string.Format("\"displays\":{0}", JsonHelper.FromIList(l)));
            }

            if (!string.IsNullOrEmpty(param.Expression))
            {
                list.Add(string.Format("\"expression\":\"{0}\"", param.Expression));
            }

            json += string.Join(",", list.ToArray());
            json += ",";
            json += Theme.AddList(param);
            json += "}";

            return json;
        }
        /// <summary>${IS6_ThemeRange_method_FromJson_D}</summary>
        /// <returns>${IS6_ThemeRange_method_FromJson_return}</returns>
        /// <param name="jsonObject">${IS6_ThemeRange_method_FromJson_param_jsonObject}</param>
        public static ThemeRange FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeRange result = new ThemeRange
            {
                Caption = (string)jsonObject["caption"],
                Filter = (string)jsonObject["filter"],
                MaxScale = (double)jsonObject["maxScale"],
                MinScale = (double)jsonObject["minScale"],
                ForeignDataParam = ForeignDataParam.FromJson((JsonObject)jsonObject["foreignDataParam"])
            };

            if (jsonObject.ContainsKey("breakValues") && jsonObject["breakValues"] != null && jsonObject["breakValues"].Count > 0)
            {
                result.BreakValues = new List<double>();
                for (int i = 0; i < jsonObject["breakValues"].Count; i++)
                {
                    result.BreakValues.Add((double)jsonObject["breakValues"][i]);
                }
            }

            if (jsonObject.ContainsKey("displays") && jsonObject["displays"] != null && jsonObject["displays"].Count > 0)
            {
                result.Displays = new List<ServerStyle>();
                for (int i = 0; i < jsonObject["displays"].Count; i++)
                {
                    result.Displays.Add(ServerStyle.FromJson((JsonObject)jsonObject["displays"][i]));
                }
            }

            result.Expression = (string)jsonObject["expression"];

            return result;
        }
    }
}
