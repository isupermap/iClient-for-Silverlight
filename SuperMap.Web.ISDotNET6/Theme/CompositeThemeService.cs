﻿
//====================
//--- ScottXu ---2010/01/22---
//====================

using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.ISDotNET6.Resources;
using System.Text;
using SuperMap.Web.Service;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_CompositeThemeService_Title}</para>
    /// 	<para>${IS6_CompositeThemeService_Description}</para>
    /// </summary>
    public class CompositeThemeService : ServiceBase
    {

        /// <summary>${IS6_CompositeThemeService_constructor_None_D}</summary>
        /// <overloads>${IS6_CompositeThemeService_constructor_overloads_D}</overloads>
        public CompositeThemeService()
        { }
        /// <summary>${IS6_CompositeThemeService_constructor_String_D}</summary>
        /// <param name="url">${IS6_CompositeThemeService_constructor_String_param_url}</param>
        public CompositeThemeService(string url)
            : base(url)
        {
            //base.Url = url + "/common.ashx?";
        }

        /// <summary>${IS6_CompositeThemeService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_CompositeThemeService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="parameters">${IS6_CompositeThemeService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(CompositeThemeParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_CompositeThemeService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_CompositeThemeService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_CompositeThemeService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(CompositeThemeParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("CompositeThemeParameters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            base.SubmitRequest(base.Url + "/common.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(CompositeThemeParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "UpdateLayers";
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);

            string json = "[";
            List<string> list = new List<string>();

            if (parameters.LayerNames != null && parameters.LayerNames.Count > 0 && parameters.ParamList != null && parameters.ParamList.Count > 0)
            {
                //遍历每个图层；
                for (int k = 0; k < parameters.LayerNames.Count; k++)
                {
                    for (int i = 0; i < parameters.ParamList.Count; i++)
                    {
                        //找要做专题图的图层；
                        if (parameters.LayerNames[k] == parameters.ParamList[i].ThemeLayer)
                        {
                            List<string> temp = new List<string>();
                            //遍历图层上的每个专题图；
                            for (int j = 0; j < parameters.ParamList[i].Themes.Count; j++)
                            {
                                #region
                                if (parameters.ParamList[i].Themes[j] is ThemeUnique)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("\"themeUnique\":{0}", ThemeUnique.ToJson((ThemeUnique)parameters.ParamList[i].Themes[j]));
                                    temp.Add(sb.ToString());
                                }
                                else if (parameters.ParamList[i].Themes[j] is ThemeRange)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("\"themeRange\":{0}", ThemeRange.ToJson((ThemeRange)parameters.ParamList[i].Themes[j]));
                                    temp.Add(sb.ToString());
                                }
                                else if (parameters.ParamList[i].Themes[j] is ThemeLabel)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("\"themeLabel\":{0}", ThemeLabel.ToJson((ThemeLabel)parameters.ParamList[i].Themes[j]));
                                    temp.Add(sb.ToString());
                                }
                                else if (parameters.ParamList[i].Themes[j] is ThemeGraph)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("\"themeGraph\":{0}", ThemeGraph.ToJson((ThemeGraph)parameters.ParamList[i].Themes[j]));
                                    temp.Add(sb.ToString());
                                }
                                else if (parameters.ParamList[i].Themes[j] is ThemeDotDensity)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("\"themeDotDensity\":{0}", ThemeDotDensity.ToJson((ThemeDotDensity)parameters.ParamList[i].Themes[j]));
                                    temp.Add(sb.ToString());
                                }
                                else if (parameters.ParamList[i].Themes[j] is ThemeGraduatedSymbol)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("\"themeGraduatedSymbol\":{0}", ThemeGraduatedSymbol.ToJson((ThemeGraduatedSymbol)parameters.ParamList[i].Themes[j]));
                                    temp.Add(sb.ToString());
                                }
                                else if (parameters.ParamList[i].Themes[j] is ThemeGridRange)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormat("\"themeGridRange\":{0}", ThemeGridRange.ToJson((ThemeGridRange)parameters.ParamList[i].Themes[j]));
                                    temp.Add(sb.ToString());
                                }
                                #endregion
                            }

                            string str = "{";
                            str += string.Join(",", temp.ToArray());
                            str += "}";
                            list.Add(str);
                        }
                        else
                        {
                            list.Add(string.Format("\"{0}\"", "!@"));
                        }
                    }
                }
            }

            json += string.Join(",", list.ToArray());
            json += "]";

            dictionary.Add("layers", json);
            dictionary.Add("layersKey", "0");
            dictionary.Add("bModifiedByServer", "false");

            return dictionary;
        }


        private ThemeResult lastResult;

        /// <summary>${IS6_CompositeThemeService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ThemeEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonPrimitive jsonObject = (JsonPrimitive)JsonObject.Parse(e.Result);

            ThemeResult result = ThemeResult.FromJson(jsonObject);
            LastResult = result;
            ThemeEventArgs args = new ThemeEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(ThemeEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_CompositeThemeService_attribute_lastResult_D}</summary>
        public ThemeResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}