﻿//===================
//  ScottXu ---2010/01/20---
//===================

using System;
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_LabelAvoidPriority_Title}</para>
    /// 	<para>${IS6_LabelAvoidPriority_Description}</para>
    /// </summary>
    public class LabelAvoidPriority
    {
        /// <summary>${IS6_LabelAvoidPriority_constructor_None_D}</summary>
        public LabelAvoidPriority()
        {
        }

        //The syntax T? is shorthand for Nullable<(Of <(T>)>), where T  is a value type. The two forms are interchangeable.
        /// <summary>${IS6_LabelAvoidPriority_attribute_baseCenter_D}</summary>
        public int? BaseCenter { get; set; }
        /// <summary>${IS6_LabelAvoidPriority_attribute_baseLeft_D}</summary>
        public int? BaseLeft { get; set; }
        /// <summary>${IS6_LabelAvoidPriority_attribute_baseRight_D}</summary>
        public int? BaseRight { get; set; }

        /// <summary>${IS6_LabelAvoidPriority_attribute_bottomCenter_D}</summary>
        public int? BottomCenter { get; set; }
        /// <summary>${IS6_LabelAvoidPriority_attribute_bottomLeft_D}</summary>
        public int? BottomLeft { get; set; }
        /// <summary>${IS6_LabelAvoidPriority_attribute_bottomRight_D}</summary>
        public int? BottomRight { get; set; }

        /// <summary>${IS6_LabelAvoidPriority_attribute_center_D}</summary>
        public int? Center { get; set; }
        /// <summary>${IS6_LabelAvoidPriority_attribute_centerLeft_D}</summary>
        public int? CenterLeft { get; set; }
        /// <summary>${IS6_LabelAvoidPriority_attribute_centerRight_D}</summary>
        public int? CenterRight { get; set; }

        /// <summary>${IS6_LabelAvoidPriority_attribute_topCenter_D}</summary>
        public int? TopCenter { get; set; }
        /// <summary>${IS6_LabelAvoidPriority_attribute_topLeft_D}</summary>
        public int? TopLeft { get; set; }
        /// <summary>${IS6_LabelAvoidPriority_attribute_topRight_D}</summary>
        public int? TopRight { get; set; }

        internal static string ToJson(LabelAvoidPriority param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (param.BaseCenter != null)
            {
                list.Add(string.Format("\"baseCenter\":{0}", param.BaseCenter));
            }
            else
            {
                list.Add("\"baseCenter\":null");
            }

            if (param.BaseLeft != null)
            {
                list.Add(string.Format("\"baseLeft\":{0}", param.BaseLeft));
            }
            else
            {
                list.Add("\"baseLeft\":null");
            }

            if (param.BaseRight != null)
            {
                list.Add(string.Format("\"baseRight\":{0}", param.BaseRight));
            }
            else
            {
                list.Add("\"baseRight\":null");
            }

            if (param.BottomCenter != null)
            {
                list.Add(string.Format("\"bottomCenter\":{0}", param.BottomCenter));
            }
            else
            {
                list.Add("\"bottomCenter\":null");
            }

            if (param.BottomLeft != null)
            {
                list.Add(string.Format("\"bottomLeft\":{0}", param.BottomLeft));
            }
            else
            {
                list.Add("\"bottomLeft\":null");
            }

            if (param.BottomRight != null)
            {
                list.Add(string.Format("\"bottomRight\":{0}", param.BottomRight));
            }
            else
            {
                list.Add("\"bottomRight\":null");
            }

            if (null != param.Center)
            {
                list.Add(string.Format("\"center\":{0}", param.Center));
            }
            else
            {
                list.Add("\"center\":null");
            }

            if (null != param.CenterLeft)
            {
                list.Add(string.Format("\"centerLeft\":{0}", param.CenterLeft));
            }
            else
            {
                list.Add("\"centerLeft\":null");
            }

            if (null != param.CenterRight)
            {
                list.Add(string.Format("\"centerRight\":{0}", param.CenterRight));
            }
            else
            {
                list.Add("\"centerRight\":null");
            }

            if (null != param.TopCenter)
            {
                list.Add(string.Format("\"topCenter\":{0}", param.TopCenter));
            }
            else
            {
                list.Add("\"topCenter\":null");
            }

            if (null != param.TopLeft)
            {
                list.Add(string.Format("\"topLeft\":{0}", param.TopLeft));
            }
            else
            {
                list.Add("\"topLeft\":null");
            }

            if (null != param.TopRight)
            {
                list.Add(string.Format("\"topRight\":{0}", param.TopRight));
            }
            else
            {
                list.Add("\"topRight\":null");
            }
            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }

        /// <summary>${IS6_LabelAvoidPriority_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_LabelAvoidPriority_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_LabelAvoidPriority_method_FromJson_return}</returns>
        public static LabelAvoidPriority FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            return new LabelAvoidPriority
            {
                BaseCenter = jsonObject["baseCenter"],
                BaseLeft = jsonObject["baseLeft"],
                BaseRight = jsonObject["baseRight"],
                BottomCenter = jsonObject["bottomCenter"],
                BottomLeft = jsonObject["bottomLeft"],
                BottomRight = jsonObject["bottomRight"],
                Center = jsonObject["center"],
                CenterLeft = jsonObject["centerLeft"],
                CenterRight = jsonObject["centerRight"],
                TopCenter = jsonObject["topCenter"],
                TopLeft = jsonObject["topLeft"],
                TopRight = jsonObject["topRight"]
            };
        }
    }
}
