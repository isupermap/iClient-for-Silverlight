﻿
namespace SuperMap.Web.ISDotNET6
{

    /// <summary>${IS6_LabelLineDirection_Title}</summary>
    public enum LabelLineDirection
    {
        /// <summary>${IS6_LabelLineDirection_attribute_alongLine_D}</summary>
        AlongLine = 0,
        /// <summary>${IS6_LabelLineDirection_attribute_leftTopToRightBottom_D}</summary>
        LeftTopToRightBottom = 1,
        /// <summary>${IS6_LabelLineDirection_attribute_rightTopToLeftBottom_D}</summary>
        RightTopToLeftBottom = 2,
        /// <summary>${IS6_LabelLineDirection_attribute_leftBottomToRightTop_D}</summary>
        LeftBottomToRightTop = 3,
        /// <summary>${IS6_LabelLineDirection_attribute_rightBottomToLeftTop_D}</summary>
        RightBottomToLeftTop = 4,
    }
}
