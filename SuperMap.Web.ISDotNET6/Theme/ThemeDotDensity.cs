﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeDotDensity_Title}</para>
    /// 	<para>${IS6_ThemeDotDensity_Description}</para>
    /// 	<para><img src="ThemeDotDensity.png"/></para>
    /// 	<para>${IS6_ThemeDotDensity_Description_1}</para>
    /// </summary>
    public class ThemeDotDensity : Theme
    {
        /// <summary>${IS6_ThemeDotDensity_constructor_None_D}</summary>
        public ThemeDotDensity()
        {
            DotStyle = new ServerStyle();
        }
        /// <summary>${IS6_ThemeDotDensity_attribute_dotStyle_D}</summary>
        public ServerStyle DotStyle { get; set; }
        /// <summary>${IS6_ThemeDotDensity_attribute_dotValue_D}</summary>
        public double DotValue { get; set; }
        /// <summary>${IS6_ThemeDotDensity_attribute_expression_D}</summary>
        public string Expression { get; set; }


        internal static string ToJson(ThemeDotDensity param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (param.DotStyle != null)
            {
                list.Add(string.Format("\"dotStyle\":{0}", ServerStyle.ToJson(param.DotStyle)));
            }
            list.Add(string.Format("\"dotValue\":{0}", param.DotValue));
            if (!string.IsNullOrEmpty(param.Expression))
            {
                list.Add(string.Format("\"expression\":\"{0}\"", param.Expression));
            }

            json += string.Join(",", list.ToArray());
            json += ",";
            json += Theme.AddList(param);
            json += "}";

            return json;
        }
        /// <summary>${IS6_ThemeDotDensity_method_FromJson_D}</summary>
        /// <returns>${IS6_ThemeDotDensity_method_FromJson_return}</returns>
        /// <param name="jsonObject">${IS6_ThemeDotDensity_method_FromJson_param_jsonObject}</param>
        public static ThemeDotDensity FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            return new ThemeDotDensity
            {
                Caption = (string)jsonObject["caption"],
                Filter = (string)jsonObject["filter"],
                MaxScale = (double)jsonObject["maxScale"],
                MinScale = (double)jsonObject["minScale"],
                ForeignDataParam = ForeignDataParam.FromJson((JsonObject)jsonObject["foreignDataParam"]),

                DotStyle = ServerStyle.FromJson((JsonObject)jsonObject["dotStyle"]),
                DotValue = (double)jsonObject["dotValue"],
                Expression = (string)jsonObject["expression"]
            };

        }
    }
}
