﻿
// ScottXu  ---2010/01/19
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_Theme_Title}</para>
    /// 	<para>${IS6_Theme_Description}</para>
    /// </summary>
    public abstract class Theme
    {
        /// <summary>${IS6_Theme_constructor_None_D}</summary>
        public Theme()
        {
            ForeignDataParam = new ForeignDataParam();
        }

        //目前有6个大家都有的属性；在ThemeGridRange时存在冗余；

        /// <summary>${IS6_Theme_attribute_caption_D}</summary>
        public string Caption { get; set; }

        //这个属性对我们没用！
        //public bool Enabled { get; set; }

        /// <summary>${IS6_Theme_attribute_filter_D}</summary>
        public string Filter { get; set; }

        //外部数据关联参数。
        /// <summary>${IS6_Theme_attribute_foreignDataParam_D}</summary>
        public ForeignDataParam ForeignDataParam { get; set; }

        /// <summary>${IS6_Theme_attribute_maxScale_D}</summary>
        public double MaxScale { get; set; }

        /// <summary>${IS6_Theme_attribute_minScale_D}</summary>
        public double MinScale { get; set; }

        //父类中的东西就在父类中写，这样大家都能用到；减少冗余；
        internal static string AddList(Theme param)
        {
            if (param == null)
            {
                return null;
            }
            string json = string.Empty;
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(param.Caption))
            {
                list.Add(string.Format("\"caption\":\"{0}\"", param.Caption));
            }

            list.Add("\"enabled\":true");

            if (!string.IsNullOrEmpty(param.Filter))
            {
                list.Add(string.Format("\"filter\":\"{0}\"", param.Filter));
            }
            else
            {
                list.Add("\"filter\":null");
            }

            if (param.ForeignDataParam != null)
            {
                list.Add(string.Format("\"foreignDataParam\":{0}", ForeignDataParam.ToJson(param.ForeignDataParam)));
            }

            list.Add(string.Format("\"maxScale\":{0}", param.MaxScale));

            list.Add(string.Format("\"minScale\":{0}", param.MinScale));

            json += string.Join(",", list.ToArray());

            return json;
        }
    }
}
