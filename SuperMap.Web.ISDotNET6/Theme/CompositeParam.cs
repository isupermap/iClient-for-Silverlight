﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_CompositeParam_Title}</para>
    /// <para>${IS6_CompositeParam_Description}</para>
    /// </summary>
    public class CompositeParam
    {
        /// <summary>${IS6_CompositeParam_Constructor_None_D}</summary>
        public CompositeParam()
        {
        }
        /// <summary>${IS6_CompositeParam_attribute_themes_D}</summary>
        public IList<Theme> Themes { get; set; }
        /// <summary>${IS6_CompositeParam_attribute_themeLayer_D}</summary>
        public string ThemeLayer { get; set; }

    }
}
