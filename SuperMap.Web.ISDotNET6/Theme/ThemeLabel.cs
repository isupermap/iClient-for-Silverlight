﻿//===================
//  ScottXu ---2010/01/20---
//===================
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeLable_Title}</para>
    /// 	<para>${IS6_ThemeLable_Description}</para>
    /// 	<para><img src="ThemeLabel.png"/></para>
    /// </summary>
    public class ThemeLabel : Theme
    {
        /// <summary>${IS6_ThemeLable_constructor_None_D}</summary>
        public ThemeLabel()
        {
            Display = new ServerTextStyle();
            TextControlMode = TextControlMode.WrapText;
        }
        /// <summary>${IS6_ThemeLable_attribute_alongLine_D}</summary>
        public bool AlongLine { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_display_D}</summary>
        public ServerTextStyle Display { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_enableFlow_D}</summary>
        public bool EnableFlow { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_expression_D}</summary>  
        public string Expression { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_fixedAngle_D}</summary>
        public bool FixedAngle { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_onTop_D}</summary>
        public bool OnTop { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_ignoreSmallObject_D}</summary>
        public bool IgnoreSmallObject { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_singleLineCharCount_D}</summary>
        public int SingleLineCharCount { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_textControlMode_D}</summary>
        public TextControlMode TextControlMode { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_fixedRepeatLength_D}</summary>
        public bool FixedRepeatLength { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_repeatLength_D}</summary>
        public double RepeatLength { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_textSpacing_D}</summary>
        public double TextSpacing { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_numericFormat_D}</summary>
        public string NumericFormat { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_backShape_D}</summary>
        public TextBackShape BackShape { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_backStyle_D}</summary>
        public ServerStyle BackStyle { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_maxObjectCount_D}</summary>
        public int MaxObjectCount { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_lineDirection_D}</summary>
        public LabelLineDirection LineDirection { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_offsetXExpression_D}</summary>
        public string OffsetXExpression { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_offsetYExpression_D}</summary>
        public string OffsetYExpression { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_offsetFixed_D}</summary>
        public bool OffsetFixed { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_autoAvoidOverlapped_D}</summary>
        public bool AutoAvoidOverlapped { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_autoAvoidPriority_D}</summary>
        public LabelAvoidPriority AutoAvoidPriority { get; set; }
        /// <summary>${IS6_ThemeLable_attribute_onTopPriority_D}</summary>
        public int OnTopPriority { get; set; }


        internal static string ToJson(ThemeLabel param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"alongLine\":{0}", param.AlongLine.ToString().ToLower()));
            if (null != param.Display)
            {
                list.Add(string.Format("\"display\":{0}", ServerTextStyle.ToJson(param.Display)));
            }
            else
            {
                list.Add("\"display\":null");
            }
            list.Add(string.Format("\"enableFlow\":{0}", param.EnableFlow.ToString().ToLower()));

            if (!string.IsNullOrEmpty(param.Expression))
            {
                list.Add(string.Format("\"expression\":\"{0}\"", param.Expression));
            }

            list.Add(string.Format("\"fixedAngle\":{0}", param.FixedAngle.ToString().ToLower()));
            list.Add(string.Format("\"onTop\":{0}", param.OnTop.ToString().ToLower()));
            list.Add(string.Format("\"ignoreSmallObject\":{0}", param.IgnoreSmallObject.ToString().ToLower()));
            list.Add(string.Format("\"singleLineCharCount\":{0}", param.SingleLineCharCount));
            list.Add(string.Format("\"textControlMode\":{0}", (int)param.TextControlMode));
            list.Add(string.Format("\"fixedRepeatLength\":{0}", param.FixedRepeatLength.ToString().ToLower()));
            list.Add(string.Format("\"repeatLength\":{0}", param.RepeatLength));
            list.Add(string.Format("\"textSpacing\":{0}", param.TextSpacing));

            if (!string.IsNullOrEmpty(param.NumericFormat))
            {
                list.Add(string.Format("\"numericFormat\":\"{0}\"", param.NumericFormat));
            }
            else
            {
                list.Add("\"numericFormat\":\"\"");
            }

            list.Add(string.Format("\"backShape\":{0}", (int)param.BackShape));
            //这个属性只有Json中有，文档中没有体现，所以先不开出来；
            list.Add("\"backType\":0");

            if (param.BackStyle != null)
            {
                list.Add(string.Format("\"backStyle\":{0}", ServerStyle.ToJson(param.BackStyle)));
            }
            else
            {
                list.Add("\"backStyle\":null");
            }

            list.Add(string.Format("\"maxObjectCount\":{0}", param.MaxObjectCount));
            list.Add(string.Format("\"lineDirection\":{0}", (int)param.LineDirection));

            if (!string.IsNullOrEmpty(param.OffsetXExpression))
            {
                list.Add(string.Format("\"offsetXExpression\":\"{0}\"", param.OffsetXExpression));
            }
            else
            {
                list.Add("\"offsetXExpression\":\"\"");
            }

            if (!string.IsNullOrEmpty(param.OffsetYExpression))
            {
                list.Add(string.Format("\"offsetYExpression\":\"{0}\"", param.OffsetYExpression));
            }
            else
            {
                list.Add("\"offsetYExpression\":\"\"");
            }
            list.Add(string.Format("\"offsetFixed\":{0}", param.OffsetFixed.ToString().ToLower()));
            list.Add(string.Format("\"autoAvoidOverlapped\":{0}", param.AutoAvoidOverlapped.ToString().ToLower()));

            if (param.AutoAvoidPriority != null)
            {
                list.Add(string.Format("\"autoAvoidPriority\":{0}", LabelAvoidPriority.ToJson(param.AutoAvoidPriority)));
            }
            else
            {
                list.Add("\"autoAvoidPriority\":{}");
            }

            list.Add(string.Format("\"onTopPriority\":{0}", param.OnTopPriority));


            json += string.Join(",", list.ToArray());
            json += ",";
            json += Theme.AddList(param);
            json += "}";

            return json;
        }
        /// <summary>${IS6_ThemeLable_method_FromJson_D}</summary>
        /// <returns>${IS6_ThemeLable_method_FromJson_return}</returns>
        /// <param name="jsonObject">${IS6_ThemeLable_method_FromJson_param_jsonObject}</param>
        public static ThemeLabel FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ThemeLabel result = new ThemeLabel
            {
                Expression = (string)jsonObject["expression"],
                Display = ServerTextStyle.FromJson((JsonObject)jsonObject["display"]),
                AlongLine = (bool)jsonObject["alongLine"],
                EnableFlow = (bool)jsonObject["enableFlow"],
                FixedAngle = (bool)jsonObject["fixedAngle"],
                OnTop = (bool)jsonObject["onTop"],
                IgnoreSmallObject = (bool)jsonObject["ignoreSmallObject"],
                SingleLineCharCount = (int)jsonObject["singleLineCharCount"],
                TextControlMode = (TextControlMode)(int)jsonObject["textControlMode"],
                FixedRepeatLength = (bool)jsonObject["fixedRepeatLength"],
                RepeatLength = (int)jsonObject["repeatLength"],
                TextSpacing = (double)jsonObject["textSpacing"],
                NumericFormat = (string)jsonObject["numericFormat"],
                BackShape = (TextBackShape)(int)jsonObject["backShape"],
                BackStyle = ServerStyle.FromJson((JsonObject)jsonObject["backStyle"]),
                MaxObjectCount = (int)jsonObject["maxObjectCount"],
                LineDirection = (LabelLineDirection)(int)jsonObject["lineDirection"],
                AutoAvoidOverlapped = (bool)jsonObject["autoAvoidOverlapped"],
                OffsetFixed = (bool)jsonObject["offsetFixed"],
                OnTopPriority = (int)jsonObject["onTopPriority"],
                OffsetXExpression = (string)jsonObject["offsetXExpression"],
                OffsetYExpression = (string)jsonObject["offsetYExpression"],
                AutoAvoidPriority = LabelAvoidPriority.FromJson((JsonObject)jsonObject["autoAvoidPriority"]),

                Caption = (string)jsonObject["caption"],
                Filter = (string)jsonObject["filter"],
                MaxScale = (double)jsonObject["maxScale"],
                MinScale = (double)jsonObject["minScale"],
                ForeignDataParam = ForeignDataParam.FromJson((JsonObject)jsonObject["foreignDataParam"])

            };

            return result;
        }
    }
}
