﻿//===================
//  ScottXu ---2010/01/20---
//===================
using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeUnique_Title}</para>
    /// 	<para>${IS6_ThemeUnique_Description}</para>
    /// 	<para>
    /// 		<list type="table">
    /// 			<item>
    /// 				<term><img src="UniqueTheme1.bmp"/></term>
    /// 				<description><img src="UniqueTheme2.bmp"/></description>
    /// 			</item>
    /// 		</list>
    /// 	</para>
    /// 	<para>${IS6_ThemeUnique_Description_1}</para>
    /// </summary>
    public class ThemeUnique : Theme
    {
        /// <summary>${IS6_ThemeUnique_constructor_None_D}</summary>
        public ThemeUnique()
        {
            DefaultStyle = new ServerStyle();
        }
        /// <summary>${IS6_ThemeUnique_attribute_expression_D}</summary>
        public string Expression { get; set; }
        /// <summary>${IS6_ThemeUnique_attribute_defaultStyle_D}</summary>
        public ServerStyle DefaultStyle { get; set; }
        /// <summary>${IS6_ThemeUnique_attribute_itemCaptions_D}</summary>
        public IList<string> ItemCaptions { get; set; }
        /// <summary>${IS6_ThemeUnique_attribute_onTop_D}</summary>
        public bool OnTop { get; set; }
        /// <summary>${IS6_ThemeUnique_attribute_values_D}</summary>
        public IList<string> Values { get; set; }
        /// <summary>${IS6_ThemeUnique_attribute_displays_D}</summary>
        public IList<ServerStyle> Displays { get; set; }

        internal static string ToJson(ThemeUnique param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(param.Expression))
            {
                list.Add(string.Format("\"expression\":\"{0}\"", param.Expression));
            }

            if (param.DefaultStyle != null)
            {
                list.Add(string.Format("\"defaultStyle\":{0}", ServerStyle.ToJson(param.DefaultStyle)));
            }

            if (param.ItemCaptions != null)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < param.ItemCaptions.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", param.ItemCaptions[i]));
                }
                list.Add(string.Format("\"itemCaptions\":[{0}]", string.Join(",", temp.ToArray())));

            }
            else
            {
                list.Add("\"itemCaptions\":null");
            }

            list.Add(string.Format("\"onTop\":{0}", param.OnTop.ToString().ToLower()));

            if (param.Values != null && param.Values.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < param.Values.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", param.Values[i]));
                }
                list.Add(string.Format("\"values\":[{0}]", string.Join(",", temp.ToArray())));
            }

            if (param.Displays != null && param.Displays.Count > 0)
            {
                List<string> l = new List<string>();
                for (int i = 0; i < param.Displays.Count; i++)
                {
                    l.Add(ServerStyle.ToJson(param.Displays[i]));
                }
                list.Add(string.Format("\"displays\":{0}", JsonHelper.FromIList(l)));
            }

            json += string.Join(",", list.ToArray());
            json += ",";
            json += Theme.AddList(param);
            json += "}";

            return json;
        }
        /// <summary>${IS6_ThemeUnique_method_FromJson_D}</summary>
        /// <returns>${IS6_ThemeUnique_method_FromJson_return}</returns>
        /// <param name="jsonObject">${IS6_ThemeUnique_method_FromJson_param_jsonObject}</param>
        public static ThemeUnique FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeUnique result = new ThemeUnique
            {
                Expression = (string)jsonObject["expression"],
                DefaultStyle = ServerStyle.FromJson((JsonObject)jsonObject["defaultStyle"]),
                OnTop = (bool)jsonObject["onTop"],

                Caption = (string)jsonObject["caption"],
                Filter = (string)jsonObject["filter"],
                MaxScale = (double)jsonObject["maxScale"],
                MinScale = (double)jsonObject["minScale"],
                ForeignDataParam = ForeignDataParam.FromJson((JsonObject)jsonObject["foreignDataParam"])
            };

            if (jsonObject.ContainsKey("itemCaptions") && jsonObject["itemCaptions"] != null && jsonObject["itemCaptions"].Count > 0)
            {
                result.ItemCaptions = new List<string>();
                for (int i = 0; i < jsonObject["itemCaptions"].Count; i++)
                {
                    result.ItemCaptions.Add((string)jsonObject["itemCaptions"][i]);
                }
            }
            if (jsonObject.ContainsKey("values") && jsonObject["values"] != null && jsonObject["values"].Count > 0)
            {
                result.Values = new List<string>();
                for (int i = 0; i < jsonObject["values"].Count; i++)
                {
                    result.Values.Add((string)jsonObject["values"][i]);
                }
            }
            if (jsonObject.ContainsKey("displays") && jsonObject["displays"] != null && jsonObject["displays"].Count > 0)
            {
                result.Displays = new List<ServerStyle>();
                for (int i = 0; i < jsonObject["displays"].Count; i++)
                {
                    result.Displays.Add(ServerStyle.FromJson((JsonObject)jsonObject["displays"][i]));
                }
            }


            return result;
        }
    }
}
