﻿
//====================
//--- ScottXu ---2010/01/19---
//====================

using System.Windows;
using System.Windows.Browser;
using System;
using System.Collections.Generic;
using System.Json;
using System.Text;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeService_Title}</para>
    /// 	<para>${IS6_ThemeService_Description}</para>
    /// </summary>
    public class ThemeService : ServiceBase
    {
        /// <summary>${IS6_ThemeService_constructor_None_D}</summary>
        /// <overloads>${IS6_ThemeService_constructor_overloads_D}</overloads>
        public ThemeService()
        { }
        /// <summary>${IS6_ThemeService_constructor_String_D}</summary>
        /// <param name="url">${IS6_ThemeService_constructor_String_param_url}</param>
        public ThemeService(string url)
            : base(url)
        {
            //base.Url = url + "/common.ashx?";
        }

        /// <summary>${IS6_ThemeService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_ThemeService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="parameters">${IS6_ThemeService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(ThemeParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_ThemeService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_ThemeService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_ThemeService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(ThemeParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("ThemeParameters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            base.SubmitRequest(base.Url + "/common.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(ThemeParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "UpdateLayers";
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);

            string json = "[";
            List<string> list = new List<string>();
            if (parameters.Theme != null)
            {
                for (int i = 0; i < parameters.LayerNames.Count; i++)
                {
                    if (i == parameters.LayerNames.IndexOf(parameters.ThemeLayer))
                    {
                        if (parameters.Theme is ThemeUnique)
                        {
                            StringBuilder sb = new StringBuilder("{");
                            sb.AppendFormat("\"themeUnique\":{0}", ThemeUnique.ToJson((ThemeUnique)parameters.Theme));
                            //sb.Append(",\"themeRange\": null,\"themeLabel\": null,\"themeGraph\": null,\"themeGridRange\": null,\"themeGraduatedSymbol\": null,\"themeDotDensity\": null}");
                            sb.Append("}");
                            list.Add(sb.ToString());
                        }
                        if (parameters.Theme is ThemeRange)
                        {
                            StringBuilder sb = new StringBuilder("{");
                            sb.AppendFormat("\"themeRange\":{0}", ThemeRange.ToJson((ThemeRange)parameters.Theme));
                            //sb.Append(",\"themeUnique\": null,\"themeLabel\": null,\"themeGraph\": null,\"themeGridRange\": null,\"themeGraduatedSymbol\": null,\"themeDotDensity\": null}");
                            sb.Append(",\"themeUnique\": null}");
                            list.Add(sb.ToString());
                        }
                        if (parameters.Theme is ThemeLabel)
                        {
                            StringBuilder sb = new StringBuilder("{");
                            sb.AppendFormat("\"themeLabel\":{0}", ThemeLabel.ToJson((ThemeLabel)parameters.Theme));
                            //sb.Append(",\"themeUnique\": null,\"themeRange\": null,\"themeGraph\": null,\"themeGridRange\": null,\"themeGraduatedSymbol\": null,\"themeDotDensity\": null}");
                            sb.Append("}");
                            list.Add(sb.ToString());
                        }
                        if (parameters.Theme is ThemeGraph)
                        {
                            StringBuilder sb = new StringBuilder("{");
                            sb.AppendFormat("\"themeGraph\":{0}", ThemeGraph.ToJson((ThemeGraph)parameters.Theme));
                            // sb.Append(",\"themeUnique\": null,\"themeRange\": null,\"themeLabel\": null,\"themeGridRange\": null,\"themeGraduatedSymbol\": null,\"themeDotDensity\": null}");
                            sb.Append("}");
                            list.Add(sb.ToString());
                        }
                        if (parameters.Theme is ThemeDotDensity)
                        {
                            StringBuilder sb = new StringBuilder("{");
                            sb.AppendFormat("\"themeDotDensity\":{0}", ThemeDotDensity.ToJson((ThemeDotDensity)parameters.Theme));
                            // sb.Append(",\"themeUnique\": null,\"themeRange\": null,\"themeLabel\": null,\"themeGridRange\": null,\"themeGraph\": null,\"themeGraduatedSymbol\": null}");
                            sb.Append("}");
                            list.Add(sb.ToString());
                        }
                        if (parameters.Theme is ThemeGraduatedSymbol)
                        {
                            StringBuilder sb = new StringBuilder("{");
                            sb.AppendFormat("\"themeGraduatedSymbol\":{0}", ThemeGraduatedSymbol.ToJson((ThemeGraduatedSymbol)parameters.Theme));
                            //sb.Append(",\"themeUnique\": null,\"themeRange\": null,\"themeLabel\": null,\"themeGridRange\": null,\"themeGraph\": null,\"themeDotDensity\": null}");
                            sb.Append("}");
                            list.Add(sb.ToString());
                        }
                        if (parameters.Theme is ThemeGridRange)
                        {
                            StringBuilder sb = new StringBuilder("{");
                            sb.AppendFormat("\"themeGridRange\":{0}", ThemeGridRange.ToJson((ThemeGridRange)parameters.Theme));
                            //sb.Append(",\"themeUnique\": null,\"themeRange\": null,\"themeLabel\": null,\"themeGraduatedSymbol\": null,\"themeGraph\": null,\"themeDotDensity\": null}");
                            sb.Append("}");
                            list.Add(sb.ToString());
                        }
                    }
                    else
                    {
                        list.Add(string.Format("\"{0}\"", "!@"));
                    }
                }
            }
            else
            {
                for (int i = 0; i < parameters.LayerNames.Count; i++)
                {
                    if (i == parameters.LayerNames.IndexOf(parameters.ThemeLayer))
                    {
                        parameters.Theme = new ThemeUnique();
                        StringBuilder sb = new StringBuilder("{");
                        sb.AppendFormat("\"themeGraduatedSymbol\":{0}", ThemeUnique.ToJson((ThemeUnique)parameters.Theme));
                        sb.Append("}");
                        list.Add(sb.ToString());

                        //list.Add("{\"themeGridRange\":null\",\"themeUnique\": null,\"themeRange\": null,\"themeLabel\": null,\"themeGraduatedSymbol\": null,\"themeGraph\": null,\"themeDotDensity\": null}");
                    }
                    else
                    {
                        list.Add(string.Format("\"{0}\"", "!@"));
                    }
                }
            }

            json += string.Join(",", list.ToArray());
            json += "]";

            dictionary.Add("layers", json);

            if (string.IsNullOrEmpty(parameters.LayersKey))
            {
                parameters.LayersKey = "0";
            }

            dictionary.Add("layersKey", parameters.LayersKey);
            dictionary.Add("bModifiedByServer", "false");

            return dictionary;
        }


        private ThemeResult lastResult;

        /// <summary>${IS6_ThemeService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ThemeEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonPrimitive jsonObject = (JsonPrimitive)JsonObject.Parse(e.Result);

            ThemeResult result = ThemeResult.FromJson(jsonObject);
            LastResult = result;
            ThemeEventArgs args = new ThemeEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(ThemeEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_ThemeService_attribute_lastResult_D}</summary>
        public ThemeResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}