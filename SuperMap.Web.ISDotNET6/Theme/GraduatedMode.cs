﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_GraduatedMode_Title}</para>
    /// <para>${IS6_GraduatedMode_Description}</para>
    /// </summary>
    public enum GraduatedMode
    {
        /// <summary>${IS6_GraduatedMode_attribute_Constant_D}</summary>
        Constant = 0,
        /// <summary>${IS6_GraduatedMode_attribute_Squareroot_D}</summary>
        SquareRoot = 1,
        /// <summary>${IS6_GraduatedMode_attribute_Log_D}</summary>
        Log = 2
    }
}
