﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GraphType_Title}</para>
    /// 	<para>${IS6_GraphType_Description}</para>
    /// </summary>
    public enum GraphType
    {
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Area_D}</para>
        /// 	<para><img src="GraphThemeArea.bmp"/></para>
        /// </summary>
        Area = 0,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Step_D}</para>
        /// 	<para><img src="GraphThemeStep.bmp"/></para>
        /// </summary>
        Step = 1,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Line_D}</para>
        /// 	<para><img src="GraphThemeLine.bmp"/></para>
        /// </summary>
        Line = 2,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Point_D}</para>
        /// 	<para><img src="GraphThemePoint.bmp"/></para>
        /// </summary>
        Point = 3,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Bar_D}</para>
        /// 	<para><img src="GraphThemeBar.bmp"/></para>
        /// </summary>
        Bar = 4,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Bar3D_D}</para>
        /// 	<para><img src="GraphThemeBar3D.bmp"/></para>
        /// </summary>
        Bar3D = 5,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Pie_D}</para>
        /// 	<para><img src="GraphThemePie.bmp"/></para>
        /// </summary>
        Pie = 6,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Pie3D_D}</para>
        /// 	<para><img src="GraphThemePie3D.bmp"/></para>
        /// </summary>
        Pie3D = 7,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Rose_D}</para>
        /// 	<para><img src="GraphThemeRose.bmp"/></para>
        /// </summary>
        Rose = 8,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Rose3D_D}</para>
        /// 	<para><img src="GraphThemeRose3D.bmp"/></para>
        /// </summary>
        Rose3D = 9,
        ///// <summary>${IS6_GraphType_attribute_PyramidBar_D}</summary>
        //PyramidBar = 10,
        ///// <summary>${IS6_GraphType_attribute_PyramidPolygon_D}</summary>
        //PyramidPolygon = 11,       
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_StrackBar_D}</para>
        /// 	<para><img src="GraphThemeStackBar.bmp"/></para>
        /// </summary>
        StackedBar = 12,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_StackBar3D_D}</para>
        /// 	<para><img src="GraphThemeStackBar3D.bmp"/></para>
        /// </summary>
        StackedBar3D = 13,
        /// <summary>
        /// 	<para>${IS6_GraphType_attribute_Ring_D}</para>
        /// 	<para><img src="GraphThemeRing.bmp"/></para>
        /// </summary>
        Doughnut = 14
    }
}
