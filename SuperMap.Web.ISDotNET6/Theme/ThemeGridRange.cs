﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeGridRange_Title}</para>
    /// 	<para>${IS6_ThemeGridRange_Description}</para>
    /// </summary>
    public class ThemeGridRange : Theme
    {
        /// <summary>${IS6_ThemeGridRange_constructor_None_D}</summary>
        public ThemeGridRange()
        { }
        /// <summary>${IS6_ThemeGridRange_attribute_breakValues_D}</summary>
        public IList<double> BreakValues { get; set; }
        /// <summary>${IS6_ThemeGridRange_attribute_displays_D}</summary>
        public IList<ServerColor> Displays { get; set; }

        internal static string ToJson(ThemeGridRange param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (param.BreakValues != null && param.BreakValues.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < param.BreakValues.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", param.BreakValues[i]));
                }
                list.Add(string.Format("\"breakValues\":[{0}]", string.Join(",", temp.ToArray())));
            }

            if (!string.IsNullOrEmpty(param.Caption))
            {
                list.Add(string.Format("\"caption\":\"{0}\"", param.Caption));
            }

            if (param.Displays != null && param.Displays.Count > 0)
            {
                List<string> l = new List<string>();
                for (int i = 0; i < param.Displays.Count; i++)
                {
                    l.Add(string.Format("{0}", ServerColor.ToJson(param.Displays[i]).ToString()));
                }
                list.Add(string.Format("\"displays\":[{0}]", string.Join(",", l.ToArray())));
            }

            list.Add("\"enabled\":true");
            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }

        internal static ThemeGridRange FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeGridRange result = new ThemeGridRange();

            if (jsonObject.ContainsKey("breakValues") && jsonObject["breakValues"] != null && jsonObject["breakValues"].Count > 0)
            {
                result.BreakValues = new List<double>();
                for (int i = 0; i < jsonObject["breakValues"].Count; i++)
                {
                    result.BreakValues.Add(jsonObject["breakValues"][i]);
                }

            }
            if (jsonObject.ContainsKey("displays") && jsonObject["displays"] != null && jsonObject["displays"].Count > 0)
            {
                result.Displays = new List<ServerColor>();
                for (int i = 0; i < jsonObject["displays"].Count; i++)
                {
                    result.Displays.Add(ServerColor.FromJson(jsonObject["displays"][i]));
                }

            }
            result.Caption = (string)jsonObject["caption"];
            return result;
        }
    }
}
