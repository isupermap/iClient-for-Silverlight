﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_TextBackShape_Title}</summary>
    public enum TextBackShape
    {
        /// <summary>${IS6_TextBackShape_attribute_none_D}</summary>
        None = 0,
        /// <summary>${IS6_TextBackShape_attribute_rect_D}</summary>
        Rect = 1,
        /// <summary>${IS6_TextBackShape_attribute_roundRect_D}</summary>
        RoundRect = 2,
        /// <summary>${IS6_TextBackShape_attribute_ellipse_D}</summary>
        Ellipse = 3,
        /// <summary>${IS6_TextBackShape_attribute_diamond_D}</summary>
        Diamond = 4,
        /// <summary>${IS6_TextBackShape_attribute_triangle_D}</summary>
        Triangle = 5,
        /// <summary>${IS6_TextBackShape_attribute_circle_D}</summary>
        Circle = 6,
        /// <summary>${IS6_TextBackShape_attribute_marker_D}</summary>
        Symbol = 100
    }
}
