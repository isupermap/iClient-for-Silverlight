﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeEventArgs_Title}</para>
    /// 	<para>${IS6_ThemeEventArgs_Description}</para>
    /// </summary>
    public class ThemeEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_ThemeEventArgs_constructor_D}</summary>
        public ThemeEventArgs(ThemeResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_ThemeEventArgs_attribute_Result_D}</summary>
        public ThemeResult Result { get; private set; }
        /// <summary>${IS6_ThemeEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }

    }
}
