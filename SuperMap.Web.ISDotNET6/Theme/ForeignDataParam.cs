﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_ForeignDataParam_Title}</para>
    /// <para>${IS6_ForeignDataParam_Description}</para>
    /// </summary>
    public class ForeignDataParam
    {
        /// <summary>${IS6_ForeignDataParam_constructor_None_D}</summary>
        public ForeignDataParam()
        { }

        /// <summary>${IS6_ForeignDataParam_attribute_foreignJoinExpression_D}</summary>
        public string ForeignJoinExpression { get; set; }

        /// <summary>${IS6_ForeignDataParam_attribute_foreignKeys_D}</summary>
        public IList<string> ForeignKeys { get; set; }

        /// <summary>${IS6_ForeignDataParam_attribute_foreignValues_D}</summary>
        public IList<string> ForeignValues { get; set; }

        /// <summary>${IS6_ForeignDataParam_attribute_useForeignValue_D}</summary>
        public bool UseForeignValue { get; set; }

        internal static string ToJson(ForeignDataParam param)
        {
            if (param == null)
            {
                return null;
            }

            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(param.ForeignJoinExpression))
            {
                list.Add(string.Format("\"foreignJoinExpression\":\"{0}\"", param.ForeignJoinExpression));
            }
            else
            {
                list.Add("\"foreignJoinExpression\":\"\"");
            }

            if (param.ForeignKeys == null)
            {
                list.Add("\"foreignKeys\":[]");
            }
            else
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < param.ForeignKeys.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", param.ForeignKeys[i]));
                }
                list.Add(string.Format("\"foreignKeys\":[{0}]", string.Join(",", temp.ToArray())));
            }

            if (param.ForeignValues == null)
            {
                list.Add("\"foreignValues\":[]");
            }
            else
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < param.ForeignValues.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", param.ForeignValues[i]));
                }
                list.Add(string.Format("\"foreignValues\":[{0}]", string.Join(",", temp.ToArray())));

            }

            list.Add(string.Format("\"useForeignValue\":{0}", param.UseForeignValue.ToString().ToLower()));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }

        /// <summary>${IS6_ForeignDataParam_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_ForeignDataParam_method_FromJson_D}</param>
        /// <returns>${IS6_ForeignDataParam_method_FromJson_return}</returns>
        public static ForeignDataParam FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ForeignDataParam result = new ForeignDataParam();

            result.ForeignJoinExpression = (string)jsonObject["foreignJoinExpression"];
            result.UseForeignValue = (bool)jsonObject["useForeignValue"];

            if (jsonObject.ContainsKey("foreignKeys") && jsonObject["foreignKeys"] != null && jsonObject["foreignKeys"].Count > 0)
            {
                result.ForeignKeys = new List<string>();
                for (int i = 0; i < jsonObject["foreignKeys"].Count; i++)
                {
                    result.ForeignKeys.Add((string)jsonObject["foreignKeys"][i]);
                }
            }

            if (jsonObject.ContainsKey("foreignValues") && jsonObject["foreignValues"] != null && jsonObject["foreignValues"].Count > 0)
            {
                result.ForeignValues = new List<string>();
                for (int i = 0; i < jsonObject["foreignValues"].Count; i++)
                {
                    result.ForeignValues.Add((string)jsonObject["foreignValues"][i]);
                }
            }

            return result;
        }
    }
}
