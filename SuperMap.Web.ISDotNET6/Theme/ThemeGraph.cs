﻿
//===================
//  ScottXu ---2010/01/20---
//===================

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeGraph_Title}</para>
    /// 	<para>${IS6_ThemeGraph_Description}</para>
    /// 	<para>
    /// 		<list type="table">
    /// 			<item>
    /// 				<term><img src="GraphTheme1.bmp"/></term>
    /// 				<description><img src="GraphTheme2.bmp"/></description>
    /// 			</item>
    /// 		</list>
    /// 	</para>
    /// 	<para>${IS6_ThemeGraph_Description_1}</para>
    /// </summary>
    public class ThemeGraph : Theme
    {
        /// <summary>${IS6_ThemeGraph_constructor_None_D}</summary>
        public ThemeGraph()
        {
            AxesTextStyle = new ServerTextStyle();
        }
        /// <summary>${IS6_ThemeGraph_attribute_axesTextStyle_D}</summary>
        public ServerTextStyle AxesTextStyle { get; set; }

        public double BarWidth { get; set; } //返回/设置 柱状专题图中每一个柱的宽度。单位为地图单位。 
        /// <summary>${IS6_ThemeGraph_attribute_enableFlow_D}</summary>
        public bool EnableFlow { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_expression_D}</summary>
        public IList<string> Expressions { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_graduatedMode_D}</summary>
        public GraduatedMode GraduatedMode { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_graphStyles_D}</summary>
        public IList<ServerStyle> GraphStyles { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_graphType_D}</summary>
        public GraphType GraphType { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_itemTextFormat_D}</summary>
        public GraphTextFormat ItemTextFormat { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_itemTextStyle_D}</summary>
        public ServerTextStyle ItemTextStyle { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_lineColor_D}</summary>
        public ServerColor LineColor { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_maxSumSize_D}</summary>
        public double MaxSumSize { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_minSumSize_D}</summary>
        public double MinSumSize { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_showAxes_D}</summary>
        public bool ShowAxes { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_showAxesText_D}</summary>
        public bool ShowAxesText { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_showAxisGrid_D}</summary>
        public bool ShowAxisGrid { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_showItemText_D}</summary>
        public bool ShowItemText { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_onTop_D}</summary>
        public bool OnTop { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_itemCaptions_D}</summary>
        public IList<string> ItemCaptions { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_offsetXExpression_D}</summary>
        public string OffsetXExpression { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_offsetYExpression_D}</summary>
        public string OffsetYExpression { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_offsetFixed_D}</summary>
        public bool OffsetFixed { get; set; }
        /// <summary>${IS6_ThemeGraph_attribute_showXAxisText_D}</summary>
        public bool ShowXAxisText { get; set; }

        internal static string ToJson(ThemeGraph param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            if (param.AxesTextStyle != null)
            {
                list.Add(string.Format("\"axesTextStyle\":{0}", ServerTextStyle.ToJson(param.AxesTextStyle)));
            }
            else
            {
                list.Add("\"axesTextStyle\":null");
            }
            list.Add(string.Format("\"barWidth\":{0}", param.BarWidth));
            list.Add(string.Format("\"enableFlow\":{0}", param.EnableFlow.ToString().ToLower()));
            if (param.Expressions != null)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < param.Expressions.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", param.Expressions[i]));
                }
                list.Add(string.Format("\"expressions\":[{0}]", string.Join(",", temp.ToArray())));

            }
            else
            {
                list.Add("\"expressions\":null");
            }
            list.Add(string.Format("\"graduatedMode\":{0}", (int)param.GraduatedMode));
            if (param.GraphStyles != null && param.GraphStyles.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < param.GraphStyles.Count; i++)
                {
                    temp.Add(ServerStyle.ToJson(param.GraphStyles[i]));
                }
                list.Add(string.Format("\"graphStyles\":[{0}]", string.Join(",", temp.ToArray())));
            }
            else
            {
                list.Add("\"graphStyles\":null");
            }
            list.Add(string.Format("\"graphType\":{0}", (int)param.GraphType));
            list.Add(string.Format("\"itemTextFormat\":{0}", (int)param.ItemTextFormat));
            if (param.ItemTextStyle != null)
            {
                list.Add(string.Format("\"itemTextStyle\":{0}", ServerTextStyle.ToJson(param.ItemTextStyle)));
            }
            else
            {
                list.Add("\"itemTextStyle\":null");
            }

            list.Add(string.Format("\"lineColor\":{0}", ServerColor.ToJson(param.LineColor)));

            list.Add(string.Format("\"maxSumSize\":{0}", param.MaxSumSize));
            list.Add(string.Format("\"minSumSize\":{0}", param.MinSumSize));
            list.Add(string.Format("\"showAxes\":{0}", param.ShowAxes.ToString().ToLower()));
            list.Add(string.Format("\"showAxesText\":{0}", param.ShowAxesText.ToString().ToLower()));
            list.Add(string.Format("\"showAxisGrid\":{0}", param.ShowAxisGrid.ToString().ToLower()));
            list.Add(string.Format("\"showItemText\":{0}", param.ShowItemText.ToString().ToLower()));
            list.Add(string.Format("\"onTop\":{0}", param.OnTop.ToString().ToLower()));

            if (param.ItemCaptions != null)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < param.ItemCaptions.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", param.ItemCaptions[i]));
                }
                list.Add(string.Format("\"itemCaptions\":[{0}]", string.Join(",", temp.ToArray())));
            }
            else
            {
                list.Add("\"itemCaptions\":null");
            }

            if (!string.IsNullOrEmpty(param.OffsetXExpression))
            {
                list.Add(string.Format("\"offsetXExpression\":\"{0}\"", param.OffsetXExpression));
            }
            else
            {
                list.Add("\"offsetXExpression\":\"\"");
            }

            if (!string.IsNullOrEmpty(param.OffsetYExpression))
            {
                list.Add(string.Format("\"offsetYExpression\":\"{0}\"", param.OffsetYExpression));
            }
            else
            {
                list.Add("\"offsetYExpression\":\"\"");
            }

            list.Add(string.Format("\"offsetFixed\":{0}", param.OffsetFixed.ToString().ToLower()));
            list.Add(string.Format("\"showXAxisText\":{0}", param.ShowXAxisText.ToString().ToLower()));

            json += string.Join(",", list.ToArray());
            json += ",";
            json += Theme.AddList(param);
            json += "}";

            return json;
        }
        /// <summary>${IS6_ThemeGraph_method_FromJson_D}</summary>
        /// <returns>${IS6_ThemeGraph_method_FromJson_return}</returns>
        /// <param name="jsonObject">${IS6_ThemeGraph_method_FromJson_param_jsonObject}</param>
        public static ThemeGraph Fromjson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeGraph result = new ThemeGraph
            {
                Caption = (string)jsonObject["caption"],
                Filter = (string)jsonObject["filter"],
                MaxScale = (double)jsonObject["maxScale"],
                MinScale = (double)jsonObject["minScale"],
                ForeignDataParam = ForeignDataParam.FromJson((JsonObject)jsonObject["foreignDataParam"]),

                AxesTextStyle = ServerTextStyle.FromJson((JsonObject)jsonObject["axesTextStyle"]),
                EnableFlow = (bool)jsonObject["enableFlow"],
                GraduatedMode = (GraduatedMode)(int)jsonObject["graduatedMode"],
                GraphType = (GraphType)(int)jsonObject["graphType"],
                ItemTextFormat = (GraphTextFormat)(int)jsonObject["itemTextFormat"],
                ItemTextStyle = ServerTextStyle.FromJson((JsonObject)jsonObject["itemTextStyle"]),
                LineColor = ServerColor.FromJson((int)jsonObject["lineColor"]),
                MaxSumSize = (double)jsonObject["maxSumSize"],
                MinSumSize = (double)jsonObject["minSumSize"],
                OffsetFixed = (bool)jsonObject["offsetFixed"],
                OffsetXExpression = (string)jsonObject["offsetXExpression"],
                OffsetYExpression = (string)jsonObject["offsetYExpression"],
                OnTop = (bool)jsonObject["onTop"],
                ShowAxes = (bool)jsonObject["showAxes"],
                ShowAxesText = (bool)jsonObject["showAxesText"],
                ShowAxisGrid = (bool)jsonObject["showAxisGrid"],
                ShowItemText = (bool)jsonObject["showItemText"],
                ShowXAxisText = (bool)jsonObject["showXAxisText"],
            };

            if (jsonObject.ContainsKey("itemCaptions") && jsonObject["itemCaptions"] != null && jsonObject["itemCaptions"].Count > 0)
            {
                result.ItemCaptions = new List<string>();
                for (int i = 0; i < jsonObject["itemCaptions"].Count; i++)
                {
                    result.ItemCaptions.Add((string)jsonObject["itemCaptions"][i]);
                }
            }
            if (jsonObject.ContainsKey("expressions") && jsonObject["expressions"] != null && jsonObject["expressions"].Count > 0)
            {
                result.Expressions = new List<string>();
                for (int i = 0; i < jsonObject["expressions"].Count; i++)
                {
                    result.Expressions.Add((string)jsonObject["expressions"][i]);
                }
            }

            if (jsonObject.ContainsKey("graphStyles") && jsonObject["graphStyles"] != null && jsonObject["graphStyles"].Count > 0)
            {
                result.GraphStyles = new List<ServerStyle>();
                for (int i = 0; i < jsonObject["graphStyles"].Count; i++)
                {
                    result.GraphStyles.Add(ServerStyle.FromJson((JsonObject)jsonObject["graphStyles"][i]));
                }
            }

            return result;
        }
    }
}
