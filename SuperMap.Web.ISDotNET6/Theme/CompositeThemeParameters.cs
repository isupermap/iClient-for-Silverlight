﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_CompositeThemeParameters_Title}</para>
    /// <para>${IS6_CompositeThemeParameters_Description}</para>
    /// </summary>
    public class CompositeThemeParameters : ParametersBase
    {
        /// <summary>${IS6_CompositeThemeParameters_Constructor_None_D}</summary>
        public CompositeThemeParameters()
        {
        }
        /// <summary>${IS6_CompositeThemeParameters_attribute_layerNames_D}</summary>
        public IList<string> LayerNames { get; set; }
        /// <summary>
        /// <para>${IS6_CompositeThemeParameters_attribute_paramList_D}</para>
        /// <para><img src="ISThemeConflict.png"/></para>
        /// </summary>
        public IList<CompositeParam> ParamList { get; set; }
    }
}
