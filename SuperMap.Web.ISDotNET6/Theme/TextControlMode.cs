﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_TextControlMode_Title}</summary>
    public enum TextControlMode
    {
        /// <summary>${IS6_TextControlMode_attribute_none_D}</summary>
        None = 0,
        /// <summary>${IS6_TextControlMode_attribute_omit_D}</summary>
        Omit = 1,
        /// <summary>${IS6_TextControlMode_attribute_newLine_D}</summary>
        WrapText = 2
    }
}
