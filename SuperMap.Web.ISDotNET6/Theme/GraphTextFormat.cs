﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GraphTextFormat_Title}</para>
    /// 	<para>${IS6_GraphTextFormat_Description}</para>
    /// </summary>
    public enum GraphTextFormat
    {
        /// <summary>${IS6_GraphTextFormat_attribute_Undefined_D}</summary>
        Undefined = 0,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphTextFormat_attribute_Percent_D}</para>
        /// 	<para><img src="GraphThemePercent.bmp"/></para>
        /// </summary>
        TextPercent = 1,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphTextFormat_attribute_Value_D}</para>
        /// 	<para><img src="GraphThemeValue.bmp"/></para>
        /// </summary>
        TextValue = 2,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphTextFormat_attribute_Caption_D}</para>
        /// 	<para><img src="GraphThemeCaption.bmp"/></para>
        /// </summary>
        TextCaption = 3,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphTextFormat_attribute_CaptionPercent_D}</para>
        /// 	<para><img src="GraphThemeCaption_Percent.bmp"/></para>
        /// </summary>
        TextCaptionPercent = 4,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphTextFormat_attribute_CaptionValue_D}</para>
        /// 	<para><img src="GraphThemeCaption_Value.bmp"/></para>
        /// </summary>
        TextCaptionValue = 5
    }
}
