﻿
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeResult_Title}</para>
    /// 	<para>${IS6_ThemeResult_Description}</para>
    /// </summary>
    public class ThemeResult
    {
        internal ThemeResult()
        { }
        /// <summary>${IS6_ThemeResult_attribute_layerKey_D}</summary>
        public string LayerKey { get; private set; }
        /// <summary>${IS6_ThemeResult_method_FromJson_D}</summary>
        /// <returns>${IS6_ThemeResult_method_FromJson_return}</returns>
        /// <param name="jsonPrimitive">${IS6_ThemeResult_method_FromJson_param_jsonObject}</param>
        public static ThemeResult FromJson(JsonPrimitive jsonPrimitive)
        {
            if (jsonPrimitive == null)
            {
                return null;
            }

            ThemeResult result = new ThemeResult();

            result.LayerKey = jsonPrimitive;

            return result;
        }
    }

}
