﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeGraduatedSymbol_Title}</para>
    /// 	<para>${IS6_ThemeGraduatedSymbol_Description}</para>
    /// 	<para><img src="ThemeGraduatedSymbol.png"/></para>
    /// </summary>
    public class ThemeGraduatedSymbol : Theme
    {

        /// <summary>${IS6_ThemeGraduatedSymbol_constructor_None_D}</summary>
        public ThemeGraduatedSymbol()
        {
        }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_definitionValue_D}</summary>
        public double DefinitionValue { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_enableFlow_D}</summary>
        public bool EnableFlow { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_expression_D}</summary>
        public string Expression { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_graduatedMode_D}</summary>
        public GraduatedMode GraduatedMode { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_showNegative_D}</summary>
        public bool ShowNegative { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_styleForNegative_D}</summary>
        public ServerStyle StyleForNegative { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_styleForPositive_D}</summary>
        public ServerStyle StyleForPositive { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_onTop_D}</summary>
        public bool OnTop { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_offsetXExpression_D}</summary>
        public string OffsetXExpression { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_offsetYExpression_D}</summary>
        public string OffsetYExpression { get; set; }
        /// <summary>${IS6_ThemeGraduatedSymbol_attribute_offsetFixed_D}</summary>
        public bool OffsetFixed { get; set; }

        internal static string ToJson(ThemeGraduatedSymbol param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            list.Add(string.Format("\"definitionValue\":{0}", param.DefinitionValue));
            list.Add(string.Format("\"enableFlow\":{0}", param.EnableFlow.ToString().ToLower()));

            if (!string.IsNullOrEmpty(param.Expression))
            {
                list.Add(string.Format("\"expression\":\"{0}\"", param.Expression));
            }
            list.Add(string.Format("\"graduatedMode\":{0}", (int)param.GraduatedMode));
            list.Add(string.Format("\"showNegative\":{0}", param.ShowNegative.ToString().ToLower()));

            if (param.StyleForNegative != null)
            {
                list.Add(string.Format("\"styleForNegative\":{0}", ServerStyle.ToJson(param.StyleForNegative)));
            }

            if (param.StyleForPositive != null)
            {
                list.Add(string.Format("\"styleForPositive\":{0}", ServerStyle.ToJson(param.StyleForPositive)));
            }
            list.Add(string.Format("\"onTop\":{0}", param.OnTop.ToString().ToLower()));

            if (!string.IsNullOrEmpty(param.OffsetXExpression))
            {
                list.Add(string.Format("\"offsetXExpression\":\"{0}\"", param.OffsetXExpression));
            }
            else
            {
                list.Add("\"offsetXExpression\":\"\"");
            }

            if (!string.IsNullOrEmpty(param.OffsetYExpression))
            {
                list.Add(string.Format("\"offsetYExpression\":\"{0}\"", param.OffsetYExpression));
            }
            else
            {
                list.Add("\"offsetYExpression\":\"\"");
            }
            list.Add(string.Format("\"offsetFixed\":{0}", param.OffsetFixed.ToString().ToLower()));

            json += string.Join(",", list.ToArray());
            json += ",";
            json += Theme.AddList(param);
            json += "}";

            return json;
        }
        /// <summary>${IS6_ThemeGraduatedSymbol_method_FromJson_D}</summary>
        /// <returns>${IS6_ThemeGraduatedSymbol_method_FromJson_return}</returns>
        /// <param name="jsonObject">${IS6_ThemeGraduatedSymbol_method_FromJson_param_jsonObject}</param>
        public static ThemeGraduatedSymbol FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            return new ThemeGraduatedSymbol
            {
                Caption = (string)jsonObject["caption"],
                Filter = (string)jsonObject["filter"],
                MaxScale = (double)jsonObject["maxScale"],
                MinScale = (double)jsonObject["minScale"],
                ForeignDataParam = ForeignDataParam.FromJson((JsonObject)jsonObject["foreignDataParam"]),

                DefinitionValue = (double)jsonObject["definitionValue"],
                EnableFlow = (bool)jsonObject["enableFlow"],
                Expression = (string)jsonObject["expression"],
                GraduatedMode = (GraduatedMode)(int)jsonObject["graduatedMode"],
                OffsetFixed = (bool)jsonObject["offsetFixed"],
                OffsetXExpression = (string)jsonObject["offsetXExpression"],
                OffsetYExpression = (string)jsonObject["offsetYExpression"],
                OnTop = (bool)jsonObject["onTop"],
                ShowNegative = (bool)jsonObject["showNegative"],
                StyleForNegative = ServerStyle.FromJson((JsonObject)jsonObject["styleForNegative"]),
                StyleForPositive = ServerStyle.FromJson((JsonObject)jsonObject["styleForPositive"]),
            };
        }
    }
}
