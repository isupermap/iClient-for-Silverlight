﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ThemeParameters_Title}</para>
    /// 	<para>${IS6_ThemeParameters_Description}</para>
    /// 	<para>
    /// 		<img src="ISThemeLayer.png"/>
    /// 	</para>
    /// </summary>
    public class ThemeParameters : ParametersBase
    {
        /// <summary>${IS6_ThemeParameters_constructor_None_D}</summary>
        public ThemeParameters()
        { }
        /// <summary>${IS6_ThemeParameters_attribute_theme_D}</summary>
        public Theme Theme { get; set; }

        /// <summary>${IS6_ThemeParameters_attribute_layerNames_D}</summary>
        public IList<string> LayerNames { get; set; }

        /// <summary>${IS6_ThemeParameters_attribute_themeLayer_D}</summary>
        public string ThemeLayer { get; set; }
        /// <summary>${IS6_ThemeParameters_attribute_layersKey_D}</summary>
        public string LayersKey { get; set; }

    }
}
