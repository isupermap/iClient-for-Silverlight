﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_BrushGradientMode_Title}</para>
    /// <para>${IS6_BrushGradientMode_Description}</para>
    /// </summary>
    public enum BrushGradientMode
    {
        /// <summary>${IS6_BrushGradientMode_attribute_None_D}</summary>
        None=0,
        /// <summary>
        /// 	<para>${IS6_BrushGradientMode_attribute_Linear_D}</para>
        /// 	<para><img src="fillGradientModeLinear.bmp"/></para>
        /// </summary>
        Linear=1,
        /// <summary>
        /// 	<para>${IS6_BrushGradientMode_attribute_Radial_D}</para>
        /// 	<para><img src="fillGradientModeRadial.bmp"/></para>
        /// </summary>
        Radial=2,
        /// <summary>
        /// 	<para>${IS6_BrushGradientMode_attribute_Conical_D}</para>
        /// 	<para><img src="fillGradientModeConical.bmp"/></para>
        /// </summary>
        Conical=3,

        /// <summary>${IS6_BrushGradientMode_attribute_Square_D}</summary>
        Square=4
    }
}
