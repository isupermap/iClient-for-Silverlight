﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_NetworkParam_Title}</para>
    /// 	<para>${IS6_NetworkParam_Description}</para>
    /// </summary>
    public class NetworkParams
    {
        /// <summary>${IS6_NetworkParam_constructor_None_D}</summary>
        public NetworkParams()
        {
            NetworkSetting = new NetworkSetting();
        }
        /// <summary>${IS6_NetworkParam_attribute_networkSetting_D}</summary>
        public NetworkSetting NetworkSetting { get; set; }
        /// <summary>
        /// <para>${IS6_NetworkParam_attribute_tolerance_D}</para>
        /// <para><img src="tolerance.png"/></para>
        /// </summary>
        public double Tolerance { get; set; }
        /// <summary>${IS6_NetworkParam_attribute_turnTableSetting_D}</summary>
        public TurnTableSetting TurnTableSetting { get; set; }
        /// <summary>${IS6_NetworkParam_attribute_locationAllocateSetting_D}</summary>
        public LocationAllocateSetting LocationAllocateSetting { get; set; }

        internal static string ToJson(NetworkParams param)
        {
            if (param == null)
            {
                return "{}";
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"networkSetting\":{0}", NetworkSetting.ToJson(param.NetworkSetting)));
            list.Add(string.Format("\"tolerance\":{0}", param.Tolerance));

            if (param.TurnTableSetting != null)
            {
                list.Add(string.Format("\"turnTableSetting\":{0}", TurnTableSetting.ToJson(param.TurnTableSetting)));
            }
            else
            {
                list.Add("\"turnTableSetting\":null");
            }

            if (param.LocationAllocateSetting != null)
            {
                list.Add(string.Format("\"locationAllocateSetting\":{0}", LocationAllocateSetting.ToJson(param.LocationAllocateSetting)));
            }
            else
            {
                list.Add("\"locationAllocateSetting\":null");
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
