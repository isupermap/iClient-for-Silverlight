﻿
using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_ProximityResult_Title}</para>
    /// <para>${IS6_ProximityResult_Description}</para>
    /// </summary>
    public class ProximityResult
    {
        internal ProximityResult()
        { }
        /// <summary>${IS6_ProximityResult_attribute_returnRecordSet_D}</summary>
        public RecordSet ReturnRecordSet { get; private set; }
        //public int TrackingLayerIndex { get; private set; }
        //public string UserID { get; private set; }

        /// <summary>${IS6_ProximityResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_ProximityResult_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_ProximityResult_method_FromJson_return}</returns>
        public static ProximityResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ProximityResult result = new ProximityResult();
            result.ReturnRecordSet = RecordSet.FromJson((JsonObject)jsonObject["returnRecordset"]);
            //result.TrackingLayerIndex = (int)jsonObject["trackingLayerIndex"];
            //result.UserID = (string)jsonObject["userID"];
            return result;
        }
    }
}
