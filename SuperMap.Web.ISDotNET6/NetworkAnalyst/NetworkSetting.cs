﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_NetworkSetting_Title}</para>
    /// <para>${IS6_NetworkSetting_Description}</para>
    /// </summary>
    public class NetworkSetting
    {
        /// <summary>${IS6_NetworkSetting_constructor_None_D}</summary>
        public NetworkSetting()
        {
            FromNodeIDField = "SmFNode";
            FromToOneWayValue = 1;
            ProhibitValue = 0;
            ToFromOneWayValue = 2;
            ToNodeIDField = "SmTNode";
            TwoWayValue = 3;
        }
        /// <summary>${IS6_NetworkSetting_attribute_barrierEdges_D}</summary>
        public IList<int> BarrierEdges { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_barrierNodes_D}</summary>
        public IList<int> BarrierNodes { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_edgeFilter_D}</summary>
        public string EdgeFilter { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_edgeIDField_D}</summary>
        public string EdgeIDField { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_fromNodeIDField_D}</summary>
        public string FromNodeIDField { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_fromToOneWayValue_D}</summary>
        public int FromToOneWayValue { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_ftWeightField_D}</summary>
        public string FTWeightField { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_networkLayerName_D}</summary>
        public string NetworkLayerName { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_nodeIDField_D}</summary>
        public string NodeIDField { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_prohibitValue_D}</summary>
        public int ProhibitValue { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_roadRuleField_D}</summary>
        public string RoadRuleField { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_tfWeightField_D}</summary>
        public string TFWeightField { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_toFromOneWayValue_D}</summary>
        public int ToFromOneWayValue { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_toNodeIDField_D}</summary>
        public string ToNodeIDField { get; set; }
        /// <summary>${IS6_NetworkSetting_attribute_toWayValue_D}</summary>
        public int TwoWayValue { get; set; }


        internal static string ToJson(NetworkSetting networkSetting)
        {
            if (networkSetting == null)
            {
                return null;
            }

            string json = "{";

            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(networkSetting.NetworkLayerName))
            {
                list.Add(string.Format("\"networkLayerName\":\"{0}\"", networkSetting.NetworkLayerName));
            }
            else
            {
                list.Add("\"networkLayerName\":\"\"");
            }

            if (!string.IsNullOrEmpty(networkSetting.NodeIDField))
            {
                list.Add(string.Format("\"nodeIDField\":\"{0}\"", networkSetting.NodeIDField));
            }
            else
            {
                list.Add("\"nodeIDField\":\"\"");
            }

            if (!string.IsNullOrEmpty(networkSetting.EdgeIDField))
            {
                list.Add(string.Format("\"edgeIDField\":\"{0}\"", networkSetting.EdgeIDField));
            }
            else
            {
                list.Add("\"edgeIDField\":\"\"");
            }

            if (!string.IsNullOrEmpty(networkSetting.EdgeFilter))
            {
                list.Add(string.Format("\"edgeFilter\":\"{0}\"", networkSetting.EdgeFilter));
            }
            else
            {
                list.Add("\"edgeFilter\":\"\"");
            }

            if (!string.IsNullOrEmpty(networkSetting.FTWeightField))
            {
                list.Add(string.Format("\"FTWeightField\":\"{0}\"", networkSetting.FTWeightField));
            }
            else
            {
                list.Add("\"FTWeightField\":\"\"");
            }

            if (!string.IsNullOrEmpty(networkSetting.TFWeightField))
            {
                list.Add(string.Format("\"TFWeightField\":\"{0}\"", networkSetting.TFWeightField));
            }
            else
            {
                list.Add("\"TFWeightField\":\"\"");
            }


            if (networkSetting.BarrierEdges != null && networkSetting.BarrierEdges.Count > 0)
            {
                List<string> barrieredges = new List<string>();
                foreach (int i in networkSetting.BarrierEdges)
                {
                    barrieredges.Add(i.ToString());
                }
                list.Add(string.Format("\"barrierEdges\":[{0}]", string.Join(",", barrieredges.ToArray())));
            }
            else
            {
                list.Add("\"barrierEdges\":null");
            }

            if (networkSetting.BarrierNodes != null && networkSetting.BarrierNodes.Count > 0)
            {
                List<string> barriernodes = new List<string>();
                foreach (int ii in networkSetting.BarrierNodes)
                {
                    barriernodes.Add(ii.ToString());
                }
                list.Add(string.Format("\"barrierNodes\":[{0}]", string.Join(",", barriernodes.ToArray())));
            }
            else 
            {
                list.Add("\"barrierNodes\":null");
            }

            if (!string.IsNullOrEmpty(networkSetting.FromNodeIDField))
            {
                list.Add(string.Format("\"fromNodeIDField\":\"{0}\"", networkSetting.FromNodeIDField));
            }
            else
            {
                list.Add("\"fromNodeIDField\":\"\"");
            }

            if (!string.IsNullOrEmpty(networkSetting.ToNodeIDField))
            {
                list.Add(string.Format("\"toNodeIDField\":\"{0}\"", networkSetting.ToNodeIDField));
            }
            else
            {
                list.Add("\"toNodeIDField\":\"\"");
            }

            if (!string.IsNullOrEmpty(networkSetting.RoadRuleField))
            {
                list.Add(string.Format("\"roadRuleField\":\"{0}\"", networkSetting.RoadRuleField));
            }
            else
            {
                list.Add("\"roadRuleField\":\"\"");
            }

            list.Add(string.Format("\"fromToOneWayValue\":{0}", networkSetting.FromToOneWayValue));
            list.Add(string.Format("\"toFromOneWayValue\":{0}", networkSetting.ToFromOneWayValue));
            list.Add(string.Format("\"twoWayValue\":{0}", networkSetting.TwoWayValue));
            list.Add(string.Format("\"prohibitValue\":{0}", networkSetting.ProhibitValue));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
    }
}
