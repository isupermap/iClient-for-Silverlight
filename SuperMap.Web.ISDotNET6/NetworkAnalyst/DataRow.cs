﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_DataRow_Title}</summary>
    public class DataRow
    {
        internal DataRow()
        { }
        /// <summary>${IS6_DataRow_attribute_values_D}</summary>
        public List<string> Values { get; private set; }

        /// <summary>${iServer2_DataRow_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_DataRow_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_DataRow_method_FromJson_return}</returns>
        public static DataRow FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            DataRow dr = new DataRow();

            JsonArray valuesInJson = (JsonArray)jsonObject["values"];
            if (valuesInJson != null && valuesInJson.Count > 0)
            {
                dr.Values = new List<string>();
                for (int i = 0; i < valuesInJson.Count; i++)
                {
                    dr.Values.Add((string)valuesInJson[i]);
                }
            }

            return dr;
        }
    }
}
