﻿
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_DataColumn_Title}</summary>
    public class DataColumn
    {
        internal DataColumn()
        { }
        /// <summary>${IS6_DataColumn_attribute_columnName_D}</summary>
        public string ColumnName { get; private set; }

        /// <summary>${iServer2_DataColumn_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_DataColumn_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_DataColumn_method_FromJson_return}</returns>
        public static DataColumn FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            DataColumn dc = new DataColumn();
            dc.ColumnName = (string)jsonObject["columnName"];
            return dc;
        }
    }
}
