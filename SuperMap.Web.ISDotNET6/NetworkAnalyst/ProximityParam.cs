﻿
using System.Collections.Generic;
using System.Text;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_ProximityParam_Title}</para>
    /// <para>${IS6_ProximityParam_Description}</para>
    /// </summary>
    public class ProximityParam
    {
        /// <summary>${IS6_ProximityParam_constructor_None_D}</summary>
        public ProximityParam()
        {
            NetworkParams = new NetworkParams();
            FacilityCount = 1;
            IsFromEvent = false;
        }
        /// <summary>${IS6_ProximityParam_attribute_facilityCount_D}</summary>
        public int FacilityCount { get; set; }
        /// <summary>${IS6_ProximityParam_attribute_facilityFilter_D}</summary>
        public string FacilityFilter { get; set; }
        /// <summary>${IS6_ProximityParam_attribute_facilityLayer_D}</summary>
        public string FacilityLayer { get; set; }
        /// <summary>${IS6_ProximityParam_attribute_isFromEvent_D}</summary>
        public bool IsFromEvent { get; set; }
        /// <summary>${IS6_ProximityParam_attribute_maxRadius_D}</summary>
        public double MaxRadius { get; set; }
        /// <summary>${IS6_ProximityParam_attribute_networkParams_D}</summary>
        public NetworkParams NetworkParams { get; set; }

        internal static string ToJson(ProximityParam param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();
            list.Add(string.Format("\"highlight\":null"));

            if (param.NetworkParams != null)
            {
                list.Add(string.Format("\"networkParams\":{0}", NetworkParams.ToJson(param.NetworkParams)));
            }
            else
            {
                list.Add(string.Format("\"networkParams\":null"));
            }

            if (!string.IsNullOrEmpty(param.FacilityFilter))
            {
                list.Add(string.Format("\"facilityFilter\":\"{0}\"", param.FacilityFilter));
            }
            else
            {
                list.Add("\"facilityFilter\":\"\"");
            }

            if (!string.IsNullOrEmpty(param.FacilityLayer))
            {
                list.Add(string.Format("\"facilityLayer\":\"{0}\"", param.FacilityLayer));
            }
            else
            {
                list.Add("\"facilityLayer\":\"\"");
            }


            list.Add(string.Format("\"isFromEvent\":{0}", param.IsFromEvent.ToString().ToLower()));

            list.Add(string.Format("\"maxRadius\":{0}", param.MaxRadius));
            list.Add(string.Format("\"facilityCount\":{0}", param.FacilityCount));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;

        }
    }
}
