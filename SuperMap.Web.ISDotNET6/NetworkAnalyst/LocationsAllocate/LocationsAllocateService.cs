﻿
using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Json;
using System.Windows;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_LocationsAllocateService_Title}</para>
    /// 	<para>${IS6_LocationsAllocateService_Description}</para>
    /// </summary>
    public class LocationsAllocateService : ServiceBase
    {
        //选址分析定义：
        //为一个或多个待建的设施选定最佳或最优的地址以使需求方以一种最高效的
        //方式获取服务或者商品。这个模型 不仅要决定设施的分布状况，还要将需求
        //点的需求分配到相应的设施中去，因而称之为选址分区。 

        /// <summary>${IS6_LocationsAllocateService_constructor_None_D}</summary>
        /// <overloads>${IS6_LocationsAllocateService_constructor_overloads_D}</overloads>
        public LocationsAllocateService()
        { 
        }

        /// <summary>${IS6_LocationsAllocateService_constructor_String_D}</summary>
        /// <param name="url">${IS6_LocationsAllocateService_constructor_String_param_url}</param>
        public LocationsAllocateService(string url)
            : base(url)
        {
        }

        /// <summary>${IS6_LocationsAllocateService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_LocationsAllocateService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="parameters">${IS6_LocationsAllocateService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(LocationsAllocateParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_LocationsAllocateService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_LocationsAllocateService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_LocationsAllocateService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(LocationsAllocateParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("LocationsAllocateParameters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }
            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(LocationsAllocateParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "LocationsAllocate";
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);
            dictionary.Add("locationsAllocateParam",LocationsAllocateParam.ToJson(parameters.LocationsAllocateParam));
            return dictionary;
        }


        private LocationsAllocateResult lastResult;

        /// <summary>${IS6_LocationsAllocateService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<LocationsAllocateEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            LocationsAllocateResult result = LocationsAllocateResult.FromJson(jsonObject);
            LastResult = result;
            LocationsAllocateEventArgs args = new LocationsAllocateEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(LocationsAllocateEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_LocationsAllocateService_attribute_lastResult_D}</summary>
        public LocationsAllocateResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}

