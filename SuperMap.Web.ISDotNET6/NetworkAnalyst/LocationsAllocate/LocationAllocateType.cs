﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_LocationAllocateType_Title}</para>
    /// <para>${IS6_LocationAllocateType_Description}</para>
    /// </summary>
    public enum LocationAllocateType
    {
        /// <summary>${IS6_LocationAllocateType_attribute_undefined_D}</summary>
        Undefined = 0,
        /// <summary>${IS6_LocationAllocateType_attribute_minDistance_D}</summary>
        MinDistance = 1,
        /// <summary>${IS6_LocationAllocateType_attribute_maxCover_D}</summary>
        MaxCover = 2,
        /// <summary>${IS6_LocationAllocateType_attribute_minDistPower_D}</summary>
        MinDistPower = 3,
        /// <summary>${IS6_LocationAllocateType_attribute_maxAttend_D}</summary>
        MaxAttend = 4
    }
}
