﻿
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_DataResult_Title}</summary>
    public class DataResult
    {
        internal DataResult()
        { }

        /// <summary>${IS6_DataResult_attribute_actualRecordCount_D}</summary>
        public int ActualRecordCount { get; private set; }

        /// <summary>${IS6_DataResult_attribute_dataset_D}</summary>
        public string Dataset { get; private set; }

        /// <summary>${IS6_DataResult_attribute_message_D}</summary>
        public string Message { get; private set; }

        /// <summary>${IS6_DataResult_attribute_recordSet_D}</summary>
        public RecordSet RecordSet { get; private set; }

        /// <summary>${IS6_DataResult_attribute_returnOption_D}</summary>
        public DataReturnOption ReturnOption { get; private set; }

        /// <summary>${IS6_DataResult_attribute_succeed_D}</summary>
        public bool Succeed { get; private set; }

        /// <summary>${IS6_DataResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_DataResult_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_DataResult_method_FromJson_return}</returns>
        public static DataResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            DataResult result = new DataResult();

            result.ActualRecordCount = (int)jsonObject["actualRecordCount"];

            result.Dataset = (string)jsonObject["dataset"];

            result.Message = (string)jsonObject["message"];

            result.RecordSet = RecordSet.FromJson((JsonObject)jsonObject["recordset"]);

            result.ReturnOption = DataReturnOption.FromJson((JsonObject)jsonObject["returnOption"]);

            result.Succeed = (bool)jsonObject["succeed"];

            return result;
        }
    }
}
