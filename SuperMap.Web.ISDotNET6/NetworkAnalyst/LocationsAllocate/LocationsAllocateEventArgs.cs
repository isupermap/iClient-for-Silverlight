﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_LocationsAllocateEventArgs_Title}</para>
    /// 	<para>${IS6_LocationsAllocateEventArgs_Description}</para>
    /// </summary>
    public class LocationsAllocateEventArgs:ServiceEventArgs
    {
        /// <summary>${IS6_LocationsAllocateResult_constructor_D}</summary>
        /// <param name="result">${IS6_LocationsAllocateResult_constructor_param_result}</param>
        /// <param name="originResult">${IS6_LocationsAllocateResult_constructor_param_originResult}</param>
        /// <param name="token">${IS6_LocationsAllocateResult_constructor_param_token}</param>
        public LocationsAllocateEventArgs(LocationsAllocateResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_LocationsAllocateEventArgs_attribute_result_D}</summary>
        public LocationsAllocateResult Result { get; private set; }
        /// <summary>${IS6_LocationsAllocateEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
