﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_DataReturnMode_Title}</para>
    /// <para>${IS6_DataReturnMode_Description}</para>
    /// </summary>
    public enum DataReturnMode
    {
        /// <summary>${IS6_DataReturnMode_attribute_none_D}</summary>
        None = 0,
        /// <summary>${IS6_DataReturnMode_attribute_asRecordset_D}</summary>
        AsRecordset = 1,
        /// <summary>${IS6_DataReturnMode_attribute_asDataset_D}</summary>
        AsDataset = 2
    }
}
