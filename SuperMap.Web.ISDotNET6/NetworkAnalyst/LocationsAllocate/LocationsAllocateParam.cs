﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_LocationsAllocateParam_Title}</para>
    /// 	<para>${IS6_LocationsAllocateParam_Description}</para>
    /// </summary>
    public class LocationsAllocateParam
    {
        /// <summary>${IS6_LocationsAllocateParam_constructor_None_D}</summary>
        public LocationsAllocateParam()
        {
            FromCenter = true;
            //目前IS6只能采用最小距离模型
            LocationAllocateMode = LocationAllocateType.MinDistance;
        }

        /// <summary>${IS6_LocationsAllocateParam_attribute_fromCenter_D}</summary>
        public bool FromCenter { get; set; }

        /// <summary>${IS6_LocationsAllocateParam_attribute_locationAllocateMode_D}</summary>
        public LocationAllocateType LocationAllocateMode { get; set; }

        /// <summary>${IS6_LocationsAllocateParam_attribute_locationsCount_D}</summary>
        public int LocationsCount { get; set; }

        /// <summary>${IS6_LocationsAllocateParam_attribute_networkParams_D}</summary>
        public NetworkParams NetworkParams { get; set; }

        /// <summary>${IS6_LocationsAllocateParam_attribute_resultCenterNodesTableOption_D}</summary>
        public DataReturnOption ResultCenterNodesTableOption { get; set; }

        /// <summary>${IS6_LocationsAllocateParam_attribute_resultCenterOption_D}</summary>
        public DataReturnOption ResultCenterOption { get; set; }


        internal static string ToJson(LocationsAllocateParam param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"fromCenter\":{0}", param.FromCenter.ToString().ToLower()));

            list.Add(string.Format("\"locationAllocateMode\":{0}", (int)param.LocationAllocateMode));

            list.Add(string.Format("\"locationsCount\":{0}", param.LocationsCount));

            list.Add(string.Format("\"networkParams\":{0}", NetworkParams.ToJson(param.NetworkParams)));

            list.Add(string.Format("\"resultCenterNodesTableOption\":{0}", DataReturnOption.ToJson(param.ResultCenterNodesTableOption)));

            list.Add(string.Format("\"resultCenterOption\":{0}", DataReturnOption.ToJson(param.ResultCenterOption)));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
