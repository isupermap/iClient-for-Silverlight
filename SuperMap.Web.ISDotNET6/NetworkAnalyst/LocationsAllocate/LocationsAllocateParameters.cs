﻿
namespace SuperMap.Web.ISDotNET6
{

    /// <summary>
    /// 	<para>${IS6_LocationsAllocateParameters_Title}</para>
    /// 	<para>${IS6_LocationsAllocateParameters_Description}</para>
    /// </summary>
    public class LocationsAllocateParameters:ParametersBase
    {
        /// <summary>${IS6_LocationsAllocateParameters_constructor_None_D}</summary>
        public LocationsAllocateParameters()
        { }
        /// <summary>${IS6_LocationsAllocateParameters_attribute_locationsAllocateParam_D}</summary>
        public LocationsAllocateParam  LocationsAllocateParam { get; set; }
    }
}
