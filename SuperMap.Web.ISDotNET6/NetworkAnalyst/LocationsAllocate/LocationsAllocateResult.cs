﻿

using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_LocationsAllocateResult_Title}</para>
    /// 	<para>${IS6_LocationsAllocateResult_Description}</para>
    /// </summary>
    public class LocationsAllocateResult
    {
        internal LocationsAllocateResult()
        { }

        /// <summary>${IS6_LocationsAllocateResult_attribute_message_D}</summary>
        public string Message { get; set; }

        /// <summary>${IS6_LocationsAllocateResult_attribute_resultCenterData_D}</summary>
        public DataResult ResultCenterData { get; set; }

        /// <summary>${IS6_LocationsAllocateResult_attribute_resultCenterNodesTableData_D}</summary>
        public DataResult ResultCenterNodesTableData { get; set; }

        /// <summary>${IS6_LocationsAllocateResult_attribute_succeed_D}</summary>
        public bool Succeed { get; set; }


        /// <summary>${IS6_LocationsAllocateResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_LocationsAllocateResult_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_LocationsAllocateResult_method_FromJson_return}</returns>
        public static LocationsAllocateResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            LocationsAllocateResult result = new LocationsAllocateResult();

            result.Message = (string)jsonObject["message"];

            result.ResultCenterData = DataResult.FromJson((JsonObject)jsonObject["resultCenterData"]);

            result.ResultCenterNodesTableData = DataResult.FromJson((JsonObject)jsonObject["resultCenterNodesTableData"]);

            result.Succeed = (bool)jsonObject["succeed"];

            return result;
        }
    }
}
