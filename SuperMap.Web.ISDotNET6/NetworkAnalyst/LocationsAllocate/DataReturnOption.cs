﻿
using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_DataReturnOption_Title}</para>
    /// 	<para>${IS6_DataReturnOption_Description}</para>
    /// </summary>
    public class DataReturnOption
    {
        /// <summary>${IS6_DataReturnOption_constructor_None_D}</summary>
        public DataReturnOption()
        {
            //OverwriteIfExists = true;
        }

        /// <summary>${IS6_DataReturnOption_attribute_dataset_D}</summary>
        public string Dataset { get; set; }

        /// <summary>${IS6_DataReturnOption_attribute_maxRecordCount_D}</summary>
        public int MaxRecordCount { get; set; }

        /// <summary>${IS6_DataReturnOption_attribute_overwriteIfExists_D}</summary>
        public bool OverwriteIfExists { get; set; }

        /// <summary>${IS6_DataReturnOption_attribute_returnMode_D}</summary>
        public DataReturnMode ReturnMode { get; set; }



        internal static string ToJson(DataReturnOption param)
        {
            if (param == null)
            {
                return "{}";
            }
            string json = "{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(param.Dataset))
            {
                list.Add(string.Format("\"dataset\":\"{0}\"", param.Dataset));
            }
            else
            {
                list.Add("\"dataset\":\"\"");
            }

            list.Add(string.Format("\"maxRecordCount\":{0}", param.MaxRecordCount));

            list.Add(string.Format("\"overwriteIfExists\":{0}", param.OverwriteIfExists.ToString().ToLower()));

            list.Add(string.Format("\"returnMode\":{0}", (int)param.ReturnMode));


            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        /// <summary>${iServer2_DataReturnOption_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_DataReturnOption_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_DataReturnOption_method_FromJson_return}</returns>
        public static DataReturnOption FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            DataReturnOption drp = new DataReturnOption();

            if (jsonObject.ContainsKey("dataset"))
            {
                drp.Dataset = (string)jsonObject["dataset"];
            }

            if (jsonObject.ContainsKey("maxRecordCount"))
            {
                drp.MaxRecordCount = (int)jsonObject["maxRecordCount"];
            }

            if (jsonObject.ContainsKey("overwriteIfExists"))
            {
                drp.OverwriteIfExists = (bool)jsonObject["overwriteIfExists"];
            }

            if (jsonObject.ContainsKey("returnMode"))
            {
                drp.ReturnMode = (DataReturnMode)((int)jsonObject["returnMode"]);
            }
            return drp;
        }
    }
}
