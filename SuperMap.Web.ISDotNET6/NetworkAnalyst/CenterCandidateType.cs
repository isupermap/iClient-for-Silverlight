﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_CenterCandidateType_Title}</para>
    /// <para>${IS6_CenterCandidateType_Description}</para>
    /// </summary>
    public enum CenterCandidateType
    {
        /// <summary>${IS6_CenterCandidateType_attribute_Not_D}</summary>
        Not=0,
        /// <summary>${IS6_CenterCandidateType_attribute_Mobile_D}</summary>
        Mobile=1,
        /// <summary>${IS6_CenterCandidateType_attribute_Fixed_D}</summary>
        Fixed=2
    }
}
