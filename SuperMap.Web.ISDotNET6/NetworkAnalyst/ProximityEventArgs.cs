﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_ProximityEventArgs_Title}</para>
    /// <para>${IS6_ProximityEventArgs_Description}</para>
    /// </summary>
    public class ProximityEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_ProximityEventArgs_constructor_D}</summary>
        /// <param name="result">${IS6_ProximityEventArgs_constructor_param_result}</param>
        /// <param name="originResult">${IS6_ProximityEventArgs_constructor_param_originResult}</param>
        /// <param name="token">${IS6_ProximityEventArgs_constructor_param_token}</param>
        public ProximityEventArgs(ProximityResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_ProximityEventArgs_attribute_result_D}</summary>
        public ProximityResult Result { get; private set; }
        /// <summary>${IS6_ProximityEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
