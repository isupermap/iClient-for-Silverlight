﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_DataTable_Title}</para>
    /// <para>${IS6_DataTable_Description}</para>
    /// </summary>
    public class DataTable
    {
        internal DataTable()
        { }
        /// <summary>${IS6_PathGuide_attribute_columns_D}</summary>
        public List<DataColumn> Columns { get; private set; }
        /// <summary>${IS6_PathGuide_attribute_rows_D}</summary>
        public List<DataRow> Rows { get; private set; }

        /// <summary>${iServer2_DataTable_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_DataTable_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_DataTable_method_FromJson_return}</returns>
        public static DataTable FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            DataTable dt = new DataTable();

            JsonArray columnsInJson = (JsonArray)jsonObject["columns"];
            if (columnsInJson != null && columnsInJson.Count > 0)
            {
                dt.Columns = new List<DataColumn>();
                for (int i = 0; i < columnsInJson.Count; i++)
                {
                    dt.Columns.Add(DataColumn.FromJson((JsonObject)columnsInJson[i]));
                }
            }
            JsonArray rowsInJson = (JsonArray)jsonObject["rows"];
            if (rowsInJson != null && rowsInJson.Count > 0)
            {
                dt.Rows = new List<DataRow>();
                for (int i = 0; i < rowsInJson.Count; i++)
                {
                    dt.Rows.Add(DataRow.FromJson((JsonObject)rowsInJson[i]));
                }
            }

            return dt;
        }
    }
}
