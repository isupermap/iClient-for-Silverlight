﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_RouteEventArgs_Title}</para>
    /// <para>${IS6_RouteEventArgs_Description}</para>
    /// </summary>
    public class RouteEventArgs:ServiceEventArgs
    {
        /// <summary>${IS6_RouteEventArgs_constructor_D}</summary>
        /// <param name="result">${IS6_RouteEventArgs_constructor_param_result}</param>
        /// <param name="originResult">${IS6_RouteEventArgs_constructor_param_originResult}</param>
        /// <param name="token">${IS6_RouteEventArgs_constructor_param_token}</param>
        public RouteEventArgs(RouteResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_RouteEventArgs_attribute_result_D}</summary>
        public RouteResult Result { get; private set; }
        /// <summary>${IS6_RouteEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
