﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using System.Windows;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ClosestFacilityService_Title}</para>
    /// 	<para>${IS6_ClosestFacilityService_Description}</para>
    /// </summary>
    public class ClosestFacilityService : ServiceBase
    {
        /// <summary>${IS6_ClosestFacilityService_constructor_None_D}</summary>
        /// <overloads>${IS6_ClosestFacilityService_constructor_overloads_D}</overloads>
        public ClosestFacilityService()
        { }
        /// <summary>${IS6_ClosestFacilityService_constructor_String_D}</summary>
        /// <param name="url">${IS6_ClosestFacilityService_constructor_String_param_url}</param>
        public ClosestFacilityService(string url)
            : base(url)
        { }

        /// <summary>${IS6_ClosestFacilityService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_ClosestFacilityService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="parameters">${IS6_ClosestFacilityService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(ClosestFacilityParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${IS6_ClosestFacilityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_ClosestFacilityService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_ClosestFacilityService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(ClosestFacilityParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("ClosestFacilityParameters is Null");
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }

            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);

        }

        private Dictionary<string, string> GetParameters(ClosestFacilityParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "ClosestFacility";
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);
            dictionary.Add("point", JsonHelper.FromPoint2D(parameters.Point));
            dictionary.Add("proximityParam", ProximityParam.ToJson(parameters.ProximityParam));
            dictionary.Add("trackingLayerIndex", "-1");
            dictionary.Add("userID", Guid.NewGuid().ToString());
            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            ProximityResult result = ProximityResult.FromJson(jsonObject);
            LastResult = result;
            ProximityEventArgs args = new ProximityEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private ProximityResult lastResult;

        /// <summary>${IS6_ClosestFacilityService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ProximityEventArgs> ProcessCompleted;
        private void OnProcessCompleted(ProximityEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_ClosestFacilityService_attribute_lastResult_D}</summary>
        public ProximityResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
