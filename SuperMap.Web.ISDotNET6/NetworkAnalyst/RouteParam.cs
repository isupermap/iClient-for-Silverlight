﻿

using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_RouteParam_Title}</para>
    /// 	<para>${IS6_RouteParam_Description}</para>
    /// </summary>
    public class RouteParam
    {
        /// <summary>${IS6_RouteParam_constructor_None_D}</summary>
        public RouteParam()
        {
            NetworkParams = new NetworkParams();
            ReturnCosts = false;
            ReturnEdgeIDsAndNodeIDs = true;
            ReturnNodePositions = true;
        }
        /// <summary>${IS6_RouteParam_attribute_networkParams_D}</summary>
        public NetworkParams NetworkParams { get; set; }
        /// <summary>${IS6_RouteParam_attribute_returnPathInfo_D}</summary>
        public ReturnPathInfoParam ReturnPathInfo { get; set; }
        /// <summary>${IS6_RouteParam_attribute_returnNodePositions_D}</summary>
        public bool ReturnNodePositions { get; set; }
        /// <summary>${IS6_RouteParam_attribute_returnEdgeIDsAndNodeIDs_D}</summary>
        public bool ReturnEdgeIDsAndNodeIDs { get; set; }
        /// <summary>${IS6_RouteParam_attribute_returnCosts_D}</summary>
        public bool ReturnCosts { get; set; }

        internal static string ToJson(RouteParam param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"highlight\":null"));

            if (param.NetworkParams != null)
            {
                list.Add(string.Format("\"networkParams\":{0}", NetworkParams.ToJson(param.NetworkParams)));
            }
            else
            {
                list.Add(string.Format("\"networkParams\":null")); 
            }

            if (param.ReturnPathInfo != null)
            {
                list.Add(string.Format("\"returnPathInfo\":{0}", ReturnPathInfoParam.ToJson(param.ReturnPathInfo)));
            }
            else
            {
                list.Add(string.Format("\"returnPathInfo\":null"));
            }

            list.Add(string.Format("\"returnNodePositions\":{0}", param.ReturnNodePositions.ToString().ToLower()));
            list.Add(string.Format("\"returnEdgeIDsAndNodeIDs\":{0}", param.ReturnEdgeIDsAndNodeIDs.ToString().ToLower()));
            list.Add(string.Format("\"returnCosts\":{0}", param.ReturnCosts.ToString().ToLower()));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
