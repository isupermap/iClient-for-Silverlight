﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_SideType_Title}</para>
    /// <para>${IS6_SideType_Description}</para>
    /// </summary>
    public enum SideType
    {
        /// <summary>${IS6_SideType_attribute_middle_D}</summary>
        Middle=0,
        /// <summary>${IS6_SideType_attribute_left_D}</summary>
        Left=1,
        /// <summary>${IS6_SideType_attribute_right_D}</summary>
        Right=2,
        /// <summary>${IS6_SideType_attribute_none_D}</summary>
        None=-1
    }
}
