﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_LocationAllocateSetting_Title}</para>
    /// <para>${IS6_LocationAllocateSetting_Description}</para>
    /// </summary>
    public class LocationAllocateSetting
    {
        /// <summary>${IS6_LocationAllocateSetting_constructor_None_D}</summary>
        public LocationAllocateSetting()
        { }
        /// <summary>${IS6_LocationAllocateSetting_attribute_centers_D}</summary>
        public IList<ServerCenter> Centers { get; set; }
        /// <summary>${IS6_LocationAllocateSetting_attribute_edgeDemandField_D}</summary>
        public string EdgeDemandField { get; set; }
        /// <summary>${IS6_LocationAllocateSetting_attribute_nodeDemandField_D}</summary>
        public string NodeDemandField { get; set; }

        internal static string ToJson(LocationAllocateSetting param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (param.Centers != null)
            {
                List<string> centers = new List<string>();
                foreach (ServerCenter c in param.Centers)
                {
                    centers.Add(ServerCenter.ToJson(c));
                }
                list.Add(string.Format("\"centers\":[{0}]", string.Join(",", centers.ToArray())));
            }
            else
            {
                list.Add("\"centers\":null");
            }

            if (!string.IsNullOrEmpty(param.EdgeDemandField))
            {
                list.Add(string.Format("\"edgeDemandField\":\"{0}\"", param.EdgeDemandField));
            }
            else
            {
                list.Add("\"edgeDemandField\":\"\"");
            }

            if (!string.IsNullOrEmpty(param.NodeDemandField))
            {
                list.Add(string.Format("\"nodeDemandField\":\"{0}\"", param.NodeDemandField));
            }
            else
            {
                list.Add("\"nodeDemandField\":\"\"");
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
