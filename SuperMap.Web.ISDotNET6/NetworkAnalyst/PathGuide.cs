﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_PathGuide_Title}</para>
    /// 	<para>${IS6_PathGuide_Description}</para>
    /// </summary>
    public class PathGuide
    {
        internal PathGuide()
        { }
        /// <summary>${IS6_PathGuide_attribute_count_D}</summary>
        public int Count { get; private set; }
        /// <summary>${IS6_PathGuide_attribute_pathGuideItems_D}</summary>
        public List<PathGuideItem> PathGuideItems { get; private set; }

        /// <summary>${IS6_PathGuide_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_PathGuide_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_PathGuide_method_FromJson_return}</returns>
        public static PathGuide FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            PathGuide pg = new PathGuide();

            JsonArray itemINJson = (JsonArray)jsonObject["pathGuideItems"];
            if (itemINJson != null && itemINJson.Count > 0)
            {
                pg.PathGuideItems = new List<PathGuideItem>();
                for (int i = 0; i < itemINJson.Count; i++)
                {
                    PathGuideItem pi = PathGuideItem.FromJson((JsonObject)itemINJson[i]);
                    pg.PathGuideItems.Add(pi);
                }
            }
            pg.Count = (int)jsonObject["count"];
            return pg;
        }
    }
}
