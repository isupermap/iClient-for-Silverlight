﻿

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_TurnType_Title}</para>
    /// <para>${IS6_TurnType_Description}</para>
    /// </summary>
    public enum TurnType
    {
        /// <summary>${IS6_TurnType_attribute_end_D}</summary>
        End=0,
        /// <summary>${IS6_TurnType_attribute_left_D}</summary>
        Left=1,
        /// <summary>${IS6_TurnType_attribute_right_D}</summary>
        Right=2,
        /// <summary>${IS6_TurnType_attribute_ahead_D}</summary>
        Ahead=3,
        /// <summary>${IS6_TurnType_attribute_back_D}</summary>
        Back=4,
        /// <summary>${IS6_TurnType_attribute_none_D}</summary>
        None=-1
    }
}
