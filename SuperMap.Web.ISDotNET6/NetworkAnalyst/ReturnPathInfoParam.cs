﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_ReturnPathInfo_Title}</para>
    /// <para>${IS6_ReturnPathInfo_Description}</para>
    /// </summary>
    public class ReturnPathInfoParam
    {
        /// <summary>${IS6_ReturnPathInfo_constructor_None_D}</summary>
        public ReturnPathInfoParam()
        { }
        /// <summary>${IS6_ReturnPathInfo_attribute_edgeNameField_D}</summary>
        public string EdgeNameField { get; set; }
        /// <summary>${IS6_ReturnPathInfo_attribute_nodeNameField_D}</summary>
        public string NodeNameField { get; set; }
        /// <summary>${IS6_ReturnPathInfo_attribute_returnPathGuide_D}</summary>
        public bool ReturnPathGuide { get; set; }
        /// <summary>${IS6_ReturnPathInfo_attribute_returnPathTable_D}</summary>
        public bool ReturnPathTable { get; set; }

        internal static string ToJson(ReturnPathInfoParam param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(param.EdgeNameField))
            {
                list.Add(string.Format("\"edgeNameField\":\"{0}\"", param.EdgeNameField));
            }
            else
            {
                list.Add("\"edgeNameField\":\"\"");
            }

            if (!string.IsNullOrEmpty(param.NodeNameField))
            {
                list.Add(string.Format("\"nodeNameField\":\"{0}\"", param.NodeNameField));
            }
            else
            {
                list.Add("\"nodeNameField\":\"\"");
            }

            list.Add(string.Format("\"returnPathGuide\":{0}",param.ReturnPathGuide.ToString().ToLower()));
            list.Add(string.Format("\"returnPathTable\":{0}", param.ReturnPathTable.ToString().ToLower()));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

    }
}
