﻿
using SuperMap.Web.Core;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ClosestFacilityParameters_Title}</para>
    /// 	<para>${IS6_ClosestFacilityParameters_Description}</para>
    /// </summary>
    /// <remarks>${IS6_ClosestFacilityParameters_Remarks}</remarks>
    public class ClosestFacilityParameters : ParametersBase
    {
        /// <summary>${IS6_ClosestFacilityParameters_constructor_None_D}</summary>
        public ClosestFacilityParameters()
        { }
        /// <summary>${IS6_ClosestFacilityParameters_attribute_point_D}</summary>
        public Point2D Point { get; set; }
        /// <summary>${IS6_ClosestFacilityParameters_attribute_proximityParam_D}</summary>
        public ProximityParam ProximityParam { get; set; }
    }
}
