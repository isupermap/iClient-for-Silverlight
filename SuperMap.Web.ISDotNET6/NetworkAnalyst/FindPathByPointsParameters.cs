﻿
using System.Collections.Generic;
using SuperMap.Web.Core;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FindPathByPointsParameters_Title}</para>
    /// 	<para>${IS6_FindPathByPointsParameters_Description}</para>
    /// </summary>
    /// <remarks>${IS6_FindPathByPointsParameters_Remarks}</remarks>
    public class FindPathByPointsParameters : ParametersBase
    {
        /// <summary>${IS6_FindPathByPointsParameters_constructor_None_D}</summary>
        public FindPathByPointsParameters()
        {
            RouteParam = new RouteParam();
        }
        /// <summary>${IS6_FindPathByPointsParameters_attribute_points_D}</summary>
        public Point2DCollection Points { get; set; }
        /// <summary>${IS6_FindPathByPointsParameters_attribute_routeParam_D}</summary>
        public RouteParam RouteParam { get; set; }
    }
}
