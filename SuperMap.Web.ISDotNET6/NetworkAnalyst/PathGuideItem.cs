﻿
using SuperMap.Web.Core;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_PathGuideItem_Title}</para>
    /// <para>${IS6_PathGuideItem_Description}</para>
    /// </summary>
    public class PathGuideItem
    {
        internal PathGuideItem()
        { }
        /// <summary>${IS6_PathGuideItem_attribute_bounds_D}</summary>
        public Rectangle2D Bounds { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_directionType_D}</summary>
        public DirectionType DirectionType { get; private set; }
        /// <summary>
        /// 	<para>${IS6_PathGuideItem_attribute_distance_D}</para>
        /// 	<para><img src="tolerance.png"/></para>
        /// </summary>
        public double Distance { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_ID_D}</summary>
        public int ID { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_index_D}</summary>
        public int Index { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_isEdge_D}</summary>
        public bool IsEdge { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_isStop_D}</summary>
        public bool IsStop { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_length_D}</summary>
        public double Length { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_name_D}</summary>
        public string Name { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_sideType_D}</summary>
        public SideType SideType { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_turnAngle_D}</summary>
        public double TurnAngle { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_turnType_D}</summary>
        public TurnType TurnType { get; private set; }
        /// <summary>${IS6_PathGuideItem_attribute_weight_D}</summary>
        public double Weight { get; private set; }

        /// <summary>${IS6_PathGuideItem_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_PathGuideItem_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_PathGuideItem_method_FromJson_return}</returns>
        public static PathGuideItem FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            PathGuideItem item = new PathGuideItem();
            #region Bounds
            if (jsonObject["bounds"] != null)
            {
                double mbMinX = (double)jsonObject["bounds"]["leftBottom"]["x"];
                double mbMinY = (double)jsonObject["bounds"]["leftBottom"]["y"];
                double mbMaxX = (double)jsonObject["bounds"]["rightTop"]["x"];
                double mbMaxY = (double)jsonObject["bounds"]["rightTop"]["y"];
                item.Bounds = new Rectangle2D(mbMinX, mbMinY, mbMaxX, mbMaxY);
            }
            #endregion

            item.DirectionType = (DirectionType)(int)jsonObject["directionType"];

            item.Distance = (double)jsonObject["distance"];
            item.ID = (int)jsonObject["id"];
            item.Index = (int)jsonObject["index"];
            item.IsEdge = (bool)jsonObject["isEdge"];
            item.IsStop = (bool)jsonObject["isStop"];
            item.Length = (double)jsonObject["length"];
            item.Name = (string)jsonObject["name"];
            item.SideType = (SideType)(int)jsonObject["sideType"];
            item.TurnAngle = (double)jsonObject["turnAngle"];
            item.TurnType = (TurnType)(int)jsonObject["turnType"];
            item.Weight = (double)jsonObject["weight"];

            return item;
        }

    }
}
