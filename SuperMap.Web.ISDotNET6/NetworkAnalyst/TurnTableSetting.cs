﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_TurnTableSetting_Title}</para>
    /// 	<para>${IS6_TurnTableSetting_Description_1}</para>
    /// 	<para><img src="turn.png"/></para>
    /// 	<para>${IS6_TurnTableSetting_Description_2}</para>
    /// 	<para><img src="turnTable.jpg"/></para>
    /// </summary>
    public class TurnTableSetting
    {
        /// <summary>${IS6_TurnTableSetting_constructor_None_D}</summary>
        public TurnTableSetting()
        { }
        /// <summary>${IS6_TurnTableSetting_attribute_fromEdgeIDField_D}</summary>
        public string FromEdgeIDField { get; set; }
        /// <summary>${IS6_TurnTableSetting_attribute_nodeIDField_D}</summary>
        public string NodeIDField { get; set; }
        /// <summary>${IS6_TurnTableSetting_attribute_toEdgeIDField_D}</summary>
        public string ToEdgeIDField { get; set; }
        /// <summary>${IS6_TurnTableSetting_attribute_turnTableName_D}</summary>
        public string TurnTableName { get; set; }
        /// <summary>${IS6_TurnTableSetting_attribute_weightField_D}</summary>
        public string WeightField { get; set; }

        internal static string ToJson(TurnTableSetting turnTableSetting)
        {
            if (turnTableSetting == null)
            {
                return null;
            }

            string json = "{";

            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(turnTableSetting.FromEdgeIDField))
            {
                list.Add(string.Format("\"fromEdgeIDField\":\"{0}\"", turnTableSetting.FromEdgeIDField));
            }
            else
            {
                list.Add("\"fromEdgeIDField\":\"\"");
            }

            if (!string.IsNullOrEmpty(turnTableSetting.NodeIDField))
            {
                list.Add(string.Format("\"nodeIDField\":\"{0}\"", turnTableSetting.NodeIDField));
            }
            else
            {
                list.Add("\"nodeIDField\":\"\"");
            }

            if (!string.IsNullOrEmpty(turnTableSetting.ToEdgeIDField))
            {
                list.Add(string.Format("\"toEdgeIDField\":\"{0}\"", turnTableSetting.ToEdgeIDField));
            }
            else
            {
                list.Add("\"toEdgeIDField\":\"\"");
            }

            if (!string.IsNullOrEmpty(turnTableSetting.TurnTableName))
            {
                list.Add(string.Format("\"turnTableName\":\"{0}\"", turnTableSetting.TurnTableName));
            }
            else
            {
                list.Add("\"turnTableName\":\"\"");
            }

            if (!string.IsNullOrEmpty(turnTableSetting.WeightField))
            {
                list.Add(string.Format("\"weightField\":\"{0}\"", turnTableSetting.WeightField));
            }
            else
            {
                list.Add("\"weightField\":\"\"");
            }

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
    }
}
