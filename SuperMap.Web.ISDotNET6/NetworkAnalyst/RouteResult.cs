﻿
using SuperMap.Web.Core;
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_RouteResult_Title}</para>
    /// <para>${IS6_RouteResult_Description}</para>
    /// </summary>
    public class RouteResult
    {
        private RouteResult()
        { }
        /// <summary>${IS6_RouteResult_attribute_bounds_D}</summary>
        public Rectangle2D Bounds { get; private set; }
        /// <summary>${IS6_RouteResult_attribute_costs_D}</summary>
        public List<double> Costs { get; private set; }
        /// <summary>${IS6_RouteResult_attribute_edgeIDs_D}</summary>
        public List<int> EdgeIDs { get; private set; }
        /// <summary>${IS6_RouteResult_attribute_nodeIDs_D}</summary>
        public List<int> NodeIDs { get; private set; }
        /// <summary>${IS6_RouteResult_attribute_nodePositions_D}</summary>
        public Point2DCollection NodePositions { get; private set; }
        /// <summary>${IS6_RouteResult_attribute_pathGuides_D}</summary>
        public List<PathGuide> PathGuides { get; private set; }
        /// <summary>${IS6_RouteResult_attribute_pathTable_D}</summary>
        public DataTable PathTable { get; private set; }
        /// <summary>${IS6_RouteResult_attribute_totalLength_D}</summary>
        public double TotalLength { get; private set; }
        //public int TrackingLayerIndex { get; private set; }
        //public string UserID { get; private set; }

        /// <summary>${IS6_RouteResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_RouteResult_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_RouteResult_method_FromJson_return}</returns>
        public static RouteResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            RouteResult result = new RouteResult();
            #region Bounds
            if (jsonObject["bounds"] != null)
            {
                double mbMinX = (double)jsonObject["bounds"]["leftBottom"]["x"];
                double mbMinY = (double)jsonObject["bounds"]["leftBottom"]["y"];
                double mbMaxX = (double)jsonObject["bounds"]["rightTop"]["x"];
                double mbMaxY = (double)jsonObject["bounds"]["rightTop"]["y"];
                result.Bounds = new Rectangle2D(mbMinX, mbMinY, mbMaxX, mbMaxY);
            }
            #endregion

            #region Costs
            JsonArray costsInJson = (JsonArray)jsonObject["costs"];
            if (costsInJson != null && costsInJson.Count > 0)
            {
                result.Costs = new List<double>();
                for (int i = 0; i < costsInJson.Count; i++)
                {
                    result.Costs.Add((double)costsInJson[i]);
                }
            }
            #endregion

            #region EdgeIDs
            JsonArray edgeIDsInJson = (JsonArray)jsonObject["edgeIDs"];
            if (edgeIDsInJson != null && edgeIDsInJson.Count > 0)
            {
                result.EdgeIDs = new List<int>();
                for (int i = 0; i < edgeIDsInJson.Count; i++)
                {
                    result.EdgeIDs.Add((int)edgeIDsInJson[i]);
                }
            }
            #endregion

            #region NodeIDs
            JsonArray nodeIDsInJson = (JsonArray)jsonObject["nodeIDs"];
            if (nodeIDsInJson != null && nodeIDsInJson.Count > 0)
            {
                result.NodeIDs = new List<int>();
                for (int i = 0; i < nodeIDsInJson.Count; i++)
                {
                    result.NodeIDs.Add((int)nodeIDsInJson[i]);
                }
            }
            #endregion

            #region NodePositions
            JsonArray nodePositionsInJson = (JsonArray)jsonObject["nodePositions"];
            if (nodePositionsInJson != null && nodePositionsInJson.Count > 0)
            {
                result.NodePositions = new Point2DCollection();
                for (int i = 0; i < nodePositionsInJson.Count; i++)
                {
                    result.NodePositions.Add(new Point2D(nodePositionsInJson[i]["x"], nodePositionsInJson[i]["y"]));
                }
            }
            #endregion

            #region PathGuides
            JsonArray pathGuidesInJson = (JsonArray)jsonObject["pathGuides"];
            if (pathGuidesInJson != null && pathGuidesInJson.Count > 0)
            {
                result.PathGuides = new List<PathGuide>();
                for (int i = 0; i < pathGuidesInJson.Count; i++)
                {
                    PathGuide pg = PathGuide.FromJson((JsonObject)pathGuidesInJson[i]);
                    result.PathGuides.Add(pg);
                }
            }
            #endregion

            result.PathTable = DataTable.FromJson((JsonObject)jsonObject["pathTable"]);
            result.TotalLength = (double)jsonObject["totalLength"];
            //result.TrackingLayerIndex = (int)jsonObject["trackingLayerIndex"];
            //result.UserID = (string)jsonObject["userID"];
            return result;
        }
    }
}
