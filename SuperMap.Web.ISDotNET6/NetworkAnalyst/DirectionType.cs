﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_DirectionType_Title}</para>
    /// <para>${IS6_DirectionType_Description}</para>
    /// </summary>
    public enum DirectionType
    {
        /// <summary>${IS6_DirectionType_attribute_east_D}</summary>
        East=0,
        /// <summary>${IS6_DirectionType_attribute_south_D}</summary>
        South=1,
        /// <summary>${IS6_DirectionType_attribute_west_D}</summary>
        West=2,
        /// <summary>${IS6_DirectionType_attribute_north_D}</summary>
        North=3,
        /// <summary>${IS6_DirectionType_attribute_none_D}</summary>
        None=-1
    }
}
