﻿
using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Json;
using System.Windows;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FindPathByPointsService_Title}</para>
    /// 	<para>${IS6_FindPathByPointsService_Description}</para>
    /// </summary>
    public class FindPathByPointsService : ServiceBase
    {
        /// <summary>${IS6_FindPathByPointsService_constructor_None_D}</summary>
        /// <overloads>${IS6_FindPathByPointsService_constructor_overloads_D}</overloads>
        public FindPathByPointsService()
        { }
        /// <summary>${IS6_FindPathByPointsService_constructor_String_D}</summary>
        /// <param name="url">${IS6_FindPathService_constructor_String_param_url}</param>
        public FindPathByPointsService(string url)
            : base(url)
        {
        }

        /// <summary>${IS6_FindPathByPointsService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_FindPathByPointsService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="parameters">${IS6_FindPathByPointsService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(FindPathByPointsParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_FindPathByPointsService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_FindPathByPointsService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_FindPathByPointsService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(FindPathByPointsParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("FindPathByPointsParameters is Null");
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }
            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(FindPathByPointsParameters parameters)
        {

            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "FindPathByPoints";
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);

            dictionary.Add("points", JsonHelper.FromPoint2DCollection(parameters.Points));
            dictionary.Add("routeParam",RouteParam.ToJson( parameters.RouteParam));

            dictionary.Add("trackingLayerIndex", "-1");
            dictionary.Add("userID", Guid.NewGuid().ToString());
            dictionary.Add("isHighlight", "false");

            return dictionary;
        }


        private RouteResult lastResult;

        /// <summary>${IS6_FindPathByPointsService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<RouteEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            RouteResult result = RouteResult.FromJson(jsonObject);
            LastResult = result;
            RouteEventArgs args = new RouteEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(RouteEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_FindPathByPointsService_attribute_lastResult_D}</summary>
        public RouteResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}

