﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_paramertersBase_Title}</para>
    /// 	<para>${IS6_ParamertersBase_Description}</para>
    /// </summary>
    public abstract class ParametersBase
    {
        /// <summary><para>${IS6_paramertersBase_attribute_mapName_D}</para></summary>
        public string MapName { get; set; }
    }
}
