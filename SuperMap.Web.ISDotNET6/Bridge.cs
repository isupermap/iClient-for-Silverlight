﻿
using System.Text;
using SuperMap.Web.Core;
using System.Globalization;
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_Utility_Title}</para>
    /// 	<para>${IS6_Utility_Description}</para>
    /// </summary>
    public static class Bridge
    {
        /// <summary>${IS6_Utility_method_GeometryToServerGeometry_D}</summary>
        /// <returns>${IS6_Utility_method_GeometryToServerGeometry_return}</returns>
        /// <param name="geo">${IS6_Utility_method_GeometryToServerGeometry_param_geo}</param>
        public static ServerGeometry ToServerGeometry(this Geometry geo)
        {
            if (geo == null)
            {
                return null;
            }

            ServerGeometry sg = new ServerGeometry();

            Point2DCollection list = new Point2DCollection();
            List<int> parts = new List<int>();

            if (geo is GeoRegion)
            {
                for (int i = 0; i < ((GeoRegion)geo).Parts.Count; i++)
                {
                    for (int j = 0; j < ((GeoRegion)geo).Parts[i].Count; j++)
                    {
                        list.Add(new Point2D(((GeoRegion)geo).Parts[i][j].X, ((GeoRegion)geo).Parts[i][j].Y));
                    }
                    parts.Add(((GeoRegion)geo).Parts[i].Count);
                }
                sg.Feature = ServerFeatureType.Polygon;
            }

            if (geo is GeoCircle)
            {
                for (int i = 0; i < ((GeoCircle)geo).Parts.Count; i++)
                {
                    for (int j = 0; j < ((GeoCircle)geo).Parts[i].Count; j++)
                    {
                        list.Add(new Point2D(((GeoCircle)geo).Parts[i][j].X, ((GeoCircle)geo).Parts[i][j].Y));
                    }
                    parts.Add(((GeoCircle)geo).Parts[i].Count);
                }
                sg.Feature = ServerFeatureType.Polygon;
            }

            if (geo is GeoLine)
            {
                for (int i = 0; i < ((GeoLine)geo).Parts.Count; i++)
                {
                    for (int j = 0; j < ((GeoLine)geo).Parts[i].Count; j++)
                    {
                        list.Add(new Point2D(((GeoLine)geo).Parts[i][j].X, ((GeoLine)geo).Parts[i][j].Y));
                    }
                    parts.Add(((GeoLine)geo).Parts[i].Count);
                }
                sg.Feature = ServerFeatureType.Line;
            }

            if (geo is GeoPoint)
            {
                list.Add(new Point2D(((GeoPoint)geo).X, ((GeoPoint)geo).Y));
                parts.Add(list.Count);
                sg.Feature = ServerFeatureType.Point;
            }

            sg.Point2Ds = list;
            sg.Parts = parts;
            sg.ID = -1;
            return sg;
        }

        /// <summary>${IS6_Utility_method_ToGeometry_D}</summary>
        /// <param name="geo">${IS6_Utility_method_ToGeometry_param_geo}</param>
        /// <returns>${IS6_Utility_method_ToGeometry_return}</returns>
        public static Geometry ToGeometry(this ServerGeometry geo)
        {
            if (geo == null)
            {
                return null;
            }

            if (geo.Feature == ServerFeatureType.Point)
            {
                return geo.ToGeoPoint();
            }
            else if (geo.Feature == ServerFeatureType.Polygon)
            {
                return geo.ToGeoRegion();
            }
            else if (geo.Feature == ServerFeatureType.Line)
            {
                return geo.ToGeoLine();
            }
            return null;
        }

    }
}
