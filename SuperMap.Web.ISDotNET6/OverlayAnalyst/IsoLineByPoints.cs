﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperMap.Web.Core;
using System.Windows;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoLineByPoints_Title}</para>
    /// 	<para>${IS6_IsoLineByPoints_Description}</para>
    /// </summary>
    public class IsoLineByPoints
    {
        /// <summary> ${IS6_IsoLineByPoints_constructor_D} </summary>
        public IsoLineByPoints() { }
        /// <summary> ${IS6_IsoLineByPoints_attribute_MapName_D} </summary>
        public string MapName { get; set; }
        /// <summary> ${IS6_IsoLineByPoints_attribute_Points_D} </summary>
        public Point2D[] Points { get; set; }
        /// <summary> ${IS6_IsoLineByPoints_attribute_ZValues_D} </summary>
        public double[] ZValues { get; set; }
        /// <summary> ${IS6_IsoLineByPoints_attribute_InterpolateType_D} </summary>
        public InterpolateType InterpolateType { get; set; }
        /// <summary> ${IS6_IsoLineByPoints_attribute_GridResolution_D} </summary>
        public double GridResolution { get; set; }
        /// <summary> ${IS6_IsoLineByPoints_attribute_IsoLineParam_D} </summary>
        public IsoLineParam IsoLineParam { get; set; }
        
        public InterpolateParam InterpolateParam { get; set; }

        internal static Dictionary<string, string> ParseClass(IsoLineByPoints parameters)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("method", "IsoLineByPointPositions");
            dic.Add("map", parameters.MapName);

            if (parameters.Points != null)
            {
                List<string> list = new List<string>();
                foreach (Point2D p in parameters.Points)
                {
                    list.Add("{" + string.Format("\"x\":{0},\"y\":{1}", p.X, p.Y) + "}");
                }
                dic.Add("pointPositions", "[" + string.Join(",", list.ToArray()) + "]");
            }

            if (parameters.ZValues != null)
            {
                dic.Add("pointValues", "[" + string.Join(",", parameters.ZValues) + "]");
            }
            if (parameters.InterpolateParam != null)
            {
                dic.Add("interpolateParam", InterpolateParam.ToJson(parameters.InterpolateParam));
            }
            else
            {
                dic.Add("gridResolution", parameters.GridResolution.ToString());
                dic.Add("interpolateType", (((int)Enum.Parse(typeof(InterpolateType), parameters.InterpolateType.ToString(), true)).ToString()));
            }
            
            dic.Add("isoLineParam", IsoLineParam.ToJson(parameters.IsoLineParam));
            return dic;
        }
    }

    /// <summary> ${IS6_InterpolateType_Title} </summary>
    public enum InterpolateType
    {
        /// <summary> ${IS6_InterpolateType_attribute_TIN_D} </summary>
        TIN = 1, //使用 TIN 方法插值。
        /// <summary> ${IS6_InterpolateType_attribute_IDW_D} </summary>
        IDW = 2, //使用 IDW 方法插值。
        /// <summary> ${IS6_InterpolateType_attribute_Krig_D} </summary>
        Krig = 3, //使用 Krig 方法插值。
        /// <summary> ${IS6_InterpolateType_attribute_BSpline_D} </summary>
        BSpline = 4, //使用 B 样条方法插值。  

    }
}
