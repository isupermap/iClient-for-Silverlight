﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using SuperMap.Web.Core;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoRegionByGridDataset_Title}</para>
    /// 	<para>${IS6_IsoLineEventArgs_Description}</para>
    /// </summary>
    public class IsoRegionByGridDataset
    {
        /// <summary> ${IS6_IsoRegionByGridDataset_constructor_D} </summary>
        public IsoRegionByGridDataset()
        { }
        /// <summary> ${IS6_IsoRegionByGridDataset_attribute_MapName_D} </summary>
        public string MapName { get; set; }
        /// <summary> ${IS6_IsoRegionByGridDataset_attribute_GridDatasetName_D} </summary>
        public string GridDatasetName { get; set; }
        ///// <summary> ${IS6_IsoRegionByGridDataset_attribute_ClipRegion_D} </summary>
        //public GeoRegion ClipRegion { get; set; }
        /// <summary> ${IS6_IsoRegionByGridDataset_attribute_IsoRegionParam_D} </summary>
        public IsoRegionParam IsoRegionParam { get; set; }

        internal static Dictionary<string, string> ParseClass(IsoRegionByGridDataset parameters)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("map", parameters.MapName);
            dic.Add("method", "IsoRegion");
            parameters.IsoRegionParam.GridDatasetName = parameters.GridDatasetName;
            //parameters.IsoRegionParam.ClipRegion = parameters.ClipRegion;
            dic.Add("isoRegionParam", IsoRegionParam.ToJson(parameters.IsoRegionParam));
            return dic;
        }
    }
}
