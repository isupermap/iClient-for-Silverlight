﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperMap.Web.Core;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary> ${IS6_InterpolateParam_Title} </summary>
    public class InterpolateParam
    {
        /// <summary> ${IS6_InterpolateParam_constructor_D} </summary>
        public InterpolateParam()
        {
            this.MaxPntCountInQuadNode = 50;
            this.MaxPntCountForInterpolation = 200;
            this.Tension = 40;
            this.Smooth = 0.1;
            this.Power = 2;
            this.Bounds = Rectangle2D.Empty;
        }
        /// <summary> ${IS6_InterpolateParam_attribute_Bounds_D} </summary>
        public Rectangle2D Bounds { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_CessSize_D} </summary>
        public double CellSize { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_Count_D} </summary>
        public int Count { get; set; }
        ///// <summary> ${IS6_InterpolateParam_attribute_DatasetName_D} </summary>
        public string DatasetName { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_Distance_D} </summary>
        public double Distance { get; set; }
        ///// <summary> ${IS6_InterpolateParam_attribute_FieldName_D} </summary>
        public string FieldName { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_InterpolateMode_D} </summary>
        public InterpolateOperateMode InterpolateMode { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_MaxPntCountForInterpolation_D} </summary>
        public int MaxPntCountForInterpolation { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_MaxPntCountInQuadNode_D} </summary>
        public int MaxPntCountInQuadNode { get; set; }
        ///// <summary> ${IS6_InterpolateParam_attribute_OverwriteIfExists_D} </summary>
        public bool OverwriteIfExists { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_Power_D} </summary>
        public double Power { get; set; }
        ///// <summary> ${IS6_InterpolateParam_attribute_ResultDataset_D} </summary>
        public string ResultDataset { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_Smooth_D} </summary>
        public double Smooth { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_Tension_D} </summary>
        public int Tension { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_VariantSearch_D} </summary>
        public bool VariantSearch { get; set; }
        /// <summary> ${IS6_InterpolateParam_attribute_VariogramMode_D} </summary>
        public VariogramMode VariogramMode { get; set; }

        internal static string ToJson(InterpolateParam parameters)
        {
            List<string> list = new List<string>();
            list.Add(string.Format("\"cellSize\":{0}", parameters.CellSize));
            list.Add(string.Format("\"dataset\":\"{0}\"", parameters.DatasetName));
            list.Add(string.Format("\"fieldName\":\"{0}\"", parameters.FieldName));
            list.Add(string.Format("\"variantSearch\":{0}", parameters.VariantSearch.ToString().ToLower()));
            list.Add(string.Format("\"count\":{0}", parameters.Count));

            list.Add(string.Format("\"distance\":{0}", parameters.Distance));
            list.Add(string.Format("\"resultDataset\":\"{0}\"", parameters.ResultDataset));
            list.Add(string.Format("\"overwriteIfExists\":{0}", parameters.OverwriteIfExists.ToString().ToLower()));
            list.Add(string.Format("\"interpolateMode\":{0}", (((int)Enum.Parse(typeof(InterpolateOperateMode), parameters.InterpolateMode.ToString(), true)).ToString())));
            list.Add(string.Format("\"power\":{0}", parameters.Power));
            list.Add(string.Format("\"varMode\":{0}", (((int)Enum.Parse(typeof(VariogramMode), parameters.VariogramMode.ToString(), true)).ToString())));
            list.Add(string.Format("\"maxPntCountInQuadNode\":{0}", parameters.MaxPntCountInQuadNode));
            list.Add(string.Format("\"maxPntCountForInterpolation\":{0}", parameters.MaxPntCountForInterpolation));
            list.Add(string.Format("\"tension\":{0}", parameters.Tension));
            list.Add(string.Format("\"smooth\":{0}", parameters.Smooth));
            if (!parameters.Bounds.IsEmpty)
            {
                string bounds = "{\"leftBottom\":{" + string.Format("\"x\":{0},\"y\":{1}", parameters.Bounds.Left, parameters.Bounds.Bottom) +
                    "},\"rightTop\":{" + string.Format("\"x\":{0},\"y\":{1}", parameters.Bounds.Right, parameters.Bounds.Top) + "}}";

                list.Add(string.Format("\"bounds\":{0}", bounds));
            }
            else
            {
                list.Add("\"bounds\":null");
            }
            string content = string.Join(",", list.ToArray());
            return "{" + content + "}";
        }
    }

    /// <summary> ${IS6_InterpolateOperateMode_Title} </summary>
    public enum InterpolateOperateMode
    {
        /// <summary> ${IS6_InterpolateOperateMode_attribute_IDW_D} </summary>
        IDW = 0, //距离反比权值内插法(Inverse Distance Weight)。
        /// <summary> ${IS6_InterpolateOperateMode_attribute_Krig_D} </summary>
        Krig = 1, //克吕金内插法。
        /// <summary> ${IS6_InterpolateOperateMode_attribute_KrigWithQuadTree_D} </summary>
        KrigWithQuadTree = 2, //利用四叉树的方法进行Kriging插值，这种方法适合于用来插值的点比较多的情况。  
        /// <summary> ${IS6_InterpolateOperateMode_attribute_RBF_D} </summary>
        RBF = 3, //利用径向基函数（Radial Basis Function）进行插值。
        /// <summary> ${IS6_InterpolateOperateMode_attribute_RBFWithQuadTree_D} </summary>
        RBFWithQuadTree = 4, //利用四叉树的方法进行RBF插值，这种方法适合于用来插值的点比较多的情况。  
    }
    /// <summary> ${IS6_VariogramMode_Title} </summary>
    public enum VariogramMode
    {
        /// <summary> ${IS6_VariogramMode_attribute_Spherical_D} </summary>
        Spherical = 1, //圆函数（Spherical Variogram Mode）。
        /// <summary> ${IS6_VariogramMode_attribute_Exponential_D} </summary>
        Exponential = 2, //指数函数（Exponential Variogram Mode）。  
    }
}
