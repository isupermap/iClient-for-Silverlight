﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoLineByPointDataset_Title}</para>
    /// 	<para>${IS6_IsoLineByPointDataset_Description}</para>
    /// </summary>
    public class IsoLineByPointDataset
    {
        /// <summary> ${IS6_IsoLineByPointDataset_constructor_D} </summary>
        public IsoLineByPointDataset() { }
        /// <summary> ${IS6_IsoLineByPointDataset_attribute_MapName_D} </summary>
        public string MapName { get; set; }
        /// <summary> ${IS6_IsoLineByPointDataset_attribute_PointDatasetName_D} </summary>
        public string PointDatasetName { get; set; }
        /// <summary> ${IS6_IsoLineByPointDataset_attribute_ZValueField_D} </summary>
        public string ZValueField { get; set; }
        /// <summary> ${IS6_IsoLineByPointDataset_attribute_InterpolateType_D} </summary>
        public InterpolateType InterpolateType { get; set; }
        /// <summary> ${IS6_IsoLineByPointDataset_attribute_GridResolution_D} </summary>
        public double GridResolution { get; set; }
        /// <summary> ${IS6_IsoLineByPointDataset_attribute_IsoLineParam_D} </summary>
        public IsoLineParam IsoLineParam { get; set; }

        public InterpolateParam InterpolateParam { get; set; }

        internal static Dictionary<string, string> ParseClass(IsoLineByPointDataset parameters)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("map", parameters.MapName);
            dic.Add("method", "IsoLineByPointDataset");
            dic.Add("pointDataset", parameters.PointDatasetName);
            dic.Add("pointValueField", parameters.ZValueField);
            if (parameters.InterpolateParam != null)
            {
                dic.Add("interpolateParam", InterpolateParam.ToJson(parameters.InterpolateParam));
            }
            else
            {
                dic.Add("gridResolution", parameters.GridResolution.ToString());
                dic.Add("interpolateType", (((int)Enum.Parse(typeof(InterpolateType), parameters.InterpolateType.ToString(), true)).ToString()));
            }
            dic.Add("isoLineParam", IsoLineParam.ToJson(parameters.IsoLineParam));
            return dic;
        }
    }
}
