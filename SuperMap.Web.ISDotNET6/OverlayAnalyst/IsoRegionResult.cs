﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Json;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoRegionResult_Title}</para>
    /// 	<para>${IS6_IsoRegionResult_Description}</para>
    /// </summary>
    public class IsoRegionResult
    {
        /// <summary> ${IS6_IsoRegionResult_constructor_D} </summary>
        public IsoRegionResult()
        { }
        /// <summary> ${IS6_IsoRegionResult_attribute_Message_D} </summary>
        public string Message { get; set; }
        /// <summary> ${IS6_IsoRegionResult_attribute_DatasetName_D} </summary>
        public string DatasetName { get; set; }
        /// <summary> ${IS6_IsoRegionResult_attribute_Succeed_D} </summary>
        public bool Succeed { get; set; }
        /// <summary> ${IS6_IsoRegionResult_attribute_IsoRegions_D} </summary>
        public List<Entity> IsoRegions { get; set; }

        internal static IsoRegionResult FromJson(System.Json.JsonObject jsonObject)
        {
            IsoRegionResult result = new IsoRegionResult();

            if (jsonObject != null)
            {
                if (jsonObject.ContainsKey("succeed"))
                {
                    result.Succeed = (bool)jsonObject["succeed"];
                }
                if (jsonObject.ContainsKey("resultDataset"))
                {
                    result.DatasetName = jsonObject["resultDataset"];
                }
                if (jsonObject.ContainsKey("message"))
                {
                    result.Message = jsonObject["message"];
                }
                if (jsonObject.ContainsKey("isoRegions") && jsonObject["isoRegions"] != null)
                {
                    List<Entity> list = new List<Entity>();
                    JsonArray array = (JsonArray)jsonObject["isoRegions"];
                    foreach (var item in array)
                    {
                        if (item != null)
                        {
                            JsonObject itemJson = (JsonObject)item;
                            list.Add(Entity.FromJson(itemJson));
                        }
                    }
                    result.IsoRegions = list;
                }
            }

            return result;
        }
    }
}
