﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SuperMap.Web.Core;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoRegionByPoints_Title}</para>
    /// 	<para>${IS6_IsoRegionByPoints_Description}</para>
    /// </summary>
    public class IsoRegionByPoints
    {
        /// <summary> ${IS6_IsoRegionByPoints_constructor_D} </summary>
        public IsoRegionByPoints()
        { }
        /// <summary> ${IS6_IsoRegionByPoints_attribute_MapName_D} </summary>
        public string MapName { get; set; }
        /// <summary> ${IS6_IsoRegionByPoints_attribute_Points_D} </summary>
        public Point2D[] Points { get; set; }
        /// <summary> ${IS6_IsoRegionByPoints_attribute_ZValues_D} </summary>
        public double[] ZValues { get; set; }
        /// <summary> ${IS6_IsoRegionByPoints_attribute_interpolateParam_D} </summary>
        public InterpolateParam interpolateParam { get; set; }
        /// <summary> ${IS6_IsoRegionByPoints_attribute_IsoRegionParam_D} </summary>
        public IsoRegionParam IsoRegionParam { get; set; }

        internal static Dictionary<string, string> ParseClass(IsoRegionByPoints parameters)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("map", parameters.MapName);
            dic.Add("method", "IsoRegionByPointPositions");
            if (parameters.Points != null)
            {
                List<string> list = new List<string>();
                foreach (Point2D p in parameters.Points)
                {
                    list.Add("{" + string.Format("\"x\":{0},\"y\":{1}", p.X, p.Y) + "}");
                }
                dic.Add("pointPositions", "[" + string.Join(",", list.ToArray()) + "]");
            }

            if (parameters.ZValues != null)
            {
                dic.Add("pointValues", "[" + string.Join(",", parameters.ZValues) + "]");
            }

            dic.Add("interpolateParam", InterpolateParam.ToJson(parameters.interpolateParam));
            dic.Add("isoRegionParam", IsoRegionParam.ToJson(parameters.IsoRegionParam));
            return dic;
        }
    }
}
