﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoLineByGridDataset_Title}</para>
    /// 	<para>${IS6_IsoLineByGridDataset_Description}</para>
    /// </summary>
    public class IsoLineByGridDataset
    {
        /// <summary> ${IS6_IsoLineByGridDataset_constructor_D} </summary>
        public IsoLineByGridDataset()
        { }
        /// <summary> ${IS6_IsoLineByGridDataset_attribute_MapName_D} </summary>
        public string MapName { get; set; }
        /// <summary> ${IS6_IsoLineByGridDataset_attribute_GridDatasetName_D} </summary>
        public string GridDatasetName { get; set; }
        /// <summary> ${IS6_IsoLineByGridDataset_attribute_IsoLineParam_D} </summary>
        public IsoLineParam IsoLineParam { get; set; }

        internal static Dictionary<string, string> ParseClass(IsoLineByGridDataset parameters)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("method", "IsoLineByGridDataset");
            dic.Add("map", parameters.MapName);
            dic.Add("gridDataset", parameters.GridDatasetName);
            dic.Add("isoLineParam", IsoLineParam.ToJson(parameters.IsoLineParam));
            return dic;
        }
    }
}
