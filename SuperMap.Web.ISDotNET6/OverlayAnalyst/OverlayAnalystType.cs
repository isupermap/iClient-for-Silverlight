﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_OverlayAnalystType_Title}</summary>
    public enum OverlayAnalystType
    {
        /// <summary>${IS6_OverlayAnalystType_attribute_Undefined_D}</summary>
        Undefined = 0,
        /// <summary>${IS6_OverlayAnalystType_attribute_Clip_D}</summary>
        Clip=1,
        /// <summary>${IS6_OverlayAnalystType_attribute_Erase_D}</summary>
        Erase=2,
        /// <summary>${IS6_OverlayAnalystType_attribute_Identity_D}</summary>
        Identity=3,
        /// <summary>${IS6_OverlayAnalystType_attribute_Intersect_D}</summary>
        Intersect=4,
        /// <summary>${IS6_OverlayAnalystType_attribute_SymmetricDifference_D}</summary>
        SymmetricDifference=5,
        /// <summary>${IS6_OverlayAnalystType_attribute_Union_D}</summary>
        Union=6
    }
}
