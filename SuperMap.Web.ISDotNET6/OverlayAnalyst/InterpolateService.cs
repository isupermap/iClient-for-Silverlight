﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using System.Windows.Browser;
using System.Json;
using SuperMap.Web.ISDotNET6.Resources;

namespace SuperMap.Web.ISDotNET6
{
	/// <summary>
    /// ${IS6_InterpolateService_Description}
    /// </summary>
    public class InterpolateService : ServiceBase
    {
		/// <summary> ${IS6_InterpolateService_constructor_D} </summary>
        public InterpolateService()
        { }
         /// <summary> ${IS6_InterpolateService_constructor_url_D} </summary>
        public InterpolateService(string url)
            : base(url)
        { }
		/// <summary> ${IS6_InterpolateService_method_ProcessAsync_D} </summary>
        /// <param name="parameters">${IS6_InterpolateService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(InterpolateParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary> ${IS6_InterpolateService_method_ProcessAsync_overloads_D} </summary>
        /// <param name="parameters">${IS6_InterpolateService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_InterpolateService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(InterpolateParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

                base.SubmitRequest(base.Url + "/path.ashx?",
                    InterpolateParameters.ParseClass(parameters),
                    new EventHandler<RequestEventArgs>(request_Completed), state, false);
            
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            InterpolateResult result = InterpolateResult.FromJson(jsonObject);
            LastResult = result;
            InterpolateEventArgs args = new InterpolateEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(InterpolateEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary> ${IS6_InterpolateService_event_ProcessCompleted_D} </summary>
        [ScriptableMember]
        public event EventHandler<InterpolateEventArgs> ProcessCompleted;

        private InterpolateResult lastResult;
		/// <summary> ${IS6_InterpolateService_attribute_lastResult_D} </summary>
        public InterpolateResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
    /// <summary>
    /// ${IS6_InterpolateEventArgs_Title}
    /// </summary>
    public class InterpolateEventArgs : ServiceEventArgs
    {
	   /// <summary> ${IS6_InterpolateEventArgs_constructor_D} </summary>
        /// <param name="result">${IS6_InterpolateEventArgs_constructor_param_result}</param>
        /// <param name="originResult">${IS6_InterpolateEventArgs_constructor_param_originResult}</param>
        /// <param name="token">${IS6_InterpolateEventArgs_constructor_param_token}</param>
        public InterpolateEventArgs(InterpolateResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
		/// <summary> ${IS6_InterpolateEventArgs_attribute_Result_D} </summary>
        public InterpolateResult Result { get; private set; }
        /// <summary> ${IS6_InterpolateEventArgs_attribute_OriginResult_D} </summary>
        public string OriginResult { get; private set; }
    }
}
