﻿
using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Json;
using System.Windows;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_OverlayAnalystService_Title}</para>
    /// 	<para>${IS6_OverlayAnalystService_Description}</para>
    /// </summary>
    public class OverlayAnalystService : ServiceBase
    {
        /// <summary>${IS6_OverlayAnalystService_constructor_None_D}</summary>
        /// <overloads>${IS6_OverlayAnalystService_constructor_overloads_D}</overloads>
        public OverlayAnalystService() { }

        /// <summary>${IS6_OverlayAnalystService_constructor_String_D}</summary>
        /// <param name="url">${IS6_OverlayAnalystService_constructor_String_param_url}</param>
        public OverlayAnalystService(string url) : base(url) { }

        /// <summary>${IS6_OverlayAnalystService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_OverlayAnalystService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(OverlayAnalystParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${IS6_OverlayAnalystService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_OverlayAnalystService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_OverlayAnalystService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(OverlayAnalystParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("OverlayParameters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);

        }

        private Dictionary<string, string> GetParameters(OverlayAnalystParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "OverlayAnalyst";
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);

            dictionary.Add("overlayParam", OverlayParam.ToJson(parameters.OverlayParm));

            return dictionary;
        }


        private OverlayAnalystResult lastResult;

        /// <summary>${IS6_OverlayAnalystService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<OverlayAnalystEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            OverlayAnalystResult result = OverlayAnalystResult.FromJson(jsonObject);
            LastResult = result;
            OverlayAnalystEventArgs args = new OverlayAnalystEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(OverlayAnalystEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_OverlayAnalystService_attribute_lastResult_D}</summary>
        public OverlayAnalystResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}