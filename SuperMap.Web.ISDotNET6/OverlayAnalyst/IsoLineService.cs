﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
using SuperMap.Web.Service;
using SuperMap.Web.ISDotNET6.Resources;
using System.Json;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoLineService_Title}</para>
    /// 	<para>${IS6_IsoLineService_Description}</para>
    /// </summary>
    public class IsoLineService : Service.ServiceBase
    {
        /// <summary> ${IS6_IsoLineService_constructor_D} </summary>
        public IsoLineService()
        { }
        /// <summary> ${IS6_IsoLineService_constructor_url_D} </summary>
        public IsoLineService(string url)
            : base(url)
        { }
        /// <summary> ${IS6_IsoLineService_method_ProcessAsync_D} </summary>
        /// <param name="parameters">${IS6_IsoLineService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(IsoLineParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary> ${IS6_IsoLineService_method_ProcessAsync_overloads_D} </summary>
        /// <param name="parameters">${IS6_IsoLineService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_IsoLineService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(IsoLineParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (parameters.IsoLineByGridDataset != null)
            {
                base.SubmitRequest(base.Url + "/path.ashx?",
                    IsoLineByGridDataset.ParseClass(parameters.IsoLineByGridDataset),
                    new EventHandler<RequestEventArgs>(request_Completed), state, false);
            }
            else if (parameters.IsoLineByPointDataset != null)
            {
                base.SubmitRequest(base.Url + "/path.ashx?",
                    IsoLineByPointDataset.ParseClass(parameters.IsoLineByPointDataset),
                    new EventHandler<RequestEventArgs>(request_Completed), state, false);
            }
            else if (parameters.IsoLineByPoints != null)
            {
                base.SubmitRequest(base.Url + "/path.ashx?",
                    IsoLineByPoints.ParseClass(parameters.IsoLineByPoints),
                    new EventHandler<RequestEventArgs>(request_Completed), state, false);
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            IsoLineResult result = IsoLineResult.FromJson(jsonObject);
            LastResult = result;
            IsoLineEventArgs args = new IsoLineEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(IsoLineEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <param>${IS6_IsoLineService_event_ProcessCompleted_D}</param>
        [ScriptableMember]       
        public event EventHandler<IsoLineEventArgs> ProcessCompleted;

        private IsoLineResult lastResult;
        /// <param>${IS6_IsoLineService_attribute_lastResult_D}</param>
        public IsoLineResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
    /// <summary>
    /// 	<para>${IS6_IsoLineEventArgs_Title}</para>
    /// 	<para>${IS6_IsoLineEventArgs_Description}</para>
    /// </summary>
    public class IsoLineEventArgs : ServiceEventArgs
    {
        /// <summary> ${IS6_IsoLineEventArgs_constructor_D} </summary>
        /// <param name="result">${IS6_IsoLineEventArgs_constructor_param_result}</param>
        /// <param name="originResult">${IS6_IsoLineEventArgs_constructor_param_originResult}</param>
        /// <param name="token">${IS6_IsoLineEventArgs_constructor_param_token}</param>
        public IsoLineEventArgs(IsoLineResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary> ${IS6_IsoLineEventArgs_attribute_Result_D} </summary>
        public IsoLineResult Result { get; private set; }
        /// <summary> ${IS6_IsoLineEventArgs_attribute_OriginResult_D} </summary>
        public string OriginResult { get; private set; }
    }
}
