﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_OverlayAnalystParameters_Title}</para>
    /// 	<para>${IS6_OverlayAnalystParameters_Description}</para>
    /// </summary>
    /// <remarks>${IS6_OverlayAnalystParameters_Remarks}</remarks>
    public class OverlayAnalystParameters : ParametersBase
    {
        /// <summary>${IS6_OverlayAnalystParameters_constructor_None_D}</summary>
        public OverlayAnalystParameters()
        { }
        /// <summary>${IS6_OverlayAnalystParameters_attribute_OverlayParam_D}</summary>
        public OverlayParam OverlayParm { get; set; }
    }
}
