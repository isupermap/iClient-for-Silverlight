﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperMap.Web.Core;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoLineParam_Title}</para>
    /// 	<para>${IS6_IsoLineParam_Description}</para>
    /// </summary>
    public class IsoLineParam
    {
        /// <summary> ${IS6_IsoLineParam_constructor_D} </summary>
        public IsoLineParam() { }
        /// <summary> ${IS6_IsoLineParam_attribute_BaseValue_D} </summary>
        public double BaseValue { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_Interval_D} </summary>
        public double Interval { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_Intervals_D} </summary>
        public double[] Intervals { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_OverwriteIfExists_D} </summary>
        public bool OverwriteIfExists { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_ResultDataset_D} </summary>
        public string ResultDataset { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_SaveDatasetAsCad_D} </summary>
        public bool SaveDatasetAsCad { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_SaveResultToDataset_D} </summary>
        public bool SaveResultToDataset { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_SmoothMethod_D} </summary>
        public SmoothMethod SmoothMethod { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_Smoothness_D} </summary>
        public int Smoothness { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_UseFastMethod_D} </summary>
        public bool UseFastMethod { get; set; }
        /// <summary> ${IS6_IsoLineParam_attribute_UseIntervals_D} </summary>
        public bool UseIntervals { get; set; }

        /// <summary>
        /// 裁剪区域。结果等值线数据集将只在裁剪区域内生成。如果不指定，则按照 Grid 或 DEM 数据集的范围生成。 可选字段。
        /// </summary>
        public GeoRegion ClipRegion { get; set; }

        internal static string ToJson(IsoLineParam parameters)
        {
            List<string> list = new List<string>();
            list.Add(string.Format("\"interval\":{0}", parameters.Interval));
            list.Add(string.Format("\"baseValue\":{0}", parameters.BaseValue));
            list.Add(string.Format("\"smoothMethod\":{0}", (((int)Enum.Parse(typeof(SmoothMethod), parameters.SmoothMethod.ToString(), true)).ToString())));
            list.Add(string.Format("\"smoothness\":{0}", parameters.Smoothness));
            list.Add(string.Format("\"useFastMethod\":{0}", parameters.UseFastMethod.ToString().ToLower()));
            list.Add(string.Format("\"saveResultToDataset\":{0}", parameters.SaveResultToDataset.ToString().ToLower()));
            list.Add(string.Format("\"resultDataset\":\"{0}\"", parameters.ResultDataset));
            list.Add(string.Format("\"saveDatasetAsCad\":{0}", parameters.SaveDatasetAsCad.ToString().ToLower()));
            list.Add(string.Format("\"overwriteIfExists\":{0}", parameters.OverwriteIfExists.ToString().ToLower()));

            if (parameters.Intervals != null && parameters.Intervals.Count() > 0)
            {
                list.Add(string.Format("\"intervals\":[{0}]", string.Join(",", parameters.Intervals)));
            }
            else
            {
                list.Add("\"intervals\":[]");
            }
            list.Add(string.Format("\"useIntervals\":{0}", parameters.UseIntervals.ToString().ToLower()));
            if (parameters.ClipRegion != null)
            {
                list.Add(string.Format("\"clipRegion\":{0}", ServerGeometry.ToJson(parameters.ClipRegion.ToServerGeometry())));
            }
            return "{" + string.Join(",", list.ToArray()) + "}";
        }
    }

    /// <summary>
    /// 	<para>${IS6_SmoothMethod_Title}</para>
    /// 	<para>${IS6_SmoothMethod_Description}</para>
    /// </summary>
    public enum SmoothMethod
    {
        /// <summary> ${IS6_SmoothMethod_attribute_BSpline_D} </summary>
        BSpline = 0,
        /// <summary> ${IS6_SmoothMethod_attribute_Polish_D} </summary>
        Polish = 1
    }
}
