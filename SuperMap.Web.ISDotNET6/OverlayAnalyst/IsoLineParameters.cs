﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary> ${IS6_IsoLineParameters_Title} </summary>
    public class IsoLineParameters
    {
        /// <summary> ${IS6_IsoLineParameters_constructor_D} </summary>
        public IsoLineParameters()
        { }
        /// <summary> ${IS6_IsoLineParameters_attribute_IsoLineByGrid_D} </summary>
        public IsoLineByGridDataset IsoLineByGridDataset { get; set; }
        /// <summary> ${IS6_IsoLineParameters_attribute_IsoLineByPoint_D} </summary>
        public IsoLineByPoints IsoLineByPoints { get; set; }
        /// <summary> ${IS6_IsoLineParameters_attribute_IsoLineByDataset_D} </summary>
        public IsoLineByPointDataset IsoLineByPointDataset { get; set; }
    }

}
