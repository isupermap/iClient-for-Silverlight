﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperMap.Web.Core;
using System.Json;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoLineResult_Title}</para>
    /// 	<para>${IS6_IsoLineResult_Description}</para>
    /// </summary>
    public class IsoLineResult
    {
        /// <summary> ${IS6_IsoLineResult_constructor_D} </summary>
        public IsoLineResult() { }
        /// <summary> ${IS6_IsoLineResult_attribute_IsoLineDataset_D} </summary>
        public string IsoLineDataset { get; set; }
        /// <summary> ${IS6_IsoLineResult_attribute_IsoLines_D} </summary>
        public List<Entity> IsoLines { get; set; }
        /// <summary> ${IS6_IsoLineResult_attribute_Message_D} </summary>
        public string Message { get; set; }
        /// <summary> ${IS6_IsoLineResult_attribute_Succeed_D} </summary>
        public bool Succeed { get; set; }

        internal static IsoLineResult FromJson(System.Json.JsonObject jsonObject)
        {
            IsoLineResult result = new IsoLineResult();
            if (jsonObject != null)
            {
                if (jsonObject.ContainsKey("isoLines") && jsonObject["isoLines"] != null)
                {
                    List<Entity> list = new List<Entity>();
                    JsonArray array = (JsonArray)jsonObject["isoLines"];
                    foreach (var item in array)
                    {
                        if (item != null)
                        {
                            JsonObject itemJson = (JsonObject)item;
                            list.Add(Entity.FromJson(itemJson));
                        }
                    }
                    result.IsoLines = list;
                }

                if (jsonObject.ContainsKey("isoLineDataset"))
                {
                    result.IsoLineDataset = jsonObject["isoLineDataset"];
                }

                if (jsonObject.ContainsKey("message"))
                {
                    result.Message = jsonObject["message"];
                }

                if (jsonObject.ContainsKey("succeed"))
                {
                    result.Succeed = (bool)jsonObject["succeed"];
                }
            }
            return result;

        }
    }
}
