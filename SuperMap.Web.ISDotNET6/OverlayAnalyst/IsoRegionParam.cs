﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperMap.Web.Core;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoRegionParam_Title}</para>
    /// 	<para>${IS6_IsoRegionParam_Description}</para>
    /// </summary>
    public class IsoRegionParam
    {
        /// <summary> ${IS6_IsoRegionParam_constructor_D} </summary>
        public IsoRegionParam()
        { SaveResultToDataset = true; }
        /// <summary> ${IS6_IsoRegionParam_attribute_BaseValue_D} </summary>
        public double BaseValue { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_ClipRegion_D} </summary>
        public GeoRegion ClipRegion { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_Interval_D} </summary>
        public double Interval { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_Intervals_D} </summary>
        public double[] Intervals { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_OverwriteIfExists_D} </summary>
        public bool OverwriteIfExists { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_ResultDataset_D} </summary>
        public string ResultDataset { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_SmoothMethod_D} </summary>
        public SmoothMethod SmoothMethod { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_Smoothness_D} </summary>
        public int Smoothness { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_Tolerance_D} </summary>
        public double Tolerance { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_UseIntervals_D} </summary>
        public bool UseIntervals { get; set; }
        /// <summary> ${IS6_IsoRegionParam_attribute_SaveResultToDataset_D} </summary>
        public bool SaveResultToDataset { get; set; }

        internal string GridDatasetName { get; set; }

        internal static string ToJson(IsoRegionParam parameters)
        {
            List<string> list = new List<string>();
            if (!string.IsNullOrEmpty(parameters.GridDatasetName))
            {
                list.Add(string.Format("\"gridDataset\":\"{0}\"", parameters.GridDatasetName));
            }
            list.Add(string.Format("\"interval\":{0}", parameters.Interval));
            list.Add(string.Format("\"baseValue\":{0}", parameters.BaseValue));
            list.Add(string.Format("\"smoothMethod\":{0}", (((int)Enum.Parse(typeof(SmoothMethod), parameters.SmoothMethod.ToString(), true)).ToString())));
            list.Add(string.Format("\"smoothness\":{0}", parameters.Smoothness));
            list.Add(string.Format("\"resultDataset\":\"{0}\"", parameters.ResultDataset));
            list.Add(string.Format("\"overwriteIfExists\":{0}", parameters.OverwriteIfExists.ToString().ToLower()));
            list.Add(string.Format("\"tolerance\":{0}", parameters.Tolerance));
            list.Add(string.Format("\"saveResultToDataset\":{0}", parameters.SaveResultToDataset.ToString().ToLower()));
            if (parameters.ClipRegion != null)
            {
                list.Add(string.Format("\"clipRegion\":{0}", ServerGeometry.ToJson(parameters.ClipRegion.ToServerGeometry())));
            }
            else
            {
                list.Add("\"clipRegion\":null");
            }

            if (parameters.Intervals != null && parameters.Intervals.Count() > 0)
            {
                list.Add(string.Format("\"intervals\":[{0}]", string.Join(",", parameters.Intervals)));
            }
            else
            {
                list.Add("\"intervals\":[]");
            }

            list.Add(string.Format("\"useIntervals\":{0}", parameters.UseIntervals.ToString().ToLower()));

            return "{" + string.Join(",", list.ToArray()) + "}";
        }
    }
}
