﻿
using SuperMap.Web.Core;
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_OverlayParam_Title}</para>
    /// 	<para>${IS6_OverlayParam_Description}</para>
    /// </summary>
    public class OverlayParam
    {
        /// <summary>${IS6_OverlayParam_constructor_D}</summary>
        public OverlayParam()
        {
            //OverwriteIfExists = true;
        }

        /// <summary>${IS6_DeleteEntityService_attribute_InLayer_D}</summary>
        public string InLayer { get; set; }

        /// <summary>${IS6_DeleteEntityService_attribute_OperateLayer_D}</summary>
        public string OperateLayer { get; set; }

        //这里需要将Geometry转成ServerGeometry
        //操作区域。只有当 operateLayer 为空时，本属性才会被使用。 
        /// <summary>${IS6_DeleteEntityService_attribute_OperateRegion_D}</summary>
        public Geometry OperateRegion { get; set; }

        /// <summary>${IS6_DeleteEntityService_attribute_Operation_D}</summary>
        public OverlayAnalystType Operation { get; set; }

        /// <summary>${IS6_DeleteEntityService_attribute_OverwriteIfExists_D}</summary>
        public bool OverwriteIfExists { get; set; }

        /// <summary>${IS6_DeleteEntityService_attribute_ResultName_D}</summary>
        public string ResultName { get; set; }


        internal static string ToJson(OverlayParam param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();


            list.Add(string.Format("\"operation\":\"{0}\"", (int)param.Operation));

            //InLayer
            if (!string.IsNullOrEmpty(param.InLayer))
            {
                list.Add(string.Format("\"inLayer\":\"{0}\"", param.InLayer));
            }
            else
            {
                list.Add("\"inLayer\":\"\"");
            }

            //OperateLayer
            if (!string.IsNullOrEmpty(param.OperateLayer))
            {
                list.Add(string.Format("\"operateLayer\":\"{0}\"", param.OperateLayer));
            }
            else
            {
                list.Add("\"operateLayer\":\"\"");
            }

            //Geometry=>ServerGeometry
            if (param.OperateRegion != null)
            {
                list.Add(string.Format("\"operateRegion\":{0}", ServerGeometry.ToJson(param.OperateRegion.ToServerGeometry())));
            }
            else
            {
                list.Add("\"operateRegion\":null");
            }


            //OverwriteIfExists
            list.Add(string.Format("\"overwriteIfExists\":{0}", param.OverwriteIfExists.ToString().ToLower()));


            //ResultName
            if (!string.IsNullOrEmpty(param.ResultName))
            {
                list.Add(string.Format("\"resultName\":\"{0}\"", param.ResultName));
            }
            else
            {
                list.Add("\"resultName\":\"\"");
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
