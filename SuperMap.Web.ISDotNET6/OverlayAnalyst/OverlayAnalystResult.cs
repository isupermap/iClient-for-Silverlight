﻿
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_OverlayAnalystResult_Title}</para>
    /// 	<para>${IS6_OverlayAnalystResult_Description}</para>
    /// </summary>
    public class OverlayAnalystResult
    {
        internal OverlayAnalystResult()
        { }

        /// <summary>${IS6_OverlayAnalystResult_attribute_Message_D}</summary>
        public string Message { get; private set; }

        /// <summary>${IS6_OverlayAnalystResult_attribute_ResultDataset_D}</summary>
        public string ResultDataset { get; private set; }

        /// <summary>${IS6_OverlayAnalystResult_attribute_Succeed_D}</summary>
        public bool Succeed { get; private set; }


        /// <summary>${IS6_OverlayAnalystResult_method_fromJSON_D}</summary>
        /// <returns>${IS6_OverlayAnalystResult_method_fromJSON_Return}</returns>
        /// <param name="jsonObject">${IS6_OverlayAnalystResult_method_fromJSON_param}</param>
        public static OverlayAnalystResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            OverlayAnalystResult result = new OverlayAnalystResult();

            result.Message = jsonObject["message"];

            result.ResultDataset = jsonObject["resultDataset"];

            result.Succeed = (bool)jsonObject["succeed"];

            return result;
        }
    }
}
