﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
using SuperMap.Web.Service;
using SuperMap.Web.ISDotNET6.Resources;
using System.Json;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_IsoRegionService_Title}</para>
    /// 	<para>${IS6_IsoRegionService_Description}</para>
    /// </summary>
    public class IsoRegionService : Service.ServiceBase
    {
        /// <summary> ${IS6_IsoRegionService_constructor_D} </summary>
        public IsoRegionService()
        { }
        /// <summary> ${IS6_IsoRegionService_constructor_url_D} </summary>
        public IsoRegionService(string url)
            : base(url)
        { }
        /// <summary> ${IS6_IsoRegionService_method_ProcessAsync_D} </summary>
        /// <param name="parameters">${IS6_IsoRegionService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(IsoRegionParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary> ${IS6_IsoRegionService_method_ProcessAsync_overloads_D} </summary>
        /// <param name="parameters">${IS6_IsoRegionService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_IsoRegionService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(IsoRegionParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            if (parameters.IsoRegionByGridDataset != null)
            {
                base.SubmitRequest(base.Url + "/path.ashx?",
                    IsoRegionByGridDataset.ParseClass(parameters.IsoRegionByGridDataset),
                    new EventHandler<RequestEventArgs>(request_Completed), state, false);
            }
            else if (parameters.IsoRegionByPoints != null)
            {
                base.SubmitRequest(base.Url + "/path.ashx?",
                    IsoRegionByPoints.ParseClass(parameters.IsoRegionByPoints),
                    new EventHandler<RequestEventArgs>(request_Completed), state, false);
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            IsoRegionResult result = IsoRegionResult.FromJson(jsonObject);
            LastResult = result;
            IsoRegionEventArgs args = new IsoRegionEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(IsoRegionEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary> ${IS6_IsoRegionService_event_ProcessCompleted_D} </summary>
        [ScriptableMember]       
        public event EventHandler<IsoRegionEventArgs> ProcessCompleted;

        private IsoRegionResult lastResult;
        /// <summary> ${IS6_IsoRegionService_attribute_lastResult_D} </summary>
        public IsoRegionResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
    /// <summary>
    /// 	<para>${IS6_IsoRegionEventArgs_Title}</para>
    /// 	<para>${IS6_IsoRegionEventArgs_Description}</para>
    /// </summary>
    public class IsoRegionEventArgs : ServiceEventArgs
    {
        /// <summary> ${IS6_IsoRegionEventArgs_constructor_D} </summary>
        /// <param name="result">${IS6_IsoRegionEventArgs_constructor_param_result}</param>
        /// <param name="originResult">${IS6_IsoRegionEventArgs_constructor_param_originResult}</param>
        /// <param name="token">${IS6_IsoRegionEventArgs_constructor_param_token}</param>
        public IsoRegionEventArgs(IsoRegionResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary> ${IS6_IsoRegionEventArgs_attribute_Result_D} </summary>
        public IsoRegionResult Result { get; private set; }
        /// <summary> ${IS6_IsoRegionEventArgs_attribute_OriginResult_D} </summary>
        public string OriginResult { get; private set; }
    }
}
