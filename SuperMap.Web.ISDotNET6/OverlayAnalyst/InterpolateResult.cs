﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// ${IS6_InterpolateResult_Title}
    /// </summary>
    public class InterpolateResult
    {
        /// <summary>
        /// ${IS6_InterpolateResult_constructor_D} 
        /// </summary>
        public InterpolateResult()
        {

        }

        /// <summary>
        /// ${IS6_InterpolateResult_attribute_Succeed_D}
        /// </summary>
        public bool Succeed { get; set; }

        /// <summary>
        /// ${IS6_InterpolateResult_attribute_ResultDataset_D}
        /// </summary>
        public string ResultDataset { get; set; }

        /// <summary>
        /// ${IS6_InterpolateResult_attribute_Message_D}
        /// </summary>
        public string Message { get; set; }

        internal static InterpolateResult FromJson(System.Json.JsonObject jsonObject)
        {
            InterpolateResult result = new InterpolateResult();

            if (jsonObject != null)
            {
                if (jsonObject.ContainsKey("succeed"))
                {
                    result.Succeed = (bool)jsonObject["succeed"];
                }
                if (jsonObject.ContainsKey("resultDataset"))
                {
                    result.ResultDataset = jsonObject["resultDataset"];
                }
                if (jsonObject.ContainsKey("message"))
                {
                    result.Message = jsonObject["message"];
                }
            }

            return result;
        }
    }
}
