﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_OverlayAnalystEventArgs_Title}</para>
    /// 	<para>${IS6_OverlayAnalystEventArgs_Description}</para>
    /// </summary>
    public class OverlayAnalystEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_OverlayAnalystEventArgs_constructor_None_D}</summary>
        public OverlayAnalystEventArgs(OverlayAnalystResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_OverlayAnalystEventArgs_attribute_result_D}</summary>
        public OverlayAnalystResult Result { get; private set; }
        /// <summary>${IS6_OverlayAnalystEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
