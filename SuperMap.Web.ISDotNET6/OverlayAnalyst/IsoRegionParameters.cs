﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary> ${IS6_IsoRegionParameters_Title} </summary>
    public class IsoRegionParameters
    {
        /// <summary> ${IS6_IsoRegionParameters_constructor_D} </summary>
        public IsoRegionParameters() { }
        /// <summary> ${IS6_IsoRegionParameters_attribute_IsoRegionByGridDataset_D} </summary>
        public IsoRegionByGridDataset IsoRegionByGridDataset { get; set; }
        /// <summary> ${IS6_IsoRegionParameters_attribute_IsoRegionByPoints_D} </summary>
        public IsoRegionByPoints IsoRegionByPoints { get; set; }
    }
}
