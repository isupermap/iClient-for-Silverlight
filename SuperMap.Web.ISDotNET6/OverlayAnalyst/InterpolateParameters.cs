﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary> ${IS6_InterpolateParameters_Title} </summary>
    
    public class InterpolateParameters
    {
        /// <summary> ${IS6_InterpolateParameters_constructor_D} </summary>
        public InterpolateParameters()
        {

        }

        /// <summary>
        /// ${IS6_InterpolateParameters_attribute_MapName_D}
        /// </summary>
        public string MapName { get; set; }

        /// <summary>
        ///  ${IS6_InterpolateParameters_attribute_InterpolateParam_D}
        /// </summary>
        public InterpolateParam InterpolateParam { get; set; }

        internal static Dictionary<string, string> ParseClass(InterpolateParameters parameters)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("method", "Interpolate");
            dic.Add("map", parameters.MapName);
            dic.Add("interpolateParam", InterpolateParam.ToJson(parameters.InterpolateParam));
            return dic;
        }
    }
}
