﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetBusSolutionResult_Title}</para>
    /// 	<para>${IS6_GetBusSolutionResult_Description}</para>
    /// </summary>
    public class GetBusSolutionResult
    {
        internal GetBusSolutionResult()
        {
        }

        /// <summary>${IS6_GetBusSolutionResult_constructor_D}</summary>
        public List<BusSolution> BusSolutions { get; private set; }


        /// <summary>${IS6_GetBusSolutionResult_method_fromJSON_D}</summary>
        /// <returns>${IS6_GetBusSolutionResult_method_fromJSON_Return}</returns>
        /// <param name="jsonArray">${IS6_GetBusSolutionResult_method_fromJSON_param}</param>
        public static GetBusSolutionResult FromJson(JsonArray jsonArray)
        {
            if (jsonArray != null && jsonArray.Count > 0)
            {
                GetBusSolutionResult result = new GetBusSolutionResult();
                result.BusSolutions = new List<BusSolution>();
                for (int i = 0; i < jsonArray.Count; i++)
                {
                    result.BusSolutions.Add(BusSolution.FromJson((JsonObject)jsonArray[i]));
                }

                return result;
            }
            return null;
        }
    }
}
