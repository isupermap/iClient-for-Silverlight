﻿


namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowBusStopParameters_Title}</para>
    /// 	<para>${IS6_ShowBusStopParameters_Description}</para>
    /// </summary>
    public class ShowBusStopParameters : ParametersBase
    {
        /// <summary>${IS6_ShowBusStopParameters_constructor_D}</summary>
        /// <overloads>${IS6_ShowBusStopParameters_constructor_overloads_D}</overloads>
        public ShowBusStopParameters()
        { }

        /// <summary>${IS6_ShowBusStopParameters_constructor_String_D}</summary>
        /// <param name="mapName">${IS6_ShowBusStopParameters_constructor_param_MapName}</param>
        /// <param name="id">${IS6_ShowBusStopParameters_constructor_param_ID}</param>
        public ShowBusStopParameters(string mapName, int id)
        {
            MapName = mapName;
            ID = id;
        }

        /// <summary>${IS6_ShowBusStopParameters_attribute_ID_D}</summary>
        public int ID { get; set; }
    }
}
