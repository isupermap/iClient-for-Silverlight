﻿
using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetBusLineResult_Title}</para>
    /// 	<para>${IS6_GetBusLineResult_Description}</para>
    /// </summary>
    public class GetBusLineResult
    {
        internal GetBusLineResult()
        { }

        /// <summary>${IS6_GetBusLineResult_attribute_BusLines_D}</summary>
        public List<BusLine> BusLines { get; private set; }


        /// <summary>${IS6_GetBusLineResult_method_fromJSON_D}</summary>
        /// <returns>${IS6_GetBusLineResult_method_fromJSON_Return}</returns>
        /// <param name="json">${IS6_GetBusLineResult_method_fromJSON_param}</param>
        public static GetBusLineResult FromJson(JsonArray json)
        {
            if (json != null && json.Count > 0)
            {
                GetBusLineResult result = new GetBusLineResult();
                result.BusLines = new List<BusLine>();

                for (int i = 0; i < json.Count; i++)
                {
                    result.BusLines.Add(BusLine.FromJson((JsonObject)json[i]));
                }

                return result;
            }
            return null;
        }
    }
}
