﻿


namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowRoutingParameters_Title}</para>
    /// 	<para>${IS6_ShowRoutingParameters_Description}</para>
    /// </summary>
    public class ShowRoutingParameters : ParametersBase
    {
        /// <summary>${IS6_ShowRoutingParameters_constructor_D}</summary>
        public ShowRoutingParameters()
        { }

        /// <summary>${IS6_ShowRoutingParameters_attribute_BusRouting_D}</summary>
        public BusRouting BusRouting { get; set; }
    }
}
