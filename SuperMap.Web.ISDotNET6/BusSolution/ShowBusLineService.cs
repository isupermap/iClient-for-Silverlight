﻿


using SuperMap.Web.Service;
using System;
using SuperMap.Web.ISDotNET6.Resources;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using System.Windows;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowBusLineService_Title}</para>
    /// 	<para>${IS6_ShowBusLineService_Description}</para>
    /// </summary>
    public class ShowBusLineService : ServiceBase
    {
        /// <summary>${IS6_ShowBusLineService_constructor_D}</summary>
        /// <overloads>${IS6_ShowBusLineService_constructor_overloads_D}</overloads>
        public ShowBusLineService() { }

        /// <summary>${IS6_ShowBusLineService_constructor_String_D}</summary>
        /// <param name="url">${IS6_ShowBusLineService_constructor_param_url}</param>
        public ShowBusLineService(string url) : base(url) { }

        /// <summary>${IS6_ShowBusLineService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_ShowBusLineService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(ShowBusLineParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${IS6_ShowBusLineService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_ShowBusLineService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${IS6_ShowBusLineService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(ShowBusLineParameters parameters, object state)
        {
            if (parameters == null)
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);

            if (string.IsNullOrEmpty(this.Url))
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);

            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(ShowBusLineParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "ShowBusLine";
            dictionary.Add("method", method);
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("id", parameters.ID.ToString());

            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject json = (JsonObject)JsonObject.Parse(e.Result);

            ShowBusLineResult result = ShowBusLineResult.FromJson(json);
            LastResult = result;
            ShowBusLineEventArgs args = new ShowBusLineEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);

        }

        private void OnProcessCompleted(ShowBusLineEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_ShowBusLineService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ShowBusLineEventArgs> ProcessCompleted;

        private ShowBusLineResult lastResult;
        /// <summary>${IS6_ShowBusLineService_attribute_LastResult_D}</summary>
        public ShowBusLineResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
