﻿


namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowBusLineParameters_Title}</para>
    /// 	<para>${IS6_ShowBusLineParameters_Description}</para>
    /// </summary>
    public class ShowBusLineParameters:ParametersBase
    {
        /// <summary>${IS6_ShowBusLineParameters_constructor_D}</summary>
        /// <overloads>${IS6_GetBusSolutionService_constructor_overloads_D}</overloads>
        public ShowBusLineParameters()
        { }

        /// <summary>${IS6_ShowBusLineParameters_constructor_String_D}</summary>
        /// <param name="mapName">${IS6_GetBusLineParameters_constructor_param_MapName}</param>
        /// <param name="id">${IS6_GetBusLineParameters_constructor_param_ID}</param>
        public ShowBusLineParameters(string mapName, int id)
        {
            MapName = mapName;
            ID = id;
        }

        /// <summary>${IS6_ShowBusLineParameters_attribute_ID_D}</summary>
        public int ID { get; set; }
    }
}
