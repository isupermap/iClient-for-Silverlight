﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetBusSolutionParameters_Title}</para>
    /// 	<para>${IS6_GetBusSolutionParameters_Description}</para>
    /// </summary>
    public class GetBusSolutionParameters : ParametersBase
    {
        /// <summary>${IS6_GetBusSolutionParameters_constructor_D}</summary>
        public GetBusSolutionParameters()
        {
        }
        //将IDs和Names都开出来，但是以IDs为准，
        //当设置了IDs时，Names就不起作用了，
        //当没有设置IDs时，Names才起作用；
        /// <summary>${IS6_GetBusSolutionParameters_attribute_IDs_D}</summary>
        public IList<int> IDs { get; set; }

        /// <summary>${IS6_GetBusSolutionParameters_attribute_Names_D}</summary>
        public IList<string> Names { get; set; }

        /// <summary>${IS6_GetBusSolutionParameters_attribute_BusSolutionParam_D}</summary>
        public BusSolutionParam BusSolutionParam { get; set; }

       
    }
}
