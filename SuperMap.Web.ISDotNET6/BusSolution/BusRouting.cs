﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_BusRouting_Title}</para>
    /// 	<para>${IS6_BusRouting_Description}</para>
    /// </summary>
    public class BusRouting
    {
        /// <summary>${IS6_BusRouting_constructor_D}</summary>
        public BusRouting()
        { }

        /// <summary>${IS6_BusRouting_attribute_BusLines_D}</summary>
        public IList<BusLine> BusLines { get; set; }
        /// <summary>${IS6_BusRouting_attribute_UpStops_D}</summary>
        public IList<BusStop> UpStops { get; set; }
        /// <summary>${IS6_BusRouting_attribute_DownStops_D}</summary>
        public IList<BusStop> DownStops { get; set; }
        /// <summary>${IS6_BusRouting_attribute_Distance_D}</summary>
        public double Distance { get; set; }
        /// <summary>${IS6_BusRouting_attribute_Time_D}</summary>
        public double Time { get; set; }
        /// <summary>${IS6_BusRouting_attribute_PartsAngle_D}</summary>
        public IList<double> PartsAngle { get; set; }
        /// <summary>${IS6_BusRouting_attribute_PartsDistance_D}</summary>
        public IList<double> PartsDistance { get; set; }


        internal static string ToJson(BusRouting param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            if (param.BusLines != null && param.BusLines.Count > 0)
            {
                List<string> bs = new List<string>();
                for (int i = 0; i < param.BusLines.Count; i++)
                {
                    bs.Add(BusLine.ToJson(param.BusLines[i]));
                }
                list.Add(string.Format("\"busLines\":[{0}]", string.Join(",", bs.ToArray())));
            }

            if (param.UpStops != null && param.UpStops.Count > 0)
            {
                List<string> us = new List<string>();
                for (int i = 0; i < param.UpStops.Count; i++)
                {
                    us.Add(BusStop.ToJson(param.UpStops[i]));
                }
                list.Add(string.Format("\"upStops\":[{0}]", string.Join(",", us.ToArray())));
            }

            if (param.DownStops != null && param.DownStops.Count > 0)
            {
                List<string> ds = new List<string>();
                for (int i = 0; i < param.DownStops.Count; i++)
                {
                    ds.Add(BusStop.ToJson(param.DownStops[i]));
                }
                list.Add(string.Format("\"downStops\":[{0}]", string.Join(",", ds.ToArray())));
            }

            list.Add(string.Format("\"distance\":{0}", param.Distance));
            list.Add(string.Format("\"time\":{0}", param.Time));


            if (param.PartsAngle != null && param.PartsAngle.Count > 0)
            {
                List<string> pa = new List<string>();
                for (int i = 0; i < param.PartsAngle.Count; i++)
                {
                    pa.Add(param.PartsAngle[i].ToString());
                }
                list.Add(string.Format("\"partsAngle\":[{0}]", string.Join(",", pa.ToArray())));
            }

            if (param.PartsDistance != null && param.PartsDistance.Count > 0)
            {
                List<string> pa = new List<string>();
                for (int i = 0; i < param.PartsDistance.Count; i++)
                {
                    pa.Add(param.PartsDistance[i].ToString());
                }
                list.Add(string.Format("\"partsDistance\":[{0}]", string.Join(",", pa.ToArray())));
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }


        /// <summary>${IS6_BusRouting_method_fromJSON_D}</summary>
        /// <returns>${IS6_BusRouting_method_fromJSON_Return}</returns>
        /// <param name="jsonObject">${IS6_BusRouting_method_fromJSON_param}</param>
        public static BusRouting FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            BusRouting result = new BusRouting();

            if (jsonObject["busLines"] != null)
            {
                result.BusLines = new List<BusLine>();
                for (int i = 0; i < jsonObject["busLines"].Count; i++)
                {
                    result.BusLines.Add(BusLine.FromJson((JsonObject)jsonObject["busLines"][i]));
                }
            }

            if (jsonObject["upStops"] != null)
            {
                result.UpStops = new List<BusStop>();
                for (int i = 0; i < jsonObject["upStops"].Count; i++)
                {
                    result.UpStops.Add(BusStop.FromJson((JsonObject)jsonObject["upStops"][i]));
                }
            }

            if (jsonObject["downStops"] != null)
            {
                result.DownStops = new List<BusStop>();
                for (int i = 0; i < jsonObject["downStops"].Count; i++)
                {
                    result.DownStops.Add(BusStop.FromJson((JsonObject)jsonObject["downStops"][i]));
                }
            }

            result.Distance = jsonObject["distance"];
            result.Time = jsonObject["time"];

            if (jsonObject["partsAngle"] != null)
            {
                result.PartsAngle = new List<double>();
                for (int i = 0; i < jsonObject["partsAngle"].Count; i++)
                {
                    result.PartsAngle.Add((double)jsonObject["partsAngle"][i]);
                }
            }

            if (jsonObject["partsDistance"] != null)
            {
                result.PartsDistance = new List<double>();
                for (int i = 0; i < jsonObject["partsDistance"].Count; i++)
                {
                    result.PartsDistance.Add((double)jsonObject["partsDistance"][i]);
                }
            }

            return result;
        }

    }
}
