﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_BusSolutionParam_Title}</para>
    /// 	<para>${IS6_BusSolutionParam_Description}</para>
    /// </summary>
    public class BusSolutionParam
    {
        /// <summary>${IS6_BusSolutionParam_constructor_D}</summary>
        public BusSolutionParam()
        { }

        /// <summary>${IS6_BusSolutionParam_attribute_Expected_D}</summary>
        public int Expected { get; set; }

        /// <summary>${IS6_BusSolutionParam_attribute_Most_D}</summary>
        public bool Most { get; set; }

        /// <summary>${IS6_BusSolutionParam_attribute_OrderMode_D}</summary>
        public BusSolutionOrderMode OrderMode { get; set; }

        /// <summary>${IS6_BusSolutionParam_attribute_TransferTimes_D}</summary>
        public int TransferTimes { get; set; }

        /// <summary>${IS6_BusSolutionParam_attribute_ReturnPoints_D}</summary>
        public bool ReturnPoints { get; set; }

        internal static string ToJson(BusSolutionParam param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"expected\":{0}", param.Expected));
            list.Add(string.Format("\"most\":{0}", param.Most.ToString().ToLower()));
            list.Add(string.Format("\"orderMode\":{0}", (int)param.OrderMode));
            list.Add(string.Format("\"transferTimes\":{0}", param.TransferTimes));
            list.Add(string.Format("\"returnPoints\":{0}", param.ReturnPoints.ToString().ToLower()));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
