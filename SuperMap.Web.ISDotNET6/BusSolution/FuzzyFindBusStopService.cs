﻿

using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FuzzyFindBusStopService_Title}</para>
    /// 	<para>${IS6_FuzzyFindBusStopService_Description}</para>
    /// </summary>
    public class FuzzyFindBusStopService : ServiceBase
    {
        /// <summary>${IS6_FuzzyFindBusStopService_constructor_D}</summary>
        /// <overloads>${IS6_FuzzyFindBusStopService_constructor_overloads_D}</overloads>
        public FuzzyFindBusStopService() { }

        /// <summary>${IS6_FuzzyFindBusStopService_constructor_String_D}</summary>
        /// <param name="url">${IS6_FuzzyFindBusStopService_constructor_param_url}</param>
        public FuzzyFindBusStopService(string url) : base(url) { }


        /// <summary>${IS6_FuzzyFindBusStopService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_FuzzyFindBusStopService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(FuzzyFindBusStopParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_FuzzyFindBusStopService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_FuzzyFindBusStopService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${IS6_FuzzyFindBusStopService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(FuzzyFindBusStopParameters parameters, object state)
        {
            if (parameters == null)
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);

            if (string.IsNullOrEmpty(this.Url))
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);

            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(FuzzyFindBusStopParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "FuzzyFindBusStop";
            dictionary.Add("method", method);
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("fuzzyStopName", parameters.FuzzyStopName);

            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonArray jsonArray = (JsonArray)JsonObject.Parse(e.Result);

            FuzzyFindBusStopResult result = FuzzyFindBusStopResult.FromJson(jsonArray);
            LastResult = result;
            FuzzyFindBusStopEventArgs args = new FuzzyFindBusStopEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(FuzzyFindBusStopEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        /// <summary>${IS6_FuzzyFindBusStopService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FuzzyFindBusStopEventArgs> ProcessCompleted;

        private FuzzyFindBusStopResult lastResult;
        /// <summary>${IS6_FuzzyFindBusStopService_attribute_LastResult_D}</summary>
        public FuzzyFindBusStopResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
