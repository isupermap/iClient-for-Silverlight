﻿



using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowBusLineEventArgs_Title}</para>
    /// 	<para>${IS6_ShowBusLineEventArgs_Description}</para>
    /// </summary>
    public class ShowBusLineEventArgs:ServiceEventArgs
    {
        /// <summary>${IS6_ShowBusLineEventArgs_constructor_D}</summary>
        public ShowBusLineEventArgs(ShowBusLineResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_ShowBusLineEventArgs_attribute_Result_D}</summary>
        public ShowBusLineResult Result { get; private set; }
        /// <summary>${IS6_ShowBusLineEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
