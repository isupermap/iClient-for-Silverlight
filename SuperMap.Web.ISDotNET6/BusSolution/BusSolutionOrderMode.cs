﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_BusSolutionOrderMode_Title}</para>
    /// 	<para>${IS6_BusSolutionOrderMode_Description}</para>
    /// </summary>
    public enum BusSolutionOrderMode
    {
        /// <summary>${IS6_BusSolutionOrderMode_attribute_ByDistance_D}</summary>
        ByDistance = 0,
        /// <summary>${IS6_BusSolutionOrderMode_attribute_ByTime_D}</summary>
        ByTime = 1
    }
}
