﻿

using SuperMap.Web.Service;
using System;
using SuperMap.Web.ISDotNET6.Resources;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetBusLineService_Title}</para>
    /// 	<para>${IS6_GetBusLineService_Description}</para>
    /// </summary>
    public class GetBusLineService : ServiceBase
    {
        /// <summary>${IS6_GetBusLineServiceconstructor_D}</summary>
        /// <overloads>${IS6_GetBusLineService_constructor_overloads_D}</overloads>
        public GetBusLineService()
        { }
        /// <summary>${IS6_FuzzyFindBusStopService_constructor_String_D}</summary>
        /// <param name="url">${IS6_GetBusLineService_constructor_param_url}</param>
        public GetBusLineService(string url)
            : base(url)
        { }

        /// <summary>${IS6_GetBusLineService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_GetBusLineService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(GetBusLineParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${IS6_GetBusLineService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_GetBusLineService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${IS6_GetBusLineService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GetBusLineParameters parameters, object state)
        {
            if (parameters == null)
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);

            if (string.IsNullOrEmpty(this.Url))
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);

            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(GetBusLineParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (parameters.InterchangeID != int.MinValue)
            {
                string method = "GetBusLinesByStopID";
                dictionary.Add("method", method);
                dictionary.Add("map", parameters.MapName);
                dictionary.Add("interchangeID", parameters.InterchangeID.ToString());
            }
            else
            {
                string method = "GetBusLinesByStopName";
                dictionary.Add("method", method);
                dictionary.Add("map", parameters.MapName);
                dictionary.Add("stopName", parameters.StopName);
            }

            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonArray jsonArray = (JsonArray)JsonObject.Parse(e.Result);

            GetBusLineResult result = GetBusLineResult.FromJson(jsonArray);
            LastResult = result;
            GetBusLineEventArgs args = new GetBusLineEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(GetBusLineEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_GetBusLineService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetBusLineEventArgs> ProcessCompleted;

        private GetBusLineResult lastResult;
        /// <summary>${IS6_GetBusLineService_attribute_LastResult_D}</summary>
        public GetBusLineResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
