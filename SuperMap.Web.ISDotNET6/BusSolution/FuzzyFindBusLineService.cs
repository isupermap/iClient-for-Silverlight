﻿

using SuperMap.Web.Service;
using System;
using SuperMap.Web.ISDotNET6.Resources;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Json;
using System.Windows;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FuzzyFindBusLineService_Title}</para>
    /// 	<para>${IS6_FuzzyFindBusLineService_Description}</para>
    /// </summary>
    public class FuzzyFindBusLineService : ServiceBase
    {
        /// <summary>${IS6_FuzzyFindBusLineService_constructor_D}</summary>
        /// <overloads>${IS6_FuzzyFindBusLineService_constructor_overloads_D}</overloads>
        public FuzzyFindBusLineService() { }

        /// <summary>${IS6_FuzzyFindBusLineService_constructor_String_D}</summary>
        /// <param name="url">${IS6_FuzzyFindBusLineService_constructor_param_url}</param>
        public FuzzyFindBusLineService(string url) : base(url) { }


        /// <summary>${IS6_FuzzyFindBusLineService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_FuzzyFindBusLineService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(FuzzyFindBusLineParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_FuzzyFindBusLineService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_FuzzyFindBusLineService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${IS6_FuzzyFindBusLineService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(FuzzyFindBusLineParameters parameters, object state)
        {
            if (parameters == null)
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);

            if (string.IsNullOrEmpty(this.Url))
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);

            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(FuzzyFindBusLineParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "FuzzyFindBusLine";
            dictionary.Add("method", method);
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("fuzzyBusLineName", parameters.FuzzyBusLineName);

            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonArray jsonArray = (JsonArray)JsonObject.Parse(e.Result);

            FuzzyFindBusLineResult result = FuzzyFindBusLineResult.FromJson(jsonArray);
            LastResult = result;
            FuzzyFindBusLineEventArgs args = new FuzzyFindBusLineEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(FuzzyFindBusLineEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        /// <summary>${IS6_FuzzyFindBusLineService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FuzzyFindBusLineEventArgs> ProcessCompleted;

        private FuzzyFindBusLineResult lastResult;
        /// <summary>${IS6_FuzzyFindBusLineService_attribute_LastResult_D}</summary>
        public FuzzyFindBusLineResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
