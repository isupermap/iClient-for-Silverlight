﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowRoutingEventArgs_Title}</para>
    /// 	<para>${IS6_ShowRoutingEventArgs_Description}</para>
    /// </summary>
    public class ShowRoutingEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_ShowRoutingEventArgs_constructor_D}</summary>
        public ShowRoutingEventArgs(ShowRoutingResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_ShowRoutingEventArgs_attribute_Result_D}</summary>
        public ShowRoutingResult Result { get; private set; }
        /// <summary>${IS6_ShowRoutingEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
