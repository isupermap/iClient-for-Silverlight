﻿
using SuperMap.Web.Core;
using System.Json;
using System.Collections.Generic;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_BusStop_Title}</summary>
    public class BusStop
    {
        /// <summary>${IS6_BusStop_constructor_D}</summary>
        public BusStop()
        {
        }

        /// <summary>${IS6_BusStop_attribute_StopName_D}</summary>
        public string StopName { get; set; }
        /// <summary>${IS6_BusStop_attribute_SmID_D}</summary>
        public int SmID { get; set; }
        /// <summary>${IS6_BusStop_attribute_StopID_D}</summary>
        public int StopID { get; set; }
        /// <summary>${IS6_BusStop_attribute_Location_D}</summary>
        public Point2D Location { get; set; }


        internal static string ToJson(BusStop param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"stopName\":\"{0}\"", param.StopName));
            list.Add(string.Format("\"smId\":{0}", param.SmID));
            list.Add(string.Format("\"stopId\":{0}", param.StopID));
            list.Add(string.Format("\"Location\":{0}", JsonHelper.FromPoint2D(param.Location)));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }


        /// <summary>${IS6_BusStop_method_fromJSON_D}</summary>
        /// <returns>${IS6_BusStop_method_fromJSON_Return}</returns>
        /// <param name="jsonObject">${IS6_BusStop_method_fromJSON_param}</param>
        public static BusStop FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            BusStop result = new BusStop();

            result.StopName = jsonObject["stopName"];
            result.SmID = jsonObject["smId"];
            result.StopID = jsonObject["stopId"];
            result.Location = new Point2D((double)jsonObject["Location"]["x"], (double)jsonObject["Location"]["y"]);

            return result;
        }
    }
}
