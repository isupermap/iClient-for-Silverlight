﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetBusLineEventArgs_Title}</para>
    /// 	<para>${IS6_GetBusLineEventArgs_Description}</para>
    /// </summary>
    public class GetBusLineEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_GetBusLineEventArgs_constructor_D}</summary>
        public GetBusLineEventArgs(GetBusLineResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_GetBusLineEventArgs_attribute_Result_D}</summary>
        public GetBusLineResult Result { get; private set; }
        /// <summary>${IS6_GetBusLineEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
