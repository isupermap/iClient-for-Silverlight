﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FuzzyFindBusStopEventArgs_Title}</para>
    /// 	<para>${IS6_FuzzyFindBusStopEventArgs_Description}</para>
    /// </summary>
    public class FuzzyFindBusStopEventArgs:ServiceEventArgs
    {
        /// <summary>${IS6_FuzzyFindBusStopEventArgs_constructor_D}</summary>
        public FuzzyFindBusStopEventArgs(FuzzyFindBusStopResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_FuzzyFindBusStopEventArgs_attribute_Result_D}</summary>
        public FuzzyFindBusStopResult Result { get; private set; }
        /// <summary>${IS6_FuzzyFindBusStopEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
