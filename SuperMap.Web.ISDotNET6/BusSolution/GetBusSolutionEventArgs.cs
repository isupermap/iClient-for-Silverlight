﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetBusSolutionEventArgs_Title}</para>
    /// 	<para>${IS6_GetBusSolutionEventArgs_Description}</para>
    /// </summary>
    public class GetBusSolutionEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_GetBusSolutionEventArgs_constructor_D}</summary>
        public GetBusSolutionEventArgs(GetBusSolutionResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_GetBusSolutionEventArgs_attribute_Result_D}</summary>
        public GetBusSolutionResult Result { get; private set; }
        /// <summary>${IS6_GetBusSolutionEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }

    }
}
