﻿



using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowBusLineResult_Title}</para>
    /// 	<para>${IS6_ShowBusLineResult_Description}</para>
    /// </summary>
    public class ShowBusLineResult
    {
        internal ShowBusLineResult()
        { }

        /// <summary>${IS6_ShowBusLineResult_attribute_BusLine_D}</summary>
        public BusLine BusLine { get; private set; }


        /// <summary>${IS6_ShowBusLineResult_method_fromJSON_D}</summary>
        /// <returns>${IS6_ShowBusLineResult_method_fromJSON_Return}</returns>
        /// <param name="json">${IS6_ShowBusLineResult_method_fromJSON_param}</param>
        public static ShowBusLineResult FromJson(JsonObject json)
        {
            if (json != null)
            {
                return new ShowBusLineResult { BusLine = BusLine.FromJson(json) };
            }
            return null;
        }
    }
}
