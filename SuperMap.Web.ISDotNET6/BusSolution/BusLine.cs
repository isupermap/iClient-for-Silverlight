﻿
using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_BusLine_Title}</para>
    /// 	<para>${IS6_BusLine_Description}</para>
    /// </summary>
    public class BusLine
    {
        /// <summary>${IS6_BusLine_constructor_None_D}</summary>
        public BusLine()
        {
        }

        /// <summary>${IS6_BusLine_attribute_LineName_D}</summary>
        public string LineName { get; set; }
        /// <summary>${IS6_BusLine_attribute_SmID_D}</summary>
        public int SmID { get; set; }
        /// <summary>${IS6_BusLine_attribute_LineID_D}</summary>
        public int LineID { get; set; }
        /// <summary>${IS6_BusLine_attribute_DirectionSign_D}</summary>
        public string DirectionSign { get; set; }
        /// <summary>${IS6_BusLine_attribute_Points_D}</summary>
        public Point2DCollection Points { get; set; }

        internal static string ToJson(BusLine param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"lineName\":\"{0}\"", param.LineName));
            list.Add(string.Format("\"smId\":{0}", param.SmID));
            list.Add(string.Format("\"lineId\":{0}", param.LineID));

            if (param.DirectionSign != null)
            {
                list.Add(string.Format("\"directionSign\":\"{0}\"", param.DirectionSign));
            }
            else
            {
                list.Add("\"directionSign\":null");
            }

            if (param.Points != null)
            {
                list.Add(string.Format("\"points\":{0}", JsonHelper.FromPoint2DCollection(param.Points)));
            }
            else
            {
                list.Add("\"points\":null");
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        /// <summary>${IS6_BusLine_method_fromJSON_D}</summary>
        /// <returns>${IS6_BusLine_method_fromJSON_Return}</returns>
        /// <param name="jsonObject">${IS6_BusLine_method_fromJSON_param}</param>
        public static BusLine FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            BusLine result = new BusLine();

            result.LineName = jsonObject["lineName"];
            result.SmID = jsonObject["smId"];
            result.LineID = jsonObject["lineId"];
            result.DirectionSign = jsonObject["directionSign"];

            if (jsonObject["points"] != null)
            {
                result.Points = new Point2DCollection();
                for (int i = 0; i < jsonObject["points"].Count; i++)
                {
                    result.Points.Add(JsonHelper.ToPoint2D((JsonObject)jsonObject["points"][i]));
                }
            }

            return result;
        }

    }
}
