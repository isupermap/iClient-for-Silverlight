﻿

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FuzzyFindBusStopParameters_Title}</para>
    /// 	<para>${IS6_FuzzyFindBusStopParameters_Description}</para>
    /// </summary>
    public class FuzzyFindBusStopParameters : ParametersBase
    {
        /// <summary>${IS6_FuzzyFindBusStopParameters_constructor_D}</summary>
        /// <overloads>${IS6_FuzzyFindBusStopParameters_constructor_overloads_D}</overloads>
        public FuzzyFindBusStopParameters()
        {
        }
        /// <summary>${IS6_FuzzyFindBusStopParameters_constructor_String_D}</summary>
        /// <param name="mapName">${IS6_FuzzyFindBusStopParameters_constructor_param_mapName}</param>
        /// <param name="fuzzyStopName">${IS6_FuzzyFindBusStopParameters_constructor_param_FuzzyStopName}</param>
        public FuzzyFindBusStopParameters(string mapName, string fuzzyStopName)
        {
            MapName = mapName;
            FuzzyStopName = fuzzyStopName;
        }
        /// <summary>${IS6_FuzzyFindBusStopParameters_attribute_FuzzyStopName_D}</summary>
        public string FuzzyStopName { get; set; }
    }
}
