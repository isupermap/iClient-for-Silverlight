﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_BusSolution_Title}</para>
    /// 	<para>${IS6_BusSolution_Description}</para>
    /// </summary>
    /// <remarks>${IS6_BusSolution_Remarks}</remarks>
    public class BusSolution
    {
        internal BusSolution()
        { }


        /// <summary>${IS6_BusSolution_attribute_Routings_D}</summary>
        public List<BusRouting> Routings { get; private set; }

        /// <summary>${IS6_BusSolution_attribute_StartStops_D}</summary>
        public List<BusStop> StartStops { get; private set; }

        /// <summary>${IS6_BusSolution_attribute_EndStops_D}</summary>
        public List<BusStop> EndStops { get; private set; }

        /// <summary>${IS6_BusSolution_attribute_ReturnRouting_D}</summary>
        public bool ReturnRouting { get; private set; }


        /// <summary>${IS6_BusSolution_method_fromJSON_D}</summary>
        /// <returns>${IS6_BusSolution_method_fromJSON_Return}</returns>
        /// <param name="jsonObject">${IS6_BusSolution_method_fromJSON_param}</param>
        public static BusSolution FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            BusSolution result = new BusSolution();

            if (jsonObject["routings"] != null)
            {
                result.Routings = new List<BusRouting>();
                for (int i = 0; i < jsonObject["routings"].Count; i++)
                {
                    result.Routings.Add(BusRouting.FromJson((JsonObject)jsonObject["routings"][i]));
                }
            }

            if (jsonObject["startStops"] != null)
            {
                result.StartStops = new List<BusStop>();
                for (int i = 0; i < jsonObject["startStops"].Count; i++)
                {
                    result.StartStops.Add(BusStop.FromJson((JsonObject)jsonObject["startStops"][i]));
                }
            }

            if (jsonObject["endStops"] != null)
            {
                result.EndStops = new List<BusStop>();
                for (int i = 0; i < jsonObject["endStops"].Count; i++)
                {
                    result.EndStops.Add(BusStop.FromJson((JsonObject)jsonObject["endStops"][i]));
                }
            }

            result.ReturnRouting = jsonObject["returnRouting"];

            return result;
        }
    }
}
