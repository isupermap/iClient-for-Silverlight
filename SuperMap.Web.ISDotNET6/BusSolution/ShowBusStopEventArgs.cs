﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowBusStopEventArgs_Title}</para>
    /// 	<para>${IS6_ShowBusStopEventArgs_Description}</para>
    /// </summary>
    public class ShowBusStopEventArgs:ServiceEventArgs
    {
        /// <summary>${IS6_ShowBusStopEventArgs_constructor_D}</summary>
        public ShowBusStopEventArgs(ShowBusStopResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_ShowBusStopEventArgs_attribute_Result_D}</summary>
        public ShowBusStopResult Result { get; private set; }
        /// <summary>${IS6_ShowBusStopEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
