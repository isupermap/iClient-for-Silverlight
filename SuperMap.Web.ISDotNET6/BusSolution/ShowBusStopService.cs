﻿



using SuperMap.Web.Service;
using System;
using SuperMap.Web.ISDotNET6.Resources;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowBusStopService_Title}</para>
    /// 	<para>${IS6_ShowBusStopService_Description}</para>
    /// </summary>
    public class ShowBusStopService : ServiceBase
    {
        /// <summary>${IS6_ShowBusStopService_constructor_D}</summary>
        /// <overloads>${IS6_ShowBusStopService_constructor_overloads_D}</overloads>
        public ShowBusStopService()
        { }

        /// <summary>${IS6_ShowBusStopService_constructor_String_D}</summary>
        /// <param name="url">${IS6_ShowBusStopService_constructor_param_url}</param>
        public ShowBusStopService(string url)
            : base(url)
        { }

        /// <summary>${IS6_ShowBusStopService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_ShowBusStopService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(ShowBusStopParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_ShowBusStopService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_ShowBusStopService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${IS6_ShowBusStopService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(ShowBusStopParameters parameters, object state)
        {
            if (parameters == null)
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);

            if (string.IsNullOrEmpty(this.Url))
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);

            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(ShowBusStopParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "ShowBusStop";
            dictionary.Add("method", method);
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("id", parameters.ID.ToString());

            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonArray = (JsonObject)JsonObject.Parse(e.Result);

            ShowBusStopResult result = ShowBusStopResult.FromJson(jsonArray);
            LastResult = result;
            ShowBusStopEventArgs args = new ShowBusStopEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(ShowBusStopEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        /// <summary>${IS6_ShowBusStopService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ShowBusStopEventArgs> ProcessCompleted;

        private ShowBusStopResult lastResult;
        /// <summary>${IS6_ShowBusStopService_attribute_LastResult_D}</summary>
        public ShowBusStopResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
