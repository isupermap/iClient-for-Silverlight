﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FuzzyFindBusStopResult_Title}</para>
    /// 	<para>${IS6_FuzzyFindBusStopResult_Description}</para>
    /// </summary>
    public class FuzzyFindBusStopResult
    {
        internal FuzzyFindBusStopResult()
        { }

        /// <summary>${IS6_FuzzyFindBusStopResult_attribute_BusStops_D}</summary>
        public List<BusStop> BusStops { get; private set; }


        /// <summary>${IS6_FuzzyFindBusStopResult_method_fromJSON_D}</summary>
        /// <returns>${IS6_FuzzyFindBusStopResult_method_fromJSON_Return}</returns>
        /// <param name="json">${IS6_FuzzyFindBusStopResult_method_fromJSON_param}</param>
        public static FuzzyFindBusStopResult FromJson(JsonArray json)
        {
            if (json != null && json.Count > 0)
            {
                FuzzyFindBusStopResult result = new FuzzyFindBusStopResult();
                result.BusStops = new List<BusStop>();
                for (int i = 0; i < json.Count; i++)
                {
                    result.BusStops.Add(BusStop.FromJson((JsonObject)json[i]));
                }

                return result;
            }
            return null;
        }
    }
}
