﻿
using System.Windows.Browser;
using System.Json;
using System.Windows;
using System.Collections.Generic;
using System;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetBusSolutionService_Title}</para>
    /// 	<para>${IS6_GetBusSolutionService_Description}</para>
    /// </summary>
    public class GetBusSolutionService : ServiceBase
    {
        // Release : 2010/01/14
        // Author  : ScottXu
        /// <summary>${IS6_GetBusSolutionService_constructor_D}</summary>
        /// <overloads>${IS6_GetBusSolutionService_constructor_overloads_D}</overloads>
        public GetBusSolutionService()
        { }
        /// <summary>${IS6_GetBusSolutionService_constructor_String_D}</summary>
        /// <param name="url">${IS6_GetBusSolutionService_constructor_param_url}</param>
        public GetBusSolutionService(string url)
            : base(url)
        {
        }

        /// <summary>${IS6_GetBusSolutionService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_GetBusSolutionService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(GetBusSolutionParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_GetBusSolutionService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_GetBusSolutionService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${IS6_GetBusSolutionService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GetBusSolutionParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("GetBusSolutionByIDsParameters is Null");
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }
            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(GetBusSolutionParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (parameters.IDs != null && parameters.IDs.Count > 0)
            {
                string method = "GetBusSolutionByIds";
                dictionary.Add("method", method);
                dictionary.Add("ids", JsonHelper.FromIList((List<int>)parameters.IDs));
            }
            else
            {
                string method = "GetBusSolutionByNames";
                dictionary.Add("method", method);

                string json = "[";
                List<string> list = new List<string>();
                for (int i = 0; i < parameters.Names.Count; i++)
                {
                    list.Add(string.Format("\"{0}\"", parameters.Names[i]));
                }
                json += string.Join(",", list.ToArray());
                json += "]";

                dictionary.Add("names", json);
            }

            dictionary.Add("map", parameters.MapName);
            dictionary.Add("busSolutionParam", BusSolutionParam.ToJson(parameters.BusSolutionParam));

            return dictionary;
        }


        private GetBusSolutionResult lastResult;

        /// <summary>${IS6_GetBusSolutionService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetBusSolutionEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonArray jsonArray = (JsonArray)JsonObject.Parse(e.Result);
            GetBusSolutionResult result = GetBusSolutionResult.FromJson(jsonArray);
            LastResult = result;
            GetBusSolutionEventArgs args = new GetBusSolutionEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(GetBusSolutionEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_GetBusSolutionService_attribute_LastResult_D}</summary>
        public GetBusSolutionResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}

