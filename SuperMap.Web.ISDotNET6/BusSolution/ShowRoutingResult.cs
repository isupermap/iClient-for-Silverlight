﻿

using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowRoutingResult_Title}</para>
    /// 	<para>${IS6_ShowRoutingResult_Description}</para>
    /// </summary>
    public class ShowRoutingResult
    {
        internal ShowRoutingResult()
        { }

        /// <summary>${IS6_ShowRoutingResult_attribute_BusRouting_D}</summary>
        public BusRouting BusRouting { get; private set; }


        /// <summary>${IS6_ShowRoutingResult_method_fromJSON_D}</summary>
        /// <returns>${IS6_ShowRoutingResult_method_fromJSON_Return}</returns>
        /// <param name="json">${IS6_ShowRoutingResult_method_fromJSON_param}</param>
        public static ShowRoutingResult FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            return new ShowRoutingResult { BusRouting = BusRouting.FromJson(json) };
        }
    }
}
