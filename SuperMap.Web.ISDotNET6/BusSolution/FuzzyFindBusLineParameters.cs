﻿

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FuzzyFindBusLineParameters_Title}</para>
    /// 	<para>${IS6_FuzzyFindBusLineParameters_Description}</para>
    /// </summary>
    public class FuzzyFindBusLineParameters : ParametersBase
    {
        /// <summary>${IS6_FuzzyFindBusLineParameters_constructor_D}</summary>
        /// <overloads>${IS6_FuzzyFindBusLineParameters_constructor_overloads_D}</overloads>
        public FuzzyFindBusLineParameters()
        { }

        /// <summary>${IS6_FuzzyFindBusLineParameters_constructor_String_D}</summary>
        /// <param name="mapName">${IS6_FuzzyFindBusLineParameters_constructor_param_mapName}</param>
        /// <param name="fuzzyBusLineName">${IS6_FuzzyFindBusLineParameters_constructor_param_fuzzyBusLineName}</param>
        public FuzzyFindBusLineParameters(string mapName, string fuzzyBusLineName)
        {
            MapName = mapName;
            FuzzyBusLineName = fuzzyBusLineName;
        }

        //对应的Name，也就是几路。
        /// <summary>${IS6_FuzzyFindBusLineParameters_attribute_FuzzyBusLineName_D}</summary>
        public string FuzzyBusLineName { get; set; }
    }
}
