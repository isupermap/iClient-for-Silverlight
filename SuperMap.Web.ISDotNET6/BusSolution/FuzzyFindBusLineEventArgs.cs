﻿


using System.Collections.Generic;
using SuperMap.Web.Service;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FuzzyFindBusLineEventArgs_Title}</para>
    /// 	<para>${IS6_FuzzyFindBusLineEventArgs_Description}</para>
    /// </summary>
    public class FuzzyFindBusLineEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_FuzzyFindBusLineEventArgs_constructor_D}</summary>
        public FuzzyFindBusLineEventArgs(FuzzyFindBusLineResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_FuzzyFindBusLineEventArgs_attribute_Result_D}</summary>
        public FuzzyFindBusLineResult Result { get; private set; }
        /// <summary>${IS6_FuzzyFindBusLineEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
