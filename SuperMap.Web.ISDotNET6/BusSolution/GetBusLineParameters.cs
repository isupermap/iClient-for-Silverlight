﻿

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetBusLineParameters_Title}</para>
    /// 	<para>${IS6_GetBusLineParameters_Description}</para>
    /// </summary>
    public class GetBusLineParameters : ParametersBase
    {
        /// <summary>${IS6_GetBusLineParameters_constructor_D}</summary>
        /// <overloads>${IS6_GetBusLineParameters_constructor_overloads_D}</overloads>
        public GetBusLineParameters()
        {
            InterchangeID = int.MinValue;
        }
        /// <summary>${IS6_GetBusLineParameters_constructor_String_D_int}</summary>
        /// <param name="mapName">${IS6_GetBusLineParameters_constructor_param_mapName}</param>
        /// <param name="interchangeID">${IS6_GetBusLineParameters_constructor_param_InterchangeID}</param>
        public GetBusLineParameters(string mapName, int interchangeID)
        {
            MapName = mapName;
            InterchangeID = interchangeID;
        }
        /// <summary>${IS6_GetBusLineParameters_constructor_String_D}</summary>
        /// <param name="mapName">${IS6_GetBusLineParameters_constructor_param_mapName}</param>
        /// <param name="stopName">${IS6_GetBusLineParameters_constructor_param_StopName}</param>
        public GetBusLineParameters(string mapName, string stopName)
        {
            MapName = mapName;
            StopName = stopName;
        }

        //将ID和Name都开出来，但是以ID为准，
        //当设置了ID时，Name就不起作用了，
        //当没有设置ID时，Name才起作用；
        /// <summary>${IS6_GetBusLineParameters_attribute_InterchangeID_D}</summary>
        public int InterchangeID { get; set; }
        /// <summary>${IS6_GetBusLineParameters_attribute_StopName_D}</summary>
        public string StopName { get; set; }
    }
}
