﻿



using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ShowBusStopResult_Title}</para>
    /// 	<para>${IS6_ShowBusStopResult_Description}</para>
    /// </summary>
    public class ShowBusStopResult
    {
        internal ShowBusStopResult()
        { }

        /// <summary>${IS6_ShowBusStopResult_attribute_BusStop_D}</summary>
        public BusStop BusStop { get; private set; }

        /// <summary>${IS6_ShowBusStopResult_method_fromJSON_D}</summary>
        /// <returns>${IS6_ShowBusStopResult_method_fromJSON_Return}</returns>
        /// <param name="json">${IS6_ShowBusStopResult_method_fromJSON_param}</param>
        public static ShowBusStopResult FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new ShowBusStopResult { BusStop = BusStop.FromJson(json) };
        }
    }
}
