﻿

using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_FuzzyFindBusLineResult_Title}</para>
    /// 	<para>${IS6_FuzzyFindBusLineResult_Description}</para>
    /// </summary>
    public class FuzzyFindBusLineResult
    {
        internal FuzzyFindBusLineResult()
        { }

        /// <summary>${IS6_FuzzyFindBusLineResult_attribute_BusLines_D}</summary>
        public List<BusLine> BusLines { get; private set; }


        /// <summary>${IS6_FuzzyFindBusLineResult_method_fromJSON_D}</summary>
        /// <returns>${IS6_FuzzyFindBusLineResult_method_fromJSON_Return}</returns>
        /// <param name="json">${IS6_FuzzyFindBusLineResult_method_fromJSON_param}</param>
        public static FuzzyFindBusLineResult FromJson(JsonArray json)
        {
            if (json != null && json.Count > 0)
            {
                FuzzyFindBusLineResult result = new FuzzyFindBusLineResult();
                result.BusLines = new List<BusLine>();
                for (int i = 0; i < json.Count; i++)
                {
                    result.BusLines.Add(BusLine.FromJson((JsonObject)json[i]));
                }

                return result;
            }
            return null;
        }
    }
}
