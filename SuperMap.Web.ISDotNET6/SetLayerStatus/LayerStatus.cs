﻿
namespace SuperMap.Web.ISDotNET6
{
    //对应IS6中Layer
    /// <summary>
    /// 	<para>${IS6_LayerStatus_Title}</para>
    /// 	<para>${IS6_LayerStatus_Description}</para>
    /// </summary>
    public class LayerStatus
    {
        /// <summary>${IS6_LayerStatus_constructor_None_D}</summary>
        public LayerStatus()
        { }
        /// <summary>${IS6_LayerStatus_attribute_visible_D}</summary>
        public bool Visible { get; set; }

        /// <summary>${IS6_LayerStatus_attribute_LayerName_D}</summary>
        public string LayerName { get; set; }

        public string DisplayFilter { get; set; }
        //Layer的其他属性等有需求再加；
        internal static string ToJson(LayerStatus param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            json += string.Format("\"visible\":{0},", param.Visible.ToString().ToLower());
            json += string.Format("\"displayFilter\":\"{0}\",", param.DisplayFilter);
            json += string.Format("\"name\":\"{0}\"", param.LayerName);
            json += "}";

            return json;
        }
    }
}
