﻿
using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Json;
using System.Text;
using System.Windows;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_SetLayerStatusService_Title}</para>
    /// 	<para>${IS6_SetLayerStatusService_Description}</para>
    /// </summary>
    public class SetLayerStatusService:ServiceBase
    {
        /// <summary>${IS6_SetLayerStatusService_constructor_None_D}</summary>
        /// <overloads>${IS6_SetLayerStatusService_constructor_overloads}</overloads>
        public SetLayerStatusService()
        { }
        /// <summary>${IS6_SetLayerStatusService_constructor_String_D}</summary>
        /// <param name="url">${IS6_SetLayerStatusService_constructor_String_param_url}</param>
        public SetLayerStatusService(string url)
            : base(url)
        { }
        /// <summary>${IS6_SetLayerStatusService_method_processAsync_D}</summary>
        /// <overloads>${IS6_SetLayerStatusService_method_processAsync_overloads}</overloads>
        /// <param name="parameters">${IS6_SetLayerStatusService_method_processAsync_param_parameters}</param>
        public void ProcessAsync(SetLayerStatusParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_SetLayerStatusService_method_processAsync_D}</summary>
        /// <param name="parameters">${IS6_SetLayerStatusService_method_processAsync_param_parameters}</param>
        /// <param name="state">${IS6_SetLayerStatusService_method_processAsync_param_state}</param>
        public void ProcessAsync(SetLayerStatusParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("UpdateLayersParameters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            base.SubmitRequest(base.Url + "/common.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(SetLayerStatusParameters parameters)
        {

            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "UpdateLayers";
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);

            string json = "[";

            List<string> list = new List<string>();
            for (int i = 0; i < parameters.LayerStatusList.Count; i++)
            {
                list.Add(LayerStatus.ToJson(parameters.LayerStatusList[i]));
            }

            json += string.Join(",", list.ToArray());
            json += "]";

            dictionary.Add("layers", json);


            //这里的LayersKey初始值是0，之后是每一次返回结果中LayersKey
            dictionary.Add("layersKey", "0");

            dictionary.Add("bModifiedByServer", "false");
   
            return dictionary;
        }


        private SetLayerStatusResult lastResult;

        /// <summary>${IS6_SetLayerStatusService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<SetLayerStatusEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonPrimitive jsonObject = (JsonPrimitive)JsonObject.Parse(e.Result);

            SetLayerStatusResult result = SetLayerStatusResult.FromJson(jsonObject);
            LastResult = result;
            SetLayerStatusEventArgs args = new SetLayerStatusEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(SetLayerStatusEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_SetLayerStatusService_attribute_lastResult_D}</summary>
        public SetLayerStatusResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
