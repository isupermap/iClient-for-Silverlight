﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_SetLayerStatusEventArgs_Title}</para>
    /// <para>${IS6_SetLayerStatusEventArgs_Description}</para>
    /// </summary>
    public class SetLayerStatusEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_SetLayerStatusEventArgs_constructor_D}</summary>
        public SetLayerStatusEventArgs(SetLayerStatusResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_SetLayerStatusEventArgs_attribute_result}</summary>
        public SetLayerStatusResult Result { get; private set; }
        /// <summary>${IS6_SetLayerStatusEventArgs_attribute_originResult}</summary>
        public string OriginResult { get; private set; }
    }
}
