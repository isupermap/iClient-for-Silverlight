﻿
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_SetLayerStatusResult_Title}</para>
    /// <para>${IS6_SetLayerStatusResult_Description}</para>
    /// </summary>
    public class SetLayerStatusResult
    {
        internal SetLayerStatusResult()
        { }
        /// <summary>${IS6_SetLayerStatusResult_attribute_layersKey_D}</summary>
        public string LayersKey { get; private set; }
        /// <summary>${IS6_SetLayerStatusResult_method_FromJson_D}</summary>
        /// <returns>${IS6_SetLayerStatusResult_method_FromJson_return}</returns>
        /// <param name="jsonObject">${IS6_SetLayerStatusResult_method_FromJson_param_jsonObject}</param>
        public static SetLayerStatusResult FromJson(JsonPrimitive jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            SetLayerStatusResult result = new SetLayerStatusResult();
            result.LayersKey=(string)jsonObject;
            return result;
        }
    }
}
