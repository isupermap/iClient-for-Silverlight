﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_SetLayerStatusParameter_Title}</para>
    /// <para>${IS6_SetLayerStatusParameter_Description}</para>
    /// </summary>
    public class SetLayerStatusParameters : ParametersBase
    {
        /// <summary>${IS6_SetLayerStatusParameter_constructor_None_D}</summary>
        public SetLayerStatusParameters()
        { }
        /// <summary>${IS6_SetLayerStatusParameter_attribute_layerStatusList_D}</summary>
        public IList<LayerStatus> LayerStatusList { get; set; }
    }

}
