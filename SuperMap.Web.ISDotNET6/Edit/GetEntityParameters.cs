﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetEntityParameters_Title}</para>
    /// 	<para>${IS6_GetEntityParameters_Description}</para>
    /// </summary>
    public class GetEntityParameters:ParametersBase
    {
        /// <summary>${IS6_GetEntityParameters_constructor_None_D}</summary>
        public GetEntityParameters()
        { }
        /// <summary>${IS6_GetEntityParameters_attribute_LayerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${IS6_GetEntityParameters_attribute_ID_D}</summary>
        public int ID { get; set; }
    }
}
