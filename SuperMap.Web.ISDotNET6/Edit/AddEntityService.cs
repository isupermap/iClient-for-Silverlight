﻿
using System.Windows.Browser;
using System.Windows;
using System.Json;
using System;
using System.Collections.Generic;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_AddEntityService_Title}</para>
    /// 	<para>${IS6_AddEntityService_Description}</para>
    /// </summary>
    public class AddEntityService:ServiceBase
    {
        /// <summary>${IS6_AddEntityService_constructor_None_D}</summary>
        /// <overloads>${IS6_AddEntityService_constructor_overloads_D}</overloads>
        public AddEntityService()
        { }
        /// <summary>${IS6_AddEntityService_constructor_String_D}</summary>
        /// <param name="url">${IS6_AddEntityService_constructor_String_param_url}</param>
        public AddEntityService(string url)
            : base(url)
        { }
        /// <summary>${IS6_AddEntityService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_AddEntityService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(AddEntityParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${IS6_AddEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_AddEntityService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_AddEntityService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(AddEntityParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:本地化
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("AddParameters is Null");
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:本地化
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }

            base.SubmitRequest(base.Url + "/edit.ashx?", GetParameters(parameters),
               new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(AddEntityParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "AddEntity";
            dictionary.Add("method", method);
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("layerName",string.Format("\"{0}\"", parameters.LayerName));

            string json = "[";

            List<string> list = new List<string>();
            for (int i = 0; i < parameters.Entities.Count; i++)
            {
                list.Add(Entity.ToJson(parameters.Entities[i]));
            }

            json += string.Join(",", list.ToArray());
            json += "]";

            dictionary.Add("entities", json);


            return dictionary;

        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            EditResult result = EditResult.FromJson(jsonObject);
            LastResult = result;
            EditEventArgs args = new EditEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);

        }
        private void OnProcessCompleted(EditEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        private EditResult lastResult;

        /// <summary>${IS6_AddEntityService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<EditEventArgs> ProcessCompleted;

        /// <summary>${IS6_AddEntityService_attribute_lastResult_D}</summary>
        public EditResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
