﻿
using System.Collections.Generic;
using SuperMap.Web.Core;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_EditResult_Title}</para>
    /// 	<para>${IS6_EditResult_Description}</para>
    /// </summary>
    public class EditResult
    {
        internal EditResult()
        { }
        /// <summary>${IS6_EditResult_attribute_succeed_D}</summary>
        public bool Succeed { get; private set; }
        /// <summary>${IS6_EditResult_attribute_IDs_D}</summary>
        public List<int> IDs { get; private set; }
        /// <summary>${IS6_EditResult_attribute_Bounds_D}</summary>
        public Rectangle2D Bounds { get; private set; }
        /// <summary>${IS6_EditResult_attribute_message_D}</summary>
        public string Message { get; private set; }

        /// <summary>${IS6_EditResult_method_fromJSON_D}</summary>
        /// <returns>${IS6_EditResult_method_fromJSON_Return}</returns>
        /// <param name="jsonObject">${IS6_EditResult_method_fromJSON_param}</param>
        public static EditResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            EditResult result = new EditResult();

            result.Succeed = (bool)jsonObject["succeed"];

            JsonArray idsInJson = (JsonArray)jsonObject["ids"];
            if (idsInJson.Count > 0 && idsInJson != null)
            {
                result.IDs = new List<int>();
                for (int i = 0; i < idsInJson.Count; i++)
                {
                    result.IDs.Add(idsInJson[i]);
                }
            }

            if (jsonObject["bounds"] != null)
            {
                double mbMinX = (double)jsonObject["bounds"]["leftBottom"]["x"];
                double mbMinY = (double)jsonObject["bounds"]["leftBottom"]["y"];
                double mbMaxX = (double)jsonObject["bounds"]["rightTop"]["x"];
                double mbMaxY = (double)jsonObject["bounds"]["rightTop"]["y"];
                result.Bounds = new Rectangle2D(mbMinX, mbMinY, mbMaxX, mbMaxY);
            }

            result.Message = (string)jsonObject["message"];
            return result;
        }
    }
}
