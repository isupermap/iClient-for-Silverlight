﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_AddEntityParameters_Title}</para>
    /// 	<para>${IS6_AddEntityParameters_Description}</para>
    /// </summary>
    public class AddEntityParameters:ParametersBase
    {
        /// <summary>${IS6_AddEntityParameters_constructor_None_D}</summary>
        public AddEntityParameters()
        { }
        /// <summary>${IS6_AddEntityParameters_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${IS6_AddEntityParameters_attribute_Entity_D}</summary>
        public IList<Entity> Entities { get; set; }
    }
}
