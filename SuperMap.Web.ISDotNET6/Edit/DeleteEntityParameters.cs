﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_DeleteEntityParameters_Title}</para>
    /// 	<para>${IS6_DeleteEntityParameters_Description}</para>
    /// </summary>
    public class DeleteEntityParameters : ParametersBase
    {
        /// <summary>${IS6_DeleteEntityParameters_constructor_None_D}</summary>
        public DeleteEntityParameters()
        { }
        /// <summary>${IS6_DeleteEntityParameters_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${IS6_DeleteEntityParameters_attribute_IDs_D}</summary>
        public IList<int> IDs { get; set; }
    }
}
