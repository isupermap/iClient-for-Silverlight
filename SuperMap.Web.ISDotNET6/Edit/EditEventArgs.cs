﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_EditEventArgs_Title}</para>
    /// 	<para>${IS6_EditEventArgs_Description}</para>
    /// </summary>
    public class EditEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_EditEventArgs_constructor_None_D}</summary>
        public EditEventArgs(EditResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }

        /// <summary>${IS6_EditEventArgs_attribute_result_D}</summary>
        public EditResult Result { get; private set; }
        /// <summary>${IS6_EditEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
