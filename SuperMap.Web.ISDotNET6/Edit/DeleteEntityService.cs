﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_DeleteEntityService_Title}</para>
    /// 	<para>${IS6_DeleteEntityService_Description}</para>
    /// </summary>
    public class DeleteEntityService : ServiceBase
    {

        /// <summary>${IS6_DeleteEntityService_constructor_None_D}</summary>
        /// <overloads>${IS6_DeleteEntityService_constructor_overloads_D}</overloads>
        public DeleteEntityService()
        { }
        /// <summary>${IS6_DeleteEntityService_constructor_String_D}</summary>
        /// <param name="url">${IS6_DeleteEntityServicee_constructor_String_param_url}</param>
        public DeleteEntityService(string url)
            : base(url)
        { }
        /// <summary>${IS6_DeleteEntityService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_DeleteEntityService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(DeleteEntityParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${IS6_DeleteEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_DeleteEntityService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_DeleteEntityService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(DeleteEntityParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:本地化
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("AddParameters is Null");
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:本地化
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }

            base.SubmitRequest(base.Url + "/edit.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(DeleteEntityParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "DeleteEntity";
            dictionary.Add("method", method);
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("layerName", string.Format("\"{0}\"", parameters.LayerName));

            string json = "[";
            List<string> list = new List<string>();
            for (int i = 0; i < parameters.IDs.Count; i++)
            {
                list.Add(string.Format("{0}", parameters.IDs[i]));
            }
            json += string.Join(",", list.ToArray());
            json += "]";

            dictionary.Add("ids", json);
            return dictionary;

        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            EditResult result = EditResult.FromJson(jsonObject);
            LastResult = result;
            EditEventArgs args = new EditEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);

        }
        private void OnProcessCompleted(EditEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        private EditResult lastResult;

        /// <summary>${IS6_DeleteEntityService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<EditEventArgs> ProcessCompleted;

        /// <summary>${IS6_DeleteEntityService_attribute_lastResult_D}</summary>
        public EditResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
