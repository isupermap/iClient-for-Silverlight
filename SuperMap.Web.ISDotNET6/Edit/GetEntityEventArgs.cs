﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetEntityEventArgs_Title}</para>
    /// 	<para>${IS6_GetEntityEventArgs_Description}</para>
    /// </summary>
    public class GetEntityEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_GetEntityEventArgs_constructor_None_D}</summary>
        public GetEntityEventArgs(Entity result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }

        /// <summary>${IS6_GetEntityEventArgs_attribute_Result_D}</summary>
        public Entity Result { get; private set; }
        /// <summary>${IS6_GetEntityEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
