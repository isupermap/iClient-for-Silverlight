﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_UpdateEntityService_Title}</para>
    /// 	<para>${IS6_UpdateEntityService_Description}</para>
    /// </summary>
    public class UpdateEntityService : ServiceBase
    {
        /// <summary>${IS6_UpdateEntityService_constructor_None_D}</summary>
        /// <overloads>${IS6_UpdateEntityService_constructor_overloads_D}</overloads>
        public UpdateEntityService()
        { }
        /// <summary>${IS6_UpdateEntityService_constructor_String_D}</summary>
        /// <param name="url">${IS6_UpdateEntityService_constructor_String_param_url}</param>
        public UpdateEntityService(string url)
            : base(url)
        { }
        /// <summary>${IS6_UpdateEntityService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_UpdateEntityService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(UpdateEntityParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${IS6_UpdateEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_UpdateEntityService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_UpdateEntityService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(UpdateEntityParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:本地化
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("UpdateEntityParameters is Null");
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:本地化
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }

            base.SubmitRequest(base.Url + "/edit.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(UpdateEntityParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "UpdateEntity";
            dictionary.Add("method", method);
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("layerName", string.Format("\"{0}\"", parameters.LayerName));

            string json = "[";

            List<string> list = new List<string>();
            for (int i = 0; i < parameters.Entities.Count; i++)
            {
                list.Add(Entity.ToJson(parameters.Entities[i]));
            }

            json += string.Join(",", list.ToArray());
            json += "]";

            dictionary.Add("entities", json);


            return dictionary;

        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            EditResult result = EditResult.FromJson(jsonObject);
            LastResult = result;
            EditEventArgs args = new EditEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);

        }
        private void OnProcessCompleted(EditEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        private EditResult lastResult;

        /// <summary>${IS6_UpdateEntityService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<EditEventArgs> ProcessCompleted;

        /// <summary>${IS6_UpdateEntityService_attribute_lastResult_D}</summary>
        public EditResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
