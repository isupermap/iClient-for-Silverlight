﻿
using System.Collections.Generic;
using System;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetEntityService_Title}</para>
    /// 	<para>${IS6_GetEntityService_Description}</para>
    /// </summary>
    public class GetEntityService : ServiceBase
    {
        /// <summary>${IS6_GetEntityService_constructor_None_D}</summary>
        /// <overloads>${IS6_GetEntityService_constructor_overloads_D}</overloads>
        public GetEntityService()
        { }
        /// <summary>${IS6_GetEntityService_constructor_String_D}</summary>
        /// <param name="url">${IS6_GetEntityService_constructor_String_param_url}</param>
        public GetEntityService(string url)
            : base(url)
        { }
        /// <summary>${IS6_GetEntityService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_GetEntityService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(GetEntityParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${IS6_GetEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_GetEntityService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_GetEntityService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GetEntityParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:本地化
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("GetEntityParameters is Null");
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:本地化
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            base.SubmitRequest(base.Url + "/common.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(GetEntityParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "GetEntity";
            dictionary.Add("method", method);
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("layerName", string.Format("\"{0}\"", parameters.LayerName));
            dictionary.Add("id", parameters.ID.ToString());
            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            Entity result = Entity.FromJson(jsonObject);
            LastResult = result;
            GetEntityEventArgs args = new GetEntityEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);

        }
        private void OnProcessCompleted(GetEntityEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        private Entity lastResult;

        /// <summary>${IS6_GetEntityService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetEntityEventArgs> ProcessCompleted;

        /// <summary>${IS6_GetEntityService_attribute_lastResult_D}</summary>
        public Entity LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
