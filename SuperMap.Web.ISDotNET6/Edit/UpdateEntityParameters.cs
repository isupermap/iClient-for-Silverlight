﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_UpdateEntityParameters_Title}</para>
    /// 	<para>${IS6_UpdateEntityParameters_Description}</para>
    /// </summary>
    public class UpdateEntityParameters : ParametersBase
    {
        /// <summary>${IS6_UpdateEntityParameters_constructor_None_D}</summary>
        public UpdateEntityParameters()
        { }
        /// <summary>${IS6_UpdateEntityParameters_attribute_LayerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${IS6_UpdateEntityParameters_attribute_Entity_D}</summary>
        public IList<Entity> Entities { get; set; }
    }
}
