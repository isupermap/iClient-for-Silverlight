﻿
using System.Collections.Generic;
using System.Text;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_QueryLayer_Title}</para>
    /// 	<para>${IS6_QueryLayer_Description}</para>
    /// </summary>
    public class QueryLayer
    {
        /// <summary>${IS6_QueryLayer_constructor_None_D}</summary>
        public QueryLayer()
        { }
        /// <summary>${IS6_QueryLayer_attribute_groupClause_D}</summary>
        public string GroupClause { get; set; }
        /// <summary>${IS6_QueryLayer_attribute_layerID_D}</summary>
        public int LayerID { get; set; }
        /// <summary>${IS6_QueryLayer_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${IS6_QueryLayer_attribute_RelQueryTableInfos_D}</summary>
        public IList<RelQueryTableInfo> RelQueryTableInfos { get; set; }
        /// <summary>${IS6_QueryLayer_attribute_ReturnFields_D}</summary>
        public IList<string> ReturnFields { get; set; }
        /// <summary>${IS6_QueryLayer_attribute_sortClause_D}</summary>
        public string SortClause { get; set; }
        /// <summary>${IS6_QueryLayer_attribute_whereClause_D}</summary>
        public string WhereClause { get; set; }

        internal static string ToJson(QueryLayer queryLayer)
        {
            if (queryLayer == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();


            if (!string.IsNullOrEmpty(queryLayer.GroupClause))
            {
                list.Add(string.Format("\"groupClause\":\"{0}\"", queryLayer.GroupClause));
            }
            else
            {
                list.Add("\"groupClause\":\"\"");
            }


            list.Add(string.Format("\"layerId\":{0}", queryLayer.LayerID));

            if (!string.IsNullOrEmpty(queryLayer.LayerName))
            {
                list.Add(string.Format("\"layerName\":\"{0}\"", queryLayer.LayerName));
            }
            else
            {
                list.Add("\"layerName\":\"\"");
            }

            if (queryLayer.ReturnFields != null && queryLayer.ReturnFields.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < queryLayer.ReturnFields.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", queryLayer.ReturnFields[i]));
                }
                list.Add(string.Format("\"returnFields\":[{0}]", string.Join(",", temp.ToArray())));
            }
            else
            {
                list.Add("\"returnFields\":null");
            }

            if (!string.IsNullOrEmpty(queryLayer.SortClause))
            {
                list.Add(string.Format("\"sortClause\":\"{0}\"", queryLayer.SortClause));
            }
            else
            {
                list.Add("\"sortClause\":\"\"");
            }

            if (!string.IsNullOrEmpty(queryLayer.WhereClause))
            {
                list.Add(string.Format("\"whereClause\":\"{0}\"", queryLayer.WhereClause));
            }
            else
            {
                list.Add("\"whereClause\":\"\"");
            }

            IList<RelQueryTableInfo> relQueryTableInfos = queryLayer.RelQueryTableInfos;
            if (relQueryTableInfos != null && relQueryTableInfos.Count > 0)
            {
                List<string> ls = new List<string>();

                for (int i = 0; i < relQueryTableInfos.Count; i++)
                {
                    ls.Add(RelQueryTableInfo.ToJson(relQueryTableInfos[i]));
                }
                string temp = "[" + string.Join(",", ls.ToArray()) + "]";
                list.Add(string.Format("\"relQueryTableInfos\":{0}", temp));
            }
            else
            {
                list.Add(string.Format("\"relQueryTableInfos\":null"));
            }

            json += string.Join(",", list.ToArray());
            json += "}";


            return json;

        }
    }
}
