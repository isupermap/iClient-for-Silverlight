﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_JoinType_Title}</para>>
    /// <para>${IS6_JoinType_Description}</para>
    /// </summary>
    public enum JoinTpye
    {
        /// <summary>${IS6_JoinType_attribute_innerJion_D}</summary>
        InnerJion=0,
        /// <summary>${IS6_JoinType_attribute_leftJion_D}</summary>
        LeftJion=1,
        /// <summary>${IS6_JoinType_attribute_rightJion_D}</summary>
        RightJion=2,
        /// <summary>${IS6_JoinType_attribute_fullJoin_D}</summary>
        FullJion=3
    }
}
