﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using System.Windows;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_QueryByCenterService_Title}</para>
    /// 	<para>${IS6_QueryByCenterService_Description}</para>
    /// </summary>
    public class QueryByCenterService : ServiceBase
    {
        /// <summary>${IS6_QueryByCenterService_constructor_None_D}</summary>
        /// <overloads>${IS6_QueryByCenterService_constructor_overloads_D}</overloads>
        public QueryByCenterService()
        { }
        /// <summary>${IS6_QueryByCenterService_constructor_String_D}</summary>
        /// <param name="url">${IS6_QueryByCenterService_constructor_String_param_url}</param>
        public QueryByCenterService(string url)
            : base(url)
        { }
        /// <summary>${IS6_QueryByCenterService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_QueryByCenterService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="paramters">${IS6_QueryByCenterService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(QueryByCenterParameters paramters)
        {
            ProcessAsync(paramters, null);
        }
        /// <summary>${IS6_QueryByCenterService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_QueryByCenterService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_QueryByCenterService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(QueryByCenterParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                // throw new ArgumentNullException("QueryByCenterParamters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            base.SubmitRequest(base.Url + "/query.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(QueryByCenterParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = parameters.IsNearest ? "FindNearest" : "QueryByPoint";

            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);

            dictionary.Add("point", JsonHelper.FromPoint2D(parameters.CenterPoint));
            dictionary.Add("tolerance", parameters.Tolerance.ToString());
            dictionary.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
            dictionary.Add("trackingLayerIndex", "-1");
            dictionary.Add("userID", string.Format("\"{0}\"", Guid.NewGuid()));

            return dictionary;

        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            ResultSet result = ResultSet.FromJson(jsonObject);
            LastResult = result;
            QueryServiceEventArgs args = new QueryServiceEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(QueryServiceEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(
                    ProcessCompleted, new object[] { this, args });
            }
        }

        /// <summary>${IS6_QueryByCenterService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<QueryServiceEventArgs> ProcessCompleted;

        private ResultSet lastResult;
        /// <summary>${IS6_QueryByCenterService_attribute_lastResult_D}</summary>
        public ResultSet LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
