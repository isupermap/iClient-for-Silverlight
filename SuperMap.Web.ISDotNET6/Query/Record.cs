﻿

using System.Collections.Generic;
using SuperMap.Web.Core;
using System.Json;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_Record_Title}</para>
    /// 	<para>${IS6_Record_Description}</para>
    /// </summary>
    /// <remarks>${IS6_Record_Remarks}</remarks>
    public class Record
    {
        private Record()
        {
        }
        /// <summary>${IS6_Record_attribute_bounds_D}</summary>
        public Rectangle2D Bounds { get; private set; }
        /// <summary>${IS6_Record_attribute_center_D}</summary>
        public Point2D Center { get; private set; }
        /// <summary>${IS6_Record_attribute_fieldValues_D}</summary>
        public List<string> FieldValues { get; private set; }
        /// <summary>${IS6_Record_attribute_shape_D}</summary>
        public ServerGeometry Shape { get; private set; }


        /// <summary>${IS6_Record_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_Record_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_Record_method_FromJson_return}</returns>
        public static Record FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            Record record = new Record();

            if (jsonObject.ContainsKey("bounds") && jsonObject["bounds"] != null)
            {
                double mbMinX = (double)jsonObject["bounds"]["leftBottom"]["x"];
                double mbMinY = (double)jsonObject["bounds"]["leftBottom"]["y"];
                double mbMaxX = (double)jsonObject["bounds"]["rightTop"]["x"];
                double mbMaxY = (double)jsonObject["bounds"]["rightTop"]["y"];
                record.Bounds = new Rectangle2D(mbMinX, mbMinY, mbMaxX, mbMaxY);
            }
            else
            {
                record.Bounds = Rectangle2D.Empty;
            }

            if (jsonObject.ContainsKey("center") && jsonObject["center"] != null)
            {
                record.Center = JsonHelper.ToPoint2D((JsonObject)jsonObject["center"]);
            }
            else
            {
                record.Center = Point2D.Empty;
            }

            if (jsonObject.ContainsKey("shape") && jsonObject["shape"] != null)
            {
                record.Shape = ServerGeometry.FromJson((JsonObject)jsonObject["shape"]);
            }

            if (jsonObject.ContainsKey("fieldValues") && jsonObject["fieldValues"] != null)
            {
                record.FieldValues = JsonHelper.ToStringList((JsonArray)jsonObject["fieldValues"]);
            }
            return record;
        }

    }
}
