﻿
using SuperMap.Web.Core;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_QueryByCenterParameters_Title}</para>
    /// 	<para>${IS6_QueryByCenterParameters_Description}</para>
    /// </summary>
    public class QueryByCenterParameters : QueryParametersBase
    {
        /// <summary>${IS6_QueryByCenterParameters_constructor_None_D}</summary>
        public QueryByCenterParameters()
        {
            CenterPoint = Point2D.Empty;
            Tolerance = 50;
        }
        /// <summary>${IS6_QueryByCenterParameters_attribute_centerPoint_D}</summary>
        public Point2D CenterPoint { get; set; }
        /// <summary>${IS6_QueryByCenterParameters_attribute_tolerance_D}</summary>
        public double Tolerance { get; set; }
        /// <summary>${IS6_QueryByCenterParameters_attribute_isNearest_D}</summary>
        public bool IsNearest { get; set; }

    }
}
