﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_QueryParametersBase_Title}</para>
    /// 	<para>${IS6_QueryParametersBase_Description}</para>
    /// </summary>
    public class QueryBySqlParameters : QueryParametersBase
    {
        /// <summary>${IS6_QueryParametersBase_constructor_None_D}</summary>
        public QueryBySqlParameters()
        { }
    }
}
