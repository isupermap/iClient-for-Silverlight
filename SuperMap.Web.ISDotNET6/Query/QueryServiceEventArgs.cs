﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{

    /// <summary>
    /// 	<para>${IS6_QueryServiceEventArgs_Title}</para>
    /// 	<para>${IS6_QueryServiceEventArgs_Description}</para>
    /// </summary>
    public sealed class QueryServiceEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_QueryServiceEventArgs_constructor_D}</summary>
        /// <param name="resultSet">${IS6_QueryServiceEventArgs_constructor_param_result}</param>
        /// <param name="originResult">${IS6_QueryServiceEventArgs_constructor_param_originResult}</param>
        /// <param name="userToken">${IS6_QueryServiceEventArgs_constructor_param_token}</param>
        public QueryServiceEventArgs(ResultSet resultSet, string originResult, object userToken)
            : base(userToken)
        {
            ResultSet = resultSet;
            OriginResult = originResult;
        }

        /// <summary>${IS6_QueryServiceEventArgs_attribute_resultSet_D}</summary>
        public ResultSet ResultSet { get; private set; }
        /// <summary>${IS6_QueryServiceEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }

}
