﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_RelQueryTableInfo_Title}</para>
    /// 	<para>${IS6_RelQueryTableInfo_Description}</para>
    /// </summary>
    public class RelQueryTableInfo
    {
        /// <summary>${IS6_RelQueryTableInfo_constructor_None_D}</summary>
        public RelQueryTableInfo()
        { }
        /// <summary>${IS6_RelQueryTableInfo_attribute_joinType_D}</summary>
        public JoinTpye JoinType { get; set; }
        /// <summary>${IS6_RelQueryTableInfo_attribute_searchCondition_D}</summary>
        public string SearchCondition { get; set; }
        /// <summary>${IS6_RelQueryTableInfo_attribute_tableName_D}</summary>
        public string TableName { get; set; }

        internal static string ToJson(RelQueryTableInfo info)
        {
            if (info == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"joinType\":{0}", (int)info.JoinType));

            if (!string.IsNullOrEmpty(info.SearchCondition))
            {
                list.Add(string.Format("\"searchCondition\":\"{0}\"", info.SearchCondition));
            }
            else
            {
                list.Add("\"searchCondition\":null");
            }

            if (!string.IsNullOrEmpty(info.TableName))
            {
                list.Add(string.Format("\"tableName\":\"{0}\"", info.TableName));
            }
            else
            {
                list.Add("\"tableName\":null");
            }

            json += string.Join(",", list.ToArray());
            json += "}";


            return json;



        }

        /// <summary>${IS6_RelQueryTableInfo_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_RelQueryTableInfo_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_RelQueryTableInfo_method_FromJson_return}</returns>
        public static RelQueryTableInfo FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            return new RelQueryTableInfo { JoinType = (JoinTpye)(int)jsonObject["joinType"], SearchCondition = (string)jsonObject["searchCondition"], TableName = (string)jsonObject["tableName"] };

        }
    }
}
