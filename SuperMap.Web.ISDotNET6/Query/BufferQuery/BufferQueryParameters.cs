﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_BufferQueryParameters_Title}</para>
    /// 	<para>${IS6_BufferQueryParameters_Description}</para>
    /// </summary>
    public class BufferQueryParameters : QueryParametersBase
    {
        /// <summary>${IS6_BufferQueryParameters_constructor_None_D}</summary>
        public BufferQueryParameters()
        { }
        /// <summary>${IS6_BufferQueryParameters_attribute_bufferParam_D}</summary>
        public BufferParam BufferParam { get; set; }
    }
}
