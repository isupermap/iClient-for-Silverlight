﻿
using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Windows;
using System.Json;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Core;
using System.Globalization;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_BufferQueryService_Title}</para>
    /// 	<para>${IS6_BufferQueryService_Description}</para>
    /// </summary>
    public class BufferQueryService : ServiceBase
    {
        /// <summary>${IS6_BufferQueryService_constructor_None_D}</summary>
        /// <overloads>${IS6_BufferQueryService_constructor_overloads_D}</overloads>
        public BufferQueryService()
        { }
        /// <summary>${IS6_BufferQueryService_constructor_String_D}</summary>
        /// <param name="url">${IS6_BufferQueryService_constructor_String_param_url}</param>
        public BufferQueryService(string url)
            : base(url)
        { }
        /// <summary>${IS6_BufferQueryService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_BufferQueryService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="parameters">${IS6_BufferQueryService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(BufferQueryParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${IS6_BufferQueryService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_BufferQueryService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_BufferQueryService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(BufferQueryParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("BufferQueryParameters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            base.SubmitRequest(base.Url + "/path.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);

        }

        private Dictionary<string, string> GetParameters(BufferQueryParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("map", parameters.MapName);
            if (parameters != null && parameters.BufferParam != null)
            {
                if (parameters.BufferParam.FromCustomGeo)
                {
                    dictionary.Add("method", "BufferQueryByGeometries");
                    dictionary.Add("geometries", GetGeosStr(parameters.BufferParam.Geometries));
                }
                else
                {
                    dictionary.Add("method", "BufferQueryByLayer");
                    dictionary.Add("layer", QueryLayer.ToJson(parameters.BufferParam.FromLayer));
                }

            }
            dictionary.Add("bufferQueryParam", GetQueryStr(parameters.BufferParam, parameters.QueryParam));
            //dictionary.Add("bufferParam", BufferParam.ToJson(parameters.BufferParam));
            //dictionary.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
            dictionary.Add("trackingLayerIndex", "-1");
            dictionary.Add("userID", string.Format("\"{0}\"", Guid.NewGuid()));

            return dictionary;
        }

        private string GetQueryStr(BufferParam bufferParam, QueryParam queryParam)
        {
            if (bufferParam != null && queryParam != null)
            {
                List<string> temp = new List<string>();
                temp.Add("\"bufferDistance\":" + bufferParam.Distance.ToString(CultureInfo.InvariantCulture));
                temp.Add("\"bufferSmoothness\":" + bufferParam.Smoothness.ToString(CultureInfo.InvariantCulture));
                //temp.Add("\bufferSourceHighlight\":"+queryParam.he)
                temp.Add("\"queryMode\":" + ((int)bufferParam.QueryMode).ToString(CultureInfo.InvariantCulture));
                temp.Add("\"queryParam\":" + QueryParam.ToJson(queryParam));
                temp.Add("\"returnBufferResult\":" + bufferParam.ReturnBufferResult.ToString(CultureInfo.InvariantCulture).ToLower(CultureInfo.InvariantCulture));
                string str = "{" + string.Format("{0}", string.Join(",", temp.ToArray())) + "}";

                return str;
            }
            else
            {
                return string.Empty;
            }
        }

        private string GetGeosStr(IList<Geometry> geos)
        {
            if (geos != null && geos.Count > 0)
            {
                List<string> temp = new List<string>();

                for (int i = 0; i < geos.Count; i++)
                {
                    temp.Add(ServerGeometry.ToJson(geos[i].ToServerGeometry()));
                }

                return string.Format("[{0}]", string.Join(",", temp.ToArray()));
            }
            else
            {
                return "null";
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {

            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            BufferResultSet result = BufferResultSet.FromJson(jsonObject);
            LastResult = result;
            BufferQueryEventArgs args = new BufferQueryEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(BufferQueryEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(
                    ProcessCompleted, new object[] { this, args });
            }
        }

        /// <summary>${IS6_BufferQueryService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<BufferQueryEventArgs> ProcessCompleted;

        private BufferResultSet lastResult;
        /// <summary>${IS6_BufferQueryService_attribute_lastResult_D}</summary>
        public BufferResultSet LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
    /// <summary>${IS6_BufferResultSet_Title}</summary>
    public class BufferResultSet
    {
        /// <summary>${IS6_BufferResultSet_constructor_None_D}</summary>
        public BufferResultSet()
        { }
        /// <summary>${IS6_BufferResultSet_attribute_ResultSet_D}</summary>
        public ResultSet ResultSet { get; internal set; }
        /// <summary>${IS6_BufferResultSet_attribute_BufferRegion_D}</summary>
        public GeoRegion BufferRegion { get; internal set; }

        internal static BufferResultSet FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            BufferResultSet result = new BufferResultSet();

            result.ResultSet = ResultSet.FromJson((JsonObject)jsonObject["resultSet"]);

            ServerGeometry serverGeometry = ServerGeometry.FromJson((JsonObject)jsonObject["bufferResult"]);
            if (serverGeometry != null)
            {
                result.BufferRegion = serverGeometry.ToGeoRegion();
            }
            return result;
        }
    }
    /// <summary>${IS6_BufferQueryEventArgs_Title}</summary>
    public sealed class BufferQueryEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_BufferQueryEventArgs_constructor_None_D}</summary>
        public BufferQueryEventArgs(BufferResultSet resultSet, string originResult, object userToken)
            : base(userToken)
        {
            BufferResultSet = resultSet;
            OriginResult = originResult;
        }
        /// <summary>${IS6_BufferQueryEventArgs_attribute_BufferResultSet_D}</summary>
        public BufferResultSet BufferResultSet { get; private set; }
        /// <summary>${IS6_BufferQueryEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}

