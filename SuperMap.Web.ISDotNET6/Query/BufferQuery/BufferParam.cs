﻿
using System.Collections.Generic;
using SuperMap.Web.Core;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_BufferParam_Title}</para>
    /// 	<para>${IS6_BufferParam_Description}</para>
    /// </summary>
    public class BufferParam
    {

        /// <summary>${IS6_BufferParam_constructor_None_D}</summary>
        public BufferParam()
        {
            QueryMode = SpatialQueryMode.ExtentOverlap;
        }

        //生成缓冲区的缓冲半径。
        /// <summary>${IS6_BufferParam_attribute_distance_D}</summary>
        public double Distance { get; set; }

        //判断是否通过自定义的几何对象来构建缓冲区。
        //（当传入Geometry时应该设为true）
        /// <summary>${IS6_BufferParam_attribute_fromCustomGeo_D}</summary>
        public bool FromCustomGeo { get; set; }

        /// <summary>${IS6_BufferParam_attribute_fromLayer_D}</summary>
        public QueryLayer FromLayer { get; set; }

        /// <summary>${IS6_BufferAnalystParam_attribute_geometries_D}</summary>
        public IList<Geometry> Geometries { get; set; }

        /// <summary>${IS6_BufferAnalystParam_attribute_queryMode_D}</summary>
        public SpatialQueryMode QueryMode { get; set; }

        /// <summary>${IS6_BufferAnalystParam_attribute_smoothness_D}</summary>
        public int Smoothness { get; set; }
        /// <summary>${IS6_BufferAnalystParam_attribute_ReturnBufferResult_D}</summary>
        public bool ReturnBufferResult { get; set; }

        internal static string ToJson(BufferParam param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"distance\":{0}", param.Distance));

            list.Add(string.Format("\"fromCustomGeo\":{0}", param.FromCustomGeo.ToString().ToLower()));

            if (param.FromLayer != null)
            {
                list.Add(string.Format("\"fromLayer\":{0}", QueryLayer.ToJson(param.FromLayer)));
            }
            else
            {
                list.Add(string.Format("\"fromLayer\":null"));
            }

            //外边让用户设置Geometry，但在内部要将其转为ServerGeometry
            if (param.Geometries != null && param.Geometries.Count > 0)
            {
                List<string> temp = new List<string>();

                for (int i = 0; i < param.Geometries.Count; i++)
                {
                    temp.Add(ServerGeometry.ToJson(param.Geometries[i].ToServerGeometry()));
                }

                list.Add(string.Format("\"geometries\":[{0}]", string.Join(",", temp.ToArray())));
            }
            else
            {
                list.Add("\"geometries\":null");
            }

            list.Add(string.Format("\"queryMode\":{0}", (int)param.QueryMode));
            list.Add(string.Format("\"smoothness\":{0}", param.Smoothness));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

    }
}
