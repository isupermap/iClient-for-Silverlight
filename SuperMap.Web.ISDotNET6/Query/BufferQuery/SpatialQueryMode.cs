﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_SpatialQueryMode_Title}</para>
    /// 	<para>${IS6_SpatialQueryMode_Description}</para>
    /// </summary>
    public enum SpatialQueryMode
    {
        /// <summary>${IS6_SpatialQueryMode_attribute_ExtentOverlap_D}</summary>
        ExtentOverlap = 0,
        /// <summary>${IS6_SpatialQueryMode_attribute_CommonPoint_D}</summary>
        CommonPoint = 1,
        /// <summary>${IS6_SpatialQueryMode_attribute_LineCross_D}</summary>
        LineCross = 2,
        /// <summary>${IS6_SpatialQueryMode_attribute_CommonLine_D}</summary>
        CommonLine = 3,
        /// <summary>${IS6_SpatialQueryMode_attribute_CommonPointOrLineCross_D}</summary>
        CommonPointOrLineCross = 4,
        /// <summary>${IS6_SpatialQueryMode_attribute_EdgeTouchOrAreaIntersect_D}</summary>
        EdgeTouchOrAreaIntersect = 5,
        /// <summary>${IS6_SpatialQueryMode_attribute_AreaIntersect_D}</summary>
        AreaIntersect = 6,
        /// <summary>${IS6_SpatialQueryMode_attribute_AreaIntersectNoEdgeTouch_D}</summary>
        AreaIntersectNoEdgeTouch = 7,
        /// <summary>${IS6_SpatialQueryMode_attribute_ContainedBy_D}</summary>
        ContainedBy = 8,
        /// <summary>${IS6_SpatialQueryMode_attribute_Containing_D}</summary>
        Containing = 9,
        /// <summary>${IS6_SpatialQueryMode_attribute_ContainedByNoEdgeTouch_D}</summary>
        ContainedByNoEdgeTouch = 10,
        /// <summary>${IS6_SpatialQueryMode_attribute_ContainingNoEdgeTouch_D}</summary>
        ContainingNoEdgeTouch = 11,
        /// <summary>${IS6_SpatialQueryMode_attribute_PointInPolygon_D}</summary>
        PointInPolygon = 12,
        /// <summary>${IS6_SpatialQueryMode_attribute_CentroidInPolygon_D}</summary>
        CentroidInPolygon = 13,
        /// <summary>${IS6_SpatialQueryMode_attribute_IDentical_D}</summary>
        IDentical = 14,
        /// <summary>${IS6_SpatialQueryMode_attribute_Tangent_D}</summary>
        Tangent = 15,
        /// <summary>${IS6_SpatialQueryMode_attribute_Overlap_D}</summary>
        Overlap = 16,
        /// <summary>${IS6_SpatialQueryMode_attribute_Disjoint_D}</summary>
        Disjoint = 17,
        /// <summary>${IS6_SpatialQueryMode_attribute_Touch_D}</summary>
        Touch = 18,
        /// <summary>${IS6_SpatialQueryMode_attribute_ContainOrOverlap_D}</summary>
        ContainOrOverlap = 19,
        /// <summary>${IS6_SpatialQueryMode_attribute_TouchNoCross_D}</summary>
        TouchNoCross = 20,
        /// <summary>${IS6_SpatialQueryMode_attribute_CommonLineOrOverlap_D}</summary>
        CommonLineOrOverlap = 21
    }
}
