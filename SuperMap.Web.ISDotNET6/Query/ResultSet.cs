﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ResultSet_Title}</para>
    /// 	<para>${IS6_ResultSet_Description}</para>
    /// </summary>
    public class ResultSet
    {
        private ResultSet()
        { }
        /// <summary>${IS6_ResultSet_attribute_currentCount_D}</summary>
        public int CurrentCount { get; private set; }
        /// <summary>${IS6_ResultSet_attribute_customResponse_D}</summary>
        public string CustomRespone { get; private set; }
        /// <summary>${IS6_Query_ResultSet_attribute_recordSets_D}</summary>
        public List<RecordSet> RecordSets { get; private set; }
        /// <summary>${IS6_ResultSet_attribute_totalCount_D}</summary>
        public int TotalCount { get; private set; }
        //public int TrackingLayerIndex { get; private set; }
        //public string UserID { get; private set; }

        /// <summary>${IS6_ResultSet_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_ResultSet_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_ResultSet_method_FromJson_return}</returns>
        public static ResultSet FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ResultSet resultSet = new ResultSet();

            resultSet.TotalCount = (int)jsonObject["totalCount"];
            resultSet.CurrentCount = (int)jsonObject["currentCount"];
            resultSet.CustomRespone = (string)jsonObject["customResponse"];

            JsonArray recordsets = (JsonArray)jsonObject["recordsets"];
            if (recordsets != null && recordsets.Count > 0)
            {
                resultSet.RecordSets = new List<RecordSet>();
                for (int i = 0; i < recordsets.Count; i++)
                {
                    resultSet.RecordSets.Add(RecordSet.FromJson((JsonObject)recordsets[i]));
                }
            }

            //resultSet.TrackingLayerIndex = (int)jsonObject["trackingLayerIndex"];
            //resultSet.UserID = (string)jsonObject["userID"];

            return resultSet;
        }
    }
}
