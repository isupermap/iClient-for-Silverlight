﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_QueryParam_Title}</para>
    /// 	<para>${IS6_QueryParam_Description}</para>
    /// </summary>
    public class QueryParam
    {
        /// <summary>${IS6_QueryParam_constructor_None_D}</summary>
        public QueryParam()
        {
            HasGeometry = true;
            QueryAllLayer = false;
            ReturnCenterAndBounds = true;
            ReturnShape = true;
            StartRecord = 0;
        }
        
		//public string CustomParams { get; set; }
        /// <summary>${IS6_QueryParam_attribute_expectCount_D}</summary>
        public int ExpectCount { get; set; }
        /// <summary>${IS6_QueryParam_attribute_hasGeometry_D}</summary>
        public bool HasGeometry { get; set; }
        /// <summary>${IS6_QueryParam_attribute_networkType_D}</summary>
        public ServerFeatureType NetworkType { get; set; }
        /// <summary>${IS6_QueryParam_attribute_queryAllLayer_D}</summary>
        public bool QueryAllLayer { get; set; }
        /// <summary>${IS6_QueryParam_attribute_queryLayers_D}</summary>
        public IList<QueryLayer> QueryLayers { get; set; }
        /// <summary>${IS6_QueryParam_attribute_returnCenterAndBounds_D}</summary>
        public bool ReturnCenterAndBounds { get; set; }
        /// <summary>${IS6_QueryParam_attribute_returnFields_D}</summary>
        public IList<string> ReturnFields { get; set; }
        /// <summary>${IS6_QueryParam_attribute_returnShape_D}</summary>
        public bool ReturnShape { get; set; }
        /// <summary>${IS6_QueryParam_attribute_startRecord_D}</summary>
        public int StartRecord { get; set; }
        /// <summary>${IS6_QueryParam_attribute_whereClause_D}</summary>
        public string WhereClause { get; set; }

        internal static string ToJson(QueryParam queryParam)
        {
            if (queryParam == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add("\"customParams\":\"\"");

            if (queryParam.ExpectCount > 0)
            {
                list.Add(string.Format("\"expectCount\":{0}", queryParam.ExpectCount));
            }
            else
            {
                list.Add("\"expectCount\":null");
            }
            list.Add(string.Format("\"hasGeometry\":{0}", queryParam.HasGeometry.ToString().ToLower()));

            list.Add(string.Format("\"highlight\":null"));


            list.Add(string.Format("\"queryAllLayer\":{0}", queryParam.QueryAllLayer.ToString().ToLower()));

            IList<QueryLayer> queryLayers = queryParam.QueryLayers;
            if (queryLayers != null && queryLayers.Count > 0)
            {
                List<string> layerParams = new List<string>();

                for (int i = 0; i < queryLayers.Count; i++)
                {
                    layerParams.Add(QueryLayer.ToJson(queryLayers[i]));
                }
                string temp = "[" + string.Join(",", layerParams.ToArray()) + "]";
                list.Add(string.Format("\"queryLayers\":{0}", temp));
            }

            list.Add(string.Format("\"networkType\":{0}", (int)queryParam.NetworkType));


            if (queryParam.ReturnFields != null && queryParam.ReturnFields.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < queryParam.ReturnFields.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", queryParam.ReturnFields[i]));
                }
                list.Add(string.Format("\"returnFields\":[{0}]", string.Join(",", temp.ToArray())));
            }
            else
            {
                list.Add("\"returnFields\":null");
            }


            list.Add(string.Format("\"startRecord\":{0}", queryParam.StartRecord));

            if (!string.IsNullOrEmpty(queryParam.WhereClause))
            {
                list.Add(string.Format("\"whereClause\":\"{0}\"", queryParam.WhereClause));
            }
            else
            {
                list.Add("\"whereClause\":\"\"");
            }

            list.Add(string.Format("\"returnCenterAndBounds\":{0}", queryParam.ReturnCenterAndBounds.ToString().ToLower()));
            list.Add(string.Format("\"returnShape\":{0}", queryParam.ReturnShape.ToString().ToLower()));


            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
