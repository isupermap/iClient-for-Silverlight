﻿
using SuperMap.Web.Core;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_QueryByGeometryParameters_Title}</para>
    /// 	<para>${IS6_QueryByGeometryParameters_Description}</para>
    /// </summary>
    public class QueryByGeometryParameters : QueryParametersBase
    {
        /// <summary>${IS6_QueryByGeometryParameters_constructor_None_D}</summary>
        public QueryByGeometryParameters()
        { }
        /// <summary>${IS6_QueryByGeometryParameters_attribute_geometry_D}</summary>
        public Geometry Geometry { get; set; }
    }
}
