﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_QueryByGeometryService_Title}</para>
    /// 	<para>${IS6_QueryByGeometryService_Description}</para>
    /// </summary>
    public class QueryByGeometryService : ServiceBase
    {
        /// <summary>${IS6_QueryByGeometryService_constructor_None_D}</summary>
        /// <overloads>${IS6_QueryByGeometryService_constructor_overloads_D}</overloads>
        public QueryByGeometryService()
        { }
        /// <summary>${IS6_QueryByGeometryService_constructor_String_D}</summary>
        /// <param name="url">${IS6_QueryByGeometryService_constructor_String_param_url}</param>
        public QueryByGeometryService(string url)
            : base(url)
        { }

        /// <summary>${IS6_QueryByGeometryService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_QueryByGeometryService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="paramters">${IS6_QueryByGeometryService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(QueryByGeometryParameters paramters)
        {
            ProcessAsync(paramters, null);
        }
        /// <summary>${IS6_QueryByGeometryService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_QueryByGeometryService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_QueryByGeometryService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(QueryByGeometryParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("QueryByGeometryParameters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            base.SubmitRequest(base.Url + "/query.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(QueryByGeometryParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Point2DCollection gps = new Point2DCollection();

            if (parameters.Geometry is GeoLine)
            {
                string method = "QueryByLine";
                dictionary.Add("map", parameters.MapName);
                dictionary.Add("method", method);

                foreach (Point2DCollection g in (parameters.Geometry as GeoLine).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
            }
            else if (parameters.Geometry is GeoRegion)
            {
                string method = "QueryByPolygen";
                dictionary.Add("map", parameters.MapName);
                dictionary.Add("method", method);

                foreach (Point2DCollection g in (parameters.Geometry as GeoRegion).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
            }
            else if (parameters.Geometry is GeoCircle)
            {
                string method = "QueryByPolygen";
                dictionary.Add("map", parameters.MapName);
                dictionary.Add("method", method);

                foreach (Point2DCollection g in (parameters.Geometry as GeoCircle).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
            }
            else if (parameters.Geometry is GeoPoint)
            {
                string method = "QueryByPoint";
                dictionary.Add("map", parameters.MapName);
                dictionary.Add("method", method);

                dictionary.Add("point", JsonHelper.FromPoint2D(((GeoPoint)parameters.Geometry).Bounds.Center));
                dictionary.Add("tolerance", "0");
                dictionary.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));

                dictionary.Add("trackingLayerIndex", "-1");
                dictionary.Add("userID", string.Format("\"{0}\"", Guid.NewGuid()));

                return dictionary;
            }


            dictionary.Add("points", JsonHelper.FromPoint2DCollection(gps));

            dictionary.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
            dictionary.Add("trackingLayerIndex", "-1");
            dictionary.Add("userID", string.Format("\"{0}\"", Guid.NewGuid()));

            return dictionary;

        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            ResultSet result = ResultSet.FromJson(jsonObject);
            LastResult = result;
            QueryServiceEventArgs args = new QueryServiceEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(QueryServiceEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(
                    ProcessCompleted, new object[] { this, args });
            }
        }

        /// <summary>${IS6_QueryByGeometryService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<QueryServiceEventArgs> ProcessCompleted;

        private ResultSet lastResult;
        /// <summary>${IS6_QueryByGeometryService_attribute_lastResult_D}</summary>
        public ResultSet LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
