﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_QueryParamertersBase_Title}</para>
    /// 	<para>${IS6_QueryParamertersBase_Description}</para>
    /// </summary>
    public class QueryParametersBase:ParametersBase
    {
        /// <summary>${IS6_QueryParamertersBase_constructor_None_D}</summary>
        public QueryParametersBase()
        { }
        /// <summary>${IS6_QueryParamertersBase_attribute_QueryParam_D}</summary>
        public QueryParam QueryParam { get; set; }
    }
}
