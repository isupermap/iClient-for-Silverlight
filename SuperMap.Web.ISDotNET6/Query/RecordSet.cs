﻿
using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_Recordset_Title}</para>
    /// 	<para>${IS6_Recordset_Description}</para>
    /// </summary>
    public class RecordSet
    {
        private RecordSet()
        {
        }
        /// <summary>${IS6_Recordset_attribute_layerID_D}</summary>
        public int LayerID { get; private set; }
        /// <summary>${IS6_Recordset_attribute_layerName_D}</summary>
        public string LayerName { get; private set; }
        /// <summary>${IS6_Recordset_attribute_records_D}</summary>
        public List<Record> Records { get; private set; }
        /// <summary>${IS6_Recordset_attribute_returnFieldAliases_D}</summary>
        public List<string> ReturnFieldAliases { get; private set; }
        /// <summary>${IS6_Recordset_attribute_returnFields_D}</summary>
        public List<string> ReturnFields { get; private set; }
        /// <summary>${IS6_Recordset_attribute_returnFieldTypes_D}</summary>
        public List<ServerFieldType> ReturnFieldTypes { get; private set; }

        /// <summary>${IS6_Recordset_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_Recordset_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_Recordset_method_FromJson_return}</returns>
        public static RecordSet FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            RecordSet recordSet = new RecordSet();

            recordSet.LayerID = (int)jsonObject["layerId"];
            recordSet.LayerName = (string)jsonObject["layerName"];

            JsonArray records = (JsonArray)jsonObject["records"];
            if (records != null && records.Count > 0)
            {
                recordSet.Records = new List<Record>();
                for (int i = 0; i < records.Count; i++)
                {
                    recordSet.Records.Add(Record.FromJson((JsonObject)records[i]));
                }
            }

            if (jsonObject.ContainsKey("returnFieldAliases"))
            {
                recordSet.ReturnFieldAliases = JsonHelper.ToStringList((JsonArray)jsonObject["returnFieldAliases"]);
            }

            if (jsonObject.ContainsKey("returnFields"))
            {
                recordSet.ReturnFields = JsonHelper.ToStringList((JsonArray)jsonObject["returnFields"]);
            }

            JsonArray fieldTypes = (JsonArray)jsonObject["returnFieldTypes"];
            if (fieldTypes != null && fieldTypes.Count > 0)
            {
                recordSet.ReturnFieldTypes = new List<ServerFieldType>();
                for (int i = 0; i < fieldTypes.Count; i++)
                {
                    ServerFieldType type = (ServerFieldType)((int)fieldTypes[i]);
                    recordSet.ReturnFieldTypes.Add(type);
                }
            }


            return recordSet;
        }

        /// <summary>${IS6_Recordset_method_toFeatureSet_D}</summary>
        public FeatureCollection ToFeatureSet()
        {
            FeatureCollection featureSet = new FeatureCollection();
            if (this.Records == null)
            {
                return featureSet;
            }
            foreach (Record record in this.Records)
            {
                Feature feature = new Feature();

                ServerGeometry shape = record.Shape;
                if (shape != null)
                {
                    switch (shape.Feature)
                    {
                        case ServerFeatureType.Unknown:
                            break;
                        case ServerFeatureType.Point:
                            feature.Geometry = shape.ToGeoPoint();
                            break;
                        case ServerFeatureType.Line:
                            feature.Geometry = shape.ToGeoLine();
                            break;
                        case ServerFeatureType.Polygon:
                            feature.Geometry = shape.ToGeoRegion();
                            break;
                        case ServerFeatureType.Text:
                            break;
                        case ServerFeatureType.Circle:
                            break;
                        case ServerFeatureType.Image:
                            break;
                        default:
                            feature.Geometry = null;
                            break;
                    }
                }
                if (record.FieldValues != null)
                {
                    for (int i = 0; i < this.ReturnFields.Count; i++)
                    {
                        feature.Attributes.Add(this.ReturnFields[i], record.FieldValues[i]);
                    }
                }
                featureSet.Add(feature);
            }
            return featureSet;
        }

    }
}
