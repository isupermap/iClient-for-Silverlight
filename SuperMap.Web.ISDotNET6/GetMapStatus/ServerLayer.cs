﻿
using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ServerLayer_Title}</para>
    /// 	<para>${IS6_ServerLayer_Description}</para>
    /// </summary>
    public class ServerLayer
    {
        internal ServerLayer()
        { }

        //属性
        //图层的标识号。从 1 开始。
        /// <summary>${IS6_ServerLayer_attribute_id_D}</summary>
        public int ID { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_name_D}</summary>
        public string Name { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_caption_D}</summary>
        public string Caption { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_serverLayerType_D}</summary>
        public ServerLayerType ServerLayerType { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_visible_D}</summary>
        public bool Visible { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_queryable_D}</summary>
        public bool Queryable { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_minScale_D}</summary>
        public double MinScale { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_maxScale_D}</summary>
        public double MaxScale { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_displayFilter_D}</summary>
        public string DisplayFilter { get; private set; }

        //专题图
        /// <summary>${IS6_ServerLayer_attribute_themeUnique_D}</summary>
        public ThemeUnique ThemeUnique { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_themeRange_D}</summary>
        public ThemeRange ThemeRange { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_themeLabel_D}</summary>
        public ThemeLabel ThemeLabel { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_themeDotDensity_D}</summary>
        public ThemeDotDensity ThemeDotDensity { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_themeGraduatedSymbol_D}</summary>
        public ThemeGraduatedSymbol ThemeGraduatedSymbol { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_themeGraph_D}</summary>
        public ThemeGraph ThemeGraph { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_themeGridRange_D}</summary>
        public ThemeGridRange ThemeGridRange { get; private set; }

        /// <summary>${IS6_ServerLayer_attribute_style_D}</summary>
        public ServerStyle Style { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_relQueryTableInfos_D}</summary>
        public List<RelQueryTableInfo> RelQueryTableInfos { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_displaySortClause_D}</summary>
        public string DisplaySortClause { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_rasterOpaqueRate_D}</summary>
        public int RasterOpaqueRate { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_rasterBrightness_D}</summary>
        public int RasterBrightness { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_rasterContrast_D}</summary>
        public int RasterContrast { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_gridNoDataValue_D}</summary>
        public int GridNoDataValue { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_gridNoDataValue_D}</summary>
        public ServerColor GridNoDataColor { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_symbolScalable_D}</summary>
        public bool SymbolScalable { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_symbolScaleDefinition_D}</summary>
        public double SymbolScaleDefinition { get; private set; }
        /// <summary>${IS6_ServerLayer_attribute_crossroadOptimized_D}</summary>
        public bool CrossroadOptimized { get; private set; }

        //方法
        /// <summary>${IS6_ServerLayer_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_ServerLayer_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_ServerLayer_method_FromJson_return}</returns>
        public static ServerLayer FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ServerLayer result = new ServerLayer();
            result.ID = (int)jsonObject["id"];
            result.Name = (string)jsonObject["name"];
            result.Caption = (string)jsonObject["caption"];
            result.ServerLayerType = (ServerLayerType)(int)jsonObject["type"];
            result.Visible = (bool)jsonObject["visible"];
            result.Queryable = (bool)jsonObject["queryable"];
            result.MinScale = (double)jsonObject["minScale"];
            result.MaxScale = (double)jsonObject["maxScale"];
            result.DisplayFilter = (string)jsonObject["displayFilter"];

            //专题图部分;
            result.ThemeUnique = ThemeUnique.FromJson((JsonObject)jsonObject["themeUnique"]);
            result.ThemeRange = ThemeRange.FromJson((JsonObject)jsonObject["themeRange"]);
            result.ThemeLabel = ThemeLabel.FromJson((JsonObject)jsonObject["themeLabel"]);
            result.ThemeDotDensity = ThemeDotDensity.FromJson((JsonObject)jsonObject["themeDotDensity"]);
            result.ThemeGraduatedSymbol = ThemeGraduatedSymbol.FromJson((JsonObject)jsonObject["themeGraduatedSymbol"]);
            result.ThemeGraph = ThemeGraph.Fromjson((JsonObject)jsonObject["themeGraph"]);
            result.ThemeGridRange = ThemeGridRange.FromJson((JsonObject)jsonObject["themeGridRange"]);

            //
            result.Style = ServerStyle.FromJson((JsonObject)jsonObject["style"]);

            if (jsonObject.ContainsKey("relQueryTableInfos") && jsonObject["relQueryTableInfos"] != null && jsonObject["relQueryTableInfos"].Count > 0)
            {
                result.RelQueryTableInfos = new List<RelQueryTableInfo>();
                for (int i = 0; i < jsonObject["relQueryTableInfos"].Count; i++)
                {
                    result.RelQueryTableInfos.Add(RelQueryTableInfo.FromJson((JsonObject)jsonObject["relQueryTableInfos"][i]));
                }
            }

            result.DisplaySortClause = (string)jsonObject["displaySortClause"];
            result.RasterOpaqueRate = (int)jsonObject["rasterOpaqueRate"];
            result.RasterBrightness = (int)jsonObject["rasterBrightness"];
            result.RasterContrast = (int)jsonObject["rasterContrast"];
            result.GridNoDataValue = (int)jsonObject["gridNoDataValue"];
            result.GridNoDataColor = ServerColor.FromJson((int)jsonObject["gridNoDataColor"]);
            result.SymbolScalable = (bool)jsonObject["symbolScalable"];
            result.SymbolScaleDefinition = (double)jsonObject["symbolScaleDefinition"];
            result.CrossroadOptimized = (bool)jsonObject["crossroadOptimized"];

            return result;
        }
    }
}
