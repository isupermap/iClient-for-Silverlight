﻿
namespace SuperMap.Web.ISDotNET6
{
    
    /// <summary>
    /// <para>${IS6_ServerLayerType_Title}</para>
    /// <para>${IS6_ServerLayerType_Description}</para>
    /// </summary>
    public enum ServerLayerType
    {
        /// <summary>${IS6_ServerLayerType_attribute_undefined_D}</summary>
        Undefined = 0,
        /// <summary>${IS6_ServerLayerType_attribute_point_D}</summary>
        Point = 1,
        /// <summary>${IS6_ServerLayerType_attribute_line_D}</summary>
        Line = 3,
        /// <summary>${IS6_ServerLayerType_attribute_network_D}</summary>
        Network = 4,
        /// <summary>${IS6_ServerLayerType_attribute_polygon_D}</summary>
        Polygon = 5,
        /// <summary>${IS6_ServerLayerType_attribute_text_D}</summary>
        Text = 7,
        /// <summary>${IS6_ServerLayerType_attribute_image_D}</summary>
        Image = 81,
        /// <summary>${IS6_ServerLayerType_attribute_mrSID_D}</summary>
        MrSID = 82,
        /// <summary>${IS6_ServerLayerType_attribute_grid_D}</summary>
        Grid = 83,
        /// <summary>${IS6_ServerLayerType_attribute_dem_D}</summary>
        DEM = 84,
        /// <summary>${IS6_ServerLayerType_attribute_ecw_D}</summary>
        ECW = 85,
        /// <summary>${IS6_ServerLayerType_attribute_wms_D}</summary>
        WMS = 86,
        /// <summary>${IS6_ServerLayerType_attribute_wcs_D}</summary>
        WCS = 87,
        /// <summary>${IS6_ServerLayerType_attribute_CAD_D}</summary>
        CAD = 149
    }
}
