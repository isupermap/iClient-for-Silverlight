﻿

using System;
using SuperMap.Web.ISDotNET6.Resources;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Json;
using System.Windows;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetMapStatusService_Title}</para>
    /// 	<para>${IS6_GetMapStatusService_Description}</para>
    /// </summary>
    public class GetMapStatusService : ServiceBase
    {
        /// <summary>${IS6_GetMapStatusService_constructor_None_D}</summary>
        /// <overloads>${IS6_GetMapStatusService_constructor_overloads_D}</overloads>
        public GetMapStatusService()
        { }
        /// <summary>${IS6_GetMapStatusService_constructor_String_D}</summary>
        /// <param name="url">${IS6_GetMapStatusService_constructor_String_param_url}</param>
        public GetMapStatusService(string url)
            : base(url)
        { }

        /// <summary>${IS6_GetMapStatusService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_GetMapStatusService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="parameters">${IS6_GetMapStatusService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(GetMapStatusParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_GetMapStatusService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_GetMapStatusService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_GetMapStatusService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GetMapStatusParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("MeasureParameters is Null");
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }
            base.SubmitRequest(base.Url + "/common.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(GetMapStatusParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "GetMapStatus";
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);
            dictionary.Add("tileSize", parameters.TileSize.ToString());
            dictionary.Add("returnLayers", parameters.ReturnLayers.ToString().ToLower());
            dictionary.Add("returnThemes", parameters.ReturnThemes.ToString().ToLower());

            return dictionary;

        }
        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            GetMapStatusResult result = GetMapStatusResult.FromJson(jsonObject);
            LastResult = result;
            GetMapStatusEventArgs args = new GetMapStatusEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(GetMapStatusEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        private GetMapStatusResult lastResult;
        /// <summary>${IS6_GetMapStatusService_attribute_lastResult_D}</summary>
        public GetMapStatusResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }

        /// <summary>${IS6_GetMapStatusService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetMapStatusEventArgs> ProcessCompleted;

    }
}
