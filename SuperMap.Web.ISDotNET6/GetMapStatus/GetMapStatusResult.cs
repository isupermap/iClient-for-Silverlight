﻿

using System.Json;
using SuperMap.Web.Core;
using System.Windows;
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{

    /// <summary>
    /// 	<para>${IS6_GetMapStatusResult_Title}</para>
    /// 	<para>${IS6_GetMapStatusResult_Description}</para>
    /// </summary>
    public class GetMapStatusResult
    {
        internal GetMapStatusResult()
        { }

        //属性
        /// <summary>${IS6_GetMapStatusResult_attribute_mapName_D}</summary>
        public string MapName { get; private set; }
        /// <summary>${IS6_GetMapStatusResult_attribute_mapBounds_D}</summary>
        public Rectangle2D MapBounds { get; private set; }
        /// <summary>${IS6_GetMapStatusResult_attribute_referViewBounds_D}</summary>
        public Rectangle2D ReferViewBounds { get; private set; }
        /// <summary>${IS6_GetMapStatusResult_attribute_referBounds_D}</summary>
        public Rectangle2D ReferBounds { get; private set; }
        /// <summary>${IS6_GetMapStatusResult_attribute_referScale_D}</summary>
        public double ReferScale { get; private set; }
        /// <summary>${IS6_GetMapStatusResult_attribute_referViewer_D}</summary>
        public Rect ReferViewer { get; private set; }

        /// <summary>${IS6_GetMapStatusResult_attribute_mapNames_D}</summary>
        public List<string> MapNames { get; private set; }
        /// <summary>${IS6_GetMapStatusResult_attribute_imageFormat_D}</summary>
        public string ImageFormat { get; private set; }
        /// <summary>${IS6_GetMapStatusResult_attribute_antiAlias_D}</summary>
        public bool AntiAlias { get; private set; }

        //Layer[]
        /// <summary>${IS6_GetMapStatusResult_attribute_serverLayers_D}</summary>
        public List<ServerLayer> ServerLayers { get; private set; }

        /// <summary>${IS6_GetMapStatusResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_GetMapStatusResult_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_GetMapStatusResult_method_FromJson_return}</returns>
        public static GetMapStatusResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            GetMapStatusResult result = new GetMapStatusResult();

            result.MapName = (string)jsonObject["mapName"];
            result.MapBounds = ToRectangle2D((JsonObject)jsonObject["mapBounds"]);

            result.ReferViewBounds = ToRectangle2D((JsonObject)jsonObject["referViewBounds"]);
            result.ReferBounds = ToRectangle2D((JsonObject)jsonObject["referBounds"]);
            result.ReferScale = (double)jsonObject["referScale"];
            result.ReferViewer = ToRect((JsonObject)jsonObject["referViewer"]);

            result.ImageFormat = (string)jsonObject["imageFormat"];
            result.AntiAlias = (bool)jsonObject["antiAlias"];


            //MapNames[]
            if (jsonObject.ContainsKey("mapNames") && jsonObject["mapNames"] != null)
            {
                result.MapNames = new List<string>();
                for (int i = 0; i < jsonObject["mapNames"].Count; i++)
                {
                    result.MapNames.Add((string)jsonObject["mapNames"][i]);
                }
            }

            //Layer[]
            if (jsonObject.ContainsKey("layers") && jsonObject["layers"] != null && jsonObject["layers"].Count > 0)
            {
                result.ServerLayers = new List<ServerLayer>();
                for (int i = 0; i < jsonObject["layers"].Count; i++)
                {
                    result.ServerLayers.Add(ServerLayer.FromJson((JsonObject)jsonObject["layers"][i]));
                }
            }

            return result;
        }

        internal static Rectangle2D ToRectangle2D(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return Rectangle2D.Empty;
            }
            double mbMinX = (double)jsonObject["leftBottom"]["x"];
            double mbMinY = (double)jsonObject["leftBottom"]["y"];
            double mbMaxX = (double)jsonObject["rightTop"]["x"];
            double mbMaxY = (double)jsonObject["rightTop"]["y"];
            return new Rectangle2D(mbMinX, mbMinY, mbMaxX, mbMaxY);
        }

        internal static Rect ToRect(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return Rect.Empty;
            }
            double viewerMinX = (double)jsonObject["leftTop"]["x"];
            double viewerMinY = (double)jsonObject["leftTop"]["y"];
            double viewerMaxX = (double)jsonObject["rightBottom"]["x"];
            double viewerMaxY = (double)jsonObject["rightBottom"]["y"];
            return new Rect(new Point(viewerMinX, viewerMinY), new Point(viewerMaxX, viewerMaxY));
        }
    }
}
