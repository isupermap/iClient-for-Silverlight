﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetMapStatusEventArgs_Title}</para>
    /// 	<para>${IS6_GetMapStatusEventArgs_Description}</para>
    /// </summary>
    public class GetMapStatusEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_GetMapStatusEventArgs_constructor_D}</summary>
        public GetMapStatusEventArgs(GetMapStatusResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_GetMapStatusEventArgs_attribute_result_D}</summary>
        public GetMapStatusResult Result { get; private set; }
        /// <summary>${IS6_GetMapStatusEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
