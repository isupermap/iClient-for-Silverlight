﻿
//ScottXu 2010/03/05

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_GetMapStatusParameters_Title}</para>
    /// <para>${IS6_GetMapStatusParameters_Description}</para>
    /// </summary>
    public class GetMapStatusParameters : ParametersBase
    {
        /// <summary>${IS6_GetMapStatusParameters_constructor_None_D}</summary>
        public GetMapStatusParameters()
        {

            TileSize = 256;
            ReturnLayers = true;
            ReturnThemes = true;
        }


        //地图对应的缓存参考范围的图幅大小。该参数值必须大于0。
        /// <summary>${IS6_GetMapStatusParameters_attribute_tileSize_D}</summary>
        public int TileSize { get; set; }

        /// <summary>${IS6_GetMapStatusParameters_attribute_returnLayers_D}</summary>
        public bool ReturnLayers { get; set; }

        /// <summary>${IS6_GetMapStatusParameters_attribute_returnThemes_D}</summary>
        public bool ReturnThemes { get; set; }
    }
}
