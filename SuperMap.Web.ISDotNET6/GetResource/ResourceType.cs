﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_ResourceType_Title} </summary>
    public enum ResourceType
    {
        /// <summary>${IS6_ResourceType_attribute_symbolLib_D}</summary>
        SymbolLib = 0,
        /// <summary>${IS6_ResourceType_attribute_lineStyleLib_D}</summary>
        LineStyleLib = 1,
        /// <summary>${IS6_ResourceType_attribute_symbolLib_D}</summary>
        FillStyleLib = 2,
    }
}
