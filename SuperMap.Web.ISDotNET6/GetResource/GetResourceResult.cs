﻿
using System.Json;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_GetResourceResult_Title}</para>
    /// <para>${IS6_GetResourceResult_Title}</para>
    /// </summary>
    public class GetResourceResult
    {
        internal GetResourceResult()
        { }

        /// <summary>${IS6_GetResourceResult_attribute_imageURL_D}</summary>
        public string ImageURL { get; private set; }
        /// <summary>${IS6_GetResourceResult_attribute_layerName}</summary>
        public string LayerName { get; private set; }
        /// <summary>${IS6_GetResourceResult_attribute_istheme}</summary>
        public bool IsTheme { get; private set; }

        /// <summary>${IS6_GetResourceResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_GetResourceResult_method_FromJson_param_jsonObject}</param>
        /// <param name="layerName">${IS6_GetResourceResult_method_FromJson_param_layerName}</param>
        /// <param name="istheme">${IS6_GetResourceResult_method_FromJson_param_istheme}</param>
        /// <returns>${IS6_GetResourceResult_method_FromJson_return}</returns>
        public static GetResourceResult FromJson(string jsonObject, string layerName, bool istheme)
        {
            return new GetResourceResult() { ImageURL = jsonObject, LayerName = layerName, IsTheme = istheme };
        }
    }
}
