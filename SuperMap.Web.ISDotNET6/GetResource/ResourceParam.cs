﻿
using System.Collections.Generic;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_ResourceParam_Title}</para>
    /// 	<para>${IS6_ResourceParam_Description}</para>
    /// </summary>
    public class ResourceParam
    {
        /// <summary>${IS6_ResourceParam_constructor_None_D}</summary>
        public ResourceParam()
        { }

        //定义返回来图片的宽高；
        /// <summary>${IS6_ResourceParam_attribute_height_D}</summary>
        public int Height { get; set; }
        /// <summary>${IS6_ResourceParam_attribute_width_D}</summary>
        public int Width { get; set; }

        //设置返回图片的格式；
        /// <summary>${IS6_ResourceParam_attribute_imageFormat_D}</summary>
        public ImageFormat ImageFormat { get; set; }

        //返回资源样式的类型；
        /// <summary>${IS6_ResourceParam_attribute_resourceType_D}</summary>
        public ResourceType ResourceType { get; set; }

        /// <summary>${IS6_ResourceParam_attribute_style_D}</summary>
        public ServerStyle Style { get; set; }

        //在这里加一个属性，用来控制图层名和资源图片对应起来；      
        /// <summary>${IS6_ResourceParam_attribute_LayerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${IS6_ResourceParam_attribute_IsTheme_D}</summary>
        public bool IsTheme { get; set; }


        internal static string ToJson(ResourceParam param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(param.Style)));
            list.Add(string.Format("\"resourceType\":{0}", (int)param.ResourceType));
            list.Add(string.Format("\"imageFormat\":{0}", (int)param.ImageFormat));
            list.Add(string.Format("\"width\":{0}", param.Width));
            list.Add(string.Format("\"height\":{0}", param.Height));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
