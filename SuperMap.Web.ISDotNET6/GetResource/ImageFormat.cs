﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>${IS6_ImageFormat_Title}</summary>
    public enum ImageFormat
    {
        //注视的部分是sl不支持的；但是其他客户端还是可以用到的；

        /// <summary>${IS6_ImageFormat_attribute_default_D}</summary>
        Default = 0,
        /// <summary>${IS6_ImageFormat_attribute_PNG_D}</summary>
        PNG = 1,
        /// <summary>${IS6_ImageFormat_attribute_JPG_D}</summary>
        JPG = 2,
        /// <summary>${IS6_ImageFormat_attribute_BMP_D}</summary>
        BMP = 3,
        //TIFF = 4,
        //GIF = 5
    }
}
