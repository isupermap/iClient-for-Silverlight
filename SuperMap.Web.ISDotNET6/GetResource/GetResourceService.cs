﻿
using System;
using SuperMap.Web.ISDotNET6.Resources;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_GetResourceService_Title}</para>
    /// 	<para>${IS6_GetResourceService_Description}</para>
    /// </summary>
    public class GetResourceService : ServiceBase
    {
        /// <summary>${IS6_GetResourceService_constructor_None_D}</summary>
        /// <overloads>${IS6_GetResourceService_constructor_overloads_D}</overloads>
        public GetResourceService()
        { }
        /// <summary>${IS6_GetResourceService_constructor_String_D}</summary>
        /// <param name="url">${IS6_GetResourceService_constructor_String_param_url}</param>
        public GetResourceService(string url)
            : base(url)
        { }

        /// <summary>${IS6_GetResourceService_method_ProcessAsync_D}</summary>
        /// <overloads>${IS6_GetResourceService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="parameters">${IS6_GetResourceService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(GetResourceParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_GetResourceService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${IS6_GetResourceService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${IS6_GetResourceService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GetResourceParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("MeasureParameters is Null");
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }

            p = parameters;
            istheme = parameters.ResourceParam.IsTheme;

            base.SubmitRequest(base.Url + "/common.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(GetResourceParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "GetResource";
            dictionary.Add("map", parameters.MapName);
            dictionary.Add("method", method);
            dictionary.Add("resourceParam", ResourceParam.ToJson(parameters.ResourceParam));

            return dictionary;
        }
        private GetResourceParameters p;
        private bool istheme;
        private void request_Completed(object sender, RequestEventArgs e)
        {
            //JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            GetResourceResult result;
            string str;
            if (p.ResourceParam.LayerName.Contains("@"))
            {
                str = p.ResourceParam.LayerName.Remove(p.ResourceParam.LayerName.IndexOf("@"));
                result = GetResourceResult.FromJson(e.Result, str, istheme);
            }
            else
            {
                result = GetResourceResult.FromJson(e.Result, p.ResourceParam.LayerName, istheme);
            }
            // LastResult = result;
            GetResourceEventArgs args = new GetResourceEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(GetResourceEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        //结果直接就是string，所以这些就不需要了；
        //private GetResourceResult lastResult;
        //public GetResourceResult LastResult
        //{
        //    get
        //    {
        //        return lastResult;
        //    }
        //    private set
        //    {
        //        lastResult = value;
        //        base.OnPropertyChanged("LastResult");
        //    }
        //}

        /// <summary>${IS6_GetResourceService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetResourceEventArgs> ProcessCompleted;
    }
}
