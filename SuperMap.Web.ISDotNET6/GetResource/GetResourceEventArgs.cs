﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_GetResourceEventArgs_Title}</para>
    /// <para>${IS6_GetResourceEventArgs_Description}</para>
    /// </summary>
    public class GetResourceEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_GetResourceEventArgs_constructor_D}</summary>
        public GetResourceEventArgs(GetResourceResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_GetResourceEventArgs_attribute_result_D}</summary>
        public GetResourceResult Result { get; private set; }
        /// <summary>${IS6_GetResourceEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
