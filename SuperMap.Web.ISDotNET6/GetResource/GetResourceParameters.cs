﻿
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// <para>${IS6_GetResourceParameters_Title}</para>
    /// <para>${IS6_GetResourceParameters_Description}</para>
    /// </summary>
    public class GetResourceParameters : ParametersBase
    {
        /// <summary>${IS6_GetResourceParameters_constructor_None_D}</summary>
        public GetResourceParameters()
        {
        }

        /// <summary>${IS6_GetResourceParameters_attribute_resourceParam_D}</summary>
        public ResourceParam ResourceParam { get; set; }
    }
}
