﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using SuperMap.Web.ISDotNET6;
using SuperMap.Web.Mapping;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace SuperMap.Web.Controls
{
    /// <summary>
    /// 	<para>${controls_Legend_Title}</para>
    /// 	<para>${controls_Legend_Description}</para>
    /// </summary>
    [TemplatePart(Name = "Root", Type = typeof(Grid))]
    [TemplatePart(Name = "MyItemsControl", Type = typeof(ItemsControl))]
    [TemplatePart(Name = "BtnRefresh", Type = typeof(Button))]
    public class Legend : Control
    {
        //字段
        #region
        private const string exInfo = "Layer为空或者不是IS的图层!";
        private const string exInfo2 = "缺少当图层为Text类型是的自定义的图片！";
        #endregion

        private Grid gRoot;
        /// <summary>${controls_Legend_field_itemsControl_D}</summary>
        public ItemsControl itemsControl;
        private Button btnRefresh;

        private string layersName;
        private string mapName;
        private string url;
        private int recordIfCompeleted = 0;
        private int layerCount;

        /// <summary>${controls_Legend_constructor_None_D}</summary>
        public Legend()
        {
            DefaultStyleKey = typeof(Legend);
        }

        /// <summary>${controls_Legend_method_onApplyTemplate_D}</summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            gRoot = GetTemplateChild("Root") as Grid;
            itemsControl = GetTemplateChild("MyItemsControl") as ItemsControl;
            btnRefresh = GetTemplateChild("BtnRefresh") as Button;
            GetLayerInfo();

            if (btnRefresh != null)
            {
                btnRefresh.Click += new RoutedEventHandler(BtnRefresh);
            }
        }

        private void BtnRefresh(object sender, RoutedEventArgs e)
        {
            if (itemsControl.Items.Count > 0)
            {
                itemsControl.Items.Clear();
            }
            GetLayerInfo();
        }

        //方法

        private void GetLayerInfo()
        {
            GetMapStatusParameters param = new GetMapStatusParameters();
            if (this.Layer is TiledCachedISLayer)
            {
                param.MapName = ((TiledCachedISLayer)Layer).MapName;
            }
            else if (this.Layer is TiledDynamicISLayer)
            {
                param.MapName = ((TiledDynamicISLayer)Layer).MapName;
            }
            else if (this.Layer is DynamicISLayer)
            {
                param.MapName = ((DynamicISLayer)Layer).MapName;
            }
            else
            {
                throw new ArgumentNullException(exInfo);
            }
            GetMapStatusService getMapStatusService = new GetMapStatusService(Layer.Url);
            getMapStatusService.ProcessAsync(param);
            getMapStatusService.ProcessCompleted += new EventHandler<GetMapStatusEventArgs>(getMapStatusService_ProcessCompleted);

            //触发Loading事件
            if (Loading != null)
            {
                Dispatcher.BeginInvoke(Loading, new object[] { this, EventArgs.Empty });
            }

            mapName = param.MapName;
            url = Layer.Url;
        }

        private void getMapStatusService_ProcessCompleted(object sender, GetMapStatusEventArgs e)
        {
            AddLegendItems(e.Result.ServerLayers);
        }

        private void AddLegendItems(List<ServerLayer> sls)
        {
            if (sls != null && sls.Count > 0)
            {
                gRoot.Visibility = Visibility.Visible;

                for (int i = 0; i < sls.Count; i++)
                {
                    layerCount = sls.Count;

                    GetResourceParameters param = new GetResourceParameters();
                    param.ResourceParam = new ResourceParam();
                    //判断ResourceType的类型；
                    bool isCustomed;
                    param.ResourceParam.ResourceType = ProcessResourceType(param, i, sls, out isCustomed);
                    //这个属性就是为了图例控件开的；
                    //目的是让图标和图层名对应起来；
                    param.ResourceParam.LayerName = sls[i].Name;
                    layersName = sls[i].Name;

                    //如果这里是自定义类型就不发出请求了；
                    if (!isCustomed)
                    {
                        param.MapName = mapName;
                        param.ResourceParam.Style = sls[i].Style;
                        param.ResourceParam.Height = IconSize;
                        param.ResourceParam.Width = IconSize;
                        param.ResourceParam.ImageFormat = ImageFormat.PNG;
                        GetResourceService ser = new GetResourceService(url);
                        ser.ProcessAsync(param);
                        ser.ProcessCompleted += new EventHandler<GetResourceEventArgs>(ser_ProcessCompleted);

                        if (sls[i].ThemeUnique != null)  //当有专题图时应该将其中显示；
                        {
                            for (int j = 0; j < sls[i].ThemeUnique.Displays.Count; j++)
                            {
                                GetResourceParameters pitems = new GetResourceParameters()
                                {
                                    MapName = mapName,
                                    ResourceParam = new ResourceParam()
                                    {
                                        Height = IconSize,
                                        Width = IconSize,
                                        ImageFormat = ImageFormat.PNG,
                                        LayerName = sls[i].ThemeUnique.ItemCaptions[j],
                                        ResourceType = param.ResourceParam.ResourceType,
                                        Style = sls[i].ThemeUnique.Displays[j],
                                        IsTheme = true,
                                    }
                                };
                                GetResourceService grs = new GetResourceService(url);
                                grs.ProcessAsync(pitems);
                                grs.ProcessCompleted += new EventHandler<GetResourceEventArgs>(ser_ProcessCompleted);
                            }
                        }
                        else if (sls[i].ThemeRange != null)
                        {
                            for (int k = 0; k < sls[i].ThemeRange.BreakValues.Count; k++)
                            {
                                GetResourceParameters pRItems = new GetResourceParameters()
                                {
                                    MapName = mapName,
                                    ResourceParam = new ResourceParam()
                                    {
                                        Height = IconSize,
                                        Width = IconSize,
                                        ImageFormat = ImageFormat.PNG,
                                        IsTheme = true,
                                        ResourceType = param.ResourceParam.ResourceType,
                                        Style = sls[i].ThemeRange.Displays[k],
                                        LayerName = sls[i].ThemeRange.BreakValues[k].ToString()
                                    }
                                };
                                GetResourceService grs = new GetResourceService(url);
                                grs.ProcessAsync(pRItems);
                                grs.ProcessCompleted += new EventHandler<GetResourceEventArgs>(ser_ProcessCompleted);
                            }
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(ImageUrl))
                        {
                            throw new ArgumentNullException(exInfo2);
                        }

                        if (layersName.Contains("@"))
                        {
                            layersName = layersName.Remove(layersName.IndexOf("@"));
                        }

                        AddImageAndName(ImageUrl, UriKind.Relative, layersName, false);

                    }
                }
            }
        }

        private void ser_ProcessCompleted(object sender, GetResourceEventArgs e)
        {
            AddImageAndName(e.Result.ImageURL, UriKind.Absolute, e.Result.LayerName, e.Result.IsTheme);
        }

        private ResourceType ProcessResourceType(GetResourceParameters param, int i, List<ServerLayer> s, out bool isCustom)
        {
            isCustom = false;
            if (s[i].ServerLayerType == ServerLayerType.Point)
            {
                return ResourceType.SymbolLib;
            }
            else if (s[i].ServerLayerType == ServerLayerType.Line || s[i].ServerLayerType == ServerLayerType.Network)
            {
                return ResourceType.LineStyleLib;
            }
            else if (s[i].ServerLayerType == ServerLayerType.Polygon)
            {
                return ResourceType.FillStyleLib;
            }
            else if (s[i].ServerLayerType == ServerLayerType.Text)
            {
                isCustom = true;
            }

            return param.ResourceParam.ResourceType;
        }

        private void AddImageAndName(string url, UriKind k, string layerName, bool ist)
        {
            Image img = new Image();
            img.Height = IconSize;
            img.Width = IconSize;
            img.Source = new BitmapImage(new Uri(url, k));

            TextBlock tb = new TextBlock();
            tb.Text = layerName;

            StackPanel sp = new StackPanel()
             {
                 Orientation = Orientation.Horizontal,
                 VerticalAlignment = VerticalAlignment.Center,
                 HorizontalAlignment = HorizontalAlignment.Left
             };
            sp.Children.Add(img);
            sp.Children.Add(tb);

            if (ist)
            {
                sp.Margin = new Thickness(IconSize, 0, 0, 0);
            }

            itemsControl.Items.Add(sp);

            #region 目的：确保最后一张图片加载之后再出发Completed事件；
            recordIfCompeleted++;
            if (recordIfCompeleted == layerCount && LoadCompleted != null)
            {
                //触发LoadComplete事件
                Dispatcher.BeginInvoke(LoadCompleted, new object[] { this, EventArgs.Empty });
            }
            #endregion
        }

        /// <summary>${controls_Legend_attribute_layer_D}</summary>
        public Layer Layer
        {
            get { return (Layer)GetValue(LayerProperty); }
            set { SetValue(LayerProperty, value); }
        }

        /// <summary>${controls_Legend_field_layerProperty_D}</summary>
        public static readonly DependencyProperty LayerProperty =
            DependencyProperty.Register("Layer", typeof(Layer), typeof(Legend), null);

        /// <summary>${controls_Legend_attribute_imageUrl_D}</summary>
        public string ImageUrl
        {
            get { return (string)GetValue(ImageUrlProperty); }
            set { SetValue(ImageUrlProperty, value); }
        }

        /// <summary>${controls_Legend_field_imageUrlProperty_D}</summary>
        public static readonly DependencyProperty ImageUrlProperty =
            DependencyProperty.Register("ImageUrl", typeof(string), typeof(Legend), null);

        /// <summary>${controls_Legend_attribute_iconSize_D}</summary>
        public int IconSize
        {
            get { return (int)GetValue(IconSizeProperty); }
            set { SetValue(IconSizeProperty, value); }
        }

        /// <summary>${controls_Legend_field_iconSizeProperty_D}</summary>
        public static readonly DependencyProperty IconSizeProperty =
            DependencyProperty.Register("IconSize", typeof(int), typeof(Legend), new PropertyMetadata(16));

        //事件
        /// <summary>${controls_Legend_event_loading_D}</summary>
        public event EventHandler Loading;
        /// <summary>${controls_Legend_event_loadCompleted_D}</summary>
        public event EventHandler LoadCompleted;

    }
}
