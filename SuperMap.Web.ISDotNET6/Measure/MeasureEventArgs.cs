﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_MeasureEventArgs_Title}</para>
    /// 	<para>${IS6_MeasureEventArgs_Description}</para>
    /// </summary>
    public class MeasureEventArgs : ServiceEventArgs
    {
        /// <summary>${IS6_MeasureEventArgs_constructor_D}</summary>
        /// <param name="result">${IS6_MeasureEventArgs_constructor_param_result}</param>
        /// <param name="originResult">${IS6_MeasureEventArgs_constructor_param_originResult}</param>
        /// <param name="token">${IS6_MeasureEventArgs_constructor_param_token}</param>
        public MeasureEventArgs(MeasureResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${IS6_MeasureEventArgs_attribute_result_D}</summary>
        public MeasureResult Result { get; private set; }
        /// <summary>${IS6_MeasureEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
