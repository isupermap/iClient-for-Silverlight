﻿using System.Json;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_MeasureResult_Title}</para>
    /// 	<para>${IS6_MeasureResult_Description}</para>
    /// </summary>
    public class MeasureResult
    {
        //{"area":0,"distance":1740.86903585537,"trackingLayerIndex":1,"userID":"307ca0a3-3962-444d-9442-14f7e6a50835"}
        internal MeasureResult()
        { }
        /// <summary>${IS6_MeasureResult_attribute_area_D}</summary>
        public double Area { get; private set; }
        /// <summary>${IS6_MeasureResult_attribute_distance_D}</summary>
        public double Distance { get; private set; }       
        //public int TrackingLayerIndex { get; private set; }
        //public string UserID { get; private set; }

        /// <summary>${IS6_MeasureResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${IS6_MeasureResult_method_FromJson_param_jsonObject}</param>
        /// <returns>${IS6_MeasureResult_method_FromJson_return}</returns>
        public static MeasureResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            MeasureResult result = new MeasureResult();
            result.Distance = (double)jsonObject["distance"];
            result.Area = (double)jsonObject["area"];
            //result.TrackingLayerIndex = (int)jsonObject["trackingLayerIndex"];
            //result.UserID = jsonObject["userID"];
            return result;
        }
    }
}
