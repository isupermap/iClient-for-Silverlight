﻿
using System.Collections.Generic;
using SuperMap.Web.Core;
using System.Json;
using System;
using System.Windows.Browser;
using System.Windows;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_MeasureService_Title}</para>
    /// 	<para>${IS6_MeasureService_Description}</para>
    /// </summary>
    public class MeasureService : ServiceBase
    {
        /// <summary>${IS6_MeasureService_constructor_None_D}</summary>
        /// <overloads>${IS6_MeasureService_constructor_overloads}</overloads>
        public MeasureService()
        { }
        /// <summary>${IS6_MeasureService_constructor_String_D}</summary>
        /// <param name="url">${IS6_MeasureService_constructor_String_param_url}</param>
        public MeasureService(string url)
            : base(url)
        {
        }

        /// <summary>${IS6_MeasureService_method_processAsync_D}</summary>
        /// <overloads>${IS6_MeasureService_method_processAsync_overloads}</overloads>
        /// <param name="parameters">${IS6_MeasureService_method_processAsync_param_parameters}</param>
        public void ProcessAsync(MeasureParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${IS6_MeasureService_method_processAsync_D}</summary>
        /// <param name="parameters">${IS6_MeasureService_method_processAsync_param_parameters}</param>
        /// <param name="state">${IS6_MeasureService_method_processAsync_param_state}</param>
        public void ProcessAsync(MeasureParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                //throw new ArgumentNullException("MeasureParameters is Null");
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                //throw new InvalidOperationException("Url is not set");
            }
            base.SubmitRequest(base.Url + "/common.ashx?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(MeasureParameters parameters)
        {

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Point2DCollection gps = new Point2DCollection();

            if (parameters.Geometry is GeoLine)
            {
                string method = "MeasureDistance";
                dictionary.Add("map", parameters.MapName);
                dictionary.Add("method", method);
                foreach (Point2DCollection g in (parameters.Geometry as GeoLine).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
                dictionary.Add("points", JsonHelper.FromPoint2DCollection(gps));

                dictionary.Add("trackingLayerIndex", "-1");
                dictionary.Add("userID", Guid.NewGuid().ToString());
                dictionary.Add("isHighlight", "false");

            }
            else if (parameters.Geometry is GeoRegion)
            {
                string method = "MeasureArea";
                dictionary.Add("map", parameters.MapName);
                dictionary.Add("method", method);

                foreach (Point2DCollection g in (parameters.Geometry as GeoRegion).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
                dictionary.Add("points", JsonHelper.FromPoint2DCollection(gps));

                dictionary.Add("trackingLayerIndex", "-1");
                dictionary.Add("userID", Guid.NewGuid().ToString());
                dictionary.Add("isHighlight", "false");
            }
            return dictionary;
        }


        private MeasureResult lastResult;

        /// <summary>${IS6_MeasureService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<MeasureEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            MeasureResult result = MeasureResult.FromJson(jsonObject);
            LastResult = result;
            MeasureEventArgs args = new MeasureEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(MeasureEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${IS6_MeasureService_attribute_lastResult_D}</summary>
        public MeasureResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}

