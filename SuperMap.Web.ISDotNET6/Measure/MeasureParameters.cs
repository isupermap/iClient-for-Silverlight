﻿
using SuperMap.Web.Core;
namespace SuperMap.Web.ISDotNET6
{
    /// <summary>
    /// 	<para>${IS6_MeasureParameter_Title}</para>
    /// 	<para>${IS6_MeasureParameter_Description}</para>
    /// </summary>
    public class MeasureParameters : ParametersBase
    {
        /// <summary>${IS6_MeasureParameter_constructor_None_D}</summary>
        public MeasureParameters()
        { }
        /// <summary>${IS6_MeasureParameter_attribute_Geometry_D}</summary>
        public Geometry Geometry { get; set; }
    }
}
