﻿
using System;
using System.ComponentModel;
using System.Globalization;
using System.Security;
using System.Text;
using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Utilities;


namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// 	<para>${mapping_TiledCachedISLayer_Title}</para>
    /// 	<para>${mapping_TiledCachedISLayer_Description}</para>
    /// </summary>
    public class TiledCachedISLayer : TiledCachedLayer
    {
        private string uriFormat;
        private SmMapService mapService;
        private bool isInitializing;

        /// <summary>${mapping_TiledCachedISLayer_constructor_None_D}</summary>
        public TiledCachedISLayer( )
        {
            ImageFormat = "png";
            TileSize = 512;
            AntiAliasing = false;
            Transparent = false;
            Redirect = true;
            AdjustFactor = 1.0;
            CachedUrl = string.Empty;
        }

        /// <summary>${mapping_TiledCachedISLayer_method_getTileUrl_D}</summary>
        /// <returns>${mapping_TiledCachedISLayer_method_returns}</returns>
        /// <param name="indexX">${mapping_TiledCachedISLayer_method_param_indexX_D}</param>
        /// <param name="indexY">${mapping_TiledCachedISLayer_method_param_indexY_D}</param>
        /// <param name="level">${mapping_TiledCachedISLayer_method_param_level_D}</param>
        public override string GetTileUrl(int indexX , int indexY , int level)
        {
            string uri = string.Empty;
            if (!string.IsNullOrEmpty(this.uriFormat))
            {
                if (string.IsNullOrEmpty(CachedUrl))
                {
                    double scale = Scales[level];
                    //保证精度和IS一致
                    string tempScale = scale.ToString("R" , NumberFormatInfo.InvariantInfo);
                    uri = String.Format(this.uriFormat , tempScale , indexX , indexY);
                }
                else
                {
                    double scale = 1 / Scales[level];
                    uri = String.Format(this.uriFormat , scale , indexX , indexY);
                }
            }
            return uri;
        }

        /// <summary>${mapping_TiledCachedISLayer_method_initialize_D}</summary>
        public override void Initialize( )
        {
            if (!this.isInitializing && !base.IsInitialized)
            {
                if (string.IsNullOrEmpty(this.Url))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.InvalidUrl);
                    base.Initialize();
                    return;
                }
                if (string.IsNullOrEmpty(this.MapName))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.MapNameIsNull);
                    base.Initialize();
                    return;
                }
                if (this.Scales == null)
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.ScalesIsNull);
                    base.Initialize();
                    return;
                }

                if (!Url.Contains("http://"))  //相对地址
                {
                    var pageUrl = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                    var localUrl = pageUrl.AbsoluteUri.Substring(0 , pageUrl.AbsoluteUri.IndexOf(pageUrl.AbsolutePath));
                    Url = localUrl + Url;
                }

                if (IsSkipGetSMMapServiceInfo)
                {
                    if (Bounds.IsEmpty)
                    {
                        Error = new ArgumentNullException("Bounds");
                    }
                    Dpi = ScaleHelper.GetSmDpi(ReferViewBounds , ReferViewer , ReferScale);
                    Dpi *= AdjustFactor;

                    this.isInitializing = true;

                    double[] resolutions = new double[Scales.Length];
                    for (int i = 0 ; i < Scales.Length ; i++)
                    {
                        resolutions[i] = ScaleHelper.ScaleConversion(Scales[i] , this.Dpi);
                    }
                    this.Resolutions = resolutions;

                    base.Initialize();
                    CreateUrl();
                    return;
                }

                this.isInitializing = true;
                this.mapService = new SmMapService(this.Url.Trim() , this.MapName);
                this.mapService.Initialized += new EventHandler<SmMapService.MapServiceInitalizeArgs>(mapService_Initialized);
                this.mapService.Failed += new SmMapService.MapServiceFaultEventHandler(mapService_Failed);
                this.mapService.Initialize();
                CreateUrl();
            }
        }

        private void CreateUrl( )
        {

            #region 构造一下真正的url
            StringBuilder builder = new StringBuilder();
            //当CachedUrl不设置时，就是用IS快速缓存；
            if (string.IsNullOrEmpty(CachedUrl))
            {
                builder.Append(this.Url);
                if (!Url.EndsWith("/"))
                {
                    builder.Append("/");
                }
                builder.Append("ajax/");
                builder.Append(this.MapName);
                builder.Append("/{0}/{1}/{2}/");
                builder.AppendFormat("{0}/" , TileSize);
                builder.Append(ImageFormat.ToString().ToLower());
                builder.Append("/0/");
                builder.Append(this.AntiAliasing.ToString().ToLower());
                builder.Append("/true/map.ashx?");
                builder.AppendFormat("redirect={0}" , this.Redirect.ToString().ToLower());
                builder.Append("&");
                builder.AppendFormat("transparent={0}" , this.Transparent.ToString().ToLower());
            }
            else//当设置CachedUrl时，就使用IS的简易缓存
            {
                builder.Append(this.CachedUrl);
                if (!CachedUrl.EndsWith("/"))
                {
                    CachedUrl += "/";
                }
                builder.AppendFormat("{0}_{1}x{1}" , this.MapName , this.TileSize);
                builder.Append("/{0}/{1}/{2}.");
                builder.AppendFormat("{0}" , ImageFormat);
            }
            #endregion

            this.uriFormat = builder.ToString();

        }

        private void mapService_Failed(object sender , SmMapService.MapServiceFaultEventArgs args)
        {
            if (( ( args.Error is SecurityException ) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttp) ) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttps))
            {
                base.Error = new SecurityException(SuperMap.Web.Resources.ExceptionStrings.InvalidURISchemeHost , args.Error);
            }
            else
            {
                base.Error = args.Error;
            }
            base.Initialize();
        }

        private void mapService_Initialized(object sender , SmMapService.MapServiceInitalizeArgs e)
        {
            if (e.MapService.MapServiceUrl == this.Url.Trim())
            {
                if (base.Error != null)
                {
                    base.Initialize();
                }
                else
                {
                    this.Bounds = this.mapService.MapServiceInfo.MapBounds;
                    Dpi = ScaleHelper.GetSmDpi(this.mapService.MapServiceInfo.ReferViewBounds , this.mapService.MapServiceInfo.ReferViewer , this.mapService.MapServiceInfo.ReferScale);
                    Dpi *= AdjustFactor;

                    double[] resolutions = new double[Scales.Length];
                    for (int i = 0 ; i < Scales.Length ; i++)
                    {
                        resolutions[i] = ScaleHelper.ScaleConversion(Scales[i] , this.Dpi);
                    }
                    this.Resolutions = resolutions;

                    base.Initialize();
                }
            }
        }


        //属性
        /// <summary>${mapping_TiledCachedISLayer_attribute_CachedUrl_D}</summary>
        public string CachedUrl { get; set; }

        private double[] scales = null;

        /// <summary>${mapping_TiledCachedISLayer_attribute_scales_D}</summary>
        [TypeConverter(typeof(DoubleArrayConverter))]
        public double[] Scales
        {
            get { return scales; }
            set
            {
                if (value[0] > 1)
                {
                    for (int i = 0 ; i < value.Length ; i++)
                    {
                        value[i] = 1.0 / value[i];
                    }
                }
                scales = value;
            }
        }
        /// <summary>${mapping_TiledCachedISLayer_attribute_adjustFactor_D}</summary>    
        public double AdjustFactor { get; set; }

        /// <summary>${mapping_TiledCachedISLayer_attribute_mapName_D}</summary>
        public string MapName { get; set; }
        /// <summary>${mapping_TiledCachedISLayer_attribute_redirect_D}</summary>
        public bool Redirect { get; set; }
        /// <summary>${mapping_TiledCachedISLayer_attribute_antiAliasing_D}</summary>
        public bool AntiAliasing { get; set; }
        /// <summary>${Mapping_DynamicRESTLayer_attribute_isScaleCentric_D}</summary>
        public override bool IsScaleCentric
        {
            get
            {
                return true;
            }
        }

        //IsSkipGetSMMapServiceInfo默认是false，
        //如果设置为true，则Bounds、ReferViewBounds、ReferViewer、ReferScale必须设置；
        //CRS此时为空，要用到与CRS相关时，需要设置CRS；
        /// <summary>${Mapping_DynamicRESTLayer_attribute_IsSkipGetSMMapServiceInfo_D}</summary>      
        public bool IsSkipGetSMMapServiceInfo { get; set; }
        /// <summary>${Mapping_DynamicRESTLayer_attribute_ReferViewBounds_D}</summary>      
        public Rectangle2D ReferViewBounds { get; set; }
        /// <summary>${Mapping_DynamicRESTLayer_attribute_ReferViewer_D}</summary>
        public Rect ReferViewer { get; set; }
        /// <summary>${Mapping_DynamicRESTLayer_attribute_ReferScale_D}</summary>
        public double ReferScale { get; set; }

    }
}
