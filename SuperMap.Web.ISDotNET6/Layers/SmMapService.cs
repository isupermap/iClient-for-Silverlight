﻿using System;
using System.Json;
using System.Net;
using System.Security;
using SuperMap.Web.Resources;

namespace SuperMap.Web.Mapping
{
    internal class SmMapService
    {
        private SmMapServiceInfo mapServiceInfo;
        private string mapServiceUrl;
        private string mapName;

        //构造函数
        public SmMapService(string mapServiceUrl, string mapName)
        {
            this.mapServiceUrl = mapServiceUrl;
            this.mapName = mapName;
        }

        //方法
        public void Initialize()
        {
            string url = String.Format("{0}/common.ashx?method=GetMapStatus&map={1}", this.mapServiceUrl, this.mapName);
            WebClient wc = new WebClient();
            wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler(wc_DownloadStringCompleted);
            wc.DownloadStringAsync(new Uri(url, UriKind.Absolute), this);
        }

        private void wc_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            Exception ex = null;
            if (!this.CheckForFault(e, out ex))
            {
                JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);

                mapServiceInfo = SmMapServiceInfo.FromJson(jsonObject);

                if ((this.Initialized != null) && (this.mapServiceInfo != null))
                {
                    this.Initialized(this, new MapServiceInitalizeArgs { MapService = this });
                }
            }
        }

        private bool CheckForFault(DownloadStringCompletedEventArgs e, out Exception ex)
        {
            if (e.Cancelled)
            {
                if (this.Failed != null)
                {
                    this.Failed(this, new MapServiceFaultEventArgs { Cancelled = true });
                }
                ex = null;
                return true;
            }
            if (e.Error != null)
            {
                Exception inner = e.Error;
                if (this.Failed != null)
                {
                    if (inner is SecurityException)
                    {
                        inner = new SecurityException(SuperMap.Web.Resources.ExceptionStrings.PolicyFileAvailable, inner);
                    }
                    this.Failed(this, new MapServiceFaultEventArgs { Error = inner });
                }
                ex = inner;
                return true;
            }
            if (e.Result == "null" || String.IsNullOrEmpty(e.Result))
            {
                Exception inner = null;
                if (this.Failed != null)
                {
                    inner = new Exception(ExceptionStrings.ServiceStarted);
                    this.Failed(this, new MapServiceFaultEventArgs { Error = inner });
                }
                ex = inner;
                return true;
            }
            ex = null;
            return false;
        }


        //属性
        public SmMapServiceInfo MapServiceInfo
        {
            get
            {
                return this.mapServiceInfo;
            }
        }

        public string MapServiceUrl
        {
            get
            {
                return this.mapServiceUrl;
            }
        }

        public string MapName
        {
            get
            {
                return this.mapName;
            }
        }


        //事件
        internal class MapServiceInitalizeArgs : EventArgs
        {
            public SmMapService MapService { get; set; }
        }
        internal event EventHandler<MapServiceInitalizeArgs> Initialized;


        internal class MapServiceFaultEventArgs : EventArgs
        {
            public bool Cancelled { get; internal set; }

            public Exception Error { get; internal set; }
        }
        internal delegate void MapServiceFaultEventHandler(object sender, MapServiceFaultEventArgs args);
        internal event MapServiceFaultEventHandler Failed;


    }
}
