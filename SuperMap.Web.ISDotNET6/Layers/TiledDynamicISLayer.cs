﻿using System;
using System.Security;
using System.Text;
using System.Windows;
using SuperMap.Web.Core;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// 	<para>${mapping_TiledDynamicISLayer_Title}</para>
    /// 	<para>${mapping_TiledDynamicISLayer_Description}</para>
    /// </summary>
    public class TiledDynamicISLayer : TiledDynamicLayer//, ISuperMapLayer
    {
        private bool isInitializing;
        private SmMapService mapService;
        private string uriFormat;

        /// <summary>${mapping_TiledDynamicISLayer_constructor_None_D}</summary>
        public TiledDynamicISLayer()
        {
            ImageFormat = "png";
            TileSize = 512;
            AntiAliasing = false;
            Transparent = false;
            Redirect = true;
            AdjustFactor = 1.0;
        }

        /// <summary>${mapping_TiledDynamicLayer_method_getTileUrl_D}</summary>
        /// <returns>${mapping_TiledDynamicLayer_method_getTileUrl_param_return}</returns>
        /// <param name="indexX">${mapping_TiledDynamicLayer_method_getTileUrl_param_indexX}</param>
        /// <param name="indexY">${mapping_TiledDynamicLayer_method_getTileUrl_param_indexY}</param>
        /// <param name="resolution">${mapping_TiledDynamicLayer_method_getTileUrl_param_resolution}</param>
        public override string GetTileUrl(int indexX, int indexY, double resolution)
        {
            string uri = string.Empty;
            if (!string.IsNullOrEmpty(this.uriFormat))
            {
                double scale = ScaleHelper.ScaleConversion(resolution, this.Dpi,this.CRS) ;
                uri = String.Format(this.uriFormat, scale, indexX, indexY);

                //if (base.BitmapCreateOptions != BitmapCreateOptions.None)
                //{
                uri += string.Format("&t={0}", DateTime.Now.Ticks.ToString());
                //}

            }
            //return System.Windows.Browser.HttpUtility.UrlEncode(uri);
            return uri;
        }

        /// <summary>${mapping_TiledCachedIServerLayer_method_initialize_D}</summary>
        public override void Initialize()
        {
            if (!this.isInitializing && !base.IsInitialized)
            {
                #region 必设参数判断
                if (string.IsNullOrEmpty(this.Url))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.InvalidUrl);
                    base.Initialize();
                    return;
                }
                if (string.IsNullOrEmpty(this.MapName))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.MapNameIsNull);
                    base.Initialize();
                    return;
                }
                #endregion

                if (!Url.Contains("http://"))  //相对地址
                {
                    var pageUrl = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                    var localUrl = pageUrl.AbsoluteUri.Substring(0, pageUrl.AbsoluteUri.IndexOf(pageUrl.AbsolutePath));
                    Url = localUrl + Url;
                }

                if (IsSkipGetSMMapServiceInfo)
                {
                    if (Bounds.IsEmpty)
                    {
                        Error = new ArgumentNullException("Bounds");
                    }
                    Dpi = ScaleHelper.GetSmDpi(ReferViewBounds, ReferViewer, ReferScale);
                    Dpi *= AdjustFactor;

                    this.isInitializing = true;

                    base.Initialize();
                    CreateUrl();
                    return;
                }

                this.isInitializing = true;
                this.mapService = new SmMapService(this.Url.Trim(), this.MapName);
                this.mapService.Initialized += new EventHandler<SmMapService.MapServiceInitalizeArgs>(mapService_Initialized);
                this.mapService.Failed += new SmMapService.MapServiceFaultEventHandler(mapService_Fault);
                this.mapService.Initialize();
                CreateUrl();

            }
        }

        private void CreateUrl()
        {

            // http://192.168.172.11:8080/IS/AjaxDemo/ajax/changchun/50000/0/1/512/png/0/false/true/map.ashx?redirect=true&transparent=false

            //http://192.168.172.11:8080/IS/AjaxScriptsSamples/ajax/World/5.15671655275828e-9/4/2/256/
            //gif/34GS7Y46LWRTL1D6K0ADCFXQHE/false/true/map.ashx?t=1263190743820&redirect=true

            //[MapHandler 服务地址]/ajax/[地图名称]/[地图的显示比例尺]/[图幅列号]/[图幅行号]/[图幅大小]/[图片格式]/
            //[图层标识]/[是否反走样]/[是否使用地图对应的缓存参考范围来获取地图图幅地址]/[MapHandler 的映射文件名称]?
            //redirect=[true/false]
            #region 构造一下真正的url
            StringBuilder builder = new StringBuilder();
            builder.Append(this.Url);

            if (!Url.EndsWith("/"))
            {
                builder.Append("/");
            }
            builder.Append("ajax/");
            builder.Append(this.MapName);
            builder.Append("/{0}/{1}/{2}/");
            builder.AppendFormat("{0}/", TileSize);
            builder.Append(ImageFormat.ToString().ToLower());
            builder.AppendFormat("/{0}/", LayersKey);
            builder.Append(this.AntiAliasing.ToString().ToLower());
            builder.Append("/true/map.ashx?");
            builder.AppendFormat("redirect={0}", this.Redirect.ToString().ToLower());
            builder.Append("&");
            builder.AppendFormat("transparent={0}", this.Transparent.ToString().ToLower());
            #endregion

            this.uriFormat = builder.ToString();
        }

        private void mapService_Fault(object sender, SmMapService.MapServiceFaultEventArgs args)
        {
            if (((args.Error is SecurityException) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttp)) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttps))
            {
                base.Error = new SecurityException(SuperMap.Web.Resources.ExceptionStrings.InvalidURISchemeHost, args.Error);
            }
            else
            {
                base.Error = args.Error;
            }
            base.Initialize();
        }

        private void mapService_Initialized(object sender, SmMapService.MapServiceInitalizeArgs e)
        {
            if (e.MapService.MapServiceUrl == this.Url.Trim())
            {
                if (base.Error != null)
                {
                    base.Initialize();
                }
                else
                {
                    this.Bounds = this.mapService.MapServiceInfo.MapBounds;
                    if (this.mapService.MapServiceInfo.CRS != null)
                    {
                        CRS = this.mapService.MapServiceInfo.CRS;
                        Dpi = ScaleHelper.GetSmDpi(this.mapService.MapServiceInfo.ReferViewBounds, this.mapService.MapServiceInfo.ReferViewer, this.mapService.MapServiceInfo.ReferScale, this.mapService.MapServiceInfo.CRS.Unit, this.mapService.MapServiceInfo.CRS.DatumAxis);
                    }
                    else if (CRS != null)
                    {
                        Dpi = ScaleHelper.GetSmDpi(this.mapService.MapServiceInfo.ReferViewBounds, this.mapService.MapServiceInfo.ReferViewer, this.mapService.MapServiceInfo.ReferScale, this.CRS.Unit, this.CRS.DatumAxis);
                    }
                    else
                    {
                        Dpi = ScaleHelper.GetSmDpi(this.mapService.MapServiceInfo.ReferViewBounds, this.mapService.MapServiceInfo.ReferViewer, this.mapService.MapServiceInfo.ReferScale);
                    }
                    
                    Dpi *= AdjustFactor;
                    base.Initialize();
                }
            }
        }

        private string layersKey = "0";
        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_layersKey_D}</summary>
        public string LayersKey
        {
            get
            {
                return layersKey;
            }
            set
            {
                if (layersKey != value)
                {
                    layersKey = value;
                    if (base.IsInitialized || isInitializing)
                    {
                        base.IsInitialized = false;
                        isInitializing = false;
                        base.Error = null;
                        base.Refresh();
                        this.Initialize();
                    }
                }
            }
        }

        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_mapName_D}</summary>
        public string MapName { get; set; }

        /// <summary>${mapping_TiledDynamicISLayer_attribute_redirect_D}</summary>
        public bool Redirect { get; set; }
        /// <summary>${mapping_TiledDynamicISLayer_attribute_antialiasing_D}</summary>
        public bool AntiAliasing { get; set; }
        /// <summary>${mapping_TiledDynamicISLayer_attribute_dpi_D}</summary>
        protected override double Dpi { get; set; }
        /// <summary>${mapping_DynamicIServerLayer_attribute_ajustFactor_D}</summary>
        public double AdjustFactor { get; set; }
        /// <summary>${mapping_DynamicISLayer_attribute_IsScaleCentric_D}</summary>
        public override bool IsScaleCentric
        {
            get
            {
                return true;
            }
        }

        //IsSkipGetSMMapServiceInfo默认是false，
        //如果设置为true，则Bounds、ReferViewBounds、ReferViewer、ReferScale必须设置；
        //CRS此时为空，要用到与CRS相关时，需要设置CRS；
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_IsSkipGetSMMapServiceInfo_D}</summary>
        public bool IsSkipGetSMMapServiceInfo { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewBounds_D}</summary>
        public Rectangle2D ReferViewBounds { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewer_D}</summary>
        public Rect ReferViewer { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferScale_D}</summary>
        public double ReferScale { get; set; }
    }
}
