﻿using System.Json;
using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Mapping
{
    internal class SmMapServiceInfo
    {
        public SmMapServiceInfo()
        { }

        public string MapName { get; set; }
        public Rectangle2D MapBounds { get; set; }
        public Rectangle2D ReferViewBounds { get; set; }
        public double ReferScale { get; set; }
        public Rect ReferViewer { get; set; }
        public CoordinateReferenceSystem CRS { get; set; }

        public static SmMapServiceInfo FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            SmMapServiceInfo msi = new SmMapServiceInfo();
            msi.MapBounds = JsonHelper.ToRectangle2D((JsonObject)jsonObject["mapBounds"]);
            msi.ReferViewBounds = JsonHelper.ToRectangle2D((JsonObject)jsonObject["referViewBounds"]);
            msi.ReferViewer = JsonHelper.ToRect((JsonObject)jsonObject["referViewer"]);
            msi.ReferScale = (double)jsonObject["referScale"];

            if (jsonObject.ContainsKey("coordsSys"))
            {
                msi.CRS = JsonHelper.ToCRS((JsonObject)jsonObject["coordsSys"]);
            }
            return msi;

        }

    }
}
