﻿using System;
using System.Net;
using System.Security;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using SuperMap.Web.Core;
using SuperMap.Web.ISDotNET6.Resources;
using SuperMap.Web.Utilities;
using System.Collections.Generic;

namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// 	<para>${mapping_DynamicISLayer_Title}</para>
    /// 	<para>${mapping_DynamicISLayer_Description}</para>
    /// </summary>
    public class DynamicISLayer : DynamicLayer//, ISuperMapLayer
    {
        private bool isInitializing;
        private SmMapService mapService;
        private WebClient client;

        /// <summary>${mapping_DynamicISLayer_constructor_None_D}</summary>
        public DynamicISLayer()
            : base()
        {
            ImageFormat = "png";
            AdjustFactor = 1.0;
            AntiAliasing = false;
            Redirect = false;
        }

        private void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            base.OnProgress(e.ProgressPercentage);
        }

        private void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null)
            {
                OnImageSourceCompleted onCompleted = (OnImageSourceCompleted)e.UserState;
                
                BitmapImage imgSrc = new BitmapImage { UriSource = new Uri(e.Result.ToString(), UriKind.Absolute) };
                onCompleted(imgSrc);
            }
        }
        /// <summary>${mapping_DynamicISLayer_method_Initialize_D}</summary>
        public override void Initialize()
        {
            if (!this.isInitializing && !base.IsInitialized)
            {
                #region 必设参数判断
                if (string.IsNullOrEmpty(this.Url))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.InvalidUrl);
                    base.Initialize();
                    return;
                }
                if (string.IsNullOrEmpty(this.MapName))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.MapNameIsNull);
                    base.Initialize();
                    return;
                }
                #endregion

                if (!Url.Contains("http://"))  //相对地址
                {
                    var pageUrl = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                    var localUrl = pageUrl.AbsoluteUri.Substring(0, pageUrl.AbsoluteUri.IndexOf(pageUrl.AbsolutePath));
                    Url = localUrl + Url;
                }

                if (IsSkipGetSMMapServiceInfo)
                {
                    if (Bounds.IsEmpty)
                    {
                        Error = new ArgumentNullException("Bounds");
                    }
                    Dpi = ScaleHelper.GetSmDpi(ReferViewBounds, ReferViewer, ReferScale);
                    Dpi *= AdjustFactor;

                    this.isInitializing = true;
                    base.Initialize();
                    return;
                }

                this.isInitializing = true;
                this.mapService = new SmMapService(this.Url.Trim(), this.MapName);
                this.mapService.Initialized += new EventHandler<SmMapService.MapServiceInitalizeArgs>(mapService_Initialized);
                this.mapService.Failed += new SmMapService.MapServiceFaultEventHandler(mapService_Fault);
                this.mapService.Initialize();
            }
        }

        private void mapService_Fault(object sender, SmMapService.MapServiceFaultEventArgs args)
        {
            if (((args.Error is SecurityException) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttp)) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttps))
            {
                base.Error = new SecurityException(SuperMap.Web.Resources.ExceptionStrings.InvalidURISchemeHost, args.Error);
            }
            else
            {
                base.Error = args.Error;
            }
            base.Initialize();
        }

        private void mapService_Initialized(object sender, SmMapService.MapServiceInitalizeArgs e)
        {
            if (e.MapService.MapServiceUrl == this.Url.Trim())
            {
                if (base.Error != null)
                {
                    base.Initialize();
                }
                else
                {
                    this.Bounds = this.mapService.MapServiceInfo.MapBounds;
                    this.Dpi = ScaleHelper.GetSmDpi(this.mapService.MapServiceInfo.ReferViewBounds, this.mapService.MapServiceInfo.ReferViewer, this.mapService.MapServiceInfo.ReferScale);
                    Dpi *= AdjustFactor;
                    base.Initialize();
                }
            }
        }

        /// <summary>${mapping_DynamicISLayer_method_Cancel_D}</summary>
        protected override void Cancel()
        {
            if (client != null && client.IsBusy)
            {
                client.CancelAsync();
            }
            base.Cancel();
        }

        //string mapParam = "mapParam={\"mapName\":{0},\"mapScales\":null,mapScale:{1},\"zoomLevel\":null,\"pixelCenter\":{\"x\":{2},\"y\":{3}},\"center\":{\"x\":{4},\"y\":{5}},\"mapCenter\":{\"x\":{6},\"y\":{7}},\"pixelRect\":{\"leftTop\":{\"x\":{8},\"y\":{9}},\"rightBottom\":{\"x\":{10},\"y\":{11}}},\"mapRect\"{\"leftBottom\":{\"x\":null,\"y\":null},\"rightTop\":{\"x\":null,\"y\":null}}}";
        /// <summary>${mapping_DynamicISLayer_method_GetImageSource_D}</summary>
        /// <param name="onCompleted">${mapping_DynamicISLayer_method_GetImageSource_param_onCompleted}</param>
        protected override void GetImageSource(DynamicLayer.OnImageSourceCompleted onCompleted)
        {
            if (!IsInitialized)
            {
                onCompleted(null);
            }
            else
            {
                if (client != null && client.IsBusy)
                {
                    client.CancelAsync();
                    client = null;
                }
            }

            client = new WebClient();
            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(client_DownloadStringCompleted);
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
            
            if (this.Resolution < 0 || this.Dpi < 0)
            {
                return;
            }

            double scale = ScaleHelper.ScaleConversion(this.Resolution, this.Dpi);
            
            Rectangle2D bounds = ViewBounds;
            int width = (int)Math.Round(ViewSize.Width);
            int height = (int)Math.Round(ViewSize.Height);

            StringBuilder imgUrl = new StringBuilder();
            imgUrl.Append(Url);
            if (!Url.EndsWith("/"))
            {
                imgUrl.Append("/");
            }
            imgUrl.Append("common.ashx?method=GetMapImage");
            imgUrl.AppendFormat("&map={0}", MapName);
            imgUrl.AppendFormat("&layersKey={0}", LayersKey);
            imgUrl.AppendFormat("&t={0}", DateTime.Now.Ticks);
            imgUrl.AppendFormat("&redirect={0}", Redirect.ToString());
            imgUrl.Append("&mapParam={");
            imgUrl.AppendFormat("\"antiAlias\":{0},", this.AntiAliasing.ToString().ToLower());
            imgUrl.AppendFormat("\"mapName\":\"{0}\",", MapName);
            imgUrl.AppendFormat("\"mapScale\":{0},", scale);
            imgUrl.AppendFormat("\"transparent\":{0},", Transparent.ToString().ToLower());
            imgUrl.AppendFormat("\"center\":{{\"x\":{0},\"y\":{1}}},", bounds.Center.X, bounds.Center.Y);
            imgUrl.AppendFormat("\"pixelRect\":{{\"leftTop\":{{\"x\":0,\"y\":0}},\"rightBottom\":{{\"x\":{0},\"y\":{1}}}", width, height);//,
            imgUrl.Append("}}");
            //IS.NET中增加重定向参数，此时不需要再下载返回信息了，而是直接采用重定向
            //为了兼容，设置Redirect属性，当此属性为FALSE时，兼容以前的模式，当此属性为true时，采用重定向，不兼容以前的模式
            if (this.Redirect)
            {
                BitmapImage imgSrc = new BitmapImage { UriSource = new Uri(imgUrl.ToString(), UriKind.Absolute) };
                onCompleted(imgSrc);
            }
            else
            {
                client.DownloadStringAsync(new Uri(imgUrl.ToString(), UriKind.Absolute), onCompleted);
            }
        }

        /// <summary>${mapping_DynamicISLayer_method_GetImageUrl_D}</summary>
        protected override string GetImageUrl()
        {
            return string.Empty;
        }

        private string layersKey = "0";
        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_layersKey_D}</summary>
        public string LayersKey
        {
            get
            {
                return layersKey;
            }
            set
            {
                if (layersKey != value)
                {
                    layersKey = value;
                    if (base.IsInitialized || isInitializing)
                    {
                        base.IsInitialized = false;
                        isInitializing = false;
                        base.Error = null;
                        base.Refresh();
                        this.Initialize();
                    }
                }
            }
        }

        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_mapName_D}</summary>
        public string MapName { get; set; }

        /// <summary>${mapping_DynamicISLayer_attribute_Dpi_D}</summary>
        protected override double Dpi { get; set; }
        /// <summary>${mapping_DynamicIServerLayer_attribute_ajustFactor_D}</summary>
        public double AdjustFactor { get; set; }

        /// <summary>${mapping_DynamicISLayer_attribute_IsScaleCentric_D}</summary>
        public override bool IsScaleCentric
        {
            get
            {
                return true;
            }
        }

        //IsSkipGetSMMapServiceInfo默认是false，
        //如果设置为true，则Bounds、ReferViewBounds、ReferViewer、ReferScale必须设置；
        //CRS此时为空，要用到与CRS相关时，需要设置CRS；
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_IsSkipGetSMMapServiceInfo_D}</summary>
        public bool IsSkipGetSMMapServiceInfo { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewBounds_D}</summary>
        public Rectangle2D ReferViewBounds { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewer_D}</summary>
        public Rect ReferViewer { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferScale_D}</summary>
        public double ReferScale { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_AntiAliasing_D}</summary> 
        //地图是否反走样，对线型、文字、符号、统计图有效。
        public bool AntiAliasing { get; set; }
        /// <summary>${mapping_TiledDynamicISLayer_attribute_redirect_D}</summary>
        public bool Redirect { get; set; }
    }
}
