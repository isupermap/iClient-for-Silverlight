﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Design;

[assembly: AssemblyTitle("SuperMap.Web.Design")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SuperMap")]
[assembly: AssemblyProduct("SuperMap.Web.Design")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(
    ResourceDictionaryLocation.None,
    ResourceDictionaryLocation.SourceAssembly
)]
[assembly: AssemblyVersion("7.0.1")]
[assembly: AssemblyFileVersion("7.0.1")]
[assembly: ProvideMetadata(typeof(MetadataRegistration))]