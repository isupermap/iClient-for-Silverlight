﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Mapping;
using SuperMap.Web.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Design
{
    internal class ImageLayerMetadata : AttributeTableBuilder
    {
        public ImageLayerMetadata()
            : base()
        {
            AddCallback(typeof(ImageLayer), b =>
            {
                b.AddCustomAttributes(
                    Extensions.GetMemberName<ImageLayer>(x => x.Transparent),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<ImageLayer>(x => x.ImageFormat),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
            });
        }
    }
}
