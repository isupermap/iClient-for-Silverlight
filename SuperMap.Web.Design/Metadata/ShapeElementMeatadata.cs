﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Core;
using SuperMap.Web.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Design
{
    internal class ShapeElementMeatadata : AttributeTableBuilder
    {
        public ShapeElementMeatadata()
            : base()
        {
            AddCallback(typeof(ShapeElement), b =>
            {
                b.AddCustomAttributes(
                    Extensions.GetMemberName<ShapeElement>(x => x.Point2Ds),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<ShapeElement>(x => x.StrokeThickness),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<ShapeElement>(x => x.StrokeDashArray),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<ShapeElement>(x => x.StrokeDashCap),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<ShapeElement>(x => x.StrokeDashOffset),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<ShapeElement>(x => x.StrokeEndLineCap),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<ShapeElement>(x => x.StrokeLineJoin),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<ShapeElement>(x => x.StrokeMiterLimit),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
                b.AddCustomAttributes(
                 Extensions.GetMemberName<ShapeElement>(x => x.StrokeStartLineCap),
                new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });

            });
        }
    }
}
