﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Mapping;
using SuperMap.Web.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Design
{
    internal class LayerMetadata : AttributeTableBuilder
    {
        public LayerMetadata()
            : base()
        {
            AddCallback(typeof(Layer), b =>
                       {
                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.Metadata),
                              new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });

                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.Bounds),
                              new Attribute[] 
                                { 
                                   new CategoryAttribute(Properties.Resources.LayerSetting),
                                    new TypeConverterAttribute(typeof(ExpandableRectangle2DConverter)),
                                    //new BrowsableAttribute(false),
                                    new EditorBrowsableAttribute( EditorBrowsableState.Never)
                                });
                           b.AddCustomAttributes(
                                Extensions.GetMemberName<Layer>(x => x.ViewBounds),
                               new Attribute[] 
                                { 
                                   new CategoryAttribute(Properties.Resources.LayerSetting),
                                    new TypeConverterAttribute(typeof(ExpandableRectangle2DConverter)),
                                    new EditorBrowsableAttribute( EditorBrowsableState.Never)
                                    //new BrowsableAttribute(false)
                                });

                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.ID),
                              new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                           b.AddCustomAttributes(
                              Extensions.GetMemberName<Layer>(x => x.CRS),
                             new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting),
                                });
                           b.AddCustomAttributes(
                             Extensions.GetMemberName<Layer>(x => x.Effect),
                            new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.IsVisible),
                              new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting),
                                });

                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.Opacity),
                              new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });

                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.Resolution),
                              new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.Url),
                              new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });

                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.ViewSize),
                              new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                           //========================Advanced============================
                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.MaxVisibleResolution),
                              new Attribute[] 
                                { 
                                    new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.MaxVisibleScale),
                              new Attribute[] 
                                { 
                                    new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.MinVisibleResolution),
                              new Attribute[] 
                                {  
                                    new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                           b.AddCustomAttributes(
                               Extensions.GetMemberName<Layer>(x => x.MinVisibleScale),
                              new Attribute[] 
                                {  
                                    new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                       });
        }
    }
}
