﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Mapping;
using SuperMap.Web.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Design
{
    internal class HeatMapLayerMetadata : AttributeTableBuilder
    {
        public HeatMapLayerMetadata()
            : base()
        {
            AddCallback(typeof(HeatMapLayer), b =>
            {
                b.AddCustomAttributes(
                    Extensions.GetMemberName<HeatMapLayer>(x => x.GradientStops),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
                b.AddCustomAttributes(
                    Extensions.GetMemberName<HeatMapLayer>(x => x.HeatPoints),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<HeatMapLayer>(x => x.Intensity),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<HeatMapLayer>(x => x.Radius),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
            });
        }
    }
}
