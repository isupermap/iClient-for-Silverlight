﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Core;
using SuperMap.Web.Design.Common;
using System.ComponentModel;
using Microsoft.Windows.Design;

namespace SuperMap.Web.Design
{
    internal class PolylineElementMetadata : AttributeTableBuilder
    {
        public PolylineElementMetadata()
            : base()
        {
            AddCallback(typeof(PolylineElement), b =>
            {
                b.AddCustomAttributes(new ToolboxBrowsableAttribute(false));

                b.AddCustomAttributes(
                    Extensions.GetMemberName<PolylineElement>(x => x.FillRule),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
            });
        }
    }
}
