﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Core;
using SuperMap.Web.Design.Common;
using System.ComponentModel;
using Microsoft.Windows.Design;

namespace SuperMap.Web.Design
{
    internal class PushpinMetadata : AttributeTableBuilder
    {
        public PushpinMetadata()
            : base()
        {
            AddCallback(typeof(Pushpin), b =>
            {
                b.AddCustomAttributes(new ToolboxBrowsableAttribute(false));

                b.AddCustomAttributes(
                    Extensions.GetMemberName<Pushpin>(x => x.Location),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<Pushpin>(x => x.DisableAnimation),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.ElementSetting)
                                            });
            });
        }
    }
}
