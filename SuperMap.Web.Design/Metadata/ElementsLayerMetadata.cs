﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Mapping;
using SuperMap.Web.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Design
{
    internal class ElementsLayerMetadata : AttributeTableBuilder
    {
        public ElementsLayerMetadata()
            : base()
        {
            AddCallback(typeof(ElementsLayer), b =>
                {
                    b.AddCustomAttributes(
                        Extensions.GetMemberName<ElementsLayer>(x => x.Children),
                        new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.LayerSetting)
                        });
                });
        }
    }
}
