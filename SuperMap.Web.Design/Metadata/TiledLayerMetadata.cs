﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperMap.Web.Mapping;
using SuperMap.Web.Design.Common;
using System.ComponentModel;
using Microsoft.Windows.Design.Metadata;

namespace SuperMap.Web.Design
{
    internal class TiledLayerMetadata : AttributeTableBuilder
    {
        public TiledLayerMetadata()
            : base()
        {
            AddCallback(typeof(TiledLayer), b =>
            {
                b.AddCustomAttributes(
                    Extensions.GetMemberName<TiledLayer>(x => x.TileSize),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });


                b.AddCustomAttributes(
                    Extensions.GetMemberName<TiledLayer>(x => x.Origin),
                   new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
            });
        }
    }
}
