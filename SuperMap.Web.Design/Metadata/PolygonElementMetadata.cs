﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Core;
using SuperMap.Web.Design.Common;
using System.ComponentModel;
using Microsoft.Windows.Design;

namespace SuperMap.Web.Design
{
    internal class PolygonElementMetadata : AttributeTableBuilder
    {
        public PolygonElementMetadata()
            : base()
        {
            AddCallback(typeof(PolygonElement), b =>
            {
                b.AddCustomAttributes(new ToolboxBrowsableAttribute(false));
                   
                b.AddCustomAttributes(
                    Extensions.GetMemberName<PolygonElement>(x => x.FillRule),
                   new Attribute[] 
                    { 
                        new CategoryAttribute(Properties.Resources.ElementSetting)
                    });
                
            });
        }
    }
}
