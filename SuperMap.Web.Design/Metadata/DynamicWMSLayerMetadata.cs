﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Mapping;
using SuperMap.Web.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Design
{
    internal class DynamicWMSLayerMetadata : AttributeTableBuilder
    {
        public DynamicWMSLayerMetadata()
            : base()
        {
            AddCallback(typeof(DynamicWMSLayer), b =>
            {
                //TODO:颜色选择器
                b.AddCustomAttributes(
                    Extensions.GetMemberName<DynamicWMSLayer>(x => x.BgColor),
                   new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.LayerSetting)
                        });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<DynamicWMSLayer>(x => x.EnableGetCapabilities),
                  new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.LayerSetting)
                        });
                //TODO:枚举选择器
                b.AddCustomAttributes(
                  Extensions.GetMemberName<DynamicWMSLayer>(x => x.ImageFormat),
                 new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.LayerSetting)
                        });
                b.AddCustomAttributes(
                 Extensions.GetMemberName<DynamicWMSLayer>(x => x.Layers),
                new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.LayerSetting),
                        });
                b.AddCustomAttributes(
                 Extensions.GetMemberName<DynamicWMSLayer>(x => x.ProxyUrl),
                new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.LayerSetting)
                        });
                b.AddCustomAttributes(
                 Extensions.GetMemberName<DynamicWMSLayer>(x => x.Transparent),
                new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.LayerSetting)
                        });
                b.AddCustomAttributes(
                Extensions.GetMemberName<DynamicWMSLayer>(x => x.Version),
               new Attribute[] 
                        { 
                            new CategoryAttribute(Properties.Resources.LayerSetting)
                        });
            });
        }
    }
}
