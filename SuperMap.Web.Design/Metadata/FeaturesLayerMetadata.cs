﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Mapping;
using SuperMap.Web.Design.Common;
using System.ComponentModel;

namespace SuperMap.Web.Design
{
    internal class FeaturesLayerMetadata : AttributeTableBuilder
    {
        public FeaturesLayerMetadata()
            : base()
        {
            AddCallback(typeof(FeaturesLayer), b =>
                    {
                        //TODO:
                        b.AddCustomAttributes(
                             Extensions.GetMemberName<FeaturesLayer>(x => x.Renderer),
                            new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                        b.AddCustomAttributes(
                            Extensions.GetMemberName<FeaturesLayer>(x => x.Clusterer),
                           new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });                     
                        b.AddCustomAttributes(
                            Extensions.GetMemberName<FeaturesLayer>(x => x.ToolTip),
                           new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                        b.AddCustomAttributes(
                           Extensions.GetMemberName<FeaturesLayer>(x => x.Features),
                          new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                        b.AddCustomAttributes(
                          Extensions.GetMemberName<FeaturesLayer>(x => x.IsHitTestVisible),
                         new Attribute[] 
                                { 
                                    new CategoryAttribute(Properties.Resources.LayerSetting)
                                });
                    });
        }
    }
}
