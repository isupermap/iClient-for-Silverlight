﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Design.Common;
using SuperMap.Web.Mapping;
using System.ComponentModel;

namespace SuperMap.Web.Design
{
    internal class TiledWMSLayerMetadata : AttributeTableBuilder
    {
        public TiledWMSLayerMetadata()
            : base()
        {
            AddCallback(typeof(TiledWMSLayer), b =>
            {
                b.AddCustomAttributes(
                    Extensions.GetMemberName<TiledWMSLayer>(x => x.BgColor),
                   new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<TiledWMSLayer>(x => x.EnableGetCapabilities),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<TiledWMSLayer>(x => x.Layers),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<TiledWMSLayer>(x => x.ProxyUrl),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<TiledWMSLayer>(x => x.TileSize),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<TiledWMSLayer>(x => x.Version),
                  new Attribute[] 
                                            { 
                                                new CategoryAttribute(Properties.Resources.LayerSetting)
                                            });
            });
        }
    }
}
