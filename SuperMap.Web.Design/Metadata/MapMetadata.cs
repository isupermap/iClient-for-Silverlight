﻿using System;
using System.ComponentModel;
using Microsoft.Windows.Design;
using Microsoft.Windows.Design.Metadata;
using Microsoft.Windows.Design.PropertyEditing;
using SuperMap.Web.Design.Common;
using SuperMap.Web.Mapping;
using Microsoft.Windows.Design.Features;
using SuperMap.Web.Design.DefaultInitializers;

namespace SuperMap.Web.Design
{
    internal class MapMetadata : AttributeTableBuilder
    {
        public MapMetadata()
            : base()
        {

            AddCallback(typeof(Map), b =>
                     {
                         b.AddCustomAttributes(
                         Extensions.GetMemberName<Map>(x => x.Layers),
                         new CategoryAttribute(Properties.Resources.MapSetting),
                         new NewItemTypesAttribute(
                             new Type[]
                             {
                                 typeof(FeaturesLayer),
                                 typeof(ElementsLayer),
                                 typeof(HeatMapLayer),
                                 typeof(DynamicWMSLayer),
                                 typeof(TiledWMSLayer),
                                 
                                 //TODO：这里还要有iServer6、2 IS6的图层；
                             }),
                             new Microsoft.Windows.Design.PropertyEditing.AlternateContentPropertyAttribute()
                             );

                         b.AddCustomAttributes(new Attribute[] { new FeatureAttribute(typeof(MapDefaultInitializer)) });

                         b.AddCustomAttributes(
                              Extensions.GetMemberName<Map>(x => x.ZoomFactor),
                             new Attribute[] 
                                            { 
                                                new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                             Extensions.GetMemberName<Map>(x => x.ZoomDuration),
                             new Attribute[]
                                            {
                                                new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                            Extensions.GetMemberName<Map>(x => x.Action),
                            new Attribute[]
                                            {
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                           Extensions.GetMemberName<Map>(x => x.Angle),
                           new Attribute[]
                                            {
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.CRS),
                          new Attribute[]
                                            {
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                                          Extensions.GetMemberName<Map>(x => x.EnableDblClick),
                                          new Attribute[]
                                            {
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.EnableKey),
                          new Attribute[]
                                            {
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.EnableWheel),
                          new Attribute[]
                                            {
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.MaxResolution),
                          new Attribute[]
                                            {
                                                new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.MaxScale),
                          new Attribute[]
                                            {
                                                new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.Metadata),
                          new Attribute[]
                                            {
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.MinResolution),
                          new Attribute[]
                                            {
                                                 new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                                 new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.MinScale),
                          new Attribute[]
                                            {
                                                 new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                                 new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.PanDuration),
                          new Attribute[]
                                            {
                                                 new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                                 new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.PanFactor),
                          new Attribute[]
                                            {
                                                 new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                                                 new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         //b.AddCustomAttributes(
                         // Extensions.GetMemberName<Map>(x => x.RenderTransformOrigin),
                         // new Attribute[]
                         //        {
                         //             new EditorBrowsableAttribute(EditorBrowsableState.Advanced),
                         //             new CategoryAttribute(Properties.Resources.MapSetting)
                         //        });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.Resolutions),
                          new Attribute[]
                                            {
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.Scales),
                          new Attribute[]
                                            {
                                                new CategoryAttribute(Properties.Resources.MapSetting)
                                            });

                         //b.AddCustomAttributes(
                         // Extensions.GetMemberName<Map>(x => x.Style),
                         // new Attribute[]
                         //        {
                         //            //在设计时无法编辑，所以不予以显示
                         //            new EditorBrowsableAttribute( EditorBrowsableState.Never)
                         //        });

                         //b.AddCustomAttributes(
                         // Extensions.GetMemberName<Map>(x => x.Template),
                         // new Attribute[]
                         //        {
                         //           //在设计时无法编辑，所以不予以显示
                         //            new EditorBrowsableAttribute( EditorBrowsableState.Never)
                         //        });

                         b.AddCustomAttributes(
                          Extensions.GetMemberName<Map>(x => x.ViewBounds),
                          new Attribute[]
                                {
                                    new CategoryAttribute(Properties.Resources.MapSetting),
                                    new TypeConverterAttribute(typeof(ExpandableRectangle2DConverter)),
                                    //new BrowsableAttribute(false)
                                    new EditorBrowsableAttribute( EditorBrowsableState.Never)
                                });
                         b.AddCustomAttributes(
                         Extensions.GetMemberName<Map>(x => x.Bounds),
                         new Attribute[]
                                {
                                    new CategoryAttribute(Properties.Resources.MapSetting),
                                    new TypeConverterAttribute(typeof(ExpandableRectangle2DConverter)),
                                    //new BrowsableAttribute(false)
                                    new EditorBrowsableAttribute( EditorBrowsableState.Never)
                                });

                         b.AddCustomAttributes(new ToolboxCategoryAttribute("SuperMap.Web", false));
                     });
        }
    }
}
