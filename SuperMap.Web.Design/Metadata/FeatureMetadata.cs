﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;
using SuperMap.Web.Design.Common;
using SuperMap.Web.Core;
using System.ComponentModel;

namespace SuperMap.Web.Design
{
    internal class FeatureMetadata : AttributeTableBuilder
    {
        public FeatureMetadata()
            : base()
        {
            AddCallback(typeof(Feature), b =>
            {
                //TODO:这里怎样才能直接加上想要的Geometry呢？
                b.AddCustomAttributes(
                    Extensions.GetMemberName<Feature>(x => x.Geometry),
                   new Attribute[] 
                            { 
                                new CategoryAttribute(Properties.Resources.FeatureSetting)
                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<Feature>(x => x.Selected),
                  new Attribute[] 
                            { 
                                new CategoryAttribute(Properties.Resources.FeatureSetting)
                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<Feature>(x => x.Style),
                  new Attribute[] 
                            { 
                                new CategoryAttribute(Properties.Resources.FeatureSetting)
                            });
                b.AddCustomAttributes(
                   Extensions.GetMemberName<Feature>(x => x.ToolTip),
                  new Attribute[] 
                            { 
                                new CategoryAttribute(Properties.Resources.FeatureSetting)
                            });
            });
        }
    }
}
