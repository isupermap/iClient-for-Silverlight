﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Model;
using SuperMap.Web.Design.Common;


namespace SuperMap.Web.Design.DefaultInitializers
{
    internal class MapDefaultInitializer : DefaultInitializer
    {
        public override void InitializeDefaults(ModelItem item)
        {
            item.Properties[MyPlatformTypes.Control.BackgroundProperty].SetValue("#FFB8B8B8");
        }
    }
}
