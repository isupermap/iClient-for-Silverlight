﻿
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;

namespace SuperMap.Web.Design.Common
{
    /// <summary>
    /// Names for ToolboxCategoryAttribute.
    /// </summary>
    internal static class ToolboxCategoryPaths
    {
        /// <summary>
        /// Basic Controls category.
        /// </summary>
        public const string BasicControls = "Basic Controls";

        /// <summary>
        /// Controls category.
        /// </summary>
        public const string Controls = "";

        /// <summary>
        /// Control Parts category.
        /// </summary>
        public const string ControlParts = "Control Parts";

        /// <summary>
        /// DataVisualization category.
        /// </summary>
        public const string DataVisualization = "Data Visualization";

        /// <summary>
        /// DataVisualization/Control Parts category.
        /// </summary>
        public const string DataVisualizationControlParts = "Data Visualization/Control Parts";

        /// <summary>
        /// Theming category.
        /// </summary>
        public const string Theming = "Theming";
    }
}
