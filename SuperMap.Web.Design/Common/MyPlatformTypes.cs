﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Design.Metadata;

namespace SuperMap.Web.Design.Common
{
    internal class MyPlatformTypes
    {
        public class Control
        {
            public static readonly TypeIdentifier TypeId = new TypeIdentifier("http://schemas.microsoft.com/winfx/2006/xaml/presentation", "Control");
            public static readonly PropertyIdentifier BackgroundProperty = new PropertyIdentifier(TypeId, "Background");
        }
    }
}
