﻿using System;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindStopParameter_Title}</para>
    /// </summary>
    public class FindStopParameter
    {
        /// <summary>${iServerJava6R_FindStopParameter_attribute_KeyWord_D}</summary>
        public string KeyWord { get; set; }

        /// <summary>${iServerJava6R_FindStopParameter_attribute_ReturnPosition_D}</summary>
        public bool ReturnPosition { get; set; }

        /// <summary>${iServerJava6R_FindStopParameter_constructor_D}</summary>
        public FindStopParameter()
        {

        }

        /// <summary>${iServerJava6R_FindStopParameter_constructor_String_D}</summary>
        /// <param name="keyword">${iServerJava6R_FindStopParameter_constructor_param_keyword}</param>
        /// <param name="returnPosition">${iServerJava6R_FindStopParameter_constructor_param_returnPosition}</param>
        public FindStopParameter(string keyword, bool returnPosition)
        {
            this.KeyWord = keyword;
            this.ReturnPosition = returnPosition;
        }

    }
}
