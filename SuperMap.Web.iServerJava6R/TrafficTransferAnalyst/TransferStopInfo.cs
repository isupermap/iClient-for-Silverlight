﻿using SuperMap.Web.Core;
using System.Json;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_TransferStopInfo_Title}</para>
    /// </summary>
    public class TransferStopInfo
    {
        /// <summary>${iServerJava6R_TransferStopInfo_constructor_D}</summary>
        public TransferStopInfo()
        {

        }

        /// <summary>${iServerJava6R_TransferStopInfo_attribute_Alias_D}</summary>
        public string Alias
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferStopInfo_attribute_Id_D}</summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferStopInfo_attribute_Name_D}</summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferStopInfo_attribute_Position_D}</summary>
        public Point2D Position
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferStopInfo_attribute_StopId_D}</summary>
        public long StopId
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_TransferStopInfo_method_FromJson_D}</summary>
        /// <param name="json">${iServerJava6R_TransferStopInfo_method_FromJson_param_jsonObject}</param>
        public static TransferStopInfo FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            TransferStopInfo stop = new TransferStopInfo();
            stop.Alias = json["alias"];
            stop.Id = (int)json["id"];
            stop.Name = json["name"];
            stop.Position = JsonHelper.ToPoint2D((JsonObject)json["position"]);
            stop.StopId = (long)json["stopID"];

            return stop;
        }
    }
}
