﻿using System.Json;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_TransferLines_Title}</para>
    /// </summary>
    public class TransferLines
    {
        /// <summary>${iServerJava6R_TransferLines_constructor_D}</summary>
        public TransferLines()
        {

        }
        /// <summary>${iServerJava6R_TransferLines_attribute_LineItems_D}</summary>
        public TransferLine[] LineItems
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_TransferLines_method_FromJson_D}</summary>
        /// <param name="json">${iServerJava6R_TransferLines_method_FromJson_param_jsonObject}</param>
        public static TransferLines FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            TransferLines lines = new TransferLines();
            JsonArray items = (JsonArray)json["lineItems"];
            if (items != null && items.Count > 0)
            {
                lines.LineItems = new TransferLine[items.Count];
                for (int i = 0; i < items.Count; i++)
                {
                    lines.LineItems[i] = TransferLine.FromJson((JsonObject)items[i]);
                }
            }
            return lines;
        }
    }
}
