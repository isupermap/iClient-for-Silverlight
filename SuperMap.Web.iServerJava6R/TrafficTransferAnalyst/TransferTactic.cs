﻿
namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_TransferTactic_Title}</para>
    /// </summary>
    public enum TransferTactic
    {
        /// <summary>${iServerJava6R_TransferTactic_attribute_LESS_TIME_D}</summary>
        LESS_TIME,
        /// <summary>${iServerJava6R_TransferTactic_attribute_LESS_TRANSFER_D}</summary>
        LESS_TRANSFER,
        /// <summary>${iServerJava6R_TransferTactic_attribute_LESS_WALK_D}</summary>
        LESS_WALK,
        /// <summary>${iServerJava6R_TransferTactic_attribute_MIN_DISTANCE_D}</summary>
        MIN_DISTANCE
    }
}
