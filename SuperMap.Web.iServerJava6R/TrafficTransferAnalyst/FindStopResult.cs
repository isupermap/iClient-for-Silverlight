﻿using System;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindStopResult_Title}</para>
    /// </summary>
    public class FindStopResult
    {
        /// <summary>${iServerJava6R_FindStopResult_attribute_Stops_D}</summary>
        public List<TransferStopInfo> Stops { get; set; }

        /// <summary>${iServerJava6R_FindStopResult_constructor_D}</summary>
        public FindStopResult()
        { }
        /// <summary>${iServerJava6R_FindStopResult_constructor_TransferStopInfo_D}</summary>
        /// <param name="stops">${iServerJava6R_FindStopResult_constructor_param_stops}</param>
        public FindStopResult(List<TransferStopInfo> stops)
        {
            Stops = stops;
        }
        /// <summary>${iServerJava6R_FindStopResult_method_FromJson_D}</summary>
        /// <param name="json">${iServerJava6R_FindStopResult_method_FromJson_param_jsonObject}</param>
        public static FindStopResult FromJson(JsonArray json)
        {
            if (json == null)
            {
                return null;
            }

            FindStopResult result = new FindStopResult();
            if (json != null && json.Count > 0)
            {
                result.Stops = new List<TransferStopInfo>();
                for (int i = 0; i < json.Count; i++)
                {
                    TransferStopInfo stop = TransferStopInfo.FromJson((JsonObject)json[i]);
                    result.Stops.Add(stop);
                }
            }

            return result;
        }
    }
}
