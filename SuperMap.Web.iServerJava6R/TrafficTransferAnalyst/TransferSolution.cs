﻿using System.Json;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_TransferSolution_Title}</para>
    /// </summary>
    public class TransferSolution
    {
        /// <summary>${iServerJava6R_TransferSolution_constructor_D}</summary>
        public TransferSolution()
        {

        }
        /// <summary>${iServerJava6R_TransferSolution_attribute_TransferCount_D}</summary>
        public int TransferCount
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_TransferSolution_attribute_LinesItems_D}</summary>
        public TransferLines[] LinesItems
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_TransferSolution_method_FromJson_D}</summary>
        /// <param name="json">${iServerJava6R_TransferSolution_method_FromJson_param_jsonObject}</param>
        public static TransferSolution FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            TransferSolution solution = new TransferSolution();
            solution.TransferCount = (int)json["transferCount"];
            JsonArray items = (JsonArray)json["linesItems"];
            if (items != null && items.Count > 0)
            {
                solution.LinesItems = new TransferLines[items.Count];
                for (int i = 0; i < items.Count; i++)
                {
                    solution.LinesItems[i] = TransferLines.FromJson((JsonObject)items[i]);
                }
            }

            return solution;
        }
    }
}
