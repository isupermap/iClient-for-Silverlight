﻿using System;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_TransferPreference_Title}</para>
    /// </summary>
    public enum TransferPreference
    {
        /// <summary>${iServerJava6R_TransferPreference_attribute_BUS_D}</summary>
        BUS,
        /// <summary>${iServerJava6R_TransferPreference_attribute_NO_SUBWAY_D}</summary>
        NO_SUBWAY,
        /// <summary>${iServerJava6R_TransferPreference_attribute_NONE_D}</summary>
        NONE,
        /// <summary>${iServerJava6R_TransferPreference_attribute_SUBWAY_D}</summary>
        SUBWAY
    }
}
