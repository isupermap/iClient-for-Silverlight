﻿using System;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTransferPathParameter_Title}</para>
    /// </summary>
    public class FindTransferPathParameter
    {
        /// <summary>${iServerJava6R_FindTransferPathParameter_constructor_D}</summary>
        public FindTransferPathParameter()
        {

        }
        /// <summary>${iServerJava6R_FindTransferPathParameter_attribute_Points_D}</summary>
        public Array Points
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_FindTransferPathParameter_attribute_TransferLine_D}</summary>
        public TransferLine[] TransferLines
        {
            get;
            set;
        }

    }
}
