﻿using System;
using System.Json;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTransferSolutionResult_Title}</para>
    /// </summary>
    public class FindTransferSolutionResult
    {
        /// <summary>${iServerJava6R_FindTransferSolutionResult_constructor_D}</summary>
        public FindTransferSolutionResult()
        {

        }
        /// <summary>${iServerJava6R_FindTransferSolutionResult_attribute_DefaultGuide_D}</summary>
        public TransferGuide DefaultGuide
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_FindTransferSolutionResult_attribute_SolutionItems_D}</summary>
        public TransferSolution[] SolutionItems
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_FindTransferSolutionResult_method_FromJson_D}</summary>
        /// <param name="json">${iServerJava6R_FindTransferSolutionResult_method_FromJson_param_jsonObject}</param>
        public static FindTransferSolutionResult FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            FindTransferSolutionResult result = new FindTransferSolutionResult();
            result.DefaultGuide = TransferGuide.FromJson((JsonObject)json["defaultGuide"]);
            JsonArray items = (JsonArray)json["solutionItems"];
            if (items != null && items.Count > 0)
            {
                result.SolutionItems = new TransferSolution[items.Count];
                for (int i = 0; i < items.Count; i++)
                {
                    result.SolutionItems[i] = TransferSolution.FromJson((JsonObject)items[i]);
                }
            }

            return result;
        }
    }
}
