﻿using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindStopEventArgs_Title}</para>
    /// </summary>
    public class FindStopEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_FindStopEventArgs_constructor_D}</summary>
        public FindStopEventArgs(FindStopResult result, string originResult, object state)
            :base(state)
        {
            this.FindStopResult = result;
            this.OriginResult = originResult;
        }

        /// <summary>${iServerJava6R_FindStopEventArgs_attribute_FindStopResult_D}</summary>
        public FindStopResult FindStopResult
        {
            get;
            private set;
        }

        /// <summary>${iServerJava6R_FindStopEventArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
    }
}
