﻿using System;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTransferSolutionEventArgs_Title}</para>
    /// </summary>
    public class FindTransferSolutionEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_FindTransferSolutionEventArgs_constructor_D}</summary>       
        public FindTransferSolutionEventArgs(FindTransferSolutionResult result, string originResult, object state)
            :base(state)
        {
            this.FindTransferSolutionResult = result;
            this.OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_FindTransferSolutionEventArgs_attribute_FindTransferSolutionResult_D}</summary>
        public FindTransferSolutionResult FindTransferSolutionResult
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_FindTransferSolutionEventArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
    }
}
