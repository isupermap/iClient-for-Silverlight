﻿using System;
using SuperMap.Web.Service;
using System.Windows.Browser;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindStopService_Title}</para>
    /// </summary>
    public class FindStopService:ServiceBase
    {
        /// <summary>${iServerJava6R_FindStopService_constructor_D}</summary>
        public FindStopService()
        { }

        /// <summary>${iServerJava6R_FindStopService_constructor_param_url}</summary>
        public FindStopService(string url)
            : base(url)
        { }

        /// <summary>${iServerJava6R_FindStopService_method_ProcessAsync_param_Parameters}</summary>
        public void ProcessAsync(FindStopParameter paramer, object state)
        {
            GenerateAbsoluteUrl(paramer);
            base.SubmitRequest(this.Url, GetDictionaryParameters(paramer), new EventHandler<RequestEventArgs>(FindStopService_Complated), state, false, false, false);
        }

        private void GenerateAbsoluteUrl(FindStopParameter parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }

            if (!this.Url.EndsWith("/"))
            {
                this.Url += "/";
            }
            this.Url += string.Format("stops/keyword/{0}.rjson?", HttpUtility.UrlEncode(parameters.KeyWord));
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(FindStopParameter parameters)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic["returnPosition"] = parameters.ReturnPosition ? "true" : "false";
            return dic;
        }

        private void FindStopService_Complated(object sender, RequestEventArgs args)
        {
            JsonArray jsonObject = (JsonArray)JsonObject.Parse(args.Result);
            FindStopResult result =  FindStopResult.FromJson(jsonObject);
            FindStopEventArgs e = new FindStopEventArgs(result, args.Result, args.UserState);
            lastResult = result;
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }
        /// <summary>${iServerJava6R_FindStopService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FindStopEventArgs> ProcessCompleted;

        private FindStopResult lastResult;
        /// <summary>${iServerJava6R_FindStopService_attribute_lastResult_D}</summary>
        public FindStopResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
