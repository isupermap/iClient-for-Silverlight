﻿using System;
using System.Json;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_TransferGuide_Title}</para>
    /// </summary>
    public class TransferGuide
    {
        /// <summary>${iServerJava6R_TransferGuide_constructor_D}</summary>
        public TransferGuide()
        {

        }

        /// <summary>${iServerJava6R_TransferGuide_attribute_Count_D}</summary>
        public int Count
        {
            get;
            set;
        }


        /// <summary>${iServerJava6R_TransferGuide_attribute_Items_D}</summary>
        public TransferGuideItem[] Items
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuide_attribute_TotalDistance_D}</summary>
        public double TotalDistance
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuide_attribute_TransferCount_D}</summary>
        public int TransferCount
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_TransferGuide_method_FromJson_D}</summary>
        /// <param name="json">${iServerJava6R_TransferGuide_method_FromJson_param_jsonObject}</param>
        public static TransferGuide FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            TransferGuide guide = new TransferGuide();
            guide.Count = (int)json["count"];
            guide.TotalDistance = (double)json["totalDistance"];
            guide.TransferCount = (int)json["transferCount"];
            JsonArray items = (JsonArray)json["items"];
            if (items != null && items.Count > 0)
            {
                guide.Items = new TransferGuideItem[items.Count];
                for (int i = 0; i < items.Count; i++)
                {
                    guide.Items[i] = TransferGuideItem.FromJson((JsonObject)items[i]);
                }
            }

            return guide;
        }
    }
}
