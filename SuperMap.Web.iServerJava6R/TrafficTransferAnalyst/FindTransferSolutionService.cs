﻿using System;
using SuperMap.Web.Service;
using System.Windows.Browser;
using System.Collections.Generic;
using SuperMap.Web.Core;
using System.Json;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTransferSolutionService_Title}</para>
    /// </summary>
    public class FindTransferSolutionService : ServiceBase
    {
        /// <summary>${iServerJava6R_FindTransferSolutionService_constructor_D}</summary>
        public FindTransferSolutionService()
        { }
        /// <summary>${iServerJava6R_FindTransferSolutionService_constructor_param_url}</summary>
        public FindTransferSolutionService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_FindTransferSolutionService_method_ProcessAsync_param_Parameters}</summary>
        public void ProcessAsync(FindTransferSolutionsParameter paramer, object state)
        {
            GenerateAbsoluteUrl(paramer);
            base.SubmitRequest(this.Url, GetDictionaryParameters(paramer), new EventHandler<RequestEventArgs>(FindTransferSolutionService_Complated), state, false, false, false);
        }

        private void GenerateAbsoluteUrl(FindTransferSolutionsParameter parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }

            if (!this.Url.EndsWith("/"))
            {
                this.Url += "/";
            }
            this.Url += "solutions.rjson?";
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(FindTransferSolutionsParameter parameters)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            
            string jsonPoints = "[";
            List<string> jsonL = new List<string>();
            if (parameters.Points.GetType() == typeof(Point2D[]))
            {
                Point2D[] points = parameters.Points as Point2D[];
                for (int i = 0; i < points.Length; i++)
                {
                    jsonL.Add(JsonHelper.FromPoint2D(points[i]));
                }
            }
            else if (parameters.Points.GetType() == typeof(int[]))
            {
                int[] points = parameters.Points as int[];
                for (int i = 0; i < points.Length; i++)
                {
                    jsonL.Add(points[i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                }
            }

            jsonPoints += string.Join(",", jsonL);
            jsonPoints += "]";

            dic["points"] = jsonPoints;
            dic["solutionCount"] = parameters.SolutionCount.ToString(System.Globalization.CultureInfo.InvariantCulture);
            dic["transferTactic"] = parameters.TransferTactic.ToString();
            dic["transferPreference"] = parameters.TransferPreference.ToString();
            dic["walkingRatio"] = parameters.WalkingRatio.ToString(System.Globalization.CultureInfo.InvariantCulture);
            return dic;
        }

        private void FindTransferSolutionService_Complated(object sender, RequestEventArgs args)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(args.Result);
            FindTransferSolutionResult result = FindTransferSolutionResult.FromJson(jsonObject);

            FindTransferSolutionEventArgs e = new FindTransferSolutionEventArgs(result, args.Result, args.UserState);
            lastResult = result;
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }
        /// <summary>${iServerJava6R_FindTransferSolutionService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FindTransferSolutionEventArgs> ProcessCompleted;

        private FindTransferSolutionResult lastResult;
        /// <summary>${iServerJava6R_FindTransferSolutionService_attribute_lastResult_D}</summary>
        public FindTransferSolutionResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
