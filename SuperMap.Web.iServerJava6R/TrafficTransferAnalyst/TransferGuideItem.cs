﻿using SuperMap.Web.Core;
using System.Json;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_TransferGuideItem_Title}</para>
    /// 	<para>${iServerJava6R_TransferGuideItem_Description}</para>
    /// </summary>
    public class TransferGuideItem
    {
        /// <summary>${iServerJava6R_TransferGuideItem_constructor_D}</summary>
        public TransferGuideItem()
        {

        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_Distance_D}</summary>
        public double Distance
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_EndIndex_D}</summary>
        public int EndIndex
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_EndPosition_D}</summary>
        public Point2D EndPosition
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_EndStopName_D}</summary>
        public string EndStopName
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_IsWalking_D}</summary>
        public bool IsWalking
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_LineName_D}</summary>
        public string LineName
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_PassStopCount_D}</summary>
        public int PassStopCount
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_Route_D}</summary>
        public SuperMap.Web.Core.Geometry Route
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_StartIndex_D}</summary>
        public int StartIndex
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_StartPosition_D}</summary>
        public Point2D StartPosition
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_TransferGuideItem_attribute_StartStopName_D}</summary>
        public string StartStopName
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_TransferGuideItem_method_FromJson_D}</summary>
        /// <param name="json">${iServerJava6R_TransferGuideItem_method_FromJson_param_jsonObject}</param>
        public static TransferGuideItem FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            TransferGuideItem item = new TransferGuideItem();
            item.Distance = (double)json["distance"];
            item.EndIndex = (int)json["endIndex"];
            item.EndPosition = JsonHelper.ToPoint2D((JsonObject)json["endPosition"]);
            item.EndStopName = json["endStopName"];
            item.IsWalking = (bool)json["isWalking"];
            item.LineName = json["lineName"];
            item.PassStopCount = json["passStopCount"];
            item.Route = Bridge.ToGeometry(ServerGeometry.FromJson((JsonObject)json["route"]));
            item.StartIndex = (int)json["startIndex"];
            item.StartPosition = JsonHelper.ToPoint2D((JsonObject)json["startPosition"]);
            item.StartStopName = json["startStopName"];

            return item;
        }
    }
}
