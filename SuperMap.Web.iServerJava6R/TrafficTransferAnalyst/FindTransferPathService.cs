﻿using System;
using SuperMap.Web.Service;
using System.Collections.Generic;
using SuperMap.Web.Core;
using System.Windows.Browser;
using System.Json;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTransferPathService_Title}</para>
    /// </summary>
    public class FindTransferPathService : ServiceBase
    {
        /// <summary>${iServerJava6R_FindTransferPathService_constructor_D}</summary>
        public FindTransferPathService()
        { }
        /// <summary>${iServerJava6R_FindTransferPathService_constructor_param_url}</summary>
        public FindTransferPathService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_FindTransferPathService_method_ProcessAsync_param_Parameters}</summary>
        public void ProcessAsync(FindTransferPathParameter paramer, object state)
        {
            GenerateAbsoluteUrl(paramer);
            base.SubmitRequest(this.Url, GetDictionaryParameters(paramer), new EventHandler<RequestEventArgs>(FindTransferPathService_Complated), state, false, false, false);
        }

        private void GenerateAbsoluteUrl(FindTransferPathParameter parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }

            if (!this.Url.EndsWith("/"))
            {
                this.Url += "/";
            }
            this.Url += "path.rjson?";
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(FindTransferPathParameter parameters)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            string jsonPoints = "[";
            List<string> jsonL = new List<string>();
            if(parameters.Points.GetType()==typeof(Point2D[]))
            {
                Point2D[] points = parameters.Points as Point2D[];
                for (int i = 0; i < points.Length; i++)
                {
                    jsonL.Add(JsonHelper.FromPoint2D(points[i]));
                }
            }
            else if (parameters.Points.GetType() == typeof(long[]))
            {
                long[] points = parameters.Points as long[];
                for (int i = 0; i < points.Length; i++)
                {
                    jsonL.Add(points[i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                }
            }

            jsonPoints += string.Join(",", jsonL);
            jsonPoints += "]";
            
            dic["points"] = jsonPoints;
            TransferLine[] lines = new TransferLine[parameters.TransferLines.Length];
            for (int i = 0; i < parameters.TransferLines.Length; i++)
            {
                lines[i] = new TransferLine();
                lines[i].EndStopIndex = parameters.TransferLines[i].EndStopIndex;
                lines[i].LineID = parameters.TransferLines[i].LineID;
                lines[i].StartStopIndex = parameters.TransferLines[i].StartStopIndex;
            }

            string jsonLines = "[";
            List<string> lJsonLine = new List<string>();
            foreach (TransferLine line in lines)
            {
                lJsonLine.Add(TransferLine.ToJson(line));
            }
            jsonLines += string.Join(",", lJsonLine);
            jsonLines += "]";
            dic["transferLines"] = jsonLines;
            return dic;
        }

        private void FindTransferPathService_Complated(object sender, RequestEventArgs args)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(args.Result);
            TransferGuide result = TransferGuide.FromJson(jsonObject);

            FindTransferPathEventArgs e = new FindTransferPathEventArgs(result, args.Result, args.UserState);
            lastResult = result;
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }
        /// <summary>${iServerJava6R_FindTransferPathService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FindTransferPathEventArgs> ProcessCompleted;

        private TransferGuide lastResult;
        /// <summary>${iServerJava6R_FindTransferPathService_attribute_lastResult_D}</summary>
        public TransferGuide LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
