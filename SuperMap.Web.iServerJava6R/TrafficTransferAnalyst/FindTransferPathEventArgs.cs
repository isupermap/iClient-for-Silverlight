﻿using System;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.TrafficTransferAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTransferPathEventArgs_Title}</para>
    /// </summary>
    public class FindTransferPathEventArgs : ServiceEventArgs 
    {
        /// <summary>${iServerJava6R_FindTransferPathEventArgs_constructor_D}</summary>
        public FindTransferPathEventArgs(TransferGuide result, string originResult, object state)
            :base(state)
        {
            this.FindTransferPathResult = result;
            this.OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_FindTransferPathEventArgs_attribute_TransferGuide_D}</summary>
        public TransferGuide FindTransferPathResult
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_FindTransferPathEventArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
    }
}
