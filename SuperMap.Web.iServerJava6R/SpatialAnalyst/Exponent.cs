﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 该类定义了泛克吕金（UniversalKriging）插值时样点数据中趋势面方程的阶数的类型常量。 样点数据集中样点之间固有的某种趋势，可以通过函数或者多项式的拟合呈现。
    /// </summary>
    public enum Exponent
    {
        /// <summary>
        /// 阶数为1。
        /// </summary>
        EXP1,

        /// <summary>
        /// 阶数为2。
        /// </summary>
        EXP2
    }
}
