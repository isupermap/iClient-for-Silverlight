﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using System.Json;
using SuperMap.Web.Core;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{

    /// <summary>
    /// 	<para>${iServerJava6R_RouteLocatorResult_Title}</para>
    /// </summary>
    public class RouteLocatorResult
    {
        /// <summary>${iServerJava6R_RouteLocatorResult_constructor_D}</summary>
        public RouteLocatorResult()
        {
            
        }
        /// <summary>
        /// ${iServerJava6R_RouteLocatorResult_attribute_Image_D}
        /// </summary>
        public NAResultMapImage Image
        {
            get;
            set;
        }
        /// <summary>
        /// ${iServerJava6R_RouteLocatorResult_attribute_Succeed_D}
        /// </summary>
        public Boolean Succeed
        {
            get;
            set;
        }
        /// <summary>
        /// ${iServerJava6R_RouteLocatorResult_attribute_Message_D}
        /// </summary>
        public String Message
        {
            get;
            set;
        }
        /// <summary>
        /// ${iServerJava6R_RouteLocatorResult_attribute_ResultGeometry_D}
        /// </summary>
        public Geometry ResultGeometry
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteLocatorResult_method_fromJson_D}</summary>
        internal static RouteLocatorResult FromJson(JsonObject json)
        { 
            if(json == null){
                return null;
            }
            RouteLocatorResult result = new RouteLocatorResult();
           result.Message=(string)json["message"];
           result.Succeed=(bool)json["succeed"];
           result.ResultGeometry = Bridge.ToGeometry(ServerGeometry.FromJson((JsonObject)json["resultGeometry"]));
           result.Image = NAResultMapImage.FromJson((JsonObject)json["image"]);
           return result;
        }
    }
}
