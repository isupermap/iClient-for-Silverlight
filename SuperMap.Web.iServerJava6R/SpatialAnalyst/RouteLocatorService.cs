﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using System.Json;
using System.Windows.Browser;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_RouteLocatorService_Title}</para>
    /// </summary>
    public class RouteLocatorService:ServiceBase
    {
        /// <summary>${iServerJava6R_RouteLocatorService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_RouteLocatorService_constructor_overloads_D}</overloads>
        public RouteLocatorService()
        { 
        
        }
        /// <summary>${iServerJava6R_RouteLocatorService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_RouteLocatorService_constructor_param_url}</param>
        public RouteLocatorService(string url)
            :base(url)
        { 
            
        }
        /// <summary>${iServerJava6R_RouteLocatorService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_RouteLocatorService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(RouteLocatorParameters RouteLocatorPostParameter)
        { 
            ProcessAsync(RouteLocatorPostParameter,null);
        }
        /// <summary>${iServerJava6R_RouteLocatorService_method_ProcessAsync_D}</summary>
        /// <param name="param">${iServerJava6R_RouteLocatorService_method_ProcessAsync_param_param}</param>
        /// <param name="state">${iServerJava6R_RouteLocatorService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(RouteLocatorParameters RouteLocatorPostParameter, object state)
        {
            GenerateAbsoluteUrl(RouteLocatorPostParameter);
            base.SubmitRequest(this.Url, GetDictionaryParameter(RouteLocatorPostParameter), new EventHandler<RequestEventArgs>(RouteLocatorService_Complated), state, true, false, false);
        }

        private void GenerateAbsoluteUrl(RouteLocatorParameters RouteLocatorPostParameter)
        {
            if ( RouteLocatorPostParameter== null)
            {
                throw new ArgumentNullException("请求服务参数为空！");
            }
            if (this.Url.EndsWith("/"))
            {
                this.Url += "geometry/routelocator.json?debug=true&returnContent=true";
            }
            else
            { 
                this.Url += "/geometry/routelocator.json?debug=true&returnContent=true";
            }
            //http://localhost:8090/iserver/services/spatialanalyst-changchun/restjsr/spatialanalyst/geometry/routelocator  
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameter(RouteLocatorParameters parameters)
        {
            return RouteLocatorParameters.ToDictionary(parameters);
        }

        private void RouteLocatorService_Complated(object sender, RequestEventArgs args)
        {
            JsonObject json = (JsonObject)JsonObject.Parse(args.Result);

            RouteLocatorResult result = RouteLocatorResult.FromJson(json);

            lastResult = result;
            RouteLocatorEventArgs e = new RouteLocatorEventArgs(result, args.Result, args.UserState);

            if(ProcessCompleted!=null)    
            {
                ProcessCompleted(this, e); 
            }
        }
        /// <summary>${iServerJava6R_RouteLocatorService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<RouteLocatorEventArgs> ProcessCompleted;

        private RouteLocatorResult lastResult;
        /// <summary>${iServerJava6R_RouteCalculateMeasureService_lastResult_D}</summary>
        public RouteLocatorResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
