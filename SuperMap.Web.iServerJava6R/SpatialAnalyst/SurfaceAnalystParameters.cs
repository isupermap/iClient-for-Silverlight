﻿
namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ExtractParameters_Title}</para>
    /// 	<para>${iServerJava6R_ExtractParameters_Description}</para>
    /// </summary>
    public class SurfaceAnalystParameters
    {
        /// <summary>${iServerJava6R_ExtractParameters_constructor_D}</summary>
        public SurfaceAnalystParameters()
        { }
        /// <summary>${iServerJava6R_ExtractParamsSetting_attribute_ExtractMethod_D}</summary>
        public SurfaceAnalystMethod SurfaceAnalystMethod
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ExtractParamsSetting_attribute_Resolution_D}</summary>
        public double Resolution
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_SurfaceAnalystParameters_attribute_ExtractParamsSetting_D}</summary>
        public SurfaceAnalystParametersSetting ParametersSetting
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_SurfaceAnalystParameters_attribute_MaxReturnRecordCount_D}</summary>
        public int MaxReturnRecordCount
        {
            get;
            set;
        }
    }
}
