﻿using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ExtractEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_ExtractEventArgs_Description}</para>
    /// </summary>
    public class SurfaceAnalystEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_ExtractEventArgs_constructor_D}</summary>
        public SurfaceAnalystEventArgs(SurfaceAnalystResult result, string originResult, object state)
            : base(state)
        {
            this.Result = result;
            this.OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_ExtractEventArgs_attribute_Result_D}</summary>
        public SurfaceAnalystResult Result
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_ExtractEventArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
    }
}
