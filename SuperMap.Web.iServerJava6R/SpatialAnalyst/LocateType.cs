﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 根据路由对象定位空间对象的类型
    /// </summary>
    public enum LocateType
    {
        /// <summary>
        /// 根据起始M值和终止M值定位线对象
        /// </summary>
        LINE,
        /// <summary>
        /// 根据起始M值和终止M值定位路由对象
        /// </summary>
        LINEM,
        /// <summary>
        /// 根据M值定位点对象
        /// </summary>
        POINT
    }
}
