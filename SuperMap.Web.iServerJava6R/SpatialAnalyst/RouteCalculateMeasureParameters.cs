﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.Core;
using System.Collections.Generic;
using SuperMap.Web.Utilities;
using System.Globalization;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_RouteCalculateMeasureParameters_Title}</para>
    /// </summary>
    public class RouteCalculateMeasureParameters
    {

        /// <summary>${iServerJava6R_RouteCalculateMeasureParameters_constructor_D}</summary>
        public RouteCalculateMeasureParameters()
        { 
            
        }
        /// <summary>${iServerJava6R_RouteCalculateMeasureParameters_attribute_SourceRoute_D}</summary>
        public Route SourceRoute
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteCalculateMeasureParameters_attribute_Point_D}</summary>
        public Point2D Point
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteCalculateMeasureParameters_attribute_Tolerance_D}</summary>
        public double Tolerance
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteCalculateMeasureParameters_attribute_IsIgnoreGap_D}</summary>
        public Boolean IsIgnoreGap
        {
            get;
            set;
        }

        internal static Dictionary<string, string> ToDictionary(RouteCalculateMeasureParameters param)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("sourceRoute",ServerGeometry.ToJson(param.SourceRoute.ToServerGeometry()));
            dic.Add("point",JsonHelper.FromPoint2D(param.Point));
            dic.Add("tolerance", param.Tolerance.ToString(CultureInfo.InvariantCulture));
            dic.Add("isIgnoreGap", param.IsIgnoreGap.ToString(System.Globalization.CultureInfo.InvariantCulture));

            return dic;

            
        }
    }
}
