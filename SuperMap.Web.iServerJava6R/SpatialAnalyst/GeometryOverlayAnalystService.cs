﻿using System;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_GeometryOverlayAnalystService_Title}</para>
    /// 	<para>${iServerJava6R_GeometryOverlayAnalystService_Description}</para>
    /// </summary>
    public class GeometryOverlayAnalystService : ServiceBase
    {
        /// <summary>${iServerJava6R_GeometryOverlayAnalystService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_GeometryOverlayAnalystService_constructor_overloads_D}</overloads>
        public GeometryOverlayAnalystService()
        {
        }
        /// <summary>${iServerJava6R_GeometryOverlayAnalystService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_GeometryOverlayAnalystService_constructor_param_url}</param>
        public GeometryOverlayAnalystService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_GeometryOverlayAnalystService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_GeometryOverlayAnalystService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(GeometryOverlayAnalystParameters OverlayParams)
        {
            ProcessAsync(OverlayParams, null);
        }
        /// <summary>${iServerJava6R_GeometryOverlayAnalystService_method_ProcessAsync_D}</summary>
        /// <param name="Overlayparams">${iServerJava6R_GeometryOverlayAnalystService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${iServerJava6R_GeometryOverlayAnalystService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GeometryOverlayAnalystParameters Overlayparams, object state)
        {
            GenerateAbsoluteUrl(Overlayparams);
            base.SubmitRequest(this.Url, GetDictionaryParameters(Overlayparams), new EventHandler<RequestEventArgs>(OverlayAnalystService_Complated), state, true, false, false);
        }

        private void GenerateAbsoluteUrl(GeometryOverlayAnalystParameters Overlayparams)
        {
            if (Overlayparams == null)
            {
                throw new ArgumentNullException("请求服务参数为空！");
            }

            //http://192.168.11.154:8090/iserver/services/spatialanalyst-sample/restjsr/spatialanalyst/geometry/overlay
            if (this.Url.EndsWith("/"))
            {
                this.Url += "geometry/overlay.json?debug=true&returnContent=true";
            }
            else
            {
                this.Url += "/geometry/overlay.json?debug=true&returnContent=true";
            }
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(GeometryOverlayAnalystParameters parameters)
        {
            return GeometryOverlayAnalystParameters.ToDictionary((GeometryOverlayAnalystParameters)parameters);
        }

        private void OverlayAnalystService_Complated(object sender, RequestEventArgs args)
        {
            JsonObject json = (JsonObject)JsonObject.Parse(args.Result);

            GeometryOverlayAnalystResult result = GeometryOverlayAnalystResult.FromJson((JsonObject)json["resultGeometry"]);
            lastResult = result;
            GeometryOverlayAnalystArgs e = new GeometryOverlayAnalystArgs(result, args.Result, args.UserState);

            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }
        /// <summary>${iServerJava6R_GeometryOverlayAnalystService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GeometryOverlayAnalystArgs> ProcessCompleted;

        private GeometryOverlayAnalystResult lastResult;
        /// <summary>${iServerJava6R_GeometryOverlayAnalystService_lastResult_D}</summary>
        public GeometryOverlayAnalystResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
