﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using System.Windows.Browser;
using System.Json;
using SuperMap.Web.iServerJava6R.Resources;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    ///     <para>${iServerJava6R_ThiessenAnalystService_Tile}</para>
    ///     <para>${iServerJava6R_ThiessenAnalystService_Description}</para>
    /// </summary>
    public class ThiessenAnalystService:ServiceBase
    {
        /// <summary>${iServerJava6R_ThiessenAnalystService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_ThiessenAnalystService_constructor_overloads_D}</overloads>
        public ThiessenAnalystService() 
        { 
        
        }
        /// <summary>${iServerJava6R_ThiessenAnalystService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_ThiessenAnalystService_constructor_param_url}</param>
        public ThiessenAnalystService(string url)
            : base(url)
        { 

        }

        /// <summary>${iServerJava6R_ThiessenAnalystService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_ThiessenAnalystService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="thiessenParams">${iServerJava6R_ThiessenAnalystService_method_ProcessAsync_param_thiessenParams}</param>
        public void ProcessAsync(ThiessenAnalystParameters thiessenParams)
        {
            ProcessAsync(thiessenParams,null);    
        }

        /// <summary>${iServerJava6R_ThiessenAnalystService_method_ProcessAsync_D}</summary>
        /// <param name="thiessenParams">${iServerJava6R_ThiessenAnalystService_method_ProcessAsync_param_thiessenParams}</param>
        /// /// <param name="state">${iServerJava6R_ThiessenAnalystService_method_processAsync_param_state}</param>
        public void ProcessAsync(ThiessenAnalystParameters thiessenParams, object state)
        {
            GenerateAbsoluteUrl(thiessenParams);
            base.SubmitRequest(this.Url, GetDictionaryParameters(thiessenParams), new EventHandler<RequestEventArgs>(ThiessenAnalystService_Completed), state, true, false, false);
        }

        private void GenerateAbsoluteUrl(ThiessenAnalystParameters thiessenParams)
        {
            if (thiessenParams == null)
            {
                throw new ArgumentNullException("请求服务参数为空");
            }
            if (thiessenParams is DatasetThiessenAnalystParameters)
            {
                var datasetParams = (DatasetThiessenAnalystParameters)thiessenParams;
                if (string.IsNullOrEmpty(datasetParams.DataSet))
                {
                    throw new ArgumentNullException("数据集参数为空");
                }

                if (this.Url == null)
                {
                    throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
                }

                if (this.Url.EndsWith("/"))
                {
                    this.Url += "datasets/" + datasetParams.DataSet + "/thiessenpolygon.json?debug=true&returnContent=true";
                }
                else
                {
                    this.Url += "/datasets/" + datasetParams.DataSet + "/thiessenpolygon.json?debug=true&returnContent=true";
                }
            }
            else if(thiessenParams is GeometryThiessenAnalystParameters) 
            {
                var geometryParams = (GeometryThiessenAnalystParameters)thiessenParams;
                if (this.Url.EndsWith("/"))
                {
                    this.Url += "geometry/thiessenpolygon.json?debug=true&returnContent=true";
                }
                else
                {
                    this.Url += "/geometry/thiessenpolygon.json?debug=true&returnContent=true";
                }
            }
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(ThiessenAnalystParameters thiessenParams)
        {
            if (thiessenParams is DatasetThiessenAnalystParameters)
            {
                return DatasetThiessenAnalystParameters.ToDictionary((DatasetThiessenAnalystParameters)thiessenParams);
            }
            else if (thiessenParams is GeometryThiessenAnalystParameters) 
            {
                return GeometryThiessenAnalystParameters.ToDictionary((GeometryThiessenAnalystParameters)thiessenParams);
            }
            return null;
        }

        private void ThiessenAnalystService_Completed(Object sender, RequestEventArgs args)
        {
            System.Json.JsonObject jsonResult = (System.Json.JsonObject)System.Json.JsonValue.Parse(args.Result);
            ThiessenAnalystResult result = ThiessenAnalystResult.FromJson((JsonObject)jsonResult);
            lastResult = result;
            ThiessenAnalystEventArgs e = new ThiessenAnalystEventArgs(result, args.Result, args.UserState);

            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }

        /// <summary>${iServerJava6R_ThiessenAnalystService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ThiessenAnalystEventArgs> ProcessCompleted;

        private ThiessenAnalystResult lastResult;

        /// <summary>${iServerJava6R_ThiessenAnalystService_attribute_LastResult_D}</summary>
        public ThiessenAnalystResult LastResult 
        {
            get 
            {
                return lastResult;
            }
            set 
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
