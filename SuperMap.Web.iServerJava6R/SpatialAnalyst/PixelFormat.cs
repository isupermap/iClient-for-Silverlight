﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 定义栅格与影像数据存储的像素格式枚举。
    /// 光栅数据结构实际上就是像元的阵列，像元（或像素）是光栅数据的最基本信息存储单位，本枚举类包含了表示一个像元（或像素）的字节长度。
    /// 在 SuperMap 中有两种类型的光栅数据：栅格数据集和影像数据集（参见 DatasetGridInfo和DatasetImageInfo）。 栅格数据集多用来进行栅格分析，因而其像元值为地物的属性值，如高程，降水量等；而影像数据集一般用来进行显示或作为底图，因而其像元值为颜色值或颜色的索引值。
    /// </summary>
    public enum PixelFormat
    {
        BIT16,

        BIT32,

        BIT64,

        DOUBLE,

        SINGLE,

        UBIT1,

        UBIT24,

        UBIT32,

        UBIT4,

        UBIT8
    }
}
