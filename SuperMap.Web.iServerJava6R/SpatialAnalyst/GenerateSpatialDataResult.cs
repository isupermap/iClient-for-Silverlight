﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
	/// <summary>
    /// 	<para>${iServerJava6R_GenerateSpatialDataResult_Title}</para>
    /// </summary>
    public class GenerateSpatialDataResult
    {
		/// <summary>${iServerJava6R_GenerateSpatialDataResult_constructor_D}</summary>
        public GenerateSpatialDataResult()
        {

        }
		/// <summary>
        /// ${iServerJava6R_GenerateSpatialDataResult_attribute_Dataset_D}</summary>
        /// </summary>
        public string Dataset
        {
            get;
            set;
        }
		/// <summary>
        /// ${iServerJava6R_GenerateSpatialDataResult_attribute_Succeed_D}</summary>
        /// </summary>
        public bool Succeed
        {
            get;
            set;
        }
		/// <summary>
        /// ${iServerJava6R_GenerateSpatialDataResult_attribute_Message_D}</summary>
        /// </summary>
        public string Message
        {
            get;
            set;
        }
		/// <summary>
        /// ${iServerJava6R_GenerateSpatialDataResult_attribute_Recordset_D}</summary>
        /// </summary>
        public Recordset Recordset
        {
            get;
            set;
        }
		/// <summary>${iServerJava6R_GenerateSpatialDataResult_method_fromJson_D}</summary>
        public static GenerateSpatialDataResult FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            GenerateSpatialDataResult result=new GenerateSpatialDataResult();
            result.Dataset = json["dataset"];
            result.Message = json["message"];
            result.Succeed = (bool)json["succeed"];
            result.Recordset = Recordset.FromJson((JsonObject)json["recordset"]);
            return result;
        }
    }
}
