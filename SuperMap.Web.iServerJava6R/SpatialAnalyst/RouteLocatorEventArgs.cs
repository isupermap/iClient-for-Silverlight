﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_RouteLocatorEventArgs_Title}</para>
    /// </summary>
    public class RouteLocatorEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_RouteLocatorEventArgs_constructor_D}</summary>
        public RouteLocatorEventArgs(RouteLocatorResult result, string originResult, object state)
            : base(state)
        {
            this.Result = result;
            this.OriginResult = originResult;

        }
        /// <summary>${iServerJava6R_RouteLocatorResult_attribute_Result_D}</summary>
        public RouteLocatorResult Result { get; private set; }
        /// <summary>${iServerJava6R_RouteLocatorResult_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
