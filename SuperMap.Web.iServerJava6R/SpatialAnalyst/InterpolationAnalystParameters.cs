﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Core;
using System.Collections.Generic;
using SuperMap.Web.Utilities;
using System.Text;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_InterpolationAnalystParameters_Description}</para>
    /// </summary>
    public class InterpolationAnalystParameters
    {
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_constructor_D}</summary>
        public InterpolationAnalystParameters()
        {
            Resolution = 3000;
            MaxReturnRecordCount = 1000;
            PixelFormat = PixelFormat.BIT16;
            InterpolationAnalystType = InterpolationAnalystType.DATASET;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_Bounds_D}</summary>
        public Rectangle2D Bounds
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_Dataset_D}</summary>
        public string Dataset
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_FilterQueryParameter_D}</summary>
        public FilterParameter FilterQueryParameter
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_MaxReturnRecordCount_D}</summary>
        public int MaxReturnRecordCount
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_OutputDataSource_D}</summary>
        public string OutputDataSource
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_OutputDataset_D}</summary>
        public string OutputDataset
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_PixelFormat_D}</summary>
        public PixelFormat PixelFormat
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_Resolution_D}</summary>
        public double Resolution
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_SearchRadius_D}</summary>
        public double SearchRadius
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_ZValueFieldName_D}</summary>
        public string ZValueFieldName
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_InterpolationAnalystType_D}</summary>
        public InterpolationAnalystType InterpolationAnalystType { get; set; }

        /// <summary>${iServerJava6R_InterpolationAnalystParameters_attribute_InputPoints_D}</summary>
        public IList<Point2D> InputPoints { get; set; }

        internal virtual Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            if (this.Bounds != Rectangle2D.Empty)
            {
                dic.Add("bounds", JsonHelper.FromRectangle2D(this.Bounds));
            }

            if (!string.IsNullOrEmpty(this.Dataset) && this.InterpolationAnalystType==InterpolationAnalystType.DATASET)
            {
                dic.Add("datasetName", this.Dataset);
            }
            if (this.MaxReturnRecordCount > 0 && this.InterpolationAnalystType == InterpolationAnalystType.DATASET)
            {
                dic.Add("maxReturnRecordCount", this.MaxReturnRecordCount.ToString(System.Globalization.CultureInfo.InvariantCulture));
            }
            if (this.FilterQueryParameter != null && this.InterpolationAnalystType == InterpolationAnalystType.DATASET)
            {
                dic.Add("filterQueryParameter", FilterParameter.ToJson(this.FilterQueryParameter));
            }
            if (!string.IsNullOrEmpty(this.OutputDataset))
            {
                dic.Add("outputDatasetName", this.OutputDataset);
            }
            if (!string.IsNullOrEmpty(this.OutputDataSource))
            {
                dic.Add("outputDatasourceName", this.OutputDataSource);
            }
            dic.Add("pixelFormat", this.PixelFormat.ToString());
            dic.Add("resolution", this.Resolution.ToString(System.Globalization.CultureInfo.InvariantCulture));

            dic.Add("searchRadius", this.SearchRadius.ToString(System.Globalization.CultureInfo.InvariantCulture));

            if (!string.IsNullOrEmpty(ZValueFieldName))
            {
                dic.Add("zValueFieldName", this.ZValueFieldName);
            }
            if (this.InterpolationAnalystType == InterpolationAnalystType.GEOMETRY)
            {
                StringBuilder input = new StringBuilder();
                input.Append("[");
                for (int i = 0; i < InputPoints.Count; i++)
                {
                    Point2D point = InputPoints[i];

                    var x = point.X.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    var y = point.Y.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    var z = Convert.ToDouble(point.Tag).ToString(System.Globalization.CultureInfo.InvariantCulture);
                    input.Append(JsonHelper.FromPoint2DWithTag(point, "z", true));
                    if (i != InputPoints.Count - 1)
                        input.Append(",");
                }
                input.Append("]");
                dic.Add("inputPoints", input.ToString());
            }

            return dic;
        }


    }
}
