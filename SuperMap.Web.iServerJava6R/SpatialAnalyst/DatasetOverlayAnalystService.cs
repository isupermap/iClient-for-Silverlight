﻿using System;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.iServerJava6R.Resources;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_DatasetOverlayAnalystService_Title}</para>
    /// 	<para>${iServerJava6R_DatasetOverlayAnalystService_Description}</para>
    /// </summary>
    public class DatasetOverlayAnalystService : ServiceBase
    {
        /// <summary>${iServerJava6R_DatasetOverlayAnalystService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_DatasetOverlayAnalystService_constructor_overloads_D}</overloads>
        public DatasetOverlayAnalystService()
        {
        }
        /// <summary>${iServerJava6R_DatasetOverlayAnalystService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_DatasetOverlayAnalystService_constructor_param_url}</param>
        public DatasetOverlayAnalystService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_DatasetOverlayAnalystService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_DatasetOverlayAnalystService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(DatasetOverlayAnalystParameters OverlayParams)
        {
            ProcessAsync(OverlayParams, null);
        }
        /// <summary>${iServerJava6R_DatasetOverlayAnalystService_method_ProcessAsync_D}</summary>
        /// <param name="Overlayparams">${iServerJava6R_DatasetOverlayAnalystService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${iServerJava6R_DatasetOverlayAnalystService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(DatasetOverlayAnalystParameters Overlayparams, object state)
        {
            GenerateAbsoluteUrl(Overlayparams);
            base.SubmitRequest(this.Url, GetDictionaryParameters(Overlayparams), new EventHandler<RequestEventArgs>(OverlayAnalystService_Complated), state, true, false, false);
        }

        private void GenerateAbsoluteUrl(DatasetOverlayAnalystParameters Overlayparams)
        {
            if (Overlayparams == null)
            {
                throw new ArgumentNullException("请求服务参数为空！");
            }

            if (string.IsNullOrEmpty(((DatasetOverlayAnalystParameters)Overlayparams).SourceDataset) || string.IsNullOrEmpty(((DatasetOverlayAnalystParameters)Overlayparams).SourceDataset))
            {
                throw new ArgumentNullException("数据集参数为空");
            }

            if (this.Url == null)
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            //http://192.168.11.154:8090/iserver/services/spatialanalyst-sample/restjsr/spatialanalyst/datasets/SamplesP@Interpolation/overlay
            if (this.Url.EndsWith("/"))
            {
                this.Url += "datasets/" + ((DatasetOverlayAnalystParameters)Overlayparams).SourceDataset + "/overlay.json?returnContent=true&debug=true";
            }
            else
            {
                this.Url += "/datasets/" + ((DatasetOverlayAnalystParameters)Overlayparams).SourceDataset + "/overlay.json?returnContent=true&debug=true";
            }
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(DatasetOverlayAnalystParameters parameters)
        {
            return DatasetOverlayAnalystParameters.ToDictionary((DatasetOverlayAnalystParameters)parameters);
        }

        private void OverlayAnalystService_Complated(object sender, RequestEventArgs args)
        {
            System.Json.JsonObject jsonResult = (System.Json.JsonObject)System.Json.JsonValue.Parse(args.Result);
            DatasetOverlayAnalystResult result = DatasetOverlayAnalystResult.FromJson((JsonObject)jsonResult["recordset"]);
            lastResult = result;
            DatasetOverlayAnalystArgs e = new DatasetOverlayAnalystArgs(result, args.Result, args.UserState);

            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }
        /// <summary>${iServerJava6R_DatasetOverlayAnalystService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<DatasetOverlayAnalystArgs> ProcessCompleted;

        private DatasetOverlayAnalystResult lastResult;
        /// <summary>${iServerJava6R_DatasetOverlayAnalystService_lastResult_D}</summary>
        public DatasetOverlayAnalystResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
