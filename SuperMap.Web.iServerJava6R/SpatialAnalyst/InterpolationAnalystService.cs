﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using System.Windows.Browser;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationAnalystService_Title}</para>
    /// 	<para>${iServerJava6R_InterpolationAnalystService_Description}</para>
    /// 	<para><img src="spatialAlyPoint.png"/></para>
    /// 	<para><img src="spatialAlyGridPoint.png"/></para>
    /// 	<para><img src="spatialAlyInterpolated.png"/></para>
    /// 	<para><img src="spatialAlyInterpolated2.png"/></para>
    /// </summary>
    public class InterpolationAnalystService : ServiceBase
    {
        /// <summary>${iServerJava6R_InterpolationAnalystService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_InterpolationAnalystService_constructor_overloads_D}</overloads>
        public InterpolationAnalystService()
        {
        }
        /// <summary>${iServerJava6R_InterpolationAnalystService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_InterpolationAnalystService_constructor_param_url}</param>
        public InterpolationAnalystService(string url)
            : base(url)
        {
        }
        /// <summary>${iServerJava6R_InterpolationAnalystService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_InterpolationAnalystService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(InterpolationAnalystParameters interpolationAnalystParams)
        {
            ProcessAsync(interpolationAnalystParams, null);
        }
        /// <summary>${iServerJava6R_InterpolationAnalystService_method_ProcessAsync_D}</summary>
        /// <param name="surfaceAnalystParameters">${iServerJava6R_InterpolationAnalystService_method_ProcessAsync_param_parameter}</param>
        /// <param name="state">${iServerJava6R_InterpolationAnalystService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(InterpolationAnalystParameters interpolationAnalystParams, object state)
        {
            GenerateAbsoluteUrl(interpolationAnalystParams);
            base.SubmitRequest(this.Url, GetDictionaryParameters(interpolationAnalystParams), new EventHandler<RequestEventArgs>(InterpolationAnalystService_Complated), state, true, false, false);
        }

        private void GenerateAbsoluteUrl(InterpolationAnalystParameters interpolationAnalystParams)
        {
            if (interpolationAnalystParams == null)
            {
                throw new ArgumentNullException("请求服务参数为空!");
            }
            if (!this.Url.EndsWith("/"))
            {
                this.Url += "/";
            }
            if (interpolationAnalystParams is InterpolationDensityAnalystParameters)
            {
                this.Url += "datasets/" + interpolationAnalystParams.Dataset + "/interpolation/density.json?debug=true&returnContent=true";
            }
            if (interpolationAnalystParams is InterpolationIDWAnalystParameters)
            {
                if (interpolationAnalystParams.InterpolationAnalystType == InterpolationAnalystType.DATASET)
                {
                    this.Url += "datasets/" + interpolationAnalystParams.Dataset + "/interpolation/idw.json?debug=true&returnContent=true";
                }
                else
                {
                    this.Url += "geometry/interpolation/idw.json?debug=true&returnContent=true";
                }
            }
            if (interpolationAnalystParams is InterpolationRBFAnalystParameters)
            {
                this.Url += "datasets/" + interpolationAnalystParams.Dataset + "/interpolation/rbf.json?debug=true&returnContent=true";
            }
            if (interpolationAnalystParams is InterpolationKrigingAnalystParameters)
            {
                this.Url += "datasets/" + interpolationAnalystParams.Dataset + "/interpolation/kriging.json?debug=true&returnContent=true";
            }
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(InterpolationAnalystParameters parameters)
        {
            return parameters.ToDictionary();
        }

        private void InterpolationAnalystService_Complated(object sender, RequestEventArgs args)
        {
            InterpolateAnalystArgs e = new InterpolateAnalystArgs(CheckResult(args), args.Result, args.UserState);
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }
        private InterpolationAnalysResult lastResult;
        /// <summary>${iServerJava6R_ExtractService_lastResult_D}</summary>
        private InterpolationAnalysResult CheckResult(RequestEventArgs args)
        {
            // JsonObject resultJson = (JsonObject)JsonObject.Parse(args.Result);

            InterpolationAnalysResult result = InterpolationAnalysResult.FromJson(args.Result);
            lastResult = result;
            return result;
        }

        [ScriptableMember]

        /// <summary>${iServerJava6R_ExtractService_event_processCompleted_D}</summary>
        public event EventHandler<InterpolateAnalystArgs> ProcessCompleted;


        private void OnProcessCompleted(InterpolateAnalystArgs args)
        {
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, args);
            }
        }
    }
}
