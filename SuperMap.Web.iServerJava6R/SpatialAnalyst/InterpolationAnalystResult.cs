﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationAnalysResult_Title}</para>
    /// </summary>
    public class InterpolationAnalysResult : SpatialAnalystResult
    {
        /// <summary>${iServerJava6R_InterpolationAnalysResult_attribute_Dataset_D}</summary>
        public string Dataset
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalysResult_attribute_Success_D}</summary>
        public bool Success
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationAnalysResult_attribute_Message_D}</summary>
        public string Message
        {
            get;
            set;
        }

        internal static InterpolationAnalysResult FromJson(string jsonResult)
        {
            JsonObject result = (JsonObject)JsonObject.Parse(jsonResult);

            InterpolationAnalysResult interpolationAnalysResult = new InterpolationAnalysResult();
            interpolationAnalysResult.Message = (string)result["message"];
            interpolationAnalysResult.Dataset = (string)result["dataset"];
            interpolationAnalysResult.Success = bool.Parse(result["succeed"].ToString());

            return interpolationAnalysResult;
        }
    }
}
