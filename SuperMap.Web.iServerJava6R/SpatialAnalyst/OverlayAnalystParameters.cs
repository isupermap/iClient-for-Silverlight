﻿
namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_OverlayAnalystParms_Title}</para>
    /// 	<para>${iServerJava6R_OverlayAnalystParms_Description}</para>
    /// </summary>
    public class OverlayAnalystParameters
    {
        /// <summary>${iServerJava6R_OverlayAnalystParms_constructor_D}</summary>
        public OverlayAnalystParameters()
        { }
        /// <summary>${iServerJava6R_OverlayAnalystParms_attribute_operation_D}</summary>
        public OverlayOperationType Operation
        {
            get;
            set;
        }
    }
}
