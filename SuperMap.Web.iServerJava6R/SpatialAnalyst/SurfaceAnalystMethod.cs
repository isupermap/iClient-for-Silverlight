﻿
namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ExtractMethod_Title}</para>
    /// 	<para>${iServerJava6R_ExtractMethod_Description}</para>
    /// </summary>
    /// <remarks>${iServerJava6R_ExtractMethod_Remarks}</remarks>
    public enum SurfaceAnalystMethod
    {
        /// <summary>${iServerJava6R_ExtractMethod_attribute_ISOLINE_D}</summary>
        ISOLINE,    //等值线
        /// <summary>${iServerJava6R_ExtractMethod_attribute_ISOREGION_D}</summary>
        ISOREGION   //等值面
    }
}
