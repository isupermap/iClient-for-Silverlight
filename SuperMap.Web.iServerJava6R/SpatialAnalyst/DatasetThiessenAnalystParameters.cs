﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_DatasetThiessenAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_DatasetThiessenAnalystParameters_Description}</para>
    /// </summary>
    public class DatasetThiessenAnalystParameters : ThiessenAnalystParameters
    {
        /// <summary>${iServerJava6R_DatasetThiessenAnalystParameters_constructor_D}</summary>
        public DatasetThiessenAnalystParameters() { }

        internal static System.Collections.Generic.Dictionary<string, string> ToDictionary(DatasetThiessenAnalystParameters datasetThiessenParams)
        {
            System.Collections.Generic.Dictionary<string, string> dict = new System.Collections.Generic.Dictionary<string, string>();
            if (datasetThiessenParams.ClipRegion != null)
            {
                dict.Add("clipRegion", ServerGeometry.ToJson(datasetThiessenParams.ClipRegion));
            }
            
            dict.Add("createResultDataset", datasetThiessenParams.CreateResultDataset.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
            dict.Add("resultDatasetName", datasetThiessenParams.ResultDatasetName.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dict.Add("resultDatasourceName", datasetThiessenParams.ResultDatasourceName.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dict.Add("returnResultRegion", datasetThiessenParams.ReturnResultRegion.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
            if (datasetThiessenParams.FilterQueryParameter != null)
            {
                dict.Add("filterQueryParameter", FilterParameter.ToJson(datasetThiessenParams.FilterQueryParameter));
            }
           
            return dict;
        }

        /// <summary>${iServerJava6R_DatasetThiessenAnalystParameters_attribute_FilterQueryParameter_D}</summary>
        public FilterParameter FilterQueryParameter
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_DatasetThiessenAnalystParameters_attribute_DataSet_D}</summary>
        public String DataSet
        {
            get;
            set;
        }

    }
}
