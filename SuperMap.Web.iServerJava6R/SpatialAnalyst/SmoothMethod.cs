﻿
namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_SmoothMethod_Title}</para>
    /// 	<para>${iServerJava6R_SmoothMethod_Description}</para>
    /// </summary>
    public enum SmoothMethod
    {
        /// <summary>${iServerJava6R_SmoothMethod_attribute_BSPLINE_D}</summary>
        BSPLINE,           //B 样条法。 
        /// <summary>${iServerJava6R_SmoothMethod_attribute_POLISH_D}</summary>
        POLISH,           //磨角法。 
    }
}
