﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;
using System.Collections.ObjectModel;
using SuperMap.Web.Core;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThiessenAnalystResult_Title}</para>
    /// 	<para>${iServerJava6R_ThiessenAnalystResult_Description}</para>
    /// </summary>
    public class ThiessenAnalystResult : SpatialAnalystResult
    {
        /// <summary>${iServerJava6R_ThiessenAnalystResult_constructor_D}</summary>
        public ThiessenAnalystResult() { }

        /// <summary>${iServerJava6R_ThiessenAnalystResult_attribute_Recorset_D}</summary>
        public ThiessenRecorset Recorset
        {
            get;
            private set;
        }

        /// <summary>${iServerJava6R_ThiessenAnalystResult_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_ThiessenAnalystResult_method_FromJson_return}</returns>
        /// <param name="jsonResult">${iServerJava6R_ThiessenAnalystResult_method_FromJson_param_jsonResult}</param>
        public static ThiessenAnalystResult FromJson(JsonObject jsonResult)
        {
            ThiessenAnalystResult result = new ThiessenAnalystResult();
            result.Recorset = ThiessenRecorset.FromJson(jsonResult);
            return result;
        }
    }

    /// <summary>
    /// 	<para>${iServerJava6R_ThiessenRecorset_Title}</para>
    /// 	<para>${iServerJava6R_ThiessenRecorset_Description}</para>
    /// </summary>
    public class ThiessenRecorset
    {
        /// <summary>${iServerJava6R_ThiessenRecorset_constructor_D}</summary>
        public ThiessenRecorset()
        { }

        /// <summary>${iServerJava6R_ThiessenRecorset_attribute_DatasetName_D}</summary>
        public string DatasetName
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThiessenRecorset_attribute_DatasourceName_D}</summary>
        public string DatasourceName
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThiessenRecorset_attribute_Features_D}</summary>
        public FeatureCollection Features
        {
            get;
            set;
        }

        internal static ThiessenRecorset FromJson(JsonObject json)
        {
             if(json==null)
            {
                return null;
            }

            ThiessenRecorset respond = new ThiessenRecorset();
            respond.DatasetName = json["datasetName"].ToString();
            respond.DatasourceName = json["datasourceName"].ToString();
            JsonArray regions = (JsonArray)json["regions"];
            respond.Features = new FeatureCollection();
            try
            {
                int count = regions.Count;
                for (int i = 0; i < count; i++)
                {
                    JsonObject region = (JsonObject)regions[i];                   
                    ServerGeometry geometry = ServerGeometry.FromJson(region);
                    Feature feature = new Feature();
                    feature.Geometry = geometry.ToGeoRegion() as Core.Geometry;                    
                    respond.Features.Add(feature);
                }
            }
            catch (Exception e) {
                throw new Exception("thansform feature failed!");
            }
            return respond;
        }
    }
}
