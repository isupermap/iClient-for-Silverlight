﻿
using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_GeometryOverlayAnalystParams_Title}</para>
    /// 	<para>${iServerJava6R_GeometryOverlayAnalystParams_Description}</para>
    /// </summary>
    public class GeometryOverlayAnalystParameters : OverlayAnalystParameters
    {
        /// <summary>${iServerJava6R_GeometryOverlayAnalystParams_constructor_D}</summary>
        public GeometryOverlayAnalystParameters()
        { }
        /// <summary>${iServerJava6R_GeometryOverlayAnalystParams_attribute_SourceGeometry_D}</summary>
        public Geometry SourceGeometry
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_GeometryOverlayAnalystParams_attribute_OperateGeometry_D}</summary>
        public Geometry OperateGeometry
        {
            get;
            set;
        }

        internal static System.Collections.Generic.Dictionary<string, string> ToDictionary(GeometryOverlayAnalystParameters geometryOverlayParams)
        {
            var dict = new System.Collections.Generic.Dictionary<string, string>();
            if (geometryOverlayParams.SourceGeometry != null)
            {
                dict.Add("sourceGeometry", ServerGeometry.ToJson(geometryOverlayParams.SourceGeometry.ToServerGeometry()));
            }
            else
            {
                dict.Add("sourceGeometry", ServerGeometry.ToJson(new ServerGeometry()));
            }

            if (geometryOverlayParams.OperateGeometry != null)
            {
                dict.Add("operateGeometry", ServerGeometry.ToJson(geometryOverlayParams.OperateGeometry.ToServerGeometry()));
            }
            else
            {
                dict.Add("operateGeometry", ServerGeometry.ToJson(new ServerGeometry()));
            }

            dict.Add("operation", "\"" + geometryOverlayParams.Operation.ToString() + "\"");
            return dict;
        }
    }
}
