﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 数据返回模式枚举类。
    /// </summary>
    public enum DataReturnMode
    {
        /// <summary>
        /// 返回数据集标识和记录集。
        /// </summary>
        DATASET_AND_RECORDSET,
        /// <summary>
        /// 只返回数据集标识(数据集名称@数据源名称)。
        /// </summary>
        DATASET_ONLY,
        /// <summary>
        /// 只返回记录集。
        /// </summary>
        RECORDSET_ONLY
    }
}
