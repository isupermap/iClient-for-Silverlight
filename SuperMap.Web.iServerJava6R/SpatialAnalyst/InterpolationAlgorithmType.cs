﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationAlgorithmType_Title}</para>
    /// 	<para>${iServerJava6R_InterpolationAlgorithmType_Description}</para>
    /// </summary>
    public enum InterpolationAlgorithmType
    {
        /// <summary>${iServerJava6R_InterpolationAlgorithmType_attribute_KRIGING_D}</summary>
        /// 普通克吕金插值法      
        KRIGING,

        /// <summary>${iServerJava6R_InterpolationAlgorithmType_attribute_SimpleKriging_D}</summary>
        /// 简单克吕金插值法
        SimpleKriging,

        /// <summary>${iServerJava6R_InterpolationAlgorithmType_attribute_UniversalKriging_D}</summary>
        /// 泛克吕金插值法
        UniversalKriging
    }
}
