﻿using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_DatasetOverlayAnalystArgs_Title}</para>
    /// 	<para>${iServerJava6R_DatasetOverlayAnalystArgs_Description}</para>
    /// </summary>
    public class DatasetOverlayAnalystArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_DatasetOverlayAnalystArgs_constructor_D}</summary>
        internal DatasetOverlayAnalystArgs(DatasetOverlayAnalystResult result, string originResult, object state)
            : base(state)
        {
            this.Result = result;
            this.OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_DatasetOverlayAnalystArgs_attribute_Result_D}</summary>
        public DatasetOverlayAnalystResult Result
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_DatasetOverlayAnalystArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
    }
}
