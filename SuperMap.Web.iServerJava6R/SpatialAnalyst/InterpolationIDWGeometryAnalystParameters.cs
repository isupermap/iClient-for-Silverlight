﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Core;
using System.Collections.Generic;
using SuperMap.Web.Utilities;
using System.Text;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationIDWGeometryAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_InterpolationIDWGeometryAnalystParameters_Description}</para>
    /// </summary>
    public class InterpolationIDWGeometryAnalystParameters : InterpolationAnalystParameters
    {
        /// <summary>${iServerJava6R_InterpolationIDWGeometryAnalystParameters_constructor_D}</summary>
        public InterpolationIDWGeometryAnalystParameters()
            : base()
        {
            ZValueScale = 1.0;
            SearchMode = SearchMode.KDTREE_FIXED_RADIUS;
            ExpectedCount = 12;
            Power = 2;
        }

        /// <summary>${iServerJava6R_InterpolationIDWGeometryAnalystParameters_attribute_ZValueScale_D}</summary>
        public double ZValueScale { get; set; }

        /// <summary>${iServerJava6R_InterpolationIDWGeometryAnalystParameters_attribute_SearchMode_D}</summary>
        public SearchMode SearchMode { get; set; }

        /// <summary>${iServerJava6R_InterpolationIDWGeometryAnalystParameters_attribute_ExpectedCount_D}</summary>
        public int ExpectedCount { get; set; }

        /// <summary>${iServerJava6R_InterpolationIDWGeometryAnalystParameters_attribute_Power_D}</summary>
        public int Power { get; set; }

        /// <summary>${iServerJava6R_InterpolationIDWGeometryAnalystParameters_attribute_InputPoints_D}</summary>
        public IList<Point2D> InputPoints { get; set; }

        internal override Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("pixelFormat", this.PixelFormat.ToString());
            dic.Add("zValueScale", this.ZValueScale.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("resolution", this.Resolution.ToString(System.Globalization.CultureInfo.InvariantCulture));
            if (this.Bounds != Rectangle2D.Empty)
            {
                dic.Add("bounds", JsonHelper.FromRectangle2D(this.Bounds));
            }
            dic.Add("searchMode", this.SearchMode.ToString());
            dic.Add("searchRadius", this.SearchRadius.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("expectedCount", this.ExpectedCount.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("power", this.Power.ToString(System.Globalization.CultureInfo.InvariantCulture));
            if (!string.IsNullOrEmpty(this.OutputDataset))
            {
                dic.Add("outputDatasetName", this.OutputDataset);
            }
            if (this.InputPoints != null && this.InputPoints.Count > 0)
            {
                StringBuilder input = new StringBuilder();
                input.Append("[");
                for (int i = 0; i < InputPoints.Count; i++)
                {
                    Point2D point = InputPoints[i];

                    var x = point.X.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    var y = point.Y.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    var z = Convert.ToDouble(point.Tag).ToString(System.Globalization.CultureInfo.InvariantCulture);
                    input.Append(JsonHelper.FromPoint2DWithTag(point, "z", true));
                    if (i != InputPoints.Count - 1)
                        input.Append(",");
                }
                input.Append("]");
                dic.Add("inputPoints", input.ToString());
            }

            return dic;
        }
    }
}
