﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 空间分析数据返回选项类。
    /// <para>
    /// 该类主要用来设置空间分析完成后，返回的数据的一些选项。包括数据返回模式、返回的最大记录数、 结果数据集名称，如果重名如何处置等。
    /// </para>
    /// </summary>
    public class DataReturnOption
    {
        public DataReturnOption()
        {
            DataReturnMode = DataReturnMode.DATASET_ONLY;
        }

        /// <summary>
        /// 数据返回模式，默认为DataReturnMode.DATASET_ONLY。
        /// </summary>
        public DataReturnMode DataReturnMode
        {
            get;
            set;
        }

        /// <summary>
        /// 设置结果数据集标识 当dataReturnMode为 DataReturnMode.DATASET_ONLY或DataReturnMode.DATASET_AND_RECORDSET时有效，作为返回数据集的名称。
        /// </summary>
        public string Dataset
        {
            get;
            set;
        }

        /// <summary>
        /// 如果用户命名的结果数据集名称与已有的数据集重名，是否删除已有的数据集。
        /// </summary>
        public bool DeleteExistResultDataset
        {
            get;
            set;
        }

        /// <summary>
        /// 设置返回的最大记录数，当dataReturnMode为 DataReturnMode.RECORDSET_ONLY或DataReturnMode.DATASET_AND_RECORDSET时有效，小于或者等于0时表示返回所有。
        /// </summary>
        public int ExpectCount
        {
            get;
            set;
        }

        internal static string ToJson(DataReturnOption option)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append(string.Format("\"dataReturnMode\":\"{0}\",", option.DataReturnMode.ToString()));
            sb.Append(string.Format("\"dataset\":\"{0}\",", option.Dataset.ToString()));
            sb.Append(string.Format("\"expectCount\":{0},", option.ExpectCount.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            sb.Append(string.Format("\"deleteExistResultDataset\":{0}", option.DeleteExistResultDataset.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            sb.Append("}");
            return sb.ToString();
        }
    }
}
