﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationKrigingAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_InterpolationKrigingAnalystParameters_Description}</para>
    /// </summary>
    public class InterpolationKrigingAnalystParameters : InterpolationAnalystParameters
    {
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_constructor_D}</summary>
        public InterpolationKrigingAnalystParameters()
            : base()
        {
            VariogramMode = VariogramMode.SPHERICAL;
            Exponent = Exponent.EXP1;
            SearchMode = SearchMode.NONE;
            ExpectedCount = 12;
            MaxPointCountForInterpolation = 200;
            MaxPointCountInNode = 50;
            ZValueScale = 1;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_Type_D}</summary>
        public InterpolationAlgorithmType Type
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_Mean_D}</summary>
        public double Mean
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_Angle_D}</summary>
        public double Angle
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_Nugget_D}</summary>
        public double Nugget
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_Range_D}</summary>
        public double Range
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_Sill_D}</summary>
        public double Sill
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_VariogramMode_D}</summary>
        public VariogramMode VariogramMode
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_Exponent_D}</summary>
        public Exponent Exponent
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_SearchMode_D}</summary>
        public SearchMode SearchMode
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_ExpectedCount_D}</summary>
        public int ExpectedCount
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_MaxPointCountForInterpolation_D}</summary>
        public int MaxPointCountForInterpolation
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_MaxPointCountInNode_D}</summary>
        public int MaxPointCountInNode
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationKrigingAnalystParameters_attribute_ZValueScale_D}</summary>
        public double ZValueScale
        {
            get;
            set;
        }
        internal override System.Collections.Generic.Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dic = base.ToDictionary();
            dic.Add("angle", this.Angle.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("expectedCount", this.ExpectedCount.ToString(System.Globalization.CultureInfo.InvariantCulture));
            if (this.Type == InterpolationAlgorithmType.UniversalKriging)
            {
                dic.Add("exponent", this.Exponent.ToString());
            }
            else if (this.Type == InterpolationAlgorithmType.KRIGING)
            {
                dic.Add("maxPointCountForInterpolation", this.MaxPointCountForInterpolation.ToString(System.Globalization.CultureInfo.InvariantCulture));
                dic.Add("maxPointCountInNode", this.MaxPointCountInNode.ToString(System.Globalization.CultureInfo.InvariantCulture));
            }
            else if (this.Type == InterpolationAlgorithmType.SimpleKriging)
            {
                dic.Add("mean", this.Mean.ToString(System.Globalization.CultureInfo.InvariantCulture));
            }
            dic.Add("nugget", this.Nugget.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("range", this.Range.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("sill", this.Sill.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("type", this.Type.ToString());
            dic.Add("variogramMode", this.VariogramMode.ToString());
            return dic;
        }
    }
}
