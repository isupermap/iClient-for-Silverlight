﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using System.Windows.Browser;
using System.Json;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
	/// <summary>
	/// 	<para>${iServerJava6R_GenerateSpatialDataService_Title}</para>
	/// 	<para>${iServerJava6R_GenerateSpatialDataService_Description}</para>
	/// </summary>
    public class GenerateSpatialDataService:ServiceBase
    {
		/// <summary>${iServerJava6R_GenerateSpatialDataService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_GenerateSpatialDataService_constructor_overloads_D}</overloads>
        public GenerateSpatialDataService()
        {

        }
		/// <summary>${iServerJava6R_GenerateSpatialDataService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_GenerateSpatialDataService_constructor_param_url}</param>
        public GenerateSpatialDataService(string url)
            : base(url)
        {

        }
		 /// <summary>${iServerJava6R_GenerateSpatialDataService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_GenerateSpatialDataService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(GenerateSpatialDataParameters OverlayParams)
        {
            ProcessAsync(OverlayParams, null);
        }
		/// <summary>${iServerJava6R_GenerateSpatialDataService_method_ProcessAsync_D}</summary>
        /// <param name="param">${iServerJava6R_GenerateSpatialDataService_method_ProcessAsync_param_param}</param>
        /// <param name="state">${iServerJava6R_GenerateSpatialDataService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GenerateSpatialDataParameters param, object state)
        {
            GenerateAbsoluteUrl(param);
            base.SubmitRequest(this.Url, GetDictionaryParameters(param), new EventHandler<RequestEventArgs>(GenerateSpatialDataService_Complated), state, true, false, false);
        }

        private void GenerateAbsoluteUrl(GenerateSpatialDataParameters param)
        {
            if (param == null)
            {
                throw new ArgumentNullException("param");
            }
            if (!this.Url.EndsWith("/"))
            {
                this.Url += "/";
            }

            //http://192.168.120.70:8090/iserver/services/spatialAnalysis-MyChangchun/restjsr/spatialanalyst/datasets/RouteDT_road%40Changchun/linearreferencing/generatespatialdata.json
            this.Url += "datasets/" + param.RouteDataset + "/linearreferencing/generatespatialdata.json?debug=true&returnContent=true";
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(GenerateSpatialDataParameters parameters)
        {
            return GenerateSpatialDataParameters.ToDictionary(parameters);
        }

        private void GenerateSpatialDataService_Complated(object sender, RequestEventArgs args)
        {
            JsonObject json = (JsonObject)JsonObject.Parse(args.Result);

            GenerateSpatialDataResult result = GenerateSpatialDataResult.FromJson(json);
            lastResult = result;
            GenerateSpatialDataArgs e = new GenerateSpatialDataArgs(result, args.Result, args.UserState);

            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }
        /// <summary>${iServerJava6R_GenerateSpatialDataService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GenerateSpatialDataArgs> ProcessCompleted;

        private GenerateSpatialDataResult lastResult;
        /// <summary>${iServerJava6R_GenerateSpatialDataService_lastResult_D}</summary>
        public GenerateSpatialDataResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
