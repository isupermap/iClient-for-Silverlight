﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
	/// <summary>
    /// 	<para>${iServerJava6R_GenerateSpatialDataArgs_Title}</para>
    /// </summary>
    public class GenerateSpatialDataArgs : ServiceEventArgs
    {
		/// <summary>${iServerJava6R_GenerateSpatialDataArgs_constructor_D}</summary>
        public GenerateSpatialDataArgs(GenerateSpatialDataResult result, string originResult, object state)
            :base(state)
        {
            Result = result;
            OriginResult = originResult;
        }
		/// <summary>${iServerJava6R_GenerateSpatialDataArgs_attribute_Result_D}</summary>
        public GenerateSpatialDataResult Result
        {
            get;
            private set;
        }
		/// <summary>${iServerJava6R_GenerateSpatialDataArgs_attribute_OriginResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
    }
}
