﻿using System.Json;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ExtractResult_Title}</para>
    /// 	<para>${iServerJava6R_ExtractResult_Description}</para>
    /// </summary>
    public class SurfaceAnalystResult
    {
        /// <summary>${iServerJava6R_ExtractResult_constructor_D}</summary>
        internal SurfaceAnalystResult()
        { }
        /// <summary>${iServerJava6R_ExtractResult_attribute_recordset_D}</summary>
        public Recordset Recordset
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_ExtractResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_ExtractResult_method_fromJson_return}</returns>
        /// <param name="jsonResult">${iServerJava6R_ExtractResult_method_fromJson_param_jsonObject}</param>
        public static SurfaceAnalystResult FromJson(JsonObject jsonResult)
        {
            SurfaceAnalystResult result = new SurfaceAnalystResult();
            result.Recordset = Recordset.FromJson(jsonResult);
            return result;
        }
    }
}
