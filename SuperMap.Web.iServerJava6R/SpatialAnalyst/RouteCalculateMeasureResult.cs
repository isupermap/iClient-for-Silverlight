﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_RouteCalculateMeasureResult_Title}</para>
    /// </summary>
    public class RouteCalculateMeasureResult
    {
        /// <summary>${iServerJava6R_RouteCalculateMeasureResult_constructor_D}</summary>
        public RouteCalculateMeasureResult()
        { 
        
        }
        /// <summary>
        /// ${iServerJava6R_RouteCalculateMeasureResult_attribute_Succeed_D}
        /// </summary>
        public Boolean Succeed
        {
            get;
            set;
        }
        /// <summary>
        /// ${iServerJava6R_RouteCalculateMeasureResult_attribute_Measure_D}
        /// </summary>
        public double Measure
        {
            get;
            set;
        }
        /// <summary>
        /// ${iServerJava6R_RouteCalculateMeasureResult_attribute_Message_D}
        /// </summary>
        public string Message
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_RouteCalculateMeasureResult_method_fromJson_D}</summary>
        internal static RouteCalculateMeasureResult FromJson(JsonObject json)
        {
            if(json==null)
            {
                return null;
            }
            RouteCalculateMeasureResult result = new RouteCalculateMeasureResult();
            result.Succeed = (bool)json["succeed"];
            result.Measure = (double)json["measure"];
            result.Message = (string)json["message"];
            return result;
        }
    }
}
