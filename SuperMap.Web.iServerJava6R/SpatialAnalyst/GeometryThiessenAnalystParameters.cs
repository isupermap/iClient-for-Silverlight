﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Core;
using System.Text;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_GeometryThiessenAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_GeometryThiessenAnalystParameters_Description}</para>
    /// </summary>
    public class GeometryThiessenAnalystParameters:ThiessenAnalystParameters
    {
        /// <summary>${iServerJava6R_GeometryThiessenAnalystParameters_constructor_D}</summary>
        public GeometryThiessenAnalystParameters() { }

        internal static System.Collections.Generic.Dictionary<string, string> ToDictionary(GeometryThiessenAnalystParameters geometryThiessenParams)
        {
            System.Collections.Generic.Dictionary<string, string> dict = new System.Collections.Generic.Dictionary<string, string>();
            if (geometryThiessenParams.ClipRegion != null)
            {
                dict.Add("clipRegion", ServerGeometry.ToJson(geometryThiessenParams.ClipRegion));
            }            
            dict.Add("createResultDataset", geometryThiessenParams.CreateResultDataset.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
            dict.Add("resultDatasetName", geometryThiessenParams.ResultDatasetName.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dict.Add("resultDatasourceName", geometryThiessenParams.ResultDatasourceName.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dict.Add("returnResultRegion", geometryThiessenParams.ReturnResultRegion.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());            
            if (geometryThiessenParams.Points.Count > 0)
            {
                string points_str = "[";
                int count = geometryThiessenParams.Points.Count;
                for (int i = 0; i < count; i++)
                {
                    Point2D point = geometryThiessenParams.Points[i];
                    var point_str = "{";
                    point_str+=String.Format("\"y\":{0},\"x\":{1}", point.Y, point.X);
                    point_str += "}";
                    points_str += point_str;
                    if (i == count - 1)
                    {
                        points_str += "]";                    }
                    else
                    {
                        points_str+=",";
                    }
                }
                dict.Add("points", points_str);
            }                       
            return dict;
        }

        /// <summary>${iServerJava6R_GeometryThiessenAnalystParameters_attribute_Points_D}</summary>
        public Point2DCollection Points 
        { 
            get; 
            set;
        }


    }


}
