﻿using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_GeometryOverlayAnalystArgs_Title}</para>
    /// 	<para>${iServerJava6R_GeometryOverlayAnalystArgs_Description}</para>
    /// </summary>
    public class GeometryOverlayAnalystArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_GeometryOverlayAnalystArgs_constructor_D}</summary>
        internal GeometryOverlayAnalystArgs(GeometryOverlayAnalystResult result, string originResult, object state)
            : base(state)
        {
            this.Result = result;
            this.OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_GeometryOverlayAnalystArgs_attribute_Result_D}</summary>
        public GeometryOverlayAnalystResult Result
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_GeometryOverlayAnalystArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
    }
}
