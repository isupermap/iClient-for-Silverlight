﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationDensityAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_InterpolationDensityAnalystParameters_Description}</para>
    /// </summary>
    public class InterpolationDensityAnalystParameters:InterpolationAnalystParameters
    {
        /// <summary>${iServerJava6R_InterpolationDensityAnalystParameters_constructor_D}</summary>
        public InterpolationDensityAnalystParameters()
            : base()
        {

        }
    }
}
