﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    public enum SearchMode
    {
        /// <summary>
        /// 使用 KDTREE 的固定点数方式查找参与内插分析的点。
        /// </summary>
        KDTREE_FIXED_COUNT,
        /// <summary>
        /// 使用 KDTREE 的定长方式查找参与内插分析的点。
        /// </summary>
        KDTREE_FIXED_RADIUS,
        /// <summary>
        /// 不进行查找，使用所有的输入点进行内插分析。
        /// </summary>
        NONE,
        /// <summary>
        /// 使用 QUADTREE 方式查找参与内插分析的点，仅对样条（Radial Basis Function）插值和普通克吕金（Kriging）有用。
        /// </summary>
        QUADTREE
    }
}
