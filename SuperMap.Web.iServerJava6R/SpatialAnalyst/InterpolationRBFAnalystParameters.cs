﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationRBFAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_InterpolationRBFAnalystParameters_Description}</para>
    /// </summary>
    public class InterpolationRBFAnalystParameters : InterpolationAnalystParameters
    {
        /// <summary>${iServerJava6R_InterpolationRBFAnalystParameters_constructor_D}</summary>
        public InterpolationRBFAnalystParameters()
            : base()
        {
            ExpectedCount = 12;
            Smooth = 0.1;
            Tension = 40;
            MaxPointCountForInterpolation = 200;
            MaxPointCountInNode = 50;
            ZValueScale = 1;
        }
        /// <summary>${iServerJava6R_InterpolationRBFAnalystParameters_attribute_ExpectedCount_D}</summary>
        public int ExpectedCount
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationRBFAnalystParameters_attribute_Smooth_D}</summary>
        public double Smooth
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationRBFAnalystParameters_attribute_Tension_D}</summary>
        public double Tension
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationRBFAnalystParameters_attribute_MaxPointCountForInterpolation_D}</summary>
        public int MaxPointCountForInterpolation
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationRBFAnalystParameters_attribute_MaxPointCountInNode_D}</summary>
        public int MaxPointCountInNode
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationRBFAnalystParameters_attribute_SearchMode_D}</summary>
        public SearchMode SearchMode
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationRBFAnalystParameters_attribute_ZValueScale_D}</summary>
        public double ZValueScale
        {
            get;
            set;
        }

        internal override System.Collections.Generic.Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dic = base.ToDictionary();
            dic.Add("expectedCount", this.ExpectedCount.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("maxPointCountForInterpolation", this.MaxPointCountForInterpolation.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("maxPointCountInNode", this.MaxPointCountInNode.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("smooth", this.Smooth.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("tension", this.Tension.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("searchMode", this.SearchMode.ToString());
            dic.Add("zValueScale", this.ZValueScale.ToString(System.Globalization.CultureInfo.InvariantCulture));
            return dic;
        }


    }
}
