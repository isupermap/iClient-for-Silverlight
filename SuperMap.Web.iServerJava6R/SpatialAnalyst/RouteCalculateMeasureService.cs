﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using System.Json;
using System.Windows.Browser;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_RouteCalculateMeasureService_Title}</para>
    /// </summary>
    public class RouteCalculateMeasureService:ServiceBase
    {
        /// <summary>${iServerJava6R_RouteCalculateMeasureService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_RouteCalculateMeasureService_constructor_overloads_D}</overloads>
        public RouteCalculateMeasureService()
        {
            
        }
        /// <summary>${iServerJava6R_RouteCalculateMeasureService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_RouteCalculateMeasureService_constructor_param_url}</param>
        public RouteCalculateMeasureService(string url)
            :base(url)
        { 
            
        }
        /// <summary>${iServerJava6R_RouteCalculateMeasureService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_RouteCalculateMeasureService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(RouteCalculateMeasureParameters param)
        {
            ProcessAsync(param,null);
        }
        /// <summary>${iServerJava6R_RouteCalculateMeasureService_method_ProcessAsync_D}</summary>
        /// <param name="param">${iServerJava6R_RouteCalculateMeasureService_method_ProcessAsync_param_param}</param>
        /// <param name="state">${iServerJava6R_RouteCalculateMeasureService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(RouteCalculateMeasureParameters param,object state)
        {
            GenerateAbsoluteUrl(param);
            base.SubmitRequest(this.Url,GetDictionaryParameters(param),new EventHandler<RequestEventArgs>(RouteCalculateMeasureService_Complated),state,true,false,false);
        }
        private void GenerateAbsoluteUrl(RouteCalculateMeasureParameters RouteMeasureParamters)
        { 
            if(RouteMeasureParamters==null)
            {
                throw new ArgumentNullException("请求参数为空");
            }
            if(this.Url.EndsWith("/"))
            {
                this.Url += "geometry/calculatemeasure.json?debug=true&returnContent=true";
            }
            else
            {
                this.Url += "/geometry/calculatemeasure.json?debug=true&returnContent=true";
            }
        }
        //http://localhost:8090/iserver/services/spatialanalyst-changchun/restjsr/spatialanalyst/geometry/calculatemeasure

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(RouteCalculateMeasureParameters parameters)
        {
            return RouteCalculateMeasureParameters.ToDictionary((RouteCalculateMeasureParameters)parameters);
        }

        private void RouteCalculateMeasureService_Complated(object sender,RequestEventArgs args)
        {
            JsonObject json = (JsonObject)JsonObject.Parse(args.Result);
            RouteCalculateMeasureResult result = RouteCalculateMeasureResult.FromJson(json);
            lastResult = result;
            RouteCalculateMeasureEventArgs e = new RouteCalculateMeasureEventArgs(result, args.Result, args.UserState);

            if(ProcessComplated!=null)
            {
                ProcessComplated (this,e);
            }
        }
        /// <summary>${iServerJava6R_RouteCalculateMeasureService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<RouteCalculateMeasureEventArgs> ProcessComplated;

        private RouteCalculateMeasureResult lastResult;
        /// <summary>${iServerJava6R_RouteCalculateMeasureService_lastResult_D}</summary>
        public RouteCalculateMeasureResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
