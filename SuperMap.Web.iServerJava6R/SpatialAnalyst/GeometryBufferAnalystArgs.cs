﻿using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_GeometryBufferAnalystArgs_Title}</para>
    /// 	<para>${iServerJava6R_GeometryBufferAnalystArgs_Description}</para>
    /// </summary>
    public class GeometryBufferAnalystArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_GeometryBufferAnalystArgs_constructor_D}</summary>
        public GeometryBufferAnalystArgs(GeometryBufferAnalystResult result, string originResult, object state)
            : base(state)
        {
            this.Result = result;
            this.OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_GeometryBufferAnalystArgs_attribute_Result_D}</summary>
        public GeometryBufferAnalystResult Result
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_GeometryBufferAnalystArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
    }
}
