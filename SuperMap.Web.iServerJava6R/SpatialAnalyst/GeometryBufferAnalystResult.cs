﻿
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_GeometryBufferAnalystResult_Title}</para>
    /// 	<para>${iServerJava6R_GeometryBufferAnalystResult_Description}</para>
    /// </summary>
    public class GeometryBufferAnalystResult : SpatialAnalystResult
    {
        /// <summary>${iServerJava6R_GeometryBufferAnalystResult_constructor_D}</summary>
        public GeometryBufferAnalystResult()
        { }

        //public ImageResult Image
        //{
        //    get;
        //    set;
        //}

        //public string Message
        //{
        //    get;
        //    set;
        //}
        /// <summary>${iServerJava6R_GeometryBufferAnalystResult_attribute_ResultGeometry_D}</summary>
        public Geometry ResultGeometry
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_GeometryBufferAnalystResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_GeometryBufferAnalystResult_method_fromJson_return}</returns>
        /// <param name="jsonResult">${iServerJava6R_GeometryBufferAnalystResult_method_fromJson_param_jsonObject}</param>
        public static GeometryBufferAnalystResult FromJson(System.Json.JsonObject jsonResult)
        {
            GeometryBufferAnalystResult result = new GeometryBufferAnalystResult();
            result.ResultGeometry = (ServerGeometry.FromJson((System.Json.JsonObject)jsonResult["resultGeometry"])).ToGeometry();
            //result.Image = ImageResult.FromJson((System.Json.JsonObject)jsonResult["image"]);
            return result;
        }
    }
}
