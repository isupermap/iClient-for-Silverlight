﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationIDWAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_InterpolationIDWAnalystParameters_Description}</para>
    /// </summary>
    public class InterpolationIDWAnalystParameters : InterpolationAnalystParameters
    {
        /// <summary>${iServerJava6R_InterpolationIDWAnalystParameters_constructor_D}</summary>
        public InterpolationIDWAnalystParameters()
            : base()
        {
            Power = 2;
            SearchMode = SearchMode.NONE;
            ExpectedCount = 12;
            ZValueScale = 1;
        }
        /// <summary>${iServerJava6R_InterpolationIDWAnalystParameters_attribute_Power_D}</summary>
        public int Power
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationIDWAnalystParameters_attribute_SearchMode_D}</summary>
        public SearchMode SearchMode
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationIDWAnalystParameters_attribute_ExpectedCount_D}</summary>
        public int ExpectedCount
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_InterpolationIDWAnalystParameters_attribute_ZValueScale_D}</summary>
        public double ZValueScale
        {
            get;
            set;
        }
        internal override System.Collections.Generic.Dictionary<string, string> ToDictionary()
        {

            Dictionary<string, string> dic = base.ToDictionary();

            dic.Add("power", this.Power.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("searchMode", this.SearchMode.ToString());
            dic.Add("expectedCount", this.ExpectedCount.ToString(System.Globalization.CultureInfo.InvariantCulture));
            dic.Add("zValueScale", this.ZValueScale.ToString(System.Globalization.CultureInfo.InvariantCulture));

            return dic;
        }
    }
}
