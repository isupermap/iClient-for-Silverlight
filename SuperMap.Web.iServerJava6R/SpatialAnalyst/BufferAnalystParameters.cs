﻿
namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_BufferAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_BufferAnalystParameters_Description}</para>
    /// 	<para><img src="BufferAnalyst.bmp"/></para>
    /// </summary>
    public class BufferAnalystParameters
    {
        /// <summary>${iServerJava6R_BufferAnalystParameters_constructor_D}</summary>
        public BufferAnalystParameters()
        { }
        /// <summary>${iServerJava6R_BufferAnalystParameters_attribute_bufferSetting_D}</summary>
        public BufferSetting BufferSetting
        {
            get;
            set;
        }
    }
}
