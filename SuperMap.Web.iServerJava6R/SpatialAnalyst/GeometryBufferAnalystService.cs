﻿using System;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_GeometryBufferAnalystService_Title}</para>
    /// 	<para>${iServerJava6R_GeometryBufferAnalystService_Description}</para>
    /// </summary>
    public class GeometryBufferAnalystService : ServiceBase
    {
        /// <summary>${iServerJava6R_GeometryBufferAnalystService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_GeometryBufferAnalystService_constructor_overloads_D}</overloads>
        public GeometryBufferAnalystService()
        { }
        /// <summary>${iServerJava6R_GeometryBufferAnalystService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_GeometryBufferAnalystService_constructor_param_url}</param>
        public GeometryBufferAnalystService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_GeometryBufferAnalystService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_GeometryBufferAnalystService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(GeometryBufferAnalystParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServerJava6R_GeometryBufferAnalystService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_GeometryBufferAnalystService_method_ProcessAsync_param_parameter}</param>
        /// <param name="state">${iServerJava6R_GeometryBufferAnalystService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GeometryBufferAnalystParameters parameters, object state)
        {
            GenerateAbsoluteUrl(parameters);
            base.SubmitRequest(this.Url, GetDictionaryParameters(parameters), new EventHandler<RequestEventArgs>(BufferAnalystService_Complated), state, true, false, false);
        }

        private void GenerateAbsoluteUrl(GeometryBufferAnalystParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("请求服务参数为空！");
            }

            //http://192.168.11.154:8090/iserver/services/spatialanalyst-sample/restjsr/spatialanalyst/geometry/buffer
            if (this.Url.EndsWith("/"))
            {
                this.Url += "geometry/buffer.json?debug=true&returnContent=true";
            }
            else
            {
                this.Url += "/geometry/buffer.json?debug=true&returnContent=true";
            }
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(GeometryBufferAnalystParameters parameters)
        {
            return GeometryBufferAnalystParameters.ToDictionary(parameters);
        }

        private void BufferAnalystService_Complated(object sender, RequestEventArgs args)
        {
            GeometryBufferAnalystArgs e = new GeometryBufferAnalystArgs(CheckResult(args), args.Result, args.UserState);
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }

        private GeometryBufferAnalystResult CheckResult(RequestEventArgs args)
        {
            JsonObject resultJson = (JsonObject)JsonObject.Parse(args.Result);
            GeometryBufferAnalystResult result = GeometryBufferAnalystResult.FromJson(resultJson);
            lastResult = result;
            return result;
        }
        /// <summary>${iServerJava6R_DatasetBufferAnalystService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GeometryBufferAnalystArgs> ProcessCompleted;

        private GeometryBufferAnalystResult lastResult;
        /// <summary>${iServerJava6R_GeometryBufferAnalystService_lastResult_D}</summary>
        public GeometryBufferAnalystResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
