﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using System.Collections.Generic;
using System.Globalization;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_RouteLocatorParameters_Title}</para>
    /// </summary>
    public class RouteLocatorParameters
    {
        /// <summary>${iServerJava6R_RouteLocatorParameters_constructor_D}</summary>
        public RouteLocatorParameters()
        {
            IsIgnoreGap = false;
            Measure = 0;

        }
        /// <summary>${iServerJava6R_RouteLocatorParameters_attribute_SourceRoute_D}</summary>
        public Route SourceRoute
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteLocatorParameters_attribute_Type_D}</summary>
        public LocateType Type
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteLocatorParameters_attribute_StartMeasure_D}</summary>
        public double StartMeasure
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteLocatorParameters_attribute_EndMeasure_D}</summary>
        public double EndMeasure
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteLocatorParameters_attribute_Measure_D}</summary>
        public double Measure
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteLocatorParameters_attribute_Offset_D}</summary>
        public double Offset
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_RouteLocatorParameters_attribute_IsIgnoreGap_D}</summary>
        public Boolean IsIgnoreGap
        {
            get;
            set;
        }
        internal static Dictionary<string, string> ToDictionary(RouteLocatorParameters param)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("sourceRoute",ServerGeometry.ToJson(param.SourceRoute.ToServerGeometry()));
            dic.Add("type", param.Type.ToString());
            if (param.Type == LocateType.POINT)
            {
                dic.Add("measure", param.Measure.ToString(CultureInfo.InvariantCulture));
                
            }
            if(param.Type==LocateType.POINT)
            {
                dic.Add("offset", param.Offset.ToString(CultureInfo.InvariantCulture));
                dic.Add("isIgnoreGap", param.IsIgnoreGap.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
            }
            
           
            if(param.Type==LocateType.LINE||param.Type==LocateType.LINEM)
            {
                dic.Add("startMeasure", param.StartMeasure.ToString(CultureInfo.InvariantCulture));
                dic.Add("endMeasure", param.EndMeasure.ToString(CultureInfo.InvariantCulture));
            }

            return dic;
        }
     

    }
}
