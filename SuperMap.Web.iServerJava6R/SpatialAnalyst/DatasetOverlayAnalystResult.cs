﻿using System.Json;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_DatasetsOverlayAnalystResult_Title}</para>
    /// 	<para>${iServerJava6R_DatasetsOverlayAnalystResult_Description}</para>
    /// </summary>
    public class DatasetOverlayAnalystResult
    {
        /// <summary>${iServerJava6R_DatasetsOverlayAnalystResult_constructor_D}</summary>
        public DatasetOverlayAnalystResult()
        {
        }

        //public string DatasetName
        //{
        //    get;
        //    set;
        //}
        /// <summary>${iServerJava6R_DatasetsOverlayAnalystResult_attribute_recordset_D}</summary>
        public Recordset Recordset
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_DatasetsOverlayAnalystResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_DatasetsOverlayAnalystResult_method_fromJson_return}</returns>
        /// <param name="jsonResult">${iServerJava6R_DatasetsOverlayAnalystResult_method_fromJson_param_jsonObject}</param>
        public static DatasetOverlayAnalystResult FromJson(JsonObject jsonResult)
        {
            DatasetOverlayAnalystResult result = new DatasetOverlayAnalystResult();
            result.Recordset = Recordset.FromJson(jsonResult);
            return result;
        }
    }
}
