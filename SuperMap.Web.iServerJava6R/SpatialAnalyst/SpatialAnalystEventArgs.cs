﻿using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_SpatialAnalystEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_SpatialAnalystEventArgs_Description}</para>
    /// </summary>
    public class SpatialAnalystEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_SpatialAnalystEventArgs_constructor_D}</summary>
        internal SpatialAnalystEventArgs(SpatialAnalystResult result, string originResult, object state)
            : base(state)
        {
            this.Result = result;
            this.OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_SpatialAnalystEventArgs_attribute_Result_D}</summary>
        public SpatialAnalystResult Result
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_SpatialAnalystEventArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
    }
}
