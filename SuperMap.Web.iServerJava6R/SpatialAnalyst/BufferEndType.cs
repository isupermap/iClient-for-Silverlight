﻿
namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_BufferEndType_Title}</para>
    /// 	<para>${iServerJava6R_BufferEndType_Description}</para>
    /// </summary>
    public enum BufferEndType
    {
        /// <summary>
        ///     <para>${iServerJava6R_BufferEndType_attribute_FLAT_D}</para>
        ///     <para><img src="Flat.png"/></para>
        /// </summary>
        FLAT,//平头缓冲
        /// <summary>
        ///     <para>${iServerJava6R_BufferEndType_attribute_ROUND_D}</para>
        ///     <para><img src="roundBuffer.bmp"/></para>
        /// </summary>
        ROUND//圆头缓冲
    }
}
