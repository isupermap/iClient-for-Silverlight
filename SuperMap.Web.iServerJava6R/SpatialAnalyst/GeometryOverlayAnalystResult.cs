﻿using System.Json;
using SuperMap.Web.Core;
using System.Globalization;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_GeometryOverlayAnalystResult_Title}</para>
    /// 	<para>${iServerJava6R_GeometryOverlayAnalystResult_Description}</para>
    /// </summary>
    public class GeometryOverlayAnalystResult : SpatialAnalystResult
    {
        /// <summary>${iServerJava6R_GeometryOverlayAnalystResult_constructor_D}</summary>
        public GeometryOverlayAnalystResult()
        { }
        /// <summary>${iServerJava6R_GeometryOverlayAnalystResult_attribute_ResultGeometry_D}</summary>
        public Geometry ResultGeometry
        {
            get;
            private set;
        }

        /// <summary>${iServerJava6R_GeometryOverlayAnalystResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_GeometryOverlayAnalystResult_method_fromJson_return}</returns>
        /// <param name="jsonResult">${iServerJava6R_GeometryOverlayAnalystResult_method_fromJson_param_jsonObject }</param>
        public static GeometryOverlayAnalystResult FromJson(JsonObject jsonResult)
        {
            GeometryOverlayAnalystResult result = new GeometryOverlayAnalystResult();
            result.ResultGeometry = (ServerGeometry.FromJson(jsonResult)).ToGeometry();

            return result;
        }
    }
}
