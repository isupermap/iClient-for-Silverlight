﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolateAnalystArgs_Title}</para>
    /// 	<para>${iServerJava6R_InterpolateAnalystArgs_Description}</para>
    /// </summary>
    public class InterpolateAnalystArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_InterpolateAnalystArgs_constructor_D}</summary>
        internal InterpolateAnalystArgs(InterpolationAnalysResult result, string originResult, object token)
            : base(token)
        {
            this.Result = result;
            this.OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_InterpolateAnalystArgs_attribute_OriginResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_InterpolateAnalystArgs_attribute_Result_D}</summary>
        public InterpolationAnalysResult Result
        {
            get;
            private set;
        }
    }
}
