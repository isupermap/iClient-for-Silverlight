﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_InterpolationAnalystType_Title}</para>
    /// 	<para>${iServerJava6R_InterpolationAnalystType_Description}</para>
    /// </summary>
    public enum InterpolationAnalystType
    {
        /// <summary>
        ///     <para>${iServerJava6R_BufferEndType_attribute_DATASET_D}</para>
        /// </summary>
        DATASET,

        /// <summary>
        ///     <para>${iServerJava6R_BufferEndType_attribute_GEOMETRY_D}</para>
        /// </summary>
        GEOMETRY
    }
}
