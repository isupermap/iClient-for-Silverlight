﻿using System;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.iServerJava6R.Resources;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_DatasetBufferAnalystService_Title}</para>
    /// 	<para>${iServerJava6R_DatasetBufferAnalystService_Description}</para>
    /// </summary>
    public class DatasetBufferAnalystService : ServiceBase
    {
        /// <summary>${iServerJava6R_DatasetBufferAnalystService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_DatasetBufferAnalystService_constructor_overloads_D}</overloads>
        public DatasetBufferAnalystService()
        { }
        /// <summary>${iServerJava6R_DatasetBufferAnalystService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_DatasetBufferAnalystService_constructor_param_url}</param>
        public DatasetBufferAnalystService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_DatasetBufferAnalystService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_DatasetBufferAnalystService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(DatasetBufferAnalystParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServerJava6R_DatasetBufferAnalystService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_DatasetBufferAnalystService_method_ProcessAsync_param_parameter}</param>
        /// <param name="state">${iServerJava6R_DatasetBufferAnalystService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(DatasetBufferAnalystParameters parameters, object state)
        {
            GenerateAbsoluteUrl(parameters);
            base.SubmitRequest(this.Url, GetDictionaryParameters(parameters), new EventHandler<RequestEventArgs>(BufferAnalystService_Complated), state, true, false, false);
        }

        private void GenerateAbsoluteUrl(DatasetBufferAnalystParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("请求服务参数为空！");
            }

            if (string.IsNullOrEmpty(parameters.Dataset) || string.IsNullOrEmpty(parameters.Dataset))
            {
                throw new ArgumentNullException("数据集参数为空");
            }

            if (this.Url == null)
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            //http://192.168.11.154:8090/iserver/services/spatialanalyst-sample/restjsr/spatialanalyst/datasets/SamplesP@Interpolation/buffer.json
            if (this.Url.EndsWith("/"))
            {
                this.Url += "datasets/" + parameters.Dataset + "/buffer.json?debug=true&returnContent=true";
            }
            else
            {
                this.Url += "/datasets/" + parameters.Dataset + "/buffer.json?debug=true&returnContent=true";
            }
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(DatasetBufferAnalystParameters parameters)
        {
            return DatasetBufferAnalystParameters.ToDictionary(parameters);
        }

        private void BufferAnalystService_Complated(object sender, RequestEventArgs args)
        {
            DatasetBufferAnalystArgs e = new DatasetBufferAnalystArgs(CheckResult(args), args.Result, args.UserState);
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }

        private DatasetBufferAnalystResult CheckResult(RequestEventArgs args)
        {
            JsonObject resultJson = (JsonObject)JsonObject.Parse(args.Result);

            DatasetBufferAnalystResult result = DatasetBufferAnalystResult.FromJson((JsonObject)resultJson["recordset"]);
            lastResult = result;
            return result;
        }
        /// <summary>${iServerJava6R_DatasetBufferAnalystService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<DatasetBufferAnalystArgs> ProcessCompleted;

        private DatasetBufferAnalystResult lastResult;
        /// <summary>${iServerJava6R_DatasetBufferAnalystService_lastResult_D}</summary>
        public DatasetBufferAnalystResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
