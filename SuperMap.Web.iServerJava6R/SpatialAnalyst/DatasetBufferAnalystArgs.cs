﻿using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_DatasetBufferAnalystArgs_Title}</para>
    /// 	<para>${iServerJava6R_DatasetBufferAnalystArgs_Description}</para>
    /// </summary>
    public class DatasetBufferAnalystArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_DatasetBufferAnalystArgs_constructor_D}</summary>
        public DatasetBufferAnalystArgs(DatasetBufferAnalystResult result, string originResult, object state)
            : base(state)
        {
            this.OriginResult = originResult;
            this.Result = result;
        }
        /// <summary>${iServerJava6R_DatasetBufferAnalystArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_DatasetBufferAnalystArgs_attribute_Result_D}</summary>
        public DatasetBufferAnalystResult Result
        {
            get;
            private set;
        }
    }
}
