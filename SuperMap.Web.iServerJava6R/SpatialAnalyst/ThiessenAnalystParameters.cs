﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThiessenAnalystParameters_Title}</para>
    /// 	<para>${iServerJava6R_ThiessenAnalystParameters_Description}</para>
    /// </summary>
    public class ThiessenAnalystParameters
    {
        /// <summary>${iServerJava6R_ThiessenAnalystParameters_constructor_D}</summary>
        public ThiessenAnalystParameters()
        { }

        /// <summary>${iServerJava6R_ThiessenAnalystParameters_attribute_ClipRegion_D}</summary>
        public ServerGeometry ClipRegion
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThiessenAnalystParameters_attribute_CreateResultDataset_D}</summary>
        public bool CreateResultDataset
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThiessenAnalystParameters_attribute_ResultDatasetName_D}</summary>
        public String ResultDatasetName
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThiessenAnalystParameters_attribute_ResultDatasourceName_D}</summary>
        public String ResultDatasourceName
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThiessenAnalystParameters_attribute_ReturnResultRegion_D}</summary>
        public bool ReturnResultRegion
        {
            get;
            set;
        }        
    }
}
