﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R.SpatialAnalyst
{ 
    /// <summary>
    /// 	<para>${iServerJava6R_GenerateSpatialDataParameters_Title}</para>
    /// </summary>
    public class GenerateSpatialDataParameters
    {
	    /// <summary>${iServerJava6R_GenerateSpatialDataParameters_constructor_D}</summary>
        public GenerateSpatialDataParameters()
        {

        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_ErrorInfoField_D}</summary>
        /// </summary>
        public string ErrorInfoField
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_EventRouteIDField_D}
        /// </summary>
        public string EventRouteIDField
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_EventTable_D}
        /// </summary>
        public string EventTable
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_MeasureEndField_D}
        /// </summary>
        public string MeasureEndField
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_MeasureField_D}
        /// </summary>
        public string MeasureField
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_MeasureOffsetField_D}
        /// </summary>
        public string MeasureOffsetField
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_MeasureStartField_D}
        /// </summary>
        public string MeasureStartField
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_RouteDataset_D}
        /// </summary>
        public string RouteDataset
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_RouteIDField_D}
        /// </summary>
        public string RouteIDField
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_GenerateSpatialDataParameters_attribute_DataReturnOption_D}
        /// </summary>
        public DataReturnOption DataReturnOption
        {
            get;
            set;

        }

        internal static Dictionary<string, string> ToDictionary(GenerateSpatialDataParameters param)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("dataReturnOption", DataReturnOption.ToJson(param.DataReturnOption));
            dic.Add("errorInfoField", param.ErrorInfoField);
            dic.Add("eventRouteIDField", param.EventRouteIDField);
            dic.Add("eventTable", param.EventTable);
            dic.Add("measureEndField", param.MeasureEndField);
            dic.Add("measureField", param.MeasureField);
            dic.Add("measureOffsetField", param.MeasureOffsetField);
            dic.Add("measureStartField", param.MeasureStartField);
            dic.Add("routeIDField", param.RouteIDField);

            return dic;
        }
    }
}
