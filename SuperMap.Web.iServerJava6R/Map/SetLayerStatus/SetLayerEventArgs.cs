﻿using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServer6_SetLayerStatusEventArgs_Title}</para>
    /// 	<para>${iServer6_SetLayerStatusEventArgs_Description}</para>
    /// </summary>
    public class SetLayerEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer6_SetLayerStatusEventArgs_constructor_None_D}</summary>
        public SetLayerEventArgs(SetLayerResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServer6_SetLayerStatusEventArgs_attribute_result}</summary>
        public SetLayerResult Result { get; private set; }
        /// <summary>${iServer6_SetLayerStatusEventArgs_attribute_originResult}</summary>
        public string OriginResult { get; private set; }
    }
}
