﻿using System.Json;
using System.Collections.Generic;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using SuperMap.Web.iServerJava6R.Resources;
using System.Windows.Browser;
using System.Text;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServer6_SetLayerStatusService_Title}</para>
    /// 	<para>${iServer6_SetLayerStatusService_Description}</para>
    /// </summary>
    public class SetLayerStatusService : ServiceBase
    {
        private string mapUrl = string.Empty;
        private string mapName = string.Empty;
        private SetLayerStatusParameters requestParameters;
        private JsonObject tempLayerInfoJson;
        private string resourceID;

        /// <summary>${iServer6_SetLayerStatusService_constructor_None_D}</summary>
        /// <overloads>${iServer6_SetLayerStatusService_constructor_overloads}</overloads>
        public SetLayerStatusService()
        {
        }
        /// <summary>${iServer6_SetLayerStatusService_constructor_String_D}</summary>
        /// <param name="url">${iServer6_SetLayerStatusService_constructor_String_param_url}</param>
        public SetLayerStatusService(string url)
            : base(url)
        {
        }
        /// <summary>${iServer6_SetLayerStatusService_method_processAsync_D}</summary>
        /// <overloads>${iServer6_SetLayerStatusService_method_processAsync_overloads}</overloads>
        public void ProcessAsync(SetLayerStatusParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer6_SetLayerStatusService_method_processAsync_D}</summary>
        /// <param name="parameters">${iServer6_SetLayerStatusService_method_processAsync_param_parameters}</param>
        /// <param name="state">${iServer6_SetLayerStatusService_method_processAsync_param_state}</param>
        public void ProcessAsync(SetLayerStatusParameters parameters, object state)
        {
            mapUrl = base.Url;

            requestParameters = parameters;
            ValidationUrlAndParametres();
            resourceID = requestParameters.ResourceID;
            GetMapName();

            if (!string.IsNullOrEmpty(resourceID))//直接put
            {
                base.SubmitRequest(GetPutRequestUrl(), GetDictionaryParameters(), new EventHandler<RequestEventArgs>(SetLayerStatusRequest_Complated), state, true, false, true);
            }
            else   //先post再put
            {
                string getTempLayerSetUrl = Url;
                getTempLayerSetUrl += "/tempLayersSet.json";
                WebClient temp = new WebClient();
                temp.Encoding = Encoding.UTF8;
                temp.Headers["Content-Type"] = "application/x-www-form-urlencoded;charset=UTF-8";
                temp.UploadStringCompleted += new UploadStringCompletedEventHandler(PostRequest_Complated);
                temp.UploadStringAsync(new Uri(getTempLayerSetUrl), "POST", "", state);
            }
        }

        private void ValidationUrlAndParametres()
        {
            if (this.Url == null)
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            if (requestParameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
        }

        private void GetMapName()
        {
            if (mapUrl.EndsWith("/"))
            {
                this.mapUrl = mapUrl.TrimEnd(new char[] { '/' });
            }

            int index = this.mapUrl.LastIndexOf('/');
            mapName = this.mapUrl.Substring(index + 1);
        }

        private string GetPostRequestUrl()
        {
            string firstUrl = this.mapUrl + "/tempLayersSet.json?debug=true&holdTime=" + requestParameters.HoldTime.ToString(System.Globalization.CultureInfo.InvariantCulture);
            return firstUrl;
        }

        private void PostRequest_Complated(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error != null || e.Result == null && e.Cancelled == true)
            {
                return;
            }

            if (e.Result.Contains("succeed") && e.Result.Contains("newResourceID"))
            {
                tempLayerInfoJson = (JsonObject)JsonObject.Parse(e.Result);
                if (tempLayerInfoJson["succeed"])
                {
                    resourceID = tempLayerInfoJson["newResourceID"];

                    //执行put
                    base.SubmitRequest(GetPutRequestUrl(), GetDictionaryParameters(), new EventHandler<RequestEventArgs>(SetLayerStatusRequest_Complated), e.UserState, true, false, true);
                }
            }
        }

        private string GetPutRequestUrl()
        {
            //http://192.168.11.154:8090/iserver/services/map-world/rest/maps/World Map/tempLayersSet/7/World Map
            string secondUrl = this.mapUrl + "/tempLayersSet/" + resourceID + ".json?_method=PUT&reference=" + resourceID + "&elementRemain=true";
            return secondUrl;
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("type", "\"UGC\"");

            if (requestParameters.LayerStatusList != null && requestParameters.LayerStatusList.Count > 0)
            {
                dictionary.Add("subLayers", SetLayerStatusParameters.ToJson(requestParameters));
            }
            dictionary.Add("queryable", "false");
            dictionary.Add("name", "\"" + mapName + "\"");

            return dictionary;
        }

        private void SetLayerStatusRequest_Complated(object sender, RequestEventArgs e)
        {
            if (e.Result != null)
            {
                SetLayerResult result = SetLayerResult.FromJson(e.Result);
                result.NewResourceID = resourceID.ToString();
                lastResult = result;
                SetLayerEventArgs args = new SetLayerEventArgs(result, e.Result, e.UserState);
                OnProcessCompleted(args);
            }
        }
        /// <summary>${iServer6_SetLayerStatusService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<SetLayerEventArgs> ProcessCompleted;

        private void OnProcessCompleted(SetLayerEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, args);
            }
        }

        private SetLayerResult lastResult;
        /// <summary>${iServer6_SetLayerStatusService_attribute_lastResult_D}</summary>
        public SetLayerResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}

