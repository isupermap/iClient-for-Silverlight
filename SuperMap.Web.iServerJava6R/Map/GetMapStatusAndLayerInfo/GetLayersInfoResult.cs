﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Core;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetLayersInfoResult_Title}</para>
    /// 	<para>${iServerJava6R_GetLayersInfoResult_Description}</para>
    /// </summary>
    public class GetLayersInfoResult
    {
        internal GetLayersInfoResult()
        { }
        /// <summary>${iServerJava6R_GetLayersInfoResult_attribute_LayersInfo_D}</summary>
        public IList<ServerLayer> LayersInfo { get; internal set; }

        /// <summary>${iServerJava6R_GetLayersInfoResult_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_GetLayersInfoResult_method_FromJson_return}</returns>
        /// <param name="strResult">${iServerJava6R_GetLayersInfoResult_method_FromJson_param_jsonObject}</param>
        public static GetLayersInfoResult FromJson(string strResult)
        {
            GetLayersInfoResult result = new GetLayersInfoResult();

            var json = (System.Json.JsonValue)System.Json.JsonValue.Parse(strResult);
            if (json == null)
            {
                return null;
            }

            List<ServerLayer> layers = new List<ServerLayer>();
            foreach (JsonObject layerJson in json)
            {
                if (layerJson.ContainsKey("subLayers"))
                {
                    foreach (JsonObject item in (JsonArray)layerJson["subLayers"]["layers"])
                    {
                        layers.Add(ServerLayer.FromJson(item));
                    }
                }
            }
            result.LayersInfo = layers;
            return result;
        }
    }
}
