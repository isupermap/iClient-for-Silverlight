﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using System.Windows.Browser;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// ${iServerJava6R_ChartFeatureInfoSpecsService_Title}
    /// </summary>
    public class ChartFeatureInfoSpecsService : ServiceBase
    {
	    /// <summary>${iServerJava6R_ChartFeatureInfoSpecsService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_ChartFeatureInfoSpecsService_constructor_overloads_D}</overloads>
        public ChartFeatureInfoSpecsService()
        { }
		/// <summary>${iServerJava6R_ChartFeatureInfoSpecsService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_ChartFeatureInfoSpecsService_constructor_String_param_url}</param>
        public ChartFeatureInfoSpecsService(string url)
            : base(url)
        { }
		/// <summary>${iServerJava6R_ChartFeatureInfoSpecsService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_ChartFeatureInfoSpecsService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync()
        {
            ProcessAsync(null);
        }
		/// <summary>${iServerJava6R_ChartFeatureInfoSpecsService_method_ProcessAsync_D}</summary>
        /// <param name="state">${iServerJava6R_ChartFeatureInfoSpecsService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(object state)
        {
            CheckUrl();

            base.SubmitRequest(Url, null, new EventHandler<RequestEventArgs>(Request_Completed), state, false, false, false);
        }

        private void CheckUrl()
        {
            if (Url.EndsWith("/"))
            {
                Url.TrimEnd(new char[] { '/' });
            }

            this.Url = this.Url + "/chartFeatureInfoSpecs.json";
        }

        private void Request_Completed(object sender, RequestEventArgs e)
        {
            //不做e.Error的判断
            if (e.Result != null && !string.IsNullOrEmpty(e.Result))
            {
                JsonArray array = (JsonArray)JsonArray.Parse(e.Result);
                ChartFeatureInfoSpecsResult result = ChartFeatureInfoSpecsResult.FromJson(array);
                lastResult = result;
                ChartFeatureInfoSpecsEventArgs args = new ChartFeatureInfoSpecsEventArgs(result, e.Result, e.UserState);
                OnProcessCompleted(args);
            }
        }
		/// <summary>${iServerJava6R_ChartFeatureInfoSpecsService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ChartFeatureInfoSpecsEventArgs> ProcessCompleted;

        private void OnProcessCompleted(ChartFeatureInfoSpecsEventArgs e)
        {
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }

        private ChartFeatureInfoSpecsResult lastResult;
        /// <summary>${iServerJava6R_ChartFeatureInfoSpecsService_attribute_lastResult_D}</summary>
        public ChartFeatureInfoSpecsResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
