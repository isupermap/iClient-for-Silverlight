﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// ${iServerJava6R_ChartAttributeSpec_Title}
    /// </summary>
    public class ChartAttributeSpec
    {
		///<summary>
		/// ${iServerJava6R_ChartAttributeSpec_constructor_D}
        /// </summary>
        public ChartAttributeSpec()
        {

        }

        /// <summary>
        /// ${iServerJava6R_ChartAttributeSpec_attribute_Code_D}
        /// </summary>
        public int Code
        {
            get;
            set;
        }

        /// <summary>
        ///  ${iServerJava6R_ChartAttributeSpec_attribute_Required_D}
        /// </summary>
        public int Required
        {
            get;
            set;
        }

        internal static ChartAttributeSpec FromJson(JsonObject json)
        {
            if (json == null || !json.ContainsKey("code"))
            {
                return null;
            }
            ChartAttributeSpec attrib = new ChartAttributeSpec();

            attrib.Code = (int)json["code"];
            attrib.Required = (int)json["required"];

            return attrib;
        }
    }
}
