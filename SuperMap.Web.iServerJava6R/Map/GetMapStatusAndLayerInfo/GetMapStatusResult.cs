﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Core;
using System.Json;
using SuperMap.Web.Utilities;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetMapStatusResult_Title}</para>
    /// 	<para>${iServerJava6R_GetMapStatusResult_Description}</para>
    /// </summary>
    public class GetMapStatusResult
    {
        /// <summary>${iServerJava6R_GetMapStatusResult_constructor_D}</summary>
        internal GetMapStatusResult()
        {
            this.VisibleScales = new List<double>();
        }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_Name_D}</summary>
        public String Name { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_Center_D}</summary>
        public Point2D Center { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_Scale_D}</summary>
        public double Scale { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_MaxScale_D}</summary>
        public double MaxScale { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_MinScale_D}</summary>
        public double MinScale { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_Bounds_D}</summary>
        public Rectangle2D Bounds { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_ViewBounds_D}</summary>
        public Rectangle2D ViewBounds { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_Viewer_D}</summary>
        public Rect Viewer { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_PrjCoordSys_D}</summary>
        public PrjCoordSys PrjCoordSys { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_IsCacheEnabled_D}</summary>
        public bool IsCacheEnabled { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_CustomParams_D}</summary>
        public String CustomParams { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_UserToken_D}</summary>
        public UserInfo UserToken { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_ClipRegion_D}</summary>
        public ServerGeometry ClipRegion { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_IsClipRegionEnabled_D}</summary>
        public bool IsClipRegionEnabled { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_CustomEntireBounds_D}</summary>
        public Rectangle2D CustomEntireBounds { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_IsCustomEntireBoundsEnabled_D}</summary>
        public bool IsCustomEntireBoundsEnabled { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_Angle_D}</summary>
        public double Angle { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_Antialias_D}</summary>
        public bool Antialias { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_BackgroundStyle_D}</summary>
        public ServerStyle BackgroundStyle { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_ColorMode_D}</summary>
        public MapColorMode ColorMode { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_CoordUnit_D}</summary>
        public Unit CoordUnit { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_DistanceUnit_D}</summary>
        public Unit DistanceUnit { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_Description_D}</summary>
        public String Description { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_IsDynamicProjection_D}</summary>
        public bool IsDynamicProjection { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_IsMarkerAngleFixed_D}</summary>
        public bool IsMarkerAngleFixed { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_MaxVisibleTextSize_D}</summary>
        public double MaxVisibleTextSize { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_MaxVisibleVertex_D}</summary>
        public int MaxVisibleVertex { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_MinVisibleTextSize_D}</summary>
        public double MinVisibleTextSize { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_IsOverlapDisplayed_D}</summary>
        public bool IsOverlapDisplayed { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_IsPaintBackground_D}</summary>
        public bool IsPaintBackground { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_IsTextAngleFixed_D}</summary>
        public bool IsTextAngleFixed { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_IsTextOrientationFixed_D}</summary>
        public bool IsTextOrientationFixed { get; set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_VisibleScales_D}</summary>
        public List<double> VisibleScales { get; private set; }
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_VisibleScalesEnabled_D}</summary>
        public bool VisibleScalesEnabled { get; private set; }

        internal static GetMapStatusResult FromJson(JsonObject json)
        {
            if (json == null) return null;
            GetMapStatusResult result = new GetMapStatusResult();
            result.Angle = (double)json["angle"];
            result.Antialias = (bool)json["antialias"];
            result.BackgroundStyle = ServerStyle.FromJson((JsonObject)json["backgroundStyle"]);
            result.Bounds = JsonHelper.ToRectangle2D((JsonObject)json["bounds"]);
            result.IsCacheEnabled = (bool)json["cacheEnabled"];
            result.Center = JsonHelper.ToPoint2D((JsonObject)json["center"]);
            result.ClipRegion = ServerGeometry.FromJson((JsonObject)json["clipRegion"]);
            result.IsClipRegionEnabled = (bool)json["clipRegionEnabled"];
            if (json["colorMode"] != null)
            {
                result.ColorMode = (MapColorMode)Enum.Parse(typeof(MapColorMode), json["colorMode"], true);
            }
            if (json["coordUnit"] != null)
            {
                result.CoordUnit = (Unit)Enum.Parse(typeof(Unit), json["coordUnit"], true);
            }
            result.CustomEntireBounds = JsonHelper.ToRectangle2D((JsonObject)json["customEntireBounds"]);
            result.IsCustomEntireBoundsEnabled = (bool)json["customEntireBoundsEnabled"];
            result.CustomParams = (string)json["customParams"];
            result.Description = (string)json["description"];
            if (json["distanceUnit"] != null)
            {
                result.DistanceUnit = (Unit)Enum.Parse(typeof(Unit), json["distanceUnit"], true);
            }
            result.IsDynamicProjection = (bool)json["dynamicProjection"];
            result.IsMarkerAngleFixed = (bool)json["markerAngleFixed"];
            result.MaxScale = (double)json["maxScale"];
            result.MaxVisibleTextSize = (double)json["maxVisibleTextSize"];
            result.MaxVisibleVertex = (int)json["maxVisibleVertex"];
            result.MinScale = (double)json["minScale"];
            result.MinVisibleTextSize = (double)json["minVisibleTextSize"];
            result.Name = (string)json["name"];
            result.IsOverlapDisplayed = (bool)json["overlapDisplayed"];
            result.IsPaintBackground = (bool)json["paintBackground"];
            result.PrjCoordSys = PrjCoordSys.FromJson((JsonObject)json["prjCoordSys"]);
            result.Scale = (double)json["scale"];
            result.IsTextAngleFixed = (bool)json["textAngleFixed"];
            result.IsTextOrientationFixed = (bool)json["textOrientationFixed"];
            result.UserToken = UserInfo.FromJson((JsonObject)json["userToken"]);
            result.ViewBounds = JsonHelper.ToRectangle2D((JsonObject)json["viewBounds"]);
            result.Viewer = JsonHelper.ToRect((JsonObject)json["viewer"]);

            result.VisibleScalesEnabled = (json["visibleScalesEnabled"] != null ? (bool)json["visibleScalesEnabled"] : false);
            if (result.VisibleScalesEnabled && json["visibleScales"] != null)
            {
                try
                {
                    foreach (var item in json["visibleScales"])
                    {
                        result.VisibleScales.Add(Convert.ToDouble(item.ToString()));
                    }
                }
                catch (Exception)
                {
                    result.VisibleScales.Clear();
                    result.VisibleScalesEnabled = false;
                }
            }
            return result;
        }
    }
}
