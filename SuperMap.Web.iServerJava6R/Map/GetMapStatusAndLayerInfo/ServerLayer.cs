﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperMap.Web.Core;
using System.Windows.Shapes;
using System.Json;
using SuperMap.Web.Utilities;
using System.Windows.Media;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServerLayer_Title}</para>
    /// 	<para>${iServerJava6R_ServerLayer_Description}</para>
    /// </summary>
    public class ServerLayer
    {
        /// <summary>${iServerJava6R_ServerLayer_constructor_D}</summary>
        public ServerLayer()
        { }

        //Property
        /// <summary>${iServerJava6R_ServerLayer_attribute_Bounds_D}</summary>
        public Rectangle2D Bounds
        {
            get;
            internal set;
        }

        //对应服务端的Layer属性
        /// <summary>${iServerJava6R_ServerLayer_attribute_caption_D}</summary>
        public string Caption { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_Description_D}</summary>
        public string Description { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_Name_D}</summary>
        public string Name { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_IsQueryable_D}</summary>
        public bool IsQueryable { get; internal set; }

        //图层的子图层先不控制
        //public System.Collections.Generic.List<LayerInfo> SubLayers { get; internal set; }

        //这里默认是UGC了，不开放给用户啦
        //private string LayerType = "UGC";
        /// <summary>${iServerJava6R_ServerLayer_attribute_IsVisible_D}</summary>
        public bool IsVisible { get; internal set; }

        //对应服务端UGCMapLayer属性
        /// <summary>${iServerJava6R_ServerLayer_attribute_IsCompleteLineSymbolDisplayed_D}</summary>
        public bool IsCompleteLineSymbolDisplayed { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_MaxScale_D}</summary>
        public double MaxScale { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_minScale_D}</summary>
        public double MinScale { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_MinVisibleGeometrySize_D}</summary>
        public double MinVisibleGeometrySize { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_OpaqueRate_D}</summary>
        public int OpaqueRate { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_IsSymbolScalable_D}</summary>
        public bool IsSymbolScalable { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_SymbolScale_D}</summary>
        public double SymbolScale { get; internal set; }

        //对应服务端UGCLayer
        /// <summary>${iServerJava6R_ServerLayer_attribute_DatasetInfo_D}</summary>
        public DatasetInfo DatasetInfo { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_DisplayFilter_D}</summary>
        public string DisplayFilter { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_JoinItems_D}</summary>
        public System.Collections.Generic.List<JoinItem> JoinItems { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_RepresentationField_D}</summary>
        public string RepresentationField { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_UGCLayerType_D}</summary>
        public SuperMapLayerType UGCLayerType { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_attribute_UGCLayer_D}</summary>
        public UGCLayer UGCLayer { get; internal set; }
        /// <summary>${iServerJava6R_ServerLayer_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_ServerLayer_method_FromJson_return}</returns>
        /// <param name="json">${iServerJava6R_ServerLayer_method_FromJson_param_jsonObject}</param>
        public static ServerLayer FromJson(JsonObject json)
        {
            var serverLayer = new ServerLayer();
            if (json["bounds"] != null)
            {
                serverLayer.Bounds = JsonHelper.ToRectangle2D((JsonObject)json["bounds"]);
            }
            else
            {
                //null
            }
            serverLayer.Caption = (string)json["caption"];
            serverLayer.Description = (string)json["description"];
            serverLayer.Name = (string)json["name"];
            serverLayer.IsQueryable = (bool)json["queryable"];
            serverLayer.IsVisible = (bool)json["visible"];
            serverLayer.IsCompleteLineSymbolDisplayed = (bool)json["completeLineSymbolDisplayed"];
            serverLayer.MaxScale = (double)json["maxScale"];
            serverLayer.MinScale = (double)json["minScale"];
            serverLayer.MinVisibleGeometrySize = (double)json["minVisibleGeometrySize"];
            serverLayer.OpaqueRate = (int)json["opaqueRate"];
            serverLayer.IsSymbolScalable = (bool)json["symbolScalable"];
            serverLayer.SymbolScale = (double)json["symbolScale"];
            serverLayer.DatasetInfo = DatasetInfo.FromJson((JsonObject)json["datasetInfo"]);
            serverLayer.DisplayFilter = (string)json["displayFilter"];

            if (json["joinItems"] != null)
            {
                List<JoinItem> joinItems = new List<JoinItem>();
                foreach (JsonObject item in (JsonArray)json["joinItems"])
                {
                    joinItems.Add(JoinItem.FromJson(item));
                }
                serverLayer.JoinItems = joinItems;
            }
            serverLayer.RepresentationField = (string)json["representationField"];

            if ((string)json["ugcLayerType"] == SuperMapLayerType.GRID.ToString())
            {
                UGCGridLayer ugcGridLayer = new UGCGridLayer();

                List<Color> colors = new List<Color>();
                foreach (JsonObject colorItem in (JsonArray)json["colors"])
                {
                    colors.Add(ServerColor.FromJson(colorItem).ToColor());
                }
                ugcGridLayer.Colors = colors;

                if (json["dashStyle"] != null)
                {
                    ugcGridLayer.DashStyle = ServerStyle.FromJson((JsonObject)json["dashStyle"]);
                }
                if (json["gridType"] != null)
                {
                    ugcGridLayer.GridType = (GridType)Enum.Parse(typeof(GridType), json["gridType"], true);
                }
                else
                {
                }

                ugcGridLayer.HorizontalSpacing = (double)json["horizontalSpacing"];
                ugcGridLayer.SizeFixed = (bool)json["sizeFixed"];

                if (json["solidStyle"] != null)
                {
                    ugcGridLayer.SolidStyle = ServerStyle.FromJson((JsonObject)json["solidStyle"]);
                }

                if (json["specialColor"] != null)
                {
                    ugcGridLayer.SpecialColor = ServerColor.FromJson((JsonObject)json["specialColor"]).ToColor();
                }
                ugcGridLayer.SpecialValue = (double)json["specialValue"];
                ugcGridLayer.VerticalSpacing = (double)json["verticalSpacing"];
                serverLayer.UGCLayer = ugcGridLayer;
            }

            else if ((string)json["ugcLayerType"] == SuperMapLayerType.IMAGE.ToString())
            {
                UGCImageLayer ugcImageLayer = new UGCImageLayer();
                ugcImageLayer.Brightness = (int)json["brightness"];
                if (json["colorSpaceType"] != null)
                {
                    ugcImageLayer.ColorSpaceType = (ColorSpaceType)Enum.Parse(typeof(ColorSpaceType), (string)json["colorSpaceType"], true);
                }
                else
                {
                }
                ugcImageLayer.Contrast = (int)json["contrast"];

                List<int> bandIndexes = new List<int>();
                if (json["displayBandIndexes"] != null && ((JsonArray)json["displayBandIndexes"]).Count > 0)
                {
                    foreach (int item in (JsonArray)json["displayBandIndexes"])
                    {
                        bandIndexes.Add(item);
                    }

                    ugcImageLayer.DisplayBandIndexes = bandIndexes;
                }

                ugcImageLayer.Transparent = (bool)json["transparent"];
                ugcImageLayer.TransparentColor = ServerColor.FromJson((JsonObject)json["transparentColor"]).ToColor();
                serverLayer.UGCLayer = ugcImageLayer;
            }

            else if ((string)json["ugcLayerType"] == SuperMapLayerType.THEME.ToString())
            {
                UGCThemeLayer ugcThemeLayer = new UGCThemeLayer();
                if (json["theme"] != null)
                {

                    if ((string)json["theme"]["type"] == "UNIQUE")
                    {
                        ugcThemeLayer.Theme = ThemeUnique.FromJson((JsonObject)json["theme"]);
                    }

                    else if ((string)json["theme"]["type"] == "RANGE")
                    {
                        ugcThemeLayer.Theme = ThemeRange.FromJson((JsonObject)json["theme"]);
                    }

                    else if ((string)json["theme"]["type"] == "LABEL")
                    {
                        ugcThemeLayer.Theme = ThemeLabel.FromJson((JsonObject)json["theme"]);
                    }

                    else if ((string)json["theme"]["type"] == "GRAPH")
                    {
                        ugcThemeLayer.Theme = ThemeGraph.FromJson((JsonObject)json["theme"]);
                    }

                    else if ((string)json["theme"]["type"] == "DOTDENSITY")
                    {
                        ugcThemeLayer.Theme = ThemeDotDensity.FromJson((JsonObject)json["theme"]);
                    }

                    else if ((string)json["theme"]["type"] == "GRADUATEDSYMBOL")
                    {
                        ugcThemeLayer.Theme = ThemeGraduatedSymbol.FromJson((JsonObject)json["theme"]);
                    }
                    else
                    {
                        //以后有需求再添加，现在就写到这里，共六个专题图。
                    }
                }
                if (json["theme"]["type"] != null)
                {
                    ugcThemeLayer.ThemeType = (ThemeType)Enum.Parse(typeof(ThemeType), (string)json["theme"]["type"], true);
                }
                serverLayer.UGCLayer = ugcThemeLayer;
                //ugcThemeLayer.Theme
            }

            else if ((string)json["ugcLayerType"] == SuperMapLayerType.VECTOR.ToString() && json.ContainsKey("style"))
            {
                serverLayer.UGCLayer = UGCVectorLayer.FromJson((JsonObject)json["style"]);
            }
            else
            {
                serverLayer.UGCLayer = new UGCLayer();
            }
            if (json["ugcLayerType"] != null)
            {
                serverLayer.UGCLayerType = (SuperMapLayerType)Enum.Parse(typeof(SuperMapLayerType), (string)json["ugcLayerType"], true);
            }
            else
            {
                //不做处理
            }

            //这里不判断WMS和WFS图层。
            //else if (json["ugcLayerType"] == SuperMapLayerType.WMS.ToString())
            //{

            //}
            //根据图层类型增加相应属性。
            return serverLayer;
        }
    }
}
