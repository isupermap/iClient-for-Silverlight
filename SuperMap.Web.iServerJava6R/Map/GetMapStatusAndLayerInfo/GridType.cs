﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_GridType_Title}</para>
    /// </summary>
    public enum GridType
    {
        /// <summary>${iServerJava6R_GridType_attribute_CROSS_D}</summary>
        CROSS,//           十字叉丝。 
        /// <summary>${iServerJava6R_GridType_attribute_GRID_D}</summary>
        GRID,   //        网格线。
        /// <summary>${iServerJava6R_GridType_attribute_POINT_D}</summary>
        POINT,//           点。 
    }
}
