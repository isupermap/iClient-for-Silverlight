﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// ${iServerJava6R_ChartFeatureInfoSpecsResult_Title}
    /// </summary>
    public class ChartFeatureInfoSpecsResult
    {
		/// <summary>${iServerJava6R_ChartFeatureInfoSpecsResult_constructor_D}</summary>
        public ChartFeatureInfoSpecsResult()
        {

        }

        /// <summary>
        /// ${iServerJava6R_ChartFeatureInfoSpecsResult_attribute_ChartFeatureInfoSpecs_D}
        /// </summary>
        public List<ChartFeatureInfoSpec> ChartFeatureInfoSpecs
        {
            get;
            set;
        }

        internal static ChartFeatureInfoSpecsResult FromJson(JsonArray json)
        {
            if (json == null)
            {
                return null;
            }
            ChartFeatureInfoSpecsResult result= new ChartFeatureInfoSpecsResult();
            result.ChartFeatureInfoSpecs = new List<ChartFeatureInfoSpec>();

            foreach (var item in json)
            {
                ChartFeatureInfoSpec feature = ChartFeatureInfoSpec.FromJson((JsonObject)item);
                result.ChartFeatureInfoSpecs.Add(feature);
            }

            return result;
        }
    }
}
