﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
using SuperMap.Web.Service;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetMapStatusService_Title}</para>
    /// 	<para>${iServerJava6R_GetMapStatusService_Description}</para>
    /// </summary>
    public class GetMapStatusService : ServiceBase
    {
        private int wkid;
        /// <summary>${iServerJava6R_GetMapStatusService_Title}</summary>
        /// <overloads>${iServerJava6R_GetMapStatusService_Description}</overloads>
        public GetMapStatusService()
        { }
        /// <summary>${iServerJava6R_GetMapStatusService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_GetMapStatusService_constructor_String_param_url}</param>
        public GetMapStatusService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_GetMapStatusService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_GetMapStatusService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync()
        {
            ProcessAsync(null);
        }

        public GetMapStatusService(string url, int wkid)
            : base(url)
        {
            this.wkid = wkid;
        }

        //url=http://localhost:8090/iserver/services/map-jingjin/rest/maps/京津地区人口分布图
        //absoluteurl=http://192.168.11.11:8090/iserver/services/map-jingjin/rest/maps/京津地区人口分布图.rjson
        /// <summary>${iServerJava6R_GetMapStatusService_method_ProcessAsync_D}</summary>
        /// <param name="state">${iServerJava6R_GetMapStatusService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(object state)
        {
            CheckUrl();

            base.SubmitRequest(Url, null, new EventHandler<RequestEventArgs>(Request_Completed), state, false, false, false);
        }

        private void CheckUrl()
        {
            if (Url.EndsWith("/"))
            {
                Url.TrimEnd(new char[] { '/' });
            }
            if (this.wkid > 0)
            {
                this.Url = this.Url + ".json?prjCoordSys={\"epsgCode\":" + this.wkid + "}";
            }
            else
            {
                this.Url = this.Url + ".json";
            }
        }

        private void Request_Completed(object sender, RequestEventArgs e)
        {
            //不做e.Error的判断
            if (e.Result != null && !string.IsNullOrEmpty(e.Result))
            {
                JsonObject json = (JsonObject)JsonValue.Parse(e.Result);
                GetMapStatusResult result = GetMapStatusResult.FromJson(json);
                lastResult = result;
                GetMapStatusEventArgs args = new GetMapStatusEventArgs(result, e.Result, e.UserState);
                OnProcessCompleted(args);
            }
        }
        /// <summary>${iServerJava6R_GetMapStatusService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetMapStatusEventArgs> ProcessCompleted;

        private void OnProcessCompleted(GetMapStatusEventArgs e)
        {
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }

        private GetMapStatusResult lastResult;
        /// <summary>${iServerJava6R_GetMapStatusResult_attribute_lastResult_D}</summary>
        public GetMapStatusResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
