﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ColorSpaceType_Title}</para>
    /// 	<para>${iServerJava6R_ColorSpaceType_Description}</para>
    /// </summary>
    public enum ColorSpaceType
    {
        /// <summary>${iServerJava6R_ColorSpaceType_attribute_CMYK_D}</summary>
        CMYK,//           该类型主要在印刷系统使用。
        /// <summary>${iServerJava6R_ColorSpaceType_attribute_RGB_D}</summary>
        RGB,//           该类型主要在显示系统中使用。 
    }
}
