﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetLayersInfoEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_GetLayersInfoEventArgs_Description}</para>
    /// </summary>
    public class GetLayersInfoEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_GetLayersInfoEventArgs_constructor_D}</summary>
        /// <param name="result">${iServerJava6R_GetLayersInfoEventArgs_param_result}</param>
        /// <param name="originResult">${iServerJava6R_GetLayersInfoEventArgs_param_originResult}</param>
        /// <param name="token">${iServerJava6R_GetLayersInfoEventArgs_param_token}</param>
        public GetLayersInfoEventArgs(GetLayersInfoResult result, string originResult, object token)
            : base(token)
        {
            this.Result = result;
            this.OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_GetLayersInfoEventArgs_attribute_result_D}</summary>
        public GetLayersInfoResult Result
        {
            get;
            internal set;
        }
        /// <summary>${iServerJava6R_GetLayersInfoEventArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            internal set;
        }
    }
}
