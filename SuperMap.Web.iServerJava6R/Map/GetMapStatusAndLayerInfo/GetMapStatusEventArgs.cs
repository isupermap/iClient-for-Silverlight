﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetMapStatusEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_GetMapStatusEventArgs_Description}</para>
    /// </summary>
    public class GetMapStatusEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_GetMapStatusEventArgs_constructor_D}</summary>
        /// <param name="result">${iServerJava6R_GetMapStatusEventArgs_param_result}</param>
        /// <param name="originResult">${iServerJava6R_GetMapStatusEventArgs_param_originResult}</param>
        /// <param name="token">${iServerJava6R_GetMapStatusEventArgs_param_token}</param>
        internal GetMapStatusEventArgs(GetMapStatusResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_GetMapStatusEventArgs_attribute_result_D}</summary>
        public GetMapStatusResult Result { get; private set; }
        /// <summary>${iServerJava6R_GetMapStatusEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
