﻿
using System;
using SuperMap.Web.Core;
using System.Json;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_DatasetInfo_Title}</para>
    /// 	<para>${iServerJava6R_DatasetInfo_Description}</para>
    /// </summary>
    public class DatasetInfo
    {
        internal DatasetInfo()
        { }
        /// <summary>${iServerJava6R_DatasetInfo_attribute_Name_D}</summary>
        public string Name { get; private set; }
        /// <summary>${iServerJava6R_DatasetInfo_attribute_Type_D}</summary>
        public DatasetType Type { get; private set; }
        /// <summary>${iServerJava6R_DatasetInfo_attribute_DatasourceName_D}</summary>
        public string DatasourceName { get; private set; }
        /// <summary>${iServerJava6R_DatasetInfo_attribute_bounds_D}</summary>
        public Rectangle2D Bounds { get; private set; }
        /// <summary>${iServerJava6R_DatasetInfo_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_DatasetInfo_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServerJava6R_DatasetInfo_method_FromJson_param_jsonObject}</param>
        public static DatasetInfo FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            DatasetInfo result = new DatasetInfo();
            result.Name = (string)jsonObject["name"];

            if (jsonObject["type"] != null)
            {
                result.Type = (DatasetType)Enum.Parse(typeof(DatasetType), (string)jsonObject["type"], true);
            }
            else
            {
            }
            result.DatasourceName = (string)jsonObject["dataSourceName"];
            if (jsonObject["bounds"] != null)
            {
                result.Bounds = DatasetInfo.ToRectangle2D((JsonObject)jsonObject["bounds"]);
            }
            else
            {  }
            return result;
        }

        internal static Rectangle2D ToRectangle2D(JsonObject jsonObject)
        {
            double mbMinX = (double)jsonObject["leftBottom"]["x"];
            double mbMinY = (double)jsonObject["leftBottom"]["y"];
            double mbMaxX = (double)jsonObject["rightTop"]["x"];
            double mbMaxY = (double)jsonObject["rightTop"]["y"];
            return new Rectangle2D(mbMinX, mbMinY, mbMaxX, mbMaxY);
        }
    }
}
