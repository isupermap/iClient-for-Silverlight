﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>${iServerJava6R_SuperMapLayerType_Title}</summary>
    public enum SuperMapLayerType
    {
        /// <summary>${iServerJava6R_SuperMapLayerType_attribute_Grid_D}</summary>
        GRID,//           栅格图层。
        /// <summary>${iServerJava6R_SuperMapLayerType_attribute_Image_D}</summary>
        IMAGE,//           影像图层。
        /// <summary>${iServerJava6R_SuperMapLayerType_attribute_THEME_D}</summary>
        THEME,//           专题图层。
        /// <summary>${iServerJava6R_SuperMapLayerType_attribute_vector_D}</summary>
        VECTOR,//           矢量图层。 
        /// <summary>${iServerJava6R_SuperMapLayerType_attribute_WMS_D}</summary>
        WFS,//           WFS 图层。 
        /// <summary>${iServerJava6R_SuperMapLayerType_attribute_WFS_D}</summary>
        WMS,//           WMS 图层。 
    }
}
