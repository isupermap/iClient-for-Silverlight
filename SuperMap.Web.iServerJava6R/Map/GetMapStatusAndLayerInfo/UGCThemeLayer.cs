﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>${iServerJava6R_UGCThemeLayer_Title}</summary>
    public class UGCThemeLayer : UGCLayer
    {
        /// <summary>${iServerJava6R_UGCThemeLayer_constructor_D}</summary>
        public UGCThemeLayer() { }
        /// <summary>${iServerJava6R_UGCThemeLayer_attribute_Theme_D}</summary>
        public Theme Theme{ get; set; }
        /// <summary>${iServerJava6R_UGCThemeLayer_attribute_ThemeType_D}</summary>
        public ThemeType ThemeType{get;set;}
    }
}
