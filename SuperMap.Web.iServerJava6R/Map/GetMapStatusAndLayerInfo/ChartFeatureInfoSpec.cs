﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// ${iServerJava6R_ChartFeatureInfoSpec_Title}
    /// </summary>
    public class ChartFeatureInfoSpec
    {
	    /// <summary>${iServerJava6R_ChartFeatureInfoSpec_constructor_D}</summary>
        public ChartFeatureInfoSpec()
        {

        }

        /// <summary>
        /// ${iServerJava6R_ChartFeatureInfoSpec_attribute_Acronym_D}
        /// </summary>
        public String Acronym
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_ChartFeatureInfoSpec_attribute_Code_D}
        /// </summary>
        public int Code
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_ChartFeatureInfoSpec_attribute_LocalName_D}
        /// </summary>
        public String LocalName
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_ChartFeatureInfoSpec_attribute_Name_D}
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_ChartFeatureInfoSpec_attribute_Primitive_D}
        /// </summary>
        public String Primitive
        {
            get;
            set;
        }

        /// <summary>
        ///  ${iServerJava6R_ChartFeatureInfoSpec_attribute_AttributeFields_D}
        /// </summary>
        public List<ChartAttributeSpec> AttributeFields
        {
            get;
            set;
        }

        internal static ChartFeatureInfoSpec FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            ChartFeatureInfoSpec feature = new ChartFeatureInfoSpec();
            feature.Acronym = json["acronym"];
            feature.Code = (int)json["code"];
            feature.LocalName = json["localName"];
            feature.Name = json["name"];
            feature.Primitive = json["primitive"];
            if (json.ContainsKey("attributeFields"))
            {
                feature.AttributeFields = new List<ChartAttributeSpec>();
                JsonArray array = (JsonArray)json["attributeFields"];
                foreach (var item in array)
                {
                    ChartAttributeSpec attrib = ChartAttributeSpec.FromJson((JsonObject)item);
                    feature.AttributeFields.Add(attrib);
                }
            }
            return feature;
        }
    }
}
