﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R
{
	/// <summary>
    /// ${iServerJava6R_ChartFeatureInfoSpecsEventArgs_Title}
    /// </summary>
    public class ChartFeatureInfoSpecsEventArgs : ServiceEventArgs
    {
		/// <summary>${iServerJava6R_ChartFeatureInfoSpecsEventArgs_constructor_D}</summary>
        /// <param name="result">${iServerJava6R_ChartFeatureInfoSpecsEventArgs_param_result}</param>
        /// <param name="originResult">${iServerJava6R_ChartFeatureInfoSpecsEventArgs_param_originResult}</param>
        /// <param name="token">${iServerJava6R_ChartFeatureInfoSpecsEventArgs_param_token}</param>
        public ChartFeatureInfoSpecsEventArgs(ChartFeatureInfoSpecsResult result, string originResult, object token)
            : base(token)
        {
            this.Result = result;
            this.OriginResult = originResult;
        }
		/// <summary>${iServerJava6R_ChartFeatureInfoSpecsEventArgs_attribute_result_D}</summary>
        public ChartFeatureInfoSpecsResult Result
        {
            get;
            internal set;
        }
		/// <summary>${iServerJava6R_ChartFeatureInfoSpecsEventArgs_attribute_originResult_D}</summary>
        public string OriginResult
        {
            get;
            internal set;
        }
    }
}
