﻿using System.Windows.Browser;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetLayersInfoService_Title}</para>
    /// 	<para>${iServerJava6R_GetLayersInfoService_Description}</para>
    /// </summary>
    public class GetLayersInfoService : ServiceBase
    {
        /// <summary>${iServerJava6R_GetLayersInfoService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_GetLayersInfoService_constructor_overloads_D}</overloads>
        public GetLayersInfoService()
        { }
        /// <summary>${iServerJava6R_GetLayersInfoService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_GetLayersInfoService_constructor_String_param_url}</param>
        public GetLayersInfoService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_GetLayersInfoService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_GetLayersInfoService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync()
        {
            ProcessAsync(null);
        }

        //url=http://localhost:8090/iserver/services/map-jingjin/rest/maps/京津地区人口分布图
        //absoluteurl=http://localhost:8090/iserver/services/map-jingjin/rest/maps/京津地区人口分布图/layers.rjson
        /// <summary>${iServerJava6R_GetLayersInfoService_method_ProcessAsync_D}</summary>
        /// <param name="state">${iServerJava6R_GetLayersInfoService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(object state)
        {
            CheckUrl();

            base.SubmitRequest(Url, null, new EventHandler<RequestEventArgs>(Request_Completed), state, false, false, false);
        }

        private void CheckUrl()
        {
            if (Url.EndsWith("/"))
            {
                Url.TrimEnd(new char[] { '/' });
            }

            this.Url = this.Url + "/layers.json";
        }

        private void Request_Completed(object sender, RequestEventArgs e)
        {
            //不做e.Error的判断
            if (e.Result != null && !string.IsNullOrEmpty(e.Result))
            {
                GetLayersInfoResult result = GetLayersInfoResult.FromJson(e.Result);
                lastResult = result;
                GetLayersInfoEventArgs args = new GetLayersInfoEventArgs(result, e.Result, e.UserState);
                OnProcessCompleted(args);
            }
        }
        /// <summary>${iServerJava6R_GetLayersInfoService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetLayersInfoEventArgs> ProcessCompleted;

        private void OnProcessCompleted(GetLayersInfoEventArgs e)
        {
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }

        private GetLayersInfoResult lastResult;
        /// <summary>${iServerJava6R_GetLayersInfoResult_attribute_lastResult_D}</summary>
        public GetLayersInfoResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
