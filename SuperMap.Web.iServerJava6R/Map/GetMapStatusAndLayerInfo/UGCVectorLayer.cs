﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>${iServerJava6R_UGCVectorLayer_Title}</summary>
    public class UGCVectorLayer : UGCLayer
    {
        /// <summary>${iServerJava6R_UGCVectorLayer_constructor_D}</summary>
        public UGCVectorLayer() { }
        /// <summary>${iServerJava6R_UGCVectorLayer_attribute_Style_D}</summary>
        public ServerStyle Style { get; set; }
        /// <summary>${iServerJava6R_UGCVectorLayer_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_UGCVectorLayer_method_FromJson_return}</returns>
        /// <param name="json">${iServerJava6R_UGCVectorLayer_method_FromJson_param_jsonObject}</param>
        public static UGCVectorLayer FromJson(System.Json.JsonObject json)
        {
            if (json == null) return null;
            UGCVectorLayer vectorLayer = new UGCVectorLayer();
            vectorLayer.Style = ServerStyle.FromJson(json);
            return vectorLayer;
        }
    }
}
