﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_JoinType_Tile}</para>
    /// 	<para>${iServerJava6R_JoinType_Description}</para>
    /// </summary>
    public enum JoinType
    {
        /// <summary><para>${iServerJava6R_JoinType_attribute_INNERJOIN_D}</para></summary>
        INNERJOIN,
        /// <summary>${iServerJava6R_JoinType_attribute_LEFTJOIN_D}</summary>
        LEFTJOIN
    }
}
