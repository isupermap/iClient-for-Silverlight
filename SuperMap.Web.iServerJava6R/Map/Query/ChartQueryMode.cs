﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
	/// <summary>
    /// ${iServerJava6R_Query_ChartQueryMode_Title}
    /// </summary>
    public enum ChartQueryMode
    {
        /// <summary>
        /// ${iServerJava6R_Query_ChartQueryMode_attribute_ChartAttributeQuery_D}
        /// </summary>
        ChartAttributeQuery,
        /// <summary>
        ///  ${iServerJava6R_Query_ChartQueryMode_attribute_ChartBoundsQuery_D}
        /// </summary>
        ChartBoundsQuery
    }
}
