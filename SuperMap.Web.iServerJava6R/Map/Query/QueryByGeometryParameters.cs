﻿

using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_QueryByGeometryParameters_Tile}</para>
    /// 	<para>${iServerJava6R_QueryByGeometryParameters_Description}</para>
    /// </summary>
    public class QueryByGeometryParameters:QueryParameters
    {
        /// <summary>${iServerJava6R_QueryByGeometryParameters_constructor_None_D}</summary>
        public QueryByGeometryParameters()
        { }

        /// <summary>${iServerJava6R_QueryByGeometryParameters_attribute_Geometry_D}</summary>
        public Geometry Geometry { get; set; }
        /// <summary>${iServerJava6R_QueryByGeometryParameters_attribute_SpatialQueryMode_D}</summary>
        public SpatialQueryMode SpatialQueryMode { get; set; }

    }
}
