﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Core;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R
{
	/// <summary>
    /// ${iServerJava6R_ChartQueryParameters_Tile}
    /// </summary>
    public class ChartQueryParameters
    {
		/// <summary>${iServerJava6R_ChartQueryParameters_constructor_None_D}</summary>
        public ChartQueryParameters()
        {

        }
		/// <summary>${iServerJava6R_ChartQueryParameters_attribute_QueryMode_D}</summary>
        public ChartQueryMode QueryMode
        {
            get;
            set;
        }
		/// <summary>${iServerJava6R_ChartQueryParameters_attribute_Bounds_D}</summary>
        public Rectangle2D Bounds
        {
            get;
            set;
        }
		/// <summary>${iServerJava6R_ChartQueryParameters_attribute_ChartLayerNames_D}</summary>
        public List<string> ChartLayerNames
        {
            get;
            set;
        }
		/// <summary>${iServerJava6R_ChartQueryParameters_attribute_ChartQueryFilterParameters_D}</summary>
        public List<ChartQueryFilterParameter> ChartQueryFilterParameters
        {
            get;
            set;
        }
		/// <summary>${iServerJava6R_ChartQueryParameters_attribute_ReturnContent_D}</summary>
        public bool ReturnContent
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ChartQueryParameters_attribute_StartRecord_D}</summary>
        public int StartRecord
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ChartQueryParameters_attribute_ExpectCount_D}</summary>
        public int ExpectCount
        {
            get;
            set;
        }
    }
}
