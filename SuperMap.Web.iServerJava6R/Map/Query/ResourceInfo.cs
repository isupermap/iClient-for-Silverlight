﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Query_ResourceInfo_Title}</para>
    /// 	<para>${iServerJava6R_Query_ResourceInfo_Description}</para>
    /// </summary>
    public class ResourceInfo
    {
        internal ResourceInfo()
        { }

        /// <summary>${iServerJava6R_Query_ResourceInfo_attribute_Succeed_D}</summary>
        public bool Succeed { get; internal set; }
        /// <summary>${iServerJava6R_Query_ResourceInfo_attribute_NewResourceLocation_D}</summary>
        public string NewResourceLocation { get; internal set; }
        //
        /// <summary>${iServerJava6R_Query_ResourceInfo_attribute_NewResourceID_D}</summary>
        public string NewResourceID { get; internal set; }
    }
}
