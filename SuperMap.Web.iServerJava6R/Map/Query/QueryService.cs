﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.iServerJava6R.Resources;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Query_QueryService_Title}</para>
    /// 	<para>${iServerJava6R_Query_QueryService_Description}</para>
    /// </summary>
    public abstract class QueryService : ServiceBase
    {
        /// <summary>${iServerJava6R_Query_QueryService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_Query_QueryService_constructor_overloads}</overloads>
        public QueryService() { }

        /// <summary>${iServerJava6R_Query_QueryService_constructor_string_D}</summary>
        /// <param name="url">${iServerJava6R_Query_QueryService_constructor_string_param_url}</param>
        public QueryService(string url)
            : base(url)
        { }

        /// <summary>${iServerJava6R_Query_QueryService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_Query_QueryService_method_ProcessAsync_overloads}</overloads>
        /// <param name="parameters">${iServerJava6R_Query_QueryService_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(QueryParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${iServerJava6R_Query_QueryService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_Query_QueryService_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServerJava6R_Query_QueryService_ProcessAsync_param_state}</param>
        public void ProcessAsync(QueryParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }
            //参数必须放在 URI 中
            base.Url += string.Format("queryResults.json?returnContent={0}&debug=true", parameters.ReturnContent.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());

            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true);
        }
        /// <summary>${iServerJava6R_Query_QueryService_method_GetParameters_D}</summary>
        /// <param name="parameters">${iServerJava6R_Query_QueryService_method_GetParameters_parameters}</param>
        protected abstract Dictionary<string, string> GetParameters(QueryParameters parameters);


        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            QueryResult result = QueryResult.FromJson(jsonObject);
            LastResult = result;
            QueryEventArgs args = new QueryEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(QueryEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                //Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_Query_QueryService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<QueryEventArgs> ProcessCompleted;


        private QueryResult lastResult;
        /// <summary>${iServerJava6R_Query_QueryService_attribute_LastResult_D}</summary>
        public QueryResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }

}
