﻿

using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_QueryBySQlService_Tile}</para>
    /// 	<para>${iServerJava6R_QueryBySQlService_Description}</para>
    /// </summary>
    public class QueryBySQLService : QueryService
    {
        /// <summary>${iServerJava6R_QueryBySQlService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_QueryBySQlService_constructor_overloads}</overloads>
        public QueryBySQLService() { }

        /// <summary>${iServerJava6R_QueryBySQlService_constructor_string_url}</summary>
        /// <param name="url">${iServerJava6R_QueryBySQlService_string_constructor_param_url}</param>
        public QueryBySQLService(string url) : base(url) { }
        /// <summary>${iServerJava6R_Query_QueryByDistanceService_method_GetParameters_D}</summary>
        /// <param name="parameters">${iServerJava6R_Query_QueryByDistanceService_method_GetParameters_parameters}</param>
        protected override Dictionary<string, string> GetParameters(QueryParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("queryMode", "\"SqlQuery\"");
            dictionary.Add("queryParameters", QueryParameters.ToJson(parameters));
            return dictionary;
        }
    }
}
