﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_EngineType_Tile}</para>
    /// 	<para>${iServerJava6R_EngineType_Description}</para>
    /// </summary>
    public enum EngineType
    {
        /// <summary>${iServerJava6R_EngineType_attribute_IMAGEPLUGINS_D}</summary>
        IMAGEPLUGINS,
        /// <summary>${iServerJava6R_EngineType_attribute_OGC_D}</summary>
        OGC,
        /// <summary>${iServerJava6R_EngineType_attribute_ORACLEPLUS_D}</summary>
        ORACLEPLUS,
        /// <summary>${iServerJava6R_EngineType_attribute_SDBPLUS_D}</summary>
        SDBPLUS,
        /// <summary>${iServerJava6R_EngineType_attribute_SQLPLUS_D}</summary>
        SQLPLUS,
        /// <summary>${iServerJava6R_EngineType_attribute_UDB_D}</summary>
        UDB
    }
}
