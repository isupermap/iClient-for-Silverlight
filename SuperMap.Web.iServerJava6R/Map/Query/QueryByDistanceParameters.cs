﻿

using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_QueryByDistanceParameters_Tile}</para>
    /// 	<para>${iServerJava6R_QueryByDistanceParameters_Description}</para>
    /// </summary>
    public class QueryByDistanceParameters : QueryParameters
    {
        /// <summary>${iServerJava6R_QueryByDistanceParameters_constructor_None_D}</summary>
        public QueryByDistanceParameters()
        { }
        /// <summary>${iServerJava6R_QueryByDistanceParameters_attribute_Distance_D}</summary>
        public double Distance { get; set; }

        /// <summary>${iServerJava6R_QueryByDistanceParameters_attribute_Geometry_D}</summary>
        public Geometry Geometry { get; set; }

        /// <summary>${iServerJava6R_QueryByDistanceParameters_attribute_IsNearest_D}</summary>
        public bool IsNearest { get; set; }
    }
}
