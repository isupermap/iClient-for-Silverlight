﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_FieldType_Tile}</para>
    /// 	<para>${iServerJava6R_FieldType_Description}</para>
    /// </summary>
    public enum FieldType
    {
        /// <summary>${iServerJava6R_FieldType_attribute_BOOLEAN_D}</summary>
        BOOLEAN,
        /// <summary>${iServerJava6R_FieldType_attribute_BYTE_D}</summary>
        BYTE,
        /// <summary>${iServerJava6R_FieldType_attribute_INT16_D}</summary>
        INT16,
        /// <summary>${iServerJava6R_FieldType_attribute_INT32_D}</summary>
        INT32,
        /// 
        INT64,
        /// <summary>${iServerJava6R_FieldType_attribute_SINGLE_D}</summary>
        SINGLE,
        /// <summary>${iServerJava6R_FieldType_attribute_DOUBLE_D}</summary>
        DOUBLE,
        /// <summary>${iServerJava6R_FieldType_attribute_DATETIME_D}</summary>
        DATETIME,
        /// <summary>${iServerJava6R_FieldType_attribute_LONGBINARY_D}</summary>
        LONGBINARY,
        /// <summary>${iServerJava6R_FieldType_attribute_TEXT_D}</summary>
        TEXT,
        /// <summary>${iServerJava6R_FieldType_attribute_WTEXT_D}</summary>
        WTEXT,
        /// <summary>${iServerJava6R_FieldType_attribute_CHAR_D}</summary>
        CHAR
    }
}
