﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Query_QueryOption_Title}</para>
    /// 	<para>${iServerJava6R_Query_QueryOption_Description}</para>
    /// </summary>
    public enum QueryOption
    {
        /// <summary>${iServerJava6R_Query_QueryOption_attribute_ATTRIBUTE_D}</summary>
        ATTRIBUTE,
        /// <summary>${iServerJava6R_Query_QueryOption_attribute_ATTRIBUTEANDGEOMETRY_D}</summary>
        ATTRIBUTEANDGEOMETRY,
        /// <summary>${iServerJava6R_Query_QueryOption_attribute_GEOMETRY_D}</summary>
        GEOMETRY
    }
}
