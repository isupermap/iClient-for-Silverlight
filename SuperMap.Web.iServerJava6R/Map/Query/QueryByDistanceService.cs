﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.iServerJava6R.Resources;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_QueryByDistanceService_Tile}</para>
    /// 	<para>${iServerJava6R_QueryByDistanceService_Description}</para>
    /// </summary>
    public class QueryByDistanceService : QueryService
    {

        /// <summary>${iServerJava6R_QueryByDistanceService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_QueryByDistanceService_constructor_overloads}</overloads>
        public QueryByDistanceService() { }

        /// <summary>${iServerJava6R_QueryByDistanceService_string_constructor}</summary>
        public QueryByDistanceService(string url) : base(url) { }

        /// <summary>${iServerJava6R_Query_QueryByDistanceService_method_GetParameters_D}</summary>
        /// <param name="parameters">${iServerJava6R_Query_QueryByDistanceService_method_GetParameters_parameters}</param>
        protected override Dictionary<string, string> GetParameters(QueryParameters parameters)
        {

            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (((QueryByDistanceParameters)parameters).IsNearest)
            {
                dictionary.Add("queryMode", "\"FindNearest\"");
            }
            else
            {
                dictionary.Add("queryMode", "\"DistanceQuery\"");
            }
            dictionary.Add("queryParameters", QueryParameters.ToJson(parameters));
            dictionary.Add("geometry", ServerGeometry.ToJson(((QueryByDistanceParameters)parameters).Geometry.ToServerGeometry()));
            dictionary.Add("distance", ((QueryByDistanceParameters)parameters).Distance.ToString(System.Globalization.CultureInfo.InvariantCulture));

            return dictionary;
        }
    }
}
