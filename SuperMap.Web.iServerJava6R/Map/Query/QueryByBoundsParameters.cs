﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Core;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>${iServerJava6R_Query_QueryByBoundsParameters_Title}</summary>
    public class QueryByBoundsParameters : QueryParameters
    {
        /// <summary>${iServerJava6R_Query_QueryByBoundsParameters_constructor_None_D}</summary>
        public QueryByBoundsParameters()
        { }
        /// <summary>${iServerJava6R_Query_QueryByBoundsParameters_attribute_Bounds_D}</summary>
        public Rectangle2D Bounds { get; set; }
    }
}
