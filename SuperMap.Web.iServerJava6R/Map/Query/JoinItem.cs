﻿

using System.Collections.Generic;
using System.Json;
using System;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_JoinItem_Tile}</para>
    /// 	<para>${iServerJava6R_JoinItem_Description}</para>
    /// </summary>
    /// <remarks>${iServerJava6R_LinkItem_remarks}</remarks>
    public class JoinItem
    {
        /// <summary>${iServerJava6R_JoinItem_constructor_None_D}</summary>
        public JoinItem()
        { }

        /// <summary>${iServerJava6R_JoinItem_attribute_ForeignTableName_D}</summary>
        public string ForeignTableName { get; set; }

        /// <summary>${iServerJava6R_JoinItem_attribute_JoinFilter_D}</summary>
        public string JoinFilter { get; set; }

        /// <summary>${iServerJava6R_JoinItem_attribute_JoinType_D}</summary>
        public JoinType JoinType { get; set; }


        internal static string ToJson(JoinItem param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(param.ForeignTableName))
            {
                list.Add(string.Format("\"foreignTableName\":\"{0}\"", param.ForeignTableName));
            }
            else
            {
                list.Add("\"foreignTableName\":null");
            }

            if (!string.IsNullOrEmpty(param.JoinFilter))
            {
                list.Add(string.Format("\"joinFilter\":\"{0}\"", param.JoinFilter));
            }
            else
            {
                list.Add("\"joinFilter\":null");
            }

            list.Add(string.Format("\"joinType\":\"{0}\"", param.JoinType.ToString()));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal static JoinItem FromJson(JsonObject json)
        {
            if (json == null) return null;
            JoinItem item = new JoinItem();
            if (json.ContainsKey("foreignTableName"))
            {
                item.ForeignTableName = json["foreignTableName"];
            }
            if (json.ContainsKey("joinFilter"))
            {
                item.JoinFilter = json["joinFilter"];
            }
            if (json.ContainsKey("joinType") && !string.IsNullOrEmpty(json["joinType"]))
            {
                item.JoinType = (JoinType)Enum.Parse(typeof(JoinType), json["joinType"], true);
            }
            return item;
        }
    }
}
