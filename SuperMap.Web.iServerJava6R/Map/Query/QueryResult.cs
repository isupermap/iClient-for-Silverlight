﻿

using System.Json;
using System.Collections.Generic;
using System;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Query_QueryResult_Title}</para>
    /// 	<para>${iServerJava6R_Query_QueryResult_Description}</para>
    /// </summary>
    public class QueryResult
    {
        internal QueryResult()
        {
            Recordsets = new List<Recordset>();
        }

        /// <summary>${iServerJava6R_Query_ResultSet_attribute_totalCount_D}</summary>
        public int TotalCount { get; private set; }
        /// <summary>${iServerJava6R_Query_ResultSet_attribute_currentCount_D}</summary>
        public int CurrentCount { get; private set; }
        /// <summary>${iServerJava6R_Query_ResultSet_attribute_customResponse_D}</summary>
        public string CustomResponse { get; private set; }
        /// <summary>${iServerJava6R_Query_ResultSet_attribute_recordSets_D}</summary>
        public IList<Recordset> Recordsets { get; private set; }

        //当返回结果是资源时，用到此项；
        /// <summary>${iServerJava6R_Query_ResultSet_attribute_ResourceInfo_D}</summary>
        public ResourceInfo ResourceInfo { get; private set; }


        /// <summary>${iServerJava6R_ResultSet_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_ResultSet_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_ResultSet_method_fromJson_param_jsonObject}</param>
        public static QueryResult FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            QueryResult result = new QueryResult();

            if (json.ContainsKey("totalCount") && json.ContainsKey("currentCount") && json.ContainsKey("customResponse") && json.ContainsKey("recordsets"))
            {
                result.TotalCount = (int)json["totalCount"];

                if ((int)json["totalCount"] == 0)
                {
                    return null;
                }

                result.CurrentCount = (int)json["currentCount"];
                result.CustomResponse = (string)json["customResponse"];

                JsonArray recordsets = (JsonArray)json["recordsets"];
                if (recordsets != null && recordsets.Count > 0)
                {
                    result.Recordsets = new List<Recordset>();

                    for (int i = 0; i < recordsets.Count; i++)
                    {
                        result.Recordsets.Add(Recordset.FromJson((JsonObject)recordsets[i]));
                    }
                }
                return result;
            }
            else if (json.ContainsKey("succeed") && json.ContainsKey("newResourceLocation"))
            {
                ResourceInfo info = new ResourceInfo();
                info.Succeed = (bool)json["succeed"];
                info.NewResourceLocation = (string)json["newResourceLocation"];
                info.NewResourceID = (string)json["newResourceID"];
                result.ResourceInfo = info;
                return result;
            }
            return null;
        }
    }
}
