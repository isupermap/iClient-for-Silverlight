﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Query_QueryByBoundsService_Title}</para>
    /// 	<para>${iServerJava6R_Query_QueryByBoundsService_Description}</para>
    /// </summary>
    public class QueryByBoundsService : QueryService
    {
        /// <summary>${iServerJava6R_Query_QueryByBoundsService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_Query_QueryByBoundsService_constructor_overloads}</overloads>
        public QueryByBoundsService()
        { }
        /// <summary>${iServerJava6R_Query_QueryByBoundsService_constructor_string_D}</summary>
        /// <param name="url">${iServerJava6R_Query_QueryByBoundsService_constructor_string_param_url}</param>
        public QueryByBoundsService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_Query_QueryByBoundsService_method_GetParameters}</summary>
        /// <param name="parameters">${iServerJava6R_Query_QueryByBoundsService_method_GetParameters_parameters}</param>
        protected override Dictionary<string, string> GetParameters(QueryParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("queryMode", "\"BoundsQuery\"");
            dictionary.Add("queryParameters", QueryParameters.ToJson(parameters));
            if (!((QueryByBoundsParameters)parameters).Bounds.IsEmpty)
            {
                dictionary.Add("bounds", JsonHelper.FromRectangle2D(((QueryByBoundsParameters)parameters).Bounds));
            }
            return dictionary;
        }
    }
}
