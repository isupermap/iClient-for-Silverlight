﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Query_QueryServiceEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_Query_QueryServiceEventArgs_Description}</para>
    /// </summary>
    public class QueryEventArgs:ServiceEventArgs
    {
        /// <summary>${iServerJava6R_Query_QueryServiceEventArgs_constructor_D}</summary>
        /// <param name="result">${iServerJava6R_Query_QueryServiceEventArgs_constructor_param_result}</param>
        /// <param name="originResult">${iServerJava6R_Query_QueryServiceEventArgs_constructor_param_originResult}</param>
        /// <param name="token">${iServerJava6R_Query_QueryServiceEventArgs_constructor_param_token}</param>
        public QueryEventArgs(QueryResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_Query_QueryServiceEventArgs_attribute_Result_D}</summary>
        public QueryResult Result { get; private set; }
        /// <summary>${iServerJava6R_Query_QueryServiceEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
