﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;

namespace SuperMap.Web.iServerJava6R
{
	/// <summary>
    /// ${iServerJava6R_ChartQueryFilterParameter_Tile}
    /// </summary>
    public class ChartQueryFilterParameter
    {
		/// <summary>
        /// ${iServerJava6R_ChartQueryFilterParameter_constructor_None_D}
        /// </summary>
        public ChartQueryFilterParameter()
        {

        }

        /// <summary>
        /// ${iServerJava6R_ChartQueryFilterParameter_attribute_IsQueryPoint_D}
        /// </summary>
        public bool IsQueryPoint
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_ChartQueryFilterParameter_attribute_IsQueryLine_D}
        /// </summary>
        public bool IsQueryLine
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_ChartQueryFilterParameter_attribute_IsQueryRegion_D}
        /// </summary>
        public bool IsQueryRegion
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_ChartQueryFilterParameter_attribute_AttributeFilter_D}
        /// </summary>
        public string AttributeFilter
        {
            get;
            set;
        }

        /// <summary>
        /// ${iServerJava6R_ChartQueryFilterParameter_attribute_ChartFeatureInfoSpecCode_D}
        /// </summary>
        public int ChartFeatureInfoSpecCode
        {
            get;
            set;
        }

        internal static string ToJson(ChartQueryFilterParameter param)
        {
            if (param == null)
            {
                return string.Empty;
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("{")
              .Append(string.Format("\"isQueryPoint\":{0},", param.IsQueryPoint.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()))
              .Append(string.Format("\"isQueryLine\":{0},", param.IsQueryLine.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()))
              .Append(string.Format("\"isQueryRegion\":{0},", param.IsQueryRegion.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()))
              .Append(string.Format("\"attributeFilter\":\"{0}\",", param.AttributeFilter))
              .Append(string.Format("\"chartFeatureInfoSpecCode\":{0}", param.ChartFeatureInfoSpecCode.ToString(System.Globalization.CultureInfo.InvariantCulture)))
              .Append("}");

            return sb.ToString();
        }
    }
}
