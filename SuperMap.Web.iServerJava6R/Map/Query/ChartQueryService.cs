﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using System.Windows.Browser;
using System.Collections.Generic;
using SuperMap.Web.Utilities;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// ${iServerJava6R_Query_ChartQueryService_Title}
    /// </summary>
    public class ChartQueryService:ServiceBase
    {
        /// <summary>${iServerJava6R_Query_ChartQueryService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_Query_ChartQueryService_constructor_overloads}</overloads>
		public ChartQueryService()
        { }
        /// <summary>${iServerJava6R_Query_ChartQueryService_constructor_string_D}</summary>
        /// <param name="url">${iServerJava6R_Query_ChartQueryService_constructor_string_param_url}</param>
        public ChartQueryService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_Query_ChartQueryService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_Query_ChartQueryService_method_ProcessAsync_overloads}</overloads>
        /// <param name="parameter">${iServerJava6R_Query_ChartQueryService_ProcessAsync_param_parameter}</param>
        public void ProcessAsync(ChartQueryParameters parameter)
        {
            ProcessAsync(parameter,null);
        }
        /// <summary>${iServerJava6R_Query_ChartQueryService_method_ProcessAsync_D}</summary>
        /// <param name="parameter">${iServerJava6R_Query_ChartQueryService_ProcessAsync_param_parameter}</param>
        /// <param name="state">${iServerJava6R_Query_ChartQueryService_ProcessAsync_param_state}</param>
        public void ProcessAsync(ChartQueryParameters parameter,object state)
        {
            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }
            //参数必须放在 URI 中
            base.Url += string.Format("queryResults.json?returnContent={0}&debug=true", parameter.ReturnContent.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());

            base.SubmitRequest(base.Url, GetParameters(parameter), new EventHandler<RequestEventArgs>(Request_Completed), state, true);
        }

        private Dictionary<string, string> GetParameters(ChartQueryParameters parameter)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("queryMode", parameter.QueryMode.ToString());
            if (parameter.QueryMode == ChartQueryMode.ChartBoundsQuery)
            {
                dic.Add("bounds", JsonHelper.FromRectangle2D(parameter.Bounds));
            }
            dic.Add("chartLayerNames", JsonHelper.FromIList(parameter.ChartLayerNames));
            List<string> filters = new List<string>();
            foreach (var v in parameter.ChartQueryFilterParameters)
            {
                filters.Add(ChartQueryFilterParameter.ToJson(v));
            }
            string filter = string.Format("[{0}]", string.Join(",", filters.ToArray()));
            dic.Add("chartQueryParameters", "{" + string.Format("\"chartQueryParams\":{0},\"startRecord\":{1},\"expectCount\":{2}", filter,parameter.StartRecord.ToStringEx(),parameter.ExpectCount.ToStringEx()) + "}");

            return dic;
        }

        private void Request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            QueryResult result = QueryResult.FromJson(jsonObject);
            LastResult = result;
            QueryEventArgs args = new QueryEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(QueryEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                //Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_Query_ChartQueryService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<QueryEventArgs> ProcessCompleted;


        private QueryResult lastResult;
        /// <summary>${iServerJava6R_Query_ChartQueryService_attribute_LastResult_D}</summary>
        public QueryResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
