﻿

using System.Json;
using System.Collections.Generic;
using System;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Query_Recordset_Title}</para>
    /// 	<para>${iServerJava6R_Query_Recordset_Description}</para>
    /// </summary>
    public class Recordset
    {
        internal Recordset()
        { }

        /// <summary>${iServerJava6R_Query_Recordset_attribute_DatasetName_D}</summary>
        public string DatasetName { get; private set; }
        /// <summary>${iServerJava6R_Query_Recordset_attribute_FieldCaptions_D}</summary>
        public List<string> FieldCaptions { get; private set; }
        /// <summary>${iServerJava6R_Query_Recordset_attribute_Fields_D}</summary>
        public List<string> Fields { get; private set; }
        /// <summary>${iServerJava6R_Query_Recordset_attribute_FieldTypes_D}</summary>
        public List<FieldType> FieldTypes { get; private set; }
        /// <summary>${iServerJava6R_Query_Recordset_attribute_Features_D}</summary>
        public FeatureCollection Features { get; private set; }
        /// <summary>${iServerJava6R_Query_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_Query_method_FromJson_return}</returns>
        /// <param name="json">${iServerJava6R_Query_method_FromJson_param_jsonObject}</param>
        public static Recordset FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            Recordset result = new Recordset();

            result.DatasetName = (string)json["datasetName"];
            result.FieldCaptions = JsonHelper.ToStringList((JsonArray)json["fieldCaptions"]);


            JsonArray fieldtypes = (JsonArray)json["fieldTypes"];
            if (fieldtypes != null && fieldtypes.Count > 0)
            {
                result.FieldTypes = new List<FieldType>();
                for (int i = 0; i < fieldtypes.Count; i++)
                {
                    result.FieldTypes.Add((FieldType)Enum.Parse(typeof(FieldType), (string)fieldtypes[i], true));
                }
            }

            result.Fields = JsonHelper.ToStringList((JsonArray)json["fields"]);
            List<string> fieldNames = JsonHelper.ToStringList((JsonArray)json["fields"]);
            JsonArray features = (JsonArray)json["features"];
            if (features != null && features.Count > 0 )
            {
                result.Features = new FeatureCollection();

                for (int i = 0; i < features.Count; i++)
                {
                    ServerFeature f = ServerFeature.FromJson((JsonObject)features[i]);

                    f.FieldNames = new List<string>();
                    if (fieldNames != null && fieldNames.Count > 0)
                    {
                        for (int j = 0; j < fieldNames.Count; j++)
                        {
                            f.FieldNames.Add(fieldNames[j]);
                        }
                    }
                    result.Features.Add(f.ToFeature());
                }
            }
            return result;
        }
    }
}
