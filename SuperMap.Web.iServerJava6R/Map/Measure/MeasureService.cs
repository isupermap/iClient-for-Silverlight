﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.Resources;
using System.Collections.ObjectModel;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_MeasureService_Tile}</para>
    /// 	<para>${iServerJava6R_MeasureService_Description}</para>
    /// </summary>
    public class MeasureService : ServiceBase
    {
        /// <summary>${iServerJava6R_MeasureService_constructor_None_D}</summary>
        /// <overloads>${iServerJava6R_MeasureService_constructor_overloads}</overloads>
        public MeasureService() { }

        /// <summary>${iServerJava6R_MeasureService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_MeasureService_constructor_String_param_url}</param>
        public MeasureService(string url) : base(url) { }

        /// <summary>${iServerJava6R_MeasureService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_MeasureService_method_ProcessAsync_overloads}</overloads>
        /// <param name="parameters">${iServerJava6R_MeasureService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(MeasureParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServerJava6R_MeasureService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_MeasureService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServerJava6R_MeasureService_method_processAsync_param_state}</param>
        public void ProcessAsync(MeasureParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            //将错误抛给服务器，让其返回错误结果，出发我们的Failed事件；
            if (parameters.Geometry is GeoLine)
            {
                base.Url += "distance.json?debug=true&_method=GET&";
            }
            else if (parameters.Geometry is GeoRegion)
            {
                base.Url += "area.json?debug=true&_method=GET&";
            }
            else
            {
                base.Url += "distance.json?debug=true&_method=GET&";
            }
            
            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true);
        }

        private Dictionary<string, string> GetParameters(MeasureParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Point2DCollection ps = new Point2DCollection();
            ObservableCollection<Point2DCollection> og = new ObservableCollection<Point2DCollection>();
            if (parameters.Geometry is GeoLine)
            {
                og = (parameters.Geometry as GeoLine).Parts;
            }
            else if (parameters.Geometry is GeoRegion)
            {
                og = (parameters.Geometry as GeoRegion).Parts;
            }
            else
            {
                dictionary.Add("point2Ds", "[]");
                dictionary.Add("unit", parameters.Unit.ToString().ToUpper());
                return dictionary;
            }

            foreach (Point2DCollection g in og)
            {
                for (int i = 0; i < g.Count; i++)
                {
                    ps.Add(g[i]);
                }
            }
            dictionary.Add("point2Ds", JsonHelper.FromPoint2DCollection(ps));
            dictionary.Add("unit", parameters.Unit.ToString().ToUpper());
            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            MeasureResult result = MeasureResult.FromJson(jsonObject);
            LastResult = result;
            MeasureEventArgs args = new MeasureEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(MeasureEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                //Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_MeasureService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<MeasureEventArgs> ProcessCompleted;

        private MeasureResult lastResult;
        /// <summary>${iServerJava6R_MeasureService_attribute_lastResult_D}</summary>
        public MeasureResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
