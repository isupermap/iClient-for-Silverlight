﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_MeasureEventArgs_Tile}</para>
    /// 	<para>${iServerJava6R_MeasureEventArgs_Description}</para>
    /// </summary>
    public class MeasureEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_MeasureEventArgs_constructor_D}</summary>
        /// <param name="result">${iServerJava6R_MeasureEventArgs_constructor_param_result}</param>
        /// <param name="originResult">${iServerJava6R_MeasureEventArgs_constructor_param_originResult}</param>
        /// <param name="token">${iServerJava6R_MeasureEventArgs_constructor_param_token}</param>
        public MeasureEventArgs(MeasureResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_MeasureEventArgs_attribute_Result_D}</summary>
        public MeasureResult Result { get; private set; }
        /// <summary>${iServerJava6R_MeasureEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
