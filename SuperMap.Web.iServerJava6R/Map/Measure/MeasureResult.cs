﻿

using System.Json;
using SuperMap.Web.Core;
using System;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_MeasureResult_Tile}</para>
    /// 	<para>${iServerJava6R_MeasureResult_Description}</para>
    /// </summary>
    public class MeasureResult
    {
        internal MeasureResult()
        { }

        /// <summary>${iServerJava6R_MeasureResult_attribute_Area_D}</summary>
        public double Area { get; private set; }
        /// <summary>${iServerJava6R_MeasureResult_attribute_Distance_D}</summary>
        public double Distance { get; private set; }
        /// <summary>${iServerJava6R_MeasureResult_attribute_Unit_D}</summary>
        public Unit Unit { get; private set; }

        /// <summary>${iServerJava6R_MeasureResult_method_fromJSON_D}</summary>
        /// <returns>${iServerJava6R_MeasureResult_method_fromJSON_return}</returns>
        /// <param name="json">${iServerJava6R_MeasureResult_method_fromJSON_param_json}</param>
        public static MeasureResult FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new MeasureResult
            {
                Area = (double)json["area"],
                Distance = (double)json["distance"],
                Unit = (Unit)Enum.Parse(typeof(Unit),(string)json["unit"], true)
            };
        }
    }
}
