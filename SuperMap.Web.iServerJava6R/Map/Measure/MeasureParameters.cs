﻿
using System.Collections.Generic;
using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_MeasureParameters_Tile}</para>
    /// 	<para>${iServerJava6R_MeasureParameters_Description}</para>
    /// </summary>
    public class MeasureParameters
    {
        /// <summary>${iServerJava6R_MeasureParameters_constructor_None_D}</summary>
        public MeasureParameters()
        {
            Unit = Unit.Meter;
        }

        /// <summary>${iServerJava6R_MeasureParameters_attribute_Geometry_D}</summary>
        public Geometry Geometry { get; set; }
        /// <summary>${iServerJava6R_MeasureParameters_attribute_Unit_D}</summary>
        public Unit Unit { get; set; }
    }
}
