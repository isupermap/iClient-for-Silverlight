﻿

using System.Json;
using System;
using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_PrjCoordSys_Title}</para>
    /// 	<para>${iServerJava6R_PrjCoordSys_Description}</para>
    /// </summary>
    public class PrjCoordSys
    {
        /// <summary>${iServerJava6R_PrjCoordSys_constructor_D}</summary>
        public PrjCoordSys()
        { }
        /// <summary>${iServerJava6R_PrjCoordSys_attribute_CoordUnit_D}</summary>
        public Unit CoordUnit { get; set; }
        /// <summary>${iServerJava6R_PrjCoordSys_attribute_Name_D}</summary>
        public string Name { get; set; }
        /// <summary>${iServerJava6R_PrjCoordSys_attribute_Projection_D}</summary>
        public Projection Projection { get; set; }
        /// <summary>${iServerJava6R_PrjCoordSys_attribute_CoordSystem_D}</summary>
        public CoordSys CoordSystem { get; set; }
        /// <summary>${iServerJava6R_PrjCoordSys_attribute_DistanceUnit_D}</summary>
        public Unit DistanceUnit { get; set; }
        /// <summary>${iServerJava6R_PrjCoordSys_attribute_ProjectionParam_D}</summary>
        public PrjParameter ProjectionParam { get; set; }
        /// <summary>${iServerJava6R_PrjCoordSys_attribute_epsgCode_D}</summary>
        public int EpsgCode { get; set; }
        /// <summary>${iServerJava6R_PrjCoordSys_attribute_Type_D}</summary>
        public PrjCoordSysType Type { get; set; }

        internal static PrjCoordSys FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            PrjCoordSys coorSys = new PrjCoordSys();
            coorSys.CoordUnit = (Unit)Enum.Parse(typeof(Unit), (string)json["coordUnit"], true);
            coorSys.Name = (string)json["name"];
            coorSys.Projection = Projection.FromJson((JsonObject)json["projection"]);
            coorSys.CoordSystem = CoordSys.FromJson((JsonObject)json["coordSystem"]);
            coorSys.DistanceUnit = (Unit)Enum.Parse(typeof(Unit), (string)json["distanceUnit"], true);
            coorSys.ProjectionParam = PrjParameter.FromJson((JsonObject)json["projectionParam"]);
            //Type = (string)json["type"],
            if (json["type"] != null)
            {
                coorSys.Type = (PrjCoordSysType)Enum.Parse(typeof(PrjCoordSysType), json["type"], true);
            }
            coorSys.EpsgCode = (int)json["epsgCode"];

            return coorSys;
        }
    }
}
