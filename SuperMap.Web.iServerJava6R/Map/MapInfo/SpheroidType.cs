﻿
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>${iServerJava6R_SpheroidType_Title}</summary>
    public enum SpheroidType
    {
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_AIRY_1830_D}</summary>
        SPHEROID_AIRY_1830,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_AIRY_MOD_D}</summary>
        SPHEROID_AIRY_MOD,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_ATS_1977_D}</summary>
        SPHEROID_ATS_1977,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_AUSTRALIAN_D}</summary>
        SPHEROID_AUSTRALIAN,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_BESSEL_1841_D}</summary>
        SPHEROID_BESSEL_1841,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_BESSEL_MOD_D}</summary>
        SPHEROID_BESSEL_MOD,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_BESSEL_NAMIBIA_D}</summary>
        SPHEROID_BESSEL_NAMIBIA,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CHINA_2000_D}</summary>
        SPHEROID_CHINA_2000,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CLARKE_1858_D}</summary>
        SPHEROID_CLARKE_1858,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CLARKE_1866_D}</summary>
        SPHEROID_CLARKE_1866,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CLARKE_1866_MICH_D}</summary>
        SPHEROID_CLARKE_1866_MICH,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CLARKE_1880_D}</summary>
        SPHEROID_CLARKE_1880,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CLARKE_1880_ARC_D}</summary>
        SPHEROID_CLARKE_1880_ARC,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CLARKE_1880_BENOIT_D}</summary>
        SPHEROID_CLARKE_1880_BENOIT,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CLARKE_1880_IGN_D}</summary>
        SPHEROID_CLARKE_1880_IGN,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CLARKE_1880_RGS_D}</summary>
        SPHEROID_CLARKE_1880_RGS,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_CLARKE_1880_SGA_D}</summary>
        SPHEROID_CLARKE_1880_SGA,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_EVEREST_1830_D}</summary>
        SPHEROID_EVEREST_1830,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_EVEREST_DEF_1967_D}</summary>
        SPHEROID_EVEREST_DEF_1967,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_EVEREST_DEF_197_D}</summary>
        SPHEROID_EVEREST_DEF_197,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_EVEREST_MOD_D}</summary>
        SPHEROID_EVEREST_MOD,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_EVEREST_MOD_1969_D}</summary>
        SPHEROID_EVEREST_MOD_1969,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_FISCHER_1960_D}</summary>
        SPHEROID_FISCHER_1960,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_FISCHER_1968_D}</summary>
        SPHEROID_FISCHER_1968,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_FISCHER_MOD_D}</summary>
        SPHEROID_FISCHER_MOD,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_GEM_10C_D}</summary>
        SPHEROID_GEM_10C,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_GRS_1967_D}</summary>
        SPHEROID_GRS_1967,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_GRS_1980_D}</summary>
        SPHEROID_GRS_1980,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_HELMERT_1906_D}</summary>
        SPHEROID_HELMERT_1906,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_HOUGH_1960_D}</summary>
        SPHEROID_HOUGH_1960,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_INDONESIAN_D}</summary>
        SPHEROID_INDONESIAN,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_INTERNATIONAL_1924_D}</summary>
        SPHEROID_INTERNATIONAL_1924,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_INTERNATIONAL_1967_D}</summary>
        SPHEROID_INTERNATIONAL_1967,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_INTERNATIONAL_1975_D}</summary>
        SPHEROID_INTERNATIONAL_1975,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_KRASOVSKY_1940_D}</summary>
        SPHEROID_KRASOVSKY_1940,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_NWL_10D_D}</summary>
        SPHEROID_NWL_10D,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_NWL_9D_D}</summary>
        SPHEROID_NWL_9D,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_OSU_86F_D}</summary>
        SPHEROID_OSU_86F,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_OSU_91A_D}</summary>
        SPHEROID_OSU_91A,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_PLESSIS_1817_D}</summary>
        SPHEROID_PLESSIS_1817,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_SPHERE_D}</summary>
        SPHEROID_SPHERE,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_SPHERE_AI_D}</summary>
        SPHEROID_SPHERE_AI,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_STRUVE_1860_D}</summary>
        SPHEROID_STRUVE_1860,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_USER_DEFINED_D}</summary>
        SPHEROID_USER_DEFINED,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_WALBECK_D}</summary>
        SPHEROID_WALBECK,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_WAR_OFFICE_D}</summary>
        SPHEROID_WAR_OFFICE,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_WGS_1966_D}</summary>
        SPHEROID_WGS_1966,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_WGS_1972_D}</summary>
        SPHEROID_WGS_1972,
        /// <summary>${iServerJava6R_SpheroidType_attribute_SPHEROID_WGS_1984_D}</summary>
        SPHEROID_WGS_1984
    }
}
