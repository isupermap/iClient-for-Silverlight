﻿

using System.Json;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_UserInfo_Title}</para>
    /// 	<para>${iServerJava6R_UserInfo_Description}</para>
    /// </summary>
    public class UserInfo
    {
        /// <summary>${iServerJava6R_UserInfo_constructor_D}</summary>
        public UserInfo()
        { }
        /// <summary>${iServerJava6R_UserInfo_attribute_UserID_D}</summary>
        public string UserID { get; set; }


        internal static UserInfo FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new UserInfo { UserID = (string)json["userID"] };
        }
    }
}
