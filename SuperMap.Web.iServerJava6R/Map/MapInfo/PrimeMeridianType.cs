﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>${iServerJava6R_PrimeMeridianType_Title}</summary>
    public enum PrimeMeridianType
    {
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_ATHENS_D}</summary>
        PRIMEMERIDIAN_ATHENS,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_BERN_D}</summary>
        PRIMEMERIDIAN_BERN,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_BOGOTA_D}</summary>
        PRIMEMERIDIAN_BOGOTA,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_BRUSSELS_D}</summary>
        PRIMEMERIDIAN_BRUSSELS,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_FERRO_D}</summary>
        PRIMEMERIDIAN_FERRO,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_GREENWICH_D}</summary>
        PRIMEMERIDIAN_GREENWICH,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_JAKARTA_D}</summary>
        PRIMEMERIDIAN_JAKARTA,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_LISBON_D}</summary>
        PRIMEMERIDIAN_LISBON,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_MADRID_D}</summary>
        PRIMEMERIDIAN_MADRID,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_PARIS_D}</summary>
        PRIMEMERIDIAN_PARIS,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_ROME_D}</summary>
        PRIMEMERIDIAN_ROME,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_STOCKHOLM_D}</summary>
        PRIMEMERIDIAN_STOCKHOLM,
        /// <summary>${iServerJava6R_PrimeMeridianType_attribute_PRIMEMERIDIAN_PRIMEMERIDIAN_USER_DEFINED_D}</summary>
        PRIMEMERIDIAN_USER_DEFINED
    }
}
