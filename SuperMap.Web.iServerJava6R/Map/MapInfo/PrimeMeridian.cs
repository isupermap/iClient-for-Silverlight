﻿

using System.Json;
using System;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_PrimeMeridian_Title}</para>
    /// 	<para>${iServerJava6R_PrimeMeridian_Description}</para>
    /// </summary>
    public class PrimeMeridian
    {
        /// <summary>${iServerJava6R_PrimeMeridian_constructor_D}</summary>
        public PrimeMeridian()
        { }
        /// <summary>${iServerJava6R_PrimeMeridian_attribute_Name_D}</summary>
        public string Name { get; set; }
        /// <summary>${iServerJava6R_PrimeMeridian_attribute_LongitudeValue_D}</summary>
        public double LongitudeValue { get; set; }
        /// <summary>${iServerJava6R_PrimeMeridian_attribute_Type_D}</summary>
        public PrimeMeridianType Type { get; set; }

        internal static PrimeMeridian FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new PrimeMeridian
            {
                Name = (string)json["name"],
                LongitudeValue = (double)json["longitudeValue"],
                Type = (PrimeMeridianType)Enum.Parse(typeof(PrimeMeridianType),
                (string)json["type"], true)
            };
        }
    }
}
