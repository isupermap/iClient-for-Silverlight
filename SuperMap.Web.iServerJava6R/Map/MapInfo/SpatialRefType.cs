﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_SpatialRefType_Title}</para>
    /// 	<para>${iServerJava6R_SpatialRefType_Description}</para>
    /// </summary>
    public enum SpatialRefType
    {
        /// <summary>${iServerJava6R_SpatialRefType_attribute_SPATIALREF_EARTH_LONGITUDE_LATITUDE_D}</summary>
        SPATIALREF_EARTH_LONGITUDE_LATITUDE,
        /// <summary>${iServerJava6R_SpatialRefType_attribute_SPATIALREF_EARTH_PROJECTION_D}</summary>
        SPATIALREF_EARTH_PROJECTION,
        /// <summary>${iServerJava6R_SpatialRefType_attribute_SPATIALREF_NONEARTH_D}</summary>
        SPATIALREF_NONEARTH
    }
}
