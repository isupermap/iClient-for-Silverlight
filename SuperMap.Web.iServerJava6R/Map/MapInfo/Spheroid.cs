﻿

using System.Json;
using System;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Spheroid_Title}</para>
    /// 	<para>${iServerJava6R_Spheroid_Description}</para>
    /// </summary>
    public class Spheroid
    {
        /// <summary>${iServerJava6R_Spheroid_constructor_D}</summary>
        public Spheroid()
        { }
        /// <summary>${iServerJava6R_Spheroid_attribute_Axis_D}</summary>
        public double Axis { get; set; }
        /// <summary>${iServerJava6R_Spheroid_attribute_Flatten_D}</summary>
        public double Flatten { get; set; }
        /// <summary>${iServerJava6R_Spheroid_attribute_Name_D}</summary>
        public string Name { get; set; }
        /// <summary>${iServerJava6R_Spheroid_attribute_Type_D}</summary>
        public SpheroidType Type { get; set; }

        internal static Spheroid FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new Spheroid
            {
                Axis = (double)json["axis"],
                Flatten = (double)json["flatten"],
                Name = (string)json["name"],
                Type = (SpheroidType)Enum.Parse(typeof(SpheroidType),
                (string)json["type"], true)
            };
        }
    }
}
