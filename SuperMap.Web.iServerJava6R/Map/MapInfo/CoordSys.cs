﻿

using System.Json;
using System;
using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>${iServerJava6R_CoordSys_Title}</summary>
    public class CoordSys
    {
        /// <summary>${iServerJava6R_CoordSys_constructor_D}</summary>
        public CoordSys()
        { }
        /// <summary>${iServerJava6R_CoordSys_attribute_Unit_D}</summary>
        public Unit Unit { get; set; }
        /// <summary>${iServerJava6R_CoordSys_attribute_SpatialRefType_D}</summary>
        public SpatialRefType SpatialRefType { get; set; }
        /// <summary>${iServerJava6R_CoordSys_attribute_PrimeMeridian_D}</summary>
        public PrimeMeridian PrimeMeridian { get; set; }
        /// <summary>${iServerJava6R_CoordSys_attribute_Name_D}</summary>
        public string Name { get; set; }
        /// <summary>${iServerJava6R_CoordSys_attribute_Type_D}</summary>
        public CoordSysType Type { get; set; }
        /// <summary>${iServerJava6R_CoordSys_attribute_Datum_D}</summary>
        public Datum Datum { get; set; }


        internal static CoordSys FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new CoordSys
            {
                Unit = (Unit)Enum.Parse(typeof(Unit), (string)json["unit"], true),
                SpatialRefType = (SpatialRefType)Enum.Parse(typeof(SpatialRefType), (string)json["spatialRefType"], true),
                PrimeMeridian = PrimeMeridian.FromJson((JsonObject)json["primeMeridian"]),
                Name = (string)json["name"],
                Type = (CoordSysType)Enum.Parse(typeof(CoordSysType), (string)json["type"], true),
                Datum = Datum.FromJson((JsonObject)json["datum"]),
            };
        }
    }
}
