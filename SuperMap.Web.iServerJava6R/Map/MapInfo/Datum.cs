﻿

using System.Json;
using System;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Datum_Title}</para>
    /// 	<para>${iServerJava6R_Datum_Description}</para>
    /// </summary>
    public class Datum
    {
        /// <summary>${iServerJava6R_Datum_constructor_D}</summary>
        public Datum()
        { }
        /// <summary>${iServerJava6R_Datum_attribute_Name_D}</summary>
        public string Name { get; set; }
        /// <summary>${iServerJava6R_Datum_attribute_Type_D}</summary>
        public DatumType Type { get; set; }
        /// <summary>${iServerJava6R_Datum_attribute_Spheroid_D}</summary>
        public Spheroid Spheroid { get; set; }

        internal static Datum FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new Datum
            {
                Name = (string)json["name"],
                Type = (DatumType)Enum.Parse(typeof(DatumType),
                (string)json["type"], true),
                Spheroid = Spheroid.FromJson((JsonObject)json["spheroid"])
            };
        }

    }
}
