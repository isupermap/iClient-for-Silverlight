﻿

using System.Json;
using System;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Projection_Title}</para>
    /// 	<para>${iServerJava6R_Projection_Description}</para>
    /// </summary>
    public class Projection
    {
        /// <summary>${iServerJava6R_Projection_constructor_D}</summary>
        public Projection()
        { }
        /// <summary>${iServerJava6R_Projection_attribute_Name_D}</summary>
        public string Name { get; set; }
        /// <summary>${iServerJava6R_Projection_attribute_Type_D}</summary>
        public ProjectionType Type { get; set; }


        internal static Projection FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new Projection
            {
                Name = (string)json["name"],
                Type = (ProjectionType)Enum.Parse(typeof(ProjectionType),
                (string)json["type"], true)
            };
        }
    }
}
