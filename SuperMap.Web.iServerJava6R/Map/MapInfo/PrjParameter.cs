﻿

using System.Json;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_PrjParameter_Title}</para>
    /// 	<para>${iServerJava6R_PrjParameter_Description}</para>
    /// </summary>
    public class PrjParameter
    {
        /// <summary>${iServerJava6R_PrjParameter_constructor_D}</summary>
        public PrjParameter()
        { }
        /// <summary>${iServerJava6R_PrjParameter_attribute_SecondPointLongitude_D}</summary>
        public double SecondPointLongitude { get; set; }
        /// <summary>${iServerJava6R_PrjParameter_attribute_FirstPointLongitude_D}</summary>
        public double FirstPointLongitude { get; set; }
        /// <summary>${iServerJava6R_PrjParameter_attribute_FalseNorthing_D}</summary>
        public double FalseNorthing { get; set; }
        /// <summary>${iServerJava6R_PrjParameter_attribute_SecondStandardParallel_D}</summary>
        public double SecondStandardParallel { get; set; }
        /// <summary>${iServerJava6R_PrjParameter_attribute_FirstStandardParallel_D}</summary>
        public double FirstStandardParallel { get; set; }
        /// <summary>${iServerJava6R_PrjParameter_attribute_CentralMeridian_D}</summary>
        public double CentralMeridian { get; set; }
        /// <summary>${iServerJava6R_PrjParameter_attribute_CentralParallel_D}</summary>
        public double CentralParallel { get; set; }
        /// <summary>${iServerJava6R_PrjParameter_attribute_ScaleFactor_D}</summary>
        public double ScaleFactor { get; set; }
        /// <summary>${iServerJava6R_PrjParameter_attribute_Azimuth_D}</summary>
        public double Azimuth { get; set; }
        /// <summary>${iServerJava6R_PrjParameter_attribute_FalseEasting_D}</summary>
        public double FalseEasting { get; set; }

        internal static PrjParameter FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new PrjParameter
            {
                Azimuth = (double)json["azimuth"],
                CentralMeridian = (double)json["centralMeridian"],
                CentralParallel = (double)json["centralParallel"],
                FalseEasting = (double)json["falseEasting"],
                FalseNorthing = (double)json["falseNorthing"],
                FirstStandardParallel = (double)json["firstStandardParallel"],
                FirstPointLongitude = (double)json["firstPointLongitude"],
                ScaleFactor = (double)json["scaleFactor"],
                SecondPointLongitude = (double)json["secondPointLongitude"],
                SecondStandardParallel = (double)json["secondStandardParallel"],
            };
        }
    }
}
