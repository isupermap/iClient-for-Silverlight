﻿
using System;
using System.Json;
using System.Net;
using System.Security;
using SuperMap.Web.Resources;
using SuperMap.Web.Core;

namespace SuperMap.Web.iServerJava6R
{
    internal class SmMapService
    {
        public SmMapService(string mapServiceUrl)
        {
            MapServiceUrl = mapServiceUrl;
        }

        //方法
        public void Initialize(int wkid)
        {
            WebClient wc = new WebClient();
            wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler(wc_DownloadStringCompleted);
            string url = string.Format("{0}.json?", MapServiceUrl);
            if (Credential.CREDENTIAL.GetUrlParameters() != null)
            {
                url+=Credential.CREDENTIAL.GetUrlParameters()+"&";
            }
            wc.DownloadStringAsync(new Uri(url + "prjCoordSys={" + string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"epsgCode\":{0}", wkid) + "}", UriKind.Absolute), this);
        }
        //方法
        public void Initialize()
        {
            WebClient wc = new WebClient();
            wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler(wc_DownloadStringCompleted);
            if (Credential.CREDENTIAL.GetUrlParameters() != null)
            {
                string s = string.Format("{0}.json" +"?"+ Credential.CREDENTIAL.GetUrlParameters(), MapServiceUrl);
                wc.DownloadStringAsync(new Uri(s, UriKind.Absolute), this);
            }
            else {
                wc.DownloadStringAsync(new Uri(string.Format("{0}.json", MapServiceUrl), UriKind.Absolute), this);
            }
           
        }

        private void wc_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            Exception ex = null;
            if (!this.CheckForFault(e, out ex))
            {
                JsonObject json = (JsonObject)JsonObject.Parse(e.Result);
                MapServiceInfo = SmMapServiceInfo.FromJson(json);

                if ((this.Initialized != null) && (this.MapServiceInfo != null))
                {
                    this.Initialized(this, new MapServiceInitalizeArgs { MapService = this });
                }
            }
        }

        private bool CheckForFault(DownloadStringCompletedEventArgs e, out Exception ex)
        {
            if (e.Cancelled)
            {
                if (this.Failed != null)
                {
                    this.Failed(this, new MapServiceFaultEventArgs { Cancelled = true });
                }
                ex = null;
                return true;
            }
            if (e.Error != null)
            {
                Exception inner = e.Error;
                if (this.Failed != null)
                {
                    if (inner is SecurityException)
                    {
                        inner = new SecurityException(SuperMap.Web.Resources.ExceptionStrings.PolicyFileAvailable, inner);
                    }
                    this.Failed(this, new MapServiceFaultEventArgs { Error = inner });
                }
                ex = inner;
                return true;
            }
            if (e.Result == "null" || String.IsNullOrEmpty(e.Result))
            {
                Exception inner = null;
                if (this.Failed != null)
                {
                    inner = new Exception(ExceptionStrings.ServiceStarted);
                    this.Failed(this, new MapServiceFaultEventArgs { Error = inner });
                }
                ex = inner;
                return true;
            }
            ex = null;
            return false;
        }

        //属性
        public SmMapServiceInfo MapServiceInfo { get; private set; }
        public string MapServiceUrl { get; private set; }

        //事件
        internal class MapServiceInitalizeArgs : EventArgs
        {
            public SmMapService MapService { get; set; }
        }
        internal event EventHandler<MapServiceInitalizeArgs> Initialized;

        internal class MapServiceFaultEventArgs
        {
            public bool Cancelled { get; internal set; }

            public Exception Error { get; internal set; }
        }
        internal delegate void MapServiceFaultEventHandler(object sender, MapServiceFaultEventArgs args);
        internal event MapServiceFaultEventHandler Failed;
    }
}
