﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperMap.Web.Mapping
{
    internal interface IClearTheme
    {
        void ClearTheme();
    }
}
