﻿using System;
using System.Json;
using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R
{
    internal class SmMapServiceInfo
    {
        public double Scale { get; set; }
        public Rectangle2D ViewBounds { get; set; }
        public Rect Viewer { get; set; }
        public Rectangle2D Bounds { get; set; }
        public PrjCoordSys PrjCoordSys { get; set; }
        public Unit CoordUnit { get; set; }

        internal static SmMapServiceInfo FromJson(JsonObject json)
        {
            if (json == null) return null;

            if (!json.ContainsKey("name") || !json.ContainsKey("bounds")) return null;

            return new SmMapServiceInfo
            {
                ViewBounds = JsonHelper.ToRectangle2D((JsonObject)json["viewBounds"]),
                CoordUnit = (Unit)Enum.Parse(typeof(Unit), (string)json["coordUnit"], true),
                Bounds = JsonHelper.ToRectangle2D((JsonObject)json["bounds"]),
                PrjCoordSys = PrjCoordSys.FromJson((JsonObject)json["prjCoordSys"]),
                Scale = (double)json["scale"],
                Viewer = JsonHelper.ToRect((JsonObject)json["viewer"])
            };
        }
    }
}
