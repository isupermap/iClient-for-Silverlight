﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SuperMap.Web.Controls
{
    internal static class CollectionExtensions
    {
        //// Methods
        public static IEnumerable<T> Descendants<T>(this IEnumerable<T> enumerable, Func<T, IEnumerable<T>> childrenDelegate)
        {
            if (enumerable == null)
                yield break;

            foreach (T item in enumerable)
            {
                yield return item;

                IEnumerable<T> children = childrenDelegate(item);
                if (children != null)
                {
                    foreach (T child in children.Descendants(childrenDelegate))
                        yield return child;
                }
            }
        }

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            if (enumerable != null)
            {
                foreach (T local in enumerable)
                {
                    action(local);
                }
            }
        }

        public static IEnumerable<T> Leaves<T>(this IEnumerable<T> enumerable, Func<T, IEnumerable<T>> childrenDelegate)
        {
            if (enumerable == null)
                yield break;

            foreach (T item in enumerable)
            {

                IEnumerable<T> children = childrenDelegate(item);
                if (children == null)
                {
                    // it's a leaf
                    yield return item;
                }
                else
                {
                    // not a leaf ==> recursive call
                    foreach (T child in children.Leaves(childrenDelegate))
                        yield return child;
                }
            }
        }

        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> enumerable)
        {
            ObservableCollection<T> observables = new ObservableCollection<T>();
            foreach (T local in enumerable)
            {
                observables.Add(local);
            }
            return observables;
        }
    }
}
