﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace SuperMap.Web.Controls
{
    /// <summary>
    /// 	<para>${controls_LegendItemInfo_Title}</para>
    /// 	<para>${controls_LegendItemInfo_Description}</para>
    /// </summary>
    public class LegendItemInfo : INotifyPropertyChanged     //表示图例的一个项，由可见性，图片，名称组成。
    {
        /// <summary>${controls_LegendItemInfo_constructor_None_D}</summary>
        public LegendItemInfo()
        {
            isVisible = true;
        }

        private string description;
        /// <summary>${controls_LegendItemInfo_attribute__Description_D}</summary>
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                RaisePropertyChanged("Description");
            }
        }

        private ImageSource imageSource;
        /// <summary>${controls_LegendItemInfo_attribute_ImageSource_D}</summary>
        public ImageSource ImageSource
        {
            get
            {
                return imageSource;
            }
            set
            {
                imageSource = value;
                RaisePropertyChanged("ImageSource");
            }
        }

        private bool isVisible;
        /// <summary>${controls_LegendItemInfo_attribute_IsVisible_D}</summary>
        public bool IsVisible
        {
            get
            {
                return isVisible;
            }
            set
            {
                isVisible = value;
                RaisePropertyChanged("IsVisible");
            }
        }

        private string name;
        /// <summary>${controls_LegendItemInfo_attribute_Name_D}</summary>
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        private ObservableCollection<LegendItemInfo> layerItems;
        /// <summary>${controls_LegendItemInfo_attribute_LayerItems_D}</summary>
        public ObservableCollection<LegendItemInfo> LayerItems //表示图例的多个子项，比如对应服务器中Layer里的多个子图层，或者专题图子项。
        {
            get
            {
                return layerItems;
            }
            set
            {
                layerItems = value;
                RaisePropertyChanged("LayerItems");
            }
        }

        //绑定模板
        private DataTemplate template;
        /// <summary>${controls_LegendItemInfo_attribute_Template_D}</summary>
        public DataTemplate Template
        {
            get { return template; }
            set
            {
                template = value;
                RaisePropertyChanged("Template");
            }
        }

        internal LegendTree LegendTree;
        internal virtual void Attach(LegendTree legendTree)
        {
            LegendTree = legendTree;
            if (LegendTree != null)
            {
                this.template = GetTemplate();
            }
        }

        internal virtual void Detach()
        {
            LegendTree = null;
        }

        internal virtual DataTemplate GetTemplate()
        {
            return LegendTree.LegendItemTemplate == null ? null : LegendTree.LegendItemTemplate;
        }
        /// <summary>${controls_LegendItemInfo_event_PropertyChanged_D}</summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>${controls_LegendItemInfo_method_RaisePropertyChanged_D}</summary>
        protected void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private bool isExpanded = true;
        /// <summary>${controls_LegendItemInfo_attribute_IsExpanded_D}</summary>
        public bool IsExpanded
        {
            get 
            {
                return isExpanded; 
            }
            set 
            { 
                isExpanded = value;
                RaisePropertyChanged("IsExpanded");
            }
        }
    }
}
