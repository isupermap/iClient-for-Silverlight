﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Mapping;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Controls
{
    /// <summary>
    /// 	<para>${controls_LegendiServer6_Title}</para>
    /// 	<para>${controls_LegendiServer6_Description}</para>
    /// </summary>
    public sealed class Legend : Control, INotifyPropertyChanged
    {
        // Fields
        private LegendTree _legendTree;
        /// <summary>${controls_LegendiServer6_attribute__LayerIDsProperty_D}</summary>
        public static readonly DependencyProperty LayerIDsProperty = DependencyProperty.Register("LayerIDs", typeof(string[]), typeof(Legend), new PropertyMetadata(new PropertyChangedCallback(Legend.OnLayerIDsPropertyChanged)));
        /// <summary>${controls_LegendiServer6_attribute_LayerItemsProperty_D}</summary>
        public static readonly DependencyProperty LayerItemsProperty = DependencyProperty.Register("LayerItems", typeof(ObservableCollection<LegendItemInfo>), typeof(Legend), null);
        /// <summary>${controls_LegendiServer6_attribute_LayerTemplateProperty_D}</summary>
        public static readonly DependencyProperty LayerTemplateProperty = DependencyProperty.Register("LayerTemplate", typeof(DataTemplate), typeof(Legend), new PropertyMetadata(new PropertyChangedCallback(Legend.OnLayerTemplateChanged)));
        /// <summary>${controls_LegendiServer6_attribute_LegendItemTemplateProperty_D}</summary>
        public static readonly DependencyProperty LegendItemTemplateProperty = DependencyProperty.Register("LegendItemTemplate", typeof(DataTemplate), typeof(Legend), new PropertyMetadata(new PropertyChangedCallback(Legend.OnLegendItemTemplateChanged)));
        /// <summary>${controls_LegendiServer6_attribute_MapLayerTemplateProperty_D}</summary>
        public static readonly DependencyProperty MapLayerTemplateProperty = DependencyProperty.Register("MapLayerTemplate", typeof(DataTemplate), typeof(Legend), new PropertyMetadata(new PropertyChangedCallback(Legend.OnMapLayerTemplateChanged)));
        /// <summary>${controls_LegendiServer6_attribute_MapProperty_D}</summary>
        public static readonly DependencyProperty MapProperty = DependencyProperty.Register("Map", typeof(Map), typeof(Legend), new PropertyMetadata(new PropertyChangedCallback(Legend.OnMapPropertyChanged)));
        /// <summary>${controls_LegendiServer6_attribute_IsShowOnlyVisibleLayersProperty_D}</summary>
        public static readonly DependencyProperty IsShowOnlyVisibleLayersProperty = DependencyProperty.Register("ShowOnlyVisibleLayers", typeof(bool), typeof(Legend), new PropertyMetadata(new PropertyChangedCallback(OnIsShowOnlyVisibleLayersPropertyChanged)));


        // Methods
        /// <summary>${controls_LegendiServer6_constructor_None_D}</summary>
        public Legend()
        {
            base.DefaultStyleKey = typeof(Legend);
            this._legendTree = new LegendTree();
            this._legendTree.PropertyChanged += _legendTree_PropertyChanged;
        }

        private void LayerItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems.Count < 1)  //清除数据
            {
                this.LayerItems = null;
            }
            else   //集合更改，即通知数据源变化
            {
                ObservableCollection<LegendItemInfo> col = new ObservableCollection<LegendItemInfo>();
                for (int i = 0; i < _legendTree.LayerItems.Count; i++)
                {
                    col.Add((LegendItemInfo)_legendTree.LayerItems[i]);
                }
                this.LayerItems = col;
            }
        }

        private void _legendTree_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "LayerItems")
            {
                ObservableCollection<LegendItemInfo> col = new ObservableCollection<LegendItemInfo>();
                for (int i = 0; i < _legendTree.LayerItems.Count; i++)
                {
                    col.Add((LegendItemInfo)_legendTree.LayerItems[i]);
                }
                this.LayerItems = col;
            }
        }
        /// <summary>${controls_LegendiServer6_method_OnApplyTemplate_D}</summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        private static void OnLayerIDsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Legend).OnLayerIDsPropertyChanged((string[])e.OldValue, (string[])e.NewValue);
        }

        private void OnLayerIDsPropertyChanged(string[] oldLayerIDs, string[] newLayerIDs)
        {
            this._legendTree.LayerIDs = newLayerIDs;
        }

        private void OnLayerTemplateChanged(DataTemplate oldDataTemplate, DataTemplate newDataTemplate)
        {
            this._legendTree.LayerTemplate = newDataTemplate;
        }

        private static void OnLayerTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Legend).OnLayerTemplateChanged(e.OldValue as DataTemplate, e.NewValue as DataTemplate);
        }

        private void OnLegendItemTemplateChanged(DataTemplate oldDataTemplate, DataTemplate newDataTemplate)
        {
            this._legendTree.LegendItemTemplate = newDataTemplate;
        }

        private static void OnLegendItemTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Legend).OnLegendItemTemplateChanged(e.OldValue as DataTemplate, e.NewValue as DataTemplate);
        }

        private void OnMapLayerTemplateChanged(DataTemplate oldDataTemplate, DataTemplate newDataTemplate)
        {
            this._legendTree.MapLayerTemplate = newDataTemplate;
        }

        private static void OnMapLayerTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Legend).OnMapLayerTemplateChanged(e.OldValue as DataTemplate, e.NewValue as DataTemplate);
        }

        private void OnMapPropertyChanged(Map oldMap, Map newMap)
        {
            if (DesignerProperties.GetIsInDesignMode(this))
            {
                if ((newMap == null) || (newMap.Layers == null))
                {
                    return;
                }
                if (newMap.Layers.Count == 0)
                {
                    return;
                }
            }
            this._legendTree.Map = newMap;
        }

        private static void OnMapPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Legend).OnMapPropertyChanged(e.OldValue as Map, e.NewValue as Map);
        }

        private void OnIsShowOnlyVisibleLayersPropertyChanged(bool oldValue, bool newValue)
        {
            this._legendTree.IsShowOnlyVisibleLayers = newValue;
        }

        private static void OnIsShowOnlyVisibleLayersPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Legend).OnIsShowOnlyVisibleLayersPropertyChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        // Properties
        /// <summary>${controls_LegendiServer6_attribute_LayerIDs_D}</summary>
        [TypeConverter(typeof(StringArrayConverter))]
        public string[] LayerIDs
        {
            get
            {
                return (string[])base.GetValue(LayerIDsProperty);
            }
            set
            {
                base.SetValue(LayerIDsProperty, value);
                OnPropertyChanged("LayerIDs");
            }
        }
        /// <summary>${controls_LegendiServer6_attribute_LayerItems_D}</summary>
        public ObservableCollection<LegendItemInfo> LayerItems
        {
            get
            {
                return (ObservableCollection<LegendItemInfo>)base.GetValue(LayerItemsProperty);
            }
            internal set
            {
                base.SetValue(LayerItemsProperty, value);
                OnPropertyChanged("LayerItems");
            }
        }
        /// <summary>${controls_LegendiServer6_attribute_LayerTemplate_D}</summary>
        public DataTemplate LayerTemplate
        {
            get
            {
                return (DataTemplate)base.GetValue(LayerTemplateProperty);
            }
            set
            {
                base.SetValue(LayerTemplateProperty, value);
                OnPropertyChanged("LayerTemplate");
            }
        }
        /// <summary>${controls_LegendiServer6_attribute_LegendItemTemplate_D}</summary>
        public DataTemplate LegendItemTemplate
        {
            get
            {
                return (DataTemplate)base.GetValue(LegendItemTemplateProperty);
            }
            set
            {
                base.SetValue(LegendItemTemplateProperty, value);
                OnPropertyChanged("LegendItemTemplate");
            }
        }
        /// <summary>${controls_LegendiServer6_attribute_Map_D}</summary>
        public Map Map
        {
            get
            {
                return (base.GetValue(MapProperty) as Map);
            }
            set
            {
                base.SetValue(MapProperty, value);
            }
        }
        /// <summary>${controls_LegendiServer6_attribute_MapLayerTemplate_D}</summary>
        public DataTemplate MapLayerTemplate
        {
            get
            {
                return (DataTemplate)base.GetValue(MapLayerTemplateProperty);
            }
            set
            {
                base.SetValue(MapLayerTemplateProperty, value);
                OnPropertyChanged("MapLayerTemplate");
            }
        }

        /// <summary>${controls_LegendiServer6_attribute_IsShowOnlyVisibleLayers_D}</summary>
        public bool IsShowOnlyVisibleLayers  //true 只显示可见的Layers
        {
            get
            {
                return (bool)base.GetValue(IsShowOnlyVisibleLayersProperty);
            }
            set
            {
                base.SetValue(IsShowOnlyVisibleLayersProperty, value);
                OnPropertyChanged("IsShowOnlyVisibleLayers");
            }
        }
        /// <summary>${controls_LegendiServer6_attribute_SourceImageWidth_D}</summary>
        public double SourceImageWidth
        {
            get { return (double)GetValue(SourceImageWidthProperty); }
            set { SetValue(SourceImageWidthProperty, value); }
        }

        private static readonly DependencyProperty SourceImageWidthProperty =
            DependencyProperty.Register("SourceImageWidth", typeof(double), typeof(Legend), new PropertyMetadata(new PropertyChangedCallback(Legend.OnSourceImageWidthPropertyChanged)));

        private static void OnSourceImageWidthPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Legend).OnSourceImageWidthPropertyChanged((double)e.OldValue, (double)e.NewValue);
        }

        private void OnSourceImageWidthPropertyChanged(double oldValue, double newValue)
        {
            _legendTree.SourceImageWidth = newValue;
        }
        /// <summary>${controls_LegendiServer6_attribute_SourceImageHeight_D}</summary>
        public double SourceImageHeight
        {
            get { return (double)GetValue(SourceImageHeightProperty); }
            set { SetValue(SourceImageHeightProperty, value); }
        }

        private static readonly DependencyProperty SourceImageHeightProperty =
            DependencyProperty.Register("SourceImageHeight", typeof(double), typeof(Legend), new PropertyMetadata(new PropertyChangedCallback(Legend.OnSourceImageHeightPropertyChanged)));

        private static void OnSourceImageHeightPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Legend).OnSourceImageHeightPropertyChanged((double)e.OldValue, (double)e.NewValue);
        }

        private void OnSourceImageHeightPropertyChanged(double oldValue, double newValue)
        {
            _legendTree.SourceImageHeight = newValue;
        }
        /// <summary>${controls_LegendiServer6_attribute_IsExpanded_D}</summary>
        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsExpanded.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty IsExpandedProperty =
            DependencyProperty.Register("IsExpanded", typeof(bool), typeof(Legend), new PropertyMetadata((bool)true, new PropertyChangedCallback(OnIsExpandedPropertyChanged)));

        private static void OnIsExpandedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as Legend).OnIsExpandedPropertyChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        private void OnIsExpandedPropertyChanged(bool oldValue, bool newValue)
        {
            _legendTree.IsExpanded = IsExpanded;
        }
        /// <summary>${controls_LegendiServer6_method_PropertyChanged_D}</summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
