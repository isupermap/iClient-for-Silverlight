﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using SuperMap.Web.Mapping;
using System.Reflection;
using System;

namespace SuperMap.Web.Controls
{
    internal sealed class LegendTree : MapLegendItemInfo
    {
        private IEnumerable<string> _layerIDs;
        private DataTemplate _layerTemplate;
        private DataTemplate _legendItemTemplate;
        private Map _map;
        private DataTemplate _mapLayerTemplate;
        private LayerCollection _oldLayers;

        public LegendTree()
        {
            Attach(this);
            SourceImageWidth = 16;
            SourceImageHeight = 16;
        }

        private IEnumerable<Layer> GetLayers(IEnumerable<string> ids, Map map)
        {
            if (map != null && map.Layers != null)
            {
                if (ids != null)
                {
                    foreach (string item in ids)
                    {
                        if (!string.IsNullOrEmpty(item) && map.Layers[item] != null && map.Layers[item].IsVisible == true)
                            yield return map.Layers[item];
                    }
                }
                else
                {
                    foreach (Layer layer in map.Layers)
                    {
                        if (layer.IsVisible == true && ((layer is TiledDynamicRESTLayer) || (layer is DynamicRESTLayer)))//只要iServer6R的rest图层，如果有缓存图层了也要加上。
                        {
                            yield return layer;
                        }
                    }
                }
            }
        }

        private void Layers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.UpdateMapLayerItems();
        }

        private void Map_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Layers")
            {
                if (this._oldLayers != null)
                {
                    this._oldLayers.CollectionChanged -= new NotifyCollectionChangedEventHandler(this.Layers_CollectionChanged);
                    DetachLayerEvent(this._oldLayers);
                }
                this._oldLayers = (sender as Map).Layers;
                if (this._oldLayers != null)
                {
                    this._oldLayers.CollectionChanged += new NotifyCollectionChangedEventHandler(this.Layers_CollectionChanged);
                    AttachLayerEvent(this._oldLayers);
                }
                this.UpdateMapLayerItems();
            }
        }

        private void AttachLayerEvent(LayerCollection layers)
        {
            foreach (var layer in layers)
            {
                layer.PropertyChanged += new PropertyChangedEventHandler(layer_PropertyChanged);
            }
        }

        private void DetachLayerEvent(LayerCollection layers)
        {
            foreach (var layer in layers)
            {
                layer.PropertyChanged -= new PropertyChangedEventHandler(layer_PropertyChanged);
            }
        }

        void layer_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsVisible")
            {
                UpdateMapLayerItems();
            }
        }

        private void UpdateMapLayerItems()
        {
            ObservableCollection<MapLegendItemInfo> observables = new ObservableCollection<MapLegendItemInfo>();
            IEnumerable<Layer> layers = GetLayers(this.LayerIDs, this.Map);
            foreach (Layer layer in layers)
            {
                MapLegendItemInfo mapLegendItemInfo = new MapLegendItemInfo(layer, this.Map, isShowOnlyVisibleLayers, SourceImageHeight, SourceImageWidth, IsExpanded, (mapLegent =>
                {
                    observables.Add(mapLegent);
                    mapLegent.Attach(this);
                    if (observables.Count == layers.Count())
                    {
                        this.LayerItems = GetLegendItemInfo<MapLegendItemInfo>(observables);
                    }
                }));
            }

            this.LayerItems = GetLegendItemInfo(observables);
        }

        private ObservableCollection<LegendItemInfo> GetLegendItemInfo<T>(ObservableCollection<T> col) where T : LegendItemInfo
        {
            ObservableCollection<LegendItemInfo> coll = new ObservableCollection<LegendItemInfo>();
            foreach (LegendItemInfo item in col)
            {
                coll.Add(item);
            }

            return coll;
        }

        private void RefreshTemplate()
        {
            this.LayerItems.ForEach((legendItem) =>
            {
                ((MapLegendItemInfo)legendItem).Attach(this);
            });
        }

        internal IEnumerable<string> LayerIDs
        {
            get
            {
                return this._layerIDs;
            }
            set
            {
                if (this._layerIDs != value)
                {
                    this._layerIDs = value;
                    this.UpdateMapLayerItems();
                }
            }
        }

        internal DataTemplate LayerTemplate
        {
            get
            {
                return this._layerTemplate;
            }
            set
            {
                if (this._layerTemplate != value)
                {
                    this._layerTemplate = value;
                    RefreshTemplate();
                }
            }
        }

        internal DataTemplate LegendItemTemplate
        {
            get
            {
                return this._legendItemTemplate;
            }
            set
            {
                if (this._legendItemTemplate != value)
                {
                    this._legendItemTemplate = value;
                    RefreshTemplate();
                }
            }
        }

        internal DataTemplate MapLayerTemplate
        {
            get
            {
                return this._mapLayerTemplate;
            }
            set
            {
                if (this._mapLayerTemplate != value)
                {
                    this._mapLayerTemplate = value;
                    RefreshTemplate();
                }
            }
        }

        internal Map Map
        {
            get
            {
                return this._map;
            }
            set
            {
                if (this._map != value)
                {
                    if (this._map != null)
                    {
                        this._map.PropertyChanged -= new PropertyChangedEventHandler(this.Map_PropertyChanged);
                        if (this._map.Layers != null)
                        {
                            this._map.Layers.CollectionChanged -= new NotifyCollectionChangedEventHandler(this.Layers_CollectionChanged);
                            this.DetachLayerEvent(this._map.Layers);
                        }
                    }
                    this._map = value;
                    if (this._map != null)
                    {
                        this._map.PropertyChanged += new PropertyChangedEventHandler(this.Map_PropertyChanged);
                        if (this._map.Layers != null)
                        {
                            this._map.Layers.CollectionChanged += new NotifyCollectionChangedEventHandler(this.Layers_CollectionChanged);
                            this.AttachLayerEvent(this._map.Layers);
                        }
                    }
                    this.UpdateMapLayerItems();
                }
            }
        }
        private bool isShowOnlyVisibleLayers;
        public bool IsShowOnlyVisibleLayers
        {
            get
            {
                return isShowOnlyVisibleLayers;
            }
            set
            {
                if (isShowOnlyVisibleLayers != value)
                {
                    isShowOnlyVisibleLayers = value;
                    UpdateMapLayerItems();
                }
            }
        }

        internal double SourceImageWidth { get; set; }

        internal double SourceImageHeight { get; set; }
    }
}
