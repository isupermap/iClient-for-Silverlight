﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Mapping;

namespace SuperMap.Web.Controls
{
    internal class LayerLegendItemInfo : LegendItemInfo   //服务端的一个图层及其对应的信息
    {
        private ServerLayer serverLayer;
        private Layer iClientLayer;
        private double Height;
        private double Width;

        internal LayerLegendItemInfo()
        {

        }

        public LayerLegendItemInfo(ServerLayer sl, Layer layer, double height, double width, bool expanded)
            : this()
        {
            Height = height;
            Width = width;
            IsExpanded = expanded;
            serverLayer = sl;
            iClientLayer = layer;
            AssignValueToPropertyIgnoreLayerType();
        }

        private void AssignValueToPropertyIgnoreLayerType()
        {
            this.Description = serverLayer.Description;
            this.IsVisible = serverLayer.IsVisible;
            this.Name = serverLayer.Name;
            this.LayerItems = CheckServerLayerType();
        }

        private ObservableCollection<LegendItemInfo> CheckServerLayerType()
        {
            switch (serverLayer.UGCLayerType)
            {
                case SuperMapLayerType.THEME:
                    return ReturnSubItemsFromServerLayerWhenThemeType();
                case SuperMapLayerType.VECTOR:
                case SuperMapLayerType.GRID:
                case SuperMapLayerType.IMAGE:
                    AssignValueToImageSource(new Uri(GetServerLayerImageUri(), UriKind.RelativeOrAbsolute));
                    break;
                case SuperMapLayerType.WFS:
                case SuperMapLayerType.WMS:
                default:
                    return null;
            }

            return null;
        }

        // mapUrl,layerName均对应的是服务端的概念。
        private string GetServerLayerImageUri()
        {
            string realMapUrl;
            string mapName = GetServiceMapName(out realMapUrl);
            return realMapUrl + "/layers/" + ReplaceSharp(serverLayer.Name) + "@@" + ReplaceSharp(mapName) + "/legend.png?width=" + Width + "&height=" + Height;
        }

        // mapUrl,layerName均对应的是服务端的概念。
        private string GetServerLayerItemsImageUri(int index)
        {
            string realMapUrl;
            string mapName = GetServiceMapName(out realMapUrl);
            return realMapUrl + "/layers/" + ReplaceSharp(serverLayer.Name) + "@@" + ReplaceSharp(mapName) + "/items/" + index + "/legend.png?width=" + Width + "&height=" + Height;
        }

        private string GetServiceMapName(out string realMapUrl)
        {
            //http://192.168.11.11:8090/iserver/services/map-world/rest/maps/World Map
            string _mapUrl = iClientLayer.Url;

            if (_mapUrl.EndsWith("/"))
            {
                _mapUrl.TrimEnd('/');
            }

            realMapUrl = _mapUrl;
            _mapUrl = _mapUrl.Substring(_mapUrl.LastIndexOf('/') + 1);
            return _mapUrl;
        }

        private string ReplaceSharp(string str) //替换# use ‘.’
        {
            string sourceStr = str;
            if (sourceStr.Contains("#"))
            {
                return sourceStr.Replace('#', '.');
            }
            else
            {
                return sourceStr;
            }
        }
        private void AssignValueToImageSource(Uri url)
        {
            this.ImageSource = new BitmapImage(url);
        }

        private void AssignValueToImageSourceWhenVectorLayer()
        {
            UGCVectorLayer subLayer = (UGCVectorLayer)serverLayer.UGCLayer;
            switch (serverLayer.DatasetInfo.Type)
            {
                case DatasetType.LINE:
                    this.ImageSource = new System.Windows.Media.Imaging.WriteableBitmap(new Path
                        {
                            Data = new PathGeometry
                            {
                                Figures =
                                {
                                    new PathFigure
                                    {
                                         StartPoint=new Point(5,5),
                                         Segments=
                                         {
                                             new LineSegment{ Point=new Point(20,5)},
                                             new LineSegment{ Point=new Point(5,20)},
                                             new LineSegment{ Point=new Point(20,20)},
                                         }
                                    }
                                }
                            },
                            Width = 24,
                            Height = 24,
                            Stroke = new SolidColorBrush(subLayer.Style.LineColor)
                        }, null);
                    break;

                case DatasetType.POINT:
                    this.ImageSource = new System.Windows.Media.Imaging.WriteableBitmap(new Ellipse
                    {
                        Fill = new SolidColorBrush(subLayer.Style.LineColor),
                        Width = 24,
                        Height = 24
                    }, null);
                    break;

                case DatasetType.REGION:
                    this.ImageSource = new System.Windows.Media.Imaging.WriteableBitmap(new Rectangle
                    {
                        Fill = new SolidColorBrush(subLayer.Style.FillForeColor),
                        Width = 24,
                        Height = 24,
                    }, null);
                    break;

                case DatasetType.TEXT:  //ToDo:文本标志
                    this.ImageSource = new BitmapImage(new Uri("/SuperMap.Web.iServerJava6R;component/Controls/Images/文本矢量图层.png", UriKind.RelativeOrAbsolute));
                    break;
                case DatasetType.POINT3D:
                case DatasetType.LINE3D:
                case DatasetType.LINEM:
                case DatasetType.LINKTABLE:
                case DatasetType.NETWORK:
                case DatasetType.NETWORKPOINT:
                case DatasetType.REGION3D:
                case DatasetType.TABULAR:
                case DatasetType.CAD:
                case DatasetType.GRID:
                case DatasetType.IMAGE:
                case DatasetType.UNDEFINED:
                case DatasetType.WCS:
                case DatasetType.WMS:
                default:
                    break;
            }
        }

        private ObservableCollection<LegendItemInfo> ReturnSubItemsFromServerLayerWhenThemeType()
        {
            UGCThemeLayer subLayer = (UGCThemeLayer)serverLayer.UGCLayer;
            ObservableCollection<LegendItemInfo> items = new ObservableCollection<LegendItemInfo>();

            switch (subLayer.ThemeType)
            {
                case ThemeType.GRADUATEDSYMBOL:  //等级符合的图片
                case ThemeType.DOTDENSITY://点密度的图片
                case ThemeType.GRAPH:   //统计的图片
                    {
                        int index = 0;
                        foreach (ThemeGraphItem graphItem in ((ThemeGraph)subLayer.Theme).Items)
                        {
                            items.Add(
                                new LegendItemInfo
                                {
                                    Description = graphItem.Caption,
                                    Name = graphItem.Caption,
                                    ImageSource = new BitmapImage(new Uri(GetServerLayerItemsImageUri(index), UriKind.RelativeOrAbsolute)),
                                });
                            index++;
                        }
                        break;
                    }
                case ThemeType.LABEL:
                    {
                        if (((ThemeLabel)subLayer.Theme).Items == null || ((ThemeLabel)subLayer.Theme).Items.Count < 1) //统一风格，符合风格，标签矩阵  图片
                        {
                            break;
                        }
                        else    //分段标签专题图
                        {
                            int index = 0;
                            foreach (ThemeLabelItem labelItem in ((ThemeLabel)subLayer.Theme).Items)
                            {
                                items.Add(
                                    new LegendItemInfo
                                    {
                                        Description = labelItem.Caption,
                                        Name = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} - {1}", labelItem.Start, labelItem.End),
                                        ImageSource = new BitmapImage(new Uri(GetServerLayerItemsImageUri(index), UriKind.RelativeOrAbsolute)),
                                        IsVisible = labelItem.Visible

                                    });
                                index++;
                            }
                        }
                        break;
                    }
                case ThemeType.RANGE:
                    {
                        int index = 0;
                        foreach (ThemeRangeItem rangeItem in ((ThemeRange)subLayer.Theme).Items)
                        {
                            items.Add(
                                new LegendItemInfo
                                {
                                    Description = rangeItem.Caption,
                                    Name = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} - {1}", rangeItem.Start, rangeItem.End),
                                    ImageSource = new BitmapImage(new Uri(GetServerLayerItemsImageUri(index), UriKind.RelativeOrAbsolute)),
                                    IsVisible = rangeItem.Visible,

                                });
                            index++;
                        }
                        break;
                    }
                case ThemeType.UNIQUE:
                    {
                        int index = 0;
                        foreach (ThemeUniqueItem uniqueItem in ((ThemeUnique)subLayer.Theme).Items)
                        {
                            items.Add(
                                new LegendItemInfo
                                {
                                    Description = uniqueItem.Caption,
                                    Name = string.Format("{0}", uniqueItem.Unique),
                                    ImageSource = new BitmapImage(new Uri(GetServerLayerItemsImageUri(index), UriKind.RelativeOrAbsolute)),
                                    IsVisible = uniqueItem.Visible,

                                });
                            index++;
                        }
                        break;
                    }
                default:
                    break;
            }
            AssignValueToImageSource(new Uri(GetServerLayerImageUri(), UriKind.RelativeOrAbsolute));
            return items;
        }

        internal override void Attach(LegendTree legendTree)
        {
            if (legendTree != null)
            {
                LayerItems.ForEach<LegendItemInfo>((legendItem) => legendItem.Attach(legendTree));
                this.Template = legendTree.LayerTemplate;
            }
        }

        internal override void Detach()
        {
            base.Detach();
            LayerItems.ForEach<LegendItemInfo>((legendItem) => legendItem.Detach());
            this.Template = null;
        }
    }
}
