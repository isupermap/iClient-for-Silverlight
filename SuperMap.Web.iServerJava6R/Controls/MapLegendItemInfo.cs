﻿using System;
using System.Collections.ObjectModel;
using SuperMap.Web.Mapping;
using SuperMap.Web.iServerJava6R;

namespace SuperMap.Web.Controls
{
    internal class MapLegendItemInfo : LayerLegendItemInfo
    {
        private Map map;
        private Layer layer;
        Action<MapLegendItemInfo> DoUIRefresh;
        private bool showOnlyVisibleLayers;
        private double sourceImageHeight;
        private double sourceImageWidth;

        internal MapLegendItemInfo()
        { }

        public MapLegendItemInfo(Layer iClientLayer, Map iClientMap, bool _showOnlyVisibleLayers, double height, double width, bool expanded, Action<MapLegendItemInfo> action)
            : this()
        {
            //ToDo:  
            this.layer = iClientLayer;
            this.map = iClientMap;
            this.Description = iClientLayer.ID;
            this.Name = iClientLayer.ID;
            showOnlyVisibleLayers = _showOnlyVisibleLayers;
            DoUIRefresh = action;
            SwitchLayersInfo(iClientLayer.Url, iClientLayer.ID);
            sourceImageWidth = width;
            sourceImageHeight = height;
            IsExpanded = expanded;
        }

        private void SwitchLayersInfo(string url, string layerid)
        {
            GetLayersInfoService layerServices = new GetLayersInfoService(url);
            layerServices.ProcessAsync(layerid);
            layerServices.ProcessCompleted += new EventHandler<GetLayersInfoEventArgs>(layerServices_ProcessCompleted);
        }

        private void layerServices_ProcessCompleted(object sender, GetLayersInfoEventArgs e)
        {
            if (e.Result != null && e.Result.LayersInfo != null)
            {
                ObservableCollection<LegendItemInfo> colls = new ObservableCollection<LegendItemInfo>();
                foreach (var item in e.Result.LayersInfo)
                {
                    LegendItemInfo info = new LayerLegendItemInfo(item, this.layer, sourceImageHeight, sourceImageWidth,IsExpanded);
                    if (showOnlyVisibleLayers && !item.IsVisible)
                    {
                        continue;
                    }
                    colls.Add(info);
                }

                this.LayerItems = colls;

                DoUIRefresh(this);
            }
        }

        internal override void Attach(LegendTree legendTree)
        {
            //base.Attach(legendTree);

            LayerItems.ForEach((layerLegendInfo) => ((LayerLegendItemInfo)layerLegendInfo).Attach(legendTree));
            this.Template = legendTree.MapLayerTemplate;
        }

        internal override void Detach()
        {
            base.Detach();
            LayerItems.ForEach((layerLegendInfo) => layerLegendInfo.Detach());
            this.Template = null;
        }
    }
}
