﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace SuperMap.Web.Controls
{
    /// <summary>
    ///  <para>${controls_TreeViewExtended_Title}</para>
    ///  <para>${controls_TreeViewExtended_Description}</para>
    /// </summary>
    public class TreeViewExtended : TreeView
    {
        /// <summary> ${controls_TreeViewExtended_constructor_None_D} </summary>
        public TreeViewExtended()
        {
            DefaultStyleKey = typeof(TreeView);
        }

        static TreeViewExtended()
        { }
        /// <summary> ${controls_TreeViewExtended_method_GetContainerForItemOverride_D} </summary>
        protected override DependencyObject GetContainerForItemOverride()
        {
            var item = new TreeViewItemExtended();
            item.SetBinding(TreeViewItem.IsExpandedProperty, new Binding("IsExpanded") { Mode = BindingMode.TwoWay });
            item.SetBinding(TreeViewItem.VisibilityProperty, new Binding("IsVisible")
            {
                Mode = BindingMode.TwoWay,
                Converter = new BoolConverter()
            });
            return item;
        }
        /// <summary> ${controls_TreeViewExtended_method_IsItemItsOwnContainerOverride_D} </summary>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeViewItemExtended;
        }
    }
    /// <summary>
    ///  <para>${controls_TreeViewItemExtended_Title}</para>
    ///  <para>${controls_TreeViewItemExtended_Description}</para>
    /// </summary>
    public class TreeViewItemExtended : TreeViewItem
    {
        /// <summary> ${controls_TreeViewItemExtended_constructor_None_D}</summary>
        public TreeViewItemExtended()
        { }
        /// <summary> ${controls_TreeViewItemExtended_method_GetContainerForItemOverride_D}</summary>
        protected override DependencyObject GetContainerForItemOverride()
        {
            var item = new TreeViewItemExtended();
            item.SetBinding(TreeViewItem.IsExpandedProperty, new Binding("IsExpanded") { Mode = BindingMode.TwoWay });
            item.SetBinding(TreeViewItem.VisibilityProperty, new Binding("IsVisible")
            {
                Mode = BindingMode.TwoWay,
                Converter = new BoolConverter()
            });
            return item;
        }
        /// <summary> ${controls_TreeViewItemExtended_method_IsItemItsOwnContainerOverride_D}</summary>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeViewItemExtended;
        }
    }

    internal class BoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool realValue = (bool)value;
            return realValue ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility realValue = (Visibility)value;
            return realValue == Visibility.Visible ? true : false;
        }
    }
}
