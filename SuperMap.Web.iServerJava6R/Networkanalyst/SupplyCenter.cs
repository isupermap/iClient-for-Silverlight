﻿

using System.Collections.Generic;
using System.Json;
using System;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_SupplyCenter_Title}</para>
    /// 	<para>${iServerJava6R_SupplyCenter_Description}</para>
    /// </summary>
    public class SupplyCenter
    {
        /// <summary>${iServerJava6R_SupplyCenter_constructor_D}</summary>
        public SupplyCenter()
        { }
        /// <summary>${iServerJava6R_SupplyCenter_attribute_maxWeight_D}</summary>
        public double MaxWeight { get; set; }
        /// <summary>${iServerJava6R_SupplyCenter_attribute_nodeID_D}</summary>
        public int NodeID { get; set; }
        /// <summary>${iServerJava6R_SupplyCenter_attribute_resourceValue_D}</summary>
        public double ResourceValue { get; set; }
        /// <summary>${iServerJava6R_SupplyCenter_attribute_type_D}</summary>
        public SupplyCenterType Type { get; set; }

        internal static string ToJson(SupplyCenter param)
        {
            if (param == null)
                return null;

            string json = "{";
            List<string> list = new List<string>();
            list.Add(string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"maxWeight\":{0}", param.MaxWeight));
            list.Add(string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"nodeID\":{0}", param.NodeID));
            list.Add(string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"resourceValue\":{0}", param.ResourceValue));
            list.Add(string.Format("\"type\":{0}", param.Type.ToString()));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
        /// <summary>${iServerJava6R_SupplyCenter_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_SupplyCenter_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_SupplyCenter_method_fromJson_param_jsonObject}</param>
        public static SupplyCenter FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            SupplyCenter result = new SupplyCenter();
            result.MaxWeight = (double)json["maxWeight"];
            result.NodeID = (int)json["nodeID"];
            result.ResourceValue = (double)json["resourceValue"];
            result.Type = (SupplyCenterType)Enum.Parse(typeof(SupplyCenterType), (string)json["type"], true);
            return result;
        }
    }
}
