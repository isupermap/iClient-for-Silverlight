﻿using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Core;
using System.Globalization;
using System;
using System.Collections.ObjectModel;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_Route_Title}</para>
    /// 	<para>${iServerJava6R_Route_Description}</para>
    /// </summary>
    public class Route:GeoLine
    {
        public Route()
        {
            
        }
        /// <summary>${iServerJava6R_Route_attribute_Length_D}</summary>
        public double Length { get; set; }
        /// <summary>${iServerJava6R_Route_attribute_MaxM_D}</summary>
        public double MaxM { get; set; }
        /// <summary>${iServerJava6R_Route_attribute_MinM_D}</summary>
        public double MinM { get; set; }

    }
}
