﻿


using SuperMap.Web.Core;
using System.Collections.Generic;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    //T只能是Point2D和int类型；
    /// <summary>
    /// 	<para>${iServerJava6R_ClosestFacilitiesParameters_Title}</para>
    /// 	<para>${iServerJava6R_ClosestFacilitiesParameters_Description}</para>
    /// </summary>
    /// <remarks>${iServerJava6R_ClosestFacilitiesParameters_Remarks}</remarks>
    ///  <typeparam name="T">${iServerJava6R_ClosestFacilitiesParameters_param_T}</typeparam>
    public class FindClosestFacilitiesParameters<T>
    {
        /// <summary>${iServerJava6R_ClosestFacilitiesParameters_constructor_D}</summary>
        public FindClosestFacilitiesParameters()
        {
            ExpectFacilityCount = 1;
        }

        /// <summary>${iServerJava6R_ClosestFacilitiesParameters_attribute_Event_D}</summary>
        public T Event { get; set; }

        /// <summary>${iServerJava6R_ClosestFacilitiesParameters_attribute_Facilities_D}</summary>
        public IList<T> Facilities { get; set; }

        /// <summary>${iServerJava6R_ClosestFacilitiesParameters_attribute_ExpectFacilityCount_D}</summary>
        public int ExpectFacilityCount { get; set; }

        /// <summary>${iServerJava6R_ClosestFacilitiesParameters_attribute_FromEvent_D}</summary>
        public bool FromEvent { get; set; }

        /// <summary>${iServerJava6R_ClosestFacilitiesParameters_attribute_MaxWeight_D}</summary>
        public double MaxWeight { get; set; }

        /// <summary>${iServerJava6R_ClosestFacilitiesParameters_attribute_Parameter_D}</summary>
        public TransportationAnalystParameter Parameter { get; set; }

    }
}
