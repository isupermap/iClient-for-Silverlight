﻿

using SuperMap.Web.Core;
using System.Json;
using SuperMap.Web.Utilities;
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ClosestFacilityPath_Title}</para>
    /// 	<para>${iServerJava6R_ClosestFacilityPath_Description}</para>
    /// </summary>
    public class ClosestFacilityPath : ServerPath
    {
        internal ClosestFacilityPath()
        { }
        //实际类型为 Point2D or int
        /// <summary>${iServerJava6R_ClosestFacilityPath_attribute_Facility_D}</summary>
        public object Facility { get; private set; }
        /// <summary>${iServerJava6R_ClosestFacilityPath_attribute_FacilityIndex_D}</summary>
        public int FacilityIndex { get; private set; }

        /// <summary>${iServerJava6R_ClosestFacilityPath_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_ClosestFacilityPath_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_ClosestFacilityPath_method_fromJson_param_jsonObject}</param>
        public static ClosestFacilityPath FromJson(JsonObject json)
        {
            if (json != null)
            {
                ClosestFacilityPath result = new ClosestFacilityPath();

                if (json["facility"] is JsonPrimitive)
                {
                    result.Facility = (int)json["facility"];
                }
                else
                {
                    result.Facility = JsonHelper.ToPoint2D((JsonObject)json["facility"]);
                }
                result.FacilityIndex = json["facilityIndex"];

                //对应父类中的属性；
                ServerPath path = ServerPath.ServerPathFromJson(json);
                result.EdgeFeatures = path.EdgeFeatures;
                result.EdgeIDs = path.EdgeIDs;
                result.NodeFeatures = path.NodeFeatures;
                result.NodeIDs = path.NodeIDs;
                result.PathGuideItems = path.PathGuideItems;
                result.Route = path.Route;
                result.StopWeights = path.StopWeights;
                result.Weight = path.Weight;

                return result;
            }
            return null;
        }
    }
}
