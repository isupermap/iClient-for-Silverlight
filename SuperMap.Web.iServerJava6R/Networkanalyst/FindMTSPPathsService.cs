﻿

using System;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindMTSPPathsService_Title}</para>
    /// 	<para>${iServerJava6R_FindMTSPPathsService_Description}</para>
    /// </summary>
    public class FindMTSPPathsService : ServiceBase
    {
        /// <summary>${iServerJava6R_FindMTSPPathsService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_FindMTSPPathsService_constructor_overloads_D}</overloads>
        public FindMTSPPathsService()
        { }
        /// <summary>${iServerJava6R_FindMTSPPathsService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_FindMTSPPathsService_constructor_param_url}</param>
        public FindMTSPPathsService(string url)
            : base(url)
        { }

        /// <summary>${iServerJava6R_FindMTSPPathsService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_FindMTSPPathsService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync<T>(T parameters)
        {
            ProcessAsync<T>(parameters, null);
        }
        /// <summary>${iServerJava6R_FindMTSPPathsService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_FindMTSPPathsService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${iServerJava6R_FindMTSPPathsService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync<T>(T parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.Url += "mtsppath.json?debug=true&_method=GET";

            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true);
        }

        private Dictionary<string, string> GetParameters<T>(T parameters)
        {
            if (parameters is FindMTSPPathsParameters<Point2D>)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                if ((parameters as FindMTSPPathsParameters<Point2D>).Centers != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindMTSPPathsParameters<Point2D>).Centers.Count; i++)
                    {
                        temp.Add(string.Format("{0}", JsonHelper.FromPoint2D((parameters as FindMTSPPathsParameters<Point2D>).Centers[i])));
                    }
                    dictionary.Add("centers", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }

                if ((parameters as FindMTSPPathsParameters<Point2D>).Nodes != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindMTSPPathsParameters<Point2D>).Nodes.Count; i++)
                    {
                        temp.Add(string.Format("{0}", JsonHelper.FromPoint2D((parameters as FindMTSPPathsParameters<Point2D>).Nodes[i])));
                    }
                    dictionary.Add("nodes", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }

                dictionary.Add("hasLeastTotalCost", (parameters as FindMTSPPathsParameters<Point2D>).HasLeastTotalCost.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
                dictionary.Add("parameter", TransportationAnalystParameter.ToJson((parameters as FindMTSPPathsParameters<Point2D>).Parameter));
                return dictionary;
            }
            else if (parameters is FindMTSPPathsParameters<int>)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                if ((parameters as FindMTSPPathsParameters<int>).Centers != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindMTSPPathsParameters<int>).Centers.Count; i++)
                    {
                        temp.Add((parameters as FindMTSPPathsParameters<int>).Centers[i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                    }
                    dictionary.Add("centers", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }                

                if ((parameters as FindMTSPPathsParameters<int>).Nodes != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindMTSPPathsParameters<int>).Nodes.Count; i++)
                    {
                        temp.Add((parameters as FindMTSPPathsParameters<int>).Nodes[i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                    }
                    dictionary.Add("nodes", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }

                dictionary.Add("hasLeastTotalCost", (parameters as FindMTSPPathsParameters<int>).HasLeastTotalCost.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
                dictionary.Add("parameter", TransportationAnalystParameter.ToJson((parameters as FindMTSPPathsParameters<int>).Parameter));
                return dictionary;
            }
            else
            {
                throw new ArgumentException("参数只能为MTSPPathsParameters<int>或者MTSPPathsParameters<Point2D>类型！");
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            FindMTSPPathsResult result = FindMTSPPathsResult.FromJson(jsonObject);
            LastResult = result;
            FindMTSPPathsEventArgs args = new FindMTSPPathsEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(FindMTSPPathsEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_FindMTSPPathsService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FindMTSPPathsEventArgs> ProcessCompleted;

        private FindMTSPPathsResult lastResult;
        /// <summary>${iServerJava6R_FindMTSPPathsService_attribute_LastResult_D}</summary>
        public FindMTSPPathsResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
