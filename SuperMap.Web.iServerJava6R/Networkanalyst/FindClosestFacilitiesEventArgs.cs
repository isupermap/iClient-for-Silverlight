﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ClosestFacilitiesEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_ClosestFacilitiesEventArgs_Description}</para>
    /// </summary>
    public class FindClosestFacilitiesEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_ClosestFacilitiesEventArgs_constructor_D}</summary>
        public FindClosestFacilitiesEventArgs(FindClosestFacilitiesResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_ClosestFacilitiesEventArgs_attribute_Result_D}</summary>
        public FindClosestFacilitiesResult Result { get; private set; }
        /// <summary>${iServerJava6R_ClosestFacilitiesEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
