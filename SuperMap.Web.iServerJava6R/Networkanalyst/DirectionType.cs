﻿

namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_DirectionType_Title}</para>
    /// 	<para>${iServerJava6R_DirectionType_Description}</para>
    /// </summary>
    public enum DirectionType
    {
        /// <summary>${iServerJava6R_DirectionType_attribute_EAST_D}</summary>
        EAST,
        /// <summary>${iServerJava6R_DirectionType_attribute_NONE_D}</summary>
        NONE,
        /// <summary>${iServerJava6R_DirectionType_attribute_NORTH_D}</summary>
        NORTH,
        /// <summary>${iServerJava6R_DirectionType_attribute_SOUTH_D}</summary>
        SOURTH,
        /// <summary>${iServerJava6R_DirectionType_attribute_WEST_D}</summary>
        WEST
    }
}
