﻿

using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindMTSPPathsParameters_Title}</para>
    /// 	<para>${iServerJava6R_FindMTSPPathsParameters_Description}</para>
    /// 	<para>
    /// 		<list type="table">
    /// 			<item>
    /// 				<term><img src="MTSPPathsR.bmp"/></term>
    /// 				<description><img src="MTSPPathsL.bmp"/></description>
    /// 			</item>
    /// 		</list>
    /// 	</para>
    /// </summary>
    /// <typeparam name="T">${iServerJava6R_FindMTSPPathsParameters_param_T}</typeparam>
    public class FindMTSPPathsParameters<T>
    {
        /// <summary>${iServerJava6R_FindMTSPPathsParameters_constructor_D}</summary>
        public FindMTSPPathsParameters()
        { }

        /// <summary>${iServerJava6R_FindMTSPPathsParameters_attribute_centers_D}</summary>
        public IList<T> Centers { get; set; }
        /// <summary>${iServerJava6R_FindMTSPPathsParameters_attribute_Nodes_D}</summary>
        public IList<T> Nodes { get; set; }

        /// <summary>${iServerJava6R_FindMTSPPathsParameters_attribute_HasLeastTotalCost_D}</summary>
        public bool HasLeastTotalCost { get; set; }
        /// <summary>${iServerJava6R_FindMTSPPathsParameters_attribute_Parameter_D}</summary>
        public TransportationAnalystParameter Parameter { get; set; }
    }
}
