﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_MTSPPath_Title}</para>
    /// 	<para>${iServerJava6R_MTSPPath_Description}</para>
    /// </summary>
    public class MTSPPath :TSPPath
    {
        internal MTSPPath()
        { }

        /// <summary>${iServerJava6R_MTSPPath_attribute_Center_D}</summary>
        public object Center { get; private set; }
        /// <summary>${iServerJava6R_MTSPPath_attribute_NodesVisited_D}</summary>
        public List<object> NodesVisited { get; private set; }

        /// <summary>${iServerJava6R_MTSPPath_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_MTSPPath_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_MTSPPath_method_fromJson_param_jsonObject}</param>
        public static MTSPPath FromJson(JsonObject json)
        {
            if (json == null)
                return null;
            MTSPPath result = new MTSPPath();
            result.Center = json["center"];

            if (json["nodesVisited"] != null)
            {
                result.NodesVisited = new List<object>();
                for (int i = 0; i < json["nodesVisited"].Count; i++)
                {
                    result.NodesVisited.Add(json["nodesVisited"][i]);
                }
            }

            if (json["stopIndexes"] != null)
            {
                result.StopIndexes = new List<int>();
                for (int i = 0; i < json["stopIndexes"].Count; i++)
                {
                    result.StopIndexes.Add(json["stopIndexes"][i]);
                }
            }

            //对应父类中的属性；
            ServerPath path = ServerPath.ServerPathFromJson(json);
            result.EdgeFeatures = path.EdgeFeatures;
            result.EdgeIDs = path.EdgeIDs;
            result.NodeFeatures = path.NodeFeatures;
            result.NodeIDs = path.NodeIDs;
            result.PathGuideItems = path.PathGuideItems;
            result.Route = path.Route;
            result.StopWeights = path.StopWeights;
            result.Weight = path.Weight;

            return result;
        }
    }
}
