﻿

using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServiceAreaParameters_Title}</para>
    /// 	<para>${iServerJava6R_ServiceAreaParameters_Description}</para>
    /// </summary>
    /// <typeparam name="T">${iServerJava6R_FindServiceAreaParameters_param_T}</typeparam>
    public class FindServiceAreasParameters<T>
    {
        /// <summary>${iServerJava6R_ServiceAreaParameters_constructor_D}</summary>
        public FindServiceAreasParameters()
        { }
        //T的类型只能为int或Point2D
        /// <summary>${iServerJava6R_ServiceAreaParameters_attribute_Centers}</summary>
        public IList<T> Centers { get; set; }
        /// <summary>${iServerJava6R_ServiceAreaParameters_attribute_Weights}</summary>
        public IList<double> Weights { get; set; }

        /// <summary>${iServerJava6R_ServiceAreaParameters_attribute_IsFromCenter}</summary>
        public bool IsFromCenter { get; set; }
        /// <summary>${iServerJava6R_ServiceAreaParameters_attribute_IsCenterMutuallyExclusive}</summary>
        public bool IsCenterMutuallyExclusive { get; set; }
        /// <summary>${iServerJava6R_ServiceAreaParameters_attribute_Parameter}</summary>
        public TransportationAnalystParameter Parameter { get; set; }

    }
}
