﻿

namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>${iServerJava6R_OutputFormat_Title}</summary>
    public enum OutputFormat
    {
        /// <summary>${iServerJava6R_OutputFormat_attribute_BINARY_D}</summary>
        BINARY,
        /// <summary>${iServerJava6R_OutputFormat_attribute_BMP_D}</summary>
        BMP,
        /// <summary>${iServerJava6R_OutputFormat_attribute_DEFAULT_D}</summary>
        DEFAULT,
        /// <summary>${iServerJava6R_OutputFormat_attribute_EMF_D}</summary>
        EMF,
        /// <summary>${iServerJava6R_OutputFormat_attribute_EPS_D}</summary>
        EPS,
        /// <summary>${iServerJava6R_OutputFormat_attribute_GIF_D}</summary>
        GIF,
        /// <summary>${iServerJava6R_OutputFormat_attribute_JPG_D}</summary>
        JPG,
        /// <summary>${iServerJava6R_OutputFormat_attribute_PDF_D}</summary>
        PDF,
        /// <summary>${iServerJava6R_OutputFormat_attribute_PNG_D}</summary>
        PNG
    }
}
