﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTSPPathsEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_FindTSPPathsEventArgs_Description}</para>
    /// </summary>
    public class FindTSPPathsEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_FindTSPPathsEventArgs_constructor_D}</summary>
        public FindTSPPathsEventArgs(FindTSPPathsResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_FindTSPPathsEventArgs_attribute_Result_D}</summary>
        public FindTSPPathsResult Result { get; private set; }
        /// <summary>${iServerJava6R_FindTSPPathsEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
