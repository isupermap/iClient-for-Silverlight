﻿

namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_SideType_Title}</para>
    /// 	<para>${iServerJava6R_SideType_Description}</para>
    /// </summary>
    public enum SideType
    {
        /// <summary>${iServerJava6R_SideType_attribute_LEFT_D}</summary>
        LEFT,
        /// <summary>${iServerJava6R_SideType_attribute_MIDDLE_D}</summary>
        MIDDLE,
        /// <summary>${iServerJava6R_SideType_attribute_NONE_D}</summary>
        NONE,
        /// <summary>${iServerJava6R_SideType_attribute_RIGHT_D}</summary>
        RIGHT
    }
}
