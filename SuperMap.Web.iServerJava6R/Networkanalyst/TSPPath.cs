﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_TSPPath_Title}</para>
    /// 	<para>${iServerJava6R_TSPPath_Description}</para>
    /// </summary>
    public class TSPPath : ServerPath
    {
        internal TSPPath()
        { }

        /// <summary>${iServerJava6R_TSPPath_attribute_StopIndexes_D}</summary>
        public List<int> StopIndexes { get; protected set; }

        /// <summary>${iServerJava6R_TSPPath_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_TSPPath_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_TSPPath_method_fromJson_param_jsonObject}</param>
        public static TSPPath TSPPathFromJson(JsonObject json)
        {
            if (json == null)
                return null;

            TSPPath result = new TSPPath();
            if (json["stopIndexes"] != null)
            {
                result.StopIndexes = new List<int>();
                for (int i = 0; i < json["stopIndexes"].Count; i++)
                {
                    result.StopIndexes.Add(json["stopIndexes"][i]);
                }
            }

            //对应父类中的属性；
            ServerPath path = ServerPath.ServerPathFromJson(json);
            result.EdgeFeatures = path.EdgeFeatures;
            result.EdgeIDs = path.EdgeIDs;
            result.NodeFeatures = path.NodeFeatures;
            result.NodeIDs = path.NodeIDs;
            result.PathGuideItems = path.PathGuideItems;
            result.Route = path.Route;
            result.StopWeights = path.StopWeights;
            result.Weight = path.Weight;

            return result;
        }
    }
}
