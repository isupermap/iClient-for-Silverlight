﻿

using System;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTSPPathsService_Title}</para>
    /// 	<para>${iServerJava6R_FindTSPPathsService_Description}</para>
    /// </summary>
    public class FindTSPPathsService : ServiceBase
    {
        /// <summary>${iServerJava6R_FindTSPPathsService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_ClosestFacilitiesService_constructor_overloads_D}</overloads>
        public FindTSPPathsService()
        { }
        /// <summary>${iServerJava6R_ClosestFacilitiesService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_FindTSPPathsService_constructor_param_url}</param>
        public FindTSPPathsService(string url)
            : base(url)
        {
        }

        /// <summary>${iServerJava6R_FindTSPPathsService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_FindTSPPathsService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync<T>(T parameters)
        {
            ProcessAsync<T>(parameters, null);
        }
        /// <summary>${iServerJava6R_FindTSPPathsService_method_ProcessAsync_D}</summary>
        /// <param name="state">${iServerJava6R_FindTSPPathsService_method_ProcessAsync_param_state}</param>
        /// <param name="parameters">${iServerJava6R_FindTSPPathsService_method_ProcessAsync_param_Parameters}</param>
        public void ProcessAsync<T>(T parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }
            base.Url += "tsppath.json?debug=true&_method=GET";

            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true);
        }

        private Dictionary<string, string> GetParameters<T>(T parameters)
        {
            if (parameters is FindTSPPathsParameters<int>)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                if ((parameters as FindTSPPathsParameters<int>).Nodes != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindTSPPathsParameters<int>).Nodes.Count; i++)
                    {
                        temp.Add((parameters as FindTSPPathsParameters<int>).Nodes[i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                    }
                    dictionary.Add("nodes", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }

                dictionary.Add("endNodeAssigned", (parameters as FindTSPPathsParameters<int>).EndNodeAssigned.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
                dictionary.Add("parameter", TransportationAnalystParameter.ToJson((parameters as FindTSPPathsParameters<int>).Parameter));

                return dictionary;
            }
            else if (parameters is FindTSPPathsParameters<Point2D>)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                if ((parameters as FindTSPPathsParameters<Point2D>).Nodes != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindTSPPathsParameters<Point2D>).Nodes.Count; i++)
                    {
                        temp.Add(string.Format("{0}", JsonHelper.FromPoint2D((parameters as FindTSPPathsParameters<Point2D>).Nodes[i])));
                    }
                    dictionary.Add("nodes", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }

                dictionary.Add("endNodeAssigned", (parameters as FindTSPPathsParameters<Point2D>).EndNodeAssigned.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
                dictionary.Add("parameter", TransportationAnalystParameter.ToJson((parameters as FindTSPPathsParameters<Point2D>).Parameter));

                return dictionary;
            }
            else
            {
                throw new ArgumentException("参数只能为TSPPathsParameters<int>或者TSPPathsParameters<Point2D>类型！");
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            FindTSPPathsResult result = FindTSPPathsResult.FromJson(jsonObject);
            LastResult = result;
            FindTSPPathsEventArgs args = new FindTSPPathsEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(FindTSPPathsEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_FindTSPPathsService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FindTSPPathsEventArgs> ProcessCompleted;

        private FindTSPPathsResult lastResult;
        /// <summary>${iServerJava6R_FindTSPPathsService_attribute_lastResult_D}</summary>
        public FindTSPPathsResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
