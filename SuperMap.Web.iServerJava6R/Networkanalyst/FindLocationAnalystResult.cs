﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindLocationAnalystResult_Title}</para>
    /// 	<para>${iServerJava6R_FindLocationAnalystResult_Description}</para>
    /// </summary>
    public class FindLocationAnalystResult
    {
        internal FindLocationAnalystResult()
        { }
        //TODO:应该直接给一张图片；
        ///// <summary>${iServerJava6R_FindPathAnalystResult_attribute_MapImage_D}</summary>
        //public NAResultMapImage MapImage { get; private set; }

        /// <summary>${iServerJava6R_FindLocationAnalystResult_attribute_DemandResults_D}</summary>
        public List<DemandResult> DemandResults { get; private set; }
        /// <summary>${iServerJava6R_FindLocationAnalystResult_attribute_SupplyResults_D}</summary>
        public List<SupplyResult> SupplyResults { get; private set; }

        /// <summary>${iServerJava6R_FindLocationAnalystResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_FindLocationAnalystResult_method_fromJson_Return}</returns>
        /// <param name="json">${iServerJava6R_FindLocationAnalystResult_method_fromJson_param_jsonObject}</param>
        public static FindLocationAnalystResult FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            FindLocationAnalystResult result = new FindLocationAnalystResult();
            //result.MapImage = NAResultMapImage.FromJson((JsonObject)json["mapImage"]);

            if (json["demandResults"] != null && json["demandResults"].Count > 0)
            {
                result.DemandResults = new List<DemandResult>();
                for (int i = 0; i < json["demandResults"].Count; i++)
                {
                    result.DemandResults.Add(DemandResult.FromJson((JsonObject)json["demandResults"][i]));
                }
            }
            if (json["supplyResults"] != null && json["supplyResults"].Count > 0)
            {
                result.SupplyResults = new List<SupplyResult>();
                for (int i = 0; i < json["supplyResults"].Count; i++)
                {
                    result.SupplyResults.Add(SupplyResult.FromJson((JsonObject)json["supplyResults"][i]));
                }
            }

            return result;
        }
    }
}
