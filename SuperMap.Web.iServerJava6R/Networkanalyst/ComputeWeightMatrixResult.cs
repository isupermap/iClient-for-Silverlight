﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ComputeWeightMatrixResult_Title}</para>
    /// 	<para>${iServerJava6R_ComputeWeightMatrixResult_Description}</para>
    /// </summary>
    public class ComputeWeightMatrixResult
    {
        internal ComputeWeightMatrixResult()
        { }
        /// <summary>${iServerJava6R_ComputeWeightMatrixResult_attribute_WeightMatrix_D}</summary>
        public List<List<double>> WeightMatrix { get; private set; }

        /// <summary>${iServerJava6R_ComputeWeightMatrixResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_ComputeWeightMatrixResult_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_ComputeWeightMatrixResult_method_fromJson_param_jsonObject}</param>
        public static ComputeWeightMatrixResult FromJson(JsonArray json)
        {
            if (json == null)
                return null;

            ComputeWeightMatrixResult result = new ComputeWeightMatrixResult();
            if (json.Count > 0)
            {
                result.WeightMatrix = new List<List<double>>();
                for (int i = 0; i < json.Count; i++)
                {
                    List<double> list = new List<double>();
                    for (int j = 0; j < json[i].Count; j++)
                    {
                        list.Add(json[i][j]);
                    }
                    result.WeightMatrix.Add(list);
                }
            }

            return result;
        }
    }
}
