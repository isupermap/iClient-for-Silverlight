﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindLocationEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_FindLocationEventArgs_Description}</para>
    /// </summary>
    public class FindLocationAnalystEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_FindLocationEventArgs_constructor_D}</summary>
        public FindLocationAnalystEventArgs(FindLocationAnalystResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_FindLocationEventArgs_attribute_Result_D}</summary>
        public FindLocationAnalystResult Result { get; private set; }
        /// <summary>${iServerJava6R_FindLocationEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
