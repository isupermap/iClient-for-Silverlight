﻿

using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindPathParameters_Title}</para>
    /// 	<para>${iServerJava6R_FindPathParameters_Description}</para>
    /// </summary>
    /// <remarks>${iServerJava6R_FindPathParameters_Remarks}</remarks>
    /// <typeparam name="T">${iServerJava6R_FindPathParameters_param_T}</typeparam>
    public class FindPathParameters<T>
    {
        /// <summary>${iServerJava6R_FindPathParameters_constructor_D}</summary>
        public FindPathParameters()
        { }

        /// <summary>${iServerJava6R_FindPathParameters_attribute_Nodes_D}</summary>
        public IList<T> Nodes { get; set; }


        /// <summary><para>${iServerJava6R_FindPathParameters_attribute_HasLeastEdgeCount_D}</para>
        /// <para><img src="FindPathLeastEdges.bmp"/></para>
        /// </summary>
        /// 
        public bool HasLeastEdgeCount { get; set; }

        /// <summary>${iServerJava6R_FindPathParameters_attribute_Parameter_D}</summary>
        public TransportationAnalystParameter Parameter { get; set; }

    }
}
