﻿

using System;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.iServerJava6R.Resources;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindLocationService_Title}</para>
    /// 	<para>${iServerJava6R_FindLocationService_Description}</para>
    /// </summary>
    public class FindLocationAnalystService : ServiceBase
    {
        /// <summary>${iServerJava6R_FindLocationService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_FindLocationService_constructor_overloads_D}</overloads>
        public FindLocationAnalystService()
        { }
        /// <summary>${iServerJava6R_FindLocationService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_FindLocationService_constructor_param_url}</param>
        public FindLocationAnalystService(string url)
            : base(url)
        {
        }
        /// <summary>${iServerJava6R_FindLocationService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_FindLocationService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(FindLocationAnalystParameters parameters)
        {
            this.ProcessAsync(parameters, null);
        }
        /// <summary>${iServerJava6R_FindLocationService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_FindLocationService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${iServerJava6R_FindLocationService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(FindLocationAnalystParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }
            //参数必须放在 URI 中
            base.Url += "location.json?debug=true&_method=GET";            
                        
            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true);
        }

        private Dictionary<string, string> GetParameters(FindLocationAnalystParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            
            if (parameters.SupplyCenters != null && parameters.SupplyCenters.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < parameters.SupplyCenters.Count; i++)
                {
                    temp.Add(string.Format("{0}", SupplyCenter.ToJson(parameters.SupplyCenters[i])));
                }
                dictionary.Add("supplyCenters",string.Format("[{0}]", string.Join(",", temp.ToArray())));
            }

            dictionary.Add("isFromCenter", parameters.IsFromCenter.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
            dictionary.Add("expectedSupplyCenterCount", parameters.ExpectedSupplyCenterCount.ToString(System.Globalization.CultureInfo.InvariantCulture));
           // dictionary.Add("nodeDemandField", parameters.NodeDemandField);
            dictionary.Add("weightName", parameters.WeightName);
            dictionary.Add("turnWeightField", parameters.TurnWeightField);
            dictionary.Add("returnEdgeFeatures", parameters.ReturnEdgeFeature.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
            dictionary.Add("returnEdgeGeometry", parameters.ReturnEdgeGeometry.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
            dictionary.Add("returnNodeFeatures", parameters.ReturnNodeFeature.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());

            return dictionary;
        }
        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            FindLocationAnalystResult result = FindLocationAnalystResult.FromJson(jsonObject);
            LastResult = result;
            FindLocationAnalystEventArgs args = new FindLocationAnalystEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(FindLocationAnalystEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_FindLocationService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FindLocationAnalystEventArgs> ProcessCompleted;

        private FindLocationAnalystResult lastResult;
        /// <summary>${iServerJava6R_FindLocationService_attribute_lastResult_D}</summary>
        public FindLocationAnalystResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
