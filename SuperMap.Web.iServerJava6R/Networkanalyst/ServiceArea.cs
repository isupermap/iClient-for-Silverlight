﻿

using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServiceArea_Title}</para>
    /// 	<para>${iServerJava6R_ServiceArea_Description}</para>
    /// </summary>
    public class ServiceArea
    {
        internal ServiceArea()
        { }
        /// <summary>${iServerJava6R_ServiceArea_attribute_EdgeFeatures_D}</summary>
        public FeatureCollection EdgeFeatures { get; private set; }
        /// <summary>${iServerJava6R_ServiceArea_attribute_EdgeIDs_D}</summary>
        public List<int> EdgeIDs { get; private set; }
        /// <summary>${iServerJava6R_ServiceArea_attribute_NodeFeatures_D}</summary>
        public FeatureCollection NodeFeatures { get; private set; }
        /// <summary>${iServerJava6R_ServiceArea_attribute_NodeIDs_D}</summary>
        public List<int> NodeIDs { get; private set; }
        /// <summary>${iServerJava6R_ServiceArea_attribute_Routes_D}</summary>
        public List<Route> Routes { get; private set; }
        /// <summary>${iServerJava6R_ServiceArea_attribute_ServiceRegion_D}</summary>
        public Geometry ServiceRegion { get; private set; }

        /// <summary>${iServerJava6R_ServiceArea_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_ServiceArea_method_FromJson_return}</returns>
        /// <param name="json">${iServerJava6R_ServiceArea_method_FromJson_param_jsonObject}</param>
        public static ServiceArea FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            ServiceArea result = new ServiceArea();

            if (json["edgeFeatures"] != null)
            {
                result.EdgeFeatures = new FeatureCollection();
                for (int i = 0; i < json["edgeFeatures"].Count; i++)
                {
                    result.EdgeFeatures.Add(ServerFeature.FromJson((JsonObject)json["edgeFeatures"][i]).ToFeature());
                }
            }
            if (json["edgeIDs"] != null)
            {
                result.EdgeIDs = new List<int>();
                for (int i = 0; i < json["edgeIDs"].Count; i++)
                {
                    result.EdgeIDs.Add((int)json["edgeIDs"][i]);
                }
            }
            if (json["nodeFeatures"] != null)
            {
                result.NodeFeatures = new FeatureCollection();
                for (int i = 0; i < json["nodeFeatures"].Count; i++)
                {
                    result.NodeFeatures.Add(ServerFeature.FromJson((JsonObject)json["nodeFeatures"][i]).ToFeature());
                }
            }
            if (json["nodeIDs"] != null)
            {
                result.NodeIDs = new List<int>();
                for (int i = 0; i < json["nodeIDs"].Count; i++)
                {
                    result.NodeIDs.Add((int)json["nodeIDs"][i]);
                }
            }
            if (json["routes"] != null)
            {
                result.Routes = new List<Route>();
                for (int i = 0; i < json["routes"].Count; i++)
                {
                    result.Routes.Add(ServerGeometry.FromJson((JsonObject)json["routes"][i]).ToRoute());
                }
            }
            result.ServiceRegion = ServerGeometry.FromJson((JsonObject)json["serviceRegion"]).ToGeometry();

            return result;
        }
    }
}
