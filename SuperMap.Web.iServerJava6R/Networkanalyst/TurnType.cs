﻿

namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_TurnType_Title}</para>
    /// 	<para>${iServerJava6R_TurnType_Description}</para>
    /// </summary>
    public enum TurnType
    {
        /// <summary>${iServerJava6R_TurnType_attribute_AHEAD_D}</summary>
        AHEAD,
        /// <summary>${iServerJava6R_TurnType_attribute_BACK_D}</summary>
        BACK,
        /// <summary>${iServerJava6R_TurnType_attribute_END_D}</summary>
        END,
        /// <summary>${iServerJava6R_TurnType_attribute_LEFT_D}</summary>
        LEFT,
        /// <summary>${iServerJava6R_TurnType_attribute_NONE_D}</summary>
        NONE,
        /// <summary>${iServerJava6R_TurnType_attribute_RIGHT_D}</summary>
        RIGHT
    }
}
