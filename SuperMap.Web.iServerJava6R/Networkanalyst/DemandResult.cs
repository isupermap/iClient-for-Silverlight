﻿

using System.Json;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_DemandResult_Title}</para>
    /// 	<para>${iServerJava6R_DemandResult_Description}</para>
    /// </summary>
    public class DemandResult
    {
        internal DemandResult()
        { }
        /// <summary>${iServerJava6R_DemandResult_attribute_ActualResourceValue_D}</summary>
        public double ActualResourceValue { get; private set; }
        /// <summary>${iServerJava6R_DemandResult_attribute_DemandID_D}</summary>
        public int DemandID { get; private set; }
        /// <summary>${iServerJava6R_DemandResult_attribute_IsEdge_D}</summary>
        public bool IsEdge { get; private set; }
        /// <summary>${iServerJava6R_DemandResult_attribute_SupplyCenter_D}</summary>
        public SupplyCenter SupplyCenter { get; private set; }

        /// <summary>${iServerJava6R_DemandResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_DemandResult_method_fromJson_Return}</returns>
        /// <param name="json">${iServerJava6R_DemandResult_method_fromJson_param_jsonObject}</param>
        public static DemandResult FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            DemandResult result = new DemandResult();
            result.ActualResourceValue = (double)json["actualResourceValue"];
            result.DemandID = (int)json["demandID"];
            result.IsEdge = (bool)json["isEdge"];
            result.SupplyCenter = SupplyCenter.FromJson((JsonObject)json["supplyCenter"]);

            return result;
        }
    }
}
