﻿

using System;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.iServerJava6R.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;
using SuperMap.Web.Core;

namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ClosestFacilitiesService_Title}</para>
    /// 	<para>${iServerJava6R_ClosestFacilitiesService_Description}</para>
    /// </summary>
    public class FindClosestFacilitiesService : ServiceBase
    {
        /// <summary>${iServerJava6R_ClosestFacilitiesService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_ClosestFacilitiesService_constructor_overloads_D}</overloads>
        public FindClosestFacilitiesService()
        { }
        /// <summary>${iServerJava6R_ClosestFacilitiesService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_ClosestFacilitiesService_constructor_param_url}</param>
        public FindClosestFacilitiesService(string url)
            : base(url)
        { }

        /// <summary>${iServerJava6R_ClosestFacilitiesService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_ClosestFacilitiesService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync<T>(T parameters)
        {
            this.ProcessAsync<T>(parameters, null);
        }

        /// <summary>${iServerJava6R_ClosestFacilitiesService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_ClosestFacilitiesService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${iServerJava6R_ClosestFacilitiesService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync<T>(T parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }
            base.Url += "closestfacility.json?debug=true&_method=GET";

            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true);
        }

        private Dictionary<string, string> GetParameters<T>(T parameters)
        {
            if (parameters is FindClosestFacilitiesParameters<Point2D>)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                if ((parameters as FindClosestFacilitiesParameters<Point2D>).Facilities != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindClosestFacilitiesParameters<Point2D>).Facilities.Count; i++)
                    {
                        temp.Add(string.Format("{0}", JsonHelper.FromPoint2D((parameters as FindClosestFacilitiesParameters<Point2D>).Facilities[i])));
                    }
                    dictionary.Add("facilities", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }

                dictionary.Add("event", JsonHelper.FromPoint2D((parameters as FindClosestFacilitiesParameters<Point2D>).Event));
                dictionary.Add("expectFacilityCount", (parameters as FindClosestFacilitiesParameters<Point2D>).ExpectFacilityCount.ToString(System.Globalization.CultureInfo.InvariantCulture));
                dictionary.Add("fromEvent", (parameters as FindClosestFacilitiesParameters<Point2D>).FromEvent.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
                dictionary.Add("maxWeight", (parameters as FindClosestFacilitiesParameters<Point2D>).MaxWeight.ToString(System.Globalization.CultureInfo.InvariantCulture));
                dictionary.Add("parameter", TransportationAnalystParameter.ToJson((parameters as FindClosestFacilitiesParameters<Point2D>).Parameter));
                return dictionary;
            }
            else if (parameters is FindClosestFacilitiesParameters<int>)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                if ((parameters as FindClosestFacilitiesParameters<int>).Facilities != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindClosestFacilitiesParameters<int>).Facilities.Count; i++)
                    {
                        temp.Add((parameters as FindClosestFacilitiesParameters<int>).Facilities[i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                    }
                    dictionary.Add("facilities", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }

                dictionary.Add("event", (parameters as FindClosestFacilitiesParameters<int>).Event.ToString(System.Globalization.CultureInfo.InvariantCulture));
                dictionary.Add("expectFacilityCount", (parameters as FindClosestFacilitiesParameters<int>).ExpectFacilityCount.ToString(System.Globalization.CultureInfo.InvariantCulture));
                dictionary.Add("fromEvent", (parameters as FindClosestFacilitiesParameters<int>).FromEvent.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
                dictionary.Add("maxWeight", (parameters as FindClosestFacilitiesParameters<int>).MaxWeight.ToString(System.Globalization.CultureInfo.InvariantCulture));
                dictionary.Add("parameter", TransportationAnalystParameter.ToJson(((parameters as FindClosestFacilitiesParameters<int>).Parameter)));
                return dictionary;
            }
            else
            {
                throw new ArgumentException("参数只能为ClosestFacilitiesParameters<int>或者ClosestFacilitiesParameters<Point2D>类型！");
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            FindClosestFacilitiesResult result = FindClosestFacilitiesResult.FromJson(jsonObject);
            LastResult = result;
            FindClosestFacilitiesEventArgs args = new FindClosestFacilitiesEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(FindClosestFacilitiesEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_ClosestFacilitiesService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FindClosestFacilitiesEventArgs> ProcessCompleted;

        private FindClosestFacilitiesResult lastResult;
        /// <summary>${iServerJava6R_ClosestFacilitiesService_attribute_lastResult_D}</summary>
        public FindClosestFacilitiesResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
