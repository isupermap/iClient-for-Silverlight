﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.Networkanalyst
{
    /// <summary>
    /// <para>${iServerJava6R_EdgeWeightParameters_Title}</para>
    /// </summary>
    public class EdgeWeightParameters
    {
        /// <summary>
        /// ${iServerJava6R_EdgeWeightParameters_constructor_D}
        /// </summary>
        public EdgeWeightParameters()
        {
            WeightField = WeightFieldType.TIME;
        }
        /// <summary>${iServerJava6R_EdgeWeightParameters_attribute_EdgeID_D}</summary>
        public int EdgeID{get;set;}
        /// <summary>${iServerJava6R_EdgeWeightParameters_attribute_FromNodeID_D}</summary>
        public int FromNodeID { get; set; }
        /// <summary>${iServerJava6R_EdgeWeightParameters_attribute_ToNodeID_D}</summary>
        public int ToNodeID { get; set; }
        /// <summary>${iServerJava6R_EdgeWeightParameters_attribute_WeightField_D}</summary>
        public WeightFieldType WeightField { get; set; }
        /// <summary>${iServerJava6R_EdgeWeightParameters_attribute_Weight_D}</summary>
        public int Weight { get; set; }
    }

   

}
