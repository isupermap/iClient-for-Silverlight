﻿

namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_SupplyCenterType_Title}</para>
    /// 	<para>${iServerJava6R_SupplyCenterType_Description}</para>
    /// </summary>
    public enum SupplyCenterType
    {
        /// <summary>${iServerJava6R_SupplyCenter_attribute_FIXEDCENTER_D}</summary>
        FIXEDCENTER,
        /// <summary>${iServerJava6R_SupplyCenter_attribute_NULL_D}</summary>
        NULL,
        /// <summary>${iServerJava6R_SupplyCenter_attribute_OPTIONALCENTER_D}</summary>
        OPTIONALCENTER
    }
}
