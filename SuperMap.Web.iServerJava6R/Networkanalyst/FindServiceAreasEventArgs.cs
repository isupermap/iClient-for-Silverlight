﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServiceAreasEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_ServiceAreasEventArgs_Description}</para>
    /// </summary>
    public class FindServiceAreasEventArgs:ServiceEventArgs
    {
        /// <summary>${iServerJava6R_ServiceAreasEventArgs_constructor_D}</summary>
        public FindServiceAreasEventArgs(FindServiceAreasResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_ServiceAreasEventArgs_attribute_Result_D}</summary>
        public FindServiceAreasResult Result { get; private set; }
        /// <summary>${iServerJava6R_ServiceAreasEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
