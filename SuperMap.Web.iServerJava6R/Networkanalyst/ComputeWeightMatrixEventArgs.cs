﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ComputeWeightMatrixEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_ComputeWeightMatrixEventArgs_Description}</para>
    /// </summary>
    public class ComputeWeightMatrixEventArgs:ServiceEventArgs
    {
        /// <summary>
        /// 	<para>${iServerJava6R_ComputeWeightMatrixEventArgs_Title}</para>
        /// 	<para>${iServerJava6R_ComputeWeightMatrixEventArgs_Description}</para>
        /// </summary>
        public ComputeWeightMatrixEventArgs(ComputeWeightMatrixResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_ComputeWeightMatrixEventArgs_attribute_Result_D}</summary>
        public ComputeWeightMatrixResult Result { get; private set; }
        /// <summary>${iServerJava6R_ComputeWeightMatrixEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
