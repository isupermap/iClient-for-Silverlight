﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindMTSPPathsResult_Title}</para>
    /// 	<para>${iServerJava6R_FindMTSPPathsResult_Description}</para>
    /// </summary>
    public class FindMTSPPathsResult
    {
        internal FindMTSPPathsResult()
        { }
        ///// <summary>${iServerJava6R_FindMTSPPathsResult_attribute_MapImage_D}</summary>
        //public NAResultMapImage MapImage { get; private set; }
        /// <summary>${iServerJava6R_FindMTSPPathsResult_attribute_MTSPathList_D}</summary>
        public List<MTSPPath> MTSPathList { get; private set; }


        /// <summary>${iServerJava6R_FindMTSPPathsResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_FindMTSPPathsResult_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_FindMTSPPathsResult_method_fromJson_param_jsonObject}</param>
        public static FindMTSPPathsResult FromJson(JsonObject json)
        {
            if (json == null) return null;

            FindMTSPPathsResult result = new FindMTSPPathsResult();

            //result.MapImage = NAResultMapImage.FromJson((JsonObject)json["mapImage"]);
            if (json["pathList"] != null)
            {
                result.MTSPathList = new List<MTSPPath>();
                for (int i = 0; i < json["pathList"].Count; i++)
                {
                    result.MTSPathList.Add(MTSPPath.FromJson((JsonObject)json["pathList"][i]));
                }
            }

            return result;
        }
    }
}
