﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R.Networkanalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_WeightFieldType_Title}</para>
    /// </summary>
    public enum WeightFieldType
    {
        /// <summary>${iServerJava6R_WeightFieldType_attribute_TIME_D}</summary>
        TIME,
        /// <summary>${iServerJava6R_WeightFieldType_attribute_LENGTH_D}</summary>
        LENGTH
    }
}
