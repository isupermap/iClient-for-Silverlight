﻿

using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindPathResult_Title}</para>
    /// 	<para>${iServerJava6R_FindPathResult_Description}</para>
    /// </summary>
    public class FindPathResult
    {
        internal FindPathResult()
        { }

        ///// <summary>${iServerJava6R_FindPathResult_attribute_mapImage_D}</summary>
        //public NAResultMapImage MapImage { get; private set; }
        /// <summary>${iServerJava6R_FindPathResult_attribute_pathList_D}</summary>
        public List<ServerPath> PathList { get; private set; }


        /// <summary>${iServerJava6R_FindPathResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_FindPathResult_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_FindPathResult_method_fromJson_param_jsonObject}</param>
        public static FindPathResult FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            FindPathResult result = new FindPathResult();

           // result.MapImage = NAResultMapImage.FromJson((JsonObject)json["mapImage"]);
            if (json["pathList"] != null)
            {
                result.PathList = new List<ServerPath>();
                for (int i = 0; i < json["pathList"].Count; i++)
                {
                    result.PathList.Add(ServerPath.ServerPathFromJson((JsonObject)json["pathList"][i]));
                }
            }

            return result;
        }
    }
}
