﻿

using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ComputeWeightMatrixParameters_Title}</para>
    /// 	<para>${iServerJava6R_ComputeWeightMatrixParameters_Description}</para>
    /// </summary>
    /// <typeparam name="T">${iServerJava6R_ComputeWeightMatrixParameters_param_T}</typeparam>
    public class ComputeWeightMatrixParameters<T>
    {
        /// <summary>${iServerJava6R_ComputeWeightMatrixParameters_constructor_D}</summary>
        public ComputeWeightMatrixParameters()
        {
        }
        /// <summary>${iServerJava6R_ComputeWeightMatrixParameters_attribute_Nodes_D}</summary>
        public IList<T> Nodes { get; set; }

        /// <summary>${iServerJava6R_ComputeWeightMatrixParameters_attribute_parameter_D}</summary>
        public TransportationAnalystParameter Parameter { get; set; }
    }
}
