﻿

using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_Path_Title}</para>
    /// 	<para>${iServerJava6R_Path_Description}</para>
    /// </summary>
    public class ServerPath
    {
        /// <summary>${iServerJava6R_Path_constructor_D}</summary>
        internal ServerPath()
        { }
        /// <summary>${iServerJava6R_Path_attribute_edgeFeatures_D}</summary>
        public FeatureCollection EdgeFeatures { get; protected set; }
        /// <summary>${iServerJava6R_Path_attribute_edgeIDs_D}</summary>
        public List<int> EdgeIDs { get; protected set; }
        /// <summary>${iServerJava6R_Path_attribute_nodeFeatures_D}</summary>
        public FeatureCollection NodeFeatures { get; protected set; }
        /// <summary>${iServerJava6R_Path_attribute_nodeIDs_D}</summary>
        public List<int> NodeIDs { get; protected set; }
        /// <summary>${iServerJava6R_Path_attribute_pathGuideItems_D}</summary>
        public List<PathGuideItem> PathGuideItems { get; protected set; }
        /// <summary>${iServerJava6R_Path_attribute_route_D}</summary>
        public Route Route { get; protected set; }
        /// <summary>${iServerJava6R_Path_attribute_stopWeights_D}</summary>
        public List<double> StopWeights { get; protected set; }
        /// <summary>${iServerJava6R_Path_attribute_weight_D}</summary>
        public double Weight { get; protected set; }
        /// <summary>${iServerJava6R_Path_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_Path_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_Path_method_fromJson_param_jsonObject}</param>
        public static ServerPath ServerPathFromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            ServerPath result = new ServerPath();
            if (json["edgeFeatures"] != null)
            {
                result.EdgeFeatures = new FeatureCollection();
                for (int i = 0; i < json["edgeFeatures"].Count; i++)
                {
                    result.EdgeFeatures.Add(ServerFeature.FromJson((JsonObject)json["edgeFeatures"][i]).ToFeature());
                }
            }
            if (json["edgeIDs"] != null)
            {
                result.EdgeIDs = new List<int>();
                for (int i = 0; i < json["edgeIDs"].Count; i++)
                {
                    result.EdgeIDs.Add((int)json["edgeIDs"][i]);
                }
            }
            if (json["nodeFeatures"] != null)
            {
                result.NodeFeatures = new FeatureCollection();
                for (int i = 0; i < json["nodeFeatures"].Count; i++)
                {
                    result.NodeFeatures.Add(ServerFeature.FromJson((JsonObject)json["nodeFeatures"][i]).ToFeature());
                }
            }
            if (json["nodeIDs"] != null)
            {
                result.NodeIDs = new List<int>();
                for (int i = 0; i < json["nodeIDs"].Count; i++)
                {
                    result.NodeIDs.Add((int)json["nodeIDs"][i]);
                }
            }
            if (json["pathGuideItems"] != null)
            {
                result.PathGuideItems = new List<PathGuideItem>();
                for (int i = 0; i < json["pathGuideItems"].Count; i++)
                {
                    result.PathGuideItems.Add(PathGuideItem.FromJson((JsonObject)json["pathGuideItems"][i]));
                }
            }
            if (json["route"] != null)
            {
                result.Route = ServerGeometry.FromJson((JsonObject)json["route"]).ToRoute();
            }
            if (json["stopWeights"] != null)
            {
                result.StopWeights = new List<double>();
                for (int i = 0; i < json["stopWeights"].Count; i++)
                {
                    result.StopWeights.Add((double)json["stopWeights"][i]);
                }
            }
            result.Weight = (double)json["weight"];

            return result;
        }


    }
}
