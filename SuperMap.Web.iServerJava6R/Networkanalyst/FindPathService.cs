﻿

using SuperMap.Web.Service;
using SuperMap.Web.Core;
using System;
using SuperMap.Web.iServerJava6R.Resources;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindPathService_Title}</para>
    /// 	<para>${iServerJava6R_FindPathService_Description}</para>
    /// </summary>
    public class FindPathService : ServiceBase
    {
        /// <summary>${iServerJava6R_FindPathService_constructor_D}</summary>
        /// <overloads>
        /// 	<div>
        ///         ${iServerJava6R_FindPathService_constructor_overloads_D}
        ///     </div>
        /// </overloads>
        public FindPathService()
        { }
        /// <summary>${iServerJava6R_FindPathService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_FindPathService_constructor_param_url}</param>
        public FindPathService(string url)
            : base(url)
        { }
        /// <summary>${iServerJava6R_FindPathService_method_ProcessAsync_Point2D_D}</summary>
        /// <overloads>${iServerJava6R_FindPathService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync<T>(T parameters)
        {
            ProcessAsync<T>(parameters, null);
        }
        /// <summary>${iServerJava6R_FindPathService_method_ProcessAsync_Point2D_D}</summary>
        /// <param name="parameters">${iServerJava6R_FindPathService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${iServerJava6R_FindPathService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync<T>(T parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.Url += "path.json?debug=true&_method=GET";

            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true);
        }
        private Dictionary<string, string> GetParameters<T>(T parameters)
        {
            if (parameters is FindPathParameters<Point2D>)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                if ((parameters as FindPathParameters<Point2D>).Nodes != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindPathParameters<Point2D>).Nodes.Count; i++)
                    {
                        temp.Add(string.Format("{0}", JsonHelper.FromPoint2D((parameters as FindPathParameters<Point2D>).Nodes[i])));
                    }
                    dictionary.Add("nodes", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }

                dictionary.Add("hasLeastEdgeCount", (parameters as FindPathParameters<Point2D>).HasLeastEdgeCount.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
                dictionary.Add("parameter", TransportationAnalystParameter.ToJson((parameters as FindPathParameters<Point2D>).Parameter));

                return dictionary;
            }
            else if (parameters is FindPathParameters<int>)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                if ((parameters as FindPathParameters<int>).Nodes != null)
                {
                    List<string> temp = new List<string>();
                    for (int i = 0; i < (parameters as FindPathParameters<int>).Nodes.Count; i++)
                    {
                        temp.Add((parameters as FindPathParameters<int>).Nodes[i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                    }
                    dictionary.Add("nodes", string.Format("[{0}]", string.Join(",", temp.ToArray())));
                }

                dictionary.Add("hasLeastEdgeCount", (parameters as FindPathParameters<int>).HasLeastEdgeCount.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower());
                dictionary.Add("parameter", TransportationAnalystParameter.ToJson((parameters as FindPathParameters<int>).Parameter));

                return dictionary;
            }
            else
            {
                throw new ArgumentException("参数只能为FindPathParameters<int>或者FindPathParameters<Point2D>类型！");
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            FindPathResult result = FindPathResult.FromJson(jsonObject);
            LastResult = result;
            FindPathEventArgs args = new FindPathEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(FindPathEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_FindPathService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<FindPathEventArgs> ProcessCompleted;

        private FindPathResult lastResult;
        /// <summary>${iServerJava6R_FindPathService_attribute_lastResult_D}</summary>
        public FindPathResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
