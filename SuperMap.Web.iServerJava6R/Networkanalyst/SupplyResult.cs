﻿

using System.Json;
using System;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_SupplyResult_Title}</para>
    /// 	<para>${iServerJava6R_SupplyResult_Description}</para>
    /// </summary>
    public class SupplyResult
    {
        internal SupplyResult()
        { }

        /// <summary><para>${iServerJava6R_SupplyResult_attribute_ActualResourceValue_D}</para></summary>
        public double ActualResourceValue { get; private set; }
        /// <summary>${iServerJava6R_SupplyResult_attribute_AverageWeight_D}</summary>
        public double AverageWeight { get; private set; }
        /// <summary>${iServerJava6R_SupplyResult_attribute_DemandCount_D}</summary>
        public int DemandCount { get; private set; }
        /// <summary>${iServerJava6R_SupplyResult_attribute_MaxWeight_D}</summary>
        public double MaxWeight { get; private set; }
        /// <summary>${iServerJava6R_SupplyResult_attribute_NodeID_D}</summary>
        public int NodeID { get; private set; }
        /// <summary>${iServerJava6R_SupplyResult_attribute_ResourceValue_D}</summary>
        public double ResourceValue { get; private set; }
        /// <summary>${iServerJava6R_SupplyResult_attribute_TotalWeights_D}</summary>
        public double TotalWeights { get; private set; }
        /// <summary>${iServerJava6R_SupplyResult_attribute_Type_D}</summary>
        public SupplyCenterType Type { get; private set; }

        /// <summary>${iServerJava6R_SupplyResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_SupplyResult_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_SupplyResult_method_fromJson_param_jsonObject}</param>
        public static SupplyResult FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            SupplyResult result = new SupplyResult();
            result.ActualResourceValue = (double)json["actualResourceValue"];
            result.AverageWeight = (double)json["averageWeight"];
            result.DemandCount = (int)json["demandCount"];
            result.MaxWeight = (double)json["maxWeight"];
            result.NodeID = (int)json["nodeID"];
            result.ResourceValue = (double)json["resourceValue"];
            result.TotalWeights = (double)json["totalWeights"];
            result.Type = (SupplyCenterType)Enum.Parse(typeof(SupplyCenterType), (string)json["type"], true);

            return result;
        }
    }
}
