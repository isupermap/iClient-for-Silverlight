﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ClosestFacilityResult_Title}</para>
    /// 	<para>${iServerJava6R_ClosestFacilityResult_Description}</para>
    /// </summary>
    public class FindClosestFacilitiesResult
    {
        internal FindClosestFacilitiesResult()
        { }

        ///// <summary>${iServerJava6R_ClosestFacilitiesEventArgs_attribute_MapImage_D}</summary>
        //public NAResultMapImage MapImage { get; private set; }

        /// <summary>${iServerJava6R_ClosestFacilitiesEventArgs_attribute_FacilityPathList_D}</summary>
        public List<ClosestFacilityPath> FacilityPathList { get; private set; }

        /// <summary>${iServerJava6R_ClosestFacilityResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_ClosestFacilityResult_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_ClosestFacilityResult_method_fromJson_param_jsonObject}</param>
        public static FindClosestFacilitiesResult FromJson(JsonObject json)
        {
            if (json != null)
            {
                FindClosestFacilitiesResult result = new FindClosestFacilitiesResult();
                //result.MapImage = NAResultMapImage.FromJson((JsonObject)json["mapImage"]);

                if (json["facilityPathList"] != null)
                {
                    result.FacilityPathList = new List<ClosestFacilityPath>();
                    for (int i = 0; i < json["facilityPathList"].Count; i++)
                    {
                        result.FacilityPathList.Add(ClosestFacilityPath.FromJson((JsonObject)json["facilityPathList"][i]));
                    }
                }

                return result;
            }

            return null;
        }


    }
}
