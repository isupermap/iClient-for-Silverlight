﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using System.Json;
using SuperMap.Web.iServerJava6R.Resources;
using System.Windows.Browser;

namespace SuperMap.Web.iServerJava6R.Networkanalyst
{
    /// <summary>
    /// <para>${iServerJava6R_UpdateEdgeWeightService_Title}</para>
    /// </summary>
    public class UpdateEdgeWeightService:ServiceBase
    {
        /// <summary>
        /// ${iServerJava6R_UpdateEdgeWeightService_constructor_D}
        /// </summary>
        public UpdateEdgeWeightService()
        {
        }
        /// <summary>${iServerJava6R_UpdateEdgeWeightService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_UpdateEdgeWeightService_constructor_param_url}</param>
        public UpdateEdgeWeightService(string url)
            :base(url)
        {
        }
        /// <summary>${iServerJava6R_UpdateEdgeWeightService_method_ProcessAsync_D}</summary>
        public void ProcessAsync(EdgeWeightParameters parameters)
        {
            //url=http://192.168.11.11:8090/iserver/services/components-rest/rest/networkanalyst/RoadNet@Changchun;
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (base.Url.EndsWith("/"))
            {
                base.Url = base.Url.TrimEnd('/');
            }

            base.Url += "/edgeweight/" + parameters.EdgeID + "/fromnode/" + parameters.FromNodeID + "/tonode/" + parameters.ToNodeID + "/weightfield/" + parameters.WeightField.ToString().ToLower() + ".json?_method=PUT&debug=true";
           // base.SubmitRequest(base.Url, null, new EventHandler<RequestEventArgs>(request_Completed), false, true, false);
            base.SubmitRequest(base.Url, parameters.Weight.ToString(), new EventHandler<RequestEventArgs>(request_Completed), false, true, false);
        }
        
        private void request_Completed(object sender, EventArgs e)
        {
            OnProcessCompleted(new EventArgs());
        }
        /// <summary>${iServerJava6R_UpdateEdgeWeightService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<EventArgs> ProcessCompleted;
        private void OnProcessCompleted(EventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

       
    }
}
