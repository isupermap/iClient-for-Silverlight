﻿

using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTSPPathsParameters_Title}</para>
    /// 	<para>${iServerJava6R_FindTSPPathsParameters_Description}</para>
    /// </summary>
    /// <typeparam name="T">${iServerJava6R_FindTSPPathsParameters_param_T}</typeparam>
    public class FindTSPPathsParameters<T>
    {
        /// <summary>${iServerJava6R_FindTSPPathsParameters_constructor_D}</summary>
        public FindTSPPathsParameters()
        {
        }
        /// <summary>${iServerJava6R_FindTSPPathsParameters_attribute_Nodes_D}</summary>
        public List<T> Nodes { get; set; }

        /// <summary>${iServerJava6R_FindTSPPathsParameters_attribute_EndNodeAssigned_D}</summary>
        public bool EndNodeAssigned { get; set; }
        /// <summary>${iServerJava6R_FindTSPPathsParameters_attribute_Parameter_D}</summary>
        public TransportationAnalystParameter Parameter { get; set; }
    }
}
