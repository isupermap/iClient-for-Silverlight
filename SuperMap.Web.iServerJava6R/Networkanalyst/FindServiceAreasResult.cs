﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServiceAreasResult_Title}</para>
    /// 	<para>${iServerJava6R_ServiceAreasResult_Description}</para>
    /// </summary>
    public class FindServiceAreasResult
    {
        internal FindServiceAreasResult()
        {
        }
        ///// <summary>${iServerJava6R_ServiceAreasResult_attribute_MapImage_D}</summary>
        //public NAResultMapImage MapImage { get; private set; }
        /// <summary>${iServerJava6R_ServiceAreasResult_attribute_ServiceAreaList_D}</summary>
        public List<ServiceArea> ServiceAreaList { get; private set; }

        /// <summary>${iServerJava6R_ServiceAreasResult_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_ServiceAreasResult_method_FromJson_return}</returns>
        /// <param name="json">${iServerJava6R_ServiceAreasResult_method_FromJson_param_jsonObject}</param>
        public static FindServiceAreasResult FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            FindServiceAreasResult result = new FindServiceAreasResult();
            //result.MapImage = NAResultMapImage.FromJson((JsonObject)json["mapImage"]);
            if (json["serviceAreaList"] != null && json["serviceAreaList"].Count > 0)
            {
                result.ServiceAreaList = new List<ServiceArea>();
                for (int i = 0; i < json["serviceAreaList"].Count; i++)
                {
                    result.ServiceAreaList.Add(ServiceArea.FromJson((JsonObject)json["serviceAreaList"][i]));
                }
            }

            return result;
        }
    }
}
