﻿

using SuperMap.Web.Core;
using System.Json;
using SuperMap.Web.Utilities;
using System;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_PathGuideItem_Title}</para>
    /// 	<para>${iServerJava6R_PathGuideItem_Description}</para>
    /// </summary>
    public class PathGuideItem
    {
        internal PathGuideItem()
        {
        }

        /// <summary>${iServerJava6R_PathGuideItem_attribute_Bounds_D}</summary>
        public Rectangle2D Bounds { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_DirectionType_D}</summary>
        public DirectionType DirectionType { get; private set; }
        /// <summary><para>${iServerJava6R_PathGuideItem_attribute_Distance_D}</para>
        /// <para><img src="tolerance.png"/></para>
        /// </summary>
        public double Distance { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_ID_D}</summary>
        public int ID { get; private set; }
        /// <summary><para>${iServerJava6R_PathGuideItem_attribute_Index_D}</para>
        ///         <para><img src="PathGuideItemIndex.png"/></para>
        /// </summary>
        public int Index { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_IsEdge_D}</summary>
        public bool IsEdge { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_isStop_D}</summary>
        public bool IsStop { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_Length_D}</summary>
        public double Length { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_Name_D}</summary>
        public string Name { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_SideType_D}</summary>
        public SideType SideType { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_TurnAngle_D}</summary>
        public double TurnAngle { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_TurnType_D}</summary>
        public TurnType TurnType { get; private set; }
        /// <summary>${iServerJava6R_PathGuideItem_attribute_Weight_D}</summary>
        public double Weight { get; private set; }

        /// <summary>${iServerJava6R_PathGuideItem_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_PathGuideItem_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_PathGuideItem_method_fromJson_param_jsonObject}</param>
        public static PathGuideItem FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            PathGuideItem item = new PathGuideItem();

            if (json.ContainsKey("bounds"))
            {
                item.Bounds = JsonHelper.ToRectangle2D((JsonObject)json["bounds"]);
            }

            if (json.ContainsKey("directionType"))
            {
                item.DirectionType = (DirectionType)Enum.Parse(typeof(DirectionType), (string)json["directionType"], true);
            }

            if (json.ContainsKey("distance"))
            {
                item.Distance = (double)json["distance"];
            }

            if (json.ContainsKey("id"))
            {
                item.ID = (int)json["id"];
            }

            if (json.ContainsKey("index"))
            {
                item.Index = (int)json["index"];
            }

            if (json.ContainsKey("isEdge"))
            {
                item.IsEdge = (bool)json["isEdge"];
            }


            if (json.ContainsKey("isStop"))
            {
                item.IsStop = (bool)json["isStop"];
            }

            if (json.ContainsKey("length"))
            {
                item.Length = (double)json["length"];
            }

            if (json.ContainsKey("name"))
            {
                item.Name = (string)json["name"];
            }

            if (json.ContainsKey("sideType"))
            {
                item.SideType = (SideType)Enum.Parse(typeof(SideType), (string)json["sideType"], true);
            }
            try
            {
                if (json.ContainsKey("turnAngle"))
                {
                    item.TurnAngle = (double)json["turnAngle"];
                }
            }
            finally
            {
                item.TurnAngle = 0.0;
            }

            if (json.ContainsKey("turnType"))
            {
                item.TurnType = (TurnType)Enum.Parse(typeof(TurnType), (string)json["turnType"], true);
            }

            if (json.ContainsKey("weight"))
            {
                item.Weight = (double)json["weight"];
            }
            return item;
        }
    }
}
