﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.Networkanalyst
{
    public class EdgeWeightEventArgs:ServiceEventArgs
    {
        public EdgeWeightEventArgs(EdgeWeightResult result,string originResult,object token)
            :base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        public EdgeWeightResult Result { get; private set; }
        public string OriginResult { get; private set; }
    }
}
