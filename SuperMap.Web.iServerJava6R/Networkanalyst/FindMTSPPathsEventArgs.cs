﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindMTSPPathsEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_FindMTSPPathsEventArgs_Description}</para>
    /// </summary>
    public class FindMTSPPathsEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_FindMTSPPathsEventArgs_constructor_D}</summary>
        public FindMTSPPathsEventArgs(FindMTSPPathsResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_FindMTSPPathsEventArgs_attribute_Result_D}</summary>
        public FindMTSPPathsResult Result { get; private set; }
        /// <summary>${iServerJava6R_FindMTSPPathsEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
