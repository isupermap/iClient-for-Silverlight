﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindTSPPathsResult_Title}</para>
    /// 	<para>${iServerJava6R_FindTSPPathsResult_Description}</para>
    /// </summary>
    public class FindTSPPathsResult
    {
        internal FindTSPPathsResult()
        {
        }
        ///// <summary>${iServerJava6R_FindTSPPathsResult_attribute_MapImage_D}</summary>
        //public NAResultMapImage MapImage { get; private set; }
        /// <summary>${iServerJava6R_FindTSPPathsResult_attribute_tspPathList_D}</summary>
        public List<TSPPath> TSPPathList { get; private set; }

        /// <summary>${iServerJava6R_FindTSPPathsResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_FindTSPPathsResult_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_FindTSPPathsResult_method_fromJson_param_jsonObject}</param>
        public static FindTSPPathsResult FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            FindTSPPathsResult result = new FindTSPPathsResult();
            //result.MapImage = NAResultMapImage.FromJson((JsonObject)json["mapImage"]);
            if (json["tspPathList"] != null && json["tspPathList"].Count > 0)
            {
                result.TSPPathList = new List<TSPPath>();
                for (int i = 0; i < json["tspPathList"].Count; i++)
                {
                    result.TSPPathList.Add(TSPPath.TSPPathFromJson((JsonObject)json["tspPathList"][i]));
                }
            }
            return result;
        }
    }
}
