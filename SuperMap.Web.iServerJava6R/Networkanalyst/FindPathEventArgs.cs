﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R.NetworkAnalyst
{
    /// <summary>
    /// 	<para>${iServerJava6R_FindPathEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_FindPathEventArgs_Description}</para>
    /// </summary>
    public class FindPathEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_FindPathEventArgs_constructor_D}</summary>
        public FindPathEventArgs(FindPathResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_FindPathEventArgs_attribute_Result_D}</summary>
        public FindPathResult Result { get; private set; }
        /// <summary>${iServerJava6R_FindPathEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
