﻿

using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Utilities;
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServerType_ServerFeature_Tile}</para>
    /// 	<para>${iServerJava6R_ServerType_ServerFeature_Description}</para>
    /// </summary>
    public class ServerFeature
    {
        /// <summary>${iServerJava6R_ServerType_ServerFeature_constructor_D}</summary>
        public ServerFeature()
        {
            FieldNames = new List<string>();
            FieldValues = new List<string>();
        }

        internal int ID { get; set; }
        /// <summary>${iServerJava6R_ServerType_ServerFeature_attribute_FieldNames_D}</summary>
        public List<string> FieldNames { get; set; }
        /// <summary>${iServerJava6R_ServerType_ServerFeature_attribute_FieldValues_D}</summary>
        public List<string> FieldValues { get; set; }
        /// <summary>${iServerJava6R_ServerType_ServerFeature_attribute_Geometry_D}</summary>
        public ServerGeometry Geometry { get; set; }
        /// <summary>${iServerJava6R_ServerType_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_ServerType_method_FromJson_return}</returns>
        /// <param name="json">${iServerJava6R_ServerType_method_FromJson_param_jsonObject}</param>
        public static ServerFeature FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }

            return new ServerFeature
            {
                FieldNames = JsonHelper.ToStringList((JsonArray)json["fieldNames"]),
                FieldValues = JsonHelper.ToStringList((JsonArray)json["fieldValues"]),
                Geometry = ServerGeometry.FromJson((JsonObject)json["geometry"])
            };
        }
    }
}
