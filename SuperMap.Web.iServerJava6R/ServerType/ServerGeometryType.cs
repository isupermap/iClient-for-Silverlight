﻿
namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServerType_GeometryType_Tile}</para>
    /// 	<para>${iServerJava6R_ServerType_GeometryType_Description}</para>
    /// </summary>
    public enum ServerGeometryType 
    {
        /// <summary>${iServerJava6R_ServerType_GeometryType_attribute_LINE_D}</summary>
        LINE,
        /// <summary>${iServerJava6R_ServerType_GeometryType_attribute_LINEM_D}</summary>
        LINEM,
        /// <summary>${iServerJava6R_ServerType_GeometryType_attribute_POINT_D}</summary>
        POINT,
        /// <summary>${iServerJava6R_ServerType_GeometryType_attribute_REGION_D}</summary>
        REGION,
        /// <summary>${iServerJava6R_ServerType_GeometryType_attribute_TEXT_D}</summary>
        TEXT,
        /// <summary>${iServerJava6R_ServerType_GeometryType_attribute_UNKNOWN_D}</summary>
        UNKNOWN
    }
}
