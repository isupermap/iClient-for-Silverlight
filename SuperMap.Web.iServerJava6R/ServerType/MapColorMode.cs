﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServerType_MapColorMode_Tile}</para>
    /// 	<para>${iServerJava6R_ServerType_MapColorMode_Description}</para>
    /// </summary>
    public enum MapColorMode
    {
        /// <summary>${iServerJava6R_ServerType_MapColorMode_attribute_BLACK_WHITE_REVERSE_D}</summary>
        BLACK_WHITE_REVERSE,
        /// <summary>${iServerJava6R_ServerType_MapColorMode_attribute_BLACKWHITE_D}</summary>
        BLACKWHITE,
        /// <summary>${iServerJava6R_ServerType_MapColorMode_attribute_DEFAULT_D}</summary>
        DEFAULT,
        /// <summary>${iServerJava6R_ServerType_MapColorMode_attribute_GRAY_D}</summary>
        GRAY,
        /// <summary>${iServerJava6R_ServerType_MapColorMode_attribute_ONLY_BLACK_WHITE_REVERSE_D}</summary>
        ONLY_BLACK_WHITE_REVERSE
    }
}
