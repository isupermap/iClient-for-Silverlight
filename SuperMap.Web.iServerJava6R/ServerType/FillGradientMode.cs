﻿

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServerType_FillGradientMode_Tile}</para>
    /// 	<para>${iServerJava6R_ServerType_FillGradientMode_Description}</para>
    /// </summary>
    public enum FillGradientMode
    {
        /// <summary>${iServerJava6R_ServerType_FillGradientMode_attribute_NONE_D}</summary>
        NONE,
        /// <summary>
        /// <para>${iServerJava6R_ServerType_FillGradientMode_attribute_LINEAR_D}</para>
        /// <para><img src="fillGradientModeLinear.bmp"/></para>
        /// </summary>
        LINEAR,
        /// <summary>
        /// <para>${iServerJava6R_ServerType_FillGradientMode_attribute_RADIAL_D}</para>
        /// <para><img src="fillGradientModeRadial.bmp"/></para>
        /// </summary>
        RADIAL,
        /// <summary>
        /// <para>${iServerJava6R_ServerType_FillGradientMode_attribute_CONICAL_D}</para>
        /// <para><img src="fillGradientModeConical.bmp"/></para>
        /// </summary>
        CONICAL,
        /// <summary>
        /// <para>${iServerJava6R_ServerType_FillGradientMode_attribute_SQUARE_D}</para>
        /// <para><img src="fillGradientModeSquare.bmp"/></para>
        /// </summary>
        SQUARE,

    }
}
