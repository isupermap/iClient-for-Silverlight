﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServerType_ServerRoute_Tile}</para>
    /// 	<para>${iServerJava6R_ServerType_ServerRoute_Description}</para>
    /// </summary>
	public class ServerRoute:ServerGeometry
    {
        internal ServerRoute()
        { }

        /// <summary>${iServerJava6R_Route_attribute_Length_D}</summary>
        public double Length { get; set; }
        /// <summary>${iServerJava6R_Route_attribute_MaxM_D}</summary>
        public double MaxM { get; set; }
        /// <summary>${iServerJava6R_Route_attribute_MinM_D}</summary>
        public double MinM { get; set; }
    }
}
