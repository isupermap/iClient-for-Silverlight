﻿using SuperMap.Web.Core;
using System.Collections.Generic;
using System.Json;
using System;
using SuperMap.Web.Utilities;
using System.Globalization;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using System.Collections.ObjectModel;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ServerType_ServerGeometry_Tile}</para>
    /// 	<para>${iServerJava6R_ServerType_ServerGeometry_Description}</para>
    /// </summary>
    public class ServerGeometry
    {
        internal ServerGeometry()
        { }

        /// <summary>${iServerJava6R_ServerType_ServerGeometry_attribute_id_D}</summary>
        public int ID { get; set; }
        /// <summary>${iServerJava6R_ServerType_ServerGeometry_attribute_parts_D}</summary>
        public IList<int> Parts { get; set; }
        /// <summary>${iServerJava6R_ServerType_ServerGeometry_attribute_Points_D}</summary>
        public Point2DCollection Points { get; set; }
        /// <summary>${iServerJava6R_ServerType_ServerGeometry_attribute_Style_D}</summary>
        public ServerStyle Style { get; set; }
        /// <summary>${iServerJava6R_ServerType_ServerGeometry_attribute_type_D}</summary>
        public ServerGeometryType Type { get; set; }

        internal static string ToJson(ServerGeometry serverGeometry)
        {
            if (serverGeometry == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format(CultureInfo.InvariantCulture, "\"type\":\"{0}\"", serverGeometry.Type));
            list.Add(string.Format(CultureInfo.InvariantCulture, "\"id\":{0}", serverGeometry.ID));

            if (serverGeometry.Parts != null && serverGeometry.Parts.Count > 0)
            {
                List<string> parts = new List<string>();
                foreach (int i in serverGeometry.Parts)
                {
                    parts.Add(i.ToString(CultureInfo.InvariantCulture));
                }
                list.Add(string.Format(CultureInfo.InvariantCulture, "\"parts\":[{0}]", string.Join(",", parts.ToArray())));
            }
            else
            {
                list.Add(string.Format(CultureInfo.InvariantCulture, "\"parts\":null"));
            }
            if (serverGeometry.Points != null && serverGeometry.Points.Count > 0)
            {
                List<string> ps = new List<string>();
                foreach (Point2D p in serverGeometry.Points)
                {
                    string pointJson;
                    if (serverGeometry.Type == ServerGeometryType.LINEM)
                    {
                        pointJson = JsonHelper.FromPoint2DWithTag(p, "measure", true);
                    }
                    else
                    {
                        pointJson = JsonHelper.FromPoint2D(p);
                    }
                    ps.Add(pointJson);
                }
                list.Add(string.Format(CultureInfo.InvariantCulture, "\"points\":[{0}]", string.Join(",", ps.ToArray())));
            }
            else
            {
                list.Add(string.Format(CultureInfo.InvariantCulture, "\"points\":null"));
            }

            if (serverGeometry.Style != null)
            {
                list.Add(string.Format(CultureInfo.InvariantCulture, "\"style\":{0}", ServerStyle.ToJson(serverGeometry.Style)));
            }
            else
            {
                list.Add(string.Format(CultureInfo.InvariantCulture, "\"style\":null"));
            }

            if (serverGeometry.Type == ServerGeometryType.LINEM)
            {
                list.Add(string.Format(CultureInfo.InvariantCulture, string.Format("\"length\":{0}", ((ServerRoute)serverGeometry).Length.ToStringEx())));
                list.Add(string.Format(CultureInfo.InvariantCulture, string.Format("\"maxM\":{0}", ((ServerRoute)serverGeometry).MaxM.ToStringEx())));
                list.Add(string.Format(CultureInfo.InvariantCulture, string.Format("\"minM\":{0}", ((ServerRoute)serverGeometry).MinM.ToStringEx())));
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
        /// <summary>${iServerJava6R_ServerType_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_ServerType_method_FromJson_return}</returns>
        /// <param name="json">${iServerJava6R_ServerType_method_FromJson_param_jsonObject}</param>
        public static ServerGeometry FromJson(JsonObject json)
        {
            if (json == null)
            {
                return null;
            }
            ServerGeometryType type;
            bool isSuccess = Enum.TryParse<ServerGeometryType>((string)json["type"], true, out type);

            ServerGeometry geometry;
            if (type == ServerGeometryType.LINEM)
            {
                geometry = new ServerRoute();
                ((ServerRoute)geometry).Length = (double)json["length"];
                ((ServerRoute)geometry).MaxM = (double)json["maxM"];
                ((ServerRoute)geometry).MinM = (double)json["minM"];
            }
            else
            {
                geometry = new ServerGeometry();
            }
            
            if (isSuccess)
            {
                geometry.Type = type;
            }
            else
            {
                return geometry;
            }
            geometry.ID = (int)json["id"];
            geometry.Style = ServerStyle.FromJson((JsonObject)json["style"]);

            JsonArray parts = (JsonArray)json["parts"];
            if (parts != null && parts.Count > 0)
            {
                geometry.Parts = new List<int>();
                for (int i = 0; i < parts.Count; i++)
                {
                    geometry.Parts.Add((int)parts[i]);
                }
            }

            JsonArray point2Ds = (JsonArray)json["points"];
            if (point2Ds != null && point2Ds.Count > 0)
            {
                geometry.Points = new Point2DCollection();
                for (int i = 0; i < point2Ds.Count; i++)
                {
                    Point2D point = JsonHelper.ToPoint2D((JsonObject)point2Ds[i]);
                    if (type == ServerGeometryType.LINEM)
                    {
                        if (point != Point2D.Empty && ((JsonObject)point2Ds[i]).ContainsKey("measure"))
                        {
                            point.Tag = (double)(((JsonObject)point2Ds[i])["measure"]);
                        }
                    }
                    geometry.Points.Add(point);
                }
            }

            return geometry;
        }

        internal GeoPoint ToGeoPoint()
        {
            if (Points != null && Type == ServerGeometryType.POINT)
            {
                return new GeoPoint(Points[0].X, Points[0].Y);
            }
            return null;
        }

        internal Route ToRoute()
        {
            if (this.Parts != null)
            {
                Route route = new Route();
                route.Parts = new ObservableCollection<Point2DCollection>();
                int index = 0;
                foreach (var part in this.Parts)
                {
                    Point2DCollection collection = new Point2DCollection();
                    for (int i = index; i < index + part; i++)
                    {
                        collection.Add(this.Points[i]);
                    }
                    route.Parts.Add(collection);
                }
                route.MinM = ((ServerRoute)this).MinM;
                route.MaxM = ((ServerRoute)this).MaxM;
                route.Length = ((ServerRoute)this).Length;
                return route;
            }
            return null;
        }

        internal GeoLine ToGeoLine()
        {
            if (this.Parts != null)
            {
                List<Point2DCollection> pss = new List<Point2DCollection>();
                Point2DCollection copy = new Point2DCollection();
                foreach (Point2D item in this.Points)
                {
                    copy.Add(item);
                }
                for (int i = 0; i < this.Parts.Count; i++)
                {
                    Point2DCollection temp = new Point2DCollection();
                    for (int j = 0; j < this.Parts[i]; j++)
                    {
                        temp.Add(copy[j]);
                    }
                    pss.Add(temp);

                    copy.RemoveRange(0, this.Parts[i]);
                }

                GeoLine line = new GeoLine();
                foreach (Point2DCollection item in pss)
                {
                    line.Parts.Add(item);
                }
                return line;
            }
            return null;
        }

        internal GeoRegion ToGeoRegion()
        {
            if (this.Parts != null)
            {
                List<Point2DCollection> pss = new List<Point2DCollection>();
                Point2DCollection copy = new Point2DCollection();
                foreach (Point2D item in this.Points)
                {
                    copy.Add(item);
                }
                for (int i = 0; i < this.Parts.Count; i++)
                {
                    Point2DCollection temp = new Point2DCollection();
                    for (int j = 0; j < this.Parts[i]; j++)
                    {
                        temp.Add(copy[j]);
                    }
                    pss.Add(temp);
                    copy.RemoveRange(0, this.Parts[i]);
                }

                GeoRegion region = new GeoRegion();
                foreach (Point2DCollection item in pss)
                {
                    region.Parts.Add(item);
                }
                return region;
            }
            return null;
        }
    }
}
