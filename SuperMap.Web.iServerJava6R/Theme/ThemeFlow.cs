﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeFlow_Title}</para>
    /// 	<para>${iServerJava6R_ThemeFlow_Description}</para>
    /// </summary>
    public class ThemeFlow
    {
        /// <summary>${iServerJava6R_ThemeFlow_constructor_D}</summary>
        public ThemeFlow()
        {
            FlowEnabled = true;
            LeaderLineDisplayed = false;
        }

        /// <summary>
        /// <para>${iServerJava6R_ThemeFlow_attribute_FlowEnabled_D}</para>
        /// <para>
        /// 		<list type="table">
        /// 			<item>
        /// 				<term><img src="unflowiServer6.bmp"/></term>
        /// 				<description><img src="flowiServer6.bmp"/></description>
        /// 			</item>
        /// 		</list>
        /// 	</para>
        /// </summary>
        public bool FlowEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// <para>${iServerJava6R_ThemeFlow_attribute_LeaderLineStyle_D}</para>
        /// <para><img src="leaderLineiServer6.bmp"/></para>
        /// </summary>
        public ServerStyle LeaderLineStyle
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeFlow_attribute_LeaderLineDisplayed_D}</summary>
        public bool LeaderLineDisplayed
        {
            get;
            set;
        }

        internal static string ToJson(ThemeFlow flowTheme)
        {
            string json = "";
            List<string> list = new List<string>();

            if (flowTheme.LeaderLineStyle != null)
            {
                list.Add(string.Format("\"leaderLineStyle\":{0}", ServerStyle.ToJson(flowTheme.LeaderLineStyle)));
            }
            else
            {
                list.Add(string.Format("\"leaderLineStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            list.Add(string.Format("\"flowEnabled\":{0}", flowTheme.FlowEnabled.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            list.Add(string.Format("\"leaderLineDisplayed\":{0}", flowTheme.LeaderLineDisplayed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            json = string.Join(",", list.ToArray());
            return json;
        }

        internal static ThemeFlow FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeFlow flow = new ThemeFlow();

            flow.FlowEnabled = (bool)json["flowEnabled"];
            flow.LeaderLineDisplayed = (bool)json["leaderLineDisplayed"];
            if (json["leaderLineStyle"] != null)
            {
                flow.LeaderLineStyle = ServerStyle.FromJson((JsonObject)json["leaderLineStyle"]);
            }
            return flow;
        }
    }
}
