﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_GraduatedMode_Title}</para>
    /// 	<para>${iServerJava6R_GraduatedMode_Description}</para>
    /// </summary>
    public enum GraduatedMode
    {
        /// <summary>${iServerJava6R_GraduatedMode_attribute_Constant_D}</summary>
        CONSTANT,
        //常量分级模式。 
        /// <summary>${iServerJava6R_GraduatedMode_attribute_Logarithm_D}</summary>
        LOGARITHM,
        //对数分级模式。 
        /// <summary>${iServerJava6R_GraduatedMode_attribute_Squareroot_D}</summary>
        SQUAREROOT,
        //平方根分级模式。 

    }
}
