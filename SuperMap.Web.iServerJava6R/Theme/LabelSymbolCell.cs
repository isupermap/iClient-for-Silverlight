﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_LabelSymbolCell_Title}</para>
    /// 	<para>${iServerJava6R_LabelSymbolCell_Description}</para>
    /// 	<para><img src="MatrixLabel.png"/></para>
    /// </summary>
    public class LabelSymbolCell : LabelMatrixCell
    {
        /// <summary>${iServerJava6R_LabelSymbolCell_constructor_D}</summary>
        public LabelSymbolCell() 
        {
            Type = LabelMatrixCellType.SYMBOL;
        }
        /// <summary>${iServerJava6R_LabelSymbolCell_attribute_Style_D}</summary>
        public ServerStyle Style
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_LabelSymbolCell_attribute_SymbolIDField_D}</summary>
        public string SymbolIDField
        {
            get;
            set;
        }

        internal static string ToJson(LabelSymbolCell symbolCell)
        {
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"symbolIDField\":\"{0}\"", symbolCell.SymbolIDField));
            if (symbolCell.Style != null)
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(symbolCell.Style)));
            }
            else
            {
                list.Add(string.Format("\"style\":{0}", new ServerStyle()));
            }

            list.Add(string.Format("\"type\":\"{0}\"", symbolCell.Type));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }

        internal static LabelSymbolCell FromJson(System.Json.JsonObject json)
        {
            if (json == null) return null;
            LabelSymbolCell symbolCell = new LabelSymbolCell();
            if (json["style"] != null)
            {
                symbolCell.Style = ServerStyle.FromJson((JsonObject)json["style"]);
            }
            symbolCell.SymbolIDField = (string)json["symbolIDField"];

            return symbolCell;
        }
    }
}
