﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeUniqueItem_Title}</para>
    /// 	<para>${iServerJava6R_ThemeUniqueItem_Description}</para>
    /// </summary>
    public class ThemeUniqueItem
    {
        /// <summary>${iServerJava6R_ThemeUniqueItem_constructor_D}</summary>
        public ThemeUniqueItem()
        {
            Visible = true;
        }

        /// <summary>${iServerJava6R_ThemeUniqueItem_attribute_caption_D}</summary>
        public string Caption
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeUniqueItem_attribute_unique_D}</summary>
        public string Unique
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeUniqueItem_attribute_style_D}</summary>
        public ServerStyle Style
        {
            get;
            set;
        }


        /// <summary>${iServerJava6R_ThemeUniqueItem_attribute_visible_D}</summary>
        public bool Visible
        {
            get;
            set;
        }

        internal static string ToJson(ThemeUniqueItem item)
        {
            string json = "{";

            List<string> list = new List<string>();
            if (!string.IsNullOrEmpty(item.Caption))
            {
                list.Add(string.Format("\"caption\":\"{0}\"", item.Caption));
            }
            else
            {
                list.Add("\"caption\":\"\"");
            }

            //默认值这边儿是个问题，又不要判断呢？
            list.Add(string.Format("\"visible\":{0}", item.Visible.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            if (item.Style != null)
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(item.Style)));
            }
            else
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            if (!string.IsNullOrEmpty(item.Unique))
            {
                list.Add(string.Format("\"unique\":\"{0}\"", item.Unique));
            }
            else
            {
                list.Add("\"unique\":\"\"");
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal static ThemeUniqueItem FromJson(System.Json.JsonValue jsonValue)
        {
            if (jsonValue == null) { return null; };
            ThemeUniqueItem item = new ThemeUniqueItem();

            item.Caption = jsonValue["caption"];
            item.Style = ServerStyle.FromJson((JsonObject)jsonValue["style"]);
            item.Unique = jsonValue["unique"];
            item.Visible = (bool)jsonValue["visible"];
            return item;
        }
    }
}
