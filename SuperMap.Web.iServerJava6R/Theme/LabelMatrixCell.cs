﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_LabelMatrixCell_Title}</para>
    /// 	<para>${iServerJava6R_LabelMatrixCell_Description}</para>
    /// </summary>
    public class LabelMatrixCell
    {
        /// <summary>${iServerJava6R_LabelMatrixCell_constructor_D}</summary>
        public LabelMatrixCell() { }
        /// <summary>${iServerJava6R_LabelMatrixCell_attribute_Type_D}</summary>
        internal LabelMatrixCellType Type { get; set; }
    }
}
