﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGridUniqueItem_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGridUniqueItem_Description}</para>
    /// </summary>
   public class ThemeGridUniqueItem
    {
        /// <summary>${iServerJava6R_ThemeGridUniqueItem_constructor_D}</summary>
	   public ThemeGridUniqueItem()
       {
            
       }
       /// <summary>${iServerJava6R_ThemeGridUniqueItem_attribute_Unique_D}</summary>
        public String Unique
		{
			get;
            set;
		}
		/// <summary>${iServerJava6R_ThemeGridUniqueItem_attribute_Visible_D}</summary>
		public Boolean Visible
		{
			get;
            set;
		}

		/// <summary>${iServerJava6R_ThemeGridUniqueItem_attribute_Color_D}</summary> 
		public ServerColor Color
		{
			get;
            set;
		}

		/// <summary>${iServerJava6R_ThemeGridUniqueItem_attribute_Caption_D}</summary> 		
		public String Caption
		{
			get;
            set;
		}

        internal static string ToJson(ThemeGridUniqueItem item)
        {
            string json = "{";

            List<string> list = new List<string>();
            if (!string.IsNullOrEmpty(item.Caption))
            {
                list.Add(string.Format("\"caption\":\"{0}\"", item.Caption));
            }
            else
            {
                list.Add("\"caption\":\"\"");
            }

            //默认值这边儿是个问题，要不要判断呢？
            list.Add(string.Format("\"visible\":{0}", item.Visible.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            if (item.Caption != null)
            {
                list.Add(string.Format("\"color\":{0}", ServerColor.ToJson(item.Color)));
            }
            else
            {
                list.Add(string.Format("\"color\":{0}", ServerColor.ToJson(new ServerColor())));
            }

            if (!string.IsNullOrEmpty(item.Unique))
            {
                list.Add(string.Format("\"unique\":\"{0}\"", item.Unique));
            }
            else
            {
                list.Add("\"unique\":\"\"");
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal static ThemeGridUniqueItem FromJson(System.Json.JsonValue jsonValue)
        {
            if (jsonValue == null) { return null; };
            ThemeGridUniqueItem item = new ThemeGridUniqueItem();

            item.Caption = jsonValue["caption"];
            item.Color = ServerColor.FromJson((JsonObject)jsonValue["color"]);
            item.Unique = jsonValue["unique"];
            item.Visible = (bool)jsonValue["visible"];
            return item;
        }

		
    }
}
