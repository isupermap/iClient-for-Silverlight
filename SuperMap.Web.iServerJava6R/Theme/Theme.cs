﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_Theme_Title}</para>
    /// 	<para>${iServerJava6R_Theme_Description}</para>
    /// </summary>
    public class Theme
    {
        /// <summary>${iServerJava6R_Theme_constructor_None_D}</summary>
        public Theme()
        {

        }
        /// <summary>${iServerJava6R_Theme_MemoryData__D}</summary>
        public List<ThemeMemoryData> MemoryData { get; set; }

        /// <summary>
        /// 专题图类型。
        /// </summary>
        public ThemeType Type
        {
            get;
            set;
        }

        internal string ToJson(List<ThemeMemoryData> memorydata)
        {
            if (memorydata != null)
            {
                List<string> memoryStr = new List<string>();
                foreach (var item in memorydata)
                {
                    memoryStr.Add("\"" + item.ScrData + "\":\"" + item.TargetData + "\"");
                }
                return "{" + string.Join(",", memoryStr.ToArray()) + "}";
            }
            else
            {
                return "null";
            }
        }
    }


    /// <summary>
    /// 	<para>${iServerJava6R_ThemeMemoryData_Title}</para>
    /// 	<para>${iServerJava6R_ThemeMemoryData_Description}</para>
    /// </summary>
    public class ThemeMemoryData
    {
        /// <summary>${iServerJava6R_Theme_ThemeMemoryData_None_D}</summary>
        public ThemeMemoryData() { }
        /// <summary>${iServerJava6R_Theme_ScrData_D}</summary>
        public string ScrData { get; set; }
        /// <summary>${iServerJava6R_Theme_TargetData_D}</summary>
        public string TargetData { get; set; }
    }
}
