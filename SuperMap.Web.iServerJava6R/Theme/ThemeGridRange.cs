﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGridRange_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGridRange_Description}</para>
    /// </summary>
	public class ThemeGridRange:Theme
    {
        /// <summary>${iServerJava6R_ThemeGridRange_constructor_D}</summary>
		public ThemeGridRange()
        {
            ColorGradientType = ColorGradientType.YELLOWBLUE;
            Type = ThemeType.GRIDRANGE;
            RangeMode =RangeMode.EQUALINTERVAL;
            ReverseColor =false;

        }
        /// <summary>${iServerJava6R_ThemeGridRange_attribute_ColorGradientType_D}</summary>
        public ColorGradientType ColorGradientType
		{
			get;
            set;
		}

         /// <summary>${iServerJava6R_ThemeGridRange_attribute_Items_D}</summary>
        public IList<ThemeGridRangeItem> Items
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeGridRange_attribute_RangeMode_D}</summary>
        public RangeMode RangeMode
		{
			get;
            set;
		}
		
		/// <summary>${iServerJava6R_ThemeGridRange_attribute_RangeParameter_D}</summary>	
		public double RangeParameter
		{
			get;
            set;
		}
		
		/// <summary>${iServerJava6R_ThemeGridRange_attribute_ReverseColor_D}</summary>	
		public Boolean ReverseColor
		{
			get;
            set;

		}

        internal static string ToJson(ThemeGridRange themeGridRange)
        {
            string json = "{";
            List<string> list = new List<string>();

            if (themeGridRange.Items != null && themeGridRange.Items.Count >= 1)
            {
                List<string> tempTUI = new List<string>();
                foreach (var item in themeGridRange.Items)
                {
                    tempTUI.Add(ThemeGridRangeItem.ToJson(item));
                }

                list.Add(string.Format("\"items\":[{0}]", String.Join(",", tempTUI.ToArray())));
            }
            else
            {
                list.Add("\"items\":[]");
            }

            list.Add(string.Format("\"rangeParameter\":\"{0}\"", themeGridRange.RangeParameter.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            list.Add(string.Format("\"rangeMode\":\"{0}\"", themeGridRange.RangeMode));
            list.Add(string.Format("\"colorGradientType\":\"{0}\"", themeGridRange.ColorGradientType.ToString()));
                     
            list.Add("\"type\":\"GRIDRANGE\"");
            list.Add(string.Format("\"reverseColor\":\"{0}\"",themeGridRange.ReverseColor.ToString()));


            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }

        internal static ThemeGridRange FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeGridRange themeGridRange = new ThemeGridRange();

            if (json["items"] != null && json["items"].Count > 0)
            {
                List<ThemeGridRangeItem> itemsList = new List<ThemeGridRangeItem>();
                foreach (JsonObject item in (JsonArray)json["items"])
                {
                    itemsList.Add(ThemeGridRangeItem.FromJson(item));
                }

                themeGridRange.Items = itemsList;
            }

            if (json["colorGradientType"] != null)
            {
                themeGridRange.ColorGradientType = (ColorGradientType)Enum.Parse(typeof(ColorGradientType), json["colorGradientType"], true);
            }
            else
            {
                //这里不处理为空时的情况
            }

            if (json["rangeMode"] != null)
            {
                themeGridRange.RangeMode = (RangeMode)Enum.Parse(typeof(RangeMode), json["rangeMode"], true);
            }
            else
            {
                //不处理Null的情况
            }
            themeGridRange.RangeParameter = (double)json["rangeParameter"];
            themeGridRange.ReverseColor = (Boolean)json["reverseColor"];
            return themeGridRange;
        }
		
    }
}
