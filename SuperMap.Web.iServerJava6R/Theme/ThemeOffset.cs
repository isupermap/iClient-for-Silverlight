﻿using System.Collections.Generic;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeOffset_Title}</para>
    /// 	<para>${iServerJava6R_ThemeOffset_Description}</para>
    /// </summary>
    public class ThemeOffset
    {
        /// <summary>${iServerJava6R_ThemeOffset_constructor_D}</summary>
        public ThemeOffset()
        { }

        /// <summary>${iServerJava6R_ThemeOffset_attribute_OffsetFixed_D}</summary>
        public bool OffsetFixed
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeOffset_attribute_OffsetX_D}</summary>
        public string OffsetX
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeOffset_attribute_OffsetY_D}</summary>
        public string OffsetY
        {
            get;
            set;
        }

        internal static string ToJson(ThemeOffset offsetTheme)
        {
            string json = "";

            List<string> list = new List<string>();
            list.Add(string.Format("\"offsetFixed\":{0}", offsetTheme.OffsetFixed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            if (!string.IsNullOrEmpty(offsetTheme.OffsetX))
            {
                list.Add(string.Format("\"offsetX\":\"{0}\"", offsetTheme.OffsetX));
            }
            else
            {
                list.Add("\"offsetX\":\"\"");
            }

            if (!string.IsNullOrEmpty(offsetTheme.OffsetY))
            {
                list.Add(string.Format("\"offsetY\":\"{0}\"", offsetTheme.OffsetY));
            }
            else
            {
                list.Add("\"offsetY\":\"\"");
            }

            json = string.Join(",", list.ToArray());
            return json;
        }

        internal static ThemeOffset FromJson(JsonObject json)
        {
            if (json == null)
                return null;
            ThemeOffset themeOffset = new ThemeOffset();
            themeOffset.OffsetFixed = (bool)json["offsetFixed"];
            themeOffset.OffsetX = (string)json["offsetX"];
            themeOffset.OffsetY = (string)json["offsetY"];
            return themeOffset;
        }
    }
}
