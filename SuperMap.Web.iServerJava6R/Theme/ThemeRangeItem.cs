﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeRangeItem_Title}</para>
    /// 	<para>${iServerJava6R_ThemeRangeItem_Description}</para>
    /// </summary>
    public class ThemeRangeItem
    {
        /// <summary>${iServerJava6R_ThemeRangeItem_constructor_D}</summary>
        public ThemeRangeItem()
        {
            this.Visible = true;
        }

        /// <summary>${iServerJava6R_ThemeRangeItem_attribute_caption_D}</summary>
        public string Caption
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeRangeItem_attribute_Start_D}</summary>
        public double Start
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeRangeItem_attribute_End_D}</summary>
        public double End
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeRangeItem_attribute_visible_D}</summary>
        public bool Visible
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeRangeItem_attribute_style_D}</summary>
        public ServerStyle Style
        {
            get;
            set;
        }

        internal static string ToJson(ThemeRangeItem themeRangeItem)
        {
            string json = "{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(themeRangeItem.Caption))
            {
                list.Add(string.Format("\"caption\":\"{0}\"", themeRangeItem.Caption));
            }
            else
            {
                list.Add("\"caption\":\"\"");
            }

            if (themeRangeItem.Style != null)
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(themeRangeItem.Style)));
            }
            else
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            list.Add(string.Format("\"visible\":{0}", themeRangeItem.Visible.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            list.Add(string.Format("\"end\":{0}", themeRangeItem.End.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            list.Add(string.Format("\"start\":{0}", themeRangeItem.Start.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal static ThemeRangeItem FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeRangeItem item = new ThemeRangeItem();

            item.Caption = json["caption"];
            item.End = (double)json["end"];
            item.Start = (double)json["start"];
            if (json["style"] != null)
            {
                item.Style = ServerStyle.FromJson((JsonObject)json["style"]);
            }
            item.Visible = (bool)json["visible"];
            return item;
        }
    }
}
