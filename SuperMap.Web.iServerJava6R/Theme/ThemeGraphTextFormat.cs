﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGraphTextFormat_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGraphTextFormat_Description}</para>
    /// </summary>
    public enum ThemeGraphTextFormat
    {
        /// <summary>${iServerJava6R_ThemeGraphTextFormat_attribute_Caption_D}</summary>
        CAPTION,            //标题。
        /// <summary>${iServerJava6R_ThemeGraphTextFormat_attribute_CaptionPercent_D}</summary>
        CAPTION_PERCENT,    //标题 + 百分数。 
        /// <summary>${iServerJava6R_ThemeGraphTextFormat_attribute_CaptionValue_D}</summary>
        CAPTION_VALUE,      //标题 + 实际数值。 
        /// <summary>${iServerJava6R_ThemeGraphTextFormat_attribute_Percent_D}</summary>
        PERCENT,            //百分数。 
        /// <summary>${iServerJava6R_ThemeGraphTextFormat_attribute_Value_D}</summary>
        VALUE,              //实际数值。
    }
}
