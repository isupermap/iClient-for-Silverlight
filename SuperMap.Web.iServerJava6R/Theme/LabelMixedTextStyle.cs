﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_LabelMixedTextStyle_Title}</para>
    /// 	<para>${iServerJava6R_LabelMixedTextStyle_Description}</para>
    /// </summary>
    public class LabelMixedTextStyle
    {
        /// <summary>${iServerJava6R_LabelMixedTextStyle_constructor_D}</summary>
        public LabelMixedTextStyle()
        { 
        }

        /// <summary>${iServerJava6R_LabelMixedTextStyle_attribute_DefaultStyle_D}</summary>
        public ServerTextStyle DefaultStyle
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_LabelMixedTextStyle_attribute_Sparator_D}</summary>
        public string Separator
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_LabelMixedTextStyle_attribute_SeparatorEnabled_D}</summary>
        public bool SeparatorEnabled
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_LabelMixedTextStyle_attribute_SplitIndexes_D}</summary>
        public IList<int> SplitIndexes
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_LabelMixedTextStyle_attribute_Styles_D}</summary>
        public IList<ServerTextStyle> Styles
        {
            get;
            set;
        }

        internal static string ToJson(LabelMixedTextStyle labelMixedTextStyle)
        {
            string json = "{";
            System.Collections.Generic.List<string> list = new List<string>();

            if (labelMixedTextStyle.DefaultStyle != null)
            {
                list.Add(string.Format("\"defaultStyle\":{0}", ServerTextStyle.ToJson(labelMixedTextStyle.DefaultStyle)));
            }
            else
            {
                list.Add(string.Format("\"defaultStyle\":{0}", ServerTextStyle.ToJson(new ServerTextStyle())));
            }

            if (!string.IsNullOrEmpty(labelMixedTextStyle.Separator))
            {
                list.Add(string.Format("\"separator\":\"{0}\"", labelMixedTextStyle.Separator));
            }
            else
            {
                list.Add("\"separator\":\"\"");
            }

            list.Add(string.Format("\"separatorEnabled\":{0}", labelMixedTextStyle.SeparatorEnabled.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            if (labelMixedTextStyle.SplitIndexes != null && labelMixedTextStyle.SplitIndexes.Count > 0)
            {
                List<string> splitList = new List<string>();
                for (int i = 0; i < labelMixedTextStyle.SplitIndexes.Count; i++)
                {
                    splitList.Add(string.Format("{0}", labelMixedTextStyle.SplitIndexes[i].ToString(System.Globalization.CultureInfo.InvariantCulture)));
                }
                list.Add(string.Format("\"splitIndexes\":[{0}]", string.Join(",", splitList.ToArray())));
            }
            else
            {
                list.Add("\"splitIndexes\":[]");
            }

            if (labelMixedTextStyle.Styles != null && labelMixedTextStyle.Styles.Count > 0)
            {
                List<string> stylesList = new List<string>();
                foreach (var item in labelMixedTextStyle.Styles)
                {
                    stylesList.Add(ServerTextStyle.ToJson(item));
                }

                list.Add(string.Format("\"styles\":[{0}]", string.Join(",", stylesList.ToArray())));
            }
            else
            {
                list.Add("\"styles\":[]");
            }
            json += string.Join(",", list.ToArray());

            json += "}";
            return json;
        }

        internal static LabelMixedTextStyle FromJson(JsonObject json)
        {
            if (json == null) return null;
            LabelMixedTextStyle textStyle = new LabelMixedTextStyle();
            textStyle.DefaultStyle = ServerTextStyle.FromJson((JsonObject)json["defaultStyle"]);
            textStyle.Separator = (string)json["separator"];
            textStyle.SeparatorEnabled = (bool)json["separatorEnabled"];

            if ((JsonArray)json["splitIndexes"] != null && ((JsonArray)json["splitIndexes"]).Count > 0)
            {
                List<int> list = new List<int>();
                foreach (int item in (JsonArray)json["splitIndexes"])
                {
                    list.Add(item);
                }
                textStyle.SplitIndexes = list;
            }

            if (json["styles"] != null)
            {
                List<ServerTextStyle> textStyleList = new List<ServerTextStyle>();
                foreach (JsonObject item in (JsonArray)json["styles"])
                {
                    textStyleList.Add(ServerTextStyle.FromJson(item));
                }
                textStyle.Styles = textStyleList;
            }

            return textStyle;
        }
    }
}
