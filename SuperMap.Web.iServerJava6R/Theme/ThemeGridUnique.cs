﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
     /// <summary>
    /// 	<para>${iServerJava6R_ThemeGridUnique_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGridUnique_Description}</para>
    /// </summary>
	public class ThemeGridUnique:Theme
    {
        /// <summary>${iServerJava6R_ThemeGridUnique_constructor_D}</summary>
		public ThemeGridUnique()
        { 

        }
        
		/// <summary>${iServerJava6R_ThemeGridUnique_attribute_Items_D}</summary>
        public IList<ThemeGridUniqueItem> Items
        {
            get;
            set;
        }

		/// <summary>${iServerJava6R_ThemeGridUnique_defaultColor_DefaultColor_D}</summary>
		public ServerColor DefaultColor
		{
			get;
            set;
		}

        internal static string ToJson(ThemeGridUnique themeGridUnique)
        {
            string json = "{";
            List<string> list = new List<string>();

            if (themeGridUnique.Items != null && themeGridUnique.Items.Count >= 1)
            {
                List<string> tempTUI = new List<string>();
                foreach (var item in themeGridUnique.Items)
                {
                    tempTUI.Add(ThemeGridUniqueItem.ToJson(item));
                }

                list.Add(string.Format("\"items\":[{0}]", String.Join(",", tempTUI.ToArray())));
            }
            else
            {
                list.Add("\"items\":[]");
            }

            if (themeGridUnique.DefaultColor != null)
            {
                list.Add(string.Format("\"defaultColor\":{0}", ServerColor.ToJson(themeGridUnique.DefaultColor)));
            }
            else
            {
                list.Add(string.Format("\"defaultColor\":{0}", ServerColor.ToJson(new ServerColor())));
            }
        
            list.Add("\"type\":\"GRIDUNIQUE\"");

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }

        internal static ThemeGridUnique FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeGridUnique themeGridUnique = new ThemeGridUnique();

            if (json["items"] != null && json["items"].Count > 0)
            {
                List<ThemeGridUniqueItem> itemsList = new List<ThemeGridUniqueItem>();
                foreach (JsonObject item in (JsonArray)json["items"])
                {
                    itemsList.Add(ThemeGridUniqueItem.FromJson(item));
                }

                themeGridUnique.Items = itemsList;
            }

            if (json["defaultcolor"] != null)
            {
                themeGridUnique.DefaultColor = ServerColor.FromJson((JsonObject)json["defaultcolor"]);
            }
    
            return themeGridUnique;
        }

    }
}
