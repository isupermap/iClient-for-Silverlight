﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_LabelOverLengthMode_Title}</para>
    /// 	<para>${iServerJava6R_LabelOverLengthMode_Description}</para>
    /// </summary>
    public enum LabelOverLengthMode
    {
        /// <summary>${iServerJava6R_LabelOverLengthMode_attribute_NEWLINE_D}</summary>
        NEWLINE,          //换行显示。
        /// <summary>${iServerJava6R_LabelOverLengthMode_attribute_NONE_D}</summary>
        NONE,          //对超长标签不进行处理。
        /// <summary>${iServerJava6R_LabelOverLengthMode_attribute_OMIT_D}</summary>
        OMIT,          //省略超出部分。
    }
}
