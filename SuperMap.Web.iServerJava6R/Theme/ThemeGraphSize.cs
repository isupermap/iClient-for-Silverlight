﻿using System.Collections.Generic;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGraphSize_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGraphSize_Description}</para>
    /// </summary>
    public class ThemeGraphSize
    {
        /// <summary>${iServerJava6R_ThemeGraphSize_constructor_D}</summary>
        public ThemeGraphSize()
        { }
        /// <summary>${iServerJava6R_ThemeGraphSize_attribute_maxGraphSize_D}</summary>  
        public double MaxGraphSize
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeGraphSize_attribute_minGraphSize_D}</summary>
        public double MinGraphSize
        {
            get;
            set;
        }

        internal static string ToJson(ThemeGraphSize graphSize)
        {
            string json = "";

            List<string> list = new List<string>();
            list.Add(string.Format("\"maxGraphSize\":{0}", graphSize.MaxGraphSize.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            list.Add(string.Format("\"minGraphSize\":{0}", graphSize.MinGraphSize.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            json = string.Join(",", list.ToArray());
            return json;
        }

        internal static ThemeGraphSize FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeGraphSize graphSize = new ThemeGraphSize();
            graphSize.MaxGraphSize = (double)json["maxGraphSize"];
            graphSize.MinGraphSize = (double)json["minGraphSize"];
            return graphSize;
        }
    }
}
