﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_LabelThemeCell_Title}</para>
    /// 	<para>${iServerJava6R_LabelThemeCell_Description}</para>
    /// </summary>
    public class LabelThemeCell : LabelMatrixCell
    {
        /// <summary>${iServerJava6R_LabelThemeCell_constructor_D}</summary>
        public LabelThemeCell() 
        {
            Type = LabelMatrixCellType.THEME;
        }
        /// <summary>${iServerJava6R_LabelThemeCell_attribute_ThemeLabel_D}</summary>
        public ThemeLabel ThemeLabel
        {
            get;
            set;
        }

        internal static string ToJson(LabelThemeCell themeCell)
        {
            string json = "{";
            List<string> list = new List<string>();

            if (themeCell.ThemeLabel != null)
            {
                list.Add(string.Format("\"themeLabel\":{0}", ThemeLabel.ToJson(themeCell.ThemeLabel)));
            }
            else
            {
                list.Add("\"themeLabel\":null");
            }

            list.Add(string.Format("\"type\":\"{0}\"", themeCell.Type));

            json += string.Join(",", list.ToArray());

            json += "}";
            return json;
        }

        internal static LabelThemeCell FromJson(System.Json.JsonObject json)
        {
            if (json == null) return null;

            LabelThemeCell themeCell = new LabelThemeCell();
            themeCell.ThemeLabel = ThemeLabel.FromJson(json);

            return themeCell;
        }
    }
}
