﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeLabelBackGround_Title}</para>
    /// 	<para>${iServerJava6R_ThemeLabelBackGround_Description}</para>
    /// </summary>
    public class ThemeLabelBackSetting
    {
        /// <summary>${iServerJava6R_ThemeLabelBackGround_constructor_D}</summary>
        public ThemeLabelBackSetting()
        {
            //this.LabelBackShape = LabelBackShape.ROUNDRECT;
        }

        /// <summary>${iServerJava6R_ThemeLabelBackGround_attribute_labelBackShape_D}</summary>
        public LabelBackShape LabelBackShape
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelBackGround_attribute_backStyle_D}</summary>
        public ServerStyle BackStyle
        {
            get;
            set;
        }

        internal static string ToJson(ThemeLabelBackSetting labelBackSetting)
        {
            string json = "";
            System.Collections.Generic.List<string> list = new System.Collections.Generic.List<string>();

            if (labelBackSetting.BackStyle != null)
            {
                list.Add(string.Format("\"backStyle\":{0}", ServerStyle.ToJson(labelBackSetting.BackStyle)));
            }
            else
            {
                list.Add(string.Format("\"backStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            list.Add(string.Format("\"labelBackShape\":\"{0}\"", labelBackSetting.LabelBackShape));
            json = string.Join(",", list.ToArray());
            return json;
        }

        internal static ThemeLabelBackSetting FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeLabelBackSetting backSetting = new ThemeLabelBackSetting();

            if (json["backStyle"] != null)
            {
                backSetting.BackStyle = ServerStyle.FromJson((JsonObject)json["backStyle"]);
            }

            if (json["labelBackShape"] != null)
            {
                backSetting.LabelBackShape = (LabelBackShape)Enum.Parse(typeof(LabelBackShape), json["labelBackShape"], true);
            }
            else
            {
                //不处理null的情况
            }
            return backSetting;
        }
    }
}
