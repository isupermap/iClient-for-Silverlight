﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGraphType_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGraphType_Description}</para>
    /// </summary>
    public enum ThemeGraphType
    {
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Area_D}</summary>
        AREA,                   //面积图。 
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Bar_D}</summary>
        BAR,                    //柱状图。 
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Bar3D_D}</summary>
        BAR3D,                  //三维柱状图。 
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Line_D}</summary>
        LINE,                   //折线图。
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Pie_D}</summary>
        PIE,                    //饼图。 
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Pie3D_D}</summary>
        PIE3D,                  //三维饼图。
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Point_D}</summary>
        POINT,                  //点状图。 
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Ring_D}</summary>
        RING,                   //环状图。
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Rose_D}</summary>
        ROSE,                   //玫瑰图。 
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Rose3D_D}</summary>
        ROSE3D,                 //三维玫瑰图。 
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Strack_Bar_D}</summary>
        STACK_BAR,              //堆叠柱状图。 
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Stack_Bar3D_D}</summary>
        STACK_BAR3D,            //三维堆叠柱状图。
        /// <summary>${iServerJava6R_ThemeGraphType_attribute_Step_D}</summary>
        STEP,                   //阶梯图。 
    }
}
