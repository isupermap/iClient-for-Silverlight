﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeRange_Title}</para>
    /// 	<para>${iServerJava6R_ThemeRange_Description}</para>
    /// 	<para>
    /// 		<img src="ThemeRange_iServer6.bmp"/>
    /// 	</para>
    /// </summary>
    public class ThemeRange : Theme
    {
        /// <summary>${iServerJava6R_ThemeRange_constructor_D}</summary>
        public ThemeRange()
        {
            ColorGradientType = ColorGradientType.YELLOWRED;
            Type = ThemeType.RANGE;
        }

        /// <summary>${iServerJava6R_ThemeRange_attribute_RangeExpression_D}</summary>
        public string RangeExpression
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeRange_attribute_RangeMode_D}</summary>
        public RangeMode RangeMode
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeUnique_attribute_ColorGradientType_D}</summary>
        public ColorGradientType ColorGradientType
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeRange_attribute_RangeParameter_D}</summary>
        public double RangeParameter
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeRange_attribute_Items_D}</summary>
        public IList<ThemeRangeItem> Items
        {
            get;
            set;
        }

        internal static string ToJson(ThemeRange themeRange)
        {
            string json = "{";
            List<string> list = new List<string>();

            if (themeRange.Items != null && themeRange.Items.Count >= 1)
            {
                List<string> tempTUI = new List<string>();
                foreach (var item in themeRange.Items)
                {
                    tempTUI.Add(ThemeRangeItem.ToJson(item));
                }

                list.Add(string.Format("\"items\":[{0}]", String.Join(",", tempTUI.ToArray())));
            }
            else
            {
                list.Add("\"items\":[]");
            }

            list.Add(string.Format("\"rangeParameter\":\"{0}\"", themeRange.RangeParameter.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            list.Add(string.Format("\"rangeMode\":\"{0}\"", themeRange.RangeMode));
            list.Add(string.Format("\"colorGradientType\":\"{0}\"", themeRange.ColorGradientType.ToString()));
            if (!string.IsNullOrEmpty(themeRange.RangeExpression))
            {
                list.Add(string.Format("\"rangeExpression\":\"{0}\"", themeRange.RangeExpression));
            }
            else
            {
                list.Add("\"rangeExpression\":\"\"");
            }

            if (themeRange.MemoryData != null)
            {
                list.Add("\"memoryData\":" + themeRange.ToJson(themeRange.MemoryData));
            }
            else
            {
                list.Add("\"memoryData\":null");
            }
            list.Add("\"type\":\"RANGE\"");

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }

        internal static ThemeRange FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeRange themeRange = new ThemeRange();

            if (json["items"] != null && json["items"].Count > 0)
            {
                List<ThemeRangeItem> itemsList = new List<ThemeRangeItem>();
                foreach (JsonObject item in (JsonArray)json["items"])
                {
                    itemsList.Add(ThemeRangeItem.FromJson(item));
                }

                themeRange.Items = itemsList;
            }

            if (json["colorGradientType"] != null)
            {
                themeRange.ColorGradientType = (ColorGradientType)Enum.Parse(typeof(ColorGradientType), json["colorGradientType"], true);
            }
            else
            {
                //这里不处理为空时的情况
            }

            themeRange.RangeExpression = json["rangeExpression"];
            if (json["rangeMode"] != null)
            {
                themeRange.RangeMode = (RangeMode)Enum.Parse(typeof(RangeMode), json["rangeMode"], true);
            }
            else
            {
                //不处理Null的情况
            }
            themeRange.RangeParameter = (double)json["rangeParameter"];
            return themeRange;
        }
    }
}
