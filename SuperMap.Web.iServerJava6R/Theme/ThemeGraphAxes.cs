﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGraphAxes_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGraphAxes_Description}</para>
    /// </summary>
    public class ThemeGraphAxes
    {
        /// <summary>${iServerJava6R_ThemeGraphAxes_constructor_D}</summary>
        public ThemeGraphAxes()
        {
        }

        /// <summary>${iServerJava6R_ThemeGraphAxes_attribute_axesColor_D}</summary>
        public ServerColor AxesColor
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeGraphAxes_attribute_axesDisplayed_D}</summary>
        public bool AxesDisplayed
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeGraphAxes_attribute_axesGridDisplayed_D}</summary>
        public bool AxesGridDisplayed
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeGraphAxes_attribute_axesTextDisplayed_D}</summary>
        public bool AxesTextDisplayed
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeGraphAxes_attribute_axesTextStyle_D}</summary>
        public ServerTextStyle AxesTextStyle
        {
            get;
            set;
        }

        internal static string ToJson(ThemeGraphAxes graphAxes)
        {
            string json = "";

            List<string> list = new List<string>();

            if (graphAxes.AxesColor != null)
            {
                list.Add(string.Format("\"axesColor\":{0}", ServerColor.ToJson(graphAxes.AxesColor)));
            }
            else
            {
                list.Add(string.Format("\"axesColor\":{0}", ServerColor.ToJson(new ServerColor())));
            }

            list.Add(string.Format("\"axesDisplayed\":{0}", graphAxes.AxesDisplayed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            list.Add(string.Format("\"axesGridDisplayed\":{0}", graphAxes.AxesGridDisplayed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            list.Add(string.Format("\"axesTextDisplayed\":{0}", graphAxes.AxesTextDisplayed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            if (graphAxes.AxesTextStyle != null)
            {
                list.Add(string.Format("\"axesTextStyle\":{0}", ServerTextStyle.ToJson(graphAxes.AxesTextStyle)));
            }
            else
            {
                list.Add(string.Format("\"axesTextStyle\":{0}", ServerTextStyle.ToJson(new ServerTextStyle())));
            }

            json = string.Join(",", list.ToArray());
            return json;
        }

        internal static ThemeGraphAxes FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeGraphAxes graphAxes = new ThemeGraphAxes();
            graphAxes.AxesColor = ServerColor.FromJson((JsonObject)json["axesColor"]);
            graphAxes.AxesDisplayed = (bool)json["axesDisplayed"];
            graphAxes.AxesGridDisplayed = (bool)json["axesGridDisplayed"];
            graphAxes.AxesTextDisplayed = (bool)json["axesTextDisplayed"];
            graphAxes.AxesTextStyle = ServerTextStyle.FromJson((JsonObject)json["axesTextStyle"]);
            return graphAxes;
        }
    }
}
