﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_RangeMode_Title}</para>
    /// </summary>
    public enum RangeMode
    {
        //自定义分段。
        /// <summary>${iServerJava6R_RangeMode_Custominterval_D}</summary>
        CUSTOMINTERVAL,

        //等距离分段。
        /// <summary>${iServerJava6R_RangeMode_Equalinterval_D}</summary>
        EQUALINTERVAL,

        //对数分段。
        /// <summary>${iServerJava6R_RangeMode_Logarithm_D}</summary>
        LOGARITHM,

        //等计数分段。
        /// <summary>${iServerJava6R_RangeMode_Quantile_D}</summary>
        QUANTILE,

        //平方根分段。
        /// <summary>${iServerJava6R_RangeMode_Squareroot_D}</summary>
        SQUAREROOT,

        //标准差分段。 
        /// <summary>${iServerJava6R_RangeMode_Stddeviation_D}</summary>
        STDDEVIATION
    }
}
