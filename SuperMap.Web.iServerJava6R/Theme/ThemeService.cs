﻿using System.Json;
using System.Collections.Generic;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;
using SuperMap.Web.iServerJava6R.Resources;
using System.Windows.Browser;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeService_Title}</para>
    /// 	<para>${iServerJava6R_ThemeService_Description}</para>
    /// </summary>
    public class ThemeService : ServiceBase
    {
        private string serviceMapName;
        /// <summary>${iServerJava6R_ThemeService_constructor_D}</summary>
        public ThemeService()
        {
        }

        /// <summary>${iServerJava6R_ThemeService_constructor_param_url}</summary>
        public ThemeService(string url)
            : base(url)
        {
            serviceMapName = GetServiceMapName();
        }

        private string GetServiceMapName()
        {
            //http://192.168.11.11:8090/iserver/services/map-world/rest/maps/World Map
            string mapName = this.Url;

            if (this.Url.EndsWith("/"))
            {
                mapName.TrimEnd('/');
            }

            mapName = mapName.Substring(mapName.LastIndexOf('/') + 1);
            return mapName;
        }

        /// <summary>${iServerJava6R_ThemeService_method_ProcessAsync_D}</summary>
        public void ProcessAsync(ThemeParameters themeParameters)
        {
            ProcessAsync(themeParameters, null);
        }

        /// <summary>${iServerJava6R_ThemeService_method_ProcessAsync_param_Parameters}</summary>
        public void ProcessAsync(ThemeParameters themeParameters, object state)
        {
            ValidationUrlAndParametres(themeParameters);
            CombineAbsolutePath();

            base.SubmitRequest(base.Url, GetDictionaryParameters(themeParameters), new EventHandler<RequestEventArgs>(ThemeRequest_Complated), state, true, false, true);
        }

        private System.Collections.Generic.Dictionary<string, string> GetDictionaryParameters(ThemeParameters themeParameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("type", "\"UGC\"");

            dictionary.Add("subLayers", ThemeParameters.ToJson(themeParameters));
            dictionary.Add("name", "\"" + serviceMapName + "\"");

            return dictionary;
        }

        private void ValidationUrlAndParametres(ThemeParameters themeParameters)
        {
            if (themeParameters == null)
            {
                throw new ArgumentNullException("请求服务参数为空！");
            }

            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
        }

        private void CombineAbsolutePath()
        {
            if (this.Url.EndsWith("/"))
            {
                this.Url += "tempLayersSet.json?debug=true&";
            }
            else
            {
                this.Url += "/tempLayersSet.json?debug=true&";
            }
        }

        private void ThemeRequest_Complated(object sender, RequestEventArgs e)
        {
            ThemeResult result = ThemeResult.FromJson(e.Result);
            lastResult = result;
            ThemeEventArgs args = new ThemeEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        /// <summary>${iServerJava6R_ThemeService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ThemeEventArgs> ProcessCompleted;

        private void OnProcessCompleted(ThemeEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, args);
            }
        }

        private ThemeResult lastResult;
        /// <summary>${iServerJava6R_ThemeService_attribute_lastResult_D}</summary>
        public ThemeResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
