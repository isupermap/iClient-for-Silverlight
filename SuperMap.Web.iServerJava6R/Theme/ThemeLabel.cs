﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeLabel_Title}</para>
    /// 	<para>${iServerJava6R_ThemeLabel_Description}</para>
    /// 	<para><img src="ThemeLableiServer6.bmp"/></para>
    /// 	</summary>
    public class ThemeLabel : Theme
    {
        /// <summary>${iServerJava6R_ThemeLabel_constructor_D}</summary>
        public ThemeLabel()
        {
            LabelOverLengthMode = LabelOverLengthMode.NONE;
            Type = ThemeType.LABEL;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_AlongLine_D}</summary>
        public ThemeLabelAlongLine AlongLine
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_offset_D}</summary>
        public ThemeOffset Offset
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_Flow_D}</summary>
        public ThemeFlow Flow
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_text_D}</summary>
        public ThemeLabelText Text
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_Background_D}</summary>
        public ThemeLabelBackSetting BackSetting
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_labelOverLengthMode_D}</summary>
        public LabelOverLengthMode LabelOverLengthMode
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_maxLabelLength_D}</summary>
        public int MaxLabelLength
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_smallGeometryLabeled_D}</summary>
        public bool SmallGeometryLabeled
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_rangeExpression_D}</summary>
        public string RangeExpression
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_numericPrecision_D}</summary>
        public int NumericPrecision
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_Items_D}</summary>
        public IList<ThemeLabelItem> Items
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_labelExpression_D}</summary>
        public string LabelExpression
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabel_attribute_overlapAvoided_D}</summary>
        public bool OverlapAvoided
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeLabel_attribute_MatrixCells_D}</summary>
        public LabelMatrixCell[,] MatrixCells
        {
            get;
            set;
        }

        internal static string ToJson(ThemeLabel themeLabel)
        {
            string json = "{";

            List<string> list = new List<string>();
            if (themeLabel.AlongLine != null)
            {
                list.Add(ThemeLabelAlongLine.ToJson(themeLabel.AlongLine));
            }
            else
            {
                list.Add(string.Format("\"alongLineDirection\":\"{0}\"", AlongLineDirection.ALONG_LINE_NORMAL));
                list.Add("\"labelRepeatInterval\":0.0");
            }

            if (themeLabel.BackSetting != null)
            {
                list.Add(ThemeLabelBackSetting.ToJson(themeLabel.BackSetting));
            }
            else
            {
                list.Add(string.Format("\"backStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
                list.Add("\"labelBackShape\":\"ROUNDRECT\"");
            }

            if (themeLabel.Flow != null)
            {
                list.Add(ThemeFlow.ToJson(themeLabel.Flow));
            }
            else
            {
                list.Add(string.Format("\"leaderLineStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            if (themeLabel.Items != null && themeLabel.Items.Count > 1)
            {
                List<string> itemList = new List<string>();
                foreach (var item in themeLabel.Items)
                {
                    itemList.Add(ThemeLabelItem.ToJson(item));
                }

                list.Add(string.Format("\"items\":[{0}]", string.Join(",", itemList.ToArray())));
            }
            else
            {
                list.Add("\"items\":[]");
            }

            if (!string.IsNullOrEmpty(themeLabel.LabelExpression))
            {
                list.Add(string.Format("\"labelExpression\":\"{0}\"", themeLabel.LabelExpression));
            }
            else
            {
                list.Add("\"labelExpression\":\"\"");
            }

            list.Add(string.Format("\"labelOverLengthMode\":\"{0}\"", themeLabel.LabelOverLengthMode));

            list.Add(string.Format("\"maxLabelLength\":{0}", themeLabel.MaxLabelLength.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            list.Add(string.Format("\"numericPrecision\":{0}", themeLabel.NumericPrecision.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            if (themeLabel.Offset != null)
            {
                list.Add(ThemeOffset.ToJson(themeLabel.Offset));
            }
            else
            {
                list.Add("\"offsetX\":\"\"");
                list.Add("\"offsetY\":\"\"");
            }

            list.Add(string.Format("\"overlapAvoided\":{0}", themeLabel.OverlapAvoided.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            if (!string.IsNullOrEmpty(themeLabel.RangeExpression))
            {
                list.Add(string.Format("\"rangeExpression\":\"{0}\"", themeLabel.RangeExpression));
            }
            else
            {
                list.Add("\"rangeExpression\":\"\"");
            }

            list.Add(string.Format("\"smallGeometryLabeled\":{0}", themeLabel.SmallGeometryLabeled.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            if (themeLabel.Text != null)
            {
                list.Add(ThemeLabelText.ToJson(themeLabel.Text));
            }
            else
            {
                list.Add("\"minTextHeight\":0");
                list.Add("\"maxTextWidth\":0");
                list.Add("\"minTextWidth\":0");
                list.Add("\"maxTextHeight\":0");
                list.Add(string.Format("\"uniformStyle\":{0}", ServerTextStyle.ToJson(new ServerTextStyle())));
                list.Add("\"uniformMixedStyle\":null");
            }

            if (themeLabel.MatrixCells != null)
            {
                List<string> cellList = new List<string>();
                for (int column = 0; column < themeLabel.MatrixCells.GetLength(1); column++)          //列
                {
                    List<string> columnList = new List<string>();

                    for (int row = 0; row < themeLabel.MatrixCells.GetLength(0); row++)                  //行
                    {
                        if (themeLabel.MatrixCells[row, column] is LabelImageCell)
                        {
                            columnList.Add(LabelImageCell.ToJson((LabelImageCell)themeLabel.MatrixCells[row, column]));
                        }
                        else if (themeLabel.MatrixCells[row, column] is LabelSymbolCell)
                        {
                            columnList.Add(LabelSymbolCell.ToJson((LabelSymbolCell)themeLabel.MatrixCells[row, column]));
                        }
                        else if (themeLabel.MatrixCells[row, column] is LabelThemeCell)
                        {
                            columnList.Add(LabelThemeCell.ToJson((LabelThemeCell)themeLabel.MatrixCells[row, column]));
                        }
                    }

                    cellList.Add(string.Format("[{0}]", string.Join(",", columnList.ToArray())));
                }

                list.Add(string.Format("\"matrixCells\":[{0}]", string.Join(",", cellList.ToArray())));
            }
            else
            {
                list.Add("\"matrixCells\":null");
            }

            if (themeLabel.MemoryData != null)
            {
                list.Add("\"memoryData\":" + themeLabel.ToJson(themeLabel.MemoryData));
            }
            else
            {
                list.Add("\"memoryData\":null");
            }
            list.Add("\"type\":\"LABEL\"");

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal static ThemeLabel FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeLabel themeLabel = new ThemeLabel();

            ThemeLabelAlongLine alongLine = new ThemeLabelAlongLine();
            if (json["alongLineDirection"] != null)
            {
                alongLine.AlongLineDirection = (AlongLineDirection)Enum.Parse(typeof(AlongLineDirection), json["alongLineDirection"], true);
            }
            else
            {
                //不处理null的情况
            }
            alongLine.AngleFixed = (bool)json["angleFixed"];
            alongLine.IsAlongLine = (bool)json["alongLine"];
            alongLine.LabelRepeatInterval = (double)json["labelRepeatInterval"];
            alongLine.RepeatedLabelAvoided = (bool)json["repeatedLabelAvoided"];
            alongLine.RepeatIntervalFixed = (bool)json["repeatIntervalFixed"];
            themeLabel.AlongLine = alongLine;

            ThemeLabelBackSetting backSetting = new ThemeLabelBackSetting();
            if (json["backStyle"] != null)
            {
                backSetting.BackStyle = ServerStyle.FromJson((JsonObject)json["backStyle"]);
            }
            if (json["labelBackShape"] != null)
            {
                backSetting.LabelBackShape = (LabelBackShape)Enum.Parse(typeof(LabelBackShape), json["labelBackShape"], true);
            }
            else
            {
                //不处理null的情况
            }

            themeLabel.BackSetting = backSetting;

            ThemeFlow flow = new ThemeFlow();
            flow.FlowEnabled = (bool)json["flowEnabled"];
            flow.LeaderLineDisplayed = (bool)json["leaderLineDisplayed"];
            if (json["leaderLineStyle"] != null)
            {
                flow.LeaderLineStyle = ServerStyle.FromJson((JsonObject)json["leaderLineStyle"]);
            }
            themeLabel.Flow = flow;

            List<ThemeLabelItem> items = new List<ThemeLabelItem>();
            if (json["items"] != null && json["items"].Count > 0)
            {
                for (int i = 0; i < json["items"].Count; i++)
                {
                    items.Add(ThemeLabelItem.FromJson(((JsonObject)json["items"][i])));
                }
            }
            themeLabel.Items = items;

            themeLabel.LabelExpression = (string)json["labelExpression"];
            if (json["labelOverLengthMode"] != null)
            {
                themeLabel.LabelOverLengthMode = (LabelOverLengthMode)Enum.Parse(typeof(LabelOverLengthMode), json["labelOverLengthMode"], true);
            }
            else
            {
                //不处理null的情况
            }

            //themeLabel.MatrixCells
            if (json["matrixCells"] != null)
            {
                int rowCount = ((JsonArray)json["matrixCells"]).Count;
                int columnCount = ((JsonArray)((JsonArray)json["matrixCells"])[0]).Count;
                LabelMatrixCell[,] matrixCells = new LabelMatrixCell[columnCount, rowCount];

                for (int column = 0; column < ((JsonArray)json["matrixCells"]).Count; column++)
                {
                    JsonArray cells = (JsonArray)((JsonArray)json["matrixCells"])[column];
                    for (int row = 0; row < cells.Count; row++)
                    {
                        if (cells[row].ContainsKey("height") && cells[row].ContainsKey("pathField") && cells[row].ContainsKey("rotation") && cells[row].ContainsKey("sizeFixed") && cells[row].ContainsKey("width"))
                        {
                            matrixCells[row, column] = (LabelImageCell.FromJson((JsonObject)cells[row]));
                        }
                        else if (cells[row].ContainsKey("style") && cells[row].ContainsKey("symbolIDField"))
                        {
                            matrixCells[row, column] = (LabelSymbolCell.FromJson((JsonObject)cells[row]));
                        }
                        else if (cells[row].ContainsKey("themeLabel"))
                        {
                            matrixCells[row, column] = (LabelThemeCell.FromJson((JsonObject)((JsonObject)cells[row])["themeLabel"]));
                        }
                    }
                }

                themeLabel.MatrixCells = matrixCells;
            }
            themeLabel.MaxLabelLength = (int)json["maxLabelLength"];
            themeLabel.NumericPrecision = (int)json["numericPrecision"];
            themeLabel.Offset = ThemeOffset.FromJson(json);
            themeLabel.OverlapAvoided = (bool)json["overlapAvoided"];
            themeLabel.RangeExpression = (string)json["rangeExpression"];
            themeLabel.SmallGeometryLabeled = (bool)json["smallGeometryLabeled"];
            themeLabel.Text = ThemeLabelText.FromJson(json);
            return themeLabel;
        }
    }
}
