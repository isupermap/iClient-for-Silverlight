﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeLabelItem_Title}</para>
    /// 	<para>${iServerJava6R_ThemeLabelItem_Description}</para>
    /// </summary>
    public class ThemeLabelItem
    {
        /// <summary>${iServerJava6R_ThemeLabelItem_constructor_D}</summary>
        public ThemeLabelItem()
        {
            Visible = true;
        }

        /// <summary>${iServerJava6R_ThemeLabelItem_attribute_caption_D}</summary>
        public string Caption
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelItem_attribute_start_D}</summary>
        public double Start
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelItem_attribute_end_D}</summary>
        public double End
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelItem_attribute_visible_D}</summary>
        public bool Visible
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelItem_attribute_style_D}</summary>
        public ServerTextStyle Style
        {
            get;
            set;
        }

        internal static string ToJson(ThemeLabelItem themeLabelItem)
        {
            string json = "{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(themeLabelItem.Caption))
            {
                list.Add(string.Format("\"caption\":\"{0}\"", themeLabelItem.Caption));
            }
            else
            {
                list.Add("\"caption\":\"\"");
            }

            if (themeLabelItem.Style != null)
            {
                list.Add(string.Format("\"style\":{0}", ServerTextStyle.ToJson(themeLabelItem.Style)));
            }
            else
            {
                list.Add(string.Format("\"style\":{0}", ServerTextStyle.ToJson(new ServerTextStyle())));
            }

            list.Add(string.Format("\"visible\":{0}", themeLabelItem.Visible.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            list.Add(string.Format("\"end\":\"{0}\"", themeLabelItem.End.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            list.Add(string.Format("\"start\":\"{0}\"", themeLabelItem.Start.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal static ThemeLabelItem FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeLabelItem item = new ThemeLabelItem();
            item.Caption = json["caption"];
            item.End = (double)json["end"];
            item.Start = (double)json["start"];
            if (json["style"] != null)
            {
                item.Style = ServerTextStyle.FromJson((JsonObject)json["style"]);
            }
            item.Visible = (bool)json["visible"];
            return item;
        }

    }
}
