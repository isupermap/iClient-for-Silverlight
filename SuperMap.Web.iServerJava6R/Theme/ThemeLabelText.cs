﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeLabelText_Title}</para>
    /// 	<para>${iServerJava6R_ThemeLabelText_Description}</para>
    /// </summary>
    public class ThemeLabelText
    {
        /// <summary>${iServerJava6R_ThemeLabelText_constructor_D}</summary>
        public ThemeLabelText()
        {
        }

        /// <summary>${iServerJava6R_ThemeLabelText_attribute_MaxTextHeight_D}</summary>
        public int MaxTextHeight
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelText_attribute_MaxTextWidth_D}</summary>
        public int MaxTextWidth
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelText_attribute_MinTextWidth_D}</summary>
        public int MinTextHeight
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelText_attribute_MinTextHeight_D}</summary>
        public int MinTextWidth
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelText_attribute_UniformSytle_D}</summary>
        public ServerTextStyle UniformStyle
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeLabelText_attribute_UniformMixedSytle_D}</summary>
        public LabelMixedTextStyle UniformMixedStyle
        {
            get;
            set;
        }

        internal static string ToJson(ThemeLabelText labelText)
        {
            string json = "";

            System.Collections.Generic.List<string> list = new System.Collections.Generic.List<string>();

            list.Add(string.Format("\"minTextHeight\":{0}", labelText.MinTextHeight.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            list.Add(string.Format("\"maxTextWidth\":{0}", labelText.MaxTextWidth.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            list.Add(string.Format("\"minTextWidth\":{0}", labelText.MinTextWidth.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            list.Add(string.Format("\"maxTextHeight\":{0}", labelText.MaxTextHeight.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            if (labelText.UniformStyle != null)
            {
                list.Add(string.Format("\"uniformStyle\":{0}", ServerTextStyle.ToJson(labelText.UniformStyle)));
            }
            else
            {
                list.Add(string.Format("\"uniformStyle\":{0}", ServerTextStyle.ToJson(new ServerTextStyle())));
            }

            if (labelText.UniformMixedStyle != null)
            {
                list.Add(string.Format("\"uniformMixedStyle\":{0}", LabelMixedTextStyle.ToJson(labelText.UniformMixedStyle)));
            }
            else
            {
                list.Add("\"uniformMixedStyle\":null");
            }

            json = string.Join(",", list.ToArray());
            return json;
        }

        internal static ThemeLabelText FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            ThemeLabelText labelText = new ThemeLabelText();
            labelText.MaxTextHeight = (int)json["maxTextHeight"];
            labelText.MaxTextWidth = (int)json["maxTextWidth"];
            labelText.MinTextHeight = (int)json["minTextHeight"];
            labelText.MinTextWidth = (int)json["minTextWidth"];
            labelText.UniformMixedStyle = LabelMixedTextStyle.FromJson((JsonObject)json["uniformMixedStyle"]);
            labelText.UniformStyle = ServerTextStyle.FromJson((JsonObject)json["uniformStyle"]);
            return labelText;
        }
    }
}
