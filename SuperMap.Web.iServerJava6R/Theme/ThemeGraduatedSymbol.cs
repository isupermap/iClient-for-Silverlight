﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGraduatedSymbol_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGraduatedSymbol_Description}</para>
    /// 	<para>
    /// 		<img src="ThemeGraduatedSymboliServer6.bmp"/>
    /// 	</para>
    /// </summary>
    public class ThemeGraduatedSymbol:Theme
    {
        /// <summary>${iServerJava6R_ThemeGraduatedSymbol_constructor_D}</summary>
        public ThemeGraduatedSymbol()
        {
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbol_attribute_BaseValue_D}</summary>
        public double BaseValue 
        { 
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbol_attribute_Expression_D}</summary>
        public string Expression 
        {
            get; 
            set;
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbol_attribute_GraduatedMode_D}</summary>
        public GraduatedMode GraduatedMode
        { 
            get; 
            set;
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbol_attribute_Offset_D}</summary>
        public ThemeOffset Offset 
        {            
            get; 
            set; 
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbol_attribute_style_D}</summary>
        public ThemeGraduatedSymbolStyle Style
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbol_attribute_Flow_D}</summary>
        public ThemeFlow Flow
        {
            get;
            set;
        }

        internal static string ToJson(ThemeGraduatedSymbol graduatedSymbolTheme)
        {
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"baseValue\":{0}", graduatedSymbolTheme.BaseValue.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            if (!string.IsNullOrEmpty(graduatedSymbolTheme.Expression))
            {
                list.Add(string.Format("\"expression\":\"{0}\"", graduatedSymbolTheme.Expression));
            }
            else
            {
                list.Add("\"expression\":\"\"");
            }

            list.Add(string.Format("\"graduatedMode\":\"{0}\"", graduatedSymbolTheme.GraduatedMode.ToString()));

            if (graduatedSymbolTheme.Offset != null)
            {
                list.Add(ThemeOffset.ToJson(graduatedSymbolTheme.Offset));
            }
            else 
            {
                list.Add("\"offsetX\":\"\"");
                list.Add("\"offsetY\":\"\"");
            }

            if (graduatedSymbolTheme.Style != null)
            {
                list.Add(ThemeGraduatedSymbolStyle.ToJson(graduatedSymbolTheme.Style));
            }
            else
            {
                list.Add(string.Format("\"positiveStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
                list.Add(string.Format("\"negativeStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
                list.Add(string.Format("\"zeroStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            if (graduatedSymbolTheme.Flow != null)
            {
                list.Add(ThemeFlow.ToJson(graduatedSymbolTheme.Flow));
            }
            else
            {
                list.Add(string.Format("\"leaderLineStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            if (graduatedSymbolTheme.MemoryData != null)
            {
                list.Add("\"memoryData\":" + graduatedSymbolTheme.ToJson(graduatedSymbolTheme.MemoryData));
            }
            else
            {
                list.Add("\"memoryData\":null");
            }
            list.Add("\"type\":\"GRADUATEDSYMBOL\"");

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal static ThemeGraduatedSymbol FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeGraduatedSymbol graduatedSymbol = new ThemeGraduatedSymbol();
            graduatedSymbol.BaseValue = (double)json["baseValue"];
            graduatedSymbol.Expression = (string)json["expression"];
            graduatedSymbol.Flow = ThemeFlow.FromJson(json);
            if (json["graduatedMode"] != null)
            {
                graduatedSymbol.GraduatedMode = (GraduatedMode)Enum.Parse(typeof(GraduatedMode), json["graduatedMode"], true);
            }
            else
            {
                //这里不处理为空时的情况
            }
            graduatedSymbol.Offset = ThemeOffset.FromJson(json);
            graduatedSymbol.Style = ThemeGraduatedSymbolStyle.FromJson((JsonObject)json);
            return graduatedSymbol;
        }
    }
}
