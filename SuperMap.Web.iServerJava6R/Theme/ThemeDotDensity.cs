﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeDotDensity_Title}</para>
    /// 	<para>${iServerJava6R_ThemeDotDensity_Description}</para>
    /// 	<para>
    /// 		<img src="ThemeDotDensityiServer6.bmp"/>
    /// 	</para>
    /// </summary>
    public class ThemeDotDensity : Theme
    {
        /// <summary>${iServerJava6R_ThemeDotDensity_constructor_D}</summary>
        public ThemeDotDensity()
        {
            Style = new ServerStyle { MarkerSize = 2 };
            Type = ThemeType.DOTDENSITY;
        }

        /// <summary>${iServerJava6R_ThemeDotDensity_attribute_DotExpression_D}</summary>
        public string DotExpression
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeDotDensity_attribute_Value_D}</summary>
        public double Value
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeDotDensity_attribute_Style_D}</summary>
        public ServerStyle Style
        {
            get;
            set;
        }

        internal static string ToJson(ThemeDotDensity dotDensity)
        {
            string json = "{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(dotDensity.DotExpression))
            {
                list.Add(string.Format("\"dotExpression\":\"{0}\"", dotDensity.DotExpression));
            }
            else
            {
                list.Add("\"dotExpression\":\"\"");
            }

            if (dotDensity.Style != null)
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(dotDensity.Style)));
            }
            else
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            list.Add(string.Format("\"value\":{0}", dotDensity.Value.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            list.Add("\"type\":\"DOTDENSITY\"");

            if (dotDensity.MemoryData!=null)
            {
                list.Add( "\"memoryData\":" + dotDensity.ToJson(dotDensity.MemoryData));
            }
            else
            {
                list.Add("\"memoryData\":null");
            }

            json += string.Join(",", list.ToArray());

            json += "}";
            return json;
        }

        internal static ThemeDotDensity FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeDotDensity dotDensity = new ThemeDotDensity();
            dotDensity.DotExpression = (string)json["dotExpression"];
            dotDensity.Style = ServerStyle.FromJson((JsonObject)json["style"]);
            dotDensity.Value = (double)json["value"];
            return dotDensity;
        }
    }
}
