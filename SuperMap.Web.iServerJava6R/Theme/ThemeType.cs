﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>${iServerJava6R_ThemeType_Title}</summary>
    public enum ThemeType
    {
        /// <summary>${iServerJava6R_ThemeType_attribute_DOTDENSITY_D}</summary>
        DOTDENSITY,//           点密度专题图。 
        /// <summary>${iServerJava6R_ThemeType_attribute_GRADUATEDSYMBOL_D}</summary>
        GRADUATEDSYMBOL,//           等级符号专题图。 
        /// <summary>${iServerJava6R_ThemeType_attribute_GRAPH_D}</summary>
        GRAPH,//           统计专题图。
        /// <summary>${iServerJava6R_ThemeType_attribute_LABEL_D}</summary>
        LABEL,//           标签专题图。 
        /// <summary>${iServerJava6R_ThemeType_attribute_RANGE_D}</summary>
        RANGE,//           分段专题图。
        /// <summary>${iServerJava6R_ThemeType_attribute_UNIQUE_D}</summary>
        UNIQUE,//           単值专题图。 
        /// <summary>${iServerJava6R_ThemeType_attribute_GRIDRANGE_D}</summary>
        GRIDRANGE,//         栅格分段专题图。 
        /// <summary>${iServerJava6R_ThemeType_attribute_GRIDUNIQUE_D}</summary>
        GRIDUNIQUE,//         栅格单值专题图。 
    }
}
