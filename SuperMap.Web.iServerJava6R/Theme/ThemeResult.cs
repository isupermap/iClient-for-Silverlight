﻿using System;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeResult_Title}</para>
    /// 	<para>${iServerJava6R_ThemeResult_Description}</para>
    /// </summary>
    public class ThemeResult
    {
        internal ThemeResult()
        { 

        }

        /// <summary>${iServerJava6R_ThemeResult_attribute_ResourceInfo_D}</summary>
        public ResourceInfo ResourceInfo
        {
            get;
            private set;
        }
        /// <summary>${iServerJava6R_ThemeResult_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_ThemeResult_method_FromJson_return}</returns>
        /// <param name="jsonResult">${iServerJava6R_ThemeResult_method_FromJson_param_jsonObject}</param>
        public static ThemeResult FromJson(string jsonResult)
        {
            JsonObject resultObject = (JsonObject)JsonValue.Parse(jsonResult);
            ResourceInfo resourceInfo = new ResourceInfo
            {
                NewResourceID = resultObject["newResourceID"],
                NewResourceLocation = resultObject["newResourceLocation"],
                Succeed = (bool)resultObject["succeed"]
            };

            ThemeResult themeResult = new ThemeResult
            {
                ResourceInfo = resourceInfo
            };
            return themeResult;
        }
    }
}
