﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>${iServerJava6R_ServerTextStyle_Title}</summary>
    public class ServerTextStyle
    {
        /// <summary>${iServerJava6R_ServerTextStyle_constructor_D}</summary>
        public ServerTextStyle()
        {
            FontHeight = 10;
            FontWeight = 400;
            FontWidth = 0;
            FontName = "微软雅黑";
            SizeFixed = true;
        }

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_align_D}</summary>
        public TextAlignment Align { get; set; }             //文本的对齐方式。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_backColor_D}</summary>
        public Color BackColor { get; set; }                        //文本的背景色。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_backOpaque_D}</summary>
        public bool BackOpaque { get; set; }                        //文本背景是否不透明，true 表示文本背景不透明。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_bold_D}</summary>
        public bool Bold { get; set; }                              //文本是否为粗体字，true 表示为粗体。 

        ///// <summary>${iServerJava6R_ServerTextStyle_attribute_fixedTextSize_D}</summary>
        //public int FixedTextSize { get; set; }                      //固定文本的大小尺寸。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_fontHeight_D}</summary>
        public double FontHeight { get; set; }                      //文本字体的高度。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_fontName_D}</summary>
        public string FontName { get; set; }                        //文本字体的名称。 

        ///// <summary>${iServerJava6R_ServerTextStyle_attribute_fontScale_D}</summary>
        //public double FontScale { get; set; }                       //文本字体的缩放比例。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_fontWeight_D}</summary>
        public int FontWeight { get; set; }                         //文本字体的磅数，表示粗体的具体数值。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_fontWidth_D}</summary>
        public double FontWidth { get; set; }                       //文本字体的宽度。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_foreColor_D}</summary>
        public Color ForeColor { get; set; }                        //文本的前景色。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_italic_D}</summary>
        public bool Italic { get; set; }                            //文本是否采用斜体，true 表示采用斜体。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_italicAngle_D}</summary>
        public double ItalicAngle { get; set; }                     //字体倾斜角度，正负度之间，以度为单位，精确到0.1度。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_opaqueRate_D}</summary>
        public int OpaqueRate { get; set; }                         //注记文字的不透明度。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_outline_D}</summary>
        public bool Outline { get; set; }                           //是否以轮廓的方式来显示文本的背景。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_rotation_D}</summary>
        public double Rotation { get; set; }                        //文本旋转的角度。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_shadow_D}</summary>
        public bool Shadow { get; set; }                            //文本是否有阴影。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_sizeFixed_D}</summary>
        public bool SizeFixed { get; set; }                         //文本大小是否固定。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_strikeout_D}</summary>
        public bool Strikeout { get; set; }                         //文本字体是否加删除线。 

        /// <summary>${iServerJava6R_ServerTextStyle_attribute_underline_D}</summary>
        public bool Underline { get; set; }                         //文本字体是否加下划线。         

        internal static string ToJson(ServerTextStyle serverTextStyle)
        {
            List<string> jsonlist = new List<string>();
            string json = "{";

            jsonlist.Add(string.Format("\"align\":\"{0}\"", serverTextStyle.Align.ToString()));
            //backcolor格式不对，提交时将此注释删除
            jsonlist.Add(string.Format("\"backColor\":{0}", ServerColor.ToJson(serverTextStyle.BackColor.ToServerColor())));
            jsonlist.Add(string.Format("\"backOpaque\":\"{0}\"", serverTextStyle.BackOpaque.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            jsonlist.Add(string.Format("\"bold\":\"{0}\"", serverTextStyle.Bold.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            //jsonlist.Add(string.Format("\"fixedTextSize\":\"{0}\"", serverTextStyle.FixedTextSize.ToString()));
            jsonlist.Add(string.Format("\"fontHeight\":\"{0}\"", serverTextStyle.FontHeight.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            jsonlist.Add(string.Format("\"fontName\":\"{0}\"", serverTextStyle.FontName));
            //jsonlist.Add(string.Format("\"fontScale\":\"{0}\"", serverTextStyle.FontScale.ToString()));
            jsonlist.Add(string.Format("\"fontWeight\":\"{0}\"", serverTextStyle.FontWeight.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            jsonlist.Add(string.Format("\"fontWidth\":\"{0}\"", serverTextStyle.FontWidth.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            //foreColor格式不对，提交时将此注释删除
            jsonlist.Add(string.Format("\"foreColor\":{0}", ServerColor.ToJson(serverTextStyle.ForeColor.ToServerColor())));
            jsonlist.Add(string.Format("\"italic\":\"{0}\"", serverTextStyle.Italic.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            jsonlist.Add(string.Format("\"italicAngle\":\"{0}\"", serverTextStyle.ItalicAngle.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            jsonlist.Add(string.Format("\"opaqueRate\":\"{0}\"", serverTextStyle.OpaqueRate.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            jsonlist.Add(string.Format("\"outline\":\"{0}\"", serverTextStyle.Outline.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            jsonlist.Add(string.Format("\"rotation\":\"{0}\"", serverTextStyle.Rotation.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            jsonlist.Add(string.Format("\"shadow\":\"{0}\"", serverTextStyle.Shadow.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            jsonlist.Add(string.Format("\"sizeFixed\":\"{0}\"", serverTextStyle.SizeFixed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            jsonlist.Add(string.Format("\"strikeout\":\"{0}\"", serverTextStyle.Strikeout.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            jsonlist.Add(string.Format("\"underline\":\"{0}\"", serverTextStyle.Underline.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            json += string.Join(",", jsonlist.ToArray());
            json += "}";
            return json;
        }

        internal static ServerTextStyle FromJson(JsonObject json)
        {
            if (json == null) return null;
            ServerTextStyle textStyle = new ServerTextStyle();

            textStyle.Align = (TextAlignment)Enum.Parse(typeof(TextAlignment), json["align"], true);
            textStyle.BackColor = ServerColor.FromJson((JsonObject)json["backColor"]).ToColor();
            textStyle.BackOpaque = (bool)json["backOpaque"];
            textStyle.Bold = (bool)json["bold"];
            textStyle.FontHeight = (double)json["fontHeight"];
            textStyle.FontName = json["fontName"];
            textStyle.FontWeight = (int)json["fontWeight"];
            textStyle.FontWidth = (double)json["fontWidth"];
            textStyle.ForeColor = ServerColor.FromJson((JsonObject)json["foreColor"]).ToColor();
            textStyle.Italic = (bool)json["italic"];
            textStyle.ItalicAngle = (double)json["italicAngle"];
            textStyle.OpaqueRate = (int)json["opaqueRate"];
            textStyle.Outline = (bool)json["outline"];
            textStyle.Rotation = (double)json["rotation"];
            textStyle.Shadow = (bool)json["shadow"];
            textStyle.SizeFixed = (bool)json["sizeFixed"];
            textStyle.Strikeout = (bool)json["strikeout"];
            textStyle.Underline = (bool)json["underline"];
            return textStyle;
        }
    }
}
