﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGraphText_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGraphText_Description}</para>
    /// </summary>
    public class ThemeGraphText
    {
        /// <summary>${iServerJava6R_ThemeGraphText_constructor_D}</summary>
        public ThemeGraphText()
        {
            //GraphTextDisplayed = false;
        }
        /// <summary>${iServerJava6R_ThemeGraphText_attribute_graphTextDisplayed_D}</summary>
        public bool GraphTextDisplayed
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeGraphText_attribute_graphTextFormat_D}</summary>
        public ThemeGraphTextFormat GraphTextFormat
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_ThemeGraphText_attribute_graphTextStyle_D}</summary>
        public ServerTextStyle GraphTextStyle
        {
            get;
            set;
        }

        internal static string ToJson(ThemeGraphText graphText)
        {
            string json = "";
            List<string> list = new List<string>();

            list.Add(string.Format("\"graphTextDisplayed\":{0}", graphText.GraphTextDisplayed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            list.Add(string.Format("\"graphTextFormat\":\"{0}\"", graphText.GraphTextFormat));

            if (graphText.GraphTextStyle != null)
            {
                list.Add(string.Format("\"graphTextStyle\":{0}", ServerTextStyle.ToJson(graphText.GraphTextStyle)));
            }
            else
            {
                list.Add(string.Format("\"graphTextStyle\":{0}", ServerTextStyle.ToJson(new ServerTextStyle())));
            }
            json = string.Join(",", list.ToArray());
            return json;
        }

        internal static ThemeGraphText FromJson(System.Json.JsonObject json)
        {
            if (json == null) return null;

            ThemeGraphText graphText = new ThemeGraphText();
            graphText.GraphTextDisplayed = (bool)json["graphTextDisplayed"];
            if (json["graphTextFormat"] != null)
            {
                graphText.GraphTextFormat = (ThemeGraphTextFormat)Enum.Parse(typeof(ThemeGraphTextFormat), json["graphTextFormat"], true);
            }
            else
            {
                //不处理null的情况
            }
            graphText.GraphTextStyle = ServerTextStyle.FromJson((JsonObject)json["graphTextStyle"]);
            return graphText;
        }
    }
}
