﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGraduatedSymbolStyle_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGraduatedSymbolStyle_Description}</para>
    /// </summary>
    public class ThemeGraduatedSymbolStyle
    {
        /// <summary>${iServerJava6R_ThemeGraduatedSymbolStyle_constructor_D}</summary>
        public ThemeGraduatedSymbolStyle()
        {

        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbolStyle_attribute_positiveStyle_D}</summary>
        public ServerStyle PositiveStyle
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbolStyle_attribute_negativeStyle_D}</summary>
        public ServerStyle NegativeStyle
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbolStyle_attribute_negativeDisplayed_D}</summary>
        public bool NegativeDisplayed
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbolStyle_attribute_zeroStyle_D}</summary>
        public ServerStyle ZeroStyle
        {
            get;
            set;
        }

        /// <summary>${iServerJava6R_ThemeGraduatedSymbolStyle_attribute_zeroDisplayed_D}</summary>
        public bool ZeroDisplayed
        {
            get;
            set;
        }

        internal static string ToJson(ThemeGraduatedSymbolStyle valueSection)
        {
            string json = "";
            List<string> list = new List<string>();

            if (valueSection.PositiveStyle != null)
            {
                list.Add(string.Format("\"positiveStyle\":{0}", ServerStyle.ToJson(valueSection.PositiveStyle)));
            }
            else
            {
                list.Add(string.Format("\"positiveStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            if (valueSection.NegativeStyle != null)
            {
                list.Add(string.Format("\"negativeStyle\":{0}", ServerStyle.ToJson(valueSection.NegativeStyle)));
            }
            else
            {
                list.Add(string.Format("\"negativeStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            if (valueSection.ZeroStyle != null)
            {
                list.Add(string.Format("\"zeroStyle\":{0}", ServerStyle.ToJson(valueSection.ZeroStyle)));
            }
            else
            {
                list.Add(string.Format("\"zeroStyle\":{0}", ServerStyle.ToJson(new ServerStyle())));
            }

            if (valueSection.ZeroDisplayed)
            {
                list.Add(string.Format("\"zeroDisplayed\":{0}", valueSection.ZeroDisplayed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            }

            if (valueSection.NegativeDisplayed)
            {
                list.Add(string.Format("\"negativeDisplayed\":{0}", valueSection.NegativeDisplayed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            }
            json = string.Join(",", list.ToArray());
            return json;
        }


        internal static ThemeGraduatedSymbolStyle FromJson(System.Json.JsonObject json)
        {
            if (json == null) return null;
            ThemeGraduatedSymbolStyle symbolStyle = new ThemeGraduatedSymbolStyle();
            symbolStyle.NegativeDisplayed = (bool)json["negativeDisplayed"];
            symbolStyle.NegativeStyle = ServerStyle.FromJson((JsonObject)json["negativeStyle"]);
            symbolStyle.PositiveStyle = ServerStyle.FromJson((JsonObject)json["positiveStyle"]);
            symbolStyle.ZeroDisplayed = (bool)json["zeroDisplayed"];
            symbolStyle.ZeroStyle = ServerStyle.FromJson((JsonObject)json["zeroStyle"]);
            return symbolStyle;
        }
    }
}
