﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_ThemeGridRangeItem_Title}</para>
    /// 	<para>${iServerJava6R_ThemeGridRangeItem_Description}</para>
    /// </summary>
   public class ThemeGridRangeItem
    {
        /// <summary>${iServerJava6R_ThemeGridRangeItem_constructor_D}</summary>
		public ThemeGridRangeItem()
        {
            this.Visible = true;
           
        }
       /// <summary>${iServerJava6R_ThemeGridRangeItem_attribute_Caption_D}</summary>
       public String  Caption
		{
			get;
            set;
		}
		
		 /// <summary>${iServerJava6R_ThemeGridRangeItem_attribute_Color_D}</summary>		
		public ServerColor Color
		{
			get;
            set;
		}
		
        /// <summary>${iServerJava6R_ThemeGridRangeItem_attribute_End_D}</summary>		
		public double End
		{
			get;
            set;
		}
		
		/// <summary>${iServerJava6R_ThemeGridRangeItem_attribute_Start_D}</summary>		 
		public double Start
		{
			get;
            set;
		}
		
        /// <summary>${iServerJava6R_ThemeGridRangeItem_attribute_Visible_D}</summary>		 
        public Boolean Visible
        {
            get;
            set;
        }

        internal static string ToJson(ThemeGridRangeItem themeGridRangeItem)
        {
            string json = "{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(themeGridRangeItem.Caption))
            {
                list.Add(string.Format("\"caption\":\"{0}\"", themeGridRangeItem.Caption));
            }
            else
            {
                list.Add("\"caption\":\"\"");
            }

            if (themeGridRangeItem.Color != null)
            {
                list.Add(string.Format("\"color\":{0}", ServerColor.ToJson(themeGridRangeItem.Color)));
            }
            else
            {
                list.Add(string.Format("\"color\":{0}", ServerColor.ToJson(new ServerColor())));
            }

            list.Add(string.Format("\"visible\":{0}", themeGridRangeItem.Visible.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));

            list.Add(string.Format("\"end\":{0}", themeGridRangeItem.End.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            list.Add(string.Format("\"start\":{0}", themeGridRangeItem.Start.ToString(System.Globalization.CultureInfo.InvariantCulture)));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal static ThemeGridRangeItem FromJson(JsonObject json)
        {
            if (json == null) return null;
            ThemeGridRangeItem item = new ThemeGridRangeItem();

            item.Caption = json["caption"];
            item.End = (double)json["end"];
            item.Start = (double)json["start"];
            if (json["color"] != null)
            {
                item.Color = ServerColor.FromJson((JsonObject)json["color"]);
            }
            item.Visible = (bool)json["visible"];
            return item;
        }
		
        
    }

}
