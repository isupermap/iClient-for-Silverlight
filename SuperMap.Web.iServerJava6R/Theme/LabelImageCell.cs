﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava6R
{
    /// <summary>
    /// 	<para>${iServerJava6R_LabelImageCell_Title}</para>
    /// 	<para>${iServerJava6R_LabelImageCell_Description}</para>
    /// </summary>
    public class LabelImageCell : LabelMatrixCell
    {
        /// <summary>${iServerJava6R_LabelImageCell_constructor_D}</summary>
        public LabelImageCell()
        {
            height = 10;
            width = 10;
            Type = LabelMatrixCellType.IMAGE;
        }

        private double height;
        /// <summary>${iServerJava6R_LabelImageCell_attribute_Height_D}</summary>
        public double Height
        {
            get
            {
                return height;
            }
            set
            {
                if (value < 0)
                {
                    value = 10;
                }

                height = value;
            }
        }
        /// <summary>${iServerJava6R_LabelImageCell_attribute_PathField_D}</summary>
        public string PathField
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_LabelImageCell_attribute_Rotation_D}</summary>
        public double Rotation
        {
            get;
            set;
        }
        /// <summary>${iServerJava6R_LabelImageCell_attribute_SizeFixed_D}</summary>
        public bool SizeFixed
        {
            get;
            set;
        }

        private double width;
        /// <summary>${iServerJava6R_LabelImageCell_attribute_Width_D}</summary>
        public double Width
        {
            get
            {
                return width;
            }
            set
            {
                if (width < 0)
                {
                    value = 10;
                }
                width = value;
            }
        }

        internal static string ToJson(LabelImageCell imageCell)
        {
            string json = "{";
            List<string> list = new List<string>();
            list.Add(string.Format("\"pathField\":\"{0}\"", imageCell.PathField));
            list.Add(string.Format("\"sizeFixed\":{0}", imageCell.SizeFixed.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLower()));
            list.Add(string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"height\":{0}", imageCell.Height));
            list.Add(string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"width\":{0}", imageCell.Width));
            list.Add(string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"rotation\":{0}", imageCell.Rotation));
            list.Add(string.Format("\"type\":\"{0}\"", imageCell.Type));
            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        internal static LabelImageCell FromJson(JsonObject json)
        {
            if (json == null) return null;

            LabelImageCell imageCell = new LabelImageCell();
            imageCell.Height = (double)json["height"];
            imageCell.PathField = (string)json["pathField"];
            imageCell.Rotation = (double)json["rotation"];
            imageCell.SizeFixed = (bool)json["sizeFixed"];
            imageCell.Width = (double)json["width"];

            return imageCell;
        }
    }
}
