﻿

using System.Json;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_EditFeaturesResult_Title}</para>
    /// 	<para>${iServerJava6R_EditFeaturesResult_Description}</para>
    /// </summary>
    public class EditFeaturesResult
    {
        internal EditFeaturesResult()
        {
        }

        /// <summary>${iServerJava6R_EditFeaturesResult_attribute_Succeed_D}</summary>
        public bool Succeed { get; internal set; }
        /// <summary>${iServerJava6R_EditFeaturesResult_attribute_NewResourceLocation_D}</summary>
        public string NewResourceLocation { get; private set; }
        //public HttpError Error { get; private set; }
        /// <summary>${iServerJava6R_EditFeaturesResult_attribute_IDs_D}</summary>
        public List<int> IDs { get; internal set; }

        /// <summary>${iServerJava6R_EditFeaturesResult_method_FromJson_D}</summary>
        /// <returns>${iServerJava6R_EditFeaturesResult_method_FromJson_return}</returns>
        /// <param name="json">${iServerJava6R_EditFeaturesResult_method_FromJson_param_jsonObject}</param>
        public static EditFeaturesResult FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            EditFeaturesResult result = new EditFeaturesResult();

            result.Succeed = (bool)json["succeed"];
            //TODO:
            if (json.ContainsKey("newResourceLocation"))
            {
                result.NewResourceLocation = (string)json["newResourceLocation"];
            }
            //if (json.ContainsKey("error"))
            //{
            //    result.Error = HttpError.FromJson((JsonObject)json["error"]);
            //}

            return result;
        }


    }
}
