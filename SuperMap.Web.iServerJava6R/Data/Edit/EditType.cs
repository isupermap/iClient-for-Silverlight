﻿

namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary><para>${iServerJava6R_EditType_Title}</para></summary>
    public enum EditType
    {

        /// <summary>${iServerJava6R_EditType_attribute_ADD_D}</summary>
        ADD,//创建要素
        /// <summary>${iServerJava6R_EditType_attribute_UPData_D}</summary>
        UPDATA,
        /// <summary>${iServerJava6R_EditType_attribute_DELETE_D}</summary>
        DELETE
    }
}
