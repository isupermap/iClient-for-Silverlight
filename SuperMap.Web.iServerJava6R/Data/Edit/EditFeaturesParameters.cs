﻿

using SuperMap.Web.Core;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_EditFeaturesParameters_Title}</para>
    /// 	<para>${iServerJava6R_EditFeaturesParameters_Description}</para>
    /// </summary>
    public class EditFeaturesParameters
    {
        /// <summary>${iServerJava6R_EditFeaturesParameters_constructor_D}</summary>
        public EditFeaturesParameters()
        {
            EditType = EditType.ADD;
        }
        /// <summary>${iServerJava6R_EditFeaturesParameters_attribute_Features_D}</summary>
        public FeatureCollection Features { get; set; }

        /// <summary>${iServerJava6R_EditFeaturesParameters_attribute_EditType_D}</summary>
        public EditType EditType { get; set; }

        /// <summary>${iServerJava6R_EditFeaturesParameters_attribute_IDs_D}</summary>
        public IList<int> IDs { get; set; }

        /// <summary>${iServerJava6R_EditFeaturesParameters_attribute_IsUseBatche_D}</summary>
        public bool IsUseBatche { get; set; }
        

    }
}
