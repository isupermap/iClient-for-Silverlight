﻿

using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_EditFeaturesEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_EditFeaturesEventArgs_Description}</para>
    /// </summary>
    public class EditFeaturesEventArgs : ServiceEventArgs
    {
        /// <summary>${iServerJava6R_EditFeaturesEventArgs_constructor_D}</summary>
        public EditFeaturesEventArgs(EditFeaturesResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_EditFeaturesEventArgs_attribute_Result_D}</summary>
        public EditFeaturesResult Result { get; private set; }
        /// <summary>${iServerJava6R_EditFeaturesEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
