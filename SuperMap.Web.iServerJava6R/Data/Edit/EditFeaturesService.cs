﻿

using System;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_EditFeaturesService_Title}</para>
    /// 	<para>${iServerJava6R_EditFeaturesService_Description}</para>
    /// </summary>
    public class EditFeaturesService : ServiceBase
    {
        /// <summary>${iServerJava6R_EditFeaturesService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_ComputeWeightMatrixService_constructor_overloads_D}</overloads>
        public EditFeaturesService()
        { }

        /// <summary>${iServerJava6R_ComputeWeightMatrixService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_EditFeaturesService_constructor_param_url}</param>
        public EditFeaturesService(string url)
            : base(url)
        {
        }
        /// <summary>${iServerJava6R_EditFeaturesService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_EditFeaturesService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(EditFeaturesParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${iServerJava6R_EditFeaturesService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_EditFeaturesService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${iServerJava6R_EditFeaturesService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(EditFeaturesParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (parameters.EditType == EditType.ADD)
            {
                if (parameters.IsUseBatche)
                {
                    base.Url += ".json?isUseBatche=true&debug=true";
                }
                else
                {
                    base.Url += ".json?returnContent=true&debug=true";//直接获取添加地物SmID
                }
                
            }
            else if (parameters.EditType == EditType.DELETE)
            {
                base.Url += ".json?_method=DELETE&debug=true";
            }
            else
            {
                base.Url += ".json?_method=PUT&debug=true";
            }

            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true, true);
        }

        private System.Collections.Generic.Dictionary<string, string> GetParameters(EditFeaturesParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (parameters.EditType == EditType.DELETE)
            {
                if (parameters.IDs != null)
                {
                    string ids = string.Empty;
                    List<string> idslist = new List<string>();
                    for (int k = 0; k < parameters.IDs.Count; k++)
                    {
                        //idslist.Add(parameters.IDs[k].ToString());
                        dictionary.Add(k.ToString(System.Globalization.CultureInfo.InvariantCulture), parameters.IDs[k].ToString(System.Globalization.CultureInfo.InvariantCulture));
                    }
                    //ids += string.Join(",", idslist.ToArray());
                    //dictionary.Add("Edit", ids);
                }
                else
                {
                    throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
                }
            }
            else
            {
                if (parameters.Features != null && parameters.Features.Count > 0)
                {
                    string json = "";
                    List<string> list = new List<string>();

                    for (int i = 0; i < parameters.Features.Count; i++)
                    {
                        Dictionary<string, string> dict = new Dictionary<string, string>();
                        ServerFeature sf = parameters.Features[i].ToServerFeature();

                        //FiledNames
                        dict.Add("fieldNames", JsonHelper.FromIList(sf.FieldNames));

                        //FiledValues
                        dict.Add("fieldValues", JsonHelper.FromIList(sf.FieldValues));

                        //Geometry
                        if (parameters.EditType == EditType.UPDATA)
                        {
                            //更新时，ID的个数和Feature的个数不一致更新就不起作用！
                            if (parameters.IDs.Count == parameters.Features.Count)
                            {
                                if (sf.Geometry != null)
                                {
                                    sf.Geometry.ID = parameters.IDs[i];
                                }
                                else
                                {
                                    sf.ID = parameters.IDs[i];
                                }

                            }
                        }

                        dict.Add("ID", sf.ID.ToString(System.Globalization.CultureInfo.InvariantCulture));
                        dict.Add("geometry", sf.Geometry == null ? "null" : ServerGeometry.ToJson(sf.Geometry));

                        list.Add(EditGetStringFromDict(dict));
                    }


                    json += string.Join(",", list.ToArray());
                    dictionary.Add("Edit", json);
                }
            }

            return dictionary;
        }
        private static string EditGetStringFromDict(Dictionary<string, string> parameters)
        {
            string json = "{";
            List<string> list = new List<string>();
            foreach (string key in parameters.Keys)
            {
                list.Add(string.Format("\"{0}\":{1}", new object[] { key, parameters[key] }));
            }
            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            EditFeaturesResult result = null;
            if (JsonObject.Parse(e.Result) is JsonObject)
            {
                JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
                result = EditFeaturesResult.FromJson(jsonObject);

            }
            else
            {
                JsonArray json = (JsonArray)JsonObject.Parse(e.Result);
                result = new EditFeaturesResult();
                result.Succeed = true;
                result.IDs = new List<int>();
                for (int i = 0; i < json.Count; i++)
                {
                    result.IDs.Add((int)json[i]);
                }
            }

            LastResult = result;
            EditFeaturesEventArgs args = new EditFeaturesEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);

        }

        private void OnProcessCompleted(EditFeaturesEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_EditFeaturesService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<EditFeaturesEventArgs> ProcessCompleted;

        private EditFeaturesResult lastResult;
        /// <summary>${iServerJava6R_EditFeaturesService_attribute_lastResult_D}</summary>
        public EditFeaturesResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
