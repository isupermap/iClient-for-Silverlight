﻿

using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetFeaturesBySQLParameters_Title}</para>
    /// 	<para>${iServerJava6R_GetFeaturesBySQLParameters_Description}</para>
    /// </summary>
    public class GetFeaturesBySQLParameters:GetFeaturesParametersBase
    {
        /// <summary>${iServerJava6R_GetFeaturesBySQLParameters_constructor_None_D}</summary>
        public GetFeaturesBySQLParameters()
        {
        }
        /// <summary>${iServerJava6R_GetFeaturesBySQLParameters_attribute_FilterParameter_D}</summary>
        public FilterParameter FilterParameter { get; set; }

        /// <summary>${iServerJava6R_GetFeaturesParametersBase_attribute_MaxFeatures_D}</summary>
        public int MaxFeatures { get; set; }
    }
}
