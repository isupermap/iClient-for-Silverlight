﻿


using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetFeaturesParametersBase_Title}</para>
    /// 	<para>${iServerJava6R_GetFeaturesParametersBase_Description}</para>
    /// </summary>
    public abstract class GetFeaturesParametersBase
    {
        /// <summary><para>${iServerJava6R_GetFeaturesParametersBase_constructor_D}</para></summary>
        public GetFeaturesParametersBase()
        {
            FromIndex = 0;
            ToIndex = -1;
        }

        /// <summary>${iServerJava6R_GetFeaturesParametersBase_attribute_DatasetNames_D}</summary>
        public IList<string> DatasetNames { get; set; }

        /// <summary>${iServerJava6R_GetFeaturesParametersBase_attribute_FromIndex_D}</summary>
        public int FromIndex { get; set; }
        /// <summary>${iServerJava6R_GetFeaturesParametersBase_attribute_ToIndex_D}</summary>
        public int ToIndex { get; set; }
        
        
    }
}
