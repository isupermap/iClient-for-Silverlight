﻿

using System.Collections.Generic;
using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetFeaturesByGeometryParameters_Title}</para>
    /// 	<para>${iServerJava6R_GetFeaturesByGeometryParameters_Description}</para>
    /// </summary>
    public class GetFeaturesByGeometryParameters : GetFeaturesParametersBase
    {
        /// <summary>${iServerJava6R_GetFeaturesByGeometryParameters_constructor_D}</summary>
        public GetFeaturesByGeometryParameters()
        {
        }
        /// <summary>${iServerJava6R_GetFeaturesByGeometryParameters_attribute_Geometry_D}</summary>
        public Geometry Geometry { get; set; }
        /// <summary>${iServerJava6R_GetFeaturesByGeometryParameters_attribute_SpatialQueryMode_D}</summary>
        public SpatialQueryMode SpatialQueryMode { get; set; }
        /// <summary>${iServerJava6R_GetFeaturesByGeometryParameters_attribute_AttributeFilter_D}</summary>
        public string AttributeFilter { get; set; }
        /// <summary>${iServerJava6R_GetFeaturesByBufferParameters_attribute_Fields_D}</summary>
        public IList<string> Fields { get; set; }

    }
}
