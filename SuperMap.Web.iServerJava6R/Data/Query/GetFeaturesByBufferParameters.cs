﻿

using System.Collections.Generic;
using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetFeaturesByBufferParameters_Title}</para>
    /// 	<para>${iServerJava6R_GetFeaturesByBufferParameters_Description}</para>
    /// </summary>
    public class GetFeaturesByBufferParameters : GetFeaturesParametersBase
    {
        /// <summary>${iServerJava6R_GetFeaturesByBufferParameters_constructor_D}</summary>
        public GetFeaturesByBufferParameters()
        {
        }
        /// <summary>${iServerJava6R_GetFeaturesByBufferParameters_attribute_Geometry_D}</summary>
        public Geometry Geometry { get; set; }
        /// <summary>${iServerJava6R_GetFeaturesByBufferParameters_attribute_BufferDistance_D}</summary>
        public double BufferDistance { get; set; }
        /// <summary>${iServerJava6R_GetFeaturesByBufferParameters_attribute_AttributeFilter_D}</summary>
        public string AttributeFilter { get; set; }
        /// <summary>${iServerJava6R_GetFeaturesByBufferParameters_attribute_Fields_D}</summary>
        public IList<string> Fields { get; set; }

    }
}
