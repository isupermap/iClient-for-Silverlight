﻿

using SuperMap.Web.Service;
using System;
using SuperMap.Web.iServerJava6R.Resources;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetFeaturesBySQLService_Title}</para>
    /// 	<para>${iServerJava6R_GetFeaturesBySQLService_Description}</para>
    /// </summary>
    public class GetFeaturesBySQLService : ServiceBase
    {
        /// <summary>${iServerJava6R_GetFeaturesBySQLService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_GetFeaturesBySQLService_constructor_overloads_D}</overloads>
        public GetFeaturesBySQLService()
        {
        }
        /// <summary>${iServerJava6R_GetFeaturesBySQLService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_GetFeaturesBySQLService_constructor_param_url}</param>
        public GetFeaturesBySQLService(string url)
            : base(url)
        {
        }
        /// <summary>${iServerJava6R_GetFeaturesBySQLService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_GetFeaturesBySQLService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(GetFeaturesBySQLParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${iServerJava6R_GetFeaturesBySQLService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_GetFeaturesBySQLService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${iServerJava6R_GetFeaturesBySQLService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GetFeaturesBySQLParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            //base.Url += ".json?returnContent=true";
            base.Url += string.Format(System.Globalization.CultureInfo.InvariantCulture, ".json?returnContent=true&debug=true&fromIndex={0}&toIndex={1}", parameters.FromIndex, parameters.ToIndex);

            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true);
        }

        private Dictionary<string, string> GetParameters(GetFeaturesBySQLParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("getFeatureMode", "\"SQL\"");

            if (parameters.DatasetNames != null && parameters.DatasetNames.Count > 0)
            {
                string jsonDatasetNames = "[";
                List<string> list = new List<string>();
                for (int i = 0; i < parameters.DatasetNames.Count; i++)
                {
                    list.Add(string.Format("\"{0}\"", parameters.DatasetNames[i]));
                }
                jsonDatasetNames += string.Join(",", list.ToArray());
                jsonDatasetNames += "]";

                dictionary.Add("datasetNames", jsonDatasetNames);
            }
            if (parameters.MaxFeatures > 0)
            {
                dictionary.Add("maxFeatures", parameters.MaxFeatures.ToString(System.Globalization.CultureInfo.InvariantCulture));
            }
            dictionary.Add("queryParameter", FilterParameter.ToJson(parameters.FilterParameter));

            return dictionary;
        }
        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            GetFeaturesResult result = GetFeaturesResult.FromJson(jsonObject);
            LastResult = result;
            GetFeaturesEventArgs args = new GetFeaturesEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(GetFeaturesEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_GetFeaturesBySQLService_event_ProcessCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetFeaturesEventArgs> ProcessCompleted;


        private GetFeaturesResult lastResult;
        /// <summary>${iServerJava6R_GetFeaturesBySQLService_attribute_LastResult_D}</summary>
        public GetFeaturesResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
