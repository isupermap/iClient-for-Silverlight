﻿

using System.Collections.Generic;
using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetFeaturesByIDsParameters_Title}</para>
    /// 	<para>${iServerJava6R_GetFeaturesByIDsParameters_Description}</para>
    /// </summary>
    public class GetFeaturesByIDsParameters:GetFeaturesParametersBase
    {
        /// <summary>${iServerJava6R_GetFeaturesByGeometryParameters_constructor_D}</summary>
        public GetFeaturesByIDsParameters()
        {
        }
        /// <summary>${iServerJava6R_GetFeaturesByGeometryParameters_attribute_IDs_D}</summary>
        public IList<int> IDs { get; set; }
        /// <summary>${iServerJava6R_GetFeaturesByBufferParameters_attribute_Fields_D}</summary>
        public IList<string> Fields { get; set; }
    }
}
