﻿

using SuperMap.Web.Service;
using System;
using SuperMap.Web.iServerJava6R.Resources;
using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;
using System.Windows.Browser;
namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetFeaturesByGeometryService_Title}</para>
    /// 	<para>${iServerJava6R_GetFeaturesByGeometryService_Description}</para>
    /// </summary>
    public class GetFeaturesByGeometryService : ServiceBase
    {
        /// <summary>${iServerJava6R_GetFeaturesByGeometryService_constructor_D}</summary>
        /// <overloads>${iServerJava6R_GetFeaturesByBufferService_constructor_overloads_D}</overloads>
        public GetFeaturesByGeometryService()
        {

        }
        /// <summary>${iServerJava6R_GetFeaturesByBufferService_constructor_String_D}</summary>
        /// <param name="url">${iServerJava6R_GetFeaturesByGeometryService_constructor_param_url}</param>
        public GetFeaturesByGeometryService(string url)
            : base(url)
        {

        }

        /// <summary>${iServerJava6R_GetFeaturesByGeometryService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServerJava6R_GetFeaturesByGeometryService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(GetFeaturesByGeometryParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${iServerJava6R_GetFeaturesByGeometryService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServerJava6R_GetFeaturesByGeometryService_method_ProcessAsync_param_Parameters}</param>
        /// <param name="state">${iServerJava6R_GetFeaturesByGeometryService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GetFeaturesByGeometryParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            //base.Url += ".json?returnContent=true";           
            base.Url += string.Format(System.Globalization.CultureInfo.InvariantCulture, ".json?returnContent=true&debug=true&fromIndex={0}&toIndex={1}", parameters.FromIndex, parameters.ToIndex);

            base.SubmitRequest(base.Url, GetParameters(parameters), new EventHandler<RequestEventArgs>(request_Completed), state, true);
        }

        private Dictionary<string, string> GetParameters(GetFeaturesByGeometryParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(parameters.AttributeFilter))
            {
                dictionary.Add("getFeatureMode", "\"SPATIAL\"");
            }
            else
            {
                dictionary.Add("getFeatureMode", "\"SPATIAL_ATTRIBUTEFILTER\"");
                dictionary.Add("attributeFilter", parameters.AttributeFilter);
            }
            if (parameters.DatasetNames != null && parameters.DatasetNames.Count > 0)
            {
                string jsonDatasetNames = "[";
                List<string> list = new List<string>();
                for (int i = 0; i < parameters.DatasetNames.Count; i++)
                {
                    list.Add(string.Format("\"{0}\"", parameters.DatasetNames[i]));
                }
                jsonDatasetNames += string.Join(",", list.ToArray());
                jsonDatasetNames += "]";

                dictionary.Add("datasetNames", jsonDatasetNames);
            }
            else
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            dictionary.Add("spatialQueryMode", parameters.SpatialQueryMode.ToString().ToUpper());

            if (parameters.Geometry != null)
            {
                dictionary.Add("geometry", ServerGeometry.ToJson(Bridge.ToServerGeometry(parameters.Geometry)));
            }
            else
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }

            if (parameters.Fields != null && parameters.Fields.Count > 0)
            {
                FilterParameter fp = new FilterParameter();
                fp.Fields = parameters.Fields;
                dictionary.Add("queryParameter", FilterParameter.ToJson(fp));
            }
            return dictionary;
        }
        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            GetFeaturesResult result = GetFeaturesResult.FromJson(jsonObject);
            LastResult = result;
            GetFeaturesEventArgs args = new GetFeaturesEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(GetFeaturesEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                this.ProcessCompleted(this, args);
            }
        }

        /// <summary>${iServerJava6R_GetFeaturesByGeometryService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetFeaturesEventArgs> ProcessCompleted;


        private GetFeaturesResult lastResult;
        /// <summary>${iServerJava6R_GetFeaturesByGeometryService_attribute_LastResult_D}</summary>
        public GetFeaturesResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
