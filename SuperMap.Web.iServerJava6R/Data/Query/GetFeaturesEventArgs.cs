﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetFeaturesEventArgs_Title}</para>
    /// 	<para>${iServerJava6R_GetFeaturesEventArgs_Description}</para>
    /// </summary>
    public class GetFeaturesEventArgs:ServiceEventArgs
    {
        /// <summary>${iServerJava6R_GetFeaturesEventArgs_constructor_D}</summary>
        public GetFeaturesEventArgs(GetFeaturesResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServerJava6R_GetFeaturesEventArgs_attribute_Result_D}</summary>
        public GetFeaturesResult Result { get; private set; }
        /// <summary>${iServerJava6R_GetFeaturesEventArgs_attribute_OriginResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
