﻿

using SuperMap.Web.Core;
using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava6R.Data
{
    /// <summary>
    /// 	<para>${iServerJava6R_GetFeaturesResult_Title}</para>
    /// 	<para>${iServerJava6R_GetFeaturesResult_Description}</para>
    /// </summary>
    public class GetFeaturesResult
    {
        internal GetFeaturesResult()
        {
        }
        /// <summary>${iServerJava6R_GetFeaturesResult_attribute_FeatureCount_D}</summary>
        public int FeatureCount { get; private set; }
        /// <summary>${iServerJava6R_GetFeaturesResult_attribute_Features_D}</summary>
        public FeatureCollection Features { get; private set; }

        /// <summary>${iServerJava6R_GetFeaturesResult_method_fromJson_D}</summary>
        /// <returns>${iServerJava6R_GetFeaturesResult_method_fromJson_return}</returns>
        /// <param name="json">${iServerJava6R_GetFeaturesResult_method_fromJson_param_jsonObject}</param>
        public static GetFeaturesResult FromJson(JsonObject json)
        {
            if (json == null)
                return null;

            GetFeaturesResult result = new GetFeaturesResult();
            result.FeatureCount = (int)json["featureCount"];
            if (result.FeatureCount < 1)
            {
                return null;
            }

            JsonArray features = (JsonArray)json["features"];
            if (features != null && features.Count > 0)
            {
                result.Features = new FeatureCollection();

                for (int i = 0; i < features.Count; i++)
                {
                    ServerFeature f = ServerFeature.FromJson((JsonObject)features[i]);
                    result.Features.Add(f.ToFeature());
                }
            }

            return result;
        }
    }
}
