﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SetLayerStatusEventArgs_Title}</para>
    /// 	<para>${iServer2_SetLayerStatusEventArgs_Description}</para>
    /// </summary>
    public class SetLayerStatusEventArgs:ServiceEventArgs
    {
        /// <summary>${iServer2_SetLayerStatusEventArgs_constructor_D}</summary>
        public SetLayerStatusEventArgs(SetLayerStatusResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServer2_SetLayerStatusEventArgs_attribute_result}</summary>
        public SetLayerStatusResult Result { get; private set; }
        /// <summary>${iServer2_SetLayerStatusEventArgs_attribute_originResult}</summary>
        public string OriginResult { get; private set; }
    }
}
