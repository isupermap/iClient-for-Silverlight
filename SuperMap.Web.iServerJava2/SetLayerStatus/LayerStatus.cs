﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_LayerStatus_Title}</para>
    /// 	<para>${iServer2_LayerStatus_Description}</para>
    /// </summary>
    public class LayerStatus
    {
        /// <summary>${iServer2_LayerStatus_constructor_None_D}</summary>
        public LayerStatus()
        {
            IsVisible = true;
        }

        /// <summary>${iServer2_LayerStatus_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${iServer2_LayerStatus_attribute_isVisible_D}</summary>
        public bool IsVisible { get; set; }
        //public bool IsQueryable { get; set; }
    }
}
