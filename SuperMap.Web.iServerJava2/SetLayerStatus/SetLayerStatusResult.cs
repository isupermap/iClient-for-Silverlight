﻿
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SetLayerStatusResult_Title}</para>
    /// 	<para>${iServer2_SetLayerStatusResult_Description}</para>
    /// </summary>
    public class SetLayerStatusResult
    {
        internal SetLayerStatusResult()
        { }

        /// <summary>${iServer2_SetLayerStatusResult_attribute_layersKey_D}</summary>
        public string LayersKey { get; private set; }
        /// <summary>${iServer2_SetLayerStatusResult_method_FromJson_D}</summary>
        /// <returns>${iServer2_SetLayerStatusResult_method_FromJson_return}</returns>
        /// <param name="jsonPrimitive">${iServer2_SetLayerStatusResult_method_FromJson_param_jsonObject}</param>
        public static SetLayerStatusResult FromJson(JsonPrimitive jsonPrimitive)
        {
            if (jsonPrimitive == null)
            {
                return null;
            }
            return new SetLayerStatusResult { LayersKey = jsonPrimitive };
        }
    }
}
