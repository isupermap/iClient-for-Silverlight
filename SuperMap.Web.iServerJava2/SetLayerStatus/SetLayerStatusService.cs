﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SetLayerStatusService_Title}</para>
    /// 	<para>${iServer2_SetLayerStatusService_Description}</para>
    /// </summary>
    public class SetLayerStatusService : ServiceBase
    {
        /// <overloads>${iServer2_SetLayerStatusService_constructor_overloads}</overloads>
        /// <summary>${iServer2_SetLayerStatusService_constructor_None_D}</summary>
        public SetLayerStatusService()
        { }
        /// <summary>${iServer2_SetLayerStatusService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_SetLayerStatusService_constructor_String_param_url}</param>
        public SetLayerStatusService(string url)
            : base(url)
        { }

        /// <overloads>${iServer2_SetLayerStatusService_method_processAsync_overloads}</overloads>
        /// <summary>${iServer2_SetLayerStatusService_method_processAsync_D}</summary>
        /// <param name="parameters">${iServer2_SetLayerStatusService_method_processAsync_param_parameters}</param>
        public void ProcessAsync(SetLayerStatusParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${iServer2_SetLayerStatusService_method_processAsync_D}</summary>
        /// <param name="parameters">${iServer2_SetLayerStatusService_method_processAsync_param_parameters}</param>
        /// <param name="state">${iServer2_SetLayerStatusService_method_processAsync_param_state}</param>
        public void ProcessAsync(SetLayerStatusParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(SetLayerStatusParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "setLayerStatus";

            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            string layerNames, visibleArgs, queryableArgs;
            SetLayerStatusParameters.ToJson(parameters.LayerStatusList,out layerNames,out visibleArgs,out queryableArgs);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);
            dict.Add("layerNames", layerNames);
            dict.Add("visibleArgs", visibleArgs);
            dict.Add("queryableArgs", queryableArgs);
            dictionary.Add("params", Bridge.CreateParams(method, dict));

            return dictionary;
        }

        private SetLayerStatusResult lastResult;

        /// <summary>${iServer2_SetLayerStatusService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<SetLayerStatusEventArgs> ProcessCompleted;
        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonPrimitive jsonPrimitive = (JsonPrimitive)JsonObject.Parse(e.Result);
            SetLayerStatusResult result = SetLayerStatusResult.FromJson(jsonPrimitive);

            LastResult = result;
            SetLayerStatusEventArgs args = new SetLayerStatusEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(SetLayerStatusEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${iServer2_SetLayerStatusService_attribute_lastResult_D}</summary>
        public SetLayerStatusResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}