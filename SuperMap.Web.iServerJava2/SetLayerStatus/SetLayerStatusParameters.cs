﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SetLayerStatusParameter_Title}</para>
    /// 	<para>${iServer2_SetLayerStatusParameter_Description}</para>
    /// </summary>
    public class SetLayerStatusParameters : ParametersBase
    {
        /// <summary>${iServer2_SetLayerStatusParameter_attribute_layerStatusList_D}</summary>
        public IList<LayerStatus> LayerStatusList { get; set; }

        internal static void ToJson(IList<LayerStatus> layerStatusList, out string layerNames, out string visibleArgs, out string queryableArgs)
        {
            layerNames = visibleArgs = queryableArgs ="[";
            List<string> layerNameList = new List<string>();
            List<string> visibleArgsList = new List<string>();
            List<string> queryableArgsList = new List<string>();
            foreach (LayerStatus item in layerStatusList)
            {
                string temp = "\"" + item.LayerName + "\"";
                layerNameList.Add(temp);
                visibleArgsList.Add(item.IsVisible.ToString());
                queryableArgsList.Add("true");
            }
            layerNames += string.Join(",", layerNameList.ToArray());
            layerNames +="]";
            visibleArgs += string.Join(",", visibleArgsList.ToArray());
            visibleArgs += "]";
            queryableArgs += string.Join(",", queryableArgsList.ToArray());
            queryableArgs += "]";
        }
    }

}
