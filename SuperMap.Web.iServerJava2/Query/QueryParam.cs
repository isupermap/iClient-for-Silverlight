﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Text;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// ${iServer2_queryParam_Title}<br/>
    /// ${iServer2_queryParam_Description}<br/>
    /// </summary>
    public class QueryParam
    {
        /// <summary>${iServer2_queryParam_constructor_None_D}</summary>
        public QueryParam()
        {
            QueryLayerParams = new List<QueryLayerParam>();
            ExpectCount = 100000;
            ReturnResultSetInfo = ReturnResultSetInfo.AttributeAndGeometry;
        }

        //public string CustomParams { get; set; }
        /// <summary>${iServer2_queryParam_attribute_expectCount_D}</summary>
        public int ExpectCount { get; set; }
        /// <summary>${iServer2_queryParam_attribute_networkType_D}</summary>
        public ServerFeatureType NetworkType { get; set; }
        /// <summary>${iServer2_queryParam_attribute_queryLayerParams_D}</summary>
        public IList<QueryLayerParam> QueryLayerParams { get; set; }
        /// <summary>${iServer2_queryParam_attribute_returnResultSetInfo_D}</summary>
        public ReturnResultSetInfo ReturnResultSetInfo { get; set; }
        /// <summary>${iServer2_queryParam_attribute_startCount_D}</summary>
        public int StartRecord { get; set; }

        internal static string ToJson(QueryParam queryParam)
        {
            if (queryParam == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();
            //if (!string.IsNullOrEmpty(this.CustomParams))
            //{
            //    list.Add(string.Format("\"customParams\":\"{0}\"", this.CustomParams));
            //}
            list.Add(string.Format("\"expectCount\":{0}", queryParam.ExpectCount));
            list.Add(string.Format("\"startRecord\":{0}", queryParam.StartRecord));
            list.Add(string.Format("\"returnResultSetInfo\":{0}", (int)queryParam.ReturnResultSetInfo));
            list.Add(string.Format("\"networkType\":{0}", (int)queryParam.NetworkType));

            IList<QueryLayerParam> queryLayerParams = queryParam.QueryLayerParams;
            if (queryLayerParams != null && queryLayerParams.Count > 0)
            {
                List<string> layerParams = new List<string>();

                for (int i = 0; i < queryLayerParams.Count; i++)
                {
                    layerParams.Add(QueryLayerParam.ToJson(queryLayerParams[i]));
                }
                string temp = "[" + string.Join(",", layerParams.ToArray()) + "]";
                list.Add(string.Format("\"queryLayerParams\":{0}", temp));
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

    }
}
