﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Query_QueryBySQLService_Title}</para>
    /// 	<para>${iServer2_Query_QueryBySQLService_Description}</para>
    /// </summary>
    public class QueryBySqlService : ServiceBase
    {
        private ResultSet lastResult;

        /// <summary>${iServer2_Query_QueryBySQLService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<QueryEventArgs> ProcessCompleted;
        /// <overloads>${iServer2_Query_QueryBySQLService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_Query_QueryBySQLService_constructor_None_D}</summary>
        public QueryBySqlService()
        { }

        /// <summary>${iServer2_Query_QueryBySQLService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_Query_QueryBySQLService_constructor_String_param_url}</param>
        public QueryBySqlService(string url)
            : base(url)
        {
        }

        /// <overloads>${iServer2_Query_QueryBySQLService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_Query_QueryBySQLService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Query_QueryBySQLService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(QueryBySqlParameters parameters)
        {
            this.ProcessAsync(parameters, null);
        }

        /// <summary>${iServer2_Query_QueryBySQLService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Query_QueryBySQLService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_Query_QueryBySQLService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(QueryBySqlParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("QueryBySqlParameters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(QueryBySqlParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "QueryBySql";

            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);
            dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));

            dictionary.Add("params", Bridge.CreateParams(method, dict));
            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            ResultSet result = ResultSet.FromJson(jsonObject);
            LastResult = result;
            QueryEventArgs args = new QueryEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(QueryEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(
                    ProcessCompleted, new object[] { this, args });
            }
        }

        /// <summary>${iServer2_Query_QueryBySQLService_attribute_lastResult_D}</summary>
        public ResultSet LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }


    }
}
