﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Core;
using System.Collections.Generic;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// ${iServer2_queryByCenterParameters_Title}<br/>
    /// ${iServer2_queryByCenterParameters_Description}
    /// </summary>
    public class QueryByCenterParameters : QueryParametersBase
    {
        /// <summary>
        ///     ${pubilc_Constructors_Initializes} <see cref="QueryByCenterParameters">QueryByCenterParameters</see>
        ///     ${pubilc_Constructors_instance}
        /// </summary>
        public QueryByCenterParameters() 
        {
            CenterPoint = Point2D.Empty;
            Tolerance = 50;
        }
        /// <summary>${iServer2_queryByCenterParameters_attribute_centerPoint_D}</summary>
        public Point2D CenterPoint { get; set; }
        /// <summary>${iServer2_queryByCenterParameters_attribute_tolerance_D}</summary>
        public double Tolerance { get; set; }
        /// <summary>${iServer2_queryByCenterParameters_attribute_isNearest_D}</summary>
        public bool IsNearest { get; set; }
    }
}
