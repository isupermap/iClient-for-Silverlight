﻿using System.Collections.Generic;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_sqlParam_Title}</para>
    /// 	<para>${iServer2_sqlParam_Description}</para>
    /// </summary>
    public class SqlParam
    {
        /// <summary>${iServer2_sqlParam_constructor_None_D}</summary>
        public SqlParam()
        {
        }

        /// <summary>${iServer2_sqlParam_attribute_ids_D}</summary>
        public IList<int> IDs { get; set; }
        /// <summary>${iServer2_sqlParam_attribute_returnFields_D}</summary>
        public IList<string> ReturnFields { get; set; }
        /// <summary>${iServer2_sqlParam_attribute_sortClause_D}</summary>
        public string SortClause { get; set; }
        /// <summary>${iServer2_sqlParam_attribute_whereClause_D}</summary>
        public string WhereClause { get; set; }
        /// <summary>${iServer2_sqlParam_attribute_groupClause_D}</summary>
        public string GroupClause { get; set; }

        internal static string ToJson(SqlParam sqlParam)
        {
            if (sqlParam == null)
            {
                return null;
            }

            //if (mapName.Contains("@"))
            //{
            //    string str = mapName.Substring(0, mapName.IndexOf("@"));
            //}
            string json = "{";
            List<string> list = new List<string>();
            if (!string.IsNullOrEmpty(sqlParam.WhereClause))
            {
                list.Add(string.Format("\"whereClause\":\"{0}\"", Bridge.EncodeString(sqlParam.WhereClause)));

                //if (!sqlParam.WhereClause.Contains(".") && mapName.Contains("@"))
                //{
                //    string str = mapName.Substring(0, mapName.IndexOf("@"));
                //    //list.Add(string.Format("\"whereClause\":\"{0}.{1}\"", str, Bridge.EncodeString(sqlParam.WhereClause)));
                //    list.Add(string.Format("\"whereClause\":\"{0}\"", Bridge.EncodeString(sqlParam.WhereClause)));
                //}
                //else
                //{
                //    list.Add(string.Format("\"whereClause\":\"{0}\"", Bridge.EncodeString(sqlParam.WhereClause)));
                //}
            }
            if (!string.IsNullOrEmpty(sqlParam.SortClause))
            {
                list.Add(string.Format("\"sortClause\":\"{0}\"", Bridge.EncodeString(sqlParam.SortClause)));

                //if (!sqlParam.SortClause.Contains(".") && mapName.Contains("@"))
                //{
                //    string str = mapName.Substring(0, mapName.IndexOf("@"));
                //    list.Add(string.Format("\"sortClause\":\"{0}.{1}\"", str, Bridge.EncodeString(sqlParam.SortClause)));
                //}
                //else
                //{
                //    list.Add(string.Format("\"sortClause\":\"{0}\"", Bridge.EncodeString(sqlParam.SortClause)));
                //}
            }
            if (!string.IsNullOrEmpty(sqlParam.GroupClause))
            {
                list.Add(string.Format("\"groupClause\":\"{0}\"", Bridge.EncodeString(sqlParam.GroupClause)));

                //if (!sqlParam.GroupClause.Contains(".") && mapName.Contains("@"))
                //{
                //    string str = mapName.Substring(0, mapName.IndexOf("@"));
                //    list.Add(string.Format("\"groupClause\":\"{0}.{1}\"", str, Bridge.EncodeString(sqlParam.GroupClause)));
                //}
                //else
                //{
                //    list.Add(string.Format("\"groupClause\":\"{0}\"", Bridge.EncodeString(sqlParam.GroupClause)));
                //}
            }

            //
            if (sqlParam.ReturnFields != null && sqlParam.ReturnFields.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < sqlParam.ReturnFields.Count; i++)
                {
                    temp.Add(string.Format("\"{0}\"", sqlParam.ReturnFields[i]));
                }
                list.Add(string.Format("\"returnFields\":[{0}]", string.Join(",", temp.ToArray())));
            }

            if (sqlParam.IDs != null && sqlParam.IDs.Count > 0)
            {
                List<string> temp = new List<string>();
                foreach (int id in sqlParam.IDs)
                {
                    temp.Add(id.ToString());
                }
                list.Add(string.Format("\"ids\":[{0}]", string.Join(",", temp.ToArray())));
            }

            json += string.Join(",", list.ToArray());
            json += "}";


            return json;
        }
    }
}
