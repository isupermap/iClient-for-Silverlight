﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_QueryLayerParam_Title}</para>
    /// 	<para>${iServer2_QueryLayerParam_Description}</para>
    /// </summary>
    /// <remarks>${iServer2_QueryLayerParam_Remarks}</remarks>
    public class QueryLayerParam
    {
        /// <summary>
        ///     ${pubilc_Constructors_Initializes} <see cref="QueryLayerParam">QueryLayerParam</see> ${pubilc_Constructors_instance}
        /// </summary>
        public QueryLayerParam()
        {
        }

        /// <summary>${iServer2_QueryLayerParam_attribute_name_D}</summary>
        public string Name { get; set; }
        /// <summary>${iServer2_QueryLayerParam_attribute_sqlParam_D}</summary>
        public SqlParam SqlParam { get; set; }

        internal static string ToJson(QueryLayerParam queryLayerParam)
        {
            if (queryLayerParam == null)
            {
                return null;
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("{");
            if (!string.IsNullOrEmpty(queryLayerParam.Name))
            {
                builder.AppendFormat("\"name\":\"{0}\"", queryLayerParam.Name);
            }
            if (queryLayerParam.SqlParam != null)
            {
                //string json = SqlParam.ToJson(queryLayerParam.SqlParam, queryLayerParam.Name);
                string json = SqlParam.ToJson(queryLayerParam.SqlParam);
                if (!string.IsNullOrEmpty(json) && json.Length > 2)
                {
                    builder.Append(",");
                    builder.AppendFormat("\"sqlParam\":{0}", json);
                }
            }
            builder.Append("}");

            return builder.ToString();
        }
    }
}
