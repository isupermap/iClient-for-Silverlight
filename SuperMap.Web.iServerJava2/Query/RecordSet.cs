﻿using System.Collections.Generic;
using System.Json;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Query_Recordset_Title}</para>
    /// 	<para>${iServer2_Query_Recordset_Description}</para>
    /// </summary>
    /// <remarks>${iServer2_Query_Record_Remarks}</remarks>
    public class RecordSet
    {
        private RecordSet()
        {
        }

        /// <summary>${iServer2_Query_Recordset_attribute_layerName_D}</summary>
        public string LayerName { get; private set; }
        /// <summary>${iServer2_Query_Recordset_attribute_records_D}</summary>
        public List<Record> Records { get; private set; }
        /// <summary>${iServer2_Query_Recordset_attribute_returnFieldCaptions_D}</summary>
        public List<string> ReturnFieldCaptions { get; private set; }
        /// <summary>${iServer2_Query_Recordset_attribute_returnFields_D}</summary>
        public List<string> ReturnFields { get; private set; }
        /// <summary>${iServer2_Query_Recordset_attribute_returnFieldTypes_D}</summary>
        public List<ServerFieldType> ReturnFieldTypes { get; private set; }
        /// <summary>${iServer2_Query_method_FromJson_D}</summary>
        /// <returns>${iServer2_Query_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_Query_method_FromJson_param_jsonObject}</param>
        public static RecordSet FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            RecordSet recordSet = new RecordSet();
            recordSet.LayerName = (string)jsonObject["layerName"];

            recordSet.ReturnFieldCaptions = JsonHelper.ToStringList((JsonArray)jsonObject["returnFieldCaptions"]);
            recordSet.ReturnFields = JsonHelper.ToStringList((JsonArray)jsonObject["returnFields"]);

            JsonArray fieldTypes = (JsonArray)jsonObject["returnFieldTypes"];
            if (fieldTypes != null && fieldTypes.Count > 0)
            {
                recordSet.ReturnFieldTypes = new List<ServerFieldType>();
                for (int i = 0; i < fieldTypes.Count; i++)
                {
                    if (fieldTypes[i] != null)
                    {
                        ServerFieldType type = (ServerFieldType)((int)fieldTypes[i]);
                        recordSet.ReturnFieldTypes.Add(type);
                    }
                }
            }

            JsonArray records = (JsonArray)jsonObject["records"];
            if (records != null && records.Count > 0)
            {
                recordSet.Records = new List<Record>();
                for (int i = 0; i < records.Count; i++)
                {
                    recordSet.Records.Add(Record.FromJson((JsonObject)records[i]));
                }
            }

            return recordSet;
        }

        /// <summary>${iServer2_Query_Recordset_method_toFeatureSet_D}</summary>
        public FeatureCollection ToFeatureSet()
        {
            FeatureCollection featureSet = new FeatureCollection();
            if (this.Records == null)
            {
                return featureSet;
            }
            foreach (Record record in this.Records)
            {
                Feature feature = new Feature();

                ServerGeometry shape = record.Shape;
                if (shape != null)
                {
                    switch (shape.Feature)
                    {
                        case ServerFeatureType.Unknown:
                            break;
                        case ServerFeatureType.Point:
                            feature.Geometry = shape.ToGeoPoint();
                            break;
                        case ServerFeatureType.Line:
                            feature.Geometry = shape.ToGeoLine();
                            break;
                        case ServerFeatureType.Polygon:
                            feature.Geometry = shape.ToGeoRegion();
                            break;
                        case ServerFeatureType.Text:
                            break;
                        case ServerFeatureType.LineM:
                            break;
                        default:
                            feature.Geometry = null;
                            break;
                    }
                }
                //List<string> values = record.FieldValues;
                //List<string> key = this.ReturnFields; //没有用Caption

                if (record.FieldValues != null)
                {
                    for (int i = 0; i < this.ReturnFields.Count; i++)
                    {
                        feature.Attributes.Add(this.ReturnFields[i], record.FieldValues[i]);
                    }
                }
                featureSet.Add(feature);
            }
            return featureSet;
        }
    }
}
