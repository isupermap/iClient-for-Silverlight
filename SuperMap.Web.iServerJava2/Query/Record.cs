﻿using System.Collections.Generic;
using SuperMap.Web.Core;
using System.Json;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Query_Record_Title}</para>
    /// 	<para>${iServer2_Query_Record_Description}</para>
    /// </summary>
    /// <remarks>${iServer2_Query_Record_Remarks}</remarks>
    public class Record
    {
        private Record()
        {
        }

        /// <summary>${iServer2_Query_Record_attribute_center_D}</summary>
        public Point2D Center { get; private set; }
        /// <summary>${iServer2_Query_Record_attribute_fieldValues_D}</summary>
        public List<string> FieldValues { get; private set; }
        /// <summary>${iServer2_Query_Record_attribute_shape_D}</summary>
        public ServerGeometry Shape { get; private set; }

        /// <summary>${iServer2_Record_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_Record_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_Record_method_FromJson_return}</returns>
        public static Record FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            Record record = new Record();
            record.Center = JsonHelper.ToPoint2D((JsonObject)jsonObject["center"]);
            record.Shape = ServerGeometry.FromJson((JsonObject)jsonObject["shape"]);
            record.FieldValues = JsonHelper.ToStringList((JsonArray)jsonObject["fieldValues"]);
            return record;
        }

    }

}
