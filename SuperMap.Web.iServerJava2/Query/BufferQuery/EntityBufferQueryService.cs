﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.Core;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_EntityBufferQueryService_Title}</para>
    /// 	<para>${iServer2_EntityBufferQueryService_Description}</para>
    /// </summary>
    public class EntityBufferQueryService : ServiceBase
    {
        private BufferResultSet lastResult;

        /// <summary>${iServer2_EntityBufferQueryService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<BufferQueryEventArgs> ProcessCompleted;

        /// <overloads>${iServer2_EntityBufferQueryService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_EntityBufferQueryService_constructor_None_D}</summary>
        public EntityBufferQueryService()
        {
        }
        /// <summary>${iServer2_EntityBufferQueryService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_EntityBufferQueryService_constructor_String_param_url}</param>
        public EntityBufferQueryService(string url)
            : base(url)
        { }
        /// <overloads>${iServer2_EntityBufferQueryService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_EntityBufferQueryService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_EntityBufferQueryService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(EntityBufferQueryParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_EntityBufferQueryService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_EntityBufferQueryService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_EntityBufferQueryService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(EntityBufferQueryParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                // throw new ArgumentNullException("Paramters is Null");
                throw new ArgumentNullException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                // throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(EntityBufferQueryParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            Point2DCollection gps = new Point2DCollection();

            if (parameters.Geometry is GeoPoint)
            {
                string method = "PointEntityBufferQuery";
                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                dict.Add("point", JsonHelper.FromGeoPoint(parameters.Geometry as GeoPoint));
                dict.Add("fromLayer", parameters.FromLayer);
                dict.Add("bufferParam", BufferAnalystParam.ToJson(parameters.BufferParam));
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
                dict.Add("queryMode", ((int)parameters.QueryMode).ToString());

                dictionary.Add("params", Bridge.CreateParams(method, dict));
            }
            else if (parameters.Geometry is GeoLine)
            {
                string method = "LineEntityBufferQuery";
                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                foreach (Point2DCollection g in (parameters.Geometry as GeoLine).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
                dict.Add("points", JsonHelper.FromPoint2DCollection(gps));
                dict.Add("fromLayer", parameters.FromLayer);
                dict.Add("bufferParam", BufferAnalystParam.ToJson(parameters.BufferParam));
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
                dict.Add("queryMode", ((int)parameters.QueryMode).ToString());

                dictionary.Add("params", Bridge.CreateParams(method, dict));
            }
            else if (parameters.Geometry is GeoRegion)
            {
                string method = "PolygonEntityBufferQuery";
                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                foreach (Point2DCollection g in (parameters.Geometry as GeoRegion).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
                dict.Add("points", JsonHelper.FromPoint2DCollection(gps));
                dict.Add("fromLayer", parameters.FromLayer);
                dict.Add("bufferParam", BufferAnalystParam.ToJson(parameters.BufferParam));
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
                dict.Add("queryMode", ((int)parameters.QueryMode).ToString());

                dictionary.Add("params", Bridge.CreateParams(method, dict));

            }

            return dictionary;
        }
        private void OnProcessCompleted(BufferQueryEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(
                    ProcessCompleted, new object[] { this, args });
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            BufferResultSet result = BufferResultSet.FromJson(jsonObject);
            LastResult = result;
            BufferQueryEventArgs args = new BufferQueryEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        /// <summary>${iServer2_EntityBufferQueryService_attribute_lastResult_D}</summary>
        public BufferResultSet LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
