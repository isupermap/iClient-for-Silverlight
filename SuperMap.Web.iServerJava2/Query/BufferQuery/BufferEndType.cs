﻿//标题：线缓冲区端点类型。 
//描述：线缓冲区分析的端点类型，目前支持圆头缓冲。
//注：圆头缓冲只支持左右相等的缓冲和单边的缓冲，不支持左右不等的缓冲。

//详情请看iServer2.0的帮助文档；
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_BufferEndType_Title}</para>
    /// 	<para>${iServer2_BufferEndType_Description}</para>
    /// </summary>
    public enum BufferEndType
    {
        /// <summary>
        /// 	<para>${iServer2_BufferEndType_attribute_ROUND_D}</para>
        /// 	<para><img src="roundBuffer.bmp"/></para>
        /// </summary>
        Round=1
    }
}
