﻿//标题：空间几何对象间的查询模式。 
//描述：定义空间查询操作模式常量。
//空间查询是通过几何对象之间的空间位置关系来构建过滤条件的一种查询方式。
//例如：通过空间查询可以找到被包含在面中的空间对象，相离或者相邻的空间对象等。
//注意：当前版本提供对点、线、面、网络和文本类型数据的空间查询，其中文本类型
//仅支持 Intersect 和 Contain 两种空间查询模式，而且只能作为被搜索对象不能作为
//搜索对象。 

//详情请看iServer2.0的帮助文档；

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SpatialQueryMode_Title}</para>
    /// 	<para>${iServer2_SpatialQueryMode_Description}</para>
    /// 	<para>
    /// 		<table class="Tablehead" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 95%; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: #efeff7" cellspacing="1" cols="5" cellpadding="1" align="center">
    /// 			<tbody>
    /// 				<tr>
    /// 					<td>${iServer2_SpatialQueryMode_R1C1}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R1C2}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R1C3}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R1C4}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R1C5}</td>
    /// 				</tr>
    /// 				<tr>
    /// 					<td>${iServer2_SpatialQueryMode_R2C1}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R2C2}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R2C2}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R2C4}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R2C5}</td>
    /// 				</tr>
    /// 				<tr>
    /// 					<td>${iServer2_SpatialQueryMode_R3C1}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R3C2}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R3C3}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R3C4}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R3C5}</td>
    /// 				</tr>
    /// 				<tr>
    /// 					<td>${iServer2_SpatialQueryMode_R4C1}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R4C2}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R4C3}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R4C4}</td>
    /// 					<td>${iServer2_SpatialQueryMode_R4C5}</td>
    /// 				</tr>
    /// 			</tbody>
    /// 		</table>
    /// 	</para>
    /// </summary>
    public enum SpatialQueryMode
    {
        /// <summary>${iServer2_SpatialQueryMode_attribute_none_D}</summary>
        None = -1,
        /// <summary>
        /// 	<para>${iServer2_SpatialQueryMode_attribute_Identity_D}</para>
        /// 	<para>
        /// 	  <table class="Tablehead" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 95%; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: #efeff7" cellspacing="1" cols="4" cellpadding="1" align="center">
        /// 			<tbody>
        /// 				<tr>
        /// 					<td rowspan="2">${iServer2_SpatialQueryMode_R1C1}</td>
        /// 					<td colspan="3">${iServer2_SpatialQueryMode_R1C2}</td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SdP.png"/></td>
        /// 					<td><img src="SdL.png"/></td>
        /// 					<td><img src="SdR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SP.png"/></td>
        /// 					<td><img src="Identity_PP.png"/></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SL.png"/></td>
        /// 					<td></td>
        /// 					<td><img src="Identity_LL.png"/></td>
        /// 					<td></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SR.png"/></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 					<td><img src="Identity_RR.png"/></td>
        /// 				</tr>
        /// 			</tbody>
        /// 		</table>
        /// 	</para>
        /// </summary>
        Identity = 0,
        /// <summary>
        /// 	<para>${iServer2_SpatialQueryMode_attribute_Disjiont_D}</para>
        /// 	 <table class="Tablehead" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 95%; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: #efeff7" cellspacing="1" cols="4" cellpadding="1" align="center">
        /// 			<tbody>
        /// 				<tr>
        /// 					<td rowspan="2">${iServer2_SpatialQueryMode_R1C1}</td>
        /// 					<td colspan="3">${iServer2_SpatialQueryMode_R1C2}</td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SdP.png"/></td>
        /// 					<td><img src="SdL.png"/></td>
        /// 					<td><img src="SdR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SP.png"/></td>
        /// 					<td><img src="Disjoint_PP.png"/></td>
        /// 					<td><img src="Disjoint_PL.png"/></td>
        /// 					<td><img src="Disjoint_PR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SL.png"/></td>
        /// 					<td><img src="Disjoint_LP.png"/></td>
        /// 					<td><img src="Disjoint_LL.png"/></td>
        /// 					<td><img src="Disjoint_LR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SR.png"/></td>
        /// 					<td><img src="Disjoint_RP.png"/></td>
        /// 					<td><img src="Disjoint_RL.png"/></td>
        /// 					<td><img src="Disjoint_RR.png"/></td>
        /// 				</tr>
        /// 			</tbody>
        /// 		</table>
        /// </summary>
        Disjoint = 1,
        /// <summary>
        /// 	<para>${iServer2_SpatialQueryMode_attribute_Intersect_D}</para>
        /// 	<para>
        /// 	    <table class="Tablehead" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 95%; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: #efeff7" cellspacing="1" cols="4" cellpadding="1" align="center">
        /// 			<tbody>
        /// 				<tr>
        /// 					<td rowspan="2">${iServer2_SpatialQueryMode_R1C1}</td>
        /// 					<td colspan="3">${iServer2_SpatialQueryMode_R1C2}</td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SdP.png"/></td>
        /// 					<td><img src="SdL.png"/></td>
        /// 					<td><img src="SdR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SP.png"/></td>
        /// 					<td><img src="Intersect_PP.png"/></td>
        /// 					<td><img src="Intersect_PL.png"/></td>
        /// 					<td><img src="Intersect_PR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SL.png"/></td>
        /// 					<td><img src="Intersect_LP.png"/></td>
        /// 					<td><img src="Intersect_LL.png"/></td>
        /// 					<td><img src="Intersect_LR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SR.png"/></td>
        /// 					<td><img src="Intersect_RP.png"/></td>
        /// 					<td><img src="Intersect_RL.png"/></td>
        /// 					<td><img src="Intersect_RR.png"/></td>
        /// 				</tr>
        /// 			</tbody>
        /// 		</table>
        /// </para>
        /// </summary>
        Intersect = 2,
        /// <summary>
        /// 	<para>${iServer2_SpatialQueryMode_attribute_Touch_D}</para>
        /// 	<para>
        ///         <table class="Tablehead" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 95%; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: #efeff7" cellspacing="1" cols="4" cellpadding="1" align="center">
        /// 			<tbody>
        /// 				<tr>
        /// 					<td rowspan="2">${iServer2_SpatialQueryMode_R1C1}</td>
        /// 					<td colspan="3">${iServer2_SpatialQueryMode_R1C2}</td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SdP.png"/></td>
        /// 					<td><img src="SdL.png"/></td>
        /// 					<td><img src="SdR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SP.png"/></td>
        /// 					<td></td>
        /// 					<td><img src="Touch_PL.png"/></td>
        /// 					<td><img src="Touch_PR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SL.png"/></td>
        /// 					<td><img src="Touch_LP.png"/></td>
        /// 					<td><img src="Touch_LL.png"/></td>
        /// 					<td><img src="Touch_LR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SR.png"/></td>
        /// 					<td><img src="Touch_RP.png"/></td>
        /// 					<td><img src="Touch_RL.png"/></td>
        /// 					<td><img src="Touch_RR.png"/></td>
        /// 				</tr>
        /// 			</tbody>
        /// 		</table>	
        /// </para>
        /// </summary>
        Touch = 3,
        /// <summary>
        /// 	<para>${iServer2_SpatialQueryMode_attribute_Overlap_D}</para>
        /// 	<para>
        /// 	    <table class="Tablehead" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 95%; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: #efeff7" cellspacing="1" cols="4" cellpadding="1" align="center">
        /// 			<tbody>
        /// 				<tr>
        /// 					<td rowspan="2">${iServer2_SpatialQueryMode_R1C1}</td>
        /// 					<td colspan="3">${iServer2_SpatialQueryMode_R1C2}</td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SdP.png"/></td>
        /// 					<td><img src="SdL.png"/></td>
        /// 					<td><img src="SdR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SP.png"/></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SL.png"/></td>
        /// 					<td></td>
        /// 					<td><img src="Overlap_LL.png"/></td>
        /// 					<td></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SR.png"/></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 					<td><img src="Overlap_RR.png"/></td>
        /// 				</tr>
        /// 			</tbody>
        /// 		</table>
        /// </para>
        /// </summary>
        Overlap = 4,
        /// <summary>
        /// 	<para>${iServer2_SpatialQueryMode_attribute_Cross_D}</para>
        /// 	<para>
        ///     <table class="Tablehead" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 95%; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: #efeff7" cellspacing="1" cols="4" cellpadding="1" align="center">
        /// 			<tbody>
        /// 				<tr>
        /// 					<td rowspan="2">${iServer2_SpatialQueryMode_R1C1}</td>
        /// 					<td colspan="3">${iServer2_SpatialQueryMode_R1C2}</td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SdP.png"/></td>
        /// 					<td><img src="SdL.png"/></td>
        /// 					<td><img src="SdR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SP.png"/></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SL.png"/></td>
        /// 					<td></td>
        /// 					<td><img src="Cross_LL.png"/></td>
        /// 					<td><img src="Cross_LR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SR.png"/></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 				</tr>
        /// 			</tbody>
        /// 		</table>	
        /// </para>
        /// </summary>
        Cross = 5,
        /// <summary>
        /// 	<para>${iServer2_SpatialQueryMode_attribute_Within_D}</para>
        /// 	<para>
        ///         <table class="Tablehead" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 95%; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: #efeff7" cellspacing="1" cols="4" cellpadding="1" align="center">
        /// 			<tbody>
        /// 				<tr>
        /// 					<td rowspan="2">${iServer2_SpatialQueryMode_R1C1}</td>
        /// 					<td colspan="3">${iServer2_SpatialQueryMode_R1C2}</td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SdP.png"/></td>
        /// 					<td><img src="SdL.png"/></td>
        /// 					<td><img src="SdR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SP.png"/></td>
        /// 					<td><img src="Within_PP.png"/></td>
        /// 					<td><img src="Within_PL.png"/></td>
        /// 					<td><img src="Within_PR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SL.png"/></td>
        /// 					<td></td>
        /// 					<td><img src="Within_LL.png"/></td>
        /// 					<td><img src="Within_LR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SR.png"/></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 					<td><img src="Within_RR.png"/></td>
        /// 				</tr>
        /// 			</tbody>
        /// 		</table>	
        /// </para>
        /// </summary>
        Within = 6,
        /// <summary>
        /// 	<para>${iServer2_SpatialQueryMode_attribute_Contain_D}</para>
        /// 	<para>
        ///         <table class="Tablehead" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 95%; BORDER-BOTTOM: 1px solid; BACKGROUND-COLOR: #efeff7" cellspacing="1" cols="4" cellpadding="1" align="center">
        /// 			<tbody>
        /// 				<tr>
        /// 					<td rowspan="2">${iServer2_SpatialQueryMode_R1C1}</td>
        /// 					<td colspan="3">${iServer2_SpatialQueryMode_R1C2}</td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SdP.png"/></td>
        /// 					<td><img src="SdL.png"/></td>
        /// 					<td><img src="SdR.png"/></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SP.png"/></td>
        /// 					<td><img src="Contain_PP.png"/></td>
        /// 					<td></td>
        /// 					<td></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SL.png"/></td>
        /// 					<td><img src="Contain_LP.png"/></td>
        /// 					<td><img src="Contain_LL.png"/></td>
        /// 					<td></td>
        /// 				</tr>
        /// 				<tr>
        /// 					<td><img src="SR.png"/></td>
        /// 					<td><img src="Contain_RP.png"/></td>
        /// 					<td><img src="Contain_RL.png"/></td>
        /// 					<td><img src="Contain_RR.png"/></td>
        /// 				</tr>
        /// 			</tbody>
        /// 		</table>	
        /// </para>
        /// </summary>
        Contain = 7
    }
}
