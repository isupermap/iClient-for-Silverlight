﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_BufferQueryService_Title}</para>
    /// 	<para>${iServer2_BufferQueryService_Description}</para>
    /// </summary>
    public class BufferQueryService : ServiceBase
    {
        private BufferResultSet lastResult;

        /// <summary>${iServer2_BufferQueryService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<BufferQueryEventArgs> ProcessCompleted;

        /// <overloads>${iServer2_BufferQueryService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_BufferQueryService_constructor_None_D}</summary>
        public BufferQueryService()
        { }
        /// <summary>${iServer2_BufferQueryService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_BufferQueryService_constructor_String_param_url}</param>
        public BufferQueryService(string url)
            : base(url)
        { }
        /// <summary>${iServer2_BufferQueryService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServer2_BufferQueryService_method_ProcessAsync_overloads_D}</overloads>    
        public void ProcessAsync(BufferQueryParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_BufferQueryService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_BufferQueryService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_BufferQueryService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(BufferQueryParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("BufferQueryParameters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);

        }

        private Dictionary<string, string> GetParameters(BufferQueryParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Point2DCollection gps = new Point2DCollection();

            if (parameters.Geometry is GeoPoint)
            {
                string method = "PointBufferQuery";
                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                dict.Add("point", JsonHelper.FromGeoPoint(parameters.Geometry as GeoPoint));
                dict.Add("bufferParam", BufferAnalystParam.ToJson(parameters.BufferParam));
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
                dict.Add("queryMode", ((int)parameters.QueryMode).ToString());

                dictionary.Add("params", Bridge.CreateParams(method, dict));
            }
            else if (parameters.Geometry is GeoLine)
            {
                string method = "LineBufferQuery";
                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                foreach (Point2DCollection g in (parameters.Geometry as GeoLine).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
                dict.Add("points", JsonHelper.FromPoint2DCollection(gps));
                dict.Add("bufferParam", BufferAnalystParam.ToJson(parameters.BufferParam));
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
                dict.Add("queryMode", ((int)parameters.QueryMode).ToString());

                dictionary.Add("params", Bridge.CreateParams(method, dict));
            }
            else if (parameters.Geometry is GeoRegion)
            {
                string method = "PolygonBufferQuery";

                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                foreach (Point2DCollection g in (parameters.Geometry as GeoRegion).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
                dict.Add("points", JsonHelper.FromPoint2DCollection(gps));
                dict.Add("bufferParam", BufferAnalystParam.ToJson(parameters.BufferParam));
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
                dict.Add("queryMode", ((int)parameters.QueryMode).ToString());

                dictionary.Add("params", Bridge.CreateParams(method, dict));
            }

            return dictionary;
        }


        private void OnProcessCompleted(BufferQueryEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(
                    ProcessCompleted, new object[] { this, args });
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            BufferResultSet result = BufferResultSet.FromJson(jsonObject);
            LastResult = result;
            BufferQueryEventArgs args = new BufferQueryEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        /// <summary>${iServer2_BufferQueryService_attribute_lastResult_D}</summary>
        public BufferResultSet LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
    /// <summary>${iServer2_BufferResultSet_Title}</summary>
    public class BufferResultSet
    {
        /// <summary>${iServer2_BufferResultSet_constructor_None_D}</summary>
        public BufferResultSet()
        { }
        /// <summary>${iServer2_BufferResultSet_attribute_ResultSet_D}</summary>
        public ResultSet ResultSet { get; internal set; }
        /// <summary>${iServer2_BufferResultSet_attribute_BufferRegion_D}</summary>
        public GeoRegion BufferRegion { get; internal set; }

        internal static BufferResultSet FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            BufferResultSet result = new BufferResultSet();

            result.ResultSet = ResultSet.FromJson((JsonObject)jsonObject["resultSet"]);

            ServerGeometry serverGeometry = ServerGeometry.FromJson((JsonObject)jsonObject["bufferGeometry"]);
            if (serverGeometry != null)
            {
                result.BufferRegion = serverGeometry.ToGeoRegion();
            }
            return result;
        }
    }
    /// <summary>${iServer2_BufferQueryEventArgs_Title}</summary>
    public sealed class BufferQueryEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer2_BufferQueryEventArgs_constructor_None_D}</summary>
        public BufferQueryEventArgs(BufferResultSet resultSet, string originResult, object userToken)
            : base(userToken)
        {
            BufferResultSet = resultSet;
            OriginBufferResult = originResult;
        }
        /// <summary>${iServer2_BufferQueryEventArgs_attribute_BufferResultSet_D}</summary>
        public BufferResultSet BufferResultSet { get; private set; }
        /// <summary>${iServer2_BufferQueryEventArgs_attribute_OriginResult_D}</summary>
        public string OriginBufferResult { get; private set; }
    }
}
