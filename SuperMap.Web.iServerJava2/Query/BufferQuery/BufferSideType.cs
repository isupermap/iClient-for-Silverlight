﻿//线缓冲区分析的缓冲边的类型，目前支持缓冲左边、缓冲右边、两边同时缓冲三种类型。
//bufferSideType 1. 当该属性值为 LEFT (1)时，通过 leftDistance 属性设置线对象左边缓
//冲距离，而此时 rightDistance 属性无效；bufferSideType 2. 当该属性值为 RIGHT (2)时，
//通过 rightDistance 属性设置线对象右边缓冲距离，而此时 leftDistance 属性无效；
//bufferSideType 3. 当该属性值为 FULL (0)时，通过 leftDistance 属性设置线对象的缓冲
//距离，而此时 rightDistance 属性无效；

//详情请看iServer2.0的帮助文档；
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_BufferSideType_Title}</para>
    /// 	<para>${iServer2_BufferSideType_Description}</para>
    /// 	<para><img src="lineBufferDirection.bmp"/></para>
    /// </summary>
    public enum BufferSideType
    {
        /// <summary>${iServer2_BufferEndType_attribute_full_D}</summary>
        Full = 0,
        /// <summary>${iServer2_BufferEndType_attribute_left_D}</summary>
        Left = 1,
        /// <summary>${iServer2_BufferEndType_attribute_right_D}</summary>
        Right = 2
    }
}
