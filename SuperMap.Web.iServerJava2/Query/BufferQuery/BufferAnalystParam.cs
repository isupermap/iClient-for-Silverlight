﻿using System.Collections.Generic;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_BufferAnalystParam_Title}</para>
    /// 	<para>${iServer2_BufferAnalystParam_Description}</para>
    /// 	<para><img src="BufferAnalyst.bmp"/></para>
    /// </summary>
    public class BufferAnalystParam
    {
        /// <summary>${iServer2_BufferAnalystParam_constructor_None_D}</summary>
        public BufferAnalystParam()
        {
            SemicircleLineSegment = 12;
        }
        /// <summary>${iServer2_BufferAnalystParam_attribute_leftDistance_D}</summary>
        public double LeftDistance { get; set; }
        /// <summary>${iServer2_BufferAnalystParam_attribute_rightDistance_D}</summary>
        public double RightDistance { get; set; }
        /// <summary>${iServer2_BufferAnalystParam_attribute_semicircleLineSegment_D}</summary>
        public int SemicircleLineSegment { get; set; }
        /// <summary>${iServer2_BufferAnalystParam_attribute_bufferSideType_D}</summary>
        public BufferSideType BufferSideType { get; set; }
        /// <summary>${iServer2_BufferAnalystParam_attribute_bufferEndType_D}</summary>
        public BufferEndType BufferEndType { get; set; }

        internal static string ToJson(BufferAnalystParam bufferAnalystParam)
        {
            if (bufferAnalystParam == null)
            {
                return null;
            }

            string json = "{";
            List<string> list = new List<string>();
            list.Add(string.Format("\"leftDistance\":{0}", bufferAnalystParam.LeftDistance));
            list.Add(string.Format("\"rightDistance\":{0}", bufferAnalystParam.RightDistance));
            list.Add(string.Format("\"semicircleLineSegment\":{0}", bufferAnalystParam.SemicircleLineSegment));

            list.Add(string.Format("\"bufferSideType\":\"{0}\"", ((int)bufferAnalystParam.BufferSideType).ToString()));
            list.Add(string.Format("\"bufferEndType\":\"{0}\"", ((int)bufferAnalystParam.BufferEndType)).ToString());

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
