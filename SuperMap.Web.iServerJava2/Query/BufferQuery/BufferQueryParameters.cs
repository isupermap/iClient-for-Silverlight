﻿using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_BufferQueryParameters_Title}</para>
    /// 	<para>${iServer2_BufferQueryParameters_Description}</para>
    /// </summary>
    public class BufferQueryParameters : QueryParametersBase
    {
        /// <summary>${iServer2_BufferQueryParameters_constructor_None_D}</summary>
        public BufferQueryParameters()
        {
        }
        /// <summary>${iServer2_BufferQueryParameters_attribute_bufferParam_D}</summary>
        public BufferAnalystParam BufferParam { get; set; }
        /// <summary>${iServer2_BufferQueryParameters_attribute_queryMode_D}</summary>
        public SpatialQueryMode QueryMode { get; set; }
        /// <summary>${iServer2_BufferQueryParameters_attribute_geometry_D}</summary>
        public Geometry Geometry { get; set; }
    }
}
