﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_EntityBufferQueryParameters_Title}</para>
    /// 	<para>${iServer2_EntityBufferQueryParameters_Description}</para>
    /// </summary>
    public class EntityBufferQueryParameters : BufferQueryParameters
    {
        /// <summary>${iServer2_EntityBufferQueryParameters_constructor_None_D}</summary>
        public EntityBufferQueryParameters()
        { }
        /// <summary>${iServer2_BufferQueryParameters_attribute_fromLayer_D}</summary>
        public string FromLayer { get; set; }
    }
}
