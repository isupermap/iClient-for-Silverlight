﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Query_ReturnResultSetInfo_Title}</para>
    /// 	<para>${iServer2_Query_ReturnResultSetInfo_Description}</para>
    /// </summary>
    public enum ReturnResultSetInfo
    {
        /// <summary>${iServer2_Query_ReturnResultSetInfo_attribute_Attribute_D}</summary>
        Attribute=0,
        /// <summary>${iServer2_Query_ReturnResultSetInfo_attribute_Geometry_D}</summary>
        Geometry=1,
        /// <summary>${iServer2_Query_ReturnResultSetInfo_attribute_AttributeAndGeometry_D}</summary>
        AttributeAndGeometry=2
    }
}
