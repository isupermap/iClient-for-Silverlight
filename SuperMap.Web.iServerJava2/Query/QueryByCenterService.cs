﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Browser;
using System.Json;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Query_QueryByCenterService_Title}</para>
    /// 	<para>${iServer2_Query_QueryByCenterService_Description}</para>
    /// </summary>
    public sealed class QueryByCenterService : ServiceBase
    {
        private ResultSet lastResult;

        /// <summary>${iServer2_Query_QueryByCenterService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<QueryEventArgs> ProcessCompleted;

        /// <overloads>${iServer2_Query_QueryByCenterService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_Query_QueryByCenterService_constructor_None_D}</summary>
        public QueryByCenterService()
        { }

        /// <summary>${iServer2_Query_QueryByCenterService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_Query_QueryByCenterService_constructor_String_param_url}</param>
        public QueryByCenterService(string url)
            : base(url)
        {
        }

        /// <overloads>${iServer2_Query_QueryByCenterService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_Query_QueryByCenterService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Query_QueryByCenterService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(QueryByCenterParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <summary>${iServer2_Query_QueryByCenterService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Query_QueryByCenterService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_Query_QueryByCenterService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(QueryByCenterParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("QueryByCenterParameters");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (parameters.CenterPoint.IsEmpty)
            {
                //TODO:资源
                //throw new InvalidOperationException("CenterPoint is Empty");
                throw new InvalidOperationException(ExceptionStrings.CenterPointIsEmpty);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(QueryByCenterParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = parameters.IsNearest ? "FindNearest" : "QueryByPoint";// queryByCircle

            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);
            dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
            dict.Add("point", JsonHelper.FromPoint2D(parameters.CenterPoint));
            dict.Add("tolerance", parameters.Tolerance.ToString());

            dictionary.Add("params", Bridge.CreateParams(method, dict));
            return dictionary;
        }

        private void OnProcessCompleted(QueryEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(
                    ProcessCompleted, new object[] { this, args });
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            ResultSet result = ResultSet.FromJson(jsonObject);
            LastResult = result;
            QueryEventArgs args = new QueryEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        /// <summary>${iServer2_Query_QueryByCenterService_attribute_lastResult_D}</summary>
        public ResultSet LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
