﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Query_QueryServiceEventArgs_Title}</para>
    /// 	<para>${iServer2_Query_QueryServiceEventArgs_Description}</para>
    /// </summary>
    public sealed class QueryEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer2_QueryEventArgs_constructor_D}</summary>
        public QueryEventArgs(ResultSet resultSet, string originResult, object userToken)
            : base(userToken)
        {
            ResultSet = resultSet;
            OriginResult = originResult;
        }

        /// <summary>${iServer2_Query_QueryServiceEventArgs_attribute_Result_D}</summary>
        public ResultSet ResultSet { get; private set; }
        /// <summary>${iServer2_Query_QueryServiceEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
