﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Query_QueryByGeometryService_Title}</para>
    /// 	<para>${iServer2_Query_QueryByGeometryService_Description}</para>
    /// </summary>
    public class QueryByGeometryService : ServiceBase
    {
        private ResultSet lastResult;

        /// <summary>${iServer2_Query_QueryByGeometryService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<QueryEventArgs> ProcessCompleted;

        /// <overloads>${iServer2_Query_QueryByGeometryService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_Query_QueryByGeometryService_constructor_None_D}</summary>
        public QueryByGeometryService()
        { }


        /// <summary>${iServer2_Query_QueryByGeometryService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_Query_QueryByGeometryService_constructor_String_param_url}</param>
        public QueryByGeometryService(string url)
            : base(url)
        {
        }

        /// <overloads>${iServer2_Query_QueryByGeometryService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_Query_QueryByGeometryService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Query_QueryByGeometryService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(QueryByGeometryParameters parameters)
        {
            this.ProcessAsync(parameters, null);
        }

        /// <summary>${iServer2_Query_QueryByGeometryService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Query_QueryByGeometryService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_Query_QueryByGeometryService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(QueryByGeometryParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("QueryByGeometryParamters is Null");
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (parameters.Geometry == null)
            {
                //TODO:资源
                //throw new InvalidOperationException("The geometry is null");
                throw new InvalidOperationException(ExceptionStrings.GeometryIsNull);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }


        private Dictionary<string, string> GetParameters(QueryByGeometryParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Point2DCollection gps = new Point2DCollection();

            if (parameters.Geometry is GeoRegion)
            {
                string method = "QueryByPolygon";

                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));

                foreach (Point2DCollection g in (parameters.Geometry as GeoRegion).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }//服务端其实不支持复杂几何体的
                dict.Add("points", JsonHelper.FromPoint2DCollection(gps));

                dictionary.Add("params", Bridge.CreateParams(method, dict));

            }
            else if (parameters.Geometry is GeoCircle)
            {
                string method = "QueryByPolygon";

                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));

                foreach (Point2DCollection g in (parameters.Geometry as GeoCircle).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }//服务端其实不支持复杂几何体的
                dict.Add("points", JsonHelper.FromPoint2DCollection(gps));

                dictionary.Add("params", Bridge.CreateParams(method, dict));

            }
            else if (parameters.Geometry is GeoLine)
            {
                string method = "QueryByLine";

                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
                foreach (Point2DCollection g in (parameters.Geometry as GeoLine).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
                dict.Add("points", JsonHelper.FromPoint2DCollection(gps));

                dictionary.Add("params", Bridge.CreateParams(method, dict));
            }
            else if (parameters.Geometry is GeoPoint)
            {
                string method = "QueryByPoint";
                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                dict.Add("queryParam", QueryParam.ToJson(parameters.QueryParam));
                dict.Add("point", JsonHelper.FromPoint2D((parameters.Geometry as GeoPoint).Bounds.Center));
                dict.Add("tolerance", "0");
                dictionary.Add("params", Bridge.CreateParams(method, dict));
            }

            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            ResultSet result = ResultSet.FromJson(jsonObject);
            LastResult = result;
            QueryEventArgs args = new QueryEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(QueryEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(
                    ProcessCompleted, new object[] { this, args });
            }
        }

        /// <summary>${iServer2_Query_QueryByGeometryService_attribute_lastResult_D}</summary>
        public ResultSet LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
