﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// ${iServer2_queryParamertersBase_Title}<br/>
    /// ${iServer2_queryParamertersBase_Description}
    /// </summary>
    public abstract class QueryParametersBase : ParametersBase
    {
        /// <summary>${iServer2_QueryParamertersBase_constructor_D}</summary>
        public QueryParametersBase()
        {
        }
        /// <summary>${iServer2_queryParamertersBase_attribute_QueryParam_D}</summary>
        public QueryParam QueryParam { get; set; }
    }
}
