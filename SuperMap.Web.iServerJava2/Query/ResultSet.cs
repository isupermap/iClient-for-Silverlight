﻿using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Query_ResultSet_Title}</para>
    /// 	<para>${iServer2_Query_ResultSet_Description}</para>
    /// </summary>
    public class ResultSet
    {
        private ResultSet()
        {
            RecordSets = new List<RecordSet>();
        }

        /// <summary>${iServer2_Query_ResultSet_attribute_currentCount_D}</summary>
        public int CurrentCount { get; private set; }
        /// <summary>${iServer2_Query_ResultSet_attribute_totalCount_D}</summary>
        public int TotalCount { get; private set; }
        /// <summary>${iServer2_Query_ResultSet_attribute_recordSets_D}</summary>
        public List<RecordSet> RecordSets { get; private set; }
        /// <summary>${iServer2_Query_ResultSet_attribute_customResponse_D}</summary>
        public string CustomResponse { get; private set; }
        /// <summary>${iServer2_ResultSet_method_FromJson_D}</summary>
        /// <returns>${iServer2_ResultSet_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ResultSet_method_FromJson_param_jsonObject}</param>
        public static ResultSet FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ResultSet resultSet = new ResultSet();
            resultSet.TotalCount = (int)jsonObject["totalCount"];
            if (resultSet.TotalCount == 0)
            {
                return null;
            }//如果为0，认为结果为空?
            resultSet.CurrentCount = (int)jsonObject["currentCount"];
            resultSet.CustomResponse = (string)jsonObject["customResponse"];

            JsonArray recordSets = (JsonArray)jsonObject["recordSets"];
            if (recordSets != null && recordSets.Count > 0)
            {
                resultSet.RecordSets = new List<RecordSet>();
                for (int i = 0; i < recordSets.Count; i++)
                {
                    resultSet.RecordSets.Add(RecordSet.FromJson((JsonObject)recordSets[i]));
                }
            }
            return resultSet;
        }

        //写了反而多余
        //public List<FeatureCollection> FeatureResultSet()
        //{
        //}

    }
}
