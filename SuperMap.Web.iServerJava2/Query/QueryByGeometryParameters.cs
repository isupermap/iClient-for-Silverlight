﻿using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// ${iServer2_queryByGeometryParameters_Title}<br/>
    /// ${iServer2_queryByGeometryParameters_Description}
    /// </summary>
    public class QueryByGeometryParameters : QueryParametersBase
    {
        /// <summary>${iServer2_queryByGeometryParameters_constructor_None_D}</summary>
        public QueryByGeometryParameters()
        {
        }
        /// <summary>${iServer2_queryByGeometryParameters_attribute_geometry_D}</summary>
        public Geometry Geometry { get; set; }
    }
}
