﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_measure_MeasureServiceEventArgs_Title}</para>
    /// 	<para>${iServer2_measure_MeasureServiceEventArgs_Description}</para>
    /// </summary>
    public class MeasureEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer2_MeasureServiceEventArgs_constructor_D}</summary>
        public MeasureEventArgs(MeasureResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServer2_measure_MeasureServiceEventArgs_attribute_result}</summary>
        public MeasureResult Result { get; private set; }
        /// <summary>${iServer2_measure_MeasureServiceEventArgs_attribute_originResult}</summary>
        public string OriginResult { get; private set; }
    }
}
