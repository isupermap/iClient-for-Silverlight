﻿using System.Json;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_measure_MeasureResult_Title}</para>
    /// 	<para>${iServer2_measure_MeasureResult_Description}</para>
    /// </summary>
    public class MeasureResult
    {
        /// <summary>${iServer2_measure_MeasureResult_constructor_None_D}</summary>
        internal MeasureResult()
        { }
        /// <summary>${iServer2_measure_MeasureResult_attribute_distance}</summary>
        public double Distance { get; private set; }
        /// <summary>${iServer2_measure_MeasureResult_attribute_area}</summary>
        public double Area { get; private set; }

        /// <summary>${iServer2_MeasureResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_MeasureResult_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_MeasureResult_method_FromJson_return}</returns>
        public static MeasureResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            MeasureResult result = new MeasureResult();
            result.Distance = (double)jsonObject["distance"];
            result.Area = (double)jsonObject["area"];
            return result;
        }
    }
}
