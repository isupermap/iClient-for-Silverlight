﻿using SuperMap.Web.Core;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_measure_MeasureParameter_Title}</para>
    /// 	<para>${iServer2_measure_MeasureParameter_Description}</para>
    /// </summary>
    public class MeasureParameters : ParametersBase
    {
        /// <summary>${iServer2_measure_MeasureParameter_constructor_None_D}</summary>
        public MeasureParameters()
        {
        }
        /// <summary>${iServer2_measure_MeasureParameter_attribute_Geometry}</summary>
        public Geometry Geometry { get; set; }
    }
}
