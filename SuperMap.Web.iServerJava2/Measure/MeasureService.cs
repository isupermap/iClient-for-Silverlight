﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Service;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_measure_MeasureService_Title}</para>
    /// 	<para>${iServer2_measure_MeasureService_Description}</para>
    /// </summary>
    public class MeasureService : ServiceBase
    {
        /// <overloads>${iServer2_measure_MeasureService_constructor_overloads}</overloads>
        /// <summary>${iServer2_measure_MeasureService_constructor_None_D}</summary>
        public MeasureService()
        { }
        /// <overloads>${iServer2_measure_MeasureService_constructor_overloads}</overloads>
        /// <summary>${iServer2_measure_MeasureService_constructor_String_D}</summary>
        public MeasureService(string url)
            : base(url)
        { }

        /// <overloads>${iServer2_measure_MeasureService_method_processAsync_overloads}</overloads>
        /// <summary>${iServer2_measure_MeasureService_method_processAsync_D}</summary>
        /// <param name="parameters">${iServer2_measure_MeasureService_method_processAsync_param_parameters}</param>
        public void ProcessAsync(MeasureParameters parameters)
        {
            ProcessAsync(parameters, null);
        }

        /// <overloads>${iServer2_measure_MeasureService_method_processAsync_overloads}</overloads>
        /// <summary>${iServer2_measure_MeasureService_method_processAsync_object_D}</summary>
        /// <param name="parameters">${iServer2_measure_MeasureService_method_processAsync_param_parameters}</param>
        /// <param name="state">${iServer2_measure_MeasureService_method_processAsync_param_state}</param>
        public void ProcessAsync(MeasureParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(MeasureParameters parameters)
        {

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Point2DCollection gps = new Point2DCollection();

            if (parameters.Geometry is GeoLine)
            {
                string method = "MeasureDistance";

                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                foreach (Point2DCollection g in (parameters.Geometry as GeoLine).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
                dict.Add("points", JsonHelper.FromPoint2DCollection(gps));

                dictionary.Add("params", Bridge.CreateParams(method, dict));
            }
            else if (parameters.Geometry is GeoRegion)
            {
                string method = "MeasureArea";

                dictionary.Add("method", method);
                dictionary.Add("mapName", parameters.MapName);

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("mapName", parameters.MapName);
                foreach (Point2DCollection g in (parameters.Geometry as GeoRegion).Parts)
                {
                    for (int i = 0; i < g.Count; i++)
                    {
                        gps.Add(g[i]);
                    }
                }
                dict.Add("points", JsonHelper.FromPoint2DCollection(gps));

                dictionary.Add("params", Bridge.CreateParams(method, dict));

            }
            return dictionary;
        }


        private MeasureResult lastResult;

        /// <summary>${iServer2_measure_MeasureService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<MeasureEventArgs> ProcessCompleted;
        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            MeasureResult result = MeasureResult.FromJson(jsonObject);
            LastResult = result;
            MeasureEventArgs args = new MeasureEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(MeasureEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${iServer2_measure_MeasureService_attribute_lastResult_D}</summary>
        public MeasureResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }

}
