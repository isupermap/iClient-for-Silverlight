﻿

namespace SuperMap.Web.iServerJava2
{
    /// <summary>${iServer2_SuperMapLayerType_Title}</summary>
    public enum SuperMapLayerType
    {
        /// <summary>${iServer2_SuperMapLayerType_attribute_vector_D}</summary>
        Vector = 0,
        /// <summary>${iServer2_SuperMapLayerType_attribute_Grid_D}</summary>
        Grid = 1,
        /// <summary>${iServer2_SuperMapLayerType_attribute_Image_D}</summary>
        Image = 2,
        /// <summary>${iServer2_SuperMapLayerType_attribute_WMS_D}</summary>
        WMS = 3,
        /// <summary>${iServer2_SuperMapLayerType_attribute_WFS_D}</summary>
        WFS = 4
    }
}
