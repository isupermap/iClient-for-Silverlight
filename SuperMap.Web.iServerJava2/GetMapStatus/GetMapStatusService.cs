﻿
using System;
using SuperMap.Web.iServerJava2.Resources;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Json;
using System.Windows;
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_GetMapStatusService_Title}</para>
    /// 	<para>${iServer2_GetMapStatusService_Description}</para>
    /// </summary>
    public class GetMapStatusService : ServiceBase
    {
        /// <summary>${iServer2_GetMapStatusService_constructor_None_D}</summary>
        /// <overloads>${iServer2_GetMapStatusService_constructor_overloads_D}</overloads>
        public GetMapStatusService()
        { }
        /// <summary>${iServer2_GetMapStatusService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_GetMapStatusService_constructor_String_param_url}</param>
        public GetMapStatusService(string url)
            : base(url)
        { }

        /// <summary>${iServer2_GetMapStatusService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServer2_GetMapStatusService_method_ProcessAsync_overloads_D}</overloads>
        /// <param name="parameters">${iServer2_GetMapStatusService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(GetMapStatusParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_GetMapStatusService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_GetMapStatusService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_GetMapStatusService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GetMapStatusParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(GetMapStatusParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "GetMapStatus";
            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);
            dict.Add("mapServicesAddress", parameters.MapServicesAddress);
            dict.Add("mapServicesPort", parameters.MapServicesPort);

            dictionary.Add("params", Bridge.CreateParams(method, dict));
            return dictionary;
        }


        private GetMapStatusResult lastResult;

        /// <summary>${iServer2_GetMapStatusService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetMapStatusEventArgs> ProcessCompleted;
        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            GetMapStatusResult result = GetMapStatusResult.FromJson(jsonObject);
            LastResult = result;
            GetMapStatusEventArgs args = new GetMapStatusEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(GetMapStatusEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${iServer2_GetMapStatusService_attribute_lastResult_D}</summary>
        public GetMapStatusResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
