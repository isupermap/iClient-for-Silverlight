﻿

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_GetMapStatusParameters_Title}</para>
    /// 	<para>${iServer2_GetMapStatusParameters_Description}</para>
    /// </summary>
    public class GetMapStatusParameters : ParametersBase
    {
        /// <summary>${iServer2_GetMapStatusParameters_constructor_None_D}</summary>
        public GetMapStatusParameters()
        { }
        /// <summary>${iServer2_GetMapStatusParameters_attribute_mapServiceAddress_D}</summary>
        public string MapServicesAddress { get; set; }
        /// <summary>${iServer2_GetMapStatusParameters_attribute_mapServicesPort_D}</summary>
        public string MapServicesPort { get; set; }
    }
}
