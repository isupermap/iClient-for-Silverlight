﻿

using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_JoinItem_Title}</para>
    /// <para>${iServer2_JoinItem_Description}</para>
    /// </summary>
    public class JoinItem
    {
        public JoinItem()
        { }
        /// <summary>${iServer2_JoinItem_attribute_foreignTableName_D}</summary>
        public string ForeignTableName { get; set; }
        /// <summary>${iServer2_JoinItem_attribute_joinFilter_D}</summary>
        public string JoinFilter { get; set; }
        /// <summary>${iServer2_JoinItem_attribute_joinType_D}</summary>
        public JoinType JoinType { get; set; }

        internal static string ToJson(JoinItem param)
        {
            if (param == null)
            {
                return null;
            }

            string json = "[{";
            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(param.ForeignTableName))
            {
                list.Add(string.Format("\"foreignTableName\":\"{0}\"", param.ForeignTableName));
            }
            else
            {
                list.Add("\"foreignTableName\":null");
            }

            if (!string.IsNullOrEmpty(param.JoinFilter))
            {
                list.Add(string.Format("\"joinFilter\":\"{0}\"", param.JoinFilter));
            }
            else
            {
                list.Add("\"joinFilter\":null");
            }

            list.Add(string.Format("\"joinType\":{0}", param.JoinType == JoinType.Innerjoin ? 0 : 1));

            json += string.Join(",", list.ToArray());
            json += "}]";
            return json;
        }




        /// <summary>${iServer2_JoinItem_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_JoinItem_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_JoinItem_method_FromJson_return}</returns>
        public static JoinItem FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            JoinItem result = new JoinItem();
            result.ForeignTableName = (string)jsonObject["foreignTableName"];
            result.JoinFilter = (string)jsonObject["joinFilter"];
            result.JoinType = (JoinType)(int)jsonObject["joinType"];
            return result;
        }
    }
}
