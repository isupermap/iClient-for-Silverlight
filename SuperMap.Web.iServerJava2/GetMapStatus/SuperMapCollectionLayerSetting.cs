﻿

using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SuperMapCollectionLayerSetting_Title}</para>
    /// 	<para>${iServer2_SuperMapCollectionLayerSetting_Description}</para>
    /// </summary>
    public class SuperMapCollectionLayerSetting : ServerLayerSetting
    {
        internal SuperMapCollectionLayerSetting()
        { }

        /// <summary>${iServer2_SuperMapCollectionLayerSetting_attribute_mapName_D}</summary>
        public string MapName { get; private set; }
        /// <summary>${iServer2_SuperMapCollectionLayerSetting_attribute_serviceAddress_D}</summary>
        public string ServiceAddress { get; private set; }
        /// <summary>${iServer2_SuperMapCollectionLayerSetting_attribute_servicePort_D}</summary>
        public int ServicePort { get; private set; }
        /// <summary>${iServer2_SuperMapCollectionLayerSetting_method_FromJson_D}</summary>
        /// <returns>${iServer2_SuperMapCollectionLayerSetting_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_SuperMapCollectionLayerSetting_method_FromJson_param_jsonObject}</param>
        public static SuperMapCollectionLayerSetting FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            SuperMapCollectionLayerSetting result = new SuperMapCollectionLayerSetting();

            result.MapName = (string)jsonObject["mapName"];
            result.ServiceAddress = (string)jsonObject["serviceAddress"];
            result.ServicePort = (int)jsonObject["servicePort"];
            result.ServerLayerSettingType = (ServerLayerSettingType)(int)jsonObject["layerSettingType"];

            return result;
        }
    }
}
