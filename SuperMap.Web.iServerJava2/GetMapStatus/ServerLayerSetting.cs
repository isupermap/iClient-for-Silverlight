﻿

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ServerLayerSetting_Title}</para>
    /// </summary>
    public abstract class ServerLayerSetting
    {
        internal ServerLayerSetting()
        { }
        /// <summary>${iServer2_ServerLayerSetting_attribute_serverLayerSettingType_D}</summary>
        public ServerLayerSettingType ServerLayerSettingType { get; protected set; }
    }
}
