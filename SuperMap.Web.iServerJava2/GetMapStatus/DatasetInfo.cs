﻿

using SuperMap.Web.Core;
using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_DatasetInfo_Title}</para>
    /// <para>${iServer2_DatasetInfo_Description}</para>
    /// </summary>
    public class DatasetInfo
    {
        public DatasetInfo() { }

        /// <summary>${iServer2_DatasetInfo_attribute_datasetName_D}</summary>
        public string DatasetName { get; set; }
        /// <summary>${iServer2_DatasetInfo_attribute_datasetType_D}</summary>
        public DatasetType DatasetType { get; set; }
        /// <summary>${iServer2_DatasetInfo_attribute_datasourceName_D}</summary>
        public string DatasourceName { get; set; }
        /// <summary>${iServer2_DatasetInfo_attribute_bounds_D}</summary>
        public Rectangle2D Bounds { get; private set; }

        /// <summary>${iServer2_DatasetInfo_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_DatasetInfo_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_DatasetInfo_method_FromJson_return}</returns>
        public static DatasetInfo FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            DatasetInfo result = new DatasetInfo();

            result.DatasetName = (string)jsonObject["datasetName"];
            result.DatasetType = (DatasetType)(int)jsonObject["datasetType"];
            result.DatasourceName = (string)jsonObject["datasourceName"];
            result.Bounds = ToRectangle2D((JsonObject)jsonObject["bounds"]);

            return result;
        }
        internal static Rectangle2D ToRectangle2D(JsonObject jsonObject)
        {
            double mbMinX = (double)jsonObject["leftBottom"]["x"];
            double mbMinY = (double)jsonObject["leftBottom"]["y"];
            double mbMaxX = (double)jsonObject["rightTop"]["x"];
            double mbMaxY = (double)jsonObject["rightTop"]["y"];
            return new Rectangle2D(mbMinX, mbMinY, mbMaxX, mbMaxY);
        }

        internal static string ToJson(DatasetInfo datasetInfo)
        {
            if (datasetInfo == null)
            {
                return null;
            }
            string json = "{";

            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(datasetInfo.DatasetName))
            {
                list.Add(string.Format("\"datasetName\":\"{0}\"", datasetInfo.DatasetName));
            }
            else
            {
                list.Add("\"datasetName\":\"\"");
            }

            if (!string.IsNullOrEmpty(datasetInfo.DatasourceName))
            {
                list.Add(string.Format("\"datasourceName\":\"{0}\"", datasetInfo.DatasourceName));
            }
            else
            {
                list.Add(string.Format("\"datasourceName\":null"));
            }

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }

}
