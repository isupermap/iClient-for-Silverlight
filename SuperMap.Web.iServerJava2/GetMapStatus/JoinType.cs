﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_JoinType_Title}</para>
    /// <para>${iServer2_JoinType_Title}</para>
    /// </summary>
    public enum JoinType
    {
        /// <summary>${iServer2_JoinType_attribute_innerjoin_D}</summary>
        Innerjoin = 0,
        /// <summary>${iServer2_Entity_attribute_leftjoin_D}</summary>
        Leftjoin = 1
    }
}
