﻿

using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary><para>${iServer2_WmsLayerSetting_Title}</para></summary>
    public class WmsLayerSetting : ServerLayerSetting
    {
        internal WmsLayerSetting()
        { }

        /// <summary>${iServer2_WmsLayerSetting_attribute_exceptions_D}</summary>
        public string Exceptions { get; private set; }
        /// <summary>${iServer2_WmsLayerSetting_attribute_format_D}</summary>
        public string Format { get; private set; }
        /// <summary>${iServer2_WmsLayerSetting_attribute_request_D}</summary>
        public string Request { get; private set; }

        /// <summary>${iServer2_WmsLayerSetting_attribute_service_D}</summary>
        public string Service { get; private set; }
        /// <summary>${iServer2_WmsLayerSetting_attribute_styles_D}</summary>
        public string Styles { get; private set; }
        /// <summary>${iServer2_WmsLayerSetting_attribute_url_D}</summary>
        public string Url { get; private set; }
        /// <summary>${iServer2_WmsLayerSetting_attribute_version_D}</summary>
        public string Version { get; private set; }
        /// <summary>${iServer2_WmsLayerSetting_method_FromJson_D}</summary>
        /// <returns>${iServer2_WmsLayerSetting_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_WmsLayerSetting_method_FromJson_param_jsonObject}</param>
        public static WmsLayerSetting FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            WmsLayerSetting result = new WmsLayerSetting();

            result.Exceptions = (string)jsonObject["exceptions"];
            result.Format = (string)jsonObject["format"];
            result.Request = (string)jsonObject["request"];
            result.Service = (string)jsonObject["service"];
            result.Styles = (string)jsonObject["styles"];
            result.Url = (string)jsonObject["url"];
            result.Version = (string)jsonObject["version"];

            result.ServerLayerSettingType = (ServerLayerSettingType)(int)jsonObject["layerSettingType"];

            return result;
        }
    }
}
