﻿

using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ServerLayer_Title}</para>
    /// 	<para>${iServer2_ServerLayer_Description}</para>
    /// </summary>
    public class ServerLayer
    {
        internal ServerLayer()
        { }

        /// <summary>${iServer2_ServerLayer_attribute_caption_D}</summary>
        public string Caption { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_name_D}</summary>
        public string Name { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_isSymbolScalable_D}</summary>
        public bool IsSymbolScalable { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_minScale_D}</summary>
        public double MinScale { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_minVisibleGeometrySize_D}</summary>
        public double MinVisibleGeometrySize { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_opaqueRate_D}</summary>
        public int OpaqueRate { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_queryable_D}</summary>
        public bool Queryable { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_visible_D}</summary>
        public bool Visible { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_description_D}</summary>
        public string Description { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_serverLayerSetting_D}</summary>
        public ServerLayerSetting ServerLayerSetting { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_displayFilter_D}</summary>
        public string DisplayFilter { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_maxScale_D}</summary>
        public double MaxScale { get; private set; }
        /// <summary>${iServer2_ServerLayer_attribute_serverSubLayers_D}</summary>
        public List<ServerLayer> ServerSubLayers { get; private set; }

        /// <summary>${iServer2_ServerLayer_method_FromJson_D}</summary>
        /// <returns>${iServer2_ServerLayer_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ServerLayer_method_FromJson_param_jsonObject}</param>
        public static ServerLayer FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ServerLayer result = new ServerLayer();

            #region
            if (jsonObject.ContainsKey("caption"))
            {
                result.Caption = (string)jsonObject["caption"];
            }
            if (jsonObject.ContainsKey("name"))
            {
                result.Name = (string)jsonObject["name"];
            }
            if (jsonObject.ContainsKey("isSymbolScalable"))
            {
                result.IsSymbolScalable = (bool)jsonObject["isSymbolScalable"];
            }
            if (jsonObject.ContainsKey("minScale"))
            {
                result.MinScale = (double)jsonObject["minScale"];
            }
            if (jsonObject.ContainsKey("minVisibleGeometrySize"))
            {
                result.MinVisibleGeometrySize = (double)jsonObject["minVisibleGeometrySize"];
            }
            if (jsonObject.ContainsKey("opaqueRate"))
            {
                result.OpaqueRate = (int)jsonObject["opaqueRate"];
            }
            if (jsonObject.ContainsKey("queryable"))
            {
                result.Queryable = (bool)jsonObject["queryable"];
            }
            if (jsonObject.ContainsKey("visible"))
            {
                result.Visible = (bool)jsonObject["visible"];
            }
            if (jsonObject.ContainsKey("description"))
            {
                result.Description = (string)jsonObject["description"];
            }
            if (jsonObject.ContainsKey("layerSetting") && jsonObject["layerSetting"] != null)
            {
                if (jsonObject["layerSetting"]["layerSettingType"] == 12)
                {
                    result.ServerLayerSetting = SuperMapCollectionLayerSetting.FromJson((JsonObject)jsonObject["layerSetting"]);
                }
                else if (jsonObject["layerSetting"]["layerSettingType"] == 1)
                {
                    result.ServerLayerSetting = SuperMapLayerSetting.FromJson((JsonObject)jsonObject["layerSetting"]);
                }
                else if (jsonObject["layerSetting"]["layerSettingType"] == 2)
                {
                    result.ServerLayerSetting = WmsLayerSetting.FromJson(((JsonObject)jsonObject["layerSetting"]));
                }
                //这个应该写全了；
            }
            if (jsonObject.ContainsKey("displayFilter"))
            {
                result.DisplayFilter = (string)jsonObject["displayFilter"];
            }
            if (jsonObject.ContainsKey("maxScale"))
            {
                result.MaxScale = (double)jsonObject["maxScale"];
            }
            #endregion
            if (jsonObject.ContainsKey("subLayers") && jsonObject["subLayers"] != null)
            {
                result.ServerSubLayers = new List<ServerLayer>();
                for (int i = 0; i < jsonObject["subLayers"].Count; i++)
                {
                    ServerLayer subLayer = new ServerLayer();
                    subLayer.Caption = (string)jsonObject["subLayers"][i]["caption"];
                    subLayer.Name = (string)jsonObject["subLayers"][i]["name"];
                    subLayer.IsSymbolScalable = (bool)jsonObject["subLayers"][i]["isSymbolScalable"];
                    subLayer.MinScale = (double)jsonObject["subLayers"][i]["minScale"];
                    subLayer.MinVisibleGeometrySize = (double)jsonObject["subLayers"][i]["minVisibleGeometrySize"];
                    subLayer.OpaqueRate = (int)jsonObject["subLayers"][i]["opaqueRate"];
                    subLayer.Queryable = (bool)jsonObject["subLayers"][i]["queryable"];
                    subLayer.Visible = (bool)jsonObject["subLayers"][i]["visible"];
                    subLayer.Description = (string)jsonObject["subLayers"][i]["description"];

                    if (jsonObject["subLayers"][i]["layerSetting"] != null)
                    {
                        if (jsonObject["subLayers"][i]["layerSetting"]["layerSettingType"] == 1)
                        {
                            subLayer.ServerLayerSetting = SuperMapLayerSetting.FromJson((JsonObject)jsonObject["subLayers"][i]["layerSetting"]);
                        }
                        else if (jsonObject["subLayers"][i]["layerSetting"]["layerSettingType"] == 2)
                        {
                            subLayer.ServerLayerSetting = WmsLayerSetting.FromJson((JsonObject)jsonObject["subLayers"][i]["layerSetting"]);
                        }
                        else if (jsonObject["subLayers"][i]["layerSetting"]["layerSettingType"] == 12)
                        {
                            subLayer.ServerLayerSetting = SuperMapCollectionLayerSetting.FromJson((JsonObject)jsonObject["subLayers"][i]["layerSetting"]);
                        }
                    }

                    subLayer.DisplayFilter = (string)jsonObject["subLayers"][i]["displayFilter"];
                    subLayer.MaxScale = (double)jsonObject["subLayers"][i]["maxScale"];
                    result.ServerSubLayers.Add(subLayer);

                }
            }
            return result;
        }
    }
}
