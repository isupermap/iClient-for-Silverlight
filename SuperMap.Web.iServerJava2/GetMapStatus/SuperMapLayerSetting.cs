﻿

using System.Json;
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SuperMapLayerSetting_Title}</para>
    /// 	<para>${iServer2_SuperMapLayerSetting_Description}</para>
    /// </summary>
    public class SuperMapLayerSetting : ServerLayerSetting
    {
        internal SuperMapLayerSetting()
        { }
        /// <summary>${iServer2_SuperMapLayerSetting_attribute_brightness_D}</summary>
        public int Brightness { get; private set; }
        /// <summary>${iServer2_SuperMapLayerSetting_attribute_contrast_D}</summary>
        public int Contrast { get; private set; }
        /// <summary>${iServer2_SuperMapLayerSetting_attribute_datasetInfo_D}</summary>
        public DatasetInfo DatasetInfo { get; private set; }
        /// <summary>${iServer2_SuperMapLayerSetting_attribute_style_D}</summary>
        public ServerStyle Style { get; private set; }
        /// <summary>${iServer2_SuperMapLayerSetting_attribute_superMapLayerType_D}</summary>
        public SuperMapLayerType SuperMapLayerType { get; private set; }
        /// <summary>${iServer2_SuperMapLayerSetting_attribute_opaqueRate_D}</summary>
        public int OpaqueRate { get; private set; }
        /// <summary>${iServer2_SuperMapLayerSetting_attribute_theme_D}</summary>
        public Theme Theme { get; private set; }
        /// <summary>${iServer2_SuperMapLayerSetting_attribute_joinItems_D}</summary>
        public List<JoinItem> JoinItems { get; private set; }
        /// <summary>${iServer2_SuperMapLayerSetting_method_FromJson_D}</summary>
        /// <returns>${iServer2_SuperMapLayerSetting_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_SuperMapLayerSetting_method_FromJson_param_jsonObject}</param>
        public static SuperMapLayerSetting FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            SuperMapLayerSetting result = new SuperMapLayerSetting();
            result.Brightness = (int)jsonObject["brightness"];
            result.Contrast = (int)jsonObject["contrast"];
            if (jsonObject.ContainsKey("datasetInfo") && jsonObject["datasetInfo"] != null)
            {
                result.DatasetInfo = DatasetInfo.FromJson((JsonObject)jsonObject["datasetInfo"]);
            }

            if (jsonObject.ContainsKey("joinItems") && jsonObject["joinItems"] != null)
            {
                result.JoinItems = new List<JoinItem>();
                for (int i = 0; i < jsonObject["joinItems"].Count; i++)
                {
                    result.JoinItems.Add(JoinItem.FromJson((JsonObject)jsonObject["joinItems"]));
                }
            }

            if (jsonObject.ContainsKey("style"))
            {
                result.Style = ServerStyle.FromJson((JsonObject)jsonObject["style"]);
            }
            else if (jsonObject.ContainsKey("theme"))
            {
                //这里要判断ThemeType，对不同的专题图进行解析；
                //但是还存在问题：有Style属性的没有Theme，反之亦然；
                if (jsonObject["theme"]["themeType"] == 1)
                {
                    //ThemeUnique
                    result.Theme = ThemeUnique.FromJson((JsonObject)jsonObject["theme"]);
                }
                else if (jsonObject["theme"]["themeType"] == 2)
                {
                    //ThemeRange
                    result.Theme = ThemeRange.FromJson(((JsonObject)jsonObject["theme"]));
                }
                else if (jsonObject["theme"]["themeType"] == 3)
                {
                    //ThemeGraph
                    result.Theme = ThemeGraph.FromJson((JsonObject)jsonObject["theme"]);
                }
                else if (jsonObject["theme"]["themeType"] == 4)
                {
                    //ThemeGraduatedSymbol
                    result.Theme = ThemeGraduatedSymbol.FromJson((JsonObject)jsonObject["theme"]);
                }
                else if (jsonObject["theme"]["themeType"] == 5)
                {
                    // ThemeDotDensity
                    result.Theme = ThemeDotDensity.FromJson((JsonObject)jsonObject["theme"]);
                }
                else if (jsonObject["theme"]["themeType"] == 7)
                {
                    //ThemeLabel
                    result.Theme = Themelabel.FromJson((JsonObject)jsonObject["theme"]);
                }
            }
            if (jsonObject.ContainsKey("superMapLayerType"))
            {
                result.SuperMapLayerType = (SuperMapLayerType)(int)jsonObject["superMapLayerType"];
            }
            result.OpaqueRate = (int)jsonObject["opaqueRate"];
            result.ServerLayerSettingType = (ServerLayerSettingType)(int)jsonObject["layerSettingType"];
            return result;
        }
    }
}
