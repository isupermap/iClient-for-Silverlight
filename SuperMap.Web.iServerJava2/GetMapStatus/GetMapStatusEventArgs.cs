﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_GetMapStatusArgs_Title}</para>
    /// 	<para>${iServer2_GetMapStatusArgs_Description}</para>
    /// </summary>
    public class GetMapStatusEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer2_GetMapStatusEventArgs_constructor_D}</summary>
        public GetMapStatusEventArgs(GetMapStatusResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServer2_GetMapStatusArgs_attribute_result_D}</summary>
        public GetMapStatusResult Result { get; private set; }
        /// <summary>${iServer2_GetMapStatusEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
