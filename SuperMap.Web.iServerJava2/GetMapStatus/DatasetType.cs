﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_DatasetType_Title}</para>
    /// <para>${iServer2_DatasetType_Description}</para>
    /// </summary>
    public enum DatasetType
    {
        /// <summary>${iServer2_JoinType_attribute_undefined_D}</summary>
        Undefined=-1,
        /// <summary>${iServer2_Entity_attribute_point_D}</summary>
        Point=1,
        /// <summary>${iServer2_Entity_attribute_line_D}</summary>
        Line=3,
        /// <summary>${iServer2_Entity_attribute_region_D}</summary>
        Region=5,
        /// <summary>${iServer2_Entity_attribute_text_D}</summary>
        Text=7,
        /// <summary>${iServer2_Entity_attribute_network_D}</summary>
        Network=4,
        /// <summary>${iServer2_Entity_attribute_grid_D}</summary>
        Grid=83,
        /// <summary>${iServer2_Entity_attribute_image_D}</summary>
        Image=81,
        /// <summary>${iServer2_Entity_attribute_CAD_D}</summary>
        CAD=149,
        /// <summary>${iServer2_Entity_attribute_lineM_D}</summary>
        LineM=35,
        /// <summary>${iServer2_Entity_attribute_tabular_D}</summary>
        Tabular=0,
        /// <summary>${iServer2_Entity_attribute_networkpoint_D}</summary>
        Networkpoint=-2,
        /// <summary>${iServer2_Entity_attribute_linktable_D}</summary>
        Linktable=153,
        /// <summary>${iServer2_Entity_attribute_wcs_D}</summary>
        WCS= 87,
        /// <summary>${iServer2_Entity_attribute_wms_D}</summary>
        WMS=86
    }
}
