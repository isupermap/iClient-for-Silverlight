﻿

namespace SuperMap.Web.iServerJava2
{
    /// <summary>${iServer2_ServerLayerSettingType_Title}</summary>
    public enum ServerLayerSettingType
    {
        //Undefined = -1,
        /// <summary>${iServer2_ServerLayerSettingType_attribute_superMap_D}</summary>
        Supermap = 1,
        /// <summary>${iServer2_ServerLayerSettingType_attribute_wms_D}</summary>
        Wms = 2,
        //Wfs = 3,
        //Googlemap = 4,
        //Kml = 5,
        //Yahoomap = 6,
        //Virtualearthmap = 7,
        //Georss = 8,
        //Collection = 11,
        /// <summary>${iServer2_ServerLayerSettingType_attribute_supermapcollection_D}</summary>
        Supermapcollection = 12
    }
}
