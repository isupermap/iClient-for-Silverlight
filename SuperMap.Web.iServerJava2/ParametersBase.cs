﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>
    ///     ${iServer2_paramertersBase_Title}</para>${iServer2_ParamertersBase_Description}
    /// </summary>
    public abstract class ParametersBase
    {
        /// <summary>${iServer2_paramertersBase_attribute_mapName_D}</summary>
        public string MapName { get; set; }
    }
}
