﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SpatialOperationType_Title}</para>
    /// 	<para>${iServer2_SpatialOperationType_Description}</para>
    /// </summary>
    public enum SpatialOperationType
    {
        /// <summary>
        /// 	<para>${iServer2_SpatialOperationType_attribute_Clip_D}</para>
        /// 	<para><img src="Clip.bmp"/></para>
        /// </summary>
        Clip=1,
        /// <summary>
        /// 	<para>${iServer2_SpatialOperationType_attribute_Erase_D}</para>
        /// 	<para><img src="esrase.bmp"/></para>
        /// </summary>
        Erase=2,
        /// <summary>
        /// 	<para>${iServer2_SpatialOperationType_attribute_Identity_D}</para>
        /// 	<para><img src="identity.bmp"/></para>
        /// </summary>
        Identity=3,
        /// <summary>
        /// 	<para>${iServer2_SpatialOperationType_attribute_Intersection_D}</para>
        /// 	<para><img src="intersect.bmp"/></para>
        /// </summary>
        Intersection=4,
        /// <summary>
        /// 	<para>${iServer2_SpatialOperationType_attribute_Xor_D}</para>
        /// 	<para><img src="XOR.bmp"/></para>
        /// </summary>
        Xor=5,
        /// <summary>
        /// 	<para>${iServer2_SpatialOperationType_attribute_Union_D}</para>
        /// 	<para><img src="union.bmp"/></para>
        /// </summary>
        Union=6
    }
}
