﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SpatialOperateService_Title}</para>
    /// 	<para>${iServer2_SpatialOperateServicee_Description}</para>
    /// </summary>
    public class SpatialOperateService : ServiceBase
    {
        /// <overloads>${iServer2_SpatialOperateService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_SpatialOperateService_constructor_None_D}</summary>
        public SpatialOperateService()
        { }
        /// <summary>${iServer2_SpatialOperateService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_SpatialOperateService_constructor_String_param_url}</param>
        public SpatialOperateService(string url)
            : base(url)
        { }

        /// <overloads>${iServer2_SpatialOperateService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_SpatialOperateService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_SpatialOperateService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(SpatialOperateParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_SpatialOperateService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_SpatialOperateService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_SpatialOperateService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(SpatialOperateParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("SpatialOperateParameters is Null");
                throw new ArgumentNullException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);

        }

        private Dictionary<string, string> GetParameters(SpatialOperateParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "spatialOperate";

            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);
            dict.Add("sourceGeometry", ServerGeometry.ToJson(parameters.SourceGeometry));
            dict.Add("operatorGeometry", ServerGeometry.ToJson(parameters.OperatorGeometry));
            dict.Add("spatialOperationType", ((int)parameters.SpatialOperationType).ToString());
            dictionary.Add("params", Bridge.CreateParams(method, dict));

            return dictionary;
        }
        private ServerGeometry lastResult;

        /// <summary>${iServer2_SpatialOperateService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<SpatialOperateEventArgs> ProcessCompleted;
        private void OnProcessCompleted(SpatialOperateEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(
                    ProcessCompleted, new object[] { this, args });
            }
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            ServerGeometry result = ServerGeometry.FromJson(jsonObject);
            LastResult = result;
            SpatialOperateEventArgs args = new SpatialOperateEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        /// <summary>${iServer2_SpatialOperateService_attribute_lastResult_D}</summary>
        public ServerGeometry LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
