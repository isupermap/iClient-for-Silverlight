﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SpatialOperateParameters_Title}</para>
    /// 	<para>${iServer2_SpatialOperateParameters_Description}</para>
    /// </summary>
    public class SpatialOperateParameters : ParametersBase
    {
        /// <summary>${iServer2_SpatialOperateParameters_constructor_None_D}</summary>
        public SpatialOperateParameters()
        { }
        /// <summary>${iServer2_SpatialOperateParameters_attribute_sourceGeometry_D}</summary>
        public ServerGeometry SourceGeometry { get; set; }
        /// <summary>${iServer2_SpatialOperateParameters_attribute_operateGeometry_D}</summary>
        public ServerGeometry OperatorGeometry { get; set; }
        /// <summary>${iServer2_SpatialOperateParameters_attribute_spatialOperationType_D}</summary>
        public SpatialOperationType SpatialOperationType { get; set; }

        //应该还有怎样处理的接口；
    }
}
