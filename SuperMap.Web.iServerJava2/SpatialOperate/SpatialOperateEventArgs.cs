﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SpatialOperateEventArgs_Title}</para>
    /// 	<para>${iServer2_SpatialOperateEventArgs_Description}</para>
    /// </summary>
    public class SpatialOperateEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer2_SpatialOperateEventArgs_constructor_D}</summary>
        public SpatialOperateEventArgs(ServerGeometry result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServer2_SpatialOperateEventArgs_attribute_Result_D}</summary>
        public ServerGeometry Result { get; private set; }
        /// <summary>${iServer2_SpatialOperateEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
