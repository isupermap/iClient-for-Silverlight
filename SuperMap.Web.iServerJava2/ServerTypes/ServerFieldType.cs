﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ServerFieldType_Title}</para>
    /// 	<para>${iServer2_ServerFieldType_Description}</para>
    /// </summary>
    public enum ServerFieldType
    {
        /// <summary>${iServer2_ServerFieldType_attribute_unknown_D}</summary>
        Unknown=-1,
        /// <summary>${iServer2_ServerFieldType_attribute_BOOLEAN_D}</summary>
        Boolean = 1,
        /// <summary>${iServer2_ServerFieldType_attribute_BYTE_D}</summary>
        Byte = 2,
        /// <summary>${iServer2_ServerFieldType_attribute_INT16_D}</summary>
        Int16 = 3,
        /// <summary>${iServer2_ServerFieldType_attribute_INT32_D}</summary>
        Int32 = 4,
        /// <summary>${iServer2_ServerFieldType_attribute_CURRENCY_D}</summary>
        Currency = 5,
        /// <summary>${iServer2_ServerFieldType_attribute_SINGLE_D}</summary>
        Single = 6,
        /// <summary>${iServer2_ServerFieldType_attribute_DOUBLE_D}</summary>
        Double = 7,
        /// <summary>${iServer2_ServerFieldType_attribute_TEXT_D}</summary>
        Text = 10,
        /// <summary>${iServer2_ServerFieldType_attribute_LONGBINARY_D}</summary>
        LongBinary = 11,
        /// <summary>${iServer2_ServerFieldType_attribute_INT64_D}</summary>
        Int64 = 16,
        /// <summary>${iServer2_ServerFieldType_attribute_DECIMAL_D}</summary>
        DecimalL = 20,
        /// <summary>${iServer2_ServerFieldType_attribute_DATETIME_D}</summary>
        DateTime = 23,
        /// <summary>${iServer2_ServerFieldType_attribute_NVARCHAR_D}</summary>
        NvarChar = 127,
    }
}
