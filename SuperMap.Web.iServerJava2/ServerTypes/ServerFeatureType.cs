﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ServerFeatureType_Title}</para>
    /// 	<para>${iServer2_ServerFeatureType_Description}</para>
    /// </summary>
    public enum ServerFeatureType
    {
        /// <summary>${iServer2_ServerFeatureType_attribute_unknown_D}</summary>
        Unknown = -1,
        /// <summary>${iServer2_ServerFeatureType_attribute_point_D}</summary>
        Point = 1,
        /// <summary>${iServer2_ServerFeatureType_attribute_line_D}</summary>
        Line = 3,
        /// <summary>${iServer2_ServerFeatureType_attribute_polygon_D}</summary>
        Polygon = 5,
        /// <summary>${iServer2_ServerFeatureType_attribute_Text_D}</summary>
        Text = 7,
        /// <summary>${iServer2_ServerFeatureType_attribute_lineM_D}</summary>
        LineM= 35
        //linem 路线要素类型。默认值为35。 
    }
}
