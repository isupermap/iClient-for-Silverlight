﻿using System;
using System.Security;
using System.Text;
using System.Windows;
using SuperMap.Web.Core;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// 	<para>${mapping_TiledDynamicIServerLayer_Title}</para>
    /// 	<para>${mapping_TiledDynamicIServerLayer_Description}</para>
    /// </summary>
    public class TiledDynamicIServerLayer : TiledDynamicLayer//, ISuperMapLayer
    {
        private bool isInitializing;
        private SmMapService mapService;
        private string uriFormat;

        /// <summary>${mapping_TiledDynamicLayer_constructor_None_D}</summary>
        public TiledDynamicIServerLayer()
        {
            ImageFormat = "png";
            Transparent = false;
            TileSize = 512;
            MapServicePort = int.MinValue;
            AdjustFactor = 1.0;
        }

        /// <summary>${mapping_TiledDynamicLayer_method_getTileUrl_D}</summary>
        /// <returns>${mapping_TiledDynamicLayer_method_getTileUrl_param_return}</returns>
        /// <param name="indexX">${mapping_TiledDynamicLayer_method_getTileUrl_param_indexX}</param>
        /// <param name="indexY">${mapping_TiledDynamicLayer_method_getTileUrl_param_indexY}</param>
        /// <param name="resolution">${mapping_TiledDynamicLayer_method_getTileUrl_param_resolution}</param>
        public override string GetTileUrl(int indexX, int indexY, double resolution)
        {
            string uri = string.Empty;
            if (!string.IsNullOrEmpty(this.uriFormat))
            {
                double scale = ScaleHelper.ScaleConversion(resolution, this.Dpi);
                uri = String.Format(this.uriFormat, scale, indexX, indexY);
                if (LayersKey != "0")
                {
                    uri += string.Format("&layersKey={0}", LayersKey);
                }
                else
                {
                    uri += "&layersKey=0";
                }
                uri += string.Format("&t={0}", DateTime.Now.Ticks.ToString());
            }
            return uri;
        }

        /// <summary>${mapping_TiledDynamicIServerLayer_method_initialize_D}</summary>
        public override void Initialize()
        {
            if (!this.isInitializing && !base.IsInitialized)
            {
                #region 必设参数判断
                if (string.IsNullOrEmpty(this.Url))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.InvalidUrl);
                    base.Initialize();
                    return;
                }
                if (string.IsNullOrEmpty(this.MapName))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.MapNameIsNull);
                    base.Initialize();
                    return;
                }
                if (string.IsNullOrEmpty(this.MapServiceAddress))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.ServiceAddressError);
                    base.Initialize();
                    return;
                }
                if (MapServicePort == int.MaxValue)
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.ServicePortIsNull);
                    base.Initialize();
                    return;
                }
                #endregion

                if (!Url.Contains("http://"))  //相对地址
                {
                    var pageUrl = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                    var localUrl = pageUrl.AbsoluteUri.Substring(0, pageUrl.AbsoluteUri.IndexOf(pageUrl.AbsolutePath));
                    Url = localUrl + Url;
                }

                if (IsSkipGetSMMapServiceInfo)
                {
                    if (Bounds.IsEmpty)
                    {
                        Error = new ArgumentNullException("Bounds");
                    }
                    Dpi = ScaleHelper.GetSmDpi(ReferViewBounds, ReferViewer, ReferScale);
                    Dpi *= AdjustFactor;

                    this.isInitializing = true;

                    base.Initialize();
                    CreateUrl();
                    return;
                }

                this.isInitializing = true;
                this.mapService = new SmMapService(this.Url.Trim(), this.MapName, this.MapServiceAddress, this.MapServicePort);
                this.mapService.Initialized += new EventHandler<SmMapService.MapServiceInitalizeArgs>(mapService_Initialized);
                this.mapService.Failed += new SmMapService.MapServiceFaultEventHandler(mapService_Fault);
                this.mapService.Initialize();
                CreateUrl();
            }
        }

        private void CreateUrl()
        {

            #region 构造真正的URL
            //http://localhost:7080/demo/maphandler?&method=gettiledimage&mapName=World&x=1&y=1&imageFormat=png&width=512&height=512&mapScale=0.0009286998562018231&layersKey=0
            StringBuilder builder = new StringBuilder();

            builder.Append(this.Url);

            if (!Url.EndsWith("/"))
            {
                builder.Append("/");
            }

            builder.Append("maphandler?&method=gettiledimage");
            builder.AppendFormat("&mapName={0}", this.MapName);
            builder.AppendFormat("&width={0}&height={0}", TileSize);
            builder.AppendFormat("&transparent={0}", Transparent);
            builder.AppendFormat("&imageFormat={0}", ImageFormat.ToString().ToLower());
            builder.Append("&mapScale={0}&x={1}&y={2}");
            #endregion

            this.uriFormat = builder.ToString();
        }

        private void mapService_Fault(object sender, SmMapService.MapServiceFaultEventArgs args)
        {
            if (((args.Error is SecurityException) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttp)) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttps))
            {
                base.Error = new SecurityException(SuperMap.Web.Resources.ExceptionStrings.InvalidURISchemeHost, args.Error);
            }
            else
            {
                base.Error = args.Error;
            }
            base.Initialize();
        }

        private void mapService_Initialized(object sender, SmMapService.MapServiceInitalizeArgs e)
        {
            if (e.MapService.MapServiceUrl == this.Url.Trim())
            {
                if (base.Error != null)
                {
                    base.Initialize();
                }
                else
                {
                    Rectangle2D rect = this.mapService.MapServiceInfo.MapBounds;
                    Bounds = rect;
                    if (CRS == null)
                    {
                        CRS = this.mapService.MapServiceInfo.CRS;
                    }
                    Dpi = ScaleHelper.GetSmDpi(this.mapService.MapServiceInfo.ReferViewBounds, this.mapService.MapServiceInfo.ReferViewer, this.mapService.MapServiceInfo.ReferScale);
                    Dpi *= AdjustFactor;
                    base.Initialize();
                }
            }
        }



        private string layersKey = "0";
        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_layersKey_D}</summary>
        public string LayersKey
        {
            get
            {
                return layersKey;
            }
            set
            {
                if (layersKey != value)
                {
                    layersKey = value;
                    if (base.IsInitialized || isInitializing)
                    {
                        base.IsInitialized = false;
                        isInitializing = false;
                        base.Error = null;
                        base.Refresh();
                        this.Initialize();
                    }
                }
            }
        }

        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_mapName_D}</summary>
        public string MapName { get; set; }

        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_mapServiceAddress_D}</summary>
        public string MapServiceAddress { get; set; }
        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_mapServicePort_D}</summary>
        public int MapServicePort { get; set; }
        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_dpi_D}</summary>        
        protected override double Dpi { get; set; }
        /// <summary>${mapping_DynamicIServerLayer_attribute_ajustFactor_D}</summary>
        public double AdjustFactor { get; set; }

        /// <summary>${Mapping_DynamicRESTLayer_attribute_isScaleCentric_D}</summary>
        public override bool IsScaleCentric
        {
            get
            {
                return true;
            }
        }

        //需要已知Bounds、DPI；
        //IsSkipGetSMMapServiceInfo默认是false，
        //如果设置为true，则Bounds、ReferViewBounds、ReferViewer、ReferScale必须设置；
        //CRS此时为空，要用到与CRS相关时，需要设置CRS；
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_IsSkipGetSMMapServiceInfo_D}</summary>
        public bool IsSkipGetSMMapServiceInfo { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewBounds_D}</summary>
        public Rectangle2D ReferViewBounds { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewer_D}</summary>
        public Rect ReferViewer { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferScale_D}</summary>
        public double ReferScale { get; set; }
    }
}
