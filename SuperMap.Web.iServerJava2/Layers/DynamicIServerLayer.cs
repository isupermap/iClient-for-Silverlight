﻿using System;
using System.Security;
using System.Text;
using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// 	<para>${mapping_DynamicIServerLayer_Title}</para>
    /// 	<para>${mapping_DynamicIServerLayer_Description}</para>
    /// </summary>
    public class DynamicIServerLayer : DynamicLayer
    {
        private bool isInitializing;
        private SmMapService mapService;

        /// <summary>${mapping_DynamicIServerLayer_constructor_None_D}</summary>
        public DynamicIServerLayer()
            : base()
        {
            ImageFormat = "png";
            MapServicePort = int.MinValue;
            AdjustFactor = 1.0;
        }

        /// <summary>${mapping_Layer_method_initialize_D}</summary>
        public override void Initialize()
        {
            if (!this.isInitializing && !base.IsInitialized)
            {
                #region 必设参数判断
                if (string.IsNullOrEmpty(this.Url))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.InvalidUrl);
                    base.Initialize();
                    return;
                }
                if (string.IsNullOrEmpty(this.MapName))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.MapNameIsNull);
                    base.Initialize();
                    return;
                }
                if (string.IsNullOrEmpty(this.MapServiceAddress))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.ServiceAddressError);
                    base.Initialize();
                    return;
                }
                if (MapServicePort == int.MaxValue)
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.ServicePortIsNull);
                    base.Initialize();
                    return;
                }
                #endregion

                if (!Url.Contains("http://"))  //相对地址
                {
                    var pageUrl = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                    var localUrl = pageUrl.AbsoluteUri.Substring(0, pageUrl.AbsoluteUri.IndexOf(pageUrl.AbsolutePath));
                    Url = localUrl + Url;
                }

                if (IsSkipGetSMMapServiceInfo)
                {
                    if (Bounds.IsEmpty)
                    {
                        Error = new ArgumentNullException("Bounds");
                    }
                    Dpi = ScaleHelper.GetSmDpi(ReferViewBounds, ReferViewer, ReferScale);
                    Dpi *= AdjustFactor;

                    this.isInitializing = true;

                    base.Initialize();
                    return;
                }

                this.isInitializing = true;
                this.mapService = new SmMapService(this.Url.Trim(), this.MapName, this.MapServiceAddress, this.MapServicePort);
                this.mapService.Initialized += new EventHandler<SmMapService.MapServiceInitalizeArgs>(mapService_Initialized);
                this.mapService.Failed += new SmMapService.MapServiceFaultEventHandler(mapService_Fault);
                this.mapService.Initialize();
            }
        }

        private void mapService_Fault(object sender, SmMapService.MapServiceFaultEventArgs args)
        {
            if (((args.Error is SecurityException) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttp)) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttps))
            {
                base.Error = new SecurityException(SuperMap.Web.Resources.ExceptionStrings.InvalidURISchemeHost, args.Error);
            }
            else
            {
                base.Error = args.Error;
            }
            base.Initialize();
        }

        private void mapService_Initialized(object sender, SmMapService.MapServiceInitalizeArgs e)
        {
            if (e.MapService.MapServiceUrl == this.Url.Trim())
            {
                if (base.Error != null)
                {
                    base.Initialize();
                }
                else
                {
                    this.Bounds = this.mapService.MapServiceInfo.MapBounds;
                    if (CRS == null)
                    {
                        CRS = this.mapService.MapServiceInfo.CRS;
                    }
                    this.Dpi = ScaleHelper.GetSmDpi(this.mapService.MapServiceInfo.ReferViewBounds, this.mapService.MapServiceInfo.ReferViewer, this.mapService.MapServiceInfo.ReferScale);
                    Dpi *= AdjustFactor;
                    base.Initialize();
                }
            }
        }


        //http://192.168.11.11:7080/demo/maphandler?method=getImage&mapName=World&width=1276&height=827&imageFormat=png&mapCenterX=0&mapCenterY=0&
        //mapScale=1.06132490146981E-08&layersKey=0&transparent=False&t=633988258772777998
        /// <summary>${mapping_DynamicImageLayer_method_getImageUrl_D}</summary>
        protected override string GetImageUrl()
        {
            if (this.Resolution < 0 || this.Dpi < 0)
            {
                return null;
            }

            double scale = ScaleHelper.ScaleConversion(this.Resolution, this.Dpi); ;

            Rectangle2D bounds = ViewBounds;
            int width = (int)Math.Round(ViewSize.Width);
            int height = (int)Math.Round(ViewSize.Height);

            StringBuilder imgUrl = new StringBuilder();
            imgUrl.Append(Url);
            if (!Url.EndsWith("/"))
            {
                imgUrl.Append("/");
            }
            imgUrl.Append("maphandler?method=getImage");
            imgUrl.AppendFormat("&mapName={0}", MapName);
            imgUrl.AppendFormat("&width={0}", width);
            imgUrl.AppendFormat("&height={0}", height);
            imgUrl.AppendFormat("&imageFormat={0}", ImageFormat);
            imgUrl.AppendFormat("&mapCenterX={0}", bounds.Center.X);
            imgUrl.AppendFormat("&mapCenterY={0}", bounds.Center.Y);
            imgUrl.AppendFormat("&mapScale={0}", scale);
            imgUrl.AppendFormat("&layersKey={0}", LayersKey);
            imgUrl.AppendFormat("&transparent={0}", Transparent);
            imgUrl.AppendFormat("&t={0}", DateTime.Now.Ticks);
            return imgUrl.ToString();
        }

        private string layersKey = "0";
        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_layersKey_D}</summary>
        public string LayersKey
        {
            get
            {
                return layersKey;
            }
            set
            {
                if (layersKey != value)
                {
                    layersKey = value;
                    if (base.IsInitialized || isInitializing)
                    {
                        base.IsInitialized = false;
                        isInitializing = false;
                        base.Error = null;
                        base.Refresh();
                        this.Initialize();
                    }
                }
            }
        }

        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_mapName_D}</summary>
        public string MapName { get; set; }
        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_mapServiceAddress_D}</summary>
        public string MapServiceAddress { get; set; }
        /// <summary>${mapping_TiledDynamicIServerLayer_attribute_mapServicePort_D}</summary>
        public int MapServicePort { get; set; }
        /// <summary>${mapping_TiledDynamicISLayer_attribute_dpi_D}</summary>
        protected override double Dpi { get; set; }
        /// <summary>${mapping_DynamicIServerLayer_attribute_ajustFactor_D}</summary>
        public double AdjustFactor { get; set; }

        /// <summary>${Mapping_DynamicRESTLayer_attribute_isScaleCentric_D}</summary>
        public override bool IsScaleCentric
        {
            get
            {
                return true;
            }
        }

        //IsSkipGetSMMapServiceInfo默认是false，
        //如果设置为true，则Bounds、ReferViewBounds、ReferViewer、ReferScale必须设置；
        //CRS此时为空，要用到与CRS相关时，需要设置CRS；
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_IsSkipGetSMMapServiceInfo_D}</summary>
        public bool IsSkipGetSMMapServiceInfo { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewBounds_D}</summary>
        public Rectangle2D ReferViewBounds { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewer_D}</summary>
        public Rect ReferViewer { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferScale_D}</summary>
        public double ReferScale { get; set; }
    }
}
