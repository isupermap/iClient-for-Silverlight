﻿using System;
using System.ComponentModel;
using System.Security;
using System.Text;
using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;
using System.Collections.Generic;
using SuperMap.Web.iServerJava2.Resources;

namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// ${mapping_TiledCachedIServerLayer_Title}<br/>
    /// ${mapping_TiledCachedIServerLayer_Description}
    /// </summary>
    public class TiledCachedIServerLayer : TiledCachedLayer//, ISuperMapLayer
    {
        private bool isInitializing;
        private SmMapService mapService;
        private string uriFormat;

        /// <summary>
        ///     ${pubilc_Constructors_Initializes} <see cref="TiledCachedIServerLayer">TiledCahedIServerLayer</see>
        ///     ${pubilc_Constructors_instance}
        /// </summary>
        public TiledCachedIServerLayer()
        {
            ImageFormat = "png";
            TileSize = 512;
            MapServicePort = int.MinValue;
            AdjustFactor = 1.0;
        }

        /// <summary>${mapping_TiledCachedLayer_method_getTileUrl_D}</summary>
        /// <returns>${mapping_TiledCachedLayer_method_getTileUrl_returns}</returns>
        /// <param name="indexX">${mapping_TiledCachedLayer_method_getTileUrl_param_indexX}</param>
        /// <param name="indexY">${mapping_TiledCachedLayer_method_getTileUrl_param_indexY}</param>
        /// <param name="level">${mapping_TiledCachedLayer_method_getTileUrl_param_level}</param>
        public override string GetTileUrl(int indexX, int indexY, int level)
        {
            string uri = string.Empty;
            if (!string.IsNullOrEmpty(this.uriFormat))//&& (this.Visibility == Visibility.Visible)
            {
                double scale = 1 / Scales[level];
                uri = String.Format(this.uriFormat, scale, indexX, indexY);
            }
            return uri;
        }

        /// <summary>${mapping_Layer_method_initialize_D}</summary>
        public override void Initialize()
        {
            if (!this.isInitializing && !base.IsInitialized)
            {
                #region 必设参数判断
                if (string.IsNullOrEmpty(this.Url))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.InvalidUrl);
                    base.Initialize();
                    return;
                }
                if (string.IsNullOrEmpty(this.MapName))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.MapNameIsNull);
                    base.Initialize();
                    return;
                }
                if (string.IsNullOrEmpty(this.MapServiceAddress))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.ServiceAddressError);
                    base.Initialize();
                    return;
                }
                if (MapServicePort == int.MaxValue)
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.ServicePortIsNull);
                    base.Initialize();
                    return;
                }
                if (this.Scales == null)
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.ScalesIsNull);
                    base.Initialize();
                    return;
                }//如果是缓存必需设置Scales
                if (string.IsNullOrEmpty(CachedUrl))
                {
                    base.Error = new ArgumentNullException(ExceptionStrings.CachedUrlIsNull);
                    base.Initialize();
                    return;
                }
                #endregion

                if (!Url.Contains("http://"))  //相对地址
                {
                    var pageUrl = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                    var localUrl = pageUrl.AbsoluteUri.Substring(0, pageUrl.AbsoluteUri.IndexOf(pageUrl.AbsolutePath));
                    Url = localUrl + Url;
                }

                if (IsSkipGetSMMapServiceInfo)
                {
                    if (Bounds.IsEmpty)
                    {
                        Error = new ArgumentNullException("Bounds");
                    }
                    Dpi = ScaleHelper.GetSmDpi(ReferViewBounds, ReferViewer, ReferScale);
                    Dpi *= AdjustFactor;
                    double[] resolutions = new double[Scales.Length];
                    for (int i = 0; i < Scales.Length; i++)
                    {
                        resolutions[i] = ScaleHelper.ScaleConversion(Scales[i], this.Dpi);
                    }
                    this.Resolutions = resolutions;

                    this.isInitializing = true;

                    base.Initialize();
                    CreateUrl();
                    return;
                }

                this.isInitializing = true;
                this.mapService = new SmMapService(this.Url.Trim(), this.MapName, this.MapServiceAddress, this.MapServicePort);
                this.mapService.Initialized += new EventHandler<SmMapService.MapServiceInitalizeArgs>(mapService_Initialized);
                this.mapService.Failed += new SmMapService.MapServiceFaultEventHandler(mapService_Fault);
                this.mapService.Initialize();

                CreateUrl();
            }
        }

        private void CreateUrl()
        {
            #region 构造真正的URL
            //http://localhost:7080/output/World_512X512/10000/1/1.png
            StringBuilder builder = new StringBuilder();
            builder.Append(this.CachedUrl);
            builder.AppendFormat("/{0}_{1}x{1}", this.MapName, this.TileSize);
            builder.Append("/{0}/{1}/{2}.");
            builder.AppendFormat("{0}", ImageFormat);

            #endregion

            this.uriFormat = builder.ToString();
        }

        private void mapService_Fault(object sender, SmMapService.MapServiceFaultEventArgs args)
        {
            if (((args.Error is SecurityException) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttp)) && !Application.Current.Host.Source.Scheme.Equals(Uri.UriSchemeHttps))
            {
                base.Error = new SecurityException(SuperMap.Web.Resources.ExceptionStrings.InvalidURISchemeHost, args.Error);
            }
            else
            {
                base.Error = args.Error;
            }
            base.Initialize();
        }

        private void mapService_Initialized(object sender, SmMapService.MapServiceInitalizeArgs e)
        {
            if (e.MapService.MapServiceUrl == this.Url.Trim())
            {
                if (base.Error != null)
                {
                    base.Initialize();
                }
                else
                {
                    Bounds = this.mapService.MapServiceInfo.MapBounds;
                    if (CRS == null)
                    {
                        CRS = this.mapService.MapServiceInfo.CRS;
                    }

                    Dpi = ScaleHelper.GetSmDpi(this.mapService.MapServiceInfo.ReferViewBounds, this.mapService.MapServiceInfo.ReferViewer, this.mapService.MapServiceInfo.ReferScale);
                    Dpi *= AdjustFactor;
                    //在这儿把Scales转换为Resolutions, Dpi已经获得值了
                    double[] resolutions = new double[Scales.Length];
                    for (int i = 0; i < Scales.Length; i++)
                    {
                        resolutions[i] = ScaleHelper.ScaleConversion(Scales[i], this.Dpi);
                    }
                    this.Resolutions = resolutions;

                    base.Initialize();
                }
            }
        }


        //属性
        /// <summary>${mapping_TiledCachedIServerLayer_attribute_scales_D}</summary>
        private double[] scales = null;
        /// <summary>${mapping_TiledCachedIServerLayer_attribute_scales_D}</summary>
        [TypeConverter(typeof(DoubleArrayConverter))]
        public double[] Scales
        {
            get { return scales; }
            set
            {
                if (value[0] > 1)
                {
                    for (int i = 0; i < value.Length; i++)
                    {
                        value[i] = 1.0 / value[i];
                    }
                }
                scales = value;
            }
        }

        /// <summary>${mapping_TiledCachedIServerLayer_attribute_CachedUrl_D}</summary>
        public string CachedUrl { get; set; }
        /// <summary>${mapping_TiledCachedIServerLayer_attribute_MapName_D}</summary>
        public string MapName { get; set; }
        /// <summary>${mapping_TiledCachedIServerLayer_attribute_mapServiceAddress_D}</summary>
        public string MapServiceAddress { get; set; }
        /// <summary>${mapping_TiledCachedIServerLayer_attribute_mapServicePort_D}</summary>
        public int MapServicePort { get; set; }
        /// <summary>${mapping_TiledCachedIServerLayer_attribute_dpi_D}</summary>
        protected override double Dpi { get; set; }
        /// <summary>${mapping_DynamicIServerLayer_attribute_ajustFactor_D}</summary>
        public double AdjustFactor { get; set; }        
        /// <summary>${Mapping_DynamicRESTLayer_attribute_isScaleCentric_D}</summary>
        public override bool IsScaleCentric
        {
            get
            {
                return true;
            }
        }

        //IsSkipGetSMMapServiceInfo默认是false，
        //如果设置为true，则Bounds、ReferViewBounds、ReferViewer、ReferScale必须设置；
        //CRS此时为空，要用到与CRS相关时，需要设置CRS；
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_IsSkipGetSMMapServiceInfo_D}</summary>
        public bool IsSkipGetSMMapServiceInfo { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewBounds_D}</summary>
        public Rectangle2D ReferViewBounds { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferViewer_D}</summary>
        public Rect ReferViewer { get; set; }
        /// <summary>${Mapping_TiledDynamicRESTLayer_attribute_ReferScale_D}</summary>
        public double ReferScale { get; set; }
    }
}
