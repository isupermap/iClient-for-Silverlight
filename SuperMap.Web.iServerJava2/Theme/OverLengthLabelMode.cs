﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>${iServer2_Theme_OverLengthLabelMode_Title}</summary>
    public enum OverLengthLabelMode
    {
        /// <summary>${iServer2_Theme_OverLengthLabelMode_attribute_none_D}</summary>
        None = 0,
        /// <summary>${iServer2_Theme_OverLengthLabelMode_attribute_omit_D}</summary>
        Omit = 1,
        /// <summary>${iServer2_Theme_OverLengthLabelMode_attribute_newLine_D}</summary>
        Newline = 2
    }
}
