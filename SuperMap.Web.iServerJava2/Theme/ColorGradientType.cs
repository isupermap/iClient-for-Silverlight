﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ColorGradientType_Title}</para>
    /// 	<para>${iServer2_Theme_ColorGradientType_Description}</para>
    /// </summary>
    public enum ColorGradientType
    {
        /// <summary>${iServer2_Theme_attribute_BlackWhite_D}</summary>
        BlackWhite = 0,
        /// <summary>${iServer2_Theme_attribute_RedWhite_D}</summary>
        RedWhite = 1,
        /// <summary>${iServer2_Theme_attribute_GreenWhite_D}</summary>
        GreenWhite = 2,
        /// <summary>${iServer2_Theme_attribute_BlueWhite_D}</summary>
        BlueWhite = 3,
        /// <summary>${iServer2_Theme_attribute_YellowWhite_D}</summary>
        YellowWhite = 4,
        /// <summary>${iServer2_Theme_attribute_PinkWhite_D}</summary>
        PinkWhite = 5,
        /// <summary>${iServer2_Theme_attribute_CyanWhite_D}</summary>
        CyanWhite = 6,
        /// <summary>${iServer2_Theme_attribute_RedBlack_D}</summary>
        RedBlack = 7,
        /// <summary>${iServer2_Theme_attribute_GreenBlack_D}</summary>
        GreenBlack = 8,
        /// <summary>${iServer2_Theme_attribute_BlueBlack_D}</summary>
        BlueBlack = 9,
        /// <summary>${iServer2_Theme_attribute_YellowBlack_D}</summary>
        YellowBlack = 10,
        /// <summary>${iServer2_Theme_attribute_PinkBlack_D}</summary>
        PinkBlack = 11,
        /// <summary>${iServer2_Theme_attribute_CyanBlack_D}</summary>
        CyanBlack = 12,
        /// <summary>${iServer2_Theme_attribute_YellowRed_D}</summary>
        YellowRed=13,
        /// <summary>${iServer2_Theme_attribute_YellowGreen_D}</summary>
        YellowGreen=14,
        /// <summary>${iServer2_Theme_attribute_YellowBlue_D}</summary>
        YellowBlue=15,
        /// <summary>${iServer2_Theme_attribute_GreenBlue_D}</summary>
        GreenBlue=16,
        /// <summary>${iServer2_Theme_attribute_GreenRed_D}</summary>
        GreenRed=17,
        /// <summary>${iServer2_Theme_attribute_BlueRed_D}</summary>
        BlueRed=18,
        /// <summary>${iServer2_Theme_attribute_PinkRed_D}</summary>
        PinkRed=19,
        /// <summary>${iServer2_Theme_attribute_PinkBlue_D}</summary>
        PinkBlue=20,
        /// <summary>${iServer2_Theme_attribute_CyanBlue_D}</summary>
        CyanBlue=21,
        /// <summary>${iServer2_Theme_attribute_CyanGreen_D}</summary>
        CyanGreen=22,
        /// <summary>${iServer2_Theme_attribute_RainBow_D}</summary>
        RainBow=23,
        /// <summary>${iServer2_Theme_attribute_GreenOrangeViolet_D}</summary>
        GreenOrangeViolet=24,
        /// <summary>${iServer2_Theme_attribute_Terrain_D}</summary>
        Terrain=25,
        /// <summary>${iServer2_Theme_attribute_Spectrum_D}</summary>
        Spectrum=26
    }
}
