﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_FillGradientMode_Title}</para>
    /// 	<para>${iServer2_Theme_FillGradientMode_Description}</para>
    /// </summary>
    public enum FillGradientMode
    {
        /// <summary>${iServer2_Theme_FillGradientMode_attribute_None_D}</summary>
        None = 0,
        /// <summary>
        /// 	<para>${iServer2_Theme_FillGradientMode_attribute_Linear_D}</para>
        /// 	<para><img src="fillGradientModeLinear.bmp"/></para>
        /// </summary>
        Linear = 1,
        /// <summary>
        /// 	<para>${iServer2_Theme_FillGradientMode_attribute_Radial_D}</para>
        /// 	<para><img src="fillGradientModeRadial.bmp"/></para>
        /// </summary>
        Radial = 2,
        /// <summary>
        /// 	<para>${iServer2_Theme_FillGradientMode_attribute_Conical_D}</para>
        /// 	<para><img src="fillGradientModeConical.bmp"/></para>
        /// </summary>
        Conical = 3,
        /// <summary>
        /// 	<para>${IS6_BrushGradientMode_attribute_Square_D}</para>
        /// 	<para><img src="fillGradientModeSquare.bmp"/></para>
        /// </summary>
        Square = 4,
    }
}
