﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_GraphTextFormat_Title}</para>
    /// 	<para>${iServer2_Theme_GraphTextFormat_Description}</para>
    /// </summary>
    public enum GraphTextFormat
    {
        /// <summary>
        /// 	<para>${IS6_GraphTextFormat_attribute_TextPercent_D}</para>
        /// 	<para><img src="GraphThemePercent.bmp"/></para>
        /// </summary>
        Percent=1,
        /// <summary>
        /// 	<para>${IS6_GraphTextFormat_attribute_TextValue_D}</para>
        /// 	<para><img src="GraphThemeValue.bmp"/></para>
        /// </summary>
        Value=2,
        /// <summary>
        /// 	<para>${IS6_GraphTextFormat_attribute_TextCaption_D}</para>
        /// 	<para><img src="GraphThemeCaption.bmp"/></para>
        /// </summary>
        Caption=3,
        /// <summary>
        /// 	<para>${IS6_GraphTextFormat_attribute_TextCaptionPercent_D}</para>
        /// 	<para><img src="GraphThemeCaption_Percent.bmp"/></para>
        /// </summary>
        Caption_Percent=4,
        /// <summary>
        /// 	<para>${IS6_GraphTextFormat_attribute_TextCaptionValue_D}</para>
        /// 	<para><img src="GraphThemeCaption_Value.bmp"/></para>
        /// </summary>
        Caption_Value=5,
    }
}
