﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_GraphType_Title}</para>
    /// 	<para>${iServer2_Theme_GraphType_Description}</para>
    /// </summary>
    public enum GraphType
    {
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Area_D}</para>
        /// 	<para><img src="GraphThemeArea.bmp"/></para>
        /// </summary>
        Area=0,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Step_D}</para>
        /// 	<para><img src="GraphThemeStep.bmp"/></para>
        /// </summary>
        Step=1,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Line_D}</para>
        /// 	<para><img src="GraphThemeLine.bmp"/></para>
        /// </summary>
        Line=2,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Point_D}</para>
        /// 	<para><img src="GraphThemePoint.bmp"/></para>
        /// </summary>
        Point=3,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Bar_D}</para>
        /// 	<para><img src="GraphThemeBar.bmp"/></para>
        /// </summary>
        Bar=4,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Bar3D_D}</para>
        /// 	<para><img src="GraphThemeBar3D.bmp"/></para>
        /// </summary>
        Bar3D=5,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Pie_D}</para>
        /// 	<para><img src="GraphThemePie.bmp"/></para>
        /// </summary>
        Pie=6,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Pie3D_D}</para>
        /// 	<para><img src="GraphThemePie3D.bmp"/></para>
        /// </summary>
        Pie3D=7,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Rose_D}</para>
        /// 	<para><img src="GraphThemeRose.bmp"/></para>
        /// </summary>
        Rose=8,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Rose3D_D}</para>
        /// 	<para><img src="GraphThemeRose3D.bmp"/></para>
        /// </summary>
        Rose3D=9,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Strack_Bar_D}</para>
        /// 	<para><img src="GraphThemeStackBar.bmp"/></para>
        /// </summary>
        Stack_Bar=12,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Stack_Bar3D_D}</para>
        /// 	<para><img src="GraphThemeStackBar3D.bmp"/></para>
        /// </summary>
        Stack_Bar3D=13,
        /// <summary>
        /// 	<para>${iServer2_Theme_GraphType_attribute_Ring_D}</para>
        /// 	<para><img src="GraphThemeRing.bmp"/></para>
        /// </summary>
        Ring=14
    }
}
