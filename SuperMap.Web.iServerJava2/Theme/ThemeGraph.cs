﻿using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeGraph_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeGraph_Description}</para>
    /// 	<para>
    /// 		<list type="table">
    /// 			<item>
    /// 				<term><img src="GraphTheme1.bmp"/></term>
    /// 				<description><img src="GraphTheme2.bmp"/></description>
    /// 			</item>
    /// 		</list>
    /// 	</para>
    /// </summary>
    public class ThemeGraph : Theme
    {
        /// <summary>${iServer2_Theme_ThemeGraph_constructor_None_D}</summary>
        public ThemeGraph()
        {
            OffsetX = "0.0";
            OffsetY = "0.0";
            LeaderLineStyle = new ServerStyle(); ;
            AxesColor = new ServerColor(0, 0, 0);
            AxesTextStyle = new ServerTextStyle();
            GraphTextStyle = new ServerTextStyle();
            GraphTextFormat = GraphTextFormat.Value;
            MinGraphSize = 40.0;
            MaxGraphSize = 60.0;
            GraduatedMode = GraduatedMode.Constant;
            GraphType = GraphType.Pie;
        }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_items_D}</summary>
        public IList<ThemeGraphItem> Items { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_offsetX_D}</summary>
        public string OffsetX { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_offsetY_D}</summary>
        public string OffsetY { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_graphType_D}</summary>
        public GraphType GraphType { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_barWidth_D}</summary>
        public double BarWidth { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_startAngle_D}</summary>
        public double StartAngle { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_roseAngle_D}</summary>
        public double RoseAngle { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_isFlowEnabled_D}</summary>
        public bool IsFlowEnabled { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_leaderLineStyle_D}</summary>
        public ServerStyle LeaderLineStyle { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_isLeaderLineDisplayed_D}</summary>
        public bool IsLeaderLineDisplayed { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_isNegativeDisplayed_D}</summary>
        public bool IsNegativeDisplayed { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_axesColor_D}</summary>
        public ServerColor AxesColor { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_isAxesDisplayed_D}</summary>
        public bool IsAxesDisplayed { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_axesTextStyle_D}</summary>
        public ServerTextStyle AxesTextStyle { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_isAxesTextDisplayed_D}</summary>
        public bool IsAxesTextDisplayed { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_isAxesGridDisplayed_D}</summary>
        public bool IsAxesGridDisplayed { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_graphTextStyle_D}</summary>
        public ServerTextStyle GraphTextStyle { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_graphTextFormat_D}</summary>
        public GraphTextFormat GraphTextFormat { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_isGraphTextDisplayed_D}</summary>
        public bool IsGraphTextDisplayed { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_minGraphSize_D}</summary>
        public double MinGraphSize { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_maxGraphSize_D}</summary>
        public double MaxGraphSize { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_isGraphSizeFixed_D}</summary>
        public bool IsGraphSizeFixed { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraph_attribute_graduatedMode_D}</summary>
        public GraduatedMode GraduatedMode { get; set; }


        internal static string ToJson(ThemeGraph themeGraph)
        {
            if (themeGraph == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (themeGraph.Items != null && themeGraph.Items.Count > 0)
            {
                List<string> tempTGI = new List<string>();
                foreach (ThemeGraphItem item in themeGraph.Items)
                {
                    tempTGI.Add(ThemeGraphItem.ToJson(item));
                }
                list.Add(string.Format("\"items\":[{0}]", string.Join(",", tempTGI.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"items\":[]"));
            }

            if (!string.IsNullOrEmpty(themeGraph.OffsetX))
            {
                list.Add(string.Format("\"offsetX\":\"{0}\"", themeGraph.OffsetX));
            }

            if (!string.IsNullOrEmpty(themeGraph.OffsetY))
            {
                list.Add(string.Format("\"offsetY\":\"{0}\"", themeGraph.OffsetY));
            }
            list.Add(string.Format("\"barWidth\":{0}", themeGraph.BarWidth));
            list.Add(string.Format("\"startAngle\":{0}", themeGraph.StartAngle));
            list.Add(string.Format("\"roseAngle\":{0}", themeGraph.RoseAngle));
            list.Add(string.Format("\"isFlowEnabled\":{0}", themeGraph.IsFlowEnabled));

            if (themeGraph.LeaderLineStyle != null)
            {
                list.Add(string.Format("\"leaderLineStyle\":{0}", ServerStyle.ToJson(themeGraph.LeaderLineStyle)));
            }
            else
            {
                list.Add(string.Format("\"leaderLineStyle\":null"));
            }

            list.Add(string.Format("\"isLeaderLineDisplayed\":{0}", themeGraph.IsLeaderLineDisplayed));
            list.Add(string.Format("\"isNegativeDisplayed\":{0}", themeGraph.IsNegativeDisplayed));
            if (ServerColor.ToJson(themeGraph.AxesColor) != null)
            {
                list.Add(string.Format("\"axesColor\":{0}", ServerColor.ToJson(themeGraph.AxesColor)));
            }
            list.Add(string.Format("\"isAxesDisplayed\":{0}", themeGraph.IsAxesDisplayed));
            if (themeGraph.AxesTextStyle != null)
            {
                list.Add(string.Format("\"axesTextStyle\":{0}", ServerTextStyle.ToJson(themeGraph.AxesTextStyle)));
            }
            list.Add(string.Format("\"isAxesTextDisplayed\":{0}", themeGraph.IsAxesTextDisplayed));
            list.Add(string.Format("\"isAxesGridDisplayed\":{0}", themeGraph.IsAxesGridDisplayed));
            if (themeGraph.GraphTextStyle != null)
            {
                list.Add(string.Format("\"graphTextStyle\":{0}", ServerTextStyle.ToJson(themeGraph.GraphTextStyle)));
            }
            list.Add(string.Format("\"graphTextFormat\":{0}", (int)themeGraph.GraphTextFormat));
            list.Add(string.Format("\"isGraphTextDisplayed\":{0}", themeGraph.IsGraphTextDisplayed));
            list.Add(string.Format("\"minGraphSize\":{0}", themeGraph.MinGraphSize));
            list.Add(string.Format("\"maxGraphSize\":{0}", themeGraph.MaxGraphSize));
            list.Add(string.Format("\"isGraphSizeFixed\":{0}", themeGraph.IsGraphSizeFixed));
            list.Add(string.Format("\"graduatedMode\":{0}", (int)themeGraph.GraduatedMode));

            list.Add(string.Format("\"themeType\":3"));

            list.Add(string.Format("\"graphType\":{0}", (int)themeGraph.GraphType));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
        /// <summary>${iServer2_ThemeGraph_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeGraph_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeGraph_method_FromJson_param_jsonObject}</param>
        public static ThemeGraph FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeGraph result = new ThemeGraph()
             {
                 AxesColor = ServerColor.FromJson((JsonObject)jsonObject["axesColor"]),
                 AxesTextStyle = ServerTextStyle.FromJson((JsonObject)jsonObject["axesTextStyle"]),
                 BarWidth = (double)jsonObject["barWidth"],
                 GraphTextStyle = ServerTextStyle.FromJson((JsonObject)jsonObject["graphTextStyle"]),
                 IsAxesDisplayed = (bool)jsonObject["isAxesDisplayed"],
                 IsAxesGridDisplayed = (bool)jsonObject["isAxesGridDisplayed"],
                 IsAxesTextDisplayed = (bool)jsonObject["isAxesTextDisplayed"],
                 IsFlowEnabled = (bool)jsonObject["isFlowEnabled"],
                 IsGraphSizeFixed = (bool)jsonObject["isGraphSizeFixed"],
                 IsGraphTextDisplayed = (bool)jsonObject["isGraphTextDisplayed"],
                 IsLeaderLineDisplayed = (bool)jsonObject["isLeaderLineDisplayed"],
                 IsNegativeDisplayed = (bool)jsonObject["isNegativeDisplayed"],
                 LeaderLineStyle = ServerStyle.FromJson((JsonObject)jsonObject["leaderLineStyle"]),
                 MaxGraphSize = (double)jsonObject["maxGraphSize"],
                 MinGraphSize = (double)jsonObject["minGraphSize"],
                 OffsetX = (string)jsonObject["offsetX"],
                 OffsetY = (string)jsonObject["offsetY"],
                 RoseAngle = (double)jsonObject["roseAngle"],
                 StartAngle = (double)jsonObject["StartAngle"],
             };

            if (jsonObject["graduatedMode"] != null)
            {
                result.GraduatedMode = (GraduatedMode)(int)jsonObject["graduatedMode"];
            }
            if (jsonObject["graphTextFormat"] != null)
            {
                result.GraphTextFormat = (GraphTextFormat)(int)jsonObject["graphTextFormat"];
            }
            if (jsonObject["graphType"] != null)
            {
                result.GraphType = (GraphType)(int)jsonObject["graphType"];
            }
            if (jsonObject["items"] != null)
            {
                result.Items = new List<ThemeGraphItem>();
                for (int i = 0; i < jsonObject["items"].Count; i++)
                {
                    result.Items.Add(ThemeGraphItem.FromJson((JsonObject)jsonObject["items"][i]));
                }
            }

            return result;
        }
    }
}
