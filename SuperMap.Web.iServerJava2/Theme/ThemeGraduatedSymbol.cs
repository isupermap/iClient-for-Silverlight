﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeGraduatedSymbol_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeGraduatedSymbol_Description}</para>
    /// 	<para><img src="ThemeGraduatedSymbol.png"/></para>
    /// </summary>
    public class ThemeGraduatedSymbol : Theme
    {
        /// <summary>${iServer2_Theme_ThemeGraduatedSymbol_constructor_None_D}</summary>
        public ThemeGraduatedSymbol()
        {
            PositiveStyle = new ServerStyle() { MarkerSize = 40 };
        }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_baseValue_D}</summary>
        public double BaseValue { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_expression_D}</summary>
        public string Expression { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_graduatedMode_D}</summary>
        public GraduatedMode GraduatedMode { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_isFlowEnabled_D}</summary>
        public bool IsFlowEnabled { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_isLeaderLineDisplayed_D}</summary>
        public bool IsLeaderLineDisplayed { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_isNegativeDisplayed_D}</summary>
        public bool IsNegativeDisplayed { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_isZeroDisplayed_D}</summary>
        public bool IsZeroDisplayed { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_leaderLineStyle_D}</summary>
        public ServerStyle LeaderLineStyle { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_negativeStyle_D}</summary>
        public ServerStyle NegativeStyle { get; set; }

        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_offsetX_D}</summary>
        public string OffsetX { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_offsetY_D}</summary>
        public string OffsetY { get; set; }

        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_positiveStyle_D}</summary>
        public ServerStyle PositiveStyle { get; set; }
        /// <summary>${iServer2_ThemeGraduatedSymbol_attribute_zeroStyle_D}</summary>
        public ServerStyle ZeroStyle { get; set; }

        internal static string ToJson(ThemeGraduatedSymbol param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            list.Add(string.Format("\"baseValue\":{0}", param.BaseValue));
            if (!string.IsNullOrEmpty(param.Expression))
            {
                list.Add(string.Format("\"expression\":\"{0}\"", param.Expression));
            }
            else
            {
                list.Add(string.Format("\"expression\":null"));
            }
            list.Add(string.Format("\"graduatedMode\":{0}", (int)param.GraduatedMode));
            list.Add(string.Format("\"isFlowEnabled\":{0}", param.IsFlowEnabled));
            list.Add(string.Format("\"isLeaderLineDisplayed\":{0}", param.IsLeaderLineDisplayed));
            list.Add(string.Format("\"isNegativeDisplayed\":{0}", param.IsNegativeDisplayed));
            list.Add(string.Format("\"isZeroDisplayed\":{0}", param.IsZeroDisplayed));
            if (param.LeaderLineStyle != null)
            {
                list.Add(string.Format("\"leaderLineStyle\":{0}", ServerStyle.ToJson(param.LeaderLineStyle)));
            }
            else
            {
                list.Add(string.Format("\"leaderLineStyle\":null"));
            }

            if (param.NegativeStyle != null)
            {
                list.Add(string.Format("\"negativeStyle\":{0}", ServerStyle.ToJson(param.NegativeStyle)));
            }
            else
            {
                list.Add(string.Format("\"negativeStyle\":null"));
            }

            if (!string.IsNullOrEmpty(param.OffsetX))
            {
                list.Add(string.Format("\"offsetX\":\"{0}\"", param.OffsetX));
            }
            else
            {
                list.Add(string.Format("\"offsetX\":null"));
            }

            if (!string.IsNullOrEmpty(param.OffsetY))
            {
                list.Add(string.Format("\"offsetY\":\"{0}\"", param.OffsetY));
            }
            else
            {
                list.Add(string.Format("\"offsetY\":null"));
            }

            if (param.PositiveStyle != null)
            {
                list.Add(string.Format("\"positiveStyle\":{0}", ServerStyle.ToJson(param.PositiveStyle)));
            }
            else
            {
                list.Add(string.Format("\"positiveStyle\":null"));
            }

            if (param.ZeroStyle != null)
            {
                list.Add(string.Format("\"zeroStyle\":{0}", ServerStyle.ToJson(param.ZeroStyle)));
            }
            else
            {
                list.Add(string.Format("\"zeroStyle\":null"));
            }
            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
        /// <summary>${iServer2_ThemeGraduatedSymbol_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeGraduatedSymbol_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeGraduatedSymbol_method_FromJson_param_jsonObject}</param>
        public static ThemeGraduatedSymbol FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ThemeGraduatedSymbol result = new ThemeGraduatedSymbol()
            {
                BaseValue = (double)jsonObject["baseValue"],
                Expression = (string)jsonObject["expression"],
                IsFlowEnabled = (bool)jsonObject["isFlowEnabled"],
                IsLeaderLineDisplayed = (bool)jsonObject["isLeaderLineDisplayed"],
                IsNegativeDisplayed = (bool)jsonObject["isNegativeDisplayed"],
                IsZeroDisplayed = (bool)jsonObject["isZeroDisplayed"],
                LeaderLineStyle = ServerStyle.FromJson((JsonObject)jsonObject["leaderLineStyle"]),
                NegativeStyle = ServerStyle.FromJson((JsonObject)jsonObject["negativeStyle"]),
                OffsetX = (string)jsonObject["offsetX"],
                OffsetY = (string)jsonObject["offsetY"],
                PositiveStyle = ServerStyle.FromJson((JsonObject)jsonObject["positiveStyle"]),
                ZeroStyle = ServerStyle.FromJson((JsonObject)jsonObject["zeroStyle"])
            };

            if (jsonObject["graduatedMode"] != null)
            {
                result.GraduatedMode = (GraduatedMode)(int)jsonObject["graduatedMode"];
            }


            return result;
        }
    }
}
