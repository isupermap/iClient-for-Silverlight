﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeLable_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeLable_Description}</para>
    /// 	<para><img src="ThemeLabel.png"/></para>
    /// </summary>
    public class Themelabel : Theme
    {
        /// <summary>${iServer2_Theme_ThemeLable_constructor_None_D}</summary>
        public Themelabel()
        {
            UniformStyle = new ServerTextStyle();
            IsOverlapAvoided = true;
            MaxLabelLength = 256;
        }

        /// <summary>${iServer2_Theme_ThemeLable_attribute_items_D}</summary>
        public IList<ThemeLabelItem> Items { get; set; }

        /// <summary>${iServer2_Theme_ThemeLable_attribute_backStyle_D}</summary>
        public ServerStyle BackStyle { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_leaderLineStyle_D}</summary>
        public ServerStyle LeaderLineStyle { get; set; }

        /// <summary>${iServer2_Theme_ThemeLable_attribute_uniformStyle_D}</summary>
        public ServerTextStyle UniformStyle { get; set; }

        /// <summary>${iServer2_Theme_ThemeLable_attribute_labelBackShape_D}</summary>
        public LabelBackShape LabelBackShape { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_overLengthLabelMode_D}</summary>
        public OverLengthLabelMode OverLengthLabelMode { get; set; }

        /// <summary>${iServer2_Theme_ThemeLable_attribute_isAlongLine_D}</summary>
        public bool IsAlongLine { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_isAngleFixed_D}</summary>
        public bool IsAngleFixed { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_isFlowEnabled_D}</summary>
        public bool IsFlowEnabled { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_isLabelRepeated_D}</summary>
        public bool IsLabelRepeated { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_isLeaderLineDisplayed_D}</summary>
        public bool IsLeaderLineDisplayed { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_isOverlapAvoided_D}</summary>
        public bool IsOverlapAvoided { get; set; }

        /// <summary>${iServer2_Theme_ThemeLable_attribute_labelRepeatInterval_D}</summary>
        public double LabelRepeatInterval { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_maxLabelLength_D}</summary>
        public double MaxLabelLength { get; set; }

        /// <summary>${iServer2_Theme_ThemeLable_attribute_offsetX_D}</summary>
        public string OffsetX { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_offsetY_D}</summary>
        public string OffsetY { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_labelExpression_D}</summary>
        public string LabelExpression { get; set; }
        /// <summary>${iServer2_Theme_ThemeLable_attribute_rangeExpression_D}</summary>
        public string RangeExpression { get; set; }


        internal static string ToJson(Themelabel param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (param.Items != null && param.Items.Count > 0)
            {
                List<string> tempTGI = new List<string>();
                foreach (ThemeLabelItem item in param.Items)
                {
                    tempTGI.Add(ThemeLabelItem.ToJson(item));
                }
                list.Add(string.Format("\"items\":[{0}]", string.Join(",", tempTGI.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"items\":[]"));
            }

            if (param.BackStyle != null)
            {
                list.Add(string.Format("\"backStyle\":{0}", ServerStyle.ToJson(param.BackStyle)));
            }
            else
            {
                list.Add(string.Format("\"backStyle\":null"));
            }

            if (param.LeaderLineStyle != null)
            {
                list.Add(string.Format("\"leaderLineStyle\":{0}", ServerStyle.ToJson(param.LeaderLineStyle)));
            }
            else
            {
                list.Add(string.Format("\"leaderLineStyle\":null"));
            }

            if (param.UniformStyle != null)
            {
                list.Add(string.Format("\"uniformStyle\":{0}", ServerTextStyle.ToJson(param.UniformStyle)));
            }

            list.Add(string.Format("\"labelBackShape\":{0}", (int)param.LabelBackShape));
            list.Add(string.Format("\"overLengthLabelMode\":{0}", (int)param.OverLengthLabelMode));


            list.Add(string.Format("\"isAlongLine\":{0}", param.IsAlongLine));
            list.Add(string.Format("\"isAngleFixed\":{0}", param.IsAngleFixed));
            list.Add(string.Format("\"isFlowEnabled\":{0}", param.IsFlowEnabled));
            list.Add(string.Format("\"isLabelRepeated\":{0}", param.IsLabelRepeated));
            list.Add(string.Format("\"isLeaderLineDisplayed\":{0}", param.IsLeaderLineDisplayed));
            list.Add(string.Format("\"isOverlapAvoided\":{0}", param.IsOverlapAvoided));

            list.Add(string.Format("\"labelRepeatInterval\":{0}", param.LabelRepeatInterval));
            list.Add(string.Format("\"maxLabelLength\":{0}", param.MaxLabelLength));

            if (!string.IsNullOrEmpty(param.OffsetX))
            {
                list.Add(string.Format("\"offsetX\":\"{0}\"", param.OffsetX));
            }

            if (!string.IsNullOrEmpty(param.OffsetY))
            {
                list.Add(string.Format("\"offsetY\":\"{0}\"", param.OffsetY));
            }

            if (!string.IsNullOrEmpty(param.LabelExpression))
            {
                list.Add(string.Format("\"labelExpression\":\"{0}\"", param.LabelExpression));
            }
            else
            {
                list.Add("\"labelExpression\":\"\"");
            }
            if (!string.IsNullOrEmpty(param.RangeExpression))
            {
                list.Add(string.Format("\"rangeExpression\":\"{0}\"", param.RangeExpression));
            }
            else
            {
                list.Add("\"rangeExpression\":\"\"");
            }
            list.Add(string.Format("\"themeType\":7"));
            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
        /// <summary>${iServer2_Themelabel_method_FromJson_D}</summary>
        /// <returns>${iServer2_Themelabel_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_Themelabel_method_FromJson_param_jsonObject}</param>
        public static Themelabel FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            Themelabel result = new Themelabel();

            result.BackStyle = ServerStyle.FromJson((JsonObject)jsonObject["backStyle"]);
            result.IsAlongLine = (bool)jsonObject["isAlongLine"];
            result.IsAngleFixed = (bool)jsonObject["isAngleFixed"];
            result.IsFlowEnabled = (bool)jsonObject["isFlowEnabled"];
            result.IsLabelRepeated = (bool)jsonObject["isLabelRepeated"];
            result.IsLeaderLineDisplayed = (bool)jsonObject["isLeaderLineDisplayed"];
            result.IsOverlapAvoided = (bool)jsonObject["isOverlapAvoided"];
            if (jsonObject["items"] != null)
            {
                result.Items = new List<ThemeLabelItem>();
                for (int i = 0; i < jsonObject["items"].Count; i++)
                {
                    result.Items.Add(ThemeLabelItem.FromJson((JsonObject)jsonObject["items"][i]));
                }
            }

            if (jsonObject["labelBackShape"] != null)
            {
                result.LabelBackShape = (LabelBackShape)(int)jsonObject["labelBackShape"];
            }
            result.LabelExpression = (string)jsonObject["labelExpression"];
            result.LabelRepeatInterval = (double)jsonObject["labelRepeatInterval"];
            result.LeaderLineStyle = ServerStyle.FromJson((JsonObject)jsonObject["leaderLineStyle"]);
            result.MaxLabelLength = (double)jsonObject["maxLabelLength"];
            result.OffsetX = (string)jsonObject["offsetX"];
            result.OffsetY = (string)jsonObject["offsetY"];
            if (jsonObject["overLengthLabelMode"] != null)
            {
                result.OverLengthLabelMode = (OverLengthLabelMode)(int)jsonObject["overLengthLabelMode"];
            }
            result.RangeExpression = (string)jsonObject["rangeExpression"];
            result.UniformStyle = ServerTextStyle.FromJson((JsonObject)jsonObject["uniformStyle"]);

            return result;
        }
    }
}
