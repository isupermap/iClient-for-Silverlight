﻿using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_Theme_ThemeEventArgs_Title}</para>
    /// <para>${iServer2_Theme_ThemeEventArgs_Description}</para>
    /// </summary>
    public class ThemeEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer2_ThemeEventArgs_constructor_None_D}</summary>
        public ThemeEventArgs(ThemeResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServer2_Theme_ThemeEventArgs_attribute_Result_D}</summary>
        public ThemeResult Result { get; private set; }
        /// <summary>${iServer2_Theme_ThemeEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
