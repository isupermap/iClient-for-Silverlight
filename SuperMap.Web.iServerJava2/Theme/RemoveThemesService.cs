﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.iServerJava2.Resources;
using System.Text;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_RemoveThemesService_Title}</para>
    /// 	<para>${iServer2_Theme_RemoveThemesService_Description}</para>
    /// </summary>
    public class RemoveThemesService : ServiceBase
    {
        /// <overloads>${iServer2_Theme_RemoveThemesService_overloads_D}</overloads>
        /// <summary>${iServer2_Theme_RemoveThemesService_None_D}</summary>
        public RemoveThemesService()
        { }
        /// <summary>${iServer2_Theme_RemoveThemesService_string_D}</summary>
        /// <param name="url">${iServer2_Theme_RemoveThemesService_string_param_url}</param>
        public RemoveThemesService(string url)
            : base(url)
        { }
        /// <overloads>${iServer2_Theme_RemoveThemesService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_Theme_RemoveThemesService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Theme_RemoveThemesService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(RemoveThemesParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_Theme_RemoveThemesService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Theme_RemoveThemesService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_Theme_RemoveThemesService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(RemoveThemesParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:资源
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }
            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(RemoveThemesParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "removeSuperMapLayers";

            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);

            if (parameters.LayerNames.Count > 0 && parameters.LayerNames != null)
            {
                List<string> list = new List<string>();
                for (int i = 0; i < parameters.LayerNames.Count; i++)
                {
                    list.Add(string.Format("\"{0}\"", parameters.LayerNames[i]));
                }
                string json = "[";
                json += string.Join(",", list.ToArray());
                json += "]";

                dict.Add("layerNames", json);
            }
            dictionary.Add("params", CreateParameters(method, dict));
            return dictionary;
        }

        private static string CreateParameters(string method, Dictionary<string, string> dict)
        {
            StringBuilder paramsBuilder = new StringBuilder();
            paramsBuilder.Append("<uis><command>");
            paramsBuilder.AppendFormat("<name>{0}</name>", method);
            paramsBuilder.Append("<parameter>");
            foreach (string key in dict.Keys)
            {
                paramsBuilder.AppendFormat("<{0}>{1}</{0}>", new object[] { key, dict[key] });
            }
            paramsBuilder.Append("</parameter>");
            paramsBuilder.Append("</command></uis>");

            return paramsBuilder.ToString();
        }

        private RemoveThemesResult lastResult;

        /// <summary>${iServer2_Theme_RemoveThemesService_attribute_lastResult_D}</summary>
        public RemoveThemesResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }

        /// <summary>${iServer2_Theme_RemoveThemesService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<RemoveThemesEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonPrimitive jsonPrimitive = (JsonPrimitive)JsonObject.Parse(e.Result);
            RemoveThemesResult result = RemoveThemesResult.FromJson(jsonPrimitive);

            LastResult = result;
            RemoveThemesEventArgs args = new RemoveThemesEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(RemoveThemesEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
    }
}
