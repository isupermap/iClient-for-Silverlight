﻿using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeRange_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeRange_Description}</para>
    /// 	<para>
    /// 		<list type="table">
    /// 			<item>
    /// 				<term><img src="RangeTheme1.bmp"/></term>
    /// 				<description><img src="RangeTheme2.bmp"/></description>
    /// 			</item>
    /// 		</list>
    /// 	</para>
    /// </summary>
    public class ThemeRange : Theme
    {
        /// <summary>${iServer2_Theme_ThemeRange_constructor_None_D}</summary>
        public ThemeRange()
        { }
        /// <summary>${iServer2_Theme_ThemeRange_attribute_RangeExpression_D}</summary>
        public string RangeExpression { get; set; }
        /// <summary>${iServer2_Theme_ThemeRange_attribute_items_D}</summary>
        public IList<ThemeRangeItem> Items { get; set; }
        /// <summary>${iServer2_Theme_ThemeRange_attribute_makeDefaultParam_D}</summary>
        public ThemeRangeParam MakeDefaultParam { get; set; }

        internal static string ToJson(ThemeRange themeRange)
        {
            if (themeRange == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            if (themeRange.RangeExpression != null)
            {
                list.Add(string.Format("\"rangeExpression\":\"{0}\"", themeRange.RangeExpression));
            }

            if (themeRange.Items != null && themeRange.Items.Count > 0)
            {
                List<string> tempTUI = new List<string>();
                foreach (ThemeRangeItem item in themeRange.Items)
                {
                    tempTUI.Add(ThemeRangeItem.ToJson(item));
                }
                list.Add(string.Format("\"items\":[{0}]", string.Join(",", tempTUI.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"items\":[]"));
            }

            list.Add(string.Format("\"themeType\":2"));

            if (themeRange.MakeDefaultParam != null)
            {
                list.Add(string.Format("\"makeDefaultParam\":{0}", ThemeRangeParam.ToJson(themeRange.MakeDefaultParam)));
            }
            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
        /// <summary>${iServer2_ThemeRange_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeRange_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeRange_method_FromJson_param_jsonObject}</param>
        public static ThemeRange FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ThemeRange result = new ThemeRange();

            result.RangeExpression = (string)jsonObject["rangeExpression"];

            if (jsonObject.ContainsKey("items") && jsonObject["items"] != null)
            {
                result.Items = new List<ThemeRangeItem>();
                for (int i = 0; i < jsonObject["items"].Count; i++)
                {
                    result.Items.Add(ThemeRangeItem.FromJson((JsonObject)jsonObject["items"][i]));
                }
            }

            result.MakeDefaultParam = ThemeRangeParam.FromJson((JsonObject)jsonObject["makeDefaultParam"]);


            return result;
        }
    }
}
