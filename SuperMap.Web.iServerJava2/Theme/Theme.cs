﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_Theme_Title}</para>
    /// <para>${iServer2_Theme_Description}</para>
    /// </summary>
    public abstract class Theme
    {

        /// <summary>${iServer2_Theme_constructor_None_D}</summary>
        public Theme()
        { }
        ///// <summary>${iServer2_Theme_attribute_themeType_D}</summary>
        //public ThemeType ThemeType { get; private set; }
    }
}
