﻿using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{

    /// <summary>
    /// <para>${iServer2_Theme_ServerStyle_Title}</para>
    /// <para>${iServer2_Theme_ServerStyle_Description}</para>
    /// </summary>
    /// <remarks>${iServer2_Theme_ServerStyle_Remarks}</remarks>
    public class ServerStyle
    {
        private double markerSize;
        /// <summary>${iServer2_Theme_ServerStyle_constructor_None_D}</summary>
        public ServerStyle()
        {
            FillBackColor = new ServerColor(255, 255, 255);
            FillForeColor = new ServerColor(255, 0, 0);
            FillOpaqueRate = 100;
            FillSymbolID = 0;
            LineColor = new ServerColor(0, 0, 0);
            LineSymbolID = 0x000000;
            LineWidth = 0.01;
            MarkerAngle = 0;
            MarkerSymbolID = -1;
            MarkerSize = 1;
        }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_fillBackColor_D}</summary>
        public ServerColor FillBackColor { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_fillBackOpaque_D}</summary>
        public bool FillBackOpaque { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_fillForeColor_D}</summary>
        public ServerColor FillForeColor { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_fillGradientMode_D}</summary>
        public FillGradientMode FillGradientMode { get; set; }
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerStyle_attribute_fillGradientAngle_D_1}</para>
        /// 	<para><img style="WIDTH: 136px; HEIGHT: 83px" height="90" src="fillGradientAngleLinear0.bmp" width="159"/></para>
        /// 	<para>${iServer2_Theme_ServerStyle_attribute_fillGradientAngle_D_2}</para>
        /// 	<para><img style="WIDTH: 139px; HEIGHT: 81px" height="90" src="fillGradientAngleLinear180.bmp" width="159"/></para>
        /// 	<para>${iServer2_Theme_ServerStyle_attribute_fillGradientAngle_D_3}</para>
        /// 	<para><img style="WIDTH: 142px; HEIGHT: 82px" height="82" src="fillGradientAngleLinear90.bmp" width="145"/></para>
        /// 	<para>${iServer2_Theme_ServerStyle_attribute_fillGradientAngle_D_4}</para>
        /// 	<para><img style="WIDTH: 144px; HEIGHT: 89px" height="91" src="fillGradientAngleLinear270.bmp" width="158"/></para>
        /// 	<para>${iServer2_Theme_ServerStyle_attribute_fillGradientAngle_D_5}</para>
        /// 	<para><img src="fillGradientAngleConical0.bmp"/><img src="fillGradientAngleConical90.bmp"/></para>
        /// 	<para>${iServer2_Theme_ServerStyle_attribute_fillGradientAngle_D_6}</para>
        /// </summary>
        public double FillGradientAngle { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_fillGradientOffsetRatioX_D}</summary>
        public double FillGradientOffsetRatioX { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_fillGradientOffsetRatioY_D}</summary>
        public double FillGradientOffsetRatioY { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_fillOpaqueRate_D}</summary>
        public int FillOpaqueRate { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_fillSymbolID_D}</summary>
        public int FillSymbolID { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_lineColor_D}</summary>
        public ServerColor LineColor { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_lineSymbolID_D}</summary>
        public int LineSymbolID { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_lineWidth_D}</summary>
        public double LineWidth { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_MarkerAngle_D}</summary>
        public double MarkerAngle { get; set; }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_MarkerSize_D}</summary>
        public double MarkerSize
        {
            get
            {
                return markerSize;
            }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }
                markerSize = value;
            }
        }
        /// <summary>${iServer2_Theme_ServerStyle_attribute_MarkerSymbolID_D}</summary>
        public int MarkerSymbolID { get; set; }

        internal static string ToJson(ServerStyle serverStyle)
        {
            if (serverStyle == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"fillGradientAngle\":{0}", serverStyle.FillGradientAngle));

            if (ServerColor.ToJson(serverStyle.FillBackColor) != null)
            {
                list.Add(string.Format("\"fillBackColor\":{0}", ServerColor.ToJson(serverStyle.FillBackColor)));
            }

            list.Add(string.Format("\"fillBackOpaque\":{0}", serverStyle.FillBackOpaque));

            list.Add(string.Format("\"fillGradientOffsetRatioX\":{0}", serverStyle.FillGradientOffsetRatioX));

            list.Add(string.Format("\"fillGradientOffsetRatioY\":{0}", serverStyle.FillGradientOffsetRatioY));

            if (ServerColor.ToJson(serverStyle.FillForeColor) != null)
            {
                list.Add(string.Format("\"fillForeColor\":{0}", ServerColor.ToJson(serverStyle.FillForeColor)));
            }

            list.Add(string.Format("\"fillOpaqueRate\":{0}", serverStyle.FillOpaqueRate));

            list.Add(string.Format("\"fillSymbolID\":{0}", serverStyle.FillSymbolID));

            if (ServerColor.ToJson(serverStyle.LineColor) != null)
            {
                list.Add(string.Format("\"lineColor\":{0}", ServerColor.ToJson(serverStyle.LineColor)));
            }
            list.Add(string.Format("\"lineSymbolID\":{0}", serverStyle.LineSymbolID));

            list.Add(string.Format("\"lineWidth\":{0}", serverStyle.LineWidth));

            list.Add(string.Format("\"markerAngle\":{0}", serverStyle.MarkerAngle));

            list.Add(string.Format("\"markerSize\":{0}", serverStyle.MarkerSize));
            list.Add(string.Format("\"fillGradientMode\":{0}", (int)serverStyle.FillGradientMode));

            list.Add(string.Format("\"markerSymbolID\":{0}", serverStyle.MarkerSymbolID));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
        /// <summary>${iServer2_ServerStyle_method_FromJson_D}</summary>
        /// <returns>${iServer2_ServerStyle_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ServerStyle_method_FromJson_param_jsonObject}</param>
        public static ServerStyle FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }
            ServerStyle result = new ServerStyle()
            {
                FillGradientAngle = (double)jsonObject["fillGradientAngle"],
                FillBackColor = ServerColor.FromJson((JsonObject)jsonObject["fillBackColor"]),
                FillBackOpaque = (bool)jsonObject["fillBackOpaque"],
                FillGradientOffsetRatioX = (double)jsonObject["fillGradientOffsetRatioX"],
                FillGradientOffsetRatioY = (double)jsonObject["fillGradientOffsetRatioY"],
                FillForeColor = ServerColor.FromJson((JsonObject)jsonObject["fillForeColor"]),
                FillGradientMode = (FillGradientMode)(int)jsonObject["fillGradientMode"],
                FillOpaqueRate = (int)jsonObject["fillOpaqueRate"],
                FillSymbolID = (int)jsonObject["fillSymbolID"],

                LineColor = ServerColor.FromJson((JsonObject)jsonObject["lineColor"]),
                LineSymbolID = (int)jsonObject["lineSymbolID"],
                LineWidth = (double)jsonObject["lineWidth"],

                MarkerAngle = (double)jsonObject["markerAngle"],
                MarkerSize = (double)jsonObject["markerSize"],
                MarkerSymbolID = (int)jsonObject["markerSymbolID"]
            };

            return result;
        }
    }
}
