﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_Theme_ThemeService_Title}</para>
    /// <para>${iServer2_Theme_ThemeService_Description}</para>
    /// </summary>
    public class ThemeService : ServiceBase
    {
        /// <overloads>${iServer2_Theme_ThemeService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_Theme_ThemeService_constructor_None_D}</summary>
        public ThemeService()
        { }
        /// <summary>${iServer2_Theme_ThemeService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_Theme_ThemeService_constructor_String_param_url}</param>
        public ThemeService(string url)
            : base(url)
        { }
        /// <overloads>${iServer2_Theme_ThemeService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_Theme_ThemeService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Theme_ThemeService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(ThemeParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_Theme_ThemeService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_Theme_ThemeService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_Theme_ThemeService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(ThemeParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("ThemeParameters is Null");
                throw new ArgumentNullException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                //TODO:资源
                //throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.InvalidUrl);
            }
            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(ThemeParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "addTheme";

            if (parameters.JoinItems != null && parameters.JoinItems.Count > 0)
            {
                method = "addthemebyjoinitem";
            }

            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);
            dict.Add("layerName", parameters.LayerName);
        
            if (parameters.Theme is ThemeUnique)
            {
                dict.Add("theme", ThemeUnique.ToJson(((ThemeUnique)parameters.Theme)));
                dict.Add("themeType", "1");
            }
            else if (parameters.Theme is ThemeRange)
            {
                dict.Add("theme", ThemeRange.ToJson(((ThemeRange)parameters.Theme)));
                dict.Add("themeType", "2");
            }
            else if (parameters.Theme is ThemeGraph)
            {
                dict.Add("theme", ThemeGraph.ToJson(((ThemeGraph)parameters.Theme)));
                dict.Add("themeType", "3");
            }
            else if (parameters.Theme is ThemeGraduatedSymbol)
            {
                dict.Add("theme", ThemeGraduatedSymbol.ToJson((ThemeGraduatedSymbol)parameters.Theme));
                dict.Add("themeType", "4");
            }
            else if (parameters.Theme is ThemeDotDensity)
            {
                dict.Add("theme", ThemeDotDensity.ToJson((ThemeDotDensity)parameters.Theme));
                dict.Add("themeType", "5");
            }
            else if (parameters.Theme is Themelabel)
            {
                dict.Add("theme", Themelabel.ToJson((Themelabel)parameters.Theme));
                dict.Add("themeType", "7");
            }
            //else if (parameters.Theme is ThemeGridRange)
            //{
            //    dict.Add("theme", ThemeGridRange.ToJson(((ThemeGridRange)parameters.Theme)));
            //    dict.Add("themeType", "12");
            //}
            if (parameters.JoinItems != null && parameters.JoinItems.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < parameters.JoinItems.Count; i++)
                {
                    temp.Add(string.Format("{0}", JoinItem.ToJson(parameters.JoinItems[i])));
                }
                dict.Add("joinItems", string.Join(",", temp.ToArray()));
            }
            else
            {
                dict.Add("joinItems", "null");
            }

            dictionary.Add("params", Bridge.CreateParams(method, dict));

            return dictionary;
        }


        private ThemeResult lastResult;
        /// <summary>${iServer2_Theme_ThemeService_attribute_lastResult_D}</summary>
        public ThemeResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }

        /// <summary>${iServer2_Theme_ThemeService_event_processCompleted_D}</summary>
        [ScriptableMember]        
        public event EventHandler<ThemeEventArgs> ProcessCompleted;

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonPrimitive jsonPrimitive = (JsonPrimitive)JsonObject.Parse(e.Result);
            ThemeResult result = ThemeResult.FromJson(jsonPrimitive);

            LastResult = result;
            ThemeEventArgs args = new ThemeEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }

        private void OnProcessCompleted(ThemeEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
    }
}
