﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeDotDensity_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeDotDensity_Description}</para>
    /// 	<para><img src="ThemeDotDensity.png"/></para>
    /// </summary>
    public class ThemeDotDensity : Theme
    {
        /// <summary>${iServer2_Theme_ThemeDotDensity_constructor_None_D}</summary>
        public ThemeDotDensity()
        {
            Style = new ServerStyle() { MarkerSize = 2 };
        }

        /// <summary>${iServer2_Theme_ThemeDotDensity_attribute_dotExpression_D}</summary>
        public string DotExpression { get; set; }
        /// <summary>${iServer2_Theme_ThemeDotDensity_attribute_style_D}</summary>
        public ServerStyle Style { get; set; }
        /// <summary>${iServer2_Theme_ThemeDotDensity_attribute_value_D}</summary>
        public double Value { get; set; }

        internal static string ToJson(ThemeDotDensity param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";

            List<string> list = new List<string>();
            if (!string.IsNullOrEmpty(param.DotExpression))
            {
                list.Add(string.Format("\"dotExpression\":\"{0}\"", param.DotExpression));
            }
            else
            {
                list.Add(string.Format("\"dotExpression\":null"));
            }

            if (param.Style != null)
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(param.Style)));
            }
            else
            {
                list.Add(string.Format("\"style\":null"));
            }

            list.Add(string.Format("\"value\":{0}", param.Value));
            list.Add(string.Format("\"themeType\":5"));
            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
        /// <summary>${iServer2_ThemeDotDensity_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeDotDensity_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeDotDensity_method_FromJson_param_jsonObject}</param>
        public static ThemeDotDensity FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeDotDensity result = new ThemeDotDensity();

            result.DotExpression = (string)jsonObject["dotExpression"];
            result.Style = ServerStyle.FromJson((JsonObject)jsonObject["style"]);
            result.Value = (double)jsonObject["value"];


            return result;

        }
    }
}
