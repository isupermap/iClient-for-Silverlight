﻿using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeGraphItem_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeGraphItem_Description}</para>
    /// </summary>
    public class ThemeGraphItem
    {
        /// <summary>${iServer2_Theme_ThemeGraphItem_constructor_None_D}</summary>
        public ThemeGraphItem()
        {
            UniformStyle = new ServerStyle();
        }
        /// <summary>${iServer2_Theme_ThemeGraphItem_attribute_caption_D}</summary>
        public string Caption { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraphItem_attribute_graphExpression_D}</summary>
        public string GraphExpression { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraphItem_attribute_uniformStyle_D}</summary>
        public ServerStyle UniformStyle { get; set; }
        /// <summary>${iServer2_Theme_ThemeGraphItem_attribute_rangeSetting_D}</summary>
        public ThemeRange RangeSetting { get; set; }

        internal static string ToJson(ThemeGraphItem themeGraphItem)
        {
            if (themeGraphItem == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (themeGraphItem.GraphExpression != null)
            {
                list.Add(string.Format("\"graphExpression\":\"{0}\"", themeGraphItem.GraphExpression));
            }
            if (themeGraphItem.UniformStyle != null)
            {
                list.Add(string.Format("\"uniformStyle\":{0}", ServerStyle.ToJson(themeGraphItem.UniformStyle)));
            }
            if (themeGraphItem.Caption != null)
            {
                list.Add(string.Format("\"caption\":\"{0}\"", themeGraphItem.Caption));
            }
            if (themeGraphItem.RangeSetting != null)
            {
                list.Add(string.Format("\"rangeSetting\":\"{0}\"", ThemeRange.ToJson(themeGraphItem.RangeSetting)));
            }
            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
        /// <summary>${iServer2_ThemeGraphItem_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeGraphItem_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeGraphItem_method_FromJson_param_jsonObject}</param>
        public static ThemeGraphItem FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeGraphItem result = new ThemeGraphItem();

            result.Caption = (string)jsonObject["caption"];
            result.GraphExpression = (string)jsonObject["graphExpression"];
            result.UniformStyle = ServerStyle.FromJson((JsonObject)jsonObject["uniformStyle"]);
            result.RangeSetting = ThemeRange.FromJson((JsonObject)jsonObject["rangeSetting"]);

            return result;
        }
    }
}
