﻿using System.Json;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_Theme_ThemeResult_Title}</para>
    /// <para>${iServer2_Theme_ThemeResult_Description}</para>
    /// </summary>
    public class ThemeResult
    {
        internal ThemeResult()
        { }
        /// <summary>${iServer2_Theme_ThemeResult_attribute_name_D}</summary>
        public string Name { get; private set; }
        /// <summary>${iServer2_Theme_ThemeResult_attribute_key_D}</summary>
        public string Key { get; private set; }
        /// <summary>${iServer2_ThemeResult_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeResult_method_FromJson_return}</returns>
        /// <param name="jsonPrimitive">${iServer2_ThemeResult_method_FromJson_param_jsonObject}</param>
        public static ThemeResult FromJson(JsonPrimitive jsonPrimitive)
        {
            if (jsonPrimitive == null)
            {
                return null;
            }

            string str = jsonPrimitive;
            ThemeResult result = new ThemeResult();
            result.Name = str.Substring(0, str.IndexOf(","));
            result.Key = str.Substring(str.IndexOf(",") + 1, str.Length - str.IndexOf(",") - 1);
            return result;
        }
    }
}
