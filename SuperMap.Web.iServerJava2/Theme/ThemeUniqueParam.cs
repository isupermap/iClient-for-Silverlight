﻿using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_Theme_ThemeUniqueParam_Title}</para>
    /// <para>${iServer2_Theme_ThemeUniqueParam_Description}</para>
    /// </summary>
    public class ThemeUniqueParam
    {
        /// <summary>${iServer2_Theme_ThemeUniqueParam_constructor_None_D}</summary>
        public ThemeUniqueParam()
        {
            ColorGradientType = ColorGradientType.YellowGreen;
        }
        /// <summary>${iServer2_Theme_ThemeUniqueParam_attribute_colorGradientType_D}</summary>
        public ColorGradientType ColorGradientType { get; set; }
        /// <summary>${iServer2_Theme_ThemeUniqueParam_attribute_layerName_D}</summary>
        public string LayerName { get; set; }

        internal static string ToJson(ThemeUniqueParam themeUniqueParam)
        {
            if (themeUniqueParam == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            if (themeUniqueParam.LayerName != null)
            {
                list.Add(string.Format("\"layerName\":\"{0}\"", themeUniqueParam.LayerName));
            }

            list.Add(string.Format("\"colorGradientType\":{0}", (int)themeUniqueParam.ColorGradientType));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
        /// <summary>${iServer2_ThemeUniqueParam_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeUniqueParam_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeUniqueParam_method_FromJson_param_jsonObject}</param>
        public static ThemeUniqueParam FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeUniqueParam result = new ThemeUniqueParam();

            result.ColorGradientType = (ColorGradientType)(int)jsonObject["colorGradientType"];
            result.LayerName = (string)jsonObject["layerName"];

            return result;

        }
    }
}
