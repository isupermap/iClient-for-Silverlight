﻿using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeUnique_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeUnique_Description}</para>
    /// 	 <para>
    /// 		<list type="table">
    /// 			<item>
    /// 				<term><img src="UniqueTheme1.bmp"/></term>
    /// 				<description><img src="UniqueTheme2.bmp"/></description>
    /// 			</item>
    /// 		</list>
    /// 	</para>
    /// </summary>
    public class ThemeUnique : Theme
    {
        /// <summary>${iServer2_Theme_ThemeUnique_constructor_None_D}</summary>
        public ThemeUnique()
        {
            DefaultStyle = new ServerStyle();
        }
        /// <summary>${iServer2_Theme_ThemeUnique_attribute_items_D}</summary>
        public IList<ThemeUniqueItem> Items { get; set; }
        /// <summary>${iServer2_Theme_ThemeUnique_attribute_uniqueExpression_D}</summary>
        public string UniqueExpression { get; set; }
        /// <summary>${iServer2_Theme_ThemeUnique_attribute_defaultStyle_D}</summary>
        public ServerStyle DefaultStyle { get; set; }
        /// <summary>${iServer2_Theme_ThemeUnique_attribute_makeDefaultParam_D}</summary>
        public ThemeUniqueParam MakeDefaultParam { get; set; }

        internal static string ToJson(ThemeUnique themeUnique)
        {
            if (themeUnique == null)
            {
                return null;
            }
            string json = "{";

            List<string> list = new List<string>();

            if (themeUnique.Items != null && themeUnique.Items.Count > 0)
            {
                List<string> tempTUI = new List<string>();
                foreach (ThemeUniqueItem item in themeUnique.Items)
                {
                    tempTUI.Add(ThemeUniqueItem.ToJson(item));
                }
                list.Add(string.Format("\"items\":[{0}]", string.Join(",", tempTUI.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"items\":[]"));
            }

            if (!string.IsNullOrEmpty(themeUnique.UniqueExpression))
            {
                list.Add(string.Format("\"uniqueExpression\":\"{0}\"", themeUnique.UniqueExpression));
            }
            else
            {
                list.Add(string.Format("\"uniqueExpression\":null"));
            }

            if (themeUnique.DefaultStyle != null)
            {
                list.Add(string.Format("\"defaultStyle\":{0}", ServerStyle.ToJson(themeUnique.DefaultStyle)));
            }
            else
            {
                list.Add(string.Format("\"defaultStyle\":null"));
            }

            list.Add(string.Format("\"themeType\":1"));

            if (themeUnique.MakeDefaultParam != null)
            {
                list.Add(string.Format("\"makeDefaultParam\":{0}", ThemeUniqueParam.ToJson(themeUnique.MakeDefaultParam)));
            }

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
        /// <summary>${iServer2_ThemeUnique_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeUnique_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeUnique_method_FromJson_param_jsonObject}</param>
        public static ThemeUnique FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeUnique result = new ThemeUnique();

            result.DefaultStyle = ServerStyle.FromJson((JsonObject)jsonObject["defaultStyle"]);

            if (jsonObject.ContainsKey("items") && jsonObject["items"] != null)
            {
                result.Items = new List<ThemeUniqueItem>();
                for (int i = 0; i < jsonObject["items"].Count; i++)
                {
                    result.Items.Add(ThemeUniqueItem.FromJson((JsonObject)jsonObject["items"][i]));
                }
            }
            result.UniqueExpression = (string)jsonObject["uniqueExpression"];
            result.MakeDefaultParam = ThemeUniqueParam.FromJson((JsonObject)jsonObject["makeDefaultParam"]);

            return result;
        }
    }
}
