﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>${iServer2_Theme_RangeMode_Title}</summary>
    public enum RangeMode
    {
        /// <summary>${iServer2_Theme_RangeMode_Equalinterval_D}</summary>
        Equalinterval = 0,
        /// <summary>${iServer2_Theme_RangeMode_Squareroot_D}</summary>
        Squareroot = 1,
        /// <summary>${iServer2_Theme_RangeMode_Stddeviation_D}</summary>
        Stddeviation = 2,
        /// <summary>${iServer2_Theme_RangeMode_Logarithm_D}</summary>
        Logarithm = 3,
        /// <summary>${iServer2_Theme_RangeMode_Quantile_D}</summary>
        Quantile = 4,
        /// <summary>${iServer2_Theme_RangeMode_Custominterval_D}</summary>
        Custominterval = 5
    }
}
