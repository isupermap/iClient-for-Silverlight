﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeLableItem_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeLableItem_Description}</para>
    /// </summary>
    public class ThemeLabelItem
    {
        /// <summary>${iServer2_Theme_ThemeLableItem_constructor_None_D}</summary>
        public ThemeLabelItem()
        {
            Visible = true;
            Style = new ServerTextStyle();
        }
        /// <summary>${iServer2_Theme_ThemeLableItem_attribute_caption_D}</summary>
        public string Caption { get; set; }
        /// <summary>${iServer2_Theme_ThemeLableItem_attribute_start_D}</summary>
        public double Start { get; set; }
        /// <summary>${iServer2_Theme_ThemeLableItem_attribute_end_D}</summary>
        public double End { get; set; }
        /// <summary>${iServer2_Theme_ThemeLableItem_attribute_visible_D}</summary>
        public bool Visible { get; set; }
        /// <summary>${iServer2_Theme_ThemeLableItem_attribute_style_D}</summary>
        public ServerTextStyle Style { get; set; }

        internal static string ToJson(ThemeLabelItem param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (param.Caption != null)
            {
                list.Add(string.Format("\"caption\":\"{0}\"", param.Caption));
            }

            if (param.Style != null)
            {
                list.Add(string.Format("\"style\":{0}", ServerTextStyle.ToJson(param.Style)));
            }
            else
            {
                list.Add(string.Format("\"style\":null"));
            }

            list.Add(string.Format("\"start\":{0}", param.Start));
            list.Add(string.Format("\"end\":{0}", param.End));
            list.Add(string.Format("\"visible\":{0}", param.Visible));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
        /// <summary>${iServer2_ThemeLableItem_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeLableItem_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeLableItem_method_FromJson_param_jsonObject}</param>
        public static ThemeLabelItem FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;

            }

            ThemeLabelItem result = new ThemeLabelItem();

            result.Caption = (string)jsonObject["caption"];
            result.End = (double)jsonObject["end"];
            result.Start = (double)jsonObject["start"];
            result.Visible = (bool)jsonObject["visible"];
            result.Style = ServerTextStyle.FromJson((JsonObject)jsonObject["style"]);

            return result;
        }
    }
}
