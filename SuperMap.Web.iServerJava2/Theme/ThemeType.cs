﻿//namespace SuperMap.Web.iServerJava2
//{
//    /// <summary>
//    /// 	<para>${iServer2_Theme_ThemeType_Title}</para>
//    /// 	<para>${iServer2_Theme_ThemeType_Description}</para>
//    /// </summary>
//    public enum ThemeType
//    {
//        /// <summary>
//        /// 	<para>${iServer2_Theme_ThemeType_attribute_Unique_D}</para>
//        /// 	<para>
//        /// 		<list type="table">
//        /// 			<item>
//        /// 				<term><img src="UniqueTheme1.bmp"/></term>
//        /// 				<description><img src="UniqueTheme2.bmp"/></description>
//        /// 			</item>
//        /// 		</list>
//        /// 	</para>
//        /// </summary>
//        Unique = 1,
//        /// <summary>
//        /// 	<para>${iServer2_Theme_ThemeType_attribute_Range_D}</para>
//        /// 	<para>
//        /// 		<list type="table">
//        /// 			<item>
//        /// 				<term><img src="RangeTheme1.bmp"/></term>
//        /// 				<description><img src="RangeTheme2.bmp"/></description>
//        /// 			</item>
//        /// 		</list>
//        /// 	</para>
//        /// </summary>
//        Range = 2,
//        /// <summary>
//        /// 	<para>${iServer2_Theme_ThemeType_attribute_Graph_D}</para>
//        /// 	<para>
//        /// 		<list type="table">
//        /// 			<item>
//        /// 				<term><img src="GraphTheme1.bmp"/><img src="GraphTheme2.bmp"/></term>
//        /// 				<description></description>
//        /// 			</item>
//        /// 		</list>
//        /// 	</para>
//        /// </summary>
//        Graph = 3,

//        ThemeGraduatedSymbol = 4,
//        ThemeDotDensity = 5,
//        ThemeLabel = 7,

//        //栅格专题图
//        GridRange = 12
//    }
//}
