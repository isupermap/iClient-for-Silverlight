﻿
using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{

    /// <summary>${iServer2_Theme_ServerTextStyle_Title}</summary>
    public class ServerTextStyle
    {
        /// <summary>${iServer2_Theme_ServerTextStyle_constructor_None_D}</summary>
        public ServerTextStyle()
        {
            Align = ServerTextAlignment.TopCenter;
            BgColor = new ServerColor() { Red = 255, Green = 0, Blue = 0 };
            Color = new ServerColor() { Red = 255, Green = 0, Blue = 0 };
            FixedSize = true;
            FixedTextSize = 0;
            FontHeight = 10;
            FontWidth = 7;
            FontWeight = 400;
            FontName = "宋体";
            Rotation = 0;
        }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_align_D}</summary>
        public ServerTextAlignment Align { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_bgColor_D}</summary>
        public ServerColor BgColor { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_color_D}</summary>
        public ServerColor Color { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_fixedSize_D}</summary>
        public bool FixedSize { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_fixedTextSize_D}</summary>
        public int FixedTextSize { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_fontHeight_D}</summary>
        public double FontHeight { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_fontWidth_D}</summary>
        public double FontWidth { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_fontWeight_D}</summary>
        public int FontWeight { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_fontName_D}</summary>
        public string FontName { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_bold_D}</summary>
        public bool Bold { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_italic_D}</summary>
        public bool Italic { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_shadow_D}</summary>
        public bool Shadow { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_stroke_D}</summary>
        public bool Stroke { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_outline_D}</summary>
        public bool Outline { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_transparent_D}</summary>
        public bool Transparent { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_underline_D}</summary>
        public bool Underline { get; set; }
        /// <summary>${iServer2_Theme_ServerTextStyle_attribute_rotation_D}</summary>
        public double Rotation { get; set; }


        internal static string ToJson(ServerTextStyle serverTextStyle)
        {
            if (serverTextStyle == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            list.Add(string.Format("\"align\":{0}", (int)serverTextStyle.Align));
            if (ServerColor.ToJson(serverTextStyle.BgColor) != null)
            {
                list.Add(string.Format("\"bgColor\":{0}", ServerColor.ToJson(serverTextStyle.BgColor)));
            }
            else
            {
                list.Add(string.Format("\"bgColor\":null"));
            }
            list.Add(string.Format("\"bold\":{0}", serverTextStyle.Bold));
            if (ServerColor.ToJson(serverTextStyle.Color) != null)
            {
                list.Add(string.Format("\"color\":{0}", ServerColor.ToJson(serverTextStyle.Color)));
            }
            else
            {
                list.Add(string.Format("\"color\":null"));
            }
            list.Add(string.Format("\"fixedSize\":{0}", serverTextStyle.FixedSize));
            list.Add(string.Format("\"fixedTextSize\":{0}", serverTextStyle.FixedTextSize));
            list.Add(string.Format("\"fontHeight\":{0}", serverTextStyle.FontHeight));
            list.Add(string.Format("\"fontWeight\":{0}", serverTextStyle.FontWeight));
            if (serverTextStyle.FontName != null)
            {
                list.Add(string.Format("\"fontName\":{0}", serverTextStyle.FontName));
            }
            list.Add(string.Format("\"fontWidth\":{0}", serverTextStyle.FontWidth));
            list.Add(string.Format("\"italic\":{0}", serverTextStyle.Italic));
            list.Add(string.Format("\"outline\":{0}", serverTextStyle.Outline));
            list.Add(string.Format("\"rotation\":{0}", serverTextStyle.Rotation));
            list.Add(string.Format("\"shadow\":{0}", serverTextStyle.Shadow));
            list.Add(string.Format("\"stroke\":{0}", serverTextStyle.Stroke));
            list.Add(string.Format("\"transparent\":{0}", serverTextStyle.Transparent));
            list.Add(string.Format("\"underline\":{0}", serverTextStyle.Underline));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
        /// <summary>${iServer2_ServerTextStyle_method_FromJson_D}</summary>
        /// <returns>${iServer2_ServerTextStyle_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ServerTextStyle_method_FromJson_param_jsonObject}</param>
        public static ServerTextStyle FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ServerTextStyle result = new ServerTextStyle();

            if (jsonObject["align"] != null)
            {
                result.Align = (ServerTextAlignment)(int)jsonObject["align"];
            }

            result.BgColor = ServerColor.FromJson((JsonObject)jsonObject["bgColor"]);
            result.Bold = (bool)jsonObject["bold"];
            result.Color = ServerColor.FromJson((JsonObject)jsonObject["color"]);
            result.FixedSize = (bool)jsonObject["fixedSize"];
            result.FixedTextSize = (int)jsonObject["fixedTextSize"];
            result.FontHeight = (double)jsonObject["fontHeight"];
            result.FontName = (string)jsonObject["fontName"];
            result.FontWeight = (int)jsonObject["fontWeight"];
            result.FontWidth = (double)jsonObject["fontWidth"];
            result.Italic = (bool)jsonObject["italic"];
            result.Outline = (bool)jsonObject["outline"];
            result.Rotation = (double)jsonObject["rotation"];
            result.Shadow = (bool)jsonObject["shadow"];
            result.Stroke = (bool)jsonObject["stroke"];
            result.Transparent = (bool)jsonObject["transparent"];
            result.Underline = (bool)jsonObject["underline"];

            return result;
        }
    }
}
