﻿using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_Theme_ThemeUniqueItem_Title}</para>
    /// <para>${iServer2_Theme_ThemeUniqueItem_Description}</para>
    /// </summary>
    public class ThemeUniqueItem
    {
        /// <summary>${iServer2_Theme_ThemeUniqueItem_constructor_None_D}</summary>
        public ThemeUniqueItem()
        {
            Visible = true;
            Style = new ServerStyle();
        }
        /// <summary>${iServer2_Theme_ThemeUniqueItem_attribute_caption_D}</summary>
        public string Caption { get; set; }
        /// <summary>${iServer2_Theme_ThemeUniqueItem_attribute_style_D}</summary>
        public ServerStyle Style { get; set; }
        /// <summary>${iServer2_Theme_ThemeUniqueItem_attribute_unique_D}</summary>
        public string Unique { get; set; }
        /// <summary>${iServer2_Theme_ThemeUniqueItem_attribute_visible_D}</summary>
        public bool Visible { get; set; }

        internal static string ToJson(ThemeUniqueItem themeUniqueItem)
        {
            if (themeUniqueItem == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            if (themeUniqueItem.Caption != null)
            {
                list.Add(string.Format("\"caption\":\"{0}\"", themeUniqueItem.Caption));
            }
            if (ServerStyle.ToJson(themeUniqueItem.Style) != null)
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(themeUniqueItem.Style)));
            }
            else
            {
                list.Add(string.Format("\"style\":null"));
            }
            if (themeUniqueItem.Unique != null)
            {
                list.Add(string.Format("\"unique\":\"{0}\"", themeUniqueItem.Unique));
            }
            list.Add(string.Format("\"visible\":{0}", themeUniqueItem.Visible));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
        /// <summary>${iServer2_ThemeUniqueItem_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeUniqueItem_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeUniqueItem_method_FromJson_param_jsonObject}</param>
        public static ThemeUniqueItem FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeUniqueItem result = new ThemeUniqueItem();

            result.Caption = (string)jsonObject["caption"];
            result.Style = ServerStyle.FromJson((JsonObject)jsonObject["style"]);
            result.Unique = (string)jsonObject["unique"];
            result.Visible = (bool)jsonObject["visible"];

            return result;
        }
    }
}
