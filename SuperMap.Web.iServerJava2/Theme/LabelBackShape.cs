﻿

namespace SuperMap.Web.iServerJava2
{
    /// <summary>${iServer2_Theme_LabelBackShape_Title}</summary>
    public enum LabelBackShape
    {
        /// <summary>${iServer2_Theme_LabelBackShape_attribute_none_D}</summary>
        None = 0,
        /// <summary>${iServer2_Theme_LabelBackShape_attribute_rect_D}</summary>
        Rect = 1,
        /// <summary>${iServer2_Theme_LabelBackShape_attribute_roundRect_D}</summary>
        Roundrect = 2,
        /// <summary>${iServer2_Theme_LabelBackShape_attribute_ellipse_D}</summary>
        Ellipse = 3,
        /// <summary>${iServer2_Theme_LabelBackShape_attribute_diamond_D}</summary>
        Diamond = 4,
        /// <summary>${iServer2_Theme_LabelBackShape_attribute_triangle_D}</summary>
        Triangle = 5,
        /// <summary>${iServer2_Theme_LabelBackShape_attribute_marker_D}</summary>
        Marker = 100
    }
}
