﻿using System.Json;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_RemoveThemesResult_Title}</para>
    /// 	<para>${iServer2_Theme_RemoveThemesResult_Description}</para>
    /// </summary>
    public class RemoveThemesResult
    {
        internal RemoveThemesResult()
        { }

        /// <summary>${iServer2_Theme_RemoveThemesResult_attribute_layerKey_D}</summary>
        public string LayerKey { get; private set; }
        /// <summary>${iServer2_RemoveThemesResult_method_FromJson_D}</summary>
        /// <returns>${iServer2_RemoveThemesResult_method_FromJson_return}</returns>
        /// <param name="jsonPrimitive">${iServer2_RemoveThemesResult_method_FromJson_param_jsonObject}</param>
        public static RemoveThemesResult FromJson(JsonPrimitive jsonPrimitive)
        {
            if (jsonPrimitive == null)
            {
                return null;
            }

            return new RemoveThemesResult { LayerKey = jsonPrimitive };
        }
    }
}
