﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_GraduatedMode_Title}</para>
    /// 	<para>${iServer2_Theme_GraduatedMode_Description}</para>
    /// </summary>
    public enum GraduatedMode
    {
        /// <summary>${iServer2_Theme_GraduatedMode_attribute_Constant_D}</summary>
        Constant=0,
        /// <summary>${iServer2_Theme_GraduatedMode_attribute_Squareroot_D}</summary>
        Squareroot=1,
        /// <summary>${iServer2_Theme_GraduatedMode_attribute_Logarithm_D}</summary>
        Logarithm=2
    }
}
