﻿using System.Collections.Generic;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_Theme_ThemeParameters_Title}</para>
    /// <para>${iServer2_Theme_ThemeParameters_Description}</para>
    /// </summary>
    public class ThemeParameters : ParametersBase
    {
        /// <summary>${iServer2_Theme_ThemeParameters_constructor_None_D}</summary>
        public ThemeParameters()
        { JoinItems = new List<JoinItem>(); }

        /// <summary>${iServer2_Theme_ThemeParameters_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        ///// <summary>${iServer2_Theme_ThemeParameters_attribute_themeType_D}</summary>
        //public ThemeType ThemeType { get; set; }
        /// <summary>${iServer2_Theme_ThemeParameters_attribute_theme_D}</summary>
        public Theme Theme { get; set; }
        /// <summary>${iServer2_Theme_ThemeParameters_attribute_joinItems_D}</summary>
        public List<JoinItem> JoinItems
        {
            get;
            private set;
        }   
    }
}
