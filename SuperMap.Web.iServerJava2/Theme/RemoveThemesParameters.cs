﻿
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_RemoveThemesParameters_Title}</para>
    /// 	<para>${iServer2_Theme_RemoveThemesParameters_Description}</para>
    /// </summary>
    public class RemoveThemesParameters : ParametersBase
    {
        /// <summary>${iServer2_Theme_RemoveThemesParameters_constructor_None_D}</summary>
        public RemoveThemesParameters()
        { }
        /// <summary>${iServer2_Theme_ThemeParameters_attribute_layerNames_D}</summary>
        public IList<string> LayerNames { get; set; }
    }
}
