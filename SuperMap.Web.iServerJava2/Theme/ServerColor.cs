﻿using System.Collections.Generic;
using System.Json;
using System;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// <para>${iServer2_Theme_ServerColor_Title}</para>
    /// <para>${iServer2_Theme_ServerColor_Description}</para>
    /// </summary>

    public class ServerColor
    {
        private int blue;
        private int green;
        private int red;

        /// <summary>${iServer2_Theme_ServerColor_constructor_None_D}</summary>
        /// <overloads>${iServer2_Theme_ServerColor_constructor_overloads_D}</overloads>
        public ServerColor()
        {
            Blue = 0;
            Green = 0;
            Red = 255;
        }
        /// <summary>${iServer2_Theme_ServerColor_constructor_Int_D}</summary>
        public ServerColor(int red, int green, int blue)
        {
            Red = red;
            Green = green;
            Blue = blue;
        }

        /// <summary>${iServer2_Theme_ServerColor_attribute_Blue_D}</summary>
        public int Blue
        {
            get
            {
                return blue;
            }
            set
            {
                if (value > 255)
                {
                    value = 255;
                }
                if (value < 0)
                {
                    value = 0;
                }
                blue = value;
            }
        }
        /// <summary>${iServer2_Theme_ServerColor_attribute_Green_D}</summary>
        public int Green
        {
            get
            {
                return green;
            }
            set
            {
                if (value > 255)
                {
                    value = 255;
                }
                if (value < 0)
                {
                    value = 0;
                }
                green = value;
            }
        }
        /// <summary>${iServer2_Theme_ServerColor_attribute_Red_D}</summary>
        public int Red
        {
            get
            {
                return red;
            }
            set
            {
                if (value > 255)
                {
                    value = 255;
                }
                if (value < 0)
                {
                    value = 0;
                }
                red = value;
            }
        }

        internal static string ToJson(ServerColor serverColor)
        {
            if (serverColor == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            list.Add(string.Format("\"red\":{0}", serverColor.Red));
            list.Add(string.Format("\"green\":{0}", serverColor.Green));
            list.Add(string.Format("\"blue\":{0}", serverColor.Blue));
            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
        /// <summary>${iServer2_ServerColor_method_FromJson_D}</summary>
        /// <returns>${iServer2_ServerColor_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ServerColor_method_FromJson_param_jsonObject}</param>
        public static ServerColor FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ServerColor result = new ServerColor()
            {
                Red = Convert.ToInt32((string)jsonObject["red"]),
                Green = Convert.ToInt32((string)jsonObject["green"]),
                Blue = Convert.ToInt32((string)jsonObject["blue"])
            };
            return result;
        }
    }
}
