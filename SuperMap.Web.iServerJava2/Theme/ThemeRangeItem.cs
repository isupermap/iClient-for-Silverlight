﻿using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeRangeItem_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeRangeItem_Description}</para>
    /// </summary>
    public class ThemeRangeItem
    {
        /// <summary>${iServer2_Theme_ThemeRangeItem_constructor_None_D}</summary>
        public ThemeRangeItem()
        {
            Visible = true;
            Style = new ServerStyle();
        }
        /// <summary>${iServer2_Theme_ThemeRangeItem_attribute_caption_D}</summary>
        public string Caption { get; set; }
        /// <summary>${iServer2_Theme_ThemeRangeItem_attribute_start_D}</summary>
        public double Start { get; set; }
        /// <summary>${iServer2_Theme_ThemeRangeItem_attribute_end_D}</summary>
        public double End { get; set; }
        /// <summary>${iServer2_Theme_ThemeRangeItem_attribute_visible_D}</summary>
        public bool Visible { get; set; }
        /// <summary>${iServer2_Theme_ThemeRangeItem_attribute_style_D}</summary>
        public ServerStyle Style { get; set; }

        internal static string ToJson(ThemeRangeItem themeRangeItem)
        {
            if (themeRangeItem == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            if (themeRangeItem.Caption != null)
            {
                list.Add(string.Format("\"caption\":\"{0}\"", themeRangeItem.Caption));
            }

            if (ServerStyle.ToJson(themeRangeItem.Style) != null)
            {
                list.Add(string.Format("\"style\":{0}", ServerStyle.ToJson(themeRangeItem.Style)));
            }
            else
            {
                list.Add(string.Format("\"style\":null"));
            }

            list.Add(string.Format("\"start\":{0}", themeRangeItem.Start));
            list.Add(string.Format("\"end\":{0}", themeRangeItem.End));
            list.Add(string.Format("\"visible\":{0}", themeRangeItem.Visible));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
        /// <summary>${iServer2_ThemeRangeItem_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeRangeItem_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeRangeItem_method_FromJson_param_jsonObject}</param>
        public static ThemeRangeItem FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeRangeItem result = new ThemeRangeItem();
            result.Caption = (string)jsonObject["caption"];
            result.Start = (double)jsonObject["start"];
            result.End = (double)jsonObject["end"];
            result.Visible = (bool)jsonObject["visible"];
            result.Style = ServerStyle.FromJson((JsonObject)jsonObject["style"]);

            return result;
        }
    }
}
