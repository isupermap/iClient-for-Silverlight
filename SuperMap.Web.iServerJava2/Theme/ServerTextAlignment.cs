﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ServerTextAlignment_Title}</para>
    /// 	<para>${iServer2_Theme_ServerTextAlignment_Description}</para>
    /// </summary>
    public enum ServerTextAlignment
    {
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_TopLeft_D}</para>
        /// 	<para><img src="ServerTextAlignmentTopLeft.bmp"/></para>
        /// </summary>
        TopLeft=0,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_TopCenter_D}</para>
        /// 	<para><img src="ServerTextAlignmentTopCenter.bmp"/></para>
        /// </summary>
        TopCenter=1,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_TopRight_D}</para>
        /// 	<para><img src="ServerTextAlignmentTopRight.bmp"/></para>
        /// </summary>
        TopRight=2,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_BaseLineLeft_D}</para>
        /// 	<para><img src="ServerTextAlignmentBaseLineLeft.bmp"/></para>
        /// </summary>
        BaseLineLeft=3,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_BaseLineCenter_D}</para>
        /// 	<para><img src="ServerTextAlignmentBaseLineCenter.bmp"/></para>
        /// </summary>
        BaseLineCenter=4,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_BaseLineRight_D}</para>
        /// 	<para><img src="ServerTextAlignmentBaseLineRight.bmp"/></para>
        /// </summary>
        BaseLineRight=5,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_BottomLeft_D}</para>
        /// 	<para><img src="ServerTextAlignmentBottomLeft.bmp"/></para>
        /// </summary>
        BottomLeft=6,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_BottomCenter_D}</para>
        /// 	<para><img src="ServerTextAlignmentBottomCenter.bmp"/></para>
        /// </summary>
        BottomCenter=7,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_BottomRight_D}</para>
        /// 	<para><img src="ServerTextAlignmentBottomRight.bmp"/></para>
        /// </summary>
        BottomRight=8,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_MiddleLeft_D}</para>
        /// 	<para><img src="ServerTextAlignmentMiddleLeft.bmp"/></para>
        /// </summary>
        MiddleLeft=9,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_MiddleCenter_D}</para>
        /// 	<para><img src="ServerTextAlignmentMiddleCenter.bmp"/></para>
        /// </summary>
        MiddleCenter=10,
        /// <summary>
        /// 	<para>${iServer2_Theme_ServerTextAlignment_MiddleRight_D}</para>
        /// 	<para><img src="ServerTextAlignmentMiddleRight.bmp"/></para>
        /// </summary>
        MiddleRight=11,
    }
}
