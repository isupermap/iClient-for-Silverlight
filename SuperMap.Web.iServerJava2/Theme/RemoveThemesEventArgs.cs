﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_RemoveThemesEventArgs_Title}</para>
    /// 	<para>${iServer2_Theme_RemoveThemesEventArgs_Description}</para>
    /// </summary>
    public class RemoveThemesEventArgs:ServiceEventArgs
    {
        internal RemoveThemesEventArgs(RemoveThemesResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServer2_Theme_RemoveThemesEventArgs_attribute_Result_D}</summary>
        public RemoveThemesResult Result { get; private set; }
        /// <summary>${iServer2_Theme_RemoveThemesEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
