﻿using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Theme_ThemeRangeParam_Title}</para>
    /// 	<para>${iServer2_Theme_ThemeRangeParam_Description}</para>
    /// </summary>
    public class ThemeRangeParam
    {
        /// <summary>${iServer2_Theme_ThemeRangeParam_constructor_None_D}</summary>
        public ThemeRangeParam()
        {
            ColorGradientType = ColorGradientType.YellowGreen;
            RangeMode = RangeMode.Equalinterval;
            RangeParameter = 3;
        }
        /// <summary>${iServer2_Theme_ThemeRangeParam_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${iServer2_Theme_ThemeRangeParam_attribute_colorGradientType_D}</summary>
        public ColorGradientType ColorGradientType { get; set; }
        /// <summary>${iServer2_Theme_ThemeRangeParam_attribute_rangeMode_D}</summary>
        public RangeMode RangeMode { get; set; }
        /// <summary>${iServer2_Theme_ThemeRangeParam_attribute_rangeParameter_D}</summary>
        public double RangeParameter { get; set; }

        internal static string ToJson(ThemeRangeParam themeRangeParam)
        {
            if (themeRangeParam == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            if (themeRangeParam.LayerName != null)
            {
                list.Add(string.Format("\"layerName\":\"{0}\"", themeRangeParam.LayerName));
            }

            list.Add(string.Format("\"colorGradientType\":{0}", (int)themeRangeParam.ColorGradientType));
            list.Add(string.Format("\"rangeMode\":{0}", (int)themeRangeParam.RangeMode));
            list.Add(string.Format("\"rangeParameter\":{0}", themeRangeParam.RangeParameter));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
        /// <summary>${iServer2_ThemeRangeParam_method_FromJson_D}</summary>
        /// <returns>${iServer2_ThemeRangeParam_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ThemeRangeParam_method_FromJson_param_jsonObject}</param>
        public static ThemeRangeParam FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ThemeRangeParam result = new ThemeRangeParam();

            result.LayerName = (string)jsonObject["layerName"];
            if (jsonObject["colorGradientType"] != null)
            {
                result.ColorGradientType = (ColorGradientType)(int)jsonObject["colorGradientType"];
            }
            if (jsonObject["rangeMode"] != null)
            {
                result.RangeMode = (RangeMode)(int)jsonObject["rangeMode"];
            }
            result.RangeParameter = (double)jsonObject["rangeParameter"];

            return result;
        }
    }
}
