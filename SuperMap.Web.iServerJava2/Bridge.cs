﻿using System.Collections.Generic;
using System.Text;
using System.Globalization;
using SuperMap.Web.Core;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Utility_Title}</para>
    /// 	<para>${iServer2_Utility_Description}</para>
    /// </summary>
    public static class Bridge
    {
        /// <summary>${iServer2_Utility_method_creatParams_D}</summary>
        /// <returns>${iServer2_Utility_method_creatParams_return}</returns>
        /// <param name="method">${iServer2_Utility_method_creatParams_param_method}</param>
        /// <param name="dict">${iServer2_Utility_method_creatParams_param_dict}</param>
        public static string CreateParams(string method, Dictionary<string, string> dict)
        {
            StringBuilder paramsBuilder = new StringBuilder();
            paramsBuilder.Append("<uis><command>");
            paramsBuilder.AppendFormat("<name>{0}</name>", method);
            paramsBuilder.Append("<parameter>");
            foreach (string key in dict.Keys)
            {
                paramsBuilder.AppendFormat("<{0}>{1}</{0}>", new object[] { key, dict[key] });
            }
            paramsBuilder.Append("<needHighlight>false</needHighlight>");
            paramsBuilder.Append("<customParam></customParam>");
            paramsBuilder.Append("</parameter>");
            paramsBuilder.Append("</command></uis>");

            return paramsBuilder.ToString();
        }

        /// <param name="geo">${iServer2_Utility_method_ToGeometry_param_geo}</param>
        /// <returns>${iServer2_Utility_method_ToGeometry_return}</returns>
        public static Geometry ToGeometry(this ServerGeometry geo)
        {

            if (geo == null)
            {
                return null;
            }

            if (geo.Feature == ServerFeatureType.Point)
            {
                return geo.ToGeoPoint();
            }
            else if (geo.Feature == ServerFeatureType.Polygon)
            {
                return geo.ToGeoRegion();
            }
            else if (geo.Feature == ServerFeatureType.Line || geo.Feature == ServerFeatureType.LineM)
            {
                return geo.ToGeoLine();
            }
            return null;
        }

        /// <summary>${iServer2_Utility_method_GeometryToServerGeometry_D}</summary>
        /// <param name="geo">${iServer2_Utility_method_GeometryToServerGeometry_param_geo}</param>
        /// <returns>${iServer2_Utility_method_GeometryToServerGeometry_return}</returns>
        public static ServerGeometry ToServerGeometry(this Geometry geo)
        {
            if (geo == null)
            {
                return null;
            }

            ServerGeometry sg = new ServerGeometry();

            Point2DCollection list = new Point2DCollection();
            List<int> parts = new List<int>();

            if (geo is GeoRegion)
            {
                for (int i = 0; i < ((GeoRegion)geo).Parts.Count; i++)
                {
                    for (int j = 0; j < ((GeoRegion)geo).Parts[i].Count; j++)
                    {
                        list.Add(new Point2D(((GeoRegion)geo).Parts[i][j].X, ((GeoRegion)geo).Parts[i][j].Y));
                    }
                    parts.Add(((GeoRegion)geo).Parts[i].Count);
                }
                sg.Feature = ServerFeatureType.Polygon;
            }
            
            if (geo is GeoCircle)
            {
                for (int i = 0; i < ((GeoCircle)geo).Parts.Count; i++)
                {
                    for (int j = 0; j < ((GeoCircle)geo).Parts[i].Count; j++)
                    {
                        list.Add(new Point2D(((GeoCircle)geo).Parts[i][j].X, ((GeoCircle)geo).Parts[i][j].Y));
                    }
                    parts.Add(((GeoCircle)geo).Parts[i].Count);
                }
                sg.Feature = ServerFeatureType.Polygon;
            }

            if (geo is GeoLine)
            {
                for (int i = 0; i < ((GeoLine)geo).Parts.Count; i++)
                {
                    for (int j = 0; j < ((GeoLine)geo).Parts[i].Count; j++)
                    {
                        list.Add(new Point2D(((GeoLine)geo).Parts[i][j].X, ((GeoLine)geo).Parts[i][j].Y));
                    }
                    parts.Add(((GeoLine)geo).Parts[i].Count);
                }
                sg.Feature = ServerFeatureType.Line;
            }

            if (geo is GeoPoint)
            {
                list.Add(new Point2D(((GeoPoint)geo).X, ((GeoPoint)geo).Y));
                parts.Add(list.Count);
                sg.Feature = ServerFeatureType.Point;
            }

            sg.Point2Ds = list;
            sg.Parts = parts;
            sg.Id = -1;
            return sg;
        }

        internal static string EncodeString(string value)
        {
            StringBuilder builder = null;
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            int startIndex = 0;
            int count = 0;
            for (int i = 0; i < value.Length; i++)
            {
                char c = value[i];
                if ((((c == '\r') || (c == '\t')) || ((c == '"') || (c == '\''))) || ((((c == '<') || (c == '>')) || ((c == '\\') || (c == '\n'))) || (((c == '\b') || (c == '\f')) || (c < ' '))))
                {
                    if (builder == null)
                    {
                        builder = new StringBuilder(value.Length + 5);
                    }
                    if (count > 0)
                    {
                        builder.Append(value, startIndex, count);
                    }
                    startIndex = i + 1;
                    count = 0;
                }
                switch (c)
                {
                    case '<':
                        {
                            builder.Append(@"&lt;");
                            continue;
                        }
                    case '>':
                        {
                            builder.Append(@"&gt;");
                            continue;
                        }
                    case '\'':
                        {
                            AppendCharAsUnicode(builder, c);
                            continue;
                        }
                    case '\\':
                        {
                            builder.Append(@"\\");
                            continue;
                        }
                    case '\b':
                        {
                            builder.Append(@"\b");
                            continue;
                        }
                    case '\t':
                        {
                            builder.Append(@"\t");
                            continue;
                        }
                    case '\n':
                        {
                            builder.Append(@"\n");
                            continue;
                        }
                    case '\f':
                        {
                            builder.Append(@"\f");
                            continue;
                        }
                    case '\r':
                        {
                            builder.Append(@"\r");
                            continue;
                        }
                    case '"':
                        {
                            builder.Append("\\\"");
                            continue;
                        }
                }
                if (c < ' ')
                {
                    AppendCharAsUnicode(builder, c);
                }
                else
                {
                    count++;
                }
            }
            if (builder == null)
            {
                return value;
            }
            if (count > 0)
            {
                builder.Append(value, startIndex, count);
            }
            return builder.ToString();
        }

        private static void AppendCharAsUnicode(StringBuilder builder, char c)
        {
            builder.Append(@"\u");
            builder.AppendFormat(CultureInfo.InvariantCulture, "{0:x4}", new object[] { (int)c });
        }
    }
}
