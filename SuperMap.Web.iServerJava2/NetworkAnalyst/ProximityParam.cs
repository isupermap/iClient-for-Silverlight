﻿
using System.Collections.Generic;
using System.Text;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ProximityParam_Title}</para>
    /// 	<para>${iServer2_ProximityParam_Description}</para>
    /// </summary>
    public class ProximityParam
    {
        /// <summary>${iServer2_ProximityParam_constructor_None_D}</summary>
        public ProximityParam()
        {
            FacilityCount = 1;
            IsFromEvent = false;
            MaxImpedance = 0;
        }
        /// <summary>${iServer2_PathParam_attribute_networkAnalystParam_D}</summary>
        public NetworkAnalystParam NetworkAnalystParam { get; set; }
        /// <summary>${iServer2_ProximityParam_attribute_isFromEvent_D}</summary>
        public bool IsFromEvent { get; set; }
        /// <summary>${iServer2_ProximityParam_attribute_maxImpedance_D}</summary>
        public double MaxImpedance { get; set; }
        /// <summary>${iServer2_ProximityParam_attribute_facilityCount_D}</summary>
        public int FacilityCount { get; set; }

        internal static string ToJson(ProximityParam proximityParam)
        {
            if (proximityParam == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();
            StringBuilder stringBuilder = new StringBuilder();

            if (NetworkAnalystParam.ToJson(proximityParam.NetworkAnalystParam) != null)
            {
                stringBuilder.Append("\"networkAnalystParam\"");
                stringBuilder.Append(":");
                stringBuilder.Append(NetworkAnalystParam.ToJson(proximityParam.NetworkAnalystParam));
                list.Add(stringBuilder.ToString());
            }

            list.Add(string.Format("\"isFromEvent\":{0}", proximityParam.IsFromEvent));
            list.Add(string.Format("\"maxImpedance\":{0}", proximityParam.MaxImpedance));
            list.Add(string.Format("\"facilityCount\":{0}", proximityParam.FacilityCount));

            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }
    }
}
