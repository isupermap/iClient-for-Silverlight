﻿
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_NetworkAnalystEventArgs_Title}</para>
    /// 	<para>${iServer2_NetworkAnalystEventArgs_Description}</para>
    /// </summary>
    public class NetworkAnalystEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer2_NetworkAnalystEventArgs_constructor_D}</summary>
        public NetworkAnalystEventArgs(NetworkAnalystResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServer2_NetworkAnalystEventArgs_attribute_result_D}</summary>
        public NetworkAnalystResult Result { get; private set; }
        /// <summary>${iServer2_NetworkAnalystEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }

    }
}
