﻿

using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Json;
using System.Windows;
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ServiceAreaService_Title}</para>
    /// 	<para>${iServer2_ServiceAreaService_Description}</para>
    /// </summary>
    public class ServiceAreaService : ServiceBase
    {
        /// <overloads>${iServer2_ServiceAreaService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_ServiceAreaService_constructor_None_D}</summary>
        public ServiceAreaService()
        { }
        /// <summary>${iServer2_ServiceAreaService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_ServiceAreaService_constructor_String_param_url}</param>
        public ServiceAreaService(string url)
            : base(url)
        { }
        /// <overloads>${iServer2_ServiceAreaService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_ServiceAreaService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_ServiceAreaService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(ServiceAreaParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_ServiceAreaService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_ServiceAreaService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_ServiceAreaService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(ServiceAreaParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("NetworkAnalystParameters is Null");
                throw new ArgumentNullException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                // throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private System.Collections.Generic.Dictionary<string, string> GetParameters(ServiceAreaParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string method = "ServiceArea";

            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("mapName", parameters.MapName);
            dict.Add("networkSetting", NetworkModelSetting.ToJson(parameters.NetworkSetting));
            dict.Add("serviceAreaParam", ServiceAreaParam.ToJson(parameters.ServiceAreaParam));

            dictionary.Add("params", Bridge.CreateParams(method, dict));
            return dictionary;

        }


        private ServiceAreaResult lastResult;

        /// <summary>${iServer2_ServiceAreaService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<ServiceAreaEventArgs> ProcessCompleted;
        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            ServiceAreaResult result = ServiceAreaResult.FromJson(jsonObject);
            LastResult = result;
            ServiceAreaEventArgs args = new ServiceAreaEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(ServiceAreaEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${iServer2_ServiceAreaService_attribute_lastResult_D}</summary>
        public ServiceAreaResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}

