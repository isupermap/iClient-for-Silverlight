﻿

using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ServiceAreaResult_Title}</para>
    /// 	<para>${iServer2_ServiceAreaResult_Description}</para>
    /// </summary>
    public class ServiceAreaResult
    {
        internal ServiceAreaResult()
        { }

        /// <summary>${iServer2_ServiceAreaResult_attribute_areaRegions_D}</summary>
        public List<ServerGeometry> AreaRegions { get; private set; }
        /// <summary>${iServer2_NetworkAnalystResult_attribute_edges_D}</summary>
        public List<List<int>> Edges { get; private set; }
        /// <summary>${iServer2_NetworkAnalystResult_attribute_message_D}</summary>
        public string Message { get; private set; }
        /// <summary>${iServer2_NetworkAnalystResult_attribute_nodes_D}</summary>
        public List<List<int>> Nodes { get; private set; }
        /// <summary>${iServer2_NetworkAnalystResult_attribute_pathGuides_D}</summary>
        public List<PathGuide> PathGuides { get; private set; }
        /// <summary>${iServer2_NetworkAnalystResult_attribute_stops_D}</summary>
        public List<List<int>> Stops { get; private set; }
        /// <summary>${iServer2_NetworkAnalystResult_attribute_weights_D}</summary>
        public List<double> Weights { get; private set; }
        /// <summary>${iServer2_NetworkAnalystResult_attribute_paths_D}</summary>
        public List<ServerGeometry> Paths { get; private set; }
        /// <summary>${iServer2_ServiceAreaResult_method_FromJson_D}</summary>
        /// <returns>${iServer2_ServiceAreaResult_method_FromJson_return}</returns>
        /// <param name="jsonObject">${iServer2_ServiceAreaResult_method_FromJson_param_jsonObject}</param>
        public static ServiceAreaResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            ServiceAreaResult result = new ServiceAreaResult();

            #region AreaRegions
            JsonArray pathsInJson = (JsonArray)jsonObject["areaRegions"];
            if (pathsInJson != null && pathsInJson.Count > 0)
            {
                result.AreaRegions = new List<ServerGeometry>();
                for (int i = 0; i < pathsInJson.Count; i++)
                {
                    ServerGeometry sg = ServerGeometry.FromJson((JsonObject)pathsInJson[i]);
                    result.AreaRegions.Add(sg);
                }
            }
            #endregion        

            #region Edges
            JsonArray edgesInJson = (JsonArray)jsonObject["edges"];
            if (edgesInJson != null && edgesInJson.Count > 0)
            {
                result.Edges = new List<List<int>>();

                for (int i = 0; i < edgesInJson.Count; i++)
                {
                    List<int> l = new List<int>();
                    for (int j = 0; j < edgesInJson[i].Count; j++)
                    {
                        l.Add((int)edgesInJson[i][j]);
                    }
                    result.Edges.Add(l);
                }
            }
            #endregion

            #region Nodes
            JsonArray nodesInJson = (JsonArray)jsonObject["nodes"];
            if (nodesInJson != null && nodesInJson.Count > 0)
            {
                result.Nodes = new List<List<int>>();
                for (int i = 0; i < nodesInJson.Count; i++)
                {
                    List<int> l = new List<int>();
                    for (int j = 0; j < nodesInJson[i].Count; j++)
                    {
                        l.Add((int)nodesInJson[i][j]);
                    }
                    result.Nodes.Add(l);
                }
            }
            #endregion

            #region PathGuides
            JsonArray pathGuidesInJson = (JsonArray)jsonObject["pathGuides"];
            if (pathGuidesInJson != null && pathGuidesInJson.Count > 0)
            {
                result.PathGuides = new List<PathGuide>();
                for (int i = 0; i < pathGuidesInJson.Count; i++)
                {
                    PathGuide pg = PathGuide.FromJson((JsonObject)pathGuidesInJson[i]);
                    result.PathGuides.Add(pg);
                }
            }
            #endregion

            #region Stops
            JsonArray stopsInJson = (JsonArray)jsonObject["stops"];
            if (stopsInJson != null && stopsInJson.Count > 0)
            {
                result.Stops = new List<List<int>>();

                for (int i = 0; i < stopsInJson.Count; i++)
                {
                    List<int> l = new List<int>();
                    for (int j = 0; j < stopsInJson[i].Count; j++)
                    {
                        l.Add(stopsInJson[i][j]);
                    }
                    result.Stops.Add(l);
                }
            }
            #endregion

            #region Weights
            JsonArray weightsInJson = (JsonArray)jsonObject["weights"];
            if (weightsInJson != null && weightsInJson.Count > 0)
            {
                result.Weights = new List<double>();
                for (int i = 0; i < weightsInJson.Count; i++)
                {
                    result.Weights.Add((double)weightsInJson[i]);
                }
            }
            #endregion

            #region Message
            result.Message = (string)jsonObject["message"];
            #endregion

            #region Paths
            JsonArray p = (JsonArray)jsonObject["paths"];
            if (p != null && p.Count > 0)
            {
                result.Paths = new List<ServerGeometry>();
                for (int i = 0; i < p.Count; i++)
                {
                    ServerGeometry sg = ServerGeometry.FromJson((JsonObject)p[i]);
                    result.Paths.Add(sg);
                }
            }
            #endregion

 
            return result;
        }
    }
}
