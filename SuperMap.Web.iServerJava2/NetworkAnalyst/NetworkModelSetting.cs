﻿using System.Collections.Generic;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_NetworkModelSetting_Title}</para>
    /// 	<para>${iServer2_NetworkModelSetting_Description}</para>
    /// </summary>
    public class NetworkModelSetting
    {
        /// <summary>${iServer2_NetworkModelSetting_constructor_None_D}</summary>
        public NetworkModelSetting()
        {
            Tolerance = 100.0;
        }
        /// <summary>${iServer2_NetworkModelSetting_attribute_networkDatasetName_D}</summary>
        public string NetworkDatasetName { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_networkDataSourceName_D}</summary>
        public string NetworkDataSourceName { get; set; }
        /// <summary>
        /// 	<para>${iServer2_NetworkModelSetting_attribute_tolerance_D}</para>
        /// 	<para><img src="tolerance.png"/></para>
        /// </summary>
        public double Tolerance { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_nodeIDField_D}</summary>
        public string NodeIDField { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_edgeIDField_D}</summary>
        public string EdgeIDField { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_weightFieldInfos_D}</summary>
        public IList<WeightFieldInfo> WeightFieldInfos { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_tNodeIDField_D}</summary>
        public string TNodeIDField { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_fNodeIDField_D}</summary>
        public string FNodeIDField { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_nodeNameField_D}</summary>
        public string NodeNameField { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_edgeNameField_D}</summary>
        public string EdgeNameField { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_barrierEdges_D}</summary>
        public IList<int> BarrierEdges { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_barrierNodes_D}</summary>
        public IList<int> BarrierNodes { get; set; }
        /// <summary>${iServer2_NetworkModelSetting_attribute_turnTableSetting_D}</summary>
        public TurnTableSetting TurnTableSetting { get; set; }

        internal static string ToJson(NetworkModelSetting networkModelSetting)
        {
            if (networkModelSetting == null)
            {
                return null;
            }

            string json = "{";

            List<string> list = new List<string>();
            if (!string.IsNullOrEmpty(networkModelSetting.NetworkDatasetName))
            {
                list.Add(string.Format("\"networkDatasetName\":\"{0}\"", networkModelSetting.NetworkDatasetName));
            }


            if (!string.IsNullOrEmpty(networkModelSetting.NetworkDataSourceName))
            {
                list.Add(string.Format("\"networkDataSourceName\":\"{0}\"", networkModelSetting.NetworkDataSourceName));
            }


            list.Add(string.Format("\"tolerance\":\"{0}\"", networkModelSetting.Tolerance));

            if (!string.IsNullOrEmpty(networkModelSetting.NodeIDField))
            {
                list.Add(string.Format("\"nodeIDField\":\"{0}\"", networkModelSetting.NodeIDField));
            }

            if (!string.IsNullOrEmpty(networkModelSetting.EdgeIDField))
            {
                list.Add(string.Format("\"edgeIDField\":\"{0}\"", networkModelSetting.EdgeIDField));
            }

            if (networkModelSetting.WeightFieldInfos != null)
            {
                List<string> infos = new List<string>();
                foreach (WeightFieldInfo info in networkModelSetting.WeightFieldInfos)
                {
                    infos.Add(WeightFieldInfo.ToJson(info));
                }
                list.Add(string.Format("\"weightFieldInfos\":[{0}]", string.Join(",", infos.ToArray())));
            }

            if (!string.IsNullOrEmpty(networkModelSetting.TNodeIDField))
            {
                list.Add(string.Format("\"tNodeIDField\":\"{0}\"", networkModelSetting.TNodeIDField));
            }

            if (!string.IsNullOrEmpty(networkModelSetting.FNodeIDField))
            {
                list.Add(string.Format("\"fNodeIDField\":\"{0}\"", networkModelSetting.FNodeIDField));
            }

            if (!string.IsNullOrEmpty(networkModelSetting.NodeNameField))
            {
                list.Add(string.Format("\"nodeNameField\":\"{0}\"", networkModelSetting.NodeNameField));
            }
            else
            {
                list.Add("\"nodeNameField\":\"\"");
            }

            if (!string.IsNullOrEmpty(networkModelSetting.EdgeNameField))
            {
                list.Add(string.Format("\"edgeNameField\":\"{0}\"", networkModelSetting.EdgeNameField));
            }
            else
            {
                list.Add("\"edgeNameField\":\"\"");
            }

            if (networkModelSetting.BarrierEdges != null && networkModelSetting.BarrierEdges.Count > 0)
            {
                List<string> barrieredges = new List<string>();
                foreach (int i in networkModelSetting.BarrierEdges)
                {
                    barrieredges.Add(i.ToString());
                }
                list.Add(string.Format("\"barrierEdges\":[{0}]", string.Join(",", barrieredges.ToArray())));
            }
            else if (networkModelSetting.BarrierEdges == null)
            {
                list.Add("\"barrierEdges\":null");
            }


            if (networkModelSetting.BarrierNodes != null && networkModelSetting.BarrierNodes.Count > 0)
            {
                List<string> barriernodes = new List<string>();
                foreach (int ii in networkModelSetting.BarrierNodes)
                {
                    barriernodes.Add(ii.ToString());
                }
                list.Add(string.Format("\"barrierNodes\":[{0}]", string.Join(",", barriernodes.ToArray())));
            }
            else if (networkModelSetting.BarrierNodes == null)
            {
                list.Add("\"barrierNodes\":null");
            }

            if (networkModelSetting.TurnTableSetting != null)
            {
                list.Add(string.Format("\"turnTableSetting\":[{0}]", TurnTableSetting.ToJson(networkModelSetting.TurnTableSetting)));
            }
            else if (networkModelSetting.TurnTableSetting == null)
            {
                list.Add("\"turnTableSetting\":null");
            }

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
    }
}
