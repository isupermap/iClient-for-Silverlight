﻿

using System;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using System.Windows;
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_FindTSPPathService_Title}</para>
    /// 	<para>${iServer2_FindTSPPathService_Description}</para>
    /// </summary>
    public class FindTSPPathService : ServiceBase
    {
        /// <overloads>${iServer2_FindTSPPathService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_FindTSPPathService_constructor_None_D}</summary>
        public FindTSPPathService()
        { }
        /// <summary>${iServer2_FindTSPPathService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_FindTSPPathService_constructor_String_param_url}</param>
        public FindTSPPathService(string url)
            : base(url)
        { }
        /// <summary>${iServer2_FindTSPPathService_method_ProcessAsync_D}</summary>
        /// <overloads>${iServer2_FindTSPPathService_method_ProcessAsync_overloads_D}</overloads>
        public void ProcessAsync(FindTSPPathParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_FindTSPPathService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_FindTSPPathService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_FindTSPPathService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(FindTSPPathParameters parameters, object state)
        {
            if (parameters == null)
            {
                //TODO:资源
                //throw new ArgumentNullException("NetworkAnalystParameters is Null");
                throw new ArgumentNullException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(this.Url))
            {
                //TODO:资源
                // throw new InvalidOperationException("Url is not set");
                throw new InvalidOperationException(SuperMap.Web.iServerJava2.Resources.ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private System.Collections.Generic.Dictionary<string, string> GetParameters(FindTSPPathParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();


            string method = "FindTSPPath";

            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("mapName", parameters.MapName);
            dict.Add("networkSetting", NetworkModelSetting.ToJson(parameters.NetworkSetting));
            dict.Add("tspPathParam", TSPPathParam.ToJson(parameters.TSPPathParam));

            dictionary.Add("params", Bridge.CreateParams(method, dict));
            return dictionary;

        }


        private NetworkAnalystResult lastResult;

        /// <summary>${iServer2_FindTSPPathService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<NetworkAnalystEventArgs> ProcessCompleted;
        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            NetworkAnalystResult result = NetworkAnalystResult.FromJson(jsonObject);
            LastResult = result;
            NetworkAnalystEventArgs args = new NetworkAnalystEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);
        }
        private void OnProcessCompleted(NetworkAnalystEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }
        /// <summary>${iServer2_FindTSPPathService_attribute_lastResult_D}</summary>
        public NetworkAnalystResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
