﻿

using System.Collections.Generic;
using SuperMap.Web.Utilities;
using System.Collections;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ServiceAreaParam_Title}</para>
    /// 	<para>${iServer2_ServiceAreaParam_Description}</para>
    /// </summary>
    public class ServiceAreaParam
    {
        /// <summary>${iServer2_ServiceAreaParam_constructor_None_D}</summary>
        public ServiceAreaParam()
        {
            IsFromCenter = true;
        }
        /// <summary>${iServer2_ServiceAreaParam_attribute_weights_D}</summary>
        public IList<int> Weights { get; set; }
        /// <summary>${iServer2_ServiceAreaParam_attribute_isFromCenter_D}</summary>
        public bool IsFromCenter { get; set; }
        //是否中心点互斥。为true，表示若两个或多个相邻的服务区有交集，则将它们进行互斥处理。
        /// <summary>${iServer2_ServiceAreaParam_attribute_isCenterMutuallyExclusive_D}</summary>
        public bool IsCenterMutuallyExclusive { get; set; }

        /// <summary>${iServer2_ServiceAreaParam_attribute_networkAnalystParam_D}</summary>
        public NetworkAnalystParam NetworkAnalystParam { get; set; }

        internal static string ToJson(ServiceAreaParam param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"networkAnalystParam\":{0}", NetworkAnalystParam.ToJson(param.NetworkAnalystParam)));
            list.Add(string.Format("\"isCenterMutuallyExclusive\":{0}", param.IsCenterMutuallyExclusive));
            list.Add(string.Format("\"isFromCenter\":{0}", param.IsFromCenter));

            list.Add(string.Format("\"weights\":{0}", JsonHelper.FromIList((IList)param.Weights)));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
    }
}
