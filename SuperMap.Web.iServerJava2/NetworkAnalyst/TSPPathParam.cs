﻿
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava2
{
    //该类描述旅行商分析中所用到的网络分析参数和是否指定终点标示。
    /// <summary>
    /// 	<para>${iServer2_TSPPathParam_Title}</para>
    /// 	<para>${iServer2_TSPPathParam_Description}</para>
    /// </summary>
    public class TSPPathParam
    {
        /// <summary>${iServer2_TSPPathParam_constructor_None_D}</summary>
        public TSPPathParam()
        { }
        //是否指定旅行终点。默认为不指定，则按照代价最小的原则迭代得到旅行的最佳路线。
        //如果指定旅行终点，则相当于在原有计算基础上加了一个限制条件。
        /// <summary>${iServer2_TSPPathParam_attribute_isNodeEndAssigned_D}</summary>
        public bool IsEndNodeAssigned { get; set; }

        /// <summary>${iServer2_TSPPathParam_attribute_networkAnalystParam_D}</summary>
        public NetworkAnalystParam NetworkAnalystParam { get; set; }

        internal static string ToJson(TSPPathParam param)
        {
            if (param == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"networkAnalystParam\":{0}", NetworkAnalystParam.ToJson(param.NetworkAnalystParam)));
            list.Add(string.Format("\"isEndNodeAssigned\":{0}", param.IsEndNodeAssigned));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
    }
}
