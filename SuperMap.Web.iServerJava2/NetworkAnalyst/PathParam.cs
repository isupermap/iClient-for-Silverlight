﻿
using System.Collections.Generic;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_PathParam_Title}</para>
    /// 	<para>${iServer2_PathParam_Description}</para>
    /// </summary>
    public class PathParam
    {
        /// <summary>${iServer2_PathParam_constructor_None_D}</summary>
        public PathParam()
        {
            HasLeastEdgeCount = false;
        }
        /// <summary>${iServer2_PathParam_attribute_networkAnalystParam_D}</summary>
        public NetworkAnalystParam NetworkAnalystParam { get; set; }
        /// <summary>${iServer2_PathParam_attribute_hasLeastEdgeCount_D}</summary>
        public bool HasLeastEdgeCount { get; set; }

        internal static string ToJson(PathParam pathParam)
        {
            if (pathParam == null)
            {
                return null;
            }
            string json = "{";
            List<string> list = new List<string>();

            list.Add(string.Format("\"networkAnalystParam\":{0}", NetworkAnalystParam.ToJson(pathParam.NetworkAnalystParam)));
            list.Add(string.Format("\"hasLeastEdgeCount\":{0}", pathParam.HasLeastEdgeCount));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
    }
}
