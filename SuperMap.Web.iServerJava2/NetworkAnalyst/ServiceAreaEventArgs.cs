﻿

using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ServiceAreaEventArgs_Title}</para>
    /// 	<para>${iServer2_ServiceAreaEventArgs_Description}</para>
    /// </summary>
    public class ServiceAreaEventArgs:ServiceEventArgs
    {
        /// <summary>${iServer2_ServiceAreaEventArgs_constructor_D}</summary>
        public ServiceAreaEventArgs(ServiceAreaResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }
        /// <summary>${iServer2_ServiceAreaEventArgs_attribute_result_D}</summary>
        public ServiceAreaResult Result { get; private set; }
        /// <summary>${iServer2_ServiceAreaEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
