﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_FindTSPathParameters_Title}</para>
    /// 	<para>${iServer2_FindTSPathParameters_Description}</para>
    /// </summary>
    public class FindTSPPathParameters:ParametersBase
    {
        /// <summary>${iServer2_FindTSPathParameters_constructor_None_D}</summary>
        public FindTSPPathParameters()
        { }
        /// <summary>${iServer2_FindTSPathParameters_attribute_networkSetting_D}</summary>
        public NetworkModelSetting NetworkSetting { get; set; }
        /// <summary>${iServer2_FindTSPathParameters_attribute_TSPPathParam_D}</summary>
        public TSPPathParam TSPPathParam { get; set; }
    }
}
