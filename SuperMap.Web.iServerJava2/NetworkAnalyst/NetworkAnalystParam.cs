﻿//网络分析参数类，该类用于设置执行网络分析时需要的相关参数，包括设置
//分析时所用的障碍边、障碍点列表，分析结果是否要包含路由对象集合等。

using System.Collections.Generic;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_NetworkAnalystParam_Title}</para>
    /// 	<para>${iServer2_NetworkAnalystParam_Description}</para>
    /// </summary>
    public class NetworkAnalystParam
    {
        /// <summary>${iServer2_NetworkAnalystParam_constructor_None_D}</summary>
        public NetworkAnalystParam()
        {
        }

        /// <summary>${iServer2_NetworkAnalystParam_attribute_weightName_D}</summary>
        public string WeightName { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_nodeIDs_D}</summary>
        public IList<int> NodeIDs { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_point2Ds_D}</summary>
        public Point2DCollection Point2Ds { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_barrierNodes_D}</summary>
        public IList<int> BarrierNodes { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_barrierEdges_D}</summary>
        public IList<int> BarrierEdges { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_turnWeightField_D}</summary>
        public string TurnWeightField { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_isNodeReturn_D}</summary>
        public bool IsNodesReturn { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_isEdgesReturn_D}</summary>
        public bool IsEdgesReturn { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_isPathGuidesReturn_D}</summary>
        public bool IsPathGuidesReturn { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_isPathsReturn_D}</summary>
        public bool IsPathsReturn { get; set; }
        /// <summary>${iServer2_NetworkAnalystParam_attribute_isStopsReturn_D}</summary>
        public bool IsStopsReturn { get; set; }

        internal static string ToJson(NetworkAnalystParam networkAnalystParam)
        {
            if (networkAnalystParam == null)
            {
                return null;
            }
            string json = "{";

            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(networkAnalystParam.WeightName))
            {
                list.Add(string.Format("\"weightName\":\"{0}\"", networkAnalystParam.WeightName));
            }
            else
            {
                list.Add("\"weightName\":\"\"");
            }

            if (networkAnalystParam.NodeIDs != null && networkAnalystParam.NodeIDs.Count > 0)
            {
                List<string> temp = new List<string>();
                foreach (int id in networkAnalystParam.NodeIDs)
                {
                    temp.Add(id.ToString());
                }
                list.Add(string.Format("\"nodeIDs\":\"{0}\"", string.Join(",", temp.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"nodeIDs\":null"));
            }

            if (networkAnalystParam.Point2Ds != null && networkAnalystParam.Point2Ds.Count > 0)
            {
                List<string> tempPS = new List<string>();
                foreach (Point2D p in networkAnalystParam.Point2Ds)
                {
                    tempPS.Add(JsonHelper.FromPoint2D(p));
                }
                list.Add(string.Format("\"point2Ds\":[{0}]", string.Join(",", tempPS.ToArray())));
            }

            if (networkAnalystParam.BarrierNodes != null && networkAnalystParam.BarrierNodes.Count > 0)
            {
                List<string> tempBD = new List<string>();
                foreach (int bd in networkAnalystParam.BarrierNodes)
                {
                    tempBD.Add(bd.ToString());
                }
                list.Add(string.Format("\"barrierNodes\":[{0}]", string.Join(",", tempBD.ToArray())));
            }
            else 
            {
                list.Add(string.Format("\"barrierNodes\":null"));
            }

            if (networkAnalystParam.BarrierEdges != null && networkAnalystParam.BarrierEdges.Count > 0)
            {
                List<string> tempBE = new List<string>();
                foreach (int be in networkAnalystParam.BarrierEdges)
                {
                    tempBE.Add(be.ToString());
                }
                list.Add(string.Format("\"barrierEdges\":[{0}]", string.Join(",", tempBE.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"barrierEdges\":null"));
            }

            if (!string.IsNullOrEmpty(networkAnalystParam.TurnWeightField))
            {
                list.Add(string.Format("\"turnWeightField\":\"{0}\"", networkAnalystParam.TurnWeightField));
            }
            else
            {
                list.Add(string.Format("\"turnWeightField\":\"\""));
            }

            list.Add(string.Format("\"isNodesReturn\":{0}", networkAnalystParam.IsNodesReturn));

            list.Add(string.Format("\"isEdgesReturn\":{0}", networkAnalystParam.IsEdgesReturn));

            list.Add(string.Format("\"isPathGuidesReturn\":{0}", networkAnalystParam.IsPathGuidesReturn));

            list.Add(string.Format("\"isPathsReturn\":{0}", networkAnalystParam.IsPathsReturn));

            list.Add(string.Format("\"isStopsReturn\":{0}", networkAnalystParam.IsStopsReturn));


            json += string.Join(",", list.ToArray());
            json += "}";
            return json;

        }

    }
}
