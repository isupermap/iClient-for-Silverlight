﻿

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ServiceAreaParameters_Title}</para>
    /// 	<para>${iServer2_ServiceAreaParameters_Description}</para>
    /// </summary>
    public class ServiceAreaParameters:ParametersBase
    {
        /// <summary>${iServer2_ServiceAreaParameters_constructor_None_D}</summary>
        public ServiceAreaParameters()
        { }
        /// <summary>${iServer2_ServiceAreaParameters_attribute_networkSetting_D}</summary>
        public NetworkModelSetting NetworkSetting { get; set; }

        /// <summary>${iServer2_ServiceAreaParameters_attribute_ServiceAreaParam_D}</summary>
        public ServiceAreaParam ServiceAreaParam { get; set; }
    }
}
