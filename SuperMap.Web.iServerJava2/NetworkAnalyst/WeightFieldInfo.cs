﻿using System.Collections.Generic;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_WeightFieldInfo_Title}</para>
    /// 	<para>${iServer2_WeightFieldInfo_Description}</para>
    /// </summary>
    public class WeightFieldInfo
    {
        /// <summary>${iServer2_TurnTableSetting_constructor_None_D}</summary>
        public WeightFieldInfo()
        { }
        /// <summary>${iServer2_WeightFieldInfo_attribute_name_D}</summary>
        public string Name { get; set; }
        /// <summary>${iServer2_WeightFieldInfo_attribute_ftWeightField_D}</summary>
        public string FTWeightField { get; set; }
        /// <summary>${iServer2_WeightFieldInfo_attribute_tfWeightField_D}</summary>
        public string TFWeightField { get; set; }

        internal static string ToJson(WeightFieldInfo weightFieldInfo)
        {
            if (weightFieldInfo == null)
            {
                return null;
            }
            string json = "{";

            List<string> list = new List<string>();
            list.Add(string.Format("\"name\":\"{0}\"", weightFieldInfo.Name));
            list.Add(string.Format("\"fTWeightField\":\"{0}\"", weightFieldInfo.FTWeightField));
            list.Add(string.Format("\"tFWeightField\":\"{0}\"", weightFieldInfo.TFWeightField));

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
    }
}
