﻿using System.Collections.Generic;
using System.Json;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_PathGuide_Title}</para>
    /// 	<para>${iServer2_PathGuide_Description}</para>
    /// </summary>
    public class PathGuide
    {
        internal PathGuide()
        { }

        /// <summary>${iServer2_PathGuide_attribute_items_D}</summary>
        public List<PathGuideItem> Items { get; private set; }

        /// <summary>${iServer2_PathGuide_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_PathGuide_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_PathGuide_method_FromJson_return}</returns>
        public static PathGuide FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            PathGuide pg = new PathGuide();
            JsonArray itemINJson = (JsonArray)jsonObject["items"];
            if (itemINJson != null && itemINJson.Count > 0)
            {
                pg.Items = new List<PathGuideItem>();
                for (int i = 0; i < itemINJson.Count; i++)
                {
                    PathGuideItem pi = PathGuideItem.FromJson((JsonObject)itemINJson[i]);
                    pg.Items.Add(pi);
                }
            }
            return pg;
        }
    }
}
