﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_FindPathParameters_Title}</para>
    /// 	<para>${iServer2_FindPathParameters_Description}</para>
    /// </summary>
    /// <remarks>${iServer2_FindPathParameters_Remarks}</remarks>
    public class FindPathParameters : ParametersBase
    {
        /// <summary>${iServer2_FindPathParameters_constructor_None_D}</summary>
        public FindPathParameters()
        { }
        /// <summary>${iServer2_FindPathParameters_attribute_networkSetting_D}</summary>
        public NetworkModelSetting NetworkSetting { get; set; }
        /// <summary>${iServer2_FindPathParameters_attribute_pathParam_D}</summary>
        public PathParam PathParam { get; set; }
    }
}
