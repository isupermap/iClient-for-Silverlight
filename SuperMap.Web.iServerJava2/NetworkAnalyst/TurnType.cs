﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_TurnType_Title}</para>
    /// 	<para>${iServer2_TurnType_Description}</para>
    /// </summary>
    public enum TurnType
    {
        /// <summary>${iServer2_TurnType_attribute_none_D}</summary>
        None=255,
        /// <summary>${iServer2_TurnType_attribute_end_D}</summary>
        End=0,
        /// <summary>${iServer2_TurnType_attribute_left_D}</summary>
        Left=1,
        /// <summary>${iServer2_TurnType_attribute_right_D}</summary>
        Right=2,
        /// <summary>${iServer2_TurnType_attribute_ahead_D}</summary>
        Ahead=3,
        /// <summary>${iServer2_TurnType_attribute_back_D}</summary>
        Back=4
    }
}
