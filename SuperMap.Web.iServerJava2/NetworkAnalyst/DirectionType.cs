﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_DirectionType_Title}</para>
    /// 	<para>${iServer2_DirectionType_Description}</para>
    /// </summary>
    public enum DirectionType
    {
        /// <summary>${iServer2_DirectionType_attribute_east_D}</summary>
        East=0,
        /// <summary>${iServer2_DirectionType_attribute_south_D}</summary>
        South=1,
        /// <summary>${iServer2_DirectionType_attribute_west_D}</summary>
        West=2,
        /// <summary>${iServer2_DirectionType_attribute_north_D}</summary>
        North=3,
        /// <summary>${iServer2_DirectionType_attribute_none_D}</summary>
        None=255
    }
}
