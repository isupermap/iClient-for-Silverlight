﻿using SuperMap.Web.Core;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_ClosestFacilityParameters_Title}</para>
    /// 	<para>${iServer2_ClosestFacilityParameters_Description}</para>
    /// </summary>
    /// <remarks>${iServer2_ClosestFacilityParameters_Remarks}</remarks>
    public class ClosestFacilityParameters : ParametersBase
    {
        /// <summary>${iServer2_ClosestFacilityParameters_constructor_None_D}</summary>
        public ClosestFacilityParameters()
        { }
        /// <summary>${iServer2_FindPathParameters_attribute_networkSetting_D}</summary>
        public NetworkModelSetting NetworkSetting { get; set; }
        /// <summary>${iServer2_ClosestFacilityParameters_attribute_eventPoint_D}</summary>
        public Point2D EventPoint { get; set; }
        /// <summary>${iServer2_ClosestFacilityParameters_attribute_proximityParam_D}</summary>
        public ProximityParam ProximityParam { get; set; }
    }
}
