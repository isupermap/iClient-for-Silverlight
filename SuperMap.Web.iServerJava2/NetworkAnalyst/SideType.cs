﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_SideType_Title}</para>
    /// 	<para>${iServer2_SideType_Description}</para>
    /// </summary>
    public enum SideType
    {
        /// <summary>${iServer2_SideType_attribute_none_D}</summary>
        None=-1,
        /// <summary>${iServer2_SideType_attribute_middle_D}</summary>
        Middle=0,
        /// <summary>${iServer2_SideType_attribute_left_D}</summary>
        Left=1,
        /// <summary>${iServer2_SideType_attribute_right_D}</summary>
        Right=2
    }
}
