﻿using System.Collections.Generic;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_TurnTableSetting_Title}</para>
    /// 	<para>${iServer2_TurnTableSetting_Description_1}</para>
    /// 	<para><img src="turn.png"/></para>
    /// 	<para>${iServer2_TurnTableSetting_Description_2}</para>
    /// 	<para><img src="turnTable.jpg"/></para>
    /// </summary>
    public class TurnTableSetting
    {
        /// <summary>${iServer2_TurnTableSetting_constructor_None_D}</summary>
        public TurnTableSetting()
        { }
        /// <summary>${iServer2_TurnTableSetting_attribute_turnDatasetName_D}</summary>
        public string TurnDatasetName { get; set; }
        /// <summary>${iServer2_TurnTableSetting_attribute_turnDataSourceName_D}</summary>
        public string TurnDataSourceName { get; set; }
        /// <summary>${iServer2_TurnTableSetting_attribute_turnFromEdgeIDField_D}</summary>
        public string TurnFromEdgeIDField { get; set; }
        /// <summary>${iServer2_TurnTableSetting_attribute_turnNodeIDField_D}</summary>
        public string TurnNodeIDField { get; set; }
        /// <summary>${iServer2_TurnTableSetting_attribute_turnToEdgeIDField_D}</summary>
        public string TurnToEdgeIDField { get; set; }
        /// <summary>${iServer2_TurnTableSetting_attribute_turnWeightFields_D}</summary>
        public IList<string> TurnWeightFields { get; set; }


        internal static string ToJson(TurnTableSetting turnTableSetting)
        {
            if (turnTableSetting == null)
            {
                return null;
            }
            string json = "{";

            List<string> list = new List<string>();

            if (turnTableSetting.TurnDatasetName != null)
            {
                list.Add(string.Format("\"turnDatasetName\":\"{0}\"", turnTableSetting.TurnDatasetName));
            }

            if (turnTableSetting.TurnDataSourceName != null)
            {
                list.Add(string.Format("\"turnDataSourceName\":\"{0}\"", turnTableSetting.TurnDataSourceName));
            }

            if (turnTableSetting.TurnFromEdgeIDField != null)
            {
                list.Add(string.Format("\"turnFromEdgeIDField\":\"{0}\"", turnTableSetting.TurnFromEdgeIDField));
            }

            if (turnTableSetting.TurnNodeIDField != null)
            {
                list.Add(string.Format("\"turnNodeIDField\":\"{0}\"", turnTableSetting.TurnNodeIDField));
            }

            if (turnTableSetting.TurnToEdgeIDField != null)
            {
                list.Add(string.Format("\"turnToEdgeIDField\":\"{0}\"", turnTableSetting.TurnToEdgeIDField));
            }

            if (turnTableSetting.TurnWeightFields != null && turnTableSetting.TurnWeightFields.Count > 0)
            {
                List<string> ls = new List<string>();
                foreach (string s in turnTableSetting.TurnWeightFields)
                {
                    ls.Add(s);
                }
                list.Add(string.Format("\"turnWeightFields\":\"{0}\"", string.Join(",", ls.ToArray())));
            }

            json += string.Join(",", list.ToArray());
            json += "}";

            return json;
        }
    }
}
