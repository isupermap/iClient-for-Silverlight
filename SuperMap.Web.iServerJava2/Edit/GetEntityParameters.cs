﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_GetEntityParameters_Title}</para>
    /// 	<para>${iServer2_GetEntityParameters_Description}</para>
    /// </summary>
    public class GetEntityParameters : ParametersBase
    {
        /// <summary>${iServer2_GetEntityParameters_constructor_None_D}</summary>
        public GetEntityParameters()
        { }
        /// <summary>${iServer2_GetEntityParameters_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${iServer2_GetEntityParameters_attribute_ID_D}</summary>
        public int ID { get; set; }
    }
}
