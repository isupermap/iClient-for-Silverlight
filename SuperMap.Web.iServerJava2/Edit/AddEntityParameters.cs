﻿namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_AddEntityParameters_Title}</para>
    /// 	<para>${iServer2_AddEntityParameters_Description}</para>
    /// </summary>
    public class AddEntityParameters : ParametersBase
    {
        /// <summary>${iServer2_AddEntityParameters_constructor_None_D}</summary>
        public AddEntityParameters()
        { }
        /// <summary>${iServer2_AddEntityParameters_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${iServer2_AddEntityParameters_attribute_entity_D}</summary>
        public Entity Entity { get; set; }
    }
}
