﻿
using System;
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_GetEntityEventArgs_Title}</para>
    /// 	<para>${iServer2_GetEntityEventArgs_Description}</para>
    /// </summary>
    public class GetEntityEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer2_GetEntityEventArgs_constructor_D}</summary>
        public GetEntityEventArgs(Entity result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }

        /// <summary>${iServer2_GetEntityEventArgs_attribute_result_D}</summary>
        public Entity Result { get; private set; }
        /// <summary>${iServer2_GetEntityEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}

