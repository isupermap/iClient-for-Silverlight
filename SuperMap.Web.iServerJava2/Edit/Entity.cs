﻿using System.Collections.Generic;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_Entity_Title}</para>
    /// 	<para>${iServer2_Entity_Description}</para>
    /// </summary>
    public class Entity
    {
        /// <summary>${iServer2_Entity_constructor_None_D}</summary>
        public Entity()
        {
        }
        /// <summary>${iServer2_Entity_attribute_fieldNames_D}</summary>
        public IList<string> FieldNames { get; set; }
        /// <summary>${iServer2_Entity_attribute_fieldValues_D}</summary>
        public IList<string> FieldValues { get; set; }
        /// <summary>${iServer2_Entity_attribute_ID_D}</summary>
        public int ID { get; set; }
        /// <summary>${iServer2_Entity_attribute_shape_D}</summary>
        public ServerGeometry Shape { get; set; }

        internal static string ToJson(Entity entity)
        {
            if (entity == null)
            {
                return null;
            }
            string json = "[{";

            List<string> list = new List<string>();

            if (entity.FieldNames != null && entity.FieldNames.Count > 0)
            {
                List<string> temp = new List<string>();
                foreach (string fn in entity.FieldNames)
                {
                    temp.Add(string.Format("\"{0}\"", fn));
                }
                list.Add(string.Format("\"fieldNames\":[{0}]", string.Join(",", temp.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"fieldNames\":null"));
            }

            if (entity.FieldValues != null && entity.FieldValues.Count > 0)
            {
                List<string> temp = new List<string>();
                foreach (string fv in entity.FieldValues)
                {
                    temp.Add(string.Format("\"{0}\"", fv));
                }
                list.Add(string.Format("\"fieldValues\":[{0}]", string.Join(",", temp.ToArray())));
            }
            else
            {
                list.Add(string.Format("\"fieldValues\":null"));
            }

            list.Add(string.Format("\"shape\":{0}", ServerGeometry.ToJson(entity.Shape)));
            list.Add(string.Format("\"id\":{0}", entity.ID));

            json += string.Join(",", list.ToArray());
            json += "}]";
            return json;
        }

        /// <summary>${iServer2_Entity_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_Entity_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_Entity_method_FromJson_return}</returns>
        public static Entity FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            Entity entity = new Entity();

            entity.ID = jsonObject["id"];

            entity.Shape = ServerGeometry.FromJson((JsonObject)jsonObject["shape"]);

            //fieldValue
            JsonArray jsonValue = (JsonArray)jsonObject["fieldValues"];
            if (jsonValue != null && jsonValue.Count > 0)
            {
                entity.FieldValues = new List<string>();
                for (int i = 0; i < jsonValue.Count; i++)
                {
                    entity.FieldValues.Add(jsonValue[i]);
                }
            }

            //fieldNames
            JsonArray jsonNames = (JsonArray)jsonObject["fieldNames"];
            if (jsonNames != null && jsonNames.Count > 0)
            {
                entity.FieldNames = new List<string>();
                for (int i = 0; i < jsonNames.Count; i++)
                {
                    entity.FieldNames.Add(jsonNames[i]);
                }
            }

            return entity;
        }
    }
}
