﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Json;
using System.Windows.Browser;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_DeleteEntityService_Title}</para>
    /// 	<para>${iServer2_DeleteEntityService_Description}</para>
    /// </summary>
    public class DeleteEntityService : ServiceBase
    {
        /// <summary>${iServer2_DeleteEntityService_constructor_None_D}</summary>
        /// <overloads>${iServer2_DeleteEntityService_constructor_overloads_D}</overloads>
        public DeleteEntityService()
        { }
        /// <summary>${iServer2_DeleteEntityService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_DeleteEntityServicee_constructor_String_param_url}</param>
        public DeleteEntityService(string url)
            : base(url)
        { }
        /// <overloads>${iServer2_DeleteEntityService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_DeleteEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_DeleteEntityService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(DeleteEntityParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_DeleteEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_DeleteEntityService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_DeleteEntityService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(DeleteEntityParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(DeleteEntityParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "DeleteEntity";
            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);
            dict.Add("layerName", parameters.LayerName);

            string json = "[";
            List<string> list = new List<string>();
            if (parameters.IDs != null && parameters.IDs.Count > 0)
            {
                for (int i = 0; i < parameters.IDs.Count; i++)
                {
                    list.Add(string.Format("\"{0}\"", parameters.IDs[i]));
                }
                json += string.Join(",", list.ToArray());
            }
            json += "]";
            dict.Add("ids", json);

            dict.Add("lockID", DateTime.Now.Ticks.ToString());
            dictionary.Add("params", Bridge.CreateParams(method, dict));

            return dictionary;

        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            EditResult result = EditResult.FromJson(jsonObject);
            LastResult = result;
            EditEventArgs args = new EditEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);

        }
        private void OnProcessCompleted(EditEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        private EditResult lastResult;

        /// <summary>${iServer2_DeleteEntityService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<EditEventArgs> ProcessCompleted;

        /// <summary>${iServer2_DeleteEntityService_attribute_lastResult_D}</summary>
        public EditResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
