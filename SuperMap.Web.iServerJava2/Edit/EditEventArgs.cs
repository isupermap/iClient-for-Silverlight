﻿using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_EditEventArgs_Title}</para>
    /// 	<para>${iServer2_EditEventArgs_Description}</para>
    /// </summary>
    public class EditEventArgs : ServiceEventArgs
    {
        /// <summary>${iServer2_EditEventArgs_constructor_D}</summary>
        public EditEventArgs(EditResult result, string originResult, object token)
            : base(token)
        {
            Result = result;
            OriginResult = originResult;
        }

        /// <summary>${iServer2_EditEventArgs_attribute_result_D}</summary>
        public EditResult Result { get; private set; }
        /// <summary>${iServer2_EditEventArgs_attribute_originResult_D}</summary>
        public string OriginResult { get; private set; }
    }
}
