﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Windows;
using System.Windows.Browser;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Service;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_GetEntityService_Title}</para>
    /// 	<para>${iServer2_GetEntityService_Description}</para>
    /// </summary>
    public class GetEntityService : ServiceBase
    {
        /// <summary>${iServer2_GetEntityService_constructor_None_D}</summary>
        /// <overloads>${iServer2_GetEntityService_constructor_overloads_D}</overloads>
        public GetEntityService()
        { }
        /// <summary>${iServer2_GetEntityService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_GetEntityService_constructor_String_param_url}</param>
        public GetEntityService(string url)
            : base(url)
        { }
        /// <overloads>${iServer2_GetEntityService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_GetEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_GetEntityService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(GetEntityParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_GetEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_GetEntityService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_GetEntityService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(GetEntityParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state,false);
        }

        private Dictionary<string, string> GetParameters(GetEntityParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "GetEntity";
            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);
            dict.Add("layerName", parameters.LayerName);
            dict.Add("id", parameters.ID.ToString());
            dictionary.Add("params", Bridge.CreateParams(method, dict));

            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            Entity result = Entity.FromJson(jsonObject);
            LastResult = result;
            GetEntityEventArgs args = new GetEntityEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);

        }
        private void OnProcessCompleted(GetEntityEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        private Entity lastResult;

        /// <summary>${iServer2_GetEntityService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<GetEntityEventArgs> ProcessCompleted;

        /// <summary>${iServer2_GetEntityService_attribute_lastResult_D}</summary>
        public Entity LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}

