﻿using System.Collections.Generic;

namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_DeleteEntityParameters_Title}</para>
    /// 	<para>${iServer2_DeleteEntityParameters_Description}</para>
    /// </summary>
    public class DeleteEntityParameters : ParametersBase
    {
        /// <summary>${iServer2_DeleteEntityParameters_constructor_None_D}</summary>
        public DeleteEntityParameters()
        { }
        /// <summary>${iServer2_DeleteEntityParameters_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${iServer2_DeleteEntityParameters_attribute_IDs_D}</summary>
        public IList<int> IDs { get; set; }
    }
}
