﻿
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_UpdateEntityParameters_Title}</para>
    /// 	<para>${iServer2_UpdateEntityParameters_Description}</para>
    /// </summary>
    public class UpdateEntityParameters : ParametersBase
    {
        /// <summary>${iServer2_UpdateEntityParameters_constructor_None_D}</summary>
        public UpdateEntityParameters()
        { }
        /// <summary>${iServer2_UpdateEntityParameters_attribute_layerName_D}</summary>
        public string LayerName { get; set; }
        /// <summary>${iServer2_UpdateEntityParameters_attribute_entity_D}</summary>
        public Entity Entity { get; set; }
    }
}
