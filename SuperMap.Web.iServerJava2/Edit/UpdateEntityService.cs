﻿using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Json;
using System.Windows;
using SuperMap.Web.iServerJava2.Resources;
using SuperMap.Web.Service;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_UpdateEntityService_Title}</para>
    /// 	<para>${iServer2_UpdateEntityService_Description}</para>
    /// </summary>
    public class UpdateEntityService : ServiceBase
    {

        /// <overloads>${iServer2_UpdateEntityService_constructor_overloads_D}</overloads>
        /// <summary>${iServer2_UpdateEntityService_constructor_None_D}</summary>
        public UpdateEntityService()
        { }
        /// <summary>${iServer2_UpdateEntityService_constructor_String_D}</summary>
        /// <param name="url">${iServer2_UpdateEntityService_constructor_String_param_url}</param>
        public UpdateEntityService(string url)
            : base(url)
        { }
        /// <overloads>${iServer2_UpdateEntityService_method_ProcessAsync_overloads_D}</overloads>
        /// <summary>${iServer2_UpdateEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_UpdateEntityService_method_ProcessAsync_param_parameters}</param>
        public void ProcessAsync(UpdateEntityParameters parameters)
        {
            ProcessAsync(parameters, null);
        }
        /// <summary>${iServer2_UpdateEntityService_method_ProcessAsync_D}</summary>
        /// <param name="parameters">${iServer2_UpdateEntityService_method_ProcessAsync_param_parameters}</param>
        /// <param name="state">${iServer2_UpdateEntityService_method_ProcessAsync_param_state}</param>
        public void ProcessAsync(UpdateEntityParameters parameters, object state)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(ExceptionStrings.ArgumentIsNull);
            }
            if (string.IsNullOrEmpty(base.Url))
            {
                throw new InvalidOperationException(ExceptionStrings.InvalidUrl);
            }

            if (!base.Url.EndsWith("/"))
            {
                base.Url += '/';
            }

            base.SubmitRequest(base.Url + "commonhandler?", GetParameters(parameters),
                new EventHandler<RequestEventArgs>(request_Completed), state, false);
        }

        private Dictionary<string, string> GetParameters(UpdateEntityParameters parameters)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string method = "UpdateEntity";
            dictionary.Add("method", method);
            dictionary.Add("mapName", parameters.MapName);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("mapName", parameters.MapName);
            dict.Add("layerName", parameters.LayerName);
            dict.Add("entities", Entity.ToJson(parameters.Entity));

            dict.Add("lockID", DateTime.Now.Ticks.ToString());
            dictionary.Add("params", Bridge.CreateParams(method, dict));

            return dictionary;
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            JsonObject jsonObject = (JsonObject)JsonObject.Parse(e.Result);
            EditResult result = EditResult.FromJson(jsonObject);
            LastResult = result;
            EditEventArgs args = new EditEventArgs(result, e.Result, e.UserState);
            OnProcessCompleted(args);

        }
        private void OnProcessCompleted(EditEventArgs args)
        {
            if (ProcessCompleted != null)
            {
                Application.Current.RootVisual.Dispatcher.BeginInvoke(ProcessCompleted, new object[] { this, args });
            }
        }

        private EditResult lastResult;

        /// <summary>${iServer2_UpdateEntityService_event_processCompleted_D}</summary>
        [ScriptableMember]
        public event EventHandler<EditEventArgs> ProcessCompleted;

        /// <summary>${iServer2_UpdateEntityService_attribute_lastResult_D}</summary>
        public EditResult LastResult
        {
            get
            {
                return lastResult;
            }
            private set
            {
                lastResult = value;
                base.OnPropertyChanged("LastResult");
            }
        }
    }
}
