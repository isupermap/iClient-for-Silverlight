﻿using System.Collections.Generic;
using SuperMap.Web.Core;
using System.Json;
namespace SuperMap.Web.iServerJava2
{
    /// <summary>
    /// 	<para>${iServer2_EditResult_Title}</para>
    /// 	<para>${iServer2_EditResult_Description}</para>
    /// </summary>
    public class EditResult
    {
        internal EditResult()
        { }
        /// <summary>${iServer2_EditResult_attribute_succeed_D}</summary>
        public bool Succeed { get; private set; }
        /// <summary>${iServer2_EditResult_attribute_IDs_D}</summary>
        public List<int> IDs { get; private set; }
        /// <summary>${iServer2_EditResult_attribute_editBounds_D}</summary>
        public Rectangle2D EditBounds { get; private set; }
        /// <summary>${iServer2_EditResult_attribute_message_D}</summary>
        public string Message { get; private set; }

        /// <summary>${iServer2_EditResult_method_FromJson_D}</summary>
        /// <param name="jsonObject">${iServer2_EditResult_method_FromJson_param_jsonObject}</param>
        /// <returns>${iServer2_EditResult_method_FromJson_return}</returns>
        public static EditResult FromJson(JsonObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            EditResult result = new EditResult();

            result.Succeed = (bool)jsonObject["succeed"];

            JsonArray idsInJson = (JsonArray)jsonObject["ids"];
            if (idsInJson.Count > 0 && idsInJson != null)
            {
                result.IDs = new List<int>();
                for (int i = 0; i < idsInJson.Count; i++)
                {
                    result.IDs.Add(idsInJson[i]);
                }
            }

            if (jsonObject["editBounds"] != null)
            {
                double mbMinX = (double)jsonObject["editBounds"]["leftBottom"]["x"];
                double mbMinY = (double)jsonObject["editBounds"]["leftBottom"]["y"];
                double mbMaxX = (double)jsonObject["editBounds"]["rightTop"]["x"];
                double mbMaxY = (double)jsonObject["editBounds"]["rightTop"]["y"];
                result.EditBounds = new Rectangle2D(mbMinX, mbMinY, mbMaxX, mbMaxY);
            }

            result.Message = (string)jsonObject["message"];
            return result;
        }
    }
}
