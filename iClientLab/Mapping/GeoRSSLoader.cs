﻿//功能有限，引入了System.ServiceModel.Syndication程序集，可以考虑使用LINQ
//还有坐标的转换等，不能通用...

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.ServiceModel.Syndication;
using System.Xml;
using iClientLab;
using SuperMap.Web.Core;

namespace iClientLab
{
    public class GeoRSSLoader
    {
        public class RssLoadedEventArgs : EventArgs
        {
            public object UserState { get; set; }
            public IEnumerable<Feature> Features { get; set; }
        }
        public class RssLoadFailedEventArgs : EventArgs
        {
            public object UserState { get; set; }
            public Exception ex { get; set; }
        }
        public event EventHandler<RssLoadedEventArgs> LoadCompleted;
        public event EventHandler<RssLoadFailedEventArgs> LoadFailed;

        public void LoadRss(Uri feedUri, object userToken)
        {
            WebClient wc = new WebClient();
            wc.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);
            wc.OpenReadAsync(feedUri, userToken);
        }

        private void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                if (LoadFailed != null)
                    LoadFailed(this, new RssLoadFailedEventArgs()
                    {
                        ex = new Exception("Error in Reading the RSS feed. Try Again later!", e.Error),
                        UserState = e.UserState
                    });
                return;
            }
            FeatureCollection features = new FeatureCollection();
            using (Stream s = e.Result)
            {
                SyndicationFeed feed;
                List<SyndicationItem> feedItems = new List<SyndicationItem>();

                using (XmlReader reader = XmlReader.Create(s))
                {
                    feed = SyndicationFeed.Load(reader);
                    foreach (SyndicationItem feedItem in feed.Items)
                    {
                        SyndicationElementExtensionCollection ec = feedItem.ElementExtensions;

                        string x = "";
                        string y = "";

                        foreach (SyndicationElementExtension ee in ec)
                        {
                            XmlReader xr = ee.GetReader();
                            switch (ee.OuterName)
                            {
                                case ("lat"):
                                    {
                                        y = xr.ReadElementContentAsString();
                                        break;
                                    }
                                case ("long"):
                                    {
                                        x = xr.ReadElementContentAsString();
                                        break;
                                    }
                                case ("point"):
                                    {
                                        string sp = xr.ReadElementContentAsString();
                                        string[] sxsy = sp.Split(new char[] { ' ' });
                                        x = sxsy[1];
                                        y = sxsy[0];
                                        break;
                                    }
                            }
                        }

                        if (!string.IsNullOrEmpty(x))
                        {
                            //TODO:这儿还有一个转换，不能通用...
                            Point2D point2D = MercatorUtility.LatLonToMeters(new Point2D(Convert.ToDouble(x), Convert.ToDouble(y)));
                            Feature feature = new Feature()
                            {
                                Geometry = new GeoPoint(point2D.X, point2D.Y)

                            };

                            //属性这里也比较不好统一
                            feature.Attributes.Add("Title", feedItem.Title.Text);
                            feature.Attributes.Add("Summary", feedItem.Summary.Text);
                            feature.Attributes.Add("PublishDate", feedItem.PublishDate);
                            feature.Attributes.Add("Id", feedItem.Id);
                            features.Add(feature);
                        }
                    }
                }
            }

            if (LoadCompleted != null)
            {
                LoadCompleted(this, new RssLoadedEventArgs(){Features = features,UserState = e.UserState});
            }

        }
    }
}
