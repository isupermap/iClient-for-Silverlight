using System;
using System.Text;
using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iClientLab
{
    public enum BMapsMode
    {
        Road,
        Aerial,
        AerialWithLabels
    }

    public class TiledBingMapsLayer : TiledCachedLayer
    {
        //http://ecn.t0.tiles.virtualearth.net/tiles/h13220.jpeg?g=266
        private const string RoadTileUri = "http://r{0}.ortho.tiles.virtualearth.net/tiles/r{1}.png?g=43&amp;shading=hill";
        private const string AerialTileUri = "http://a{0}.ortho.tiles.virtualearth.net/tiles/a{1}.jpg?g=43";
        private const string AerialWithLabelsTileUri = "http://h{0}.ortho.tiles.virtualearth.net/tiles/h{1}.jpg?g=43";
        //private const string RoadTileUri = "http://ecn.t{0}.tiles.virtualearth.net/tiles/h{1}.jpeg?g=266";

        private string[] subDomains = { "0", "1", "2", "3" };
        private const double CornerCoordinate = 20037508.3427892;

        private StringBuilder keyBuilder = new StringBuilder();


        public TiledBingMapsLayer()
        { }


        public override string GetTileUrl(int indexX, int indexY, int level)
        {
            //QuadKey key = new QuadKey(row, col, level);
            //char ch = key.Key[key.ZoomLevel - 1];
            this.keyBuilder.Length = 0;
            for (int i = level; i >= 0; i--)
            {
                int num2 = ((int)1) << i;
                int num = ((indexX & num2) >> i) | (((indexY & num2) != 0) ? 2 : 0);
                this.keyBuilder.Append(num);
            }
            int num3 = ((indexY & 1) << 1) | (indexX & 1);
            return string.Format(this.Url, num3, keyBuilder);
        }

        public override void Initialize()
        {
            this.Bounds = new Rectangle2D(-CornerCoordinate, -CornerCoordinate, CornerCoordinate, CornerCoordinate);
            this.TileSize = 256;

            double res = CornerCoordinate * 2 / 512;
            double[] resolutions = new double[18];
            for (int i = 0; i < resolutions.Length; i++)
            {
                resolutions[i] = res;
                res *= 0.5;
            }
            this.Resolutions = resolutions;

            switch (this.Mode)
            {
                case BMapsMode.Road:
                    this.Url = RoadTileUri;
                    break;
                case BMapsMode.Aerial:
                    this.Url = AerialTileUri;
                    break;
                case BMapsMode.AerialWithLabels:
                    this.Url = AerialWithLabelsTileUri;
                    break;
            }

            base.Initialize();
        }


        public BMapsMode Mode
        {
            get { return (BMapsMode)GetValue(MapTypeProperty); }
            set { SetValue(MapTypeProperty, value); }
        }
        public static readonly DependencyProperty MapTypeProperty =
            DependencyProperty.Register("Mode", typeof(BMapsMode), typeof(TiledBingMapsLayer), new PropertyMetadata(new PropertyChangedCallback(new PropertyChangedCallback(OnMapTypePropertyChanged))));

        private static void OnMapTypePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TiledBingMapsLayer layer = d as TiledBingMapsLayer;
            if (layer.IsInitialized)
            {
                layer.ChangeTileSource();
            }
        }

        private void ChangeTileSource()
        {
            switch (this.Mode)
            {
                case BMapsMode.Road:
                    this.Url = RoadTileUri;
                    break;
                case BMapsMode.Aerial:
                    this.Url = AerialTileUri;
                    break;
                case BMapsMode.AerialWithLabels:
                    this.Url = AerialWithLabelsTileUri;
                    break;

            }
            if (!base.IsInitialized)
            {
                base.Initialize();
            }
            else
            {
                base.Refresh();
            }
        }

    }
}