﻿using System;
using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iClientLab
{
    public class GeoRSSLayer : FeaturesLayer
    {
        GeoRSSLoader loader;
        public GeoRSSLayer()
            : base()
        {
            loader = new GeoRSSLoader();
            loader.LoadCompleted += loader_LoadCompleted;
            loader.LoadFailed += loader_LoadFailed;
        }

        private void loader_LoadFailed(object sender, GeoRSSLoader.RssLoadFailedEventArgs e)
        {
            this.Error = e.ex;
            if (!IsInitialized)
                base.Initialize();
        }
        private void loader_LoadCompleted(object sender, GeoRSSLoader.RssLoadedEventArgs e)
        {
            this.Features.Clear();
            foreach (Feature f in e.Features)
            {
                f.Style = Style;
                this.Features.Add(f);
            }
            if (!IsInitialized)
                base.Initialize();
        }
        public override void Initialize()
        {
            Update();
        }


        public static readonly DependencyProperty StyleProperty = DependencyProperty.Register("Style", typeof(MarkerStyle), typeof(GeoRSSLayer), null);
        public MarkerStyle Style
        {
            get { return (MarkerStyle)GetValue(StyleProperty); }
            set { SetValue(StyleProperty, value); }
        }

        //用父类的URl吧，不过无法DP
        //public static readonly DependencyProperty SourceProperty = DependencyProperty.Register("Source", typeof(Uri), typeof(GeoRSSLayer), null);
        //public Uri Source
        //{
        //    get { return ((Uri)GetValue(SourceProperty)); }
        //    set { SetValue(SourceProperty, value); }
        //}

        public void Update()
        {
            if (!String.IsNullOrEmpty(Url))
            {
                loader.LoadRss(new Uri(Url), null);
            }
        }
    }
}
