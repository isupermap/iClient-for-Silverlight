﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;

namespace iClientLab
{
    public class BaiduMapLayer : TiledCachedLayer
    {
        private const string RoadTileUri = "http://q{0}.baidu.com/it/u=x={1};y={2};z={3};v=005;type=web&fm=44";
        private int[,] offsetXY = new int[14,2]{{3,3},{6,7},{12,15},{24,31},{48,63},
            {96,127},{192,255},{384,511},{768,1023},{1536,2047},{3072,4095},{6144,8191},{12288,16383},{24576,32767}};        
            //10级以后其实就没那么多了

        public BaiduMapLayer()
        {
            this.Url = RoadTileUri;
        }

        public override string GetTileUrl(int indexX, int indexY, int level)
        {
            int x = indexX + offsetXY[level, 0];
            int y = offsetXY[level,1] - indexY;
            int z = level + 5;
            //int num = Math.Abs(x + y) % 8;
            int num = (x + y) % 8;
            num++; //1-8
            string str =  string.Format(this.Url, num, x, y, z);
            return str;   
        }

        public override void Initialize()
        {
            var minX = 6291456;
            var minY = 0;
            var maxX = minX + Math.Pow(2, 14) * 256 * 5;
            var maxY = minY + Math.Pow(2, 14) * 256 * 4;
            this.Bounds = new Rectangle2D(minX, minY, maxX, maxY);
            
            this.TileSize = 256;

            double res = Math.Pow(2,14);
            double[] resolutions = new double[14];
            for (int i = 0; i < resolutions.Length; i++)
            {
                resolutions[i] = res;
                res *= 0.5;
            }
            this.Resolutions = resolutions;


            base.Initialize();
        }
    }
}
