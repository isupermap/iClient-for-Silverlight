﻿using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iClientLab
{
    public enum GMapsType
    {
        Map,
        Satellite
    }

    public class TiledGoogleMapsLayer : TiledCachedLayer
    {
        private string[] subDomains = { "0", "1", "2", "3" };
        //http://mt{0}.google.cn/vt/lyrs=m@135&hl=zh-CN&gl=cn&x={1}&y={2}&z={3}&s=Ga

        //private const string MapTileUri = "http://mt{0}.google.com/vt/lyrs=m@107&hl=en&x={1}&y={2}&z={3}&s=Ga";
        private const string MapTileUri = "http://mt{0}.google.cn/vt/lyrs=m@135&hl=zh-CN&gl=cn&x={1}&y={2}&z={3}&s=Ga";
        private const string SatelliteTileUri = "http://khm{0}.google.com/kh/v=45&x={1}&y={2}&z={3}&s=Gal";
        private const double CornerCoordinate = 20037508.3427892;
        //private const int WKID = 102113;//ESRI的Web MerCator投影编号102113，EPSG:900913

        public TiledGoogleMapsLayer()
        {
        }

        public override string GetTileUrl(int indexX, int indexY, int level)
        {
            string subDomain = subDomains[(indexX + indexY + level) % subDomains.Length];

            return string.Format(this.Url, subDomain, indexX, indexY, level);
        }



        public override void Initialize()
        {
            this.Bounds = new Rectangle2D(-CornerCoordinate, -CornerCoordinate, CornerCoordinate, CornerCoordinate);
            this.TileSize = 256;

            //CRS = new CoordinateReferenceSystem(3857);

            double res = CornerCoordinate * 2 / 256;
            double[] resolutions = new double[18];
            for (int i = 0; i < resolutions.Length; i++)
            {
                resolutions[i] = res;
                res *= 0.5;
            }
            this.Resolutions = resolutions;

            switch (this.MapType)
            {
                case GMapsType.Map:
                    this.Url = MapTileUri;
                    break;
                case GMapsType.Satellite:
                    this.Url = SatelliteTileUri;
                    break;
            }
            //枚举直接默认第一个
            //为在xaml中设置时用的，后来变了才出发onChange事件

            base.Initialize();
        }

        //用DP属性,需要 初始化\刷新 等触发事件，而且一旦无需converter转换
        public GMapsType MapType
        {
            get { return (GMapsType)GetValue(MapTypeProperty); }
            set { SetValue(MapTypeProperty, value); }
        }
        public static readonly DependencyProperty MapTypeProperty =
            DependencyProperty.Register("MapType", typeof(GMapsType), typeof(TiledGoogleMapsLayer), new PropertyMetadata(new PropertyChangedCallback(new PropertyChangedCallback(OnMapTypePropertyChanged))));

        private static void OnMapTypePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TiledGoogleMapsLayer layer = d as TiledGoogleMapsLayer;
            if (layer.IsInitialized)
            {
                layer.ChangeTileSource();
            }
        }

        private void ChangeTileSource()
        {
            switch (this.MapType)
            {
                case GMapsType.Map:
                    this.Url = MapTileUri;
                    break;
                case GMapsType.Satellite:
                    this.Url = SatelliteTileUri;
                    break;
            }
            if (!base.IsInitialized)
            {
                base.Initialize();
            }
            else
            {
                base.Refresh();
            }
        }

    }
}
