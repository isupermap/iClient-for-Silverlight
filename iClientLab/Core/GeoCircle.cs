﻿using System;
using System.ComponentModel;
using SuperMap.Web.Core;

namespace iClientLab
{
    public class GeoCircle : GeoRegion
    {
        private double radius = double.NaN;
        private GeoPoint center = null;
        private int pointCount = 359;

        public double Radius
        {
            get
            {
                return radius;
            }
            set
            {
                radius = value;
                CreateRing();
            }
        }
        new public GeoPoint Center
        {
            get
            {
                return center;
            }
            set
            {
                center = value;
                CreateRing();
            }
        }
        public int PointCount
        {
            get
            {
                return pointCount;
            }
            set
            {
                pointCount = value;
                CreateRing();
            }
        }
        private void CreateRing()
        {
            this.Parts.Clear();
            if (!double.IsNaN(Radius) && Radius > 0 && Center != null && PointCount > 2)
            {
                Point2DCollection pnts = new Point2DCollection();
                for (int i = 0; i < PointCount; i++)
                {
                    double rad = 2 * Math.PI / PointCount * i;
                    double x = Math.Cos(rad) * radius + Center.X;
                    double y = Math.Sin(rad) * radius + Center.Y;

                    pnts.Add(new Point2D(x, y));
                }
                double x0 = radius + Center.X;
                double y0 = Center.Y;
                pnts.Add(new Point2D(x0, y0));
                this.Parts.Add(pnts);
            }
        }
    }
}
