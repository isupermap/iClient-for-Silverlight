﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using SuperMap.Web.Actions;


namespace iClientLab
{
    public class MeasureDistance : MapAction, IDrawStyle
    {
        private PolylineElement polyline;
        private Point2DCollection points;
        private bool isActivated;
        private bool isDrawing;

        private Point2D startPoint;
        private GeoPoint startP;
        private double sum = 0;
        private double sumLast;
        private TextBlock measureMiddleResult;
        private TextBlock measureFinallyResult;
        private int clickeds = 0;
        private bool isOver;
        private List<TextBlock> textBlockContainer = new List<TextBlock>();
        //用于暂存TextBlock
        private ElementsLayer tempLayer = new ElementsLayer();
        private Point2DCollection ps = new Point2DCollection();

        public MeasureDistance(Map map)
            : this(map, Cursors.Hand)
        { }

        public MeasureDistance(Map map, Cursor cursor)
        {
            Name = "MeasureDistance";
            Map = map;
            Stroke = new SolidColorBrush(Colors.Red);
            StrokeThickness = 3;
            Opacity = 1;
            Map.Cursor = cursor;

            measureFinallyResult = new TextBlock();
            measureFinallyResult.FontWeight = FontWeights.ExtraBlack;
        }

        private void Activate(Point2D firstPoint)
        {
            if (DrawLayer != null)
            {
                DrawLayer.Children.Clear();
            }
            if (Map != null)
            {
                Map.Layers.Remove(DrawLayer);
            }

            DrawLayer = new ElementsLayer();
           Map.Layers.Add(DrawLayer);
           Map.Layers.Add(tempLayer);

           polyline = new PolylineElement();
            #region 所有风格的控制
            polyline.Stroke = Stroke;
            polyline.StrokeThickness = StrokeThickness;
            polyline.StrokeMiterLimit = StrokeMiterLimit;
            polyline.StrokeDashOffset = StrokeDashOffset;
            polyline.StrokeDashArray = StrokeDashArray;
            polyline.StrokeDashCap = StrokeDashCap;
            polyline.StrokeEndLineCap = StrokeEndLineCap;
            polyline.StrokeLineJoin = StrokeLineJoin;
            polyline.StrokeStartLineCap = StrokeStartLineCap;
            polyline.Opacity = Opacity;
            polyline.Fill = Fill;
            polyline.FillRule = FillRule;
            #endregion

            points = new Point2DCollection();
            polyline.Point2Ds = points;
            points.Add(firstPoint);
            points.Add(firstPoint);

            DrawLayer.Children.Add(polyline);

            isDrawing = true;
            isActivated = true;
        }

        public override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            startPoint = Map.ScreenToMap(e.GetPosition(Map));

            if (!isActivated)
            {
                Activate(startPoint);
            }
            else
            {
                points.Add(startPoint);
            }

            startP = new GeoPoint(startPoint.X, startPoint.Y);
            //每段分别计算
            measureMiddleResult = new TextBlock();
            measureMiddleResult.FontWeight = FontWeights.ExtraBlack;
            measureMiddleResult.Foreground = new SolidColorBrush(Colors.Green);

            textBlockContainer.Add(measureMiddleResult);

            for (int k = clickeds; k < textBlockContainer.Count; k++)
            {
                if (clickeds == 0)
                {
                    break;
                }
                if (textBlockContainer[k - 1].Text == "")
                {
                    return;
                }
                sum = sum + Convert.ToDouble(textBlockContainer[k - 1].Text);
            }

            clickeds++;

            //记录每个点
            ps.Add(startPoint);
            //第一次开始或者是双击后继续
            if (isOver || clickeds <= 1)
            {
                sum = 0;
                DrawLayer.AddChild(measureFinallyResult, startPoint);
                isOver = false;
            }

            if (DrawLayer != null && clickeds > 1)
            {
                //重新算中点坐标和距离
                Point2D middlePoints = new Point2D((startPoint.X + ps[ps.Count - 2].X) / 2, (startPoint.Y + ps[ps.Count - 2].Y) / 2);
                double middleDistance = Math.Sqrt((startPoint.X - ps[ps.Count - 2].X) * (startPoint.X - ps[ps.Count - 2].X) + (startPoint.Y - ps[ps.Count - 2].Y) * (startPoint.Y - ps[ps.Count - 2].Y));


                TextBlock tb = new TextBlock();
                tb.FontWeight = FontWeights.ExtraBlack;
                tb.Foreground = new SolidColorBrush(Colors.Green);

                tb.Text = string.Format("{0:0.0000}", middleDistance);
                DrawLayer.AddChild(tb, middlePoints);
            }

            e.Handled = true;
            base.OnMouseLeftButtonDown(e);
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (isDrawing)
            {
                Point2D item = Map.ScreenToMap(e.GetPosition(Map));
                if (Math.Round(item.X, 4) == Math.Round(ps[0].X, 4) && Math.Round(item.Y, 4) == Math.Round(ps[0].Y, 4))
                {
                    return;
                }
                Point2D last = points[points.Count - 1];
                points.Remove(last);
                points.Add(item);

                //获取移动中的点
                Point2D movePoint = Map.ScreenToMap(e.GetPosition(Map));
                GeoPoint moveP = new GeoPoint(movePoint.X, movePoint.Y);

                //算出两点距离
                //double moveDistance = Math.Sqrt(Math.Abs(moveP.X - startP.X) + Math.Abs(moveP.Y - startP.Y));
                double moveDistance = Math.Sqrt((moveP.X - startP.X) * (moveP.X - startP.X) + (moveP.Y - startP.Y) * (moveP.Y - startP.Y));

                // measureMiddleResult.Text = moveDistance.ToString();
                measureMiddleResult.Text = string.Format("{0:0.0000}", moveDistance);

                Point2D middle = new Point2D((movePoint.X + startPoint.X) / 2, (movePoint.Y + startPoint.Y) / 2);

                // 清除临时TextBlocks,在这里清除能提高效率。
                tempLayer.Children.Clear();
                tempLayer.AddChild(measureMiddleResult, middle);

                sumLast = sum + moveDistance;

                // measureFinallyResult.Text = "总长度:" + s.ToString() + "km";
                measureFinallyResult.Text = "总长度:" + string.Format("{0:0.0000}", sumLast) + "km";
            }

            base.OnMouseMove(e);
        }

        public override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            e.Handled = true;
            base.OnMouseLeftButtonUp(e);
        }

        public override void OnDblClick(MouseButtonEventArgs e)
        {
            Deactivate();
            e.Handled = true;
            base.OnDblClick(e);
        }

        public override void Deactivate()
        {
            //重新开始，clickeds清零
            clickeds = 0;
            //清除ps上的点
            ps.Clear();
            //重新开始，textBlockContainer清零
            textBlockContainer.Clear();

            isActivated = false;
            isDrawing = false;
            isOver = true;
        }

        public FillRule FillRule { get; set; }
        internal ElementsLayer DrawLayer { get; set; }
        #region IDrawStyle 成员
        public Brush Fill { get; set; }
        public Brush Stroke { get; set; }
        public double StrokeThickness { get; set; }
        public double StrokeMiterLimit { get; set; }
        public double StrokeDashOffset { get; set; }
        public DoubleCollection StrokeDashArray { get; set; }
        public PenLineCap StrokeDashCap { get; set; }
        public PenLineCap StrokeEndLineCap { get; set; }
        public PenLineCap StrokeStartLineCap { get; set; }
        public PenLineJoin StrokeLineJoin { get; set; }
        public double Opacity { get; set; }
        #endregion
    }
}
