﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using SuperMap.Web.Actions;


namespace iClientLab
{
    public class MeasureArea : MapAction, IDrawStyle
    {
        private PolygonElement polygon;
        private Point2DCollection points;
        private bool isActivated;
        private bool isDrawing;

        private Point2D startPoint;
        private GeoPoint startP;
        private TextBlock measureMiddleResult;
        private int clickeds = 0;
        private List<TextBlock> textBlockContainer = new List<TextBlock>();
        private ElementsLayer tempLayer = new ElementsLayer();
        private Point2DCollection ps = new Point2DCollection();


        private TextBlock lastTextBlock = new TextBlock();
        private TextBlock areaTextBlock = new TextBlock();

        public MeasureArea(Map map)
            : this(map, Cursors.Hand)
        { }

        public MeasureArea(Map map, Cursor cursor)
        {
            Name = "MeasureArea";
            Map = map;
            Stroke = new SolidColorBrush(Colors.Red);
            Fill = new SolidColorBrush(Color.FromArgb(0x99, 255, 255, 255));
            StrokeThickness = 3;
            Opacity = 1;
            Map.Cursor = cursor;

            lastTextBlock.FontWeight = FontWeights.ExtraBlack;
            lastTextBlock.Foreground = new SolidColorBrush(Colors.Green);
        }

        private void Activate(Point2D firstPoint)
        {
            if (DrawLayer != null)
            {
                DrawLayer.Children.Clear();
            }
            if (Map != null)
            {
                Map.Layers.Remove(DrawLayer);
            }

            DrawLayer = new ElementsLayer();
            Map.Layers.Add(DrawLayer);
            Map.Layers.Add(tempLayer);

            polygon = new PolygonElement();
            #region 所有风格的控制
            polygon.Stroke = Stroke;
            polygon.StrokeThickness = StrokeThickness;
            polygon.StrokeMiterLimit = StrokeMiterLimit;
            polygon.StrokeDashOffset = StrokeDashOffset;
            polygon.StrokeDashArray = StrokeDashArray;
            polygon.StrokeDashCap = StrokeDashCap;
            polygon.StrokeEndLineCap = StrokeEndLineCap;
            polygon.StrokeLineJoin = StrokeLineJoin;
            polygon.StrokeStartLineCap = StrokeStartLineCap;
            polygon.Opacity = Opacity;
            polygon.Fill = Fill;
            polygon.FillRule = FillRule;
            #endregion

            points = new Point2DCollection();
            polygon.Point2Ds = points;
            points.Add(firstPoint);
            points.Add(firstPoint);

            DrawLayer.Children.Add(polygon);

            isDrawing = true;
            isActivated = true;
        }

        public override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            startPoint = Map.ScreenToMap(e.GetPosition(Map));

            if (!isActivated)
            {
                Activate(startPoint);
            }
            else
            {
                points.Add(startPoint);
            }

            startP = new GeoPoint(startPoint.X, startPoint.Y);

            measureMiddleResult = new TextBlock();
            measureMiddleResult.FontWeight = FontWeights.ExtraBlack;
            measureMiddleResult.Foreground = new SolidColorBrush(Colors.Green);

            textBlockContainer.Add(measureMiddleResult);
            clickeds++;
            ps.Add(startPoint);

            if (DrawLayer != null && clickeds > 1)
            {
                Point2D middlePoints = new Point2D((startPoint.X + ps[ps.Count - 2].X) / 2, (startPoint.Y + ps[ps.Count - 2].Y) / 2);
                double middleDistance = Math.Sqrt((startPoint.X - ps[ps.Count - 2].X) * (startPoint.X - ps[ps.Count - 2].X) + (startPoint.Y - ps[ps.Count - 2].Y) * (startPoint.Y - ps[ps.Count - 2].Y));

                TextBlock tb = new TextBlock();
                tb.FontWeight = FontWeights.ExtraBlack;
                tb.Foreground = new SolidColorBrush(Colors.Green);

                tb.Text = string.Format("{0:0.0000}", middleDistance / 1000);
                DrawLayer.AddChild(tb, middlePoints);
            }
            e.Handled = true;
            base.OnMouseLeftButtonDown(e);
        }


        public override void OnMouseMove(MouseEventArgs e)
        {
            if (isDrawing)
            {
                Point2D item = Map.ScreenToMap(e.GetPosition(Map));

                Point2D last = points[points.Count - 1];
                if (Math.Round(item.X, 4) == Math.Round(ps[0].X, 4) && Math.Round(item.Y, 4) == Math.Round(ps[0].Y, 4))
                {
                    return;
                }
                points.Remove(last);
                points.Add(item);

                Point2D movePoint = Map.ScreenToMap(e.GetPosition(Map));
                GeoPoint moveP = new GeoPoint(movePoint.X, movePoint.Y);
                double moveDistance = Math.Sqrt((moveP.X - startP.X) * (moveP.X - startP.X) + (moveP.Y - startP.Y) * (moveP.Y - startP.Y));
                measureMiddleResult.Text = string.Format("{0:0.0000}", moveDistance / 1000);
                Point2D middle = new Point2D((movePoint.X + startPoint.X) / 2, (movePoint.Y + startPoint.Y) / 2);

                tempLayer.Children.Clear();
                tempLayer.AddChild(measureMiddleResult, middle);

                double a = GetArea(points);
                areaTextBlock.Text = a.ToString();
                this.tempLayer.AddChild(areaTextBlock, polygon.Bounds.Center);
                //得到最后一点和第一个点之间的TextBlock
                if (points.Count > 2)
                {
                    double lastToFirst = Math.Sqrt(Math.Abs(points[points.Count - 1].X - points[0].X) * Math.Abs(points[points.Count - 1].X - points[0].X) + Math.Abs(points[points.Count - 1].Y - points[0].Y) * Math.Abs(points[points.Count - 1].Y - points[0].Y));
                    lastTextBlock.Text = string.Format("{0:0.0000}", lastToFirst / 1000);
                    Point2D lastToFirstMiddle = new Point2D((points[points.Count - 1].X + points[0].X) / 2, (points[points.Count - 1].Y + points[0].Y) / 2);
                    this.tempLayer.AddChild(lastTextBlock, lastToFirstMiddle);
                }
            }
            base.OnMouseMove(e);
        }

        private double GetArea(Point2DCollection point2DCollection)
        {
            #region 法1：

            double tempArea = 0;
            double x1, x2, y1, y2;
            for (int i = 0; i < point2DCollection.Count - 1; i++)
            {
                x1 = point2DCollection[i].X;
                x2 = point2DCollection[i + 1].X;
                y1 = point2DCollection[i].Y;
                y2 = point2DCollection[i + 1].Y;
                //tempArea += x1 * yDiff - y1 * xDiff;
                tempArea += x1 * y2 - x2 * y1;
            }
            tempArea += point2DCollection[point2DCollection.Count - 1].X * point2DCollection[0].Y -
                        point2DCollection[point2DCollection.Count - 1].Y * point2DCollection[0].X;
            return Math.Abs(tempArea) / 2000000;

            #endregion

            #region 法2：
            //int i, j;
            //double ar = 0;

            //for (i = 0; i < point2DCollection.Count; i++)
            //{
            //    j = (i + 1) % point2DCollection.Count;
            //    ar += point2DCollection[i].X * point2DCollection[j].Y;
            //    ar -= point2DCollection[i].Y * point2DCollection[j].X;
            //}

            //ar /= 2;
            //return (ar < 0 ? -ar : ar);
            #endregion
        }

        public override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            e.Handled = true;
            base.OnMouseLeftButtonUp(e);
        }

        public override void OnDblClick(MouseButtonEventArgs e)
        {
            if (DrawLayer != null)
            {
                Point2D middlePoints = new Point2D((startPoint.X + ps[0].X) / 2, (startPoint.Y + ps[0].Y) / 2);
                double middleDistance = Math.Sqrt(Math.Abs(startPoint.X - ps[0].X) * Math.Abs(startPoint.X - ps[0].X) + Math.Abs(startPoint.Y - ps[0].Y) * Math.Abs(startPoint.Y - ps[0].Y));

                TextBlock lasttb = new TextBlock();
                lasttb.FontWeight = FontWeights.ExtraBlack;
                lasttb.Foreground = new SolidColorBrush(Colors.Green);

                lasttb.Text = string.Format("{0:0.0000}", middleDistance / 1000);
                DrawLayer.AddChild(lasttb, middlePoints);

                TextBlock b = new TextBlock();
                b.Text = string.Format("{0:0.0000}平方千米", GetArea(points));
                DrawLayer.AddChild(b, polygon.Bounds.Center);

            }


            Deactivate();
            e.Handled = true;
            base.OnDblClick(e);
        }

        public override void Deactivate()
        {
            clickeds = 0;
            ps.Clear();
            textBlockContainer.Clear();

            if (Map != null)
            {
                Map.Layers.Remove(tempLayer);
            }

            isActivated = false;
            isDrawing = false;
        }

        public FillRule FillRule { get; set; }
        internal ElementsLayer DrawLayer { get; set; }
        #region IDrawStyle 成员
        public Brush Fill { get; set; }
        public Brush Stroke { get; set; }
        public double StrokeThickness { get; set; }
        public double StrokeMiterLimit { get; set; }
        public double StrokeDashOffset { get; set; }
        public DoubleCollection StrokeDashArray { get; set; }
        public PenLineCap StrokeDashCap { get; set; }
        public PenLineCap StrokeEndLineCap { get; set; }
        public PenLineCap StrokeStartLineCap { get; set; }
        public PenLineJoin StrokeLineJoin { get; set; }
        public double Opacity { get; set; }
        #endregion
    }
}

