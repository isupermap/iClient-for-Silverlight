﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iClientLab
{

    [TemplatePart(Name = "ZoomInElement", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "ZoomOutElement", Type = typeof(RepeatButton))]
    [TemplatePart(Name = "ZoomSlider", Type = typeof(Slider))]
    public class SimpleZoomSlider : Control
    {

        public static readonly DependencyProperty MapProperty = DependencyProperty.Register("Map", typeof(Map), typeof(SimpleZoomSlider), new PropertyMetadata(new PropertyChangedCallback(OnMapPropertyChanged)));
        public Map Map
        {
            get
            {
                return (base.GetValue(MapProperty) as Map);
            }
            set
            {
                if (this.Map != null)
                {
                    this.Map.AngleChanged -= this.angleChangedHandler;
                    this.Map.ViewBoundsChanged -= new EventHandler<ViewBoundsEventArgs>(Map_ViewBoundsChanged);
                    this.Map.ViewBoundsChanging -= new EventHandler<ViewBoundsEventArgs>(Map_ViewBoundsChanging);
                }

                base.SetValue(MapProperty, value);
                this.Map.ViewBoundsChanging += new EventHandler<ViewBoundsEventArgs>(Map_ViewBoundsChanging);
                this.Map.ViewBoundsChanged += new EventHandler<ViewBoundsEventArgs>(Map_ViewBoundsChanged);
            }
        }
        private static void OnMapPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as SimpleZoomSlider).Map = e.NewValue as Map;
        }

        private bool hasTieldCachedLayer;
        private double[] layerResolutions;
        private bool isResolutionsDefined;
        private bool isSetLater;

        private RepeatButton zoomInElement;
        private RepeatButton zoomOutElement;
        private Slider sliderElement;

        private Map.AngleChangedEventHandler angleChangedHandler;

        public SimpleZoomSlider()
        {
            base.DefaultStyleKey = typeof(SimpleZoomSlider);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.sliderElement = base.GetTemplateChild("ZoomSlider") as Slider;
            this.zoomInElement = base.GetTemplateChild("ZoomInElement") as RepeatButton;
            this.zoomOutElement = base.GetTemplateChild("ZoomOutElement") as RepeatButton;

            this.SetupZoom();

            if (this.sliderElement != null)
            {
                sliderElement.MouseEnter += new MouseEventHandler(zoomSliderElement_MouseEnter);
                sliderElement.MouseLeave += new MouseEventHandler(zoomSliderElement_MouseLeave);

                this.sliderElement.ValueChanged += new RoutedPropertyChangedEventHandler<double>(this.ZoomSlider_ValueChanged);
            }
            if (this.zoomInElement != null)
            {
                this.zoomInElement.Click += new RoutedEventHandler(this.ZoomInButton_Click);
            }
            if (this.zoomOutElement != null)
            {
                this.zoomOutElement.Click += new RoutedEventHandler(this.ZoomOutButton_Click);
            }
        }

        private bool GoToState(bool useTransitions, string stateName)
        {
            return VisualStateManager.GoToState(this, stateName, useTransitions);
        }
        private void zoomSliderElement_MouseLeave(object sender, MouseEventArgs e)
        {
            isSliderGetFocused = false;
        }

        private void zoomSliderElement_MouseEnter(object sender, MouseEventArgs e)
        {
            isSliderGetFocused = true;
        }

        private void SetupZoom()
        {
            if (this.Map == null)
            {
                return;
            }

            foreach (Layer layer in this.Map.Layers)
            {
                if (layer is TiledCachedLayer)
                {
                    hasTieldCachedLayer = true;
                    break;
                }
            }
            if (this.Map.Scales == null && this.Map.Resolutions == null && !hasTieldCachedLayer)
            {
                if (this.sliderElement != null)
                {
                    this.sliderElement.Visibility = Visibility.Collapsed;
                }
                return;
            }

            this.layerResolutions = this.Map.Resolutions;
            if (this.sliderElement != null)
            {
                if (this.layerResolutions != null)
                {
                    this.sliderElement.Minimum = 0.0;
                    this.sliderElement.Maximum = this.layerResolutions.Length - 1;
                    double num2 = this.GetValueFromMap(this.Map.ViewBounds);
                    if (num2 >= 0.0)
                    {
                        this.sliderElement.Value = num2;
                    }
                    this.sliderElement.Visibility = Visibility.Visible;

                    if (num2 > 0.0)
                    {
                        this.isResolutionsDefined = true;
                    }
                }
                else if (isSetLater)
                {
                    this.sliderElement.Visibility = Visibility.Collapsed;
                }//if由于layerResolutions后设而造成的,主要是tiledCachedLayer
            }
        }

        private void ZoomViewEntire_Click(object sender, RoutedEventArgs e)
        {
            if (this.Map != null)
            {
                this.Map.ViewEntire();
            }
        }

        private void ZoomInButton_Click(object sender, RoutedEventArgs e)
        {
            if ((this.sliderElement != null) && (this.sliderElement.Visibility == Visibility.Visible))
            {
                if (this.layerResolutions != null)
                {
                    double a = this.sliderElement.Value;
                    a++;
                    int resolutionsValue = Convert.ToInt32(Math.Round(a));
                    if (resolutionsValue <= (this.layerResolutions.Length - 1))
                    {
                        Map.ZoomToResolution(this.layerResolutions[resolutionsValue]);
                        sliderElement.Value = a;
                        isThumbDragging = true;
                    }
                }
            }
            else
            {
                this.Map.Zoom(2);
            }
        }

        private void ZoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            if ((this.sliderElement != null) && (this.sliderElement.Visibility == Visibility.Visible))
            {
                if (this.layerResolutions != null)
                {
                    double a = this.sliderElement.Value;
                    a--;
                    int resolutionsValue = Convert.ToInt32(Math.Round(a));
                    if (resolutionsValue >= 0)
                    {
                        this.Map.ZoomToResolution(this.layerResolutions[Convert.ToInt32(Math.Round(a))]);
                        this.sliderElement.Value = a;
                        isThumbDragging = true;
                    }
                }
            }
            else
            {
                this.Map.Zoom(0.5);
            }
        }

        private bool isThumbDragging;
        private bool isSliderGetFocused;

        private void ZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int index = Convert.ToInt32(Math.Round(e.NewValue));
            if (isSliderGetFocused && layerResolutions != null)
            {
                this.Map.ZoomToResolution(this.layerResolutions[index]);
                isThumbDragging = true;
            }

        }

        private void Map_ViewBoundsChanging(object sender, ViewBoundsEventArgs e)
        {
            if (!isResolutionsDefined)
            {
                SetupZoom();
                if (hasTieldCachedLayer)
                {
                    isSetLater = true;
                }
            }
            if ((sliderElement != null) && !isThumbDragging)
            {
                double num = GetValueFromMap(Map.ViewBounds);

                if ((num >= 0.0) && (sliderElement.Value != num))
                {
                    sliderElement.Value = num;
                }
            }
        }

        private void Map_ViewBoundsChanged(object sender, ViewBoundsEventArgs e)
        {
            if (!this.isResolutionsDefined)
            {
                this.SetupZoom();
                if (this.hasTieldCachedLayer)
                {
                    isSetLater = true;
                }
            }

            if ((this.sliderElement != null) && !this.isThumbDragging)
            {
                double num = this.GetValueFromMap(this.Map.ViewBounds);

                if ((num >= 0.0) && (this.sliderElement.Value != num))
                {
                    this.sliderElement.Value = num;
                }
            }
            this.isThumbDragging = false;
        }

        private double GetValueFromMap(Rectangle2D bounds)
        {
            if (((this.layerResolutions == null) || (this.layerResolutions.Length == 0)) || ((this.Map == null) || (bounds.IsEmpty)))
            {
                return -1.0;
            }
            double num = bounds.Width / this.Map.ActualWidth;
            double t = this.Map.Resolution;
            for (int i = 0; i < (this.layerResolutions.Length - 1); i++)
            {
                double num3 = this.layerResolutions[i];
                double num4 = this.layerResolutions[i + 1];
                if (num >= num3)
                {
                    return (double)i;
                }
                if ((num < num3) && (num > num4))
                {
                    return (i + ((num3 - num) / (num3 - num4)));
                }
            }
            return Convert.ToDouble((int)(this.layerResolutions.Length - 1));
        }


    }
}