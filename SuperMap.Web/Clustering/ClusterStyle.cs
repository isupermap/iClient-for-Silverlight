﻿using System;
using System.Windows.Controls;
using System.Windows.Markup;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Clustering
{
    internal class ClusterStyle : MarkerStyle
    {
        public ClusterStyle()
        {
            String xaml = StyleUtility.XamlFileToString("/SuperMap.Web;component/Clustering/ClusterStyle.xaml");
            base.ControlTemplate = XamlReader.Load(xaml) as ControlTemplate;
            //base.ControlTemplate = ResourceData.Dictionary["ClusterStyle"] as ControlTemplate;
        }

        public override double OffsetX
        {
            get
            {
                return (this.Size / 2.0);
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        public override double OffsetY
        {
            get
            {
                return (this.Size / 2.0);
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        public double Size { get; set; }
    }
}
