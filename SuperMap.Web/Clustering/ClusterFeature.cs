﻿using SuperMap.Web.Core;

namespace SuperMap.Web.Clustering
{
    internal class ClusterFeature : Feature
    {
        public ClusterFeature(double size)
        {
            base.Style = new ClusterStyle { Size = size };
        }
    }
}
