﻿using SuperMap.Web.Core;

namespace SuperMap.Web.Rendering
{
    /// <summary>
    /// 	<para>${mapping_UniqueItem_Title}</para>
    /// 	<para>${mapping_UniqueItem_Description}</para>
    /// </summary>
    public class UniqueItem
    {
        /// <summary>${mapping_UniqueItem_attribute_value_D}</summary>
        public object Value { get; set; }
        /// <summary>${mapping_UniqueItem_attribute_style_D}</summary>
        public Style Style { get; set; }
    }
}
