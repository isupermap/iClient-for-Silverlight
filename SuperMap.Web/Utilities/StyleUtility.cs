﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Resources;

namespace SuperMap.Web.Utilities
{
    /// <summary>
    /// 	<para>${core_StyleUtility_Title}</para>
    /// 	<para>${core_StyleUtility_Description}</para>
    /// </summary>
    public static class StyleUtility
    {
        /// <summary>${core_StyleUtility_method_LoadObjectFromXamlFile_D}</summary>
        /// <returns>${core_StyleUtility_method_LoadObjectFromXamlFile_return}</returns>
        /// <param name="fileUri">${core_StyleUtility_method_LoadObjectFromXamlFile_param_fileUri}</param>
        public static object LoadObjectFromXamlFile(string fileUri)
        {
            string str = XamlFileToString(fileUri);
            if (!string.IsNullOrEmpty(str))
            {
                return XamlReader.Load(fileUri);
            }
            return null;
        }

        /// <summary>${core_StyleUtility_method_XamlFileToString_D}</summary>
        /// <returns>${core_StyleUtility_method_XamlFileToString_return}</returns>
        /// <param name="fileUri">${core_StyleUtility_method_XamlFileToString_param_fileUri}</param>
        public static string XamlFileToString(string fileUri)
        {
            Uri file = new Uri(fileUri, UriKind.Relative);
            StreamResourceInfo streamInfo = Application.GetResourceStream(file);

            if ((streamInfo != null) && (streamInfo.Stream != null))
            {
                using (StreamReader reader = new StreamReader(streamInfo.Stream))
                {
                    return reader.ReadToEnd();
                }
            }
            return string.Empty;
        }
    }
}
