﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.Core
{
	  /// <summary>
    /// 	<para>${core_Credential_Title}</para>
    /// 	<para>${core_Credential_Description}</para>
    /// </summary>
    public class Credential
    {
		   /// <summary>${core_Credential_attribute_CREDENTIAL_D}</summary>
		public static Credential CREDENTIAL = new Credential();

        private string _value;  
		private string _name = "token";

        private Credential()
        {
		}
        
		   /// <summary>${core_Credential_attribute_value_D}</summary>
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }
		  /// <summary>${core_Credential_attribute_name_D}</summary>
		 public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

         public virtual String GetUrlParameters()
		{
            string result = null;
            if (this.Value != null)
            {
                result = this.Name + "=" + this.Value;
            }
            return result;			
		}


    }
}
