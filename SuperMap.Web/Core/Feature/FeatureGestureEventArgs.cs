﻿#if WINDOWS_PHONE
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SuperMap.Web.Core
{
    public class FeatureGestureEventArgs:EventArgs
    {
        private GestureEventArgs source;

        internal FeatureGestureEventArgs(Feature f, GestureEventArgs args)
        {
            Feature = f;
            source = args;
        }

        public Point GetPosition(UIElement relativeTo)
        {
            return this.source.GetPosition(relativeTo);
        }

        public Feature Feature { get; private set; }

        public object OriginalSource
        {
            get
            {
                return this.source.OriginalSource;
            }
        }

        public bool Handled
        {
            get
            {
                return this.source.Handled;
            }
            set
            {
                this.source.Handled = value;
            }
        }
    }
}
#endif