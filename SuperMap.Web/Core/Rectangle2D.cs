﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
#if SILVERLIGHT
using System.Windows.Browser;
#endif
using SuperMap.Web.Resources;

namespace SuperMap.Web.Core
{
    /// <summary>  
    /// 	<para>${core_Rectangle2D_Title}</para>
    /// 	<para>${core_Rectangle2D_Description}</para>
    /// </summary>
#if SILVERLIGHT
    [ScriptableType, DataContract, StructLayout(LayoutKind.Sequential), TypeConverter(typeof(Rectangle2DConverter))]
#endif
    public struct Rectangle2D : IFormattable
    {
        private double _x;
        private double _y;
        private double _width;
        private double _height;

        private static readonly Rectangle2D s_empty = new Rectangle2D
            {
                _x = double.PositiveInfinity,
                _y = double.PositiveInfinity,
                _width = double.NegativeInfinity,
                _height = double.NegativeInfinity
            };


        /// <summary>${core_Rectangle2D_constructor_Double_Double_D}</summary>
        /// <param name="left">${core_Rectangle2D_constructor_Double_Double_param_left}</param>
        /// <param name="bottom">${core_Rectangle2D_constructor_Double_Double_param_bottom}</param>
        /// <param name="right">${core_Rectangle2D_constructor_Double_Double_param_right}</param>
        /// <param name="top">${core_Rectangle2D_constructor_Double_Double_param_top}</param>
        public Rectangle2D(double left, double bottom, double right, double top)
            : this()
        {
            FromXYWidthHeight(left, bottom, right - left, top - bottom);
        }

        /// <summary>${core_Rectangle2D_constructor_Point2D_Point2D_D}</summary>
        /// <overloads>${core_Rectangle2D_constructor_overloads}</overloads>
        /// <param name="point1">${core_Rectangle2D_constructor_Point2D_Point2D_param_bottomLeft}</param>
        /// <param name="point2">${core_Rectangle2D_constructor_Point2D_Point2D_param_topRight}</param>
        public Rectangle2D(Point2D point1, Point2D point2)
        {
            this._x = Math.Min(point1.X, point2.X);
            this._y = Math.Min(point1.Y, point2.Y);
            this._width = Math.Max((double)(Math.Max(point1.X, point2.X) - this._x), (double)0.0);
            this._height = Math.Max((double)(Math.Max(point1.Y, point2.Y) - this._y), (double)0.0);
        }

        /// <summary>${core_Rectangle2D_constructor_Point2D_Double_D}</summary>
        /// <param name="bottomLeft">${core_Rectangle2D_constructor_Point2D_Double_param_bottomLeft}</param>
        /// <param name="width">${core_Rectangle2D_constructor_Point2D_Double_param_width}</param>
        /// <param name="height">${core_Rectangle2D_constructor_Point2D_Double_param_height}</param>
        public Rectangle2D(Point2D bottomLeft, double width, double height)
            : this(bottomLeft.X, bottomLeft.Y, bottomLeft.X + width, bottomLeft.Y + height)
        {
        }

        private void FromXYWidthHeight(double x, double y, double width, double height)
        {
            if (width < 0.0)
            {
                throw new ArgumentException(ExceptionStrings.WidthLessThanZero);
            }
            if (height < 0.0)
            {
                throw new ArgumentException(ExceptionStrings.HeightLessThanZero);
            }
            this._x = x;
            this._y = y;
            this._width = width;
            this._height = height;
        }

        /// <summary>${core_Rectangle2D_method_CreateFromXYWidthHeight_D}</summary>
        /// <param name="x">${core_Rectangle2D_method_CreateFromXYWidthHeight_param_x}</param>
        /// <param name="y">${core_Rectangle2D_method_CreateFromXYWidthHeight_param_y}</param>
        /// <param name="width">${core_Rectangle2D_method_CreateFromXYWidthHeight_param_width}</param>
        /// <param name="height">${core_Rectangle2D_method_CreateFromXYWidthHeight_param_height}</param>
        /// <returns>${core_Rectangle2D_method_CreateFromXYWidthHeight_return}</returns>
        public static Rectangle2D CreateFromXYWidthHeight(double x, double y, double width, double height)
        {
            if (((x == double.PositiveInfinity) && (y == double.PositiveInfinity)) && ((width == double.NegativeInfinity) && (height == double.NegativeInfinity)))
            {
                return Empty;
            }

            return new Rectangle2D(x, y, x + width, y + height);
        }

        /// <summary>${core_Rectangle2D_attribute_empty_D_sl}</summary>
        public static Rectangle2D Empty
        {
            get
            {
                return s_empty;
            }
        }

        /// <summary>${core_Rectangle2D_attribute_isEmpty_D}</summary>
        public bool IsEmpty
        {
            get
            {
                return (_width < 0.0 || _width.Equals(double.NaN));
            }
        }

        /// <summary>${core_Rectangle2D_attribute_left_D}</summary>
#if SILVERLIGHT
        [ScriptableMember]
#endif
        public double Left
        {
            get
            {
                return this._x;
            }
        }

        /// <summary>${core_Rectangle2D_attribute_right_D}</summary>
#if SILVERLIGHT
        [ScriptableMember]
#endif
        public double Right
        {
            get
            {
                if (this.IsEmpty)
                {
                    return double.NegativeInfinity;
                }
                return (this._x + this._width);
            }
        }

        /// <summary>${core_Rectangle2D_attribute_bottom_D}</summary>
#if SILVERLIGHT
        [ScriptableMember]
#endif
        public double Bottom
        {
            get
            {
                if (this.IsEmpty)
                {
                    return double.NegativeInfinity;
                }
                return this._y;
            }
        }

        /// <summary>${core_Rectangle2D_attribute_top_D}</summary>
#if SILVERLIGHT
        [ScriptableMember]
#endif
        public double Top
        {
            get
            {
                return (this._y + this._height);
            }
        }

        /// <summary>${core_Rectangle2D_attribute_center_D}</summary>
#if SILVERLIGHT
        [ScriptableMember, DataMember(Name = "center")]
#endif
        public Point2D Center
        {
            get
            {
                return new Point2D((this.Left + this.Right) / 2, (this.Bottom + this.Top) / 2);
            }
        }

        /// <summary>${core_Rectangle2D_attribute_height_D}</summary>
        public double Height
        {
            get
            {
                return this.Top - this.Bottom;
            }
        }

        /// <summary>${core_Rectangle2D_attribute_width_D}</summary>
        public double Width
        {
            get
            {
                return this.Right - this.Left;
            }
        }

        /// <summary>${core_Rectangle2D_attribute_topRight_D}</summary>
        public Point2D TopRight
        {
            get
            {
                return new Point2D(this.Right, this.Top);
            }
        }

        /// <summary>${core_Rectangle2D_attribute_bottomLeft_D}</summary>
        public Point2D BottomLeft
        {
            get
            {
                return new Point2D(this.Left, this.Bottom);
            }
        }
        /// <summary>${core_Rectangle2D_constructor_Point2D_Point2D_param_TopLeft}</summary>
        public Point2D TopLeft
        {
            get
            {
                return new Point2D(this.Left, this.Top);
            }
        }
        /// <summary>${core_Rectangle2D_constructor_Point2D_Point2D_param_BottomRight}</summary>
        public Point2D BottomRight
        {
            get
            {
                return new Point2D(this.Right, this.Bottom);
            }
        }

        /// <summary>${core_Rectangle2D_operators_DoubleEquals_D}</summary>
        /// <returns>${core_Rectangle2D_operators_DoubleEquals_return}</returns>
        /// <param name="rect1">${core_Rectangle2D_operators_DoubleEquals_param_rect}</param>
        /// <param name="rect2">${core_Rectangle2D_operators_DoubleEquals_param_rect}</param>
        public static bool operator ==(Rectangle2D rect1, Rectangle2D rect2)
        {
            return (rect1.Left == rect2.Left) && (rect1.Bottom == rect2.Bottom) && (rect1.Top == rect2.Top) && (rect1.Right == rect2.Right);
        }
        /// <summary>${core_Rectangle2D_operators_NotEqual_D}</summary>
        /// <returns>${core_Rectangle2D_operators_NotEqual_return}</returns>
        /// <param name="rect1">${core_Rectangle2D_operators_DoubleEquals_param_rect}</param>
        /// <param name="rect2">${core_Rectangle2D_operators_DoubleEquals_param_rect}</param>
        public static bool operator !=(Rectangle2D rect1, Rectangle2D rect2)
        {
            return !(rect1 == rect2);
        }

        /// <summary>${core_Rectangle2D_method_equalsObject_D}</summary>
        /// <returns>${core_Rectangle2D_method_equalsObject_return}</returns>
        /// <overloads>${core_Rectangle2D_method_equals_overloads}</overloads>
        /// <param name="obj">${core_Rectangle2D_method_equalsObject_param_rect}</param>
        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is Rectangle2D))
            {
                return false;
            }
            Rectangle2D rectangle = (Rectangle2D)obj;
            return (this == rectangle);
        }
        /// <summary>${core_Rectangle2D_method_GetHashCode_D}</summary>
        public override int GetHashCode()
        {
            return ((this.Left.GetHashCode() ^ this.Bottom.GetHashCode()) ^ this.Right.GetHashCode() ^ this.Top.GetHashCode());
        }

        /// <summary>${core_Rectangle2D_method_toString_D}</summary>
        /// <returns>${core_Rectangle2D_method_toString_return}</returns>
        /// <overloads>${core_Rectangle2D_method_toString_overloads}</overloads>
        public override string ToString()
        {
            return ((IFormattable)this).ToString(null, null);
        }

        /// <summary>${core_Rectangle2D_method_equals_D}</summary>
        /// <returns>${core_Rectangle2D_method_equals_return}</returns>
        /// <overloads>${core_Rectangle2D_method_equals_overloads}</overloads>
        /// <param name="rectangle">${core_Rectangle2D_method_equals_param_rectangle}</param>
        public bool Equals(Rectangle2D rectangle)
        {
            return (this == rectangle);
        }

        /// <summary>${core_Rectangle2D_method_offset_D}</summary>
        /// <param name="dx">${core_Rectangle2D_method_offset_param_dx}</param>
        /// <param name="dy">${core_Rectangle2D_method_offset_param_dy}</param>
        public void Offset(double dx, double dy)
        {
            this._x += dx;
            this._y += dy;
        }

        /// <summary>${core_Rectangle2D_method_inflate_D_sl}</summary>
        /// <param name="width">${core_Rectangle2D_method_inflate_param_width_sl}</param>
        /// <param name="height">${core_Rectangle2D_method_inflate_param_height_sl}</param>
        public void Inflate(double width, double height)
        {
            if (width < -this.Width * 0.5)
            {
                throw new ArgumentException(ExceptionStrings.WidthLessThanZero, "width");
            }
            if (height < -this.Height * 0.5)
            {
                throw new ArgumentException(ExceptionStrings.HeightLessThanZero, "height");
            }

            this._x -= width;
            this._y -= height;
            this._width += width * 2;
            this._height += height * 2;
        }

        /// <summary>${core_Rectangle2D_method_intersectsWith_D}</summary>
        /// <returns>${core_Rectangle2D_method_intersectsWith_return}</returns>
        /// <param name="rect">${core_Rectangle2D_method_intersectsWith_param_rect}</param>
        public bool IntersectsWith(Rectangle2D rect)
        {
            if ((this._width < 0.0) || (rect.Width < 0.0))
            {
                return false;
            }

            return (rect.Left <= this.Right) && (rect.Right >= this.Left) && (rect.Bottom <= this.Top) && (rect.Top >= this.Bottom);
        }

        /// <summary>${core_Rectangle2D_method_intersect_D}</summary>
        /// <param name="rect">${core_Rectangle2D_method_intersect_param_rect}</param>
        public void Intersect(Rectangle2D rect)
        {
            if (!this.IntersectsWith(rect))
            {
                this = s_empty;
            }
            else
            {
                double maxLeft = Math.Max(this.Left, rect.Left);
                double maxBottom = Math.Max(this.Bottom, rect.Bottom);
                this._width = Math.Max((Math.Min(this.Right, rect.Right) - maxLeft), (double)0.0);
                this._height = Math.Max((Math.Min(this.Top, rect.Top) - maxBottom), (double)0.0);
                this._x = maxLeft;
                this._y = maxBottom;
            }
        }

        /// <summary>${core_Rectangle2D_method_union_Point2D_D}</summary>
        /// <overloads>${core_Rectangle2D_method_union_overloads}</overloads>
        /// <param name="point">${core_Rectangle2D_method_union_Point2D_param_point}</param>
        public void Union(Point2D point)
        {
            this.Union(new Rectangle2D(point, point));
        }

        /// <summary>${core_Rectangle2D_method_union_Rectangle2D_D}</summary>
        /// <param name="rect">${core_Rectangle2D_method_union_Rectangle2D_param_rect}</param>
        public void Union(Rectangle2D rect)
        {
            if (this.IsEmpty)
            {
                this = rect;
            }
            else if (!rect.IsEmpty)
            {
                double minLeft = Math.Min(this.Left, rect.Left);
                double minBottom = Math.Min(this.Bottom, rect.Bottom);

                if ((rect.Width == double.PositiveInfinity) || (this.Width == double.PositiveInfinity))
                {
                    this._width = double.PositiveInfinity;
                }
                else
                {
                    double maxRight = Math.Max(this.Right, rect.Right);
                    this._width = Math.Max((double)(maxRight - minLeft), (double)0.0);
                }
                if ((rect.Height == double.PositiveInfinity) || (this.Height == double.PositiveInfinity))
                {
                    this._height = double.PositiveInfinity;
                }
                else
                {
                    double maxHeight = Math.Max(this.Top, rect.Top);
                    this._height = Math.Max((double)(maxHeight - minBottom), (double)0.0);
                }
                this._x = minLeft;
                this._y = minBottom;

            }
        }

        /// <summary>${core_Rectangle2D_method_containsXY_D}</summary>
        /// <returns>${core_Rectangle2D_method_containsXY_return}</returns>
        /// <overloads>${core_Rectangle2D_method_contains_overloads}</overloads>
        /// <param name="x">${core_Rectangle2D_method_containsXY_param_x}</param>
        /// <param name="y">${core_Rectangle2D_method_containsXY_param_y}</param>
        public bool Contains(double x, double y)
        {
            return (this.Left <= x) && (x <= this.Right) && (this.Bottom <= y) && (y <= this.Top);
        }

        internal bool Within(Rectangle2D other)
        {
            return ((((this.Left >= other.Left) && (this.Right <= other.Right)) && (this.Bottom >= other.Bottom)) && (this.Top <= other.Top));
        }


        /// <summary>${core_Rectangle2D_method_containsPoint2D_D}</summary>
        /// <returns>${core_Rectangle2D_method_containsPoint2D_return_sl}</returns>
        /// <param name="point">${core_Rectangle2D_method_containsPoint2D_param_point}</param>
        public bool Contains(Point2D point)
        {
            return this.Contains(point.X, point.Y);
        }

        /// <summary>${core_Rectangle2D_method_containsRectangle2D_D}</summary>
        /// <returns>${core_Rectangle2D_method_containsRectangle2D_return_sl}</returns>
        /// <param name="rect">${core_Rectangle2D_method_containsRectangle2D_param_rect}</param>
        public bool Contains(Rectangle2D rect)
        {
            if (rect.IsEmpty)
            {
                return false;
            }
            return (this.Left <= rect.Left) && (rect.Right <= this.Right) && (this.Bottom <= rect.Bottom) && (rect.Top <= this.Top);
        }

        /// <summary>${core_Rectangle2D_method_Expand_D}</summary>
        public void Expand(double expandFactor)
        {
            if (expandFactor > 0)
            {

                this._x += (1 - expandFactor) * this._width * 0.5;
                this._y += (1 - expandFactor) * this._height * 0.5;
                this._height *= expandFactor;
                this._width *= expandFactor;
            }
        }

        /// <summary>${core_Rectangle2D_method_Update_D}</summary>
        public void Update(double left, double bottom, double right, double top)
        {
            FromXYWidthHeight(left, bottom, right - left, top - bottom);
        }

        //将Rectangle2D中的私有字段设为初始值
        internal void Reset()
        {
            this._x = 0;
            this._y = 0;
            this._width = 0;
            this._height = 0;
        }

        /// <summary>${core_Rectangle2D_method_UpdateFromXYWidthHeight_D}</summary>
        /// <param name="x">${core_Rectangle2D_method_CreateFromXYWidthHeight_param_x}</param>
        /// <param name="y">${core_Rectangle2D_method_CreateFromXYWidthHeight_param_y}</param>
        /// <param name="width">${core_Rectangle2D_method_CreateFromXYWidthHeight_param_width}</param>
        /// <param name="height">${core_Rectangle2D_method_CreateFromXYWidthHeight_param_height}</param>
        public void UpdateFromXYWidthHeight(double x, double y, double width, double height)
        {
            FromXYWidthHeight(x, y, width, height);
        }

        /// <summary>${core_Rectangle2D_method_toString_IFormatProvider_D}</summary>
        /// <returns>${core_Rectangle2D_method_toString_return}</returns>
        /// <param name="provider">${core_Rectangle2D_method_toString_IFormatProvider_param_provider}</param>
        public string ToString(IFormatProvider provider)
        {
            return ((IFormattable)this).ToString(null, provider);
        }
        string IFormattable.ToString(string format, IFormatProvider provider)
        {
            if (this.IsEmpty)
            {
                return "Empty";
            }
            return string.Format(provider, "{0:" + format + "},{1:" + format + "},{2:" + format + "},{3:" + format + "}", new object[] { this.Left, this.Bottom, this.Right, this.Top });
        }
    }
}
