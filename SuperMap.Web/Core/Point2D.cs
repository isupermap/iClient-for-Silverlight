﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
#if SILVERLIGHT
using System.Windows.Browser;
#endif

namespace SuperMap.Web.Core
{
    /// <summary>
    /// 	<para>${core_Point2D_Title}</para>
    /// 	<para>${core_Point2D_Description}</para>
    /// </summary>
#if SILVERLIGHT
    [ScriptableType, DataContract, StructLayout(LayoutKind.Sequential), TypeConverter(typeof(Point2DConverter))]
#endif
    public struct Point2D : IFormattable, INotifyPropertyChanged
    {
        private double x;
        private double y;

        private static readonly Point2D s_empty = new Point2D { x = double.PositiveInfinity, y = double.PositiveInfinity };

        /// <summary>${core_Point2D_constructor_Double_D}</summary>
        /// <summary>${core_Point2D_constructor_Double_D}</summary>
        /// <param name="x">${core_Point2D_constructor_Double_param_x}</param>
        /// <param name="y">${core_Point2D_constructor_Double_param_y}</param>
        public Point2D(double x, double y)
            : this()
        {
            X = x;
            Y = y;
        }

        /// <summary>${core_Point2D_attribute_x_D}</summary>
#if SILVERLIGHT
        [ScriptableMember, DataMember(Name = "x")]
#endif
        public double X
        {
            get
            {
                return this.x;
            }
            set
            {
                this.x = value;
                this.raisePropertyChanged("Point2D");
            }
        }
        /// <summary>${core_Point2D_attribute_y_D}</summary>
#if SILVERLIGHT
        [ScriptableMember, DataMember(Name = "y")]
#endif
        public double Y
        {
            get
            {
                return this.y;
            }
            set
            {
                this.y = value;
                this.raisePropertyChanged("Point2D");
            }
        }
        /// <summary>${core_Point2D_attribute_Tag_D}</summary>
        public object Tag
        {
            get;
            set;
        }


        /// <summary>${core_Point2D_attribute_empty_D}</summary>
        public static Point2D Empty
        {
            get { return s_empty; }
        }

        /// <summary>${core_Point2D_attribute_isEmpty_D}</summary>
        public bool IsEmpty
        {
            get
            {
                return double.IsNaN(this.x) || double.IsNaN(this.y) ||
                    this.x == double.PositiveInfinity || this.y == double.PositiveInfinity ||
                    this.x == double.NegativeInfinity || this.y == double.NegativeInfinity;
            }
        }


        /// <summary>${core_Point2D_operators_DoubleEquals_D}</summary>
        /// <returns>${core_Point2D_operators_DoubleEquals_return}</returns>
        /// <param name="point1">${core_Point2D_operators_DoubleEquals_param_point}</param>
        /// <param name="point2">${core_Point2D_operators_DoubleEquals_param_point}</param>
        public static bool operator ==(Point2D point1, Point2D point2)
        {
            return ((point1.X == point2.X) && (point1.Y == point2.Y));
        }
        /// <summary>${core_Point2D_operators_NotEqual_D}</summary>
        /// <returns>${core_Point2D_operators_NotEqual_return}</returns>
        /// <param name="point1">${core_Point2D_operators_DoubleEquals_param_point}</param>
        /// <param name="point2">${core_Point2D_operators_DoubleEquals_param_point}</param>
        public static bool operator !=(Point2D point1, Point2D point2)
        {
            return !(point1 == point2);
        }

        /// <overloads>${core_Point2D_method_equals_overloads}</overloads>
        /// <summary>${core_Point2D_method_equals_Object_D}</summary>
        /// <returns>${core_Point2D_method_equals_Object_return}</returns>
        /// <param name="obj">${core_Point2D_method_equals_Object_param_object}</param>
        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is Point2D))
            {
                return false;
            }
            Point2D p = (Point2D)obj;
            return (this == p);
        }
        /// <summary>${core_Point2D_method_GetHashCode_D}</summary>
        public override int GetHashCode()
        {
            return (this.X.GetHashCode() ^ this.Y.GetHashCode());
        }

        /// <returns>${core_Point2D_method_equals_Point2D_return}</returns>
        /// <summary>${core_Point2D_method_equals_Point2D_D}<br/></summary>
        /// <overloads>${core_Point2D_method_equals_overloads}</overloads>
        /// <param name="point">${core_Point2D_method_equals_Point2D_param_point}<br/></param>
        public bool Equals(Point2D point)
        {
            return (this == point);
        }

        /// <summary> ${core_Point2D_method_toString_D}</summary>
        /// <returns>${core_Point2D_method_toString_return}</returns>
        /// <overloads>${core_Point2D_method_toString_overloads}</overloads>
        public override string ToString()
        {
            return ((IFormattable)this).ToString(null, null);
        }
        /// <summary>${core_GeoRegion_method_Offset_D}</summary>
        /// <param name="deltaX">${core_GeoRegion_method_Offset_param_deltaX}</param>
        /// <param name="deltaY">${core_GeoRegion_method_Offset_param_deltaY}</param>
        public Point2D Offset(double deltaX, double deltaY)
        {
            return new Point2D(this.X + deltaX, this.Y + deltaY);
        }

        /// <summary>${core_Point2D_method_toString_IFormatProvider_D}</summary>
        /// <returns>${core_Point2D_method_toString_return}</returns>
        /// <overloads>${core_Point2D_method_toString_overloads}</overloads>
        /// <param name="provider">${core_Point2D_method_toString_IFormatProvider_param_provider}</param>
        public string ToString(IFormatProvider provider)
        {
            return ((IFormattable)this).ToString(null, provider);
        }
        string IFormattable.ToString(string format, IFormatProvider provider)
        {
            if (this.IsEmpty)
            {
                return "Empty";
            }
            return string.Format(provider, "{0:" + format + "},{1:" + format + "}", new object[] { this.x, this.y });
        }
        /// <summary>${core_Geometry_method_clone_D}</summary>
        public Point2D Clone()
        {
            return (Point2D)base.MemberwiseClone();
        }
        /// <summary>${mapping_Layer_event_PropertyChanged_D}</summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private void raisePropertyChanged(string propertyName)
        {
            var temp = this.PropertyChanged;
            if (temp != null)
            {
                temp(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
