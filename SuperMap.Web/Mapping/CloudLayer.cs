﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;
using System.Text;
#if SILVERLIGHT
using System.Json;
#endif

namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// 	<para>${mapping_CloudLayer_Title}</para>
    /// 	<para>${mapping_CloudLayer_Description}</para>
    /// </summary>
    public class CloudLayer : TiledCachedLayer
    {
        /// <summary>${mapping_CloudLayer_attribute_MapName_D}</summary>
        public string MapName { get; set; }
        private string type { get; set; }
        private double[] scales = null;

        /// <summary>${mapping_CloudLayer_constructor_None_D}</summary>
        public CloudLayer()
        {
            this.Url = "http://t1.supermapcloud.com/FileService/image";
            this.Bounds = new Rectangle2D(-2.00375083427892E7, -2.00375083427892E7, 2.00375083427892E7, 2.00375083427892E7);
            //this.Resolutions = new double[] { 156605.46875, 78302.734375, 39151.3671875, 19575.68359375, 9787.841796875, 4893.9208984375, 2446.96044921875, 1223.48022460937, 611.740112304687, 305.870056152344, 152.935028076172, 76.4675140380859, 38.233757019043, 19.1168785095215, 9.55843925476074, 4.77921962738037, 2.38960981369019, 1.19480490684509, 0.597402453422546 };
            this.Resolutions = new double[] { 156543.033928041, 78271.5169640203, 39135.7584820102,
                19567.8792410051, 9783.93962050254, 4891.96981025127, 2445.98490512563,
                1222.99245256282, 611.496226281409, 305.748113140704, 152.874056570352,
                76.4370282851761, 38.218514142588, 19.109257071294, 9.55462853564701,
                4.77731426782351, 2.38865713391175, 1.19432856695588, 0.597164283477938 };
            this.MapName = "quanguo";
            this.type = "web";
            this.TileSize = 256;
        }
        /// <summary>${mapping_CloudLayer_method_initialize_D}</summary>
        public override void Initialize()
        {
            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.Append(this.Url);
            urlBuilder.Append("?map={0}&type={1}&x={2}&y={3}&z={4}");
            this.Url = urlBuilder.ToString();
            base.Initialize();
        }
        /// <summary>${mapping_CloudLayer_method_GetTileUrl_D}</summary>
        public override string GetTileUrl(int indexX, int indexY, int level)
        {
            return String.Format(System.Globalization.CultureInfo.InvariantCulture,this.Url, this.MapName, this.type, indexX, indexY, level);
        }
    }
}

