﻿using System.Globalization;
using System.Windows.Controls;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Mapping
{
    internal class Tile
    {
        public Tile(int row, int col, double resolution, double[] resolutions, string url, bool useTransitions)
        {
            Row = row;
            Column = col;
            Resolution = resolution;
            Resolutions = resolutions;

            Url = url;
            UseTransitions = useTransitions;

            Progress = 0;
        }

        public Image Image { get; set; }
        public int Progress { get; set; }

        public int Column { get; private set; }
        public int Row { get; private set; }
        public double Resolution { get; private set; }
        public double[] Resolutions { get; private set; }

        public string Url { get; private set; }
        public bool UseTransitions { get; private set; }

        public int Level
        {
            get
            { 
                return MathUtil.GetNearestIndex(Resolution, Resolutions); 
            }
        }

        public static string GetTileKey(int row, int column, double resolution, double[] resolutions)
        {
            if (resolutions != null)
            {
                int level = MathUtil.GetNearestIndex(resolution, resolutions);
                return string.Format(CultureInfo.InvariantCulture, "{0}_{1}_{2}", row, column, level);
            }//只要有Resolutions则按照level返回。
            else
            {
                return string.Format(CultureInfo.InvariantCulture, "{0}_{1}_{2}", row, column, resolution);
            }
        }
        public string TileKey
        {
            get
            {
                return GetTileKey(this.Row, this.Column, this.Resolution, this.Resolutions);
            }
        }
    }
}
