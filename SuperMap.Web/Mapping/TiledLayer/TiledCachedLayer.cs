
namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// ${mapping_TiledCachedLayer_Title}<br/>
    /// ${mapping_TiledCachedLayer_Description}
    /// </summary>
    public abstract class TiledCachedLayer : TiledLayer
    {
        /// <summary>
        ///     ${pubilc_Constructors_Initializes} <see cref="TiledCachedLayer">TiledCachedLayer</see> ${pubilc_Constructors_instance}
        /// </summary>
        protected TiledCachedLayer()
        {
        }

        /// <summary>${mapping_TiledCachedLayer_method_getTileUrl_D}</summary>
        /// <returns>${mapping_TiledCachedLayer_method_getTileUrl_returns}</returns>
        /// <param name="indexX">${mapping_TiledCachedLayer_method_getTileUrl_param_indexX}</param>
        /// <param name="indexY">${mapping_TiledCachedLayer_method_getTileUrl_param_indexY}</param>
        /// <param name="level">${mapping_TiledCachedLayer_method_getTileUrl_param_level}</param>
        public abstract string GetTileUrl(int indexX, int indexY, int level);
        /// <summary>${mapping_TiledCachedLayer_method_getTileUrl_D}</summary>
        /// <returns>${mapping_TiledCachedLayer_method_getTileUrl_returns}</returns>
        /// <param name="indexX">${mapping_TiledCachedLayer_method_getTileUrl_param_indexX}</param>
        /// <param name="indexY">${mapping_TiledCachedLayer_method_getTileUrl_param_indexY}</param>
        /// <param name="resolution">${mapping_TiledCachedLayer_method_getTileUrl_param_resolution}</param>
        /// <param name="level">${mapping_TiledCachedLayer_method_getTileUrl_param_level}</param>
        protected sealed override string GetTileUrl(int indexX, int indexY, int level, double resolution)
        {
            return this.GetTileUrl(indexX, indexY, level);
        }

        /// <summary>${mapping_TiledCachedLayer_method_getCachedResolutions_D}</summary>
        /// <returns>${mapping_TiledCachedLayer_method_getCachedResolutions_returns}</returns>
        public override double[] GetCachedResolutions()
        {
            return this.Resolutions;
        }
        /// <summary>${mapping_TiledCachedLayer_attribute_resolutions_D}</summary>
        public virtual double[] Resolutions { get; protected set; }
    }
}


