﻿
namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// ${mapping_TiledDynamicLayer_Title}<br/>
    /// ${mapping_TiledDynamicLayer_Description}
    /// </summary>
    public abstract class TiledDynamicLayer : TiledLayer
    {
        /// <summary>
        ///     ${pubilc_Constructors_Initializes} <see cref="TiledDynamicLayer">TileDynamicLayer</see> ${pubilc_Constructors_instance}
        /// </summary>
        protected TiledDynamicLayer()
        {
        }

        /// <summary>${mapping_TiledDynamicLayer_method_getTileUrl_D}</summary>
        /// <returns>${mapping_TiledDynamicLayer_method_getTileUrl_param_return}</returns>
        /// <param name="indexX">${mapping_TiledDynamicLayer_method_getTileUrl_param_indexX}</param>
        /// <param name="indexY">${mapping_TiledDynamicLayer_method_getTileUrl_param_indexY}</param>
        /// <param name="resolution">${mapping_TiledDynamicLayer_method_getTileUrl_param_resolution}</param>
        public abstract string GetTileUrl(int indexX, int indexY, double resolution);

        /// <summary>${mapping_TiledDynamicLayer_attribute_GetTileUrl_D}</summary>
        protected sealed override string GetTileUrl(int indexX, int indexY, int level, double resolution)
        {
            return this.GetTileUrl(indexX, indexY, resolution);
        }

        /// <summary>${mapping_TiledDynamicLayer_method_getCachedResolutions_D}</summary>
        /// <returns>${mapping_TiledDynamicLayer_method_returns_D}</returns>
        public override double[] GetCachedResolutions()
        {
            return null;
        }
    }
}
