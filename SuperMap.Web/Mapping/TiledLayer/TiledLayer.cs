﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;
using System.IO;
using System.Diagnostics;

namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// 	<para>${mapping_TiledLayer_Title}</para>
    /// 	<para>${mapping_TiledLayer_Description}</para>
    /// </summary>
    public abstract class TiledLayer : ImageLayer
    {
        private static int uniqueId;//static
        private string uniqueLayerId;
        private Dictionary<string, Tile> pendingTiles;
        private double[] realResolutions;

        /// <summary>${mapping_TiledLayer_constructor_None_D}</summary>
        protected TiledLayer()
        {
            base.progressWeight = 0.0;
            uniqueLayerId = string.Format(CultureInfo.InvariantCulture, "TiledLayer{0}", uniqueId++);
            TileSize = MagicNumber.TILEDLAYER_DEFAULT_SIZE;
            pendingTiles = new Dictionary<string, Tile>();
        }

        /// <summary>${mapping_TiledLayer_method_getCachedResolutions_D}</summary>
        /// <returns>${mapping_TiledLayer_method_getCachedResolutions_returns}</returns>
        public abstract double[] GetCachedResolutions();
        /// <summary>${mapping_TiledLayer_attribute_GetTileUrl_D}</summary>
        protected virtual string GetTileUrl(int indexX, int indexY, int level, double resolution)
        {
            return null;
        }

        internal override void Draw(DrawParameter drawParameter)
        {
            if ((base.IsInitialized && base.IsVisible) && (!ViewBounds.IsEmpty) && (base.Container != null))
            {
                if (GetCachedResolutions() != null)
                {
                    realResolutions = this.GetCachedResolutions();
                }
                else
                {
                    realResolutions = drawParameter.Resoluitons;
                }

                int[] span = this.GetTileSpanWithin(ViewBounds, Resolution);
                if ((span[2] >= 0) || (span[3] >= 0))
                {
                    AddTiles(Resolution, span[0], span[1], span[2], span[3], drawParameter.UseTransitions);
                    RemoveTiles(ViewBounds, Resolution);
                    base.OnProgress(this.GetProgress());
                }
            }
        }


        /// <summary>${mapping_TiledLayer_method_refresh_D}</summary>
        public override void Refresh()
        {
            Cancel();
            if (base.Container != null)
            {
                base.Container.Children.Clear();
            }
            base.OnProgress(100);
            base.OnLayerChanged();
        }

        /// <summary>${mapping_TiledLayer_attribute_Cancel_D}</summary>
        protected override void Cancel()
        {
            base.Cancel();
            if (base.Container != null)
            {
                foreach (FrameworkElement element in base.Container.Children)
                {
                    element.Opacity = 1.0;
                }
            }
            foreach (Tile tile in pendingTiles.Values)
            {
                (tile.Image.Parent as Panel).Children.Remove(tile.Image);
                (tile.Image.Source as BitmapImage).UriSource = null;
                tile.Image.Source = null;
                tile.Image = null;
            }
            pendingTiles.Clear();
        }

        private void AddTiles(double resolution, int startCol, int startRow, int endCol, int endRow, bool useTransitions)
        {
            double num = ((endRow - startRow) * 0.5) + startRow;
            double num2 = ((endCol - startCol) * 0.5) + startCol;
            List<DistanceTile> list = new List<DistanceTile>();
            for (int i = startRow; i <= endRow; i++)
            {
                for (int j = startCol; j <= endCol; j++)
                {
                    string str = Tile.GetTileKey(i, j, resolution, realResolutions);
                    Image image = base.Container.FindName(uniqueLayerId + str) as Image;
                    if (image == null)
                    {
                        list.Add(new DistanceTile { Row = i, Column = j, Distance = Math.Pow(i - num, 2.0) + Math.Pow(j - num2, 2.0) });
                    }
                    else
                    {
                        image.ClearValue(LayerContainer.OutdatedProperty);
                    }//如果find，说明是上次的，可以标记outdated，予以清除
                }
            }
            list.Sort();//排序?按照distance删除重复的？
            int level = MathUtil.GetIndex(resolution, realResolutions);
            foreach (DistanceTile disTile in list)
            {
                string url = string.Empty;
                if (this.GetCachedResolutions() != null)
                {
                    if (level == -1)
                    {
                        return;//CachedLayer中没有Map的某个尺度
                    }
                    url = this.GetTileUrl(disTile.Column, disTile.Row, level, double.NaN);
                }
                else
                {
                    url = this.GetTileUrl(disTile.Column, disTile.Row, int.MinValue, resolution);
                    //统一管理token
#if SILVERLIGHT
                    if (Credential.CREDENTIAL.GetUrlParameters() != null)
                    {
                        if (url.IndexOf("?") > 0)
                            url += "&" + Credential.CREDENTIAL.GetUrlParameters();
                        else
                            url += "?" + Credential.CREDENTIAL.GetUrlParameters();
                    }
#endif
                }



                Tile tile = new Tile(disTile.Row, disTile.Column, resolution, realResolutions, url, useTransitions);


                if (TileLoading != null)
                {
                    double left = this.Origin.X + ((TileSize * tile.Column) * resolution);
                    double top = this.Origin.Y - ((TileSize * tile.Row) * resolution);
                    double right = left + (this.TileSize * resolution);
                    double bottom = top - (this.TileSize * resolution);
                    TileLoadEventArgs loadingArgs = new TileLoadEventArgs
                    {
                        Column = tile.Column,
                        Level = tile.Level,
                        Row = tile.Row,
                        Bounds = new Rectangle2D(left, bottom, right, top),
                    };
                    TileLoading(this, loadingArgs);
                }
                this.AddTile(tile);
            }
        }

        private void AddTile(Tile tile)
        {
            if (!string.IsNullOrEmpty(tile.Url) && !pendingTiles.ContainsKey(tile.Url))
            {
                BitmapImage bitmapImage = new BitmapImage()
                {
                    UriSource = new Uri(tile.Url, UriKind.Absolute)
                };

                EventHandler<DownloadProgressEventArgs> onProgressEventHandler = null;
                onProgressEventHandler = delegate(object sender, DownloadProgressEventArgs e)
                {
                    this.bmi_DownloadProgress(sender, e, tile, onProgressEventHandler);
                };
                bitmapImage.DownloadProgress += onProgressEventHandler;

                tile.Image = new Image
                {
                    Opacity = 0.0,
                    Tag = tile,
                    IsHitTestVisible = false,
                    Name = uniqueLayerId + tile.TileKey,
                    Stretch = Stretch.Fill,
                    Source = bitmapImage
                };
                tile.Image.ImageFailed += new EventHandler<ExceptionRoutedEventArgs>(this.img_ImageFailed);

                double resolution = tile.Resolution;
                //orginX originY
                double left = this.Origin.X + ((TileSize * tile.Column) * resolution);
                double top = this.Origin.Y - ((TileSize * tile.Row) * resolution);
                double right = left + (this.TileSize * resolution);
                double bottom = top - (this.TileSize * resolution);
                LayerContainer.SetBounds(tile.Image, new Rectangle2D(left, bottom, right, top));//计算该image的范围

                tile.Image.ImageOpened += delegate(object o, RoutedEventArgs e)
                {
                    if (base.Dispatcher.CheckAccess())
                    {
                        Action a = delegate
                        {
                            this.RaiseTileLoad(tile, new Rectangle2D(left, bottom, right, top));
                        };

                        this.Dispatcher.BeginInvoke(a);
                    }
                    else
                    {
                        this.RaiseTileLoad(tile, new Rectangle2D(left, bottom, right, top));
                    }
                };
                pendingTiles.Add(tile.Url, tile);
                base.Container.Children.Add(tile.Image);
            }
        }

        private void RaiseTileLoad(Tile tile, Rectangle2D bounds)
        {
            if (TileLoaded != null)
            {
                TileLoadEventArgs loadTileArgs = new TileLoadEventArgs();
                loadTileArgs.Column = tile.Column;
                loadTileArgs.ImageSource = tile.Image.Source;

                loadTileArgs.Level = tile.Level;
                loadTileArgs.Row = tile.Row;
                loadTileArgs.Bounds = bounds;

                TileLoaded(this, loadTileArgs);
            }
        }


        private void RemoveTiles(Rectangle2D bounds, double resolution)
        {
            if (!bounds.IsEmpty)
            {
                if (base.Container == null)
                {
                    pendingTiles.Clear();
                }
                else
                {
                    int level = MathUtil.GetIndex(resolution, realResolutions);

                    int[] span = this.GetTileSpanWithin(bounds, resolution);
                    if ((span[2] >= 0) || (span[3] >= 0))
                    {
                        int startCloumn = span[0];
                        int startRow = span[1];
                        int endCloumn = span[2];
                        int endRow = span[3];
                        for (int i = base.Container.Children.Count - 1; i >= 0; i--)
                        {
                            UIElement element = base.Container.Children[i];
                            Image image = element as Image;
                            if (image != null)
                            {
                                Tile tile = (Tile)image.Tag;
                                bool flag = (level != -1) ? (level != tile.Level) : (!DoubleUtil.AreClose(resolution, tile.Resolution));
                                //只要分级别，不管在map上还是layer上，都用level判断。否则用Resolution判断             
                                if ((tile.Row < (startRow - 1)) || (tile.Row > (endRow + 1)) || (tile.Column < (startCloumn - 1)) || (tile.Column > (endCloumn + 1)) || flag)
                                {
                                    string key = null;
                                    if (image.Source != null)
                                    {
                                        key = (image.Source as BitmapImage).UriSource.OriginalString;
                                        if (pendingTiles.ContainsKey(key))
                                        {
                                            pendingTiles.Remove(key);
                                        }
                                    }
                                    if (tile.Progress < 100)
                                    {
                                        if (image.Source is BitmapImage)
                                        {
                                            (image.Source as BitmapImage).UriSource = null;
                                        }
                                        image.Source = null;
                                        base.Container.Children.Remove(image);
                                    }
                                    else
                                    {
                                        image.SetValue(LayerContainer.OutdatedProperty, true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void bmi_DownloadProgress(object sender, DownloadProgressEventArgs e, Tile tile, EventHandler<DownloadProgressEventArgs> onProgressEventHandler)
        {
            tile.Progress = e.Progress;
            if (!string.IsNullOrEmpty(tile.Url) && (tile.Image != null))
            {
                if (e.Progress == 100)
                {
                    (sender as BitmapImage).DownloadProgress -= onProgressEventHandler;
                    if (!this.pendingTiles.ContainsKey(tile.Url))
                    {
                        if (base.Container != null)
                        {
                            base.Container.Children.Remove(tile.Image);
                        }
                        return;
                    }
                    bool useTransitions = tile.UseTransitions;
                    ShowImage(tile.Image, useTransitions);
                    pendingTiles.Remove(tile.Url);
                }
                base.OnProgress(GetProgress());
            }
        }

        //由bounds获取tile的起始和结束行列号
        //[startColumn,startRow,endColumn,endRow] //Returns [0,0,-1,-1] if fails
        private int[] GetTileSpanWithin(Rectangle2D bounds, double resolution)
        {
            bounds.Intersect(this.Bounds);
            if (!bounds.IsEmpty)
            {
                double x = this.Origin.X;
                double y = this.Origin.Y;

                int startColumn = (int)Math.Floor((double)(((bounds.Left - x) + (resolution * 0.5)) / (resolution * this.TileSize)));
                int startRow = (int)Math.Floor((double)(((y - bounds.Top) + (resolution * 0.5)) / (resolution * this.TileSize)));
                startColumn = (startColumn < 0) ? 0 : startColumn;
                startRow = (startColumn < 0) ? 0 : startRow;
                int endColumn = (int)Math.Floor((double)(((bounds.Right - x) - (resolution * 0.5)) / (resolution * this.TileSize)));
                int endRow = (int)Math.Floor((double)(((y - bounds.Bottom) - (resolution * 0.5)) / (resolution * this.TileSize)));
                return new int[] { startColumn, startRow, endColumn, endRow };

                //if (GetCachedResolutions() != null)
                //{
                //    return new int[] { startColumn, startRow, endColumn, endRow };
                //}
                //else
                //{
                //    return new int[] { startColumn - 1, startRow - 1, endColumn + 1, endRow + 1 };
                //}//动态的话，上下左右各扩充一张,但边缘处不美观
            }
            return new int[4] { 0, 0, -1, -1 };
        }

        private int GetProgress()
        {
            double num = 0.0;
            base.progressWeight = 0.0;
            if (base.Container == null)
            {
                return 100;
            }
            foreach (UIElement element in base.Container.Children)
            {
                Image image = element as Image;
                if (((image != null) && (image.Source != null)) && !((bool)image.GetValue(LayerContainer.OutdatedProperty)))
                {
                    Tile tag = image.Tag as Tile;
                    if (tag != null)
                    {
                        base.progressWeight++;
                        num += (tag.Progress * 0.99) + image.Opacity;
                    }
                }
            }
            using (Dictionary<string, Tile>.Enumerator enumerator2 = this.pendingTiles.GetEnumerator())
            {
                while (enumerator2.MoveNext())
                {
                    KeyValuePair<string, Tile> current = enumerator2.Current;
                    base.progressWeight++;
                }
            }
            if (base.progressWeight == 0.0)
            {
                return 100;
            }
            int num2 = (int)Math.Floor(num / base.progressWeight);
            if (num2 >= 0)
            {
                return num2;
            }
            return 0;
        }

        private void ShowImage(Image img, bool enableFading)
        {
            if (enableFading)
            {
                DoubleAnimation animation = new DoubleAnimation
                {
                    From = new double?(img.Opacity),
                    To = 1.0,
                    Duration = TimeSpan.FromSeconds(0.5)
                };
                animation.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("Opacity", new object[0]));

                Storyboard storyboard = new Storyboard();
                storyboard.Children.Add(animation);
                Storyboard.SetTarget(animation, img);
                storyboard.Completed += (s, e) =>
                {
                    base.Dispatcher.BeginInvoke(delegate { base.OnProgress(this.GetProgress()); });
                };
                storyboard.Begin();
            }
            else
            {
                img.Opacity = 1.0;
            }
            base.OnProgress(this.GetProgress()); //到处都有你的身影
        }

        private void img_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {
            this.downloadTileFailed(((Image)sender));
        }

        private void downloadTileFailed(Image image)
        {
            BitmapImage imageSrc = image.Source as BitmapImage;
            if (imageSrc != null)
            {
                if(this.pendingTiles.ContainsKey(imageSrc.UriSource.OriginalString))
                {
                    try
                    {
                        Tile tile = this.pendingTiles[imageSrc.UriSource.OriginalString];
                        tile.Progress = 100;
                        tile.Image.Opacity = 1;
                        if (this.pendingTiles.ContainsKey(imageSrc.UriSource.OriginalString))
                        {
                            pendingTiles.Remove(imageSrc.UriSource.OriginalString);
                        }
                    }
                    catch
                    {
                        //可能在执行过程中tile被移除掉了，加个catch保险吧。多线程……
                    }
                }
                base.OnProgress(this.GetProgress());
            }
        }

        internal sealed override bool ContinuousDraw
        {
            get
            {
                return true;
            }
        }

        /// <summary>${mapping_TiledLayer_attribute_tileSize_D}</summary>
        public int TileSize { get; set; }
        /// <summary>${mapping_TiledLayer_event_TileLoaded_D}</summary>
        public event EventHandler<TileLoadEventArgs> TileLoaded;
        /// <summary>${mapping_TiledLayer_event_TileLoading_D}</summary>
        public event EventHandler<TileLoadEventArgs> TileLoading;

        private Point2D mapOrigin = Point2D.Empty;
        /// <summary>${mapping_TiledLayer_attribute_origin_D}</summary>
        public Point2D Origin
        {
            set { this.mapOrigin = value; }
            get
            {
                return (this.mapOrigin.IsEmpty) ? new Point2D(this.Bounds.Left, this.Bounds.Top) : this.mapOrigin;
            }
        }
    }
    /// <summary>${mapping_TileLoadEventArgs_Title}</summary>
    public sealed class TileLoadEventArgs : EventArgs
    {
        internal TileLoadEventArgs()
        {
        }
        /// <summary>${mapping_TiledLayer_attribute_Column_D}</summary>
        public int Column
        {
            get;
            internal set;
        }
        /// <summary>${mapping_TiledLayer_attribute_ImageSource_D}</summary>
        public System.Windows.Media.ImageSource ImageSource
        {
            get;
            internal set;
        }
        /// <summary>${mapping_TiledLayer_attribute_Level_D}</summary>
        public int Level
        {
            get;
            internal set;
        }
        /// <summary>${mapping_TiledLayer_attribute_Row_D}</summary>
        public int Row
        {
            get;
            internal set;
        }
        /// <summary>${mapping_TiledLayer_attribute_Bounds_D}</summary>
        public Rectangle2D Bounds { get; internal set; }
    }
}
