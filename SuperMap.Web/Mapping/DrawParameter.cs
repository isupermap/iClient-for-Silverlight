﻿
namespace SuperMap.Web.Mapping
{
    /// <summary>${mapping_DrawParameter_Title}</summary>
    public class DrawParameter
    {
        internal DrawParameter()
        { }

        /// <summary>${mapping_DrawParameter_attribute_useTransitions_D}</summary>
        public bool UseTransitions { get; set; }

        //这个参数是Map的Resolutions传进去，比如TiledDynamicLayer需要用这个参数，其他的图层基本不需要。
        /// <summary>${mapping_Map_attribute_Resolutions_D}</summary>       
        public double[] Resoluitons { get; set; }

        //TODO:set改为internal?
    }
}
