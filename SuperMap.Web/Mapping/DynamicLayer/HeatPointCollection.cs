﻿using System.Collections.ObjectModel;

namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// 	<para>${mapping_HeatPointCollection_Title}</para>
    /// 	<para>${mapping_HeatPointCollection_Description}</para>
    /// </summary>
    public class HeatPointCollection : ObservableCollection<HeatPoint>
    {
        /// <summary>${mapping_HeatPointCollection_constructor_None_D}</summary>
        public HeatPointCollection()
        {
        }
    }
}
