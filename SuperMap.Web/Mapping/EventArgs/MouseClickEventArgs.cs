﻿using System;
using System.Windows;
using SuperMap.Web.Core;

namespace SuperMap.Web.Mapping
{
    /// <summary>
    /// 	<para>${mapping_MouseClickEventArgs_Title}</para>
    /// 	<para>${mapping_MouseClickEventArgs_Description}</para>
    /// </summary>
    public class MouseClickEventArgs : EventArgs
    {
        /// <summary>${mapping_MouseClickEventArgs_attribute_Handled_D}</summary>
        public bool Handled { get; set; }
        /// <summary>${mapping_MouseClickEventArgs_attribute_MapPoint_D}</summary>
        public Point2D MapPoint { get; set; }
        /// <summary>${mapping_MouseClickEventArgs_attribute_ScreenPoint_D}</summary>
        public Point ScreenPoint { get; set; }
    }
}
