﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.Utilities;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Threading;
using System.Windows.Threading;

namespace SuperMap.Web.Mapping
{
    //Map类太长，把涉及到键盘 鼠标 滚轮 等MapAction的部分移到这里来。
    public sealed partial class Map : Control, INotifyPropertyChanged
    {
        private bool isMouseLeftButtonDown;//单击用
        private Point curMousePos;//滚轮用
        private bool isKeyDown;//键盘用
        private MapAction curAction;
        private MapAction oldAction;
        /// <summary>${mapping_Map_event_actionChanged_D}</summary>
        public event EventHandler<MapActionArgs> MapActionChanged;
        /// <summary>${mapping_Map_event_MouseClick_D}</summary>
        public event EventHandler<MouseClickEventArgs> MouseClick;

        private DispatcherTimer _mouseWheelTimer;
        private bool _isWheeling=false;

        private void BuildMapAction()
        {
            rootElement.AddDoubleClick(Map_MouseDoubleClick);
            Pan panAction = new Pan(this);
            curAction = panAction;//默认pan操作
            oldAction = panAction;
            _mouseWheelTimer = new DispatcherTimer();
            _mouseWheelTimer.Interval = new TimeSpan(0, 0, 0, 0, 120);
            _mouseWheelTimer.Tick += (sender,e) =>
                {
                    _isWheeling = false;
                };
        }
        /// <summary>${mapping_Map_event_onKeyDown_D}</summary>
        /// <param name="e">${mapping_Map_event_onKeyDown_param_e}</param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (this.Action != null)
            {
                this.Action.OnKeyDown(e);
                if (e.Handled || !EnableKey)
                {
                    return;
                }
                if (!isKeyDown)
                {
                    isKeyDown = true;
                    if ((((e.Key == Key.Down) || (e.Key == Key.Up)) || ((e.Key == Key.Left) || (e.Key == Key.Right))) || ((e.Key == Key.Subtract) || (e.Key == Key.Add)))
                    {
                        double delta1 = PanFactor * ViewBounds.Width;
                        double delta2 = PanFactor * ViewBounds.Height;
                        switch (e.Key)
                        {
                            case Key.Left:
                                Pan(-delta1, 0);
                                break;
                            case Key.Right:
                                Pan(delta1, 0);
                                break;
                            case Key.Up:
                                Pan(0, delta2);
                                break;
                            case Key.Down:
                                Pan(0, -delta2);
                                break;
                            case Key.Add:
                                if (zoomEffectContainer != null && ZoomEffectEnabled == true)
                                {
                                    this.zoomEffectContainer.Margin = new Thickness(this.ActualWidth / 2 - 44, this.ActualHeight / 2 - 34, 0, 0);
                                    zoomEffect.beginZoomin();
                                }
                                ZoomIn();
                                break;
                            case Key.Subtract:
                                if (zoomEffectContainer != null && ZoomEffectEnabled == true)
                                {
                                    this.zoomEffectContainer.Margin = new Thickness(this.ActualWidth / 2 - 44, this.ActualHeight / 2 - 34, 0, 0);
                                    zoomEffect.beiginZoomout();
                                }
                                ZoomOut();
                                break;
                        }
                    }
                    if (e.PlatformKeyCode == 0xbb)
                    {
                        if (zoomEffectContainer != null && ZoomEffectEnabled == true)
                        {
                            this.zoomEffectContainer.Margin = new Thickness(this.ActualWidth / 2 - 44, this.ActualHeight / 2 - 34, 0, 0);
                            zoomEffect.beginZoomin();
                        }
                        ZoomIn();
                    }
                    else if (e.PlatformKeyCode == 0xbd)
                    {
                        if (zoomEffectContainer != null && ZoomEffectEnabled == true)
                        {
                            this.zoomEffectContainer.Margin = new Thickness(this.ActualWidth / 2 - 44, this.ActualHeight / 2 - 34, 0, 0);
                            zoomEffect.beiginZoomout();
                        }
                        ZoomOut();
                    }//这俩是大键盘的+-
                }
            }
        }
        /// <summary>${mapping_Map_event_onKeyUp_D}</summary>
        /// <param name="e">${mapping_Map_event_onKeyUp_param_e}</param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (this.Action != null)
            {
                this.Action.OnKeyUp(e);
            }
            isKeyDown = false;
        }
        /// <summary>${ui_action_MapAction_event_onMouseDown_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseDown_param_e}</param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            if (this.Action != null)
            {
                this.Action.OnMouseLeftButtonDown(e);
            }
            if (!e.Handled)
            {
                base.Dispatcher.BeginInvoke(delegate { isMouseLeftButtonDown = true; });
            }
        }
        /// <summary>${ui_action_MapAction_event_onMouseUp_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseUp_param_e}</param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            if (this.Action != null)
            {
                this.Action.OnMouseLeftButtonUp(e);
            }

            if (isMouseLeftButtonDown)
            {
                this.isMouseLeftButtonDown = false;
                if (this.MouseClick != null)
                {
                    Point position = e.GetPosition(this.layerCollectionContainer);
                    Point2D point2 = this.panLayerToMap(position);
                    if (!point2.IsEmpty)
                    {
                        MouseClickEventArgs args = new MouseClickEventArgs
                        {
                            ScreenPoint = position,
                            MapPoint = point2
                        };
                        this.MouseClick(this, args);
                        if (args.Handled)
                        {
                            e.Handled = true;
                        }
                    }
                }
            }
        }
        /// <summary>${ui_action_MapAction_event_onMouseEnter_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseEnter_param_e}</param>
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            if (this.Action != null)
            {
                this.Action.OnMouseEnter(e);
            }

        }
        /// <summary>${ui_action_MapAction_event_onMouseMove_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseMove_param_e}</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            curMousePos = e.GetPosition(this);
            this.isMouseLeftButtonDown = false;
            if (this.Action != null)
            {
#if !WINDOWS_PHONE
                this.Action.OnMouseMove(e);
#endif
            }
        }
        /// <summary>${ui_action_MapAction_event_onMouseLeave_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseLeave_param_e}</param>
        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            if (this.Action != null)
            {
                this.Action.OnMouseLeave(e);
            }
            this.isMouseLeftButtonDown = false;
        }

        /// <summary>${ui_action_MapAction_event_onMouseWheel_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseWheel_param_e}</param>
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);
            if (_mouseWheelTimer.IsEnabled)
            {
                _mouseWheelTimer.Stop();
            }
            _mouseWheelTimer.Start();
            if (_isWheeling)
            {
                return;
            }
            _isWheeling = true;
            if (zoomEffectContainer != null && ZoomEffectEnabled == true)
            {
                this.zoomEffectContainer.Margin = new Thickness(curMousePos.X - 44, curMousePos.Y - 34, 0, 0);
                if (e.Delta > 0)
                {
                    zoomEffect.beginZoomin();
                }
                else
                {
                    zoomEffect.beiginZoomout();
                }
            }
            if (this.Action != null)
            {
                this.Action.OnMouseWheel(e);
                if (!e.Handled && EnableWheel)
                {
                    e.Handled = true;
                    base.Focus();
                    Point2D pnt = ScreenToMap(curMousePos);
                    double newResolution = GetNextResolution(e.Delta > 0);
                    Rectangle2D newBounds = new Rectangle2D(pnt.X - curMousePos.X * newResolution, pnt.Y - (currentSize.Height - curMousePos.Y) * newResolution, pnt.X + (currentSize.Width - curMousePos.X) * newResolution, pnt.Y + curMousePos.Y * newResolution);
                    ZoomTo(newBounds);
                }
            }
        }
        /// <param name="e">${ui_action_MapAction_event_onMouseRightButtonDown_param_e}</param>
        /// <summary>${ui_action_MapAction_event_onMouseRightButtonDown_D}</summary>
        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseRightButtonDown(e);

            if (this.Action != null)
            {
                this.Action.OnMouseRightButtonDown(e);
            }
        }
        /// <param name="e">${ui_action_MapAction_event_onMouseRightButtonUp_param_e}</param>
        /// <summary>${ui_action_MapAction_event_onMouseRightButtonUp_D}</summary>
        protected override void OnMouseRightButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseRightButtonUp(e);

            if (this.Action != null)
            {
                this.Action.OnMouseLeftButtonUp(e);
            }
        }

        private void Map_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.Action != null)
            {
                this.Action.OnDblClick(e);
                if (!e.Handled && EnableDblClick)
                {
                    e.Handled = true;
                    if (zoomEffectContainer != null && ZoomEffectEnabled == true)
                    {
                        this.zoomEffectContainer.Margin = new Thickness(curMousePos.X - 44, curMousePos.Y - 34, 0, 0);
                        zoomEffect.beginZoomin();
                    }
                    ZoomToResolution(GetNextResolution(!(KeyboardHelper.ShiftIsDown())), ScreenToMap(e.GetPosition(this)));
                }
            }
        }

        private void OnMapActionChanged(MapActionArgs args)
        {
            if (this.MapActionChanged != null)
            {
                this.MapActionChanged(this, args);
            }
        }
        /// <summary>${mapping_Map_attribute_action_D}</summary>
        public MapAction Action
        {
            get { return this.curAction; }
            set
            {
                this.oldAction = this.curAction;
                if (this.oldAction != null)
                {
                    this.oldAction.Deactivate();
                }
                this.curAction = value;
                MapActionArgs args = new MapActionArgs(this.oldAction, this.curAction);
                this.OnMapActionChanged(args);
            }
        }


        /// <summary>${mapping_Map_attribute_enableDblClick_D}</summary>
        public bool EnableDblClick { get; set; }
        /// <summary>${mapping_Map_attribute_enableWheel_D}</summary>
        public bool EnableWheel { get; set; }
        /// <summary>${mapping_Map_attribute_enableKey_D}</summary>
        public bool EnableKey { get; set; }
    }
}
