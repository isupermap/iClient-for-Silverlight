﻿using System;
using System.ComponentModel;

namespace SuperMap.Web.Service
{
    /// <summary>
    /// 	<para>${mapping_ServiceRequest_RequestEventArgs_Tile}</para>
    /// 	<para>${mapping_ServiceRequest_RequestEventArgs_Description}</para>
    /// </summary>
    public sealed class RequestEventArgs : AsyncCompletedEventArgs
    {
        /// <overloads>${mapping_ServiceRequest_RequestEventArgs_constructor_overloads}</overloads>
        /// <summary>${mapping_ServiceRequest_RequestEventArgs_constructor_Object_Exception_D}</summary>
        /// <param name="state">${mapping_ServiceRequest_RequestEventArgs_constructor_Object_Exception_param_state}</param>
        /// <param name="exception">${mapping_ServiceRequest_RequestEventArgs_constructor_Object_Exception_param_exception}</param>
        public RequestEventArgs(object state, Exception exception)
            : base(exception, false, state)
        {
        }

        /// <summary>${mapping_ServiceRequest_RequestEventArgs_constructor_Object_String_D}</summary>
        /// <param name="state">${mapping_ServiceRequest_RequestEventArgs_constructor_Object_Exception_param_state}</param>
        /// <param name="result">${mapping_ServiceRequest_RequestEventArgs_constructor_Object_String_param_result}</param>
        public RequestEventArgs(object state, string result)
            : base(null, false, state)
        {
            Result = result;
        }

        /// <summary>${mapping_ServiceRequest_RequestEventArgs_attribute_Result_D}</summary>
        public string Result { get; private set; }
    }
}
