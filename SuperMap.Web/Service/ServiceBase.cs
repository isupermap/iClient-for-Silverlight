﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
#if SILVERLIGHT
using System.Windows.Browser;
#endif
using SuperMap.Web.Resources;
using SuperMap.Web.Core;

namespace SuperMap.Web.Service
{
    /// <summary>
    /// 	<para>${mapping_ServiceBase_Tile}</para>
    /// 	<para>${mapping_ServiceBase_Description}</para>
    /// </summary>
    public abstract class ServiceBase : INotifyPropertyChanged
    {
        private ServiceRequest request;
        /// <summary>${mapping_ServiceBase_event_Failed_D}</summary>
#if SILVERLIGHT
        [ScriptableMember]
#endif
        public event EventHandler<ServiceFailedEventArgs> Failed;

        /// <summary>${mapping_ServiceBase_event_PropertyChanged_D}</summary>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>${mapping_ServiceBase_constructor_None_D}</summary>
        protected ServiceBase()
            : this(null)
        {
        }

        /// <summary>${mapping_ServiceBase_constructor_string_D}</summary>
        protected ServiceBase(string url)
        {
            Url = url;
            request = new ServiceRequest();
            request.Completed += new EventHandler<RequestEventArgs>(request_Completed);
            request.Failed += new EventHandler<RequestEventArgs>(request_Failed);
        }

        /// <summary>${mapping_ServiceBase_method_CancelAsync_D}</summary>
        public void CancelAsync()
        {
            if (IsBusy)
            {
                request.CancelAsync();
            }
        }


        //这里改了很多东西，提供一个重载就好了！

        /// <param name="url">${mapping_ServiceBase_method_SubmitRequest_param_url_D}</param>
        /// <param name="parameters">${mapping_ServiceBase_method_SubmitRequest_param_parameters_D}</param>
        /// <param name="onCompleted">${mapping_ServiceBase_method_SubmitRequest_param_onCompleted_D}</param>
        /// <param name="state">${mapping_ServiceBase_method_SubmitRequest_param_state_D}</param>
        /// <param name="forcePost">${mapping_ServiceBase_method_SubmitRequest_param_forcePost_D}</param>
        /// <summary>${mapping_ServiceBase_method_SubmitRequest_D}</summary>
        /// <overloads>${mapping_ServiceBase_method_SubmitRequest_overloads_D}</overloads>
        protected void SubmitRequest(string url, Dictionary<string, string> parameters,
            EventHandler<RequestEventArgs> onCompleted, object state, bool forcePost)
        {
            SubmitRequest(url, parameters, onCompleted, state, forcePost, false);
        }

        /// <param name="url">${mapping_ServiceBase_method_SubmitRequest_param_url_D}</param>
        /// <param name="parameters">${mapping_ServiceBase_method_SubmitRequest_param_parameters_D}</param>
        /// <param name="onCompleted">${mapping_ServiceBase_method_SubmitRequest_param_onCompleted_D}</param>
        /// <param name="state">${mapping_ServiceBase_method_SubmitRequest_param_state_D}</param>
        /// <param name="forcePost">${mapping_ServiceBase_method_SubmitRequest_param_forcePost_D}</param>
        /// <param name="isEditable">${mapping_ServiceBase_method_SubmitRequest_param_isEditable_D}</param>
        /// <summary>${mapping_ServiceBase_method_SubmitRequest_D}</summary>
        /// <overloads>${mapping_ServiceBase_method_SubmitRequest_overloads_D}</overloads>
        protected void SubmitRequest(string url, Dictionary<string, string> parameters,
          EventHandler<RequestEventArgs> onCompleted, object state, bool forcePost, bool isEditable)
        {

            SubmitRequest(url, parameters, onCompleted, state, forcePost, isEditable, false);
        }
        /// <param name="url">${mapping_ServiceBase_method_SubmitRequest_param_url_D}</param>
        /// <param name="parameters">${mapping_ServiceBase_method_SubmitRequest_param_postBody_D}</param>
        /// <param name="onCompleted">${mapping_ServiceBase_method_SubmitRequest_param_onCompleted_D}</param>
        /// <param name="state">${mapping_ServiceBase_method_SubmitRequest_param_state_D}</param>
        /// <param name="forcePost">${mapping_ServiceBase_method_SubmitRequest_param_forcePost_D}</param>
        /// <param name="isEditable">${mapping_ServiceBase_method_SubmitRequest_param_isEditable_D}</param>
        /// <param name="isTempLayersSet">${mapping_ServiceBase_method_SubmitRequest_param_isTempLayersSet_D}</param>
        /// <summary>${mapping_ServiceBase_method_SubmitRequest_D}</summary>
        /// <overloads>${mapping_ServiceBase_method_SubmitRequest_overloads_D}</overloads>
        protected void SubmitRequest(string url, string postBody,
          EventHandler<RequestEventArgs> onCompleted, object state, bool isEditable, bool isTempLayersSet)
        {

            if (IsBusy)
            {
                //TODO:资源
                throw new NotSupportedException(ExceptionStrings.OperationIsBusy);
            }
            
            if (Credential.CREDENTIAL.GetUrlParameters() != null)
            {
                if (url.IndexOf("?") > 0)
                    url += "&" + Credential.CREDENTIAL.GetUrlParameters();
                else
                    url += "?" + Credential.CREDENTIAL.GetUrlParameters();
            }
            request.Url = url;
            request.ProxyUrl = ProxyURL;
            request.ForcePost = true;
            request.PostBody = postBody;
            request.IsEditable = isEditable;
            request.IsTempLayersSet = isTempLayersSet;
            request.BeginRequest(new object[] { state, onCompleted });
        }

        /// <param name="url">${mapping_ServiceBase_method_SubmitRequest_param_url_D}</param>
        /// <param name="parameters">${mapping_ServiceBase_method_SubmitRequest_param_parameters_D}</param>
        /// <param name="onCompleted">${mapping_ServiceBase_method_SubmitRequest_param_onCompleted_D}</param>
        /// <param name="state">${mapping_ServiceBase_method_SubmitRequest_param_state_D}</param>
        /// <param name="forcePost">${mapping_ServiceBase_method_SubmitRequest_param_forcePost_D}</param>
        /// <param name="isEditable">${mapping_ServiceBase_method_SubmitRequest_param_isEditable_D}</param>
        /// <param name="isTempLayersSet">${mapping_ServiceBase_method_SubmitRequest_param_isTempLayersSet_D}</param>
        /// <summary>${mapping_ServiceBase_method_SubmitRequest_D}</summary>
        /// <overloads>${mapping_ServiceBase_method_SubmitRequest_overloads_D}</overloads>
        protected void SubmitRequest(string url, Dictionary<string, string> parameters,
          EventHandler<RequestEventArgs> onCompleted, object state, bool forcePost, bool isEditable, bool isTempLayersSet)
        {
            if (IsBusy)
            {
                //TODO:资源
                throw new NotSupportedException(ExceptionStrings.OperationIsBusy);
            }
            if (parameters == null)
            {
                parameters = new Dictionary<string, string>();
            }

            if (!forcePost)
                AppendBaseQueryParameters(parameters);
            if (Credential.CREDENTIAL.GetUrlParameters() != null)
            {
                if (url.IndexOf("?") > 0)
                    url += "&" + Credential.CREDENTIAL.GetUrlParameters();
                else
                    url += "?" + Credential.CREDENTIAL.GetUrlParameters();
            }
            request.Url = url;
            request.Parameters = parameters;
            request.ProxyUrl = ProxyURL;
            request.ForcePost = forcePost;
            request.IsEditable = isEditable;
            request.IsTempLayersSet = isTempLayersSet;
            request.BeginRequest(new object[] { state, onCompleted });
        }

        private void AppendBaseQueryParameters(Dictionary<string, string> parameters)
        {
            if (DisableClientCaching)
            {
                parameters.Add("t", string.Format(CultureInfo.InvariantCulture, DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)));
            }

            if (!string.IsNullOrEmpty(Token))
            {
                parameters.Add("token", Token);
            }
        }

        /// <summary>${mapping_ServiceBase_method_OnPropertyChanged_D }</summary>
        /// <param name="name">${mapping_ServiceBase_method_OnPropertyChanged_param_name_D}</param>
        protected void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        internal void OnProcessFailed(ServiceFailedEventArgs args)
        {
            if (this.Failed != null)
            {
                this.Failed(this, args);
            }
        }

        private void request_Failed(object sender, RequestEventArgs e)
        {
            Exception inner = e.Error;
            //if (e.Error is SecurityException)
            //{
            //    //TODO:资源
            //    inner = new SecurityException(ExceptionStrings.ServiceAddressError, inner);
            //    OnProcessFailed(new ServiceFailedEventArgs(inner, e.UserState));
            //}
            //else
            //{
            //    OnProcessFailed(new ServiceFailedEventArgs(inner, e.UserState));
            //}

            //服务端返回NotFound
            ServiceException ex = new ServiceException(404, e.Error.Message);
            OnProcessFailed(new ServiceFailedEventArgs(ex, e.UserState));
        }

        private void request_Completed(object sender, RequestEventArgs e)
        {
            if (!CheckForServiceError(e))
            {
                object[] userState = (object[])e.UserState;
                EventHandler<RequestEventArgs> handler = userState[1] as EventHandler<RequestEventArgs>;
                if (handler != null)
                {
                    handler(this, new RequestEventArgs(userState[0], e.Result));
                }
            }
        }

        private bool CheckForServiceError(RequestEventArgs e)
        {
            ServiceException error = ServiceException.CreateFromJSON(e.Result);
            if (error != null)
            {
                OnProcessFailed(new ServiceFailedEventArgs(error, e.UserState));
                return true;
            }
            return false;
        }

        /// <summary>${mapping_ServiceBase_attribute_DisableClientCaching_D}</summary>
        public bool DisableClientCaching { get; set; }
        /// <summary>${mapping_ServiceBase_attribute_IsBusy_D}</summary>
        public bool IsBusy
        {
            get
            {
                return request.IsBusy;
            }
        }
        /// <summary>${mapping_ServiceBase_attribute_ProxyURL_D}</summary>
        public string ProxyURL { get; set; }
        /// <summary>${mapping_ServiceBase_attribute_Token_D}</summary>
        public string Token { get; set; }
        /// <summary>${mapping_ServiceBase_attribute_Url_D}</summary>
        public string Url { get; set; }
    }
}
