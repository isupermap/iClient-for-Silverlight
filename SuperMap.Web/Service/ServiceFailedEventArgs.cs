﻿
namespace SuperMap.Web.Service
{
    /// <summary>
    /// 	<para>${mapping_ServiceFailedEventArgs_Tile}</para>
    /// 	<para>${mapping_ServiceFailedEventArgs_Description}</para>
    /// </summary>
    public sealed class ServiceFailedEventArgs : ServiceEventArgs
    {
        internal ServiceFailedEventArgs(ServiceException error, object userToken)
            : base(userToken)
        {
            Error = error;
        }

        /// <summary>${mapping_ServiceFailedEventArgs_attribute_Error_D}</summary>
        public ServiceException Error { get; private set; }
    }
}
