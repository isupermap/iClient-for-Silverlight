﻿using System;
namespace SuperMap.Web.Service
{
    /// <summary>
    /// 	<para>${mapping_ServiceEventArgs_Tile}</para>
    /// 	<para>${mapping_ServiceEventArgs_Description}</para>
    /// </summary>
    public abstract class ServiceEventArgs : EventArgs
    {
        /// <summary>${mapping_ServiceEventArgs_constructor_None_D}</summary>
        protected ServiceEventArgs(object userToken)
        {
            UserState = userToken;
        }

        /// <summary>${mapping_ServiceEventArgs_attribute_UserState_D}</summary>
        public object UserState { get; internal set; }
    }
}
