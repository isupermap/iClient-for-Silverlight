﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using System.Windows;
#if SILVERLIGHT
using System.Windows.Browser;
#endif
using SuperMap.Web.Resources;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Service
{
    /// <summary>
    /// 	<para>${mapping_ServiceRequest_Tile}</para>
    /// 	<para>${mapping_ServiceRequest_Description}</para>
    /// </summary>
    public class ServiceRequest : DependencyObject
    {
        private WebClient client;
        /// <summary>${mapping_ServiceRequest_event_Completed_D}</summary>
        public event EventHandler<RequestEventArgs> Completed;
        /// <summary>${mapping_ServiceRequest_event_Failed_D}</summary>
        public event EventHandler<RequestEventArgs> Failed;

        internal ServiceRequest()
        {
        }

        /// <overloads>${mapping_ServiceRequest_method_BeginRequest_overloads}</overloads>
        /// <summary>${mapping_ServiceRequest_method_BeginRequest_overloads}</summary>
        public void BeginRequest()
        {
            BeginRequest(null);
        }

        /// <summary>${mapping_ServiceRequest_method_BeginRequest_Object_D}</summary>
        /// <param name="state">${mapping_ServiceRequest_method_BeginRequest_Object_param_state}</param>
        public void BeginRequest(object state)
        {
            if (IsBusy)
            {
                throw new NotSupportedException(ExceptionStrings.OperationIsBusy);
            }
#if SILVERLIGHT
            if (!Url.Contains("http://"))  //相对地址
            {
                Uri pageUrl = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                string ip = pageUrl.AbsoluteUri.Substring(0, pageUrl.AbsoluteUri.IndexOf(pageUrl.AbsolutePath));
                Url = ip + Url;
            }
#endif

            client = new WebClient();
            client.Encoding = Encoding.UTF8;
            client.UploadStringCompleted += new UploadStringCompletedEventHandler(wc_UploadStringCompleted);
            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(wc_DownloadStringCompleted);


            if (this.ForcePost)//iServerJava6R的POST
            {
                client.Headers["Content-Type"] = "application/x-www-form-urlencoded;charset=UTF-8";
                //由于REST的Edit请求体和其他REST的POST请求体不一样，所以这里需要单独判断一下！
                //此外，部分服务的请求体可能并不是键值对构成的Json，而是一个普通的字符串，因此，如果PostBody属性不为null，
                //则认为此时的请求体是PostBody，而不是键值对。
                string postBody = string.Empty;
                if (!string.IsNullOrEmpty(PostBody))
                {
                    postBody = PostBody;
                }
                else
                {
                    if (Parameters == null)
                    {
                        Parameters = new Dictionary<string, string>();
                    }
                    if (IsEditable)//编辑
                    {
                        postBody = EditGetString(Parameters);
                    }
                    else if (IsTempLayersSet)//制作专题图。
                    {
                        postBody = POSTGetStringFromDictionaryinSixthR(Parameters);
                    }

                    else//非编辑、非子图层控制
                    {
                        postBody = POSTGetStringFromDictionary(Parameters);
                    }
                }
                client.UploadStringAsync(ProxyEncodeUrl(this.Url), "POST", postBody, state);
            }
            else
            {
                if (Parameters == null)
                {
                    Parameters = new Dictionary<string, string>();
                }
                string queryString = GetStringFromDictionary(Parameters);
                string uriString = CreateUrl(this.Url, queryString, this.ProxyUrl);
                if (uriString.Length <= MagicNumber.GET_MAX_LEGTH) //iServer2、IS6、iServerJava6R的GET
                {
                    client.DownloadStringAsync(new Uri(uriString), state);
                }
                else //iServer2、IS6的POST
                {
                    client.Headers["Content-Type"] = "application/x-www-form-urlencoded";
                    client.UploadStringAsync(ProxyEncodeUrl(this.Url), "POST", queryString, state);
                }
            }
        }

        /// <summary>${mapping_ServiceRequest_method_CancelAsync_D}</summary>
        public void CancelAsync()
        {
            if (client != null)
            {
                client.CancelAsync();
                client = null;
            }
        }
        private Uri ProxyEncodeUrl(string url)
        {
            Uri uri = null;
            if (string.IsNullOrEmpty(this.ProxyUrl))
            {
                uri = new Uri(url);
            }
            else
            {
                uri = new Uri(ProxyEncodeUrl(url, this.ProxyUrl), UriKind.RelativeOrAbsolute);
            }
            if (!uri.IsAbsoluteUri)
            {
#if SILVERLIGHT
                uri = new Uri(Application.Current.Host.Source.AbsoluteUri.Substring(0, Application.Current.Host.Source.AbsoluteUri.LastIndexOf('/')) + '/' + uri.OriginalString);
#endif
            }
            return uri;
        }

        private static string ProxyEncodeUrl(string url, string proxyUrl)
        {
            if (string.IsNullOrEmpty(proxyUrl))
            {
                return url;
            }
            return string.Format(CultureInfo.InvariantCulture, "{0}?{1}", proxyUrl, HttpUtility.UrlEncode(url));
        }

        private static string CreateUrl(string url, string queryString, string proxyUrl)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(url);
            builder.Append(queryString);
            return ProxyEncodeUrl(builder.ToString(), proxyUrl);
        }

        private static string EditGetString(Dictionary<string, string> parameters)
        {
            string json = "[";
            List<string> list = new List<string>();
            foreach (string value in parameters.Values)
            {
                list.Add(JsonHelper.CheckString(value));
            }
            json += string.Join(",", list.ToArray());
            json += "]";
            return json;
        }
        //private static string SetLayerStatusGetString(Dictionary<string, string> parameters)
        //{
        //    string json = "";
        //    List<string> list = new List<string>();
        //    foreach (string value in parameters.Values)
        //    {
        //        list.Add(value);
        //    }
        //    json += string.Join(",", list.ToArray());
        //    return json;
        //}

        private static string POSTGetStringFromDictionary(Dictionary<string, string> parameters)
        {
            string json = "{";
            List<string> list = new List<string>();
            foreach (string key in parameters.Keys)
            {
                list.Add(string.Format(CultureInfo.InvariantCulture, "\"{0}\":{1}", new object[] { key, JsonHelper.CheckString(parameters[key]) }));
            }
            json += string.Join(",", list.ToArray());
            json += "}";
            return json;
        }

        private static string POSTGetStringFromDictionaryinSixthR(Dictionary<string, string> parameters)
        {
            string json = "[{";
            List<string> list = new List<string>();
            foreach (string key in parameters.Keys)
            {
                list.Add(string.Format(CultureInfo.InvariantCulture, "\"{0}\":{1}", new object[] { key, JsonHelper.CheckString(parameters[key]) }));
            }
            json += string.Join(",", list.ToArray());
            json += "}]";

            return json;
        }

        
        private static string GetStringFromDictionary(Dictionary<string, string> parameters)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string key in parameters.Keys)
            {
                builder.AppendFormat(CultureInfo.InvariantCulture, "{0}={1}&", new object[] { key, HttpUtility.UrlEncode(parameters[key]) });
            }
            return builder.ToString();
        }

        private void wc_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (sender == client)
            {
                client = null;
                if (e.Error != null)
                {
                    OnFailed(new RequestEventArgs(e.UserState, e.Error));
                }
                else
                {
                    OnCompleted(new RequestEventArgs(e.UserState, e.Result));
                }
            }
        }

        private void OnCompleted(RequestEventArgs args)
        {
            if (this.Completed != null)
            {
                this.Completed(this, args);
            }
        }

        private void OnFailed(RequestEventArgs args)
        {
            if (this.Failed != null)
            {
                this.Failed(this, args);
            }
        }

        private void wc_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (sender == client)
            {
                client = null;
                if (e.Error != null)
                {
                    this.OnFailed(new RequestEventArgs(e.UserState, e.Error));
                }
                else
                {
                    this.OnCompleted(new RequestEventArgs(e.UserState, e.Result));
                }
            }
        }

        /// <summary>${mapping_ServiceRequest_attribute_IsBusy_D}</summary>
        public bool IsBusy
        {
            get
            {
                return (client != null);
            }
        }

        /// <summary>${mapping_ServiceRequest_attribute_forcePost_D}</summary>
        public bool ForcePost { get; set; }

        /// <summary>${mapping_ServiceRequest_attribute_Url_D}</summary>
        public string Url { get; set; }
        /// <summary>${mapping_ServiceRequest_attribute_ProxyUrl_D}</summary>
        public string ProxyUrl { get; set; }
        /// <summary>${mapping_ServiceRequest_attribute_Parameters_D}</summary>
        public Dictionary<string, string> Parameters { get; set; }

        /// <summary>
        /// POST请求的请求体，还是开出来吧，有些服务的请求体并不是键值对构成的Json。
        /// </summary>
        internal string PostBody { get; set; }

        //没办法，编辑的参数构造和别的都不一样！
        internal bool IsEditable { get; set; }

        //还是没办法,iServer6制作专题图。
        internal bool IsTempLayersSet { get; set; }
    }
}
