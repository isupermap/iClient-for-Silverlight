﻿using System.Windows.Input;
using SuperMap.Web.Mapping;

namespace SuperMap.Web.Actions
{
    /// <summary>
    /// 	<para>${ui_action_MapAction_Title}。</para>
    /// 	<para>${ui_action_MapAction_Description}</para>
    /// </summary>
    public abstract class MapAction
    {
        /// <summary>${ui_action_MapAction_constructor_None_D}</summary>
        /// <overloads>${ui_action_MapAction_constructor_overloads}</overloads>
        protected MapAction()
        { }

        /// <summary>${ui_action_MapAction_constructor_Map_D}</summary>
        /// <param name="map">${ui_action_MapAction_constructor_Map_param_map}</param>
        /// <param name="name">${ui_action_MapAction_constructor_Map_param_name}</param>
        /// <param name="cursor">${ui_action_MapAction_constructor_Map_param_cursor}</param>
        protected MapAction(Map map, string name, Cursor cursor)
        {
            Name = name;
            Map = map;
            Map.Cursor = cursor;
            Map.Focus();
        }

        //TODO:推迟 如onMouseOut，onMouseOver，停顿一段时间出发的事件等等

        /// <summary>${ui_action_MapAction_event_onDblClick_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onDblClick_param_e}</param>
        public virtual void OnDblClick(MouseButtonEventArgs e) { }

        /// <summary>${ui_action_MapAction_event_onMouseDown_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseDown_param_e}</param>
        public virtual void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            //Map.Focus();//需要重新获取焦点，否则键盘事件不响应
        }

        /// <summary>${ui_action_MapAction_event_onMouseMove_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseMove_param_e}</param>
        public virtual void OnMouseMove(MouseEventArgs e) { }

        /// <summary>${ui_action_MapAction_event_onMouseEnter_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseEnter_param_e}</param>
        public virtual void OnMouseEnter(MouseEventArgs e) { }

        /// <summary>${ui_action_MapAction_event_onMouseLeave_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseLeave_param_e}</param>
        public virtual void OnMouseLeave(MouseEventArgs e) { }

        /// <summary>${ui_action_MapAction_event_onMouseUp_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseUp_param_e}</param>
        public virtual void OnMouseLeftButtonUp(MouseButtonEventArgs e) { }

        /// <param name="e">${ui_action_MapAction_event_onMouseRightButtonDown_param_e}</param>
        /// <summary>${ui_action_MapAction_event_onMouseRightButtonDown_D}</summary>
        public virtual void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            //Map.Focus();
        }

        /// <param name="e">${ui_action_MapAction_event_onMouseRightButtonUp_param_e}</param>
        /// <summary>${ui_action_MapAction_event_onMouseRightButtonUp_D}</summary>
        public virtual void OnMouseRightButtonUp(MouseButtonEventArgs e) { }

        /// <summary>${ui_action_MapAction_event_onMouseWheel_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseWheel_param_e}</param>
        public virtual void OnMouseWheel(object e) { }

        /// <summary>${ui_action_MapAction_event_onKeyDown_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onKeyDown_param_e}</param>
        public virtual void OnKeyDown(KeyEventArgs e) { }

        /// <summary>${ui_action_MapAction_event_onKeyUp_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onKeyUp_param_e}</param>
        public virtual void OnKeyUp(KeyEventArgs e) { }

        //重要,每个子类必需覆盖重写
        /// <summary>${ui_action_MapAction_method_deactivate_D}</summary>
        public abstract void Deactivate();

        /// <summary>${ui_action_MapAction_attribute_name_D}</summary>
        public string Name { get; set; }
        /// <summary>${ui_action_MapAction_attribute_map_D}</summary>
        public Map Map { get; protected set; }
    }
}
