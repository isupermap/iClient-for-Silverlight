﻿using System;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Actions
{
    /// <summary>
    /// 	<para>${ui_action_DrawPoint_Title}。</para>
    /// 	<para>${ui_action_DrawPoint_Description}</para>
    /// </summary>
    public class DrawPoint : MapAction
    {
        private Point2D center = Point2D.Empty;
        private Ellipse ellipse;
        private bool isActivated;

        /// <summary>${ui_action_DrawPoint_event_drawCompleted_D}</summary>
        public event EventHandler<DrawEventArgs> DrawCompleted;

        /// <summary>${ui_action_DrawPoint_constructor_Map_D}</summary>
        /// <overloads>${ui_action_DrawPoint_constructor_overloads}</overloads>
        /// <param name="map">${ui_action_DrawPoint_constructor_Map_param_map}</param>
        public DrawPoint(Map map)
            : this(map , Cursors.Stylus)
        {
        }
        /// <summary>${ui_action_DrawPoint_constructor_Map_D}</summary>
        /// <example>
        /// 	<code lang="CS">
        /// DrawPoint draw = new DrawPoint(MyMap,Cursors.Stylus)
        /// </code>
        /// </example>
        /// <param name="map">${ui_action_DrawPoint_constructor_Map_param_map}</param>
        /// <param name="cursor">${ui_action_MapAction_constructor_Map_param_cursor}</param>
        public DrawPoint(Map map , Cursor cursor)
            : base(map , "DrawPoint" , cursor)
        {
            DrawLayer = new ElementsLayer();

            if (map.Theme == null)
            {
                Opacity = MagicNumber.ACTION_STYLE_DEFAULT_OPACITY;
                Size = MagicNumber.ACTION_STYLE_DEFAULT_SIZE;
                Color = new SolidColorBrush(Colors.Red);
            }
            else
            {
                this.Opacity = map.Theme.Opacity;
                this.Size = map.Theme.Size;
                this.Color = map.Theme.Color;
            }
        }
        private void Activate(Point2D point)
        {
            if (Map == null || Map.Layers == null)
            {
                return;
            }
            ellipse = new Ellipse();
            ellipse.Opacity = Opacity;
            ellipse.Height = Size;
            ellipse.Width = Size;
            ellipse.Fill = Color;

            DrawLayer.AddChild(ellipse , point);
            Map.Layers.Add(DrawLayer);
            isActivated = true;
        }

        /// <summary>${ui_action_MapAction_method_deactivate_D}</summary>
        public override void Deactivate( )
        {
            isActivated = false;
            ellipse = null;
            if (DrawLayer != null)
            {
                DrawLayer.Children.Clear();
            }
            if (Map != null && Map.Layers != null)
            {
                Map.Layers.Remove(DrawLayer);
            }
        }
        /// <summary>${ui_action_DrawPoint_event_OnKeyDown_D}</summary>
        /// <param name="e">${ui_action_DrawPoint_event_OnKeyDown_param_e}</param>
        public override void OnKeyDown(KeyEventArgs e)
        {
            if (isActivated && e.Key == Key.Escape)
            {
                Deactivate();
            }
            base.OnKeyDown(e);
        }

        /// <summary>${ui_action_MapAction_event_onMouseDown_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseDown_param_e}</param>
        public override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            center = Map.ScreenToMap(e.GetPosition(Map));

            if (!isActivated)
            {
                Activate(center);
            }
            e.Handled = true;
            base.OnMouseLeftButtonDown(e);
        }

        /// <summary>${ui_action_MapAction_event_onMouseMove_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseMove_param_e}</param>
        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
        }

        /// <summary>${ui_action_MapAction_event_onMouseUp_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseUp_param_e}</param>
        public override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (ellipse != null)
            {
                DrawEventArgs args2 = new DrawEventArgs
                {
                    DrawName = Name ,
                    Element = new Pushpin { Location = center } ,
                    Geometry = new GeoPoint(center.X , center.Y)
                };

                Deactivate();
                OnDrawComplete(args2);
            }
            base.OnMouseLeftButtonUp(e);
        }

        /// <summary>${ui_action_MapAction_event_onDblClick_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onDblClick_param_e}</param>
        public override void OnDblClick(MouseButtonEventArgs e)
        {
            e.Handled = true;
            base.OnDblClick(e);
        }

        private void OnDrawComplete(DrawEventArgs args)
        {
            if (DrawCompleted != null)
            {
                DrawCompleted(this , args);
            }
        }

        internal ElementsLayer DrawLayer { get; private set; }

        /// <summary>${ui_action_DrawPoint_attribute_size_D}</summary>
        public double Size { get; set; }
        /// <summary>${ui_action_DrawPoint_attribute_color_D}</summary>
        /// <example>
        /// 	<code lang="CS">
        /// DrawPoint draw = new DrawPoint(MyMapControl);
        /// draw.Color = new SolidColorBrush(Colors.Red);
        /// </code>
        /// </example>
        public Brush Color { get; set; }
        /// <summary>${ui_action_DrawPoint_attribute_Opacity_D}</summary>
        public double Opacity { get; set; }

    }
}
