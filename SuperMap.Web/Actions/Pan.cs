﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using SuperMap.Web.Mapping;
using SuperMap.Web.Utilities;

namespace SuperMap.Web.Actions
{
    /// <summary>
    /// 	<para>${ui_action_Pan_Title}。</para>
    /// 	<para>${ui_action_Pan_Description}</para>
    /// </summary>
    public class Pan : MapAction
    {
        private bool isMouseCaptured;
        private double oldMouseX;
        private double oldMouseY;
        /// <summary>${ui_action_Pan_constructor_Map_D}</summary>
        /// <overloads>${ui_action_Pan_constructor_overloads}</overloads>
        /// <param name="map">${ui_action_Pan_constructor_Map_param_map}</param>
        public Pan(Map map)
            : this(map, Cursors.Arrow)
        {
        }

        /// <summary>${ui_action_Pan_constructor_Map_D}</summary>
        /// <example>
        /// 	<code lang="CS">
        /// Pan panTo = new Pan(MyMap,Cursors.Hand)
        /// </code>
        /// </example>
        /// <param name="map">${ui_action_Pan_constructor_Map_param_map}</param>
        /// <param name="cursor">${ui_action_MapAction_constructor_Map_param_cursor}</param>
        public Pan(Map map, Cursor cursor)
            : base(map, "Pan", cursor)
        {
        }

        /// <summary>${ui_action_MapAction_event_onMouseDown_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseDown_param_e}</param>
        public override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            oldMouseX = e.GetPosition(Map).X;
            oldMouseY = e.GetPosition(Map).Y;
            isMouseCaptured = true;
            //e.Handled = true;
            base.OnMouseLeftButtonDown(e);
        }

        /// <summary>${ui_action_MapAction_event_onMouseMove_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseMove_param_e}</param>
        public override void OnMouseMove(MouseEventArgs e)
        {
            if (isMouseCaptured)
            {
                double newMouseX = e.GetPosition(Map).X;
                double newMouseY = e.GetPosition(Map).Y;
                //double deltaX;
                //double deltaY;
                //有旋转的时候，不适合了。操作方法，假想MapControl在旋转，进行坐标旋转变换。
                //if (Map.Angle != 0.0)
                //{
                //    double radian = Map.Angle / 180.0 * Math.PI;//变成弧度
                //    Point transOrigin = new Point(Map.ActualWidth / 2, Map.ActualHeight / 2);
                //    Point transOldPoint = MathUtility.TransformPoint(new Point(oldMouseX, oldMouseY), transOrigin, radian);
                //    Point transNewPoint = MathUtility.TransformPoint(new Point(newMouseX, newMouseY), transOrigin, radian);
                //    deltaX = transNewPoint.X - transOldPoint.X;
                //    deltaY = transNewPoint.Y - transOldPoint.Y;
                //}
                //else
                //{
                //    deltaX = newMouseX - oldMouseX;
                //    deltaY = newMouseY - oldMouseY;
                //}//不旋转的时候少计算一点，不要当然也可以
                //Map.PanByPixel(-deltaX, -deltaY);//OK

                GeneralTransform transform = Map.RootElement.TransformToVisual(Map.TransformCanvas);
                Point point = transform.Transform(new Point(newMouseX, newMouseY));
                Point point2 = transform.Transform(new Point(oldMouseX, oldMouseY));
                if (Map.Popup.IsOpen)
                {
                    Map.Popup.IsOpen = false;
                }
                Map.PanHelper.DeltaPan((int)(point.X - point2.X), (int)(point.Y - point2.Y), Map.PanDuration);//OK
                if (Map.Layers != null)
                {
                    foreach (Layer layer in Map.Layers)
                    {
                        layer.Container.PrepareAnimation();
                    }
                }
                oldMouseY = e.GetPosition(Map).Y;
                oldMouseX = e.GetPosition(Map).X;
            }
            base.OnMouseMove(e);
        }

        /// <summary>${ui_action_MapAction_event_onMouseUp_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseUp_param_e}</param>
        public override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            isMouseCaptured = false;
            oldMouseY = MagicNumber.INVALID_MOUSE_COORD;
            oldMouseX = MagicNumber.INVALID_MOUSE_COORD;

            if (Map.PanDuration.Ticks == MagicNumber.PANDURATION_TICKS_ZERO)
            {
                Map.PanCompleted();
            }

            //e.Handled = true;
            base.OnMouseLeftButtonUp(e);

        }
        /// <summary>${ui_action_MapAction_event_onMouseLeave_D}</summary>
        /// <param name="e">${ui_action_MapAction_event_onMouseLeave_param_e}</param>
        public override void OnMouseLeave(MouseEventArgs e)
        {

            isMouseCaptured = false;
            oldMouseY = MagicNumber.INVALID_MOUSE_COORD;
            oldMouseX = MagicNumber.INVALID_MOUSE_COORD;

            if (Map.PanDuration.Ticks == MagicNumber.PANDURATION_TICKS_ZERO)
            {
                Map.PanCompleted();
            }

            base.OnMouseLeave(e);
        }
        /// <summary>${ui_action_MapAction_method_deactivate_D}</summary>
        public override void Deactivate()
        {
        }
    }
}