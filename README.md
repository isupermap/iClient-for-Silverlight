**一、  简介**


SuperMap iClient 7C for Silverlight 是一套基于 Microsoft Silverlight 5.0
技术的 Web 地理信息系统开发包，利用该套开发包并结合 Microsoft .NET 开发框架，可快速方便地构建跨平台跨浏览器的Service
 GIS富客户端应用程序。SuperMap iClient 7C for Silverlight 是独立的客户端产品，但又与服务器紧密相关，是将
 GIS 服务器功能丰富呈现给用户的重要工具。作为一个跨浏览器、跨平台的客户端开发平台产品，不仅可以在客户端流畅的显示地图，还可以迅速地使用
SuperMap GIS 服务器或第三方服务器提供的地图与服务，从而构建界面丰富、高度互动、体验优越的地图应用。


此处提供的是产品的源码包，用户可以根据自己的需求进行相应的需求扩展。


产品包的获取：
http://support.supermap.com.cn/ProductCenter/DownloadCenter/ProductPlatform.aspx

示范程序地址：
http://support.supermap.com.cn:8090/iserver/iClient/forSilverlight/samples/iServerJava6R_SampleCode/iServerJava6R_SampleCode.Web/default.html


**二、  产品特点**


1、    聚合服务


服务聚合是将不同类型、不同来源的服务通过标准化流程在 GIS 客户端发布。 SuperMap iClient 7C for
Silverlight 可以聚合多种服务，不仅支持 SuperMap iServer 7C、SuperMap IS .NET
发布的服务，还支持第三方服务。这些第三方服务按照 OGC 标准（WMS、WFS、KML、GeoRSS、WebService
等）发布自己的服务，用户利用 SuperMap iClient 7C for Silverlight
在客户端就能实现对标准第三方服务的访问，可以方便的将第三方服务提供的信息在客户端呈现，实现不同服务发布的地图叠加显示、聚合查询和聚合分析等功能。
如果地图以图片的形式叠加，可设置其透明度；如果地图以图片加数据的形式叠加，则可设置显示数据的风格。服务聚合能同时聚合同质和异质的服务，对服务来源
没有限制，从而达到整合全球资源的目的。


2、    独立分发


SuperMap iClient 7C for Silverlight 作为 Service GIS 产品体系的重要组成部分，在技术上与
SuperMap GIS 服务器紧密集成，支持用户调用服务器端完整专业的 GIS
功能；在产品形态上，它可被看作服务器产品的一部分，使得服务器提供的数据和服务能够在客户端以丰富的形式展现给用户。但它又是 与 SuperMap
GIS 服务器剥离的、独立分发和更新。SuperMap iClient 7C for Silverlight 支持 SuperMap
多个服务器产品的服务—— SuperMap iSever java 2008、SuperMap iServer 7C 和 SuperMap
IS.NET 6 服务，以及 OGC 等标准服务，也可通过扩展支持第三方服务器提供的服务。由于支持多种类型服务，SuperMap iClient
 7C 开发包将支持 SuperMap GIS 各类型服务库文件进行独立分发，用户根据使用的不同服务，选用相应的库即可。


3、    二次开发的简易性


SuperMap iClient 7C 基于标准的 Web
技术，采用简捷、易用的面向对象编程模型，只需编写简练的代码便能呈现地图、加载控件。同时 SuperMap iClient
为用户提供了一个功能完整的 SampleCode，用户不仅能直接体验到 SampleCode
实现的功能，还能查看、复制源码，使得用户能够快速的学习和掌握各个功能的实现技术。


4、    功能的可扩展性


SuperMap iClient 7C 除内置丰富功能外，还提供了方便扩展的诸多接口。比如：既支持 REST 风格的接口，也支持 RPC
风格的接口，可通过对第三方服务的适配来支持第三方服务器；通过继承
TiledCachedLayer，可发布自己的已有数据；还可制作个性化的地图控件；自定义各种点线面要素的风格，适用不同的行业需求等等。


此外 SuperMap iClient 7C for Silverlight 具有以下特色 GIS 功能：


5、    Silverlight 元素图层的创新


在地图数据类型划分中，常有栅格和矢量之分，对此该产品提供了图片图层（ImageLayer）和要素图层（FeaturesLayer）分别予以
支持。除此以外，还创新性的提出了 Silverlight
元素图层（ElementsLayer）的概念，该图层可以独立的加载到地图控件中，既可以添加图片图层所支持的图片，也可添加和要素图层所对应的点线面
要素
Pushpin、PolylineBase、PolygonBase，实现了在同一个图层上对栅格和矢量数据的同时支持。此外，该图层还支持添加视频元
素，完成对多媒体 GIS 的良好支持。还可以添加 Silverlight 常见 UI
元素，如按钮，列表框等，制作出个性，满足特殊需求的应用程序。所添加的这些元素只有在当前可视范围内时才会被显示。


6、    海量点数据聚合显示


如果需要加载海量点数据时，SuperMap iClient 7C for Silverlight
提供了海量点数据的聚合机制，能够实现快速、流畅的加载和显示海量点数据。随着地图的缩放，人们对点数据的关注会有所改变，当缩小地图时，离散点根据一定
算法聚合为一个更大的点，人们更关注从整体上点的分布情况；当地图放大时，聚合点分散为一个一个离散点，关注点转移到对单个区域点分布的情况。


7、    服务端专题图和客户端专题图并行


专题图是 GIS 应用程序进行地理信息最重要的表达方式之一。对于 Service GIS
而言，既可以由服务端计算，生成相应专题图图片，比如统计专题图，单值专题图，分段专题图等，传回客户端用图片图层的方式展示；也可以根据业务属性数据，
在客户端进行相应计算，根据几何数据的形状和属性数据的处理，通过要素图层或任意图层赋予不同的绘制风格并在客户端进行专题图的展示。SuperMap
iClient 7C for Silverlight
内置了单值和分段两种专题图支持，还可以结合第三方控件以柱状图、饼图等方式进行渲染显示。服务端专题图适用于大数量计算，客户端无法有效承载的场景；客
户端专题图灵活方便，支持鼠标键盘等事件操作。各有所长，可按实际需求取舍。


**三、  接口变更说明**


SuperMap.Web.iServerJava6R 程序集


(1) 增加DatasetThiessenAnalystParameters类


(2) 增加GeometryThiessenAnalystParameters类


(3) 增加ThiessenAnalystEventArgs类


(4) 增加ThiessenAnalystParameters类


(5) 增加ThiessenAnalystResult类


(6) 增加ThiessenAnalystService类


