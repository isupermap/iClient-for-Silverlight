﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using System.Collections.ObjectModel;

namespace iClient60ForSilverlight
{
    public partial class MainPage : UserControl
    {
        private FeaturesLayer featuresLayer;
        private ElementsLayer elementsLayer;
        Feature feature;
        public MainPage()
        {
            InitializeComponent();
            featuresLayer = MyMap.Layers["myFeaturesLayer"] as FeaturesLayer;
            elementsLayer = MyMap.Layers["myElementsLayer"] as ElementsLayer;

            feature = new Feature();
            feature.Geometry = new GeoRegion
            {
                Parts = new ObservableCollection<Point2DCollection>
                {
                    new Point2DCollection()
                        {
                            new Point2D(0,0),
                            new Point2D(10000000,0),
                            new Point2D(10000000,10000000),
                            new Point2D(0,10000000),
                            new Point2D(0,0)
                        },
                    new Point2DCollection()
                        {
                            new Point2D(-10000000,0),
                            new Point2D(0,-10000000),
                            new Point2D(-10000000,-10000000),
                            new Point2D(-10000000,0)
                        }
                 }
            };

            Feature fmline = new Feature();
            fmline.Geometry = new GeoLine
            {
                Parts = new ObservableCollection<Point2DCollection>
                {
                    new Point2DCollection()
                        {
                            new Point2D(-5000000,5000000),
                            new Point2D(-10000000,10000000),

                        },
                    new Point2DCollection()
                        {
                            new Point2D(5000000,-5000000),
                            new Point2D(10000000,-10000000),
                        }
                }
            };

            Feature fpoint = new Feature() { Geometry = new GeoPoint(0, 0) };
            featuresLayer.AddFeature(fpoint);
            featuresLayer.AddFeature(fmline);
            featuresLayer.Features.Add(feature);
            elementsLayer.Children.Clear();
            Pushpin pushpin = new Pushpin { Content = "test", Location = new Point2D(0, 0) };
            //elementsLayer.AddChild(pushpin);

            MyMap.MouseLeftButtonDown += new MouseButtonEventHandler(MyMap_MouseLeftButtonDown);
            MyMap.MouseLeftButtonUp += new MouseButtonEventHandler(MyMap_MouseLeftButtonUp);
            MyMap.MouseRightButtonDown += new MouseButtonEventHandler(MyMap_MouseRightButtonDown);
            MyMap.MouseRightButtonUp += new MouseButtonEventHandler(MyMap_MouseRightButtonUp);

            featuresLayer.MouseLeftButtonDown += new EventHandler<FeatureMouseButtonEventArgs>(featuresLayer_MouseLeftButtonDown);
            featuresLayer.MouseLeftButtonUp += new EventHandler<FeatureMouseButtonEventArgs>(featuresLayer_MouseLeftButtonUp);
            featuresLayer.MouseRightButtonDown += new EventHandler<FeatureMouseButtonEventArgs>(featuresLayer_MouseRightButtonDown);
            featuresLayer.MouseRightButtonUp += new EventHandler<FeatureMouseButtonEventArgs>(featuresLayer_MouseRightButtonUp);
        }

        void featuresLayer_MouseRightButtonUp(object sender, FeatureMouseButtonEventArgs e)
        {

        }

        void featuresLayer_MouseRightButtonDown(object sender, FeatureMouseButtonEventArgs e)
        {

        }
        //int flag4 = 1;
        void featuresLayer_MouseLeftButtonUp(object sender, FeatureMouseButtonEventArgs e)
        {
            //mytest4.Text = "featuresLayer_MouseLeftButtonUp" + flag4++.ToString();
        }
        int flag3 = 1;
        void featuresLayer_MouseLeftButtonDown(object sender, FeatureMouseButtonEventArgs e)
        {
            mytest3.Text = "featuresLayer_MouseLeftButtonDown" + flag3++.ToString();
        }

        void MyMap_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("MyMap_MouseRightButtonUp");
        }
        void MyMap_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            // MessageBox.Show("MyMap_MouseRightButtonDown");
        }
        int flag1 = 1;
        void MyMap_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            mytest1.Text = "MouseLeftButtonUp" + flag1++.ToString();

        }
        int flag2 = 1;
        void MyMap_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            mytest2.Text = "MouseLeftButtonDown" + flag2++.ToString();
        }
        private void BtnZoomOut_Click(object sender, RoutedEventArgs e)
        {
            MyMap.Zoom(2.0);
        }

        private void BtnZoomIn_Click(object sender, RoutedEventArgs e)
        {
            MyMap.Zoom(0.5);
        }

        private void BtnPan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap);
            MyMap.Action = pan;
        }

        private void BtnLine_Click(object sender, RoutedEventArgs e)
        {
            DrawLine drawLine = new DrawLine(MyMap, Cursors.Wait)
            {
                StrokeThickness = 5,
                Stroke = new SolidColorBrush(Colors.Green),
                StrokeEndLineCap = PenLineCap.Round,
                StrokeStartLineCap = PenLineCap.Triangle,
                StrokeLineJoin = PenLineJoin.Bevel,
            };

            MyMap.Action = drawLine;
            drawLine.DrawCompleted += new EventHandler<DrawEventArgs>(draw_DrawComplete);
        }

        private void draw_DrawComplete(object sender, DrawEventArgs e)
        {
            Feature feature = new Feature() { Geometry = e.Geometry as GeoLine };
            this.featuresLayer.Features.Add(feature);//ok
        }

        private void BtnRegion_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon drawRegion = new DrawPolygon(MyMap)
            {
                StrokeThickness = 5,
                Stroke = new SolidColorBrush(Colors.Green),
                Fill = new SolidColorBrush(Colors.Orange)
            };
            drawRegion.DrawCompleted += new EventHandler<DrawEventArgs>(drawRegion_DrawComplete);
            MyMap.Action = drawRegion;
        }

        void drawRegion_DrawComplete(object sender, DrawEventArgs e)
        {
            GeoRegion geoRegion = e.Geometry as GeoRegion;
            Feature feature = new Feature() { Geometry = e.Geometry as GeoRegion };
            this.featuresLayer.Features.Add(feature);//ok
            for (int i = 0; i < geoRegion.Parts[0].Count; i++)
            {
                mytest4.Text += "test" + geoRegion.Parts[0][i].ToString();
            }
        }

        private void BtnCircle_Click(object sender, RoutedEventArgs e)
        {
            DrawCircle drawcircle = new DrawCircle(MyMap)
            {
                StrokeThickness = 5,
                Stroke = new SolidColorBrush(Colors.Green),
                Fill = new SolidColorBrush(Colors.Orange)
            };

            MyMap.Action = drawcircle;
            drawcircle.DrawCompleted += new EventHandler<DrawEventArgs>(drawcircle_DrawComplete);
        }

        void drawcircle_DrawComplete(object sender, DrawEventArgs e)
        {

        }

        private void BtnFreeHand_Click(object sender, RoutedEventArgs e)
        {
            DrawFreeLine drawFreeHand = new DrawFreeLine(MyMap)
            {
                StrokeThickness = 5,
                Stroke = new SolidColorBrush(Colors.Green),
                StrokeEndLineCap = PenLineCap.Round,
                StrokeStartLineCap = PenLineCap.Triangle,
                StrokeLineJoin = PenLineJoin.Bevel,
            };

            MyMap.Action = drawFreeHand;
            drawFreeHand.DrawCompleted += new EventHandler<DrawEventArgs>(drawFreeHand_DrawComplete);
        }

        void drawFreeHand_DrawComplete(object sender, DrawEventArgs e)
        {
            //this.arbitraryLayer.Children.Add(e.Element as PolylineBase);
            PredefinedLineStyle lineStyle = new PredefinedLineStyle();
            lineStyle.Stroke = new SolidColorBrush(Colors.Red);
            lineStyle.StrokeThickness = 5;
            lineStyle.Symbol = SuperMap.Web.Core.PredefinedLineStyle.LineSymbol.Dash;

            Feature feature = new Feature() { Geometry = e.Geometry as GeoLine, Style = lineStyle };
            this.featuresLayer.Features.Add(feature);//ok
        }

        private void BtnRectangle_Click(object sender, RoutedEventArgs e)
        {
            DrawRectangle drawRectangle = new DrawRectangle(MyMap)
            {
                StrokeThickness = 5,
                Stroke = new SolidColorBrush(Colors.Green),
                Fill = new SolidColorBrush(Colors.Orange)
            };
            drawRectangle.DrawCompleted += new EventHandler<DrawEventArgs>(drawRectangle_DrawComplete);
            MyMap.Action = drawRectangle;
        }

        void drawRectangle_DrawComplete(object sender, DrawEventArgs e)
        {
            // this.arbitraryLayer.Children.Add(e.Element as Rectangle);//ok
            Feature feature = new Feature() { Geometry = e.Geometry as GeoRegion };
            this.featuresLayer.Features.Add(feature);//ok

        }
        private void BtnFreePolygon_Click(object sender, RoutedEventArgs e)
        {
            DrawFreePolygon fp = new DrawFreePolygon(MyMap);
            fp.DrawCompleted += new EventHandler<DrawEventArgs>(fp_DrawCompleted);

            MyMap.Action = fp;
        }

        void fp_DrawCompleted(object sender, DrawEventArgs e)
        {
            Feature feature = new Feature() { Geometry = e.Geometry as GeoRegion };
            this.featuresLayer.Features.Add(feature);//ok
        }

        private void BtnPoint_Click(object sender, RoutedEventArgs e)
        {

            DrawPoint point = new DrawPoint(this.MyMap);
            point.DrawCompleted += new EventHandler<DrawEventArgs>(point_DrawCompleted);

            MyMap.Action = point;
        }

        void point_DrawCompleted(object sender, DrawEventArgs e)
        {
            Feature feature = new Feature() { Geometry = e.Geometry as GeoPoint };
            this.featuresLayer.Features.Add(feature);//ok
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Edit edit = new Edit(MyMap, featuresLayer);
            edit.GeometryEdit += new EventHandler<Edit.GeometryEditEventArgs>(edit_GeometryEdit);
            MyMap.Action = edit;
        }

        void edit_GeometryEdit(object sender, Edit.GeometryEditEventArgs e)
        {

        }

        private void ChangedFeatureLocation_Click(object sender, RoutedEventArgs e)
        {

        }

        private void test_Click(object sender, RoutedEventArgs e)
        {
            //feature.Style = new PredefinedLineStyle { Symbol = PredefinedLineStyle.LineSymbol.Dot };
            //(feature.Style as PredefinedLineStyle).Stroke = new SolidColorBrush(Colors.Green);
            elementsLayer.Children.Clear();
            Pushpin pushpin = new Pushpin { Content = "test", Location = new Point2D(0, 0) };
            elementsLayer.AddChild(pushpin);
        }
    }
}
