﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Mapping;
using System.Windows.Media.Imaging;

namespace TestSLControl
{
    public partial class LegendControlTest : UserControl
    {
        DynamicRESTLayer layer1;
        DynamicRESTLayer layer3;
        private static string[] LayerIDs1 = new string[1] { "layer1" };
        private static string[] LayerIDs2 = new string[1] { "layer3" };

        public LegendControlTest()
        {
            InitializeComponent();
            layer1 = this.MyMap1.Layers["layer1"] as DynamicRESTLayer;
            layer3 = this.MyMap2.Layers["layer3"] as DynamicRESTLayer;
            legend4.PropertyChanged += (s, e) =>
                {
                    if (e.PropertyName == "LayerItems" && legend4.LayerItems.Count > 0)
                    {
                        legend4.LayerItems[0].ImageSource = new BitmapImage
                        {
                            UriSource = new Uri("http://www.hao123.com/images/logo_193.png", UriKind.RelativeOrAbsolute)
                        };
                    }
                };
        }

        private void ChangMapAndLayerIDs_Click(object sender, RoutedEventArgs e)
        {
            legend.Map = this.MyMap2;
            legend.LayerIDs = new string[] { "layer3" };
        }
        private void BindToMap1AndLayer3_Click(object sender, RoutedEventArgs e)
        {
            legend.Map = MyMap1;
            legend.LayerIDs = new string[] { "layer3" };
        }

        private void BindToMap2AndLayer12_Click(object sender, RoutedEventArgs e)
        {
            legend.Map = this.MyMap2;
            legend.LayerIDs = new string[] { "layer1", "layer2" };
        }

        private void BindToMap1AndLayer12_Click(object sender, RoutedEventArgs e)
        {
            this.legend.Map = this.MyMap1;
            legend.LayerIDs = new string[] { "layer1", "layer2" };
        }

        private void ChangeLayer1URL_Click(object sender, RoutedEventArgs e)
        {
            layer1.Url = "http://192.168.170.148:8090/iserver/services/map-jingjin/rest/maps/京津地区交通干线图_专题图";
            layer3.Url = "http://192.168.170.148:8090/iserver/services/map-world/rest/maps/世界地图";
            layer1.Refresh();
            layer3.Refresh();
        }

        private void ChangeMapAndLayer_Click(object sender, RoutedEventArgs e)
        {
            legend.Map = legend.Map == MyMap1 ? MyMap2 : MyMap1;
            legend.LayerIDs = legend.LayerIDs[0] == LayerIDs1[0] ? LayerIDs2 : LayerIDs1;
        }

        private void ChangeImage_Click(object sender, RoutedEventArgs e)
        {
            legend.LayerItems[0].LayerItems[0].ImageSource = new BitmapImage(new Uri
                ("/green.png", UriKind.RelativeOrAbsolute));
        }

        private void ChangeTemplate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ChangeMapItemTemplate_Click(object sender, RoutedEventArgs e)
        {
            legend.MapLayerTemplate = LayoutRoot.Resources["mapItemTemplate"] as DataTemplate;
        }

        private void ChangeLayerItemTemplate_Click(object sender, RoutedEventArgs e)
        {
            legend.LayerTemplate = LayoutRoot.Resources["layerItemTemplate"] as DataTemplate;
        }

        private void ChangeLegendItemTemplate_Click(object sender, RoutedEventArgs e)
        {
            legend.LegendItemTemplate = LayoutRoot.Resources["legendItemTemplate"] as DataTemplate;
        }

        private void ChangeShowVisibleLayer_Click(object sender, RoutedEventArgs e)
        {
            legend.IsShowOnlyVisibleLayers = !legend.IsShowOnlyVisibleLayers;
        }

        private void ChangeIClientLayerVisible_Click(object sender, RoutedEventArgs e)
        {
            this.MyMap1.Layers[0].IsVisible = !this.MyMap1.Layers[0].IsVisible;
        }

        private void ChangeLegendWidth_Click(object sender, RoutedEventArgs e)
        {
            this.legend.Width = 100;
        }

        private void ChangeLegendHeight_Click(object sender, RoutedEventArgs e)
        {
            this.legend.Height = 200;
        }

        private void ChangeLayerItems_Click(object sender, RoutedEventArgs e)
        {
            this.legend.LayerItems[0].LayerItems[7].LayerItems[0].IsVisible = false;
            
        }

        private void ChangeLayerItems1_Click(object sender, RoutedEventArgs e)
        {
            this.legend.LayerItems[0].LayerItems[7].LayerItems[0].IsVisible = true;
        }
    }
}
