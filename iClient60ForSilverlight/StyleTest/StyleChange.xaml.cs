﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Animation;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace iClient60ForSilverlight
{
    public partial class StyleChange : UserControl
    {
        private FeaturesLayer fLayer = new FeaturesLayer();
        private PredefinedMarkerStyle markerStyle = new PredefinedMarkerStyle() { Color = new SolidColorBrush(Colors.Green), Size = 15 };
        private PredefinedLineStyle lineStyle = new PredefinedLineStyle() { StrokeThickness = 2, Stroke = new SolidColorBrush(Colors.Green) };
        private PredefinedFillStyle fillStyle = new PredefinedFillStyle() { StrokeThickness = 2, Stroke = new SolidColorBrush(Colors.Red),Fill= new SolidColorBrush(Colors.Green) };
        private Feature fP;
        private Feature fL;
        private Feature fR;


        public StyleChange()
        {
            InitializeComponent();

            MyMap.Layers.Add(fLayer);

            fP = new Feature { Geometry = new GeoPoint(13000000, 6200000), Style = markerStyle };
            fL = new Feature { Style = lineStyle };
            fL.Geometry  = new GeoLine
            {
                Parts = new ObservableCollection<Point2DCollection>
                {
                    new Point2DCollection()
                        {
                            new Point2D(-5000000,5000000),
                            new Point2D(-10000000,10000000),

                        },
                    new Point2DCollection()
                        {
                            new Point2D(5000000,-5000000),
                            new Point2D(10000000,-10000000),
                        }
                }
            };

            fR = new Feature { Style = fillStyle};
            fR.Geometry = new GeoRegion
            {
                Parts = new ObservableCollection<Point2DCollection>
                {
                    new Point2DCollection()
                        {
                            new Point2D(0,0),
                            new Point2D(10000000,0),
                            new Point2D(10000000,10000000),
                            new Point2D(0,10000000),
                            new Point2D(0,0)
                        },
                    new Point2DCollection()
                        {
                            new Point2D(-10000000,0),
                            new Point2D(0,-10000000),
                            new Point2D(-10000000,-10000000),
                            new Point2D(-10000000,0)
                        }
                 }
            };


            fLayer.AddFeature(fP);
            fLayer.AddFeature(fL);
            fLayer.AddFeature(fR);
        }

        private void syle_Click(object sender, RoutedEventArgs e)
        {
            //lineStyle.Stroke = new SolidColorBrush(Colors.Black);
            lineStyle.Symbol = PredefinedLineStyle.LineSymbol.DashDotDot;
        }

        private void fea_Click(object sender, RoutedEventArgs e)
        {
            ((fR.Style) as PredefinedFillStyle).Fill = new SolidColorBrush(Colors.Black);
        }

        private void test_Click(object sender, RoutedEventArgs e)
        {
            markerStyle.Color = new SolidColorBrush(Colors.Black);
            markerStyle.Symbol = PredefinedMarkerStyle.MarkerSymbol.Star;
        }

     


    }
}
