﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iClient60ForSilverlight
{
    public partial class iServer6RThemeTest : UserControl
    {
        private const string url = "http://localhost:8090/iserver/services/map-world/rest/maps/World Map";
        //private TiledDynamicRESTLayer themeLayer;
        private DynamicRESTLayer themeLayer;
        private DynamicRESTLayer dynamicRESTLayer;

        public iServer6RThemeTest()
        {
            InitializeComponent();

            dynamicRESTLayer = new DynamicRESTLayer();
            dynamicRESTLayer.Url = url;

            //themeLayer = new TiledDynamicRESTLayer();
            themeLayer = new DynamicRESTLayer();
            themeLayer.Url = url;

            //MyMap.Layers.Add(dynamicRESTLayer);
            MyMap.Layers.Add(themeLayer);
            MyMap.ViewBoundsChanged += new EventHandler<ViewBoundsEventArgs>(MyMap_ViewBoundsChanged);
        }

        private void MyMap_ViewBoundsChanged(object sender, ViewBoundsEventArgs e)
        {
            mytb.Text = (1 / MyMap.Scale).ToString();
        }

        #region  获取Symbol
        /*private void GetMapSymbol_Click(object sender, RoutedEventArgs e)
        {
            GetSymbolParameters getSymbolParameters = new GetSymbolParameters
            {
                BackColor = new Color { R = 255, G = 255, B = 0, A = 255 },
                Height = 500,
                Width = 500,
                Style = new ServerStyle { FillBackColor = new Color { R = 255, G = 0, B = 0, A = 255 }, },

            };

            GetSymbolService getSymbolService = new GetSymbolService(url);
            getSymbolService.ProcessAsync(getSymbolParameters);
            getSymbolService.ProcessCompleted += new EventHandler<GetSymbolEventArgs>(getSymbolService_ProcessCompleted);
            getSymbolService.Failed += new EventHandler<ServiceFailedEventArgs>(getSymbolService_Failed);
        }

        void getSymbolService_Failed(object sender, ServiceFailedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        void getSymbolService_ProcessCompleted(object sender, GetSymbolEventArgs e)
        {
            //throw new NotImplementedException();
        }*/


        #endregion

        #region 专题图
        #region 单值专题图
        private void ThemeUnique_Click(object sender, RoutedEventArgs e)
        {
            List<ThemeUniqueItem> items = new List<ThemeUniqueItem> 
            {
            new ThemeUniqueItem
                 {
                     Caption="A",
                     Unique="151827600", 
                     Style=new ServerStyle
                     {
                         FillForeColor=Colors.Red,                     
                         MarkerSize=20,
                          MarkerSymbolID=10
                     },
                     Visible=true,
                 },
                 new ThemeUniqueItem
                 {
                     Caption="B",
                     Unique="1128139689",
                     Style=new ServerStyle
                     {
                         FillForeColor=Colors.Blue,
                          MarkerSize=20,
                           MarkerSymbolID=30
                     },
                     Visible=true,
                 }
            };

            ThemeUnique themeUnique = new ThemeUnique
            {
                Items = items,
                UniqueExpression = "Pop_1994",
                DefaultStyle = new ServerStyle
                {
                    FillOpaqueRate = 100,
                    FillForeColor = Colors.Orange,
                    FillBackOpaque = true,
                    FillBackColor = Colors.Transparent
                }
            };

            ThemeParameters themeUniqueParameters = new ThemeParameters
            {
                Themes = new List<Theme> { themeUnique },
                DatasetName = "Capitals",
                DataSourceName = "World",
            };

            themeUniqueParameters.JoinItems.Add(new JoinItem
            {
                ForeignTableName = "Countries",
                JoinFilter = "Capitals.Capital=Countries.Capital",
                JoinType = JoinType.LEFTJOIN
            });

            ThemeService themeUniqueService = new ThemeService(url);
            themeUniqueService.ProcessAsync(themeUniqueParameters);
            themeUniqueService.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeUniqueService_ProcessCompleted);
            themeUniqueService.Failed += new EventHandler<ServiceFailedEventArgs>(themeUniqueService_Failed);
        }

        void themeUniqueService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        void themeUniqueService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
        }
        #endregion

        #region 分段专题图
        private void ThemeRangeButton_Click(object sender, RoutedEventArgs e)
        {
            List<ThemeRangeItem> themeRangeItems = new List<ThemeRangeItem>
            {
                new ThemeRangeItem
                {
                     Visible=true,
                     Start=0,
                     End=3058,
                     Caption="A",
                     Style=new ServerStyle
                     {
                          FillForeColor=Colors.Brown
                     }
                },
                new ThemeRangeItem
                {
                     Visible=true,
                     Start=3058,
                     End=6116,
                     Caption="B",
                     Style=new ServerStyle 
                     {
                          FillForeColor=Colors.Green
                     }                      
                },
                new ThemeRangeItem
                {
                    Visible=true,
                    Start=6116,
                    End=9175,
                    Caption="C",
                    Style=new ServerStyle 
                    {
                        FillForeColor=Colors.Blue
                    }
                }
            };

            ThemeRange themeRange = new ThemeRange
            {
                RangeExpression = "SmPerimeter",
                RangeMode = RangeMode.EQUALINTERVAL,
                RangeParameter = 3,
                Items = themeRangeItems
            };

            ThemeParameters themeRangeParams = new ThemeParameters
            {
                DatasetName = "Capitals",
                DataSourceName = "World",
                Themes = new List<Theme> { themeRange }
            };

            themeRangeParams.JoinItems.Add(new JoinItem
            {
                ForeignTableName = "Countries",
                JoinFilter = "Capitals.Capital=Countries.Capital",
                JoinType = JoinType.LEFTJOIN
            });

            ThemeService themeRangeServie = new ThemeService(url);
            themeRangeServie.ProcessAsync(themeRangeParams);
            themeRangeServie.Failed += new EventHandler<ServiceFailedEventArgs>(themeRangeServie_Failed);
            themeRangeServie.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeRangeServie_ProcessCompleted);
        }

        private void themeRangeServie_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
        }

        private void themeRangeServie_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        #endregion

        #region 点密度专题图
        private void ThemeDotDensityButton_Click(object sender, RoutedEventArgs e)
        {
            ThemeDotDensity themeDonDensity = new ThemeDotDensity
            {
                Value = 1000000,
                DotExpression = "Cap_Pop",
                Style = new ServerStyle
                {
                    MarkerSize = 5,
                    FillBackColor = Colors.Red,
                    FillForeColor = Colors.Red,
                    MarkerSymbolID = 10
                }
            };
            /*
                       ThemeParameters dotDensityParams = new ThemeParameters
                       {
                           DatasetName = "Countries",
                           DataSourceName = "World",
                           Themes = new List<Theme> { themeDonDensity }
                       };*/


            ThemeParameters dotDensityParams = new ThemeParameters
            {
                DatasetName = "Countries",
                DataSourceName = "World",
                Themes = new List<Theme> { themeDonDensity }
            };

            dotDensityParams.JoinItems.Add(new JoinItem
            {
                ForeignTableName = "Capitals",
                JoinFilter = "Countries.Country=Capitals.Country",
                JoinType = JoinType.LEFTJOIN
            });


            ThemeService dotDensityService = new ThemeService(url);
            dotDensityService.ProcessAsync(dotDensityParams);
            dotDensityService.ProcessCompleted += new EventHandler<ThemeEventArgs>(dotDensityService_ProcessCompleted);
            dotDensityService.Failed += new EventHandler<ServiceFailedEventArgs>(dotDensityService_Failed);
        }

        void dotDensityService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        void dotDensityService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
        }

        #endregion

        #region 等级符号
        private void ThemeGraduatedSymbolButton_Click(object sender, RoutedEventArgs e)
        {
            ThemeGraduatedSymbol themeGraduatedSymbol = new ThemeGraduatedSymbol
            {
                Style = new ThemeGraduatedSymbolStyle
                {
                    NegativeDisplayed = false,
                    ZeroDisplayed = false,
                    PositiveStyle = new ServerStyle
                    {
                        LineColor = Colors.Red,
                        MarkerSize = 50,
                        MarkerSymbolID = 15
                    },
                },
                BaseValue = 1128139680,
                GraduatedMode = GraduatedMode.SQUAREROOT,
                Flow = new ThemeFlow
                {
                    FlowEnabled = true,


                },
                Expression = "Pop_1994",
            };

            ThemeParameters themeGraduatedSymbolParams = new ThemeParameters
            {
                DatasetName = "World",
                DataSourceName = "World",
                Themes = new List<Theme> { themeGraduatedSymbol }
            };

            ThemeService themeGraduatedSymbolService = new ThemeService(url);
            themeGraduatedSymbolService.ProcessAsync(themeGraduatedSymbolParams);
            themeGraduatedSymbolService.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeGraduatedSymbolService_ProcessCompleted);
            themeGraduatedSymbolService.Failed += new EventHandler<ServiceFailedEventArgs>(themeGraduatedSymbolService_Failed);
        }

        void themeGraduatedSymbolService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        void themeGraduatedSymbolService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
        }

        #endregion

        #region 统计专题图,*****不出图,已向iServer报bug****
        private void ThemeGraphButton_Click(object sender, RoutedEventArgs e)
        {
            ThemeGraph graphTheme = new ThemeGraph
            {
                BarWidth = 0.035,
                Offset = new ThemeOffset
                {
                    OffsetFixed = false,
                    OffsetX = "0.015",
                    OffsetY = "0.015"
                },
                Flow = new ThemeFlow
                {
                    FlowEnabled = true,
                    LeaderLineDisplayed = false,
                },
                GraduatedMode = GraduatedMode.SQUAREROOT,
                GraphAxes = new ThemeGraphAxes
                {
                    AxesDisplayed = false,
                    AxesGridDisplayed = false,
                    AxesTextDisplayed = false,
                },
                GraphSize = new ThemeGraphSize
                {
                    MaxGraphSize = 1,
                    MinGraphSize = 0.35
                },
                GraphSizeFixed = false,
                GraphText = new ThemeGraphText
                {
                    GraphTextDisplayed = true,
                    GraphTextFormat = ThemeGraphTextFormat.VALUE,
                    GraphTextStyle = new ServerTextStyle
                    {
                        FontName = "Times New Roman",
                        Align = SuperMap.Web.iServerJava6R.TextAlignment.BASELINECENTER,
                        FontHeight = 0.4,
                        Outline = false,
                    }
                },
                GraphType = ThemeGraphType.BAR3D,
                NegativeDisplayed = false,
                OverlapAvoided = false,
            };

            ThemeParameters themeGraphParams = new ThemeParameters
            {
                DatasetName = "World",
                DataSourceName = "World",
                Themes = new List<Theme> { graphTheme }
            };

            ThemeService themeGraphService = new ThemeService(url);
            themeGraphService.ProcessAsync(themeGraphParams);
            themeGraphService.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeGraphService_ProcessCompleted);
            themeGraphService.Failed += new EventHandler<ServiceFailedEventArgs>(themeGraphService_Failed);
        }

        void themeGraphService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        void themeGraphService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
        }
        #endregion

        #region 标签专题图
        private void ThemeLabelSymbolButton_Click(object sender, RoutedEventArgs e)
        {
            ThemeLabel labelTheme = new ThemeLabel
            {
                AlongLine = new ThemeLabelAlongLine
                {
                    RepeatIntervalFixed = false,
                    AlongLineDirection = AlongLineDirection.ALONG_LINE_NORMAL,
                    AngleFixed = false,
                    LabelRepeatInterval = 0,
                    IsAlongLine = false,
                    RepeatedLabelAvoided = false
                },

                BackSetting = new ThemeLabelBackSetting
                {
                    BackStyle = new ServerStyle
                    {
                        FillForeColor = Colors.Red
                    },
                    LabelBackShape = LabelBackShape.NONE,
                },

                Flow = new ThemeFlow
                {
                    FlowEnabled = true,
                    LeaderLineDisplayed = false,
                    LeaderLineStyle = new ServerStyle
                    {
                        FillForeColor = Colors.Green
                    },
                },

                Items = new List<ThemeLabelItem> { },
                LabelExpression = "Cap_Pop",
                //LabelExpression = "Capital",

                LabelOverLengthMode = LabelOverLengthMode.NONE,
                MaxLabelLength = 0,
                NumericPrecision = 0,

                Offset = new ThemeOffset
                {
                    OffsetFixed = false,
                    OffsetX = "20",
                    OffsetY = "0"
                },

                OverlapAvoided = false,
                RangeExpression = "",
                SmallGeometryLabeled = false,

                Text = new ThemeLabelText
                {
                    MaxTextHeight = 0,
                    MaxTextWidth = 0,
                    MinTextHeight = 0,
                    MinTextWidth = 0,
                    UniformMixedStyle = new LabelMixedTextStyle(),
                    UniformStyle = new ServerTextStyle
                    {
                        Align = SuperMap.Web.iServerJava6R.TextAlignment.MIDDLELEFT,
                        FontName = "黑体",
                        FontHeight = 0,
                    }
                }
            };

            ThemeParameters themeLabelParams = new ThemeParameters
            {
                DatasetName = "Countries",
                DataSourceName = "World",
                Themes = new List<Theme> { labelTheme }
            };

            themeLabelParams.JoinItems.Add(new JoinItem
            {
                ForeignTableName = "Capitals",
                JoinFilter = "Countries.Country=Capitals.Country",
                JoinType = JoinType.LEFTJOIN
            });

            ThemeService themeLabelService = new ThemeService(url);
            themeLabelService.ProcessAsync(themeLabelParams);
            themeLabelService.ProcessCompleted += new EventHandler<ThemeEventArgs>(themeLabelService_ProcessCompleted);
            themeLabelService.Failed += new EventHandler<ServiceFailedEventArgs>(themeLabelService_Failed);
        }

        void themeLabelService_Failed(object sender, ServiceFailedEventArgs e)
        {

        }

        void themeLabelService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
        }
        #endregion

        #region 复合专题图
        private void compositionTheme_Click(object sender, RoutedEventArgs e)
        {
            List<ThemeUniqueItem> items = new List<ThemeUniqueItem> 
            {
                 new ThemeUniqueItem
                 {
                     Caption="A",
                     Unique="1", 
                     Style=new ServerStyle
                     {
                         FillForeColor=Colors.Red,                     
                     },
                     Visible=true,
                 },
                 new ThemeUniqueItem
                 {
                     Caption="B",
                     Unique="247",
                     Style=new ServerStyle
                     {
                         FillForeColor=Colors.Blue,
                     },
                     Visible=true,
                 }
            };

            ThemeUnique themeUnique = new ThemeUnique
            {
                Items = items,
                UniqueExpression = "SmID",
                DefaultStyle = new ServerStyle
                {
                    FillOpaqueRate = 100,
                    FillForeColor = Colors.Orange,
                    FillBackOpaque = true,
                    FillBackColor = Colors.Transparent
                }
            };

            ThemeDotDensity themeDonDensity = new ThemeDotDensity
            {
                Value = 10000000,
                DotExpression = "Pop_1994",
                Style = new ServerStyle
                {
                    MarkerSize = 5,
                }
            };

            ThemeParameters compostionThemeParameters = new ThemeParameters
            {
                DatasetName = "World",
                DataSourceName = "World",
                Themes = new List<Theme> { themeDonDensity, themeUnique }
            };

            ThemeService compositeService = new ThemeService(url);
            compositeService.ProcessAsync(compostionThemeParameters);
            compositeService.Failed += new EventHandler<ServiceFailedEventArgs>(compositeService_Failed);
            compositeService.ProcessCompleted += new EventHandler<ThemeEventArgs>(compositeService_ProcessCompleted);
        }

        void compositeService_ProcessCompleted(object sender, ThemeEventArgs e)
        {
            themeLayer.LayersID = e.Result.ResourceInfo.NewResourceID;
        }

        void compositeService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }
        #endregion

        #region 删除专题图
        private void RemoveTheme_Click(object sender, RoutedEventArgs e)
        {
            themeLayer.ClearTheme();
        }
        #endregion
        #endregion
    }
}
