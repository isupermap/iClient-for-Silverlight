﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.iServerJava6R.SpatialAnalyst;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iClient60ForSilverlight
{
    public partial class iServer6RSpatialAnalystTest : UserControl
    {
        public static string url = "http://localhost:8090/iserver/services/spatialanalyst-sample/restjsr/spatialanalyst";
        public static string imageUrl = "http://localhost:8090/iserver/services/map-world/rest/maps/World Map";
        //public static string imageUrl = "/iserver/services/map-world/rest/maps/World Map";

        FeaturesLayer resultLayer = new FeaturesLayer();

        private DynamicRESTLayer restLayer = new DynamicRESTLayer
        {
            Url = imageUrl
        };
        public iServer6RSpatialAnalystTest()
        {
            InitializeComponent();
            //this.MyMap.Layers.Add(restLayer);
            this.MyMap.Layers.Add(resultLayer);
        }

        //数据集缓冲
        private void datasetBuffer_Click(object sender, RoutedEventArgs e)
        {
            var bufferAnalystParams = new DatasetBufferAnalystParameters
            {
                BufferSetting = new BufferSetting
                {
                    EndType = BufferEndType.ROUND,
                    LeftDistance = new BufferDistance
                    {
                        Value = 50000
                    },
                    RightDistance = new BufferDistance
                    {
                        Value = 50000
                    },
                    SemicircleLineSegment = 4
                },
                Dataset = "SamplesP@Interpolation",

                IsAttributeRetained = false,
                //IsOverwrite = true,
                //IsReturnRecordset = true,
                IsUnion = true,
                MaxReturnRecordCount = 10000,
                //ResultDatasetName = "aa@Interpolation"
            };

            var datasetBufferAnalyst = new DatasetBufferAnalystService(url);
            datasetBufferAnalyst.ProcessAsync(bufferAnalystParams);
            datasetBufferAnalyst.ProcessCompleted += new EventHandler<DatasetBufferAnalystArgs>(datasetBufferAnalyst_ProcessCompleted);
            datasetBufferAnalyst.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(datasetBufferAnalyst_Failed);
        }

        private void datasetBufferAnalyst_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
        }

        private void datasetBufferAnalyst_ProcessCompleted(object sender, DatasetBufferAnalystArgs e)
        {
            resultLayer.AddFeatureSet(e.Result.Recordset.Features);
        }

        //实体缓冲
        private void geometryBuffer_Click(object sender, RoutedEventArgs e)
        {
            var bufferAnalystParams = new GeometryBufferAnalystParameters
            {
                BufferSetting = new BufferSetting
                {
                    EndType = BufferEndType.ROUND,
                    LeftDistance = new BufferDistance
                    {
                        Value = 100
                    },
                    RightDistance = new BufferDistance
                    {
                        Value = 100
                    },
                    SemicircleLineSegment = 4
                },
                SourceGeometry = new GeoLine
                    {
                        Parts = new ObservableCollection<Point2DCollection>
                        {
                            new Point2DCollection()
                            {
                                new Point2D{ X=23,Y=23},
                                new Point2D{ X=33,Y=37},
                                new Point2D{ X=43,Y=23}
                            }
                        },
                    },
            };

            var geometryBufferAnalyst = new GeometryBufferAnalystService(url);
            geometryBufferAnalyst.ProcessAsync(bufferAnalystParams);
            geometryBufferAnalyst.ProcessCompleted += new EventHandler<GeometryBufferAnalystArgs>(geometryBufferAnalyst_ProcessCompleted);
            geometryBufferAnalyst.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(geometryBufferAnalyst_Failed);
        }

        private void geometryBufferAnalyst_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
        }

        private void geometryBufferAnalyst_ProcessCompleted(object sender, GeometryBufferAnalystArgs e)
        {
            var f = new Feature
            {
                Geometry = (GeoRegion)e.Result.ResultGeometry
            };
            resultLayer.AddFeature(f);
        }

        //数据集叠加
        private void datasetOverlay_Click(object sender, RoutedEventArgs e)
        {
            var datasetOverlayParams = new DatasetOverlayAnalystParameters
            {
                Operation = OverlayOperationType.CLIP,
                //IsReturnRecordset = true,
                //IsOverwrite = true,
                SourceDataset = "SamplesP@Interpolation",
                MaxReturnRecordCount = 100,
                OperateDataset = "BoundsR@Interpolation",
                //ResultDatasetName = "aw@Interpolation",
            };

            var datasetOverlayAnalystService = new DatasetOverlayAnalystService(url);
            datasetOverlayAnalystService.ProcessAsync(datasetOverlayParams);
            datasetOverlayAnalystService.ProcessCompleted += new EventHandler<DatasetOverlayAnalystArgs>(datasetOverlayAnalystService_ProcessCompleted);
            datasetOverlayAnalystService.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(datasetOverlayAnalystService_Failed);
        }

        private void datasetOverlayAnalystService_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
        }

        private void datasetOverlayAnalystService_ProcessCompleted(object sender, DatasetOverlayAnalystArgs e)
        {
            resultLayer.AddFeatureSet(e.Result.Recordset.Features);
        }

        //实体叠加
        private void geometryOverlay_Click(object sender, RoutedEventArgs e)
        {
            var geometryOverlayParams = new GeometryOverlayAnalystParameters
            {
                OperateGeometry = new GeoRegion
                {
                    //Points = new Point2DCollection
                    //   {
                    //       new Point2D{X=23,Y=23},
                    //       new Point2D{X=33,Y=22},
                    //       new Point2D{X=43,Y=22}
                    //   }
                    Parts = new ObservableCollection<Point2DCollection>
                               {
                                   new Point2DCollection()
                                       {        new Point2D{X=23,Y=23},
                                           new Point2D{X=33,Y=22},
                                           new Point2D{X=43,Y=22}
                                           
                                       },
                               },
                },
                SourceGeometry = new GeoRegion
                {
                    //Type = ServerGeometryType.REGION,
                    //Points = new Point2DCollection
                    // {
                    //     new Point2D{X=23,Y=23},
                    //     new Point2D{X=34,Y=47},
                    //     new Point2D{X=50,Y=12}
                    // }
                    Parts = new ObservableCollection<Point2DCollection>
                               {
                                   new Point2DCollection()
                                       {        new Point2D{X=23,Y=23},
                                           new Point2D{X=34,Y=47},
                                           new Point2D{X=50,Y=12}
                                           
                                       },
                               },
                },

                Operation = OverlayOperationType.CLIP
            };

            var geometryOverlayService = new GeometryOverlayAnalystService(url);
            geometryOverlayService.ProcessAsync(geometryOverlayParams);
            geometryOverlayService.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(geometryOverlayService_Failed);
            geometryOverlayService.ProcessCompleted += new EventHandler<GeometryOverlayAnalystArgs>(geometryOverlayService_ProcessCompleted);
        }

        private void geometryOverlayService_ProcessCompleted(object sender, GeometryOverlayAnalystArgs e)
        {
            var f = new Feature
            {
                Geometry = (GeoRegion)e.Result.ResultGeometry
            };
            resultLayer.AddFeature(f);
        }

        private void geometryOverlayService_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
        }

        //数据集提取等值线
        private void datasetIsoline_Click(object sender, RoutedEventArgs e)
        {
            var parameters = new DatasetSurfaceAnalystParameters
            {
                SurfaceAnalystMethod = SurfaceAnalystMethod.ISOLINE,
                Resolution = 3000,
                Dataset = "SamplesP@Interpolation",
                ZValueFieldName = "AVG_WTR",
                //ResultSetting = new DataReturnOption
                //{
                //    //DatasetName = "iso@Interpolation",
                //    MaxRecordCount = 100,
                //    //IsOverwrite = true
                //},
                MaxReturnRecordCount = 100,
                ParametersSetting = new SurfaceAnalystParametersSetting
                {
                    ResampleTolerance = 0.7,
                    Smoothness = 3,
                    Interval = 100,
                    SmoothMethod = SmoothMethod.BSPLINE,
                    DatumValue = 0,
                    //ClipRegion = new GeoRegion
                    //{
                    //    Parts = new ObservableCollection<Point2DCollection> 
                    //    {
                    //         new Point2DCollection
                    //         {
                    //              new Point2D(-2661198,5769677),
                    //              new Point2D(-2661198,1683273),
                    //              new Point2D(2372026,1683273),
                    //              new Point2D(2372026,5769677)
                    //         },
                    //    },
                    //}
                }
            };

            var datasetIsolineService = new SurfaceAnalystService(url);
            datasetIsolineService.ProcessAsync(parameters);
            datasetIsolineService.ProcessCompleted += new EventHandler<SurfaceAnalystEventArgs>(datasetIsolineService_ProcessCompleted);
            datasetIsolineService.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(datasetIsolineService_Failed);
        }

        private void datasetIsolineService_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
        }

        private void datasetIsolineService_ProcessCompleted(object sender, SurfaceAnalystEventArgs e)
        {
            resultLayer.AddFeatureSet(e.Result.Recordset.Features);
        }

        //数据集提取等值面
        private void datasetIsoRegion_Click(object sender, RoutedEventArgs e)
        {
            var parameters = new DatasetSurfaceAnalystParameters
            {
                SurfaceAnalystMethod = SurfaceAnalystMethod.ISOREGION,
                Resolution = 3000,
                Dataset = "SamplesP@Interpolation",
                ZValueFieldName = "AVG_WTR",
                //ResultSetting = new DataReturnOption
                //{
                //    //DatasetName = "iso1@Interpolation",
                //    MaxRecordCount = 100,
                //    //IsOverwrite = true
                //},
                MaxReturnRecordCount = 100,
                ParametersSetting = new SurfaceAnalystParametersSetting
                {
                    ResampleTolerance = 0.7,
                    Smoothness = 3,
                    Interval = 100,
                    SmoothMethod = SmoothMethod.BSPLINE,
                    DatumValue = 100,
                }
            };

            var datasetIsoRegionService = new SurfaceAnalystService(url);
            datasetIsoRegionService.ProcessAsync(parameters);
            datasetIsoRegionService.ProcessCompleted += new EventHandler<SurfaceAnalystEventArgs>(datasetIsoRegionService_ProcessCompleted);
            datasetIsoRegionService.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(datasetIsoRegionService_Failed);
        }

        private void datasetIsoRegionService_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
        }

        private void datasetIsoRegionService_ProcessCompleted(object sender, SurfaceAnalystEventArgs e)
        {
            resultLayer.AddFeatureSet(e.Result.Recordset.Features);
        }

        //geometry提取等值线
        private void geometryIsoline_Click(object sender, RoutedEventArgs e)
        {
            var parameters = new GeometrySurfaceAnalystParameters
            {
                SurfaceAnalystMethod = SurfaceAnalystMethod.ISOLINE,
                Resolution = 3000,
                Points = new Point2DCollection
                   {
                        new Point2D{
                            Y= 5846399.01175416,
                            X= 1210581.3465131
                        },
                        new Point2D{
                            Y= 5806144.68366852,
                            X= 1374568.19688557
                        },
                        new Point2D{
                            Y= 5770737.83129165,
                            X= 1521370.85300054
                        },
                        new Point2D{
                            Y= 5528199.92958311,
                            X= 1095631.45977217
                        },
                        new Point2D{
                            Y= 5570741.49064607,
                            X= 1198626.21785984
                        },
                        new Point2D{
                            Y= 5593290.50255359,
                            X= 1373169.38077361
                        },
                        new Point2D{
                            Y= 5627352.64228498,
                            X= 1612502.82819101
                        },
                        new Point2D{
                            Y= 5411257.38832255,
                            X= 1081393.24523065
                        },
                        new Point2D{
                            Y= 5458415.29448267,
                            X= 1369469.88462763
                        },
                        new Point2D{
                            Y= 5476577.89402985,
                            X= 1479703.15553903
                        },
                        new Point2D{
                            Y= 5538359.53266899,
                            X= 1625450.59309043
                        },
                        new Point2D{
                            Y= 5322253.55595858,
                            X= 874680.756098034
                        },
                        new Point2D{
                            Y= 5387691.35135605,
                            X= 1247212.60467642
                        },
                        new Point2D{
                            Y= 5365829.12376669,
                            X= 1552293.42507508
                        },
                        new Point2D{
                            Y= 5189236.97383116,
                            X= 1129966.26952038
                        },
                        new Point2D{
                            Y= 5263941.13976024,
                            X= 1421971.51732685
                        },
                        new Point2D{
                            Y= 5316511.81462062,
                            X= 1646535.17129977
                        },
                        new Point2D{
                            Y= 5380482.7734873,
                            X= 1781263.73730004
                        },
                        new Point2D{
                            Y= 5387628.37304574,
                            X= 2020110.74786557
                        },
                        new Point2D{
                            Y= 5149389.37335088,
                            X= 1404994.6955034
                        },
                        new Point2D{
                            Y= 5175828.87663573,
                            X= 1548389.26860757
                        },
                        new Point2D{
                            Y= 5309279.1724014,
                            X= 1907367.43149643
                        },
                        new Point2D{
                            Y= 5293521.85741038,
                            X= 2062286.6638636
                        },
                        new Point2D{
                            Y= 4976125.83982239,
                            X= 927814.758846577
                        },
                        new Point2D{
                            Y= 5026632.56869923,
                            X= 1543409.98087479
                        },
                        new Point2D{
                            Y= 5130288.82705181,
                            X= 1672545.4937333
                        },
                        new Point2D{
                            Y= 5188386.30774339,
                            X= 1815170.61581664
                        },
                        new Point2D{
                            Y= 5093043.21216217,
                            X= 1776810.62037654
                        },
                        new Point2D{
                            Y= 5157034.50256423,
                            X= 2000720.93926822
                        },
                        new Point2D{
                            Y= 5251089.30125404,
                            X= 2137211.95629998
                        },
                        new Point2D{
                            Y= 5332054.20434767,
                            X= -1384684.21976224
                        },
                        new Point2D{
                            Y= 5275056.3083835,
                            X= -1266592.67195803
                        },
                        new Point2D{
                            Y= 5175286.30171108,
                            X= -1173120.37547033
                        },
                        new Point2D{
                            Y= 5241172.93384647,
                            X= -1666090.72204382
                        },
                        new Point2D{
                            Y= 5203815.90203324,
                            X= -1462060.04834705
                        },
                        new Point2D{
                            Y= 5089465.30512548,
                            X= -1553103.06288549
                        },
                        new Point2D{
                            Y= 4985329.13408486,
                            X= -1122827.89723113
                        },
                        new Point2D{
                            Y= 5084477.14390444,
                            X= -1860653.07373091
                        },
                        new Point2D{
                            Y= 5012320.00449773,
                            X= -1725720.73925144
                        },
                        new Point2D{
                            Y= 4847254.4566494,
                            X= -1220678.02426651
                        },
                        new Point2D{
                            Y= 4967869.57917987,
                            X= -1863459.09401226
                        },
                        new Point2D{
                            Y= 4846715.02903969,
                            X= -1376674.68719707
                        },
                        new Point2D{
                            Y= 4749993.79908471,
                            X= -1503256.39597018
                        },
                        new Point2D{
                            Y= 4733437.65622082,
                            X= -1062382.73304631
                        },
                        new Point2D{
                            Y= 4817393.24951001,
                            X= -1666642.31853787
                        },
                        new Point2D{
                            Y= 4731665.18199693,
                            X= -1268721.06517955
                        },
                        new Point2D{
                            Y= 4691431.37946197,
                            X= -1784339.07456816
                        },
                        new Point2D{
                            Y= 4643802.53247814,
                            X= -1537407.40647844
                        },
                        new Point2D{
                            Y= 4595707.88157582,
                            X= -2419318.16518532
                        },
                        new Point2D{
                            Y= 4697780.17446741,
                            X= -2175064.94799138
                        },
                        new Point2D{
                            Y= 4571808.9805013,
                            X= -2199394.38191528
                        },
                        new Point2D{
                            Y= 4596066.82343988,
                            X= -1978125.62843053
                        },
                        new Point2D{
                            Y= 4496517.04647146,
                            X= -1432929.65674657
                        },
                        new Point2D{
                            Y= 4311774.01178654,
                            X= -1425142.47552866
                        },
                        new Point2D{
                            Y= 4452868.31749155,
                            X= -2347377.8808384
                        },
                        new Point2D{
                            Y= 4340268.27463545,
                            X= -2287997.73283675
                        },
                        new Point2D{
                            Y= 4250483.4258309,
                            X= -2163460.02757897
                        },
                        new Point2D{
                            Y= 4263739.81754219,
                            X= -1828785.52128088
                        },
                        new Point2D{
                            Y= 4188669.31509868,
                            X= -1212229.28247201
                        },
                        new Point2D{
                            Y= 4183182.83873892,
                            X= -2025606.92718856
                        },
                        new Point2D{
                            Y= 4710813.79158798,
                            X= -825209.394421958
                        },
                        new Point2D{
                            Y= 4670956.24413595,
                            X= -925452.706876877
                        },
                        new Point2D{
                            Y= 4526184.57635418,
                            X= -321480.003370163
                        },
                        new Point2D{
                            Y= 4529087.78970059,
                            X= -651959.49175178
                        },
                        new Point2D{
                            Y= 4456951.66969839,
                            X= -217002.751793607
                        },
                        new Point2D{
                            Y= 4361674.17388149,
                            X= -862966.032372303
                        },
                        new Point2D{
                            Y= 4356696.39135096,
                            X= -665822.612619922
                        },
                        new Point2D{
                            Y= 4318695.18826926,
                            X= -16755.8954939763
                        },
                        new Point2D{
                            Y= 4291518.68150134,
                            X= -548534.027519153
                        },
                        new Point2D{
                            Y= 4214198.76083203,
                            X= -281463.84777592
                        },
                        new Point2D{
                            Y= 4217114.78997084,
                            X= -993942.17692933
                        },
                        new Point2D{
                            Y= 4188310.29690308,
                            X= -388864.100827363
                        },
                        new Point2D{
                            Y= 4147044.70045506,
                            X= -163911.282778357
                        },
                        new Point2D{
                            Y= 3994669.65229173,
                            X= -989326.169926525
                        },
                        new Point2D{
                            Y= 4097277.46806724,
                            X= -831169.132434991
                        },
                        new Point2D{
                            Y= 4027790.2582361,
                            X= -663189.775811282
                        },
                        new Point2D{
                            Y= 4008890.15249491,
                            X= -423251.717948863
                        },
                        new Point2D{
                            Y= 3985439.0098199,
                            X= -185918.900601112
                        },
                        new Point2D{
                            Y= 3940087.84301485,
                            X= -887752.642321781
                        },
                        new Point2D{
                            Y= 3902840.70822795,
                            X= -607958.642974107
                        },
                        new Point2D{
                            Y= 3933468.62385761,
                            X= -284995.314627413
                        },
                        new Point2D{
                            Y= 3854005.31553064,
                            X= -98784.4216291984
                        },
                        new Point2D{
                            Y= 3823156.34762407,
                            X= -1062959.98443942
                        },
                        new Point2D{
                            Y= 3808807.65507124,
                            X= -378120.522440156
                        },
                        new Point2D{
                            Y= 3778115.4734166,
                            X= 0
                        },
                        new Point2D{
                            Y= 4730534.72017257,
                            X= 555675.893973636
                        },
                        new Point2D{
                            Y= 4852624.17143594,
                            X= 719410.765951757
                        },
                        new Point2D{
                            Y= 4596256.33005592,
                            X= 416086.892674244
                        },
                        new Point2D{
                            Y= 4791746.94877328,
                            X= 788837.859700444
                        },
                        new Point2D{
                            Y= 4453008.15232032,
                            X= 115404.754046287
                        },
                        new Point2D{
                            Y= 4596011.98746132,
                            X= 641143.983066076
                        },
                        new Point2D{
                            Y= 4481738.3587981,
                            X= 288969.735496225
                        },
                        new Point2D{
                            Y= 4503841.15536681,
                            X= 445513.596625804
                        },
                        new Point2D{
                            Y= 4547533.1139069,
                            X= 735284.890774623
                        },
                        new Point2D{
                            Y= 4410713.69270361,
                            X= 554568.91407516
                        },
                        new Point2D{
                            Y= 4443600.75131925,
                            X= 667140.230636665
                        },
                        new Point2D{
                            Y= 4340461.47812997,
                            X= 697974.317012174
                        },
                        new Point2D{
                            Y= 4275630.55404934,
                            X= 63161.0494544051
                        },
                        new Point2D{
                            Y= 4386963.12826204,
                            X= 200844.704710507
                        },
                        new Point2D{
                            Y= 4202007.50817714,
                            X= 253524.487125277
                        },
                        new Point2D{
                            Y= 4291562.13846185,
                            X= 419214.550976308
                        },
                        new Point2D{
                            Y= 4246332.74161325,
                            X= 520391.418470442
                        },
                        new Point2D{
                            Y= 4212339.56506825,
                            X= 724422.341977581
                        },
                        new Point2D{
                            Y= 4319799.30743599,
                            X= 803872.440458256
                        },
                        new Point2D{
                            Y= 4129148.23944623,
                            X= 104259.642156941
                        },
                        new Point2D{
                            Y= 4109815.76603993,
                            X= 403953.288135979
                        },
                        new Point2D{
                            Y= 4182364.9195765,
                            X= 658420.237399424
                        },
                        new Point2D{
                            Y= 4116114.81460488,
                            X= 810569.047871445
                        },
                        new Point2D{
                            Y= 4015665.70312797,
                            X= 57888.6128501599
                        },
                        new Point2D{
                            Y= 4051893.79873069,
                            X= 207560.532887203
                        },
                        new Point2D{
                            Y= 3955829.39507851,
                            X= 276969.255214574
                        },
                        new Point2D{
                            Y= 4033619.21146623,
                            X= 529235.890586695
                        },
                        new Point2D{
                            Y= 4074202.839648,
                            X= 652385.210996039
                        },
                        new Point2D{
                            Y= 3996377.3703958,
                            X= 696258.054838688
                        },
                        new Point2D{
                            Y= 4008021.5303752,
                            X= 828160.952774673
                        },
                        new Point2D{
                            Y= 3924572.25012626,
                            X= 395141.805401175
                        },
                        new Point2D{
                            Y= 3987617.53255572,
                            X= 604975.099446582
                        },
                        new Point2D{
                            Y= 3874146.39928132,
                            X= 574491.97093995
                        },
                        new Point2D{
                            Y= 3900010.46986606,
                            X= 826688.565478772
                        },
                        new Point2D{
                            Y= 3798192.21543255,
                            X= 148384.115113218
                        },
                        new Point2D{
                            Y= 3820764.88362328,
                            X= 233881.007408209
                        },
                        new Point2D{
                            Y= 3751682.66734236,
                            X= 364131.993837199
                        },
                        new Point2D{
                            Y= 3754910.35564437,
                            X= 538899.222473607
                        },
                        new Point2D{
                            Y= 3813977.85449175,
                            X= 658807.463567117
                        },
                        new Point2D{
                            Y= 3806050.10370921,
                            X= 792225.28442553
                        },
                        new Point2D{
                            Y= 4878613.66968738,
                            X= 989879.451014638
                        },
                        new Point2D{
                            Y= 4913952.11370657,
                            X= 1247443.67571332
                        },
                        new Point2D{
                            Y= 4831270.94477481,
                            X= 1140083.25107815
                        },
                        new Point2D{
                            Y= 4921315.05493549,
                            X= 1492086.6167465
                        },
                        new Point2D{
                            Y= 5052757.70771331,
                            X= 1918509.27530086
                        },
                        new Point2D{
                            Y= 5063796.81301938,
                            X= 2042017.29979237
                        },
                        new Point2D{
                            Y= 4793681.90422199,
                            X= 877851.099733359
                        },
                        new Point2D{
                            Y= 4774006.99534911,
                            X= 1040808.50860678
                        },
                        new Point2D{
                            Y= 4825058.94082724,
                            X= 1372389.78541081
                        },
                        new Point2D{
                            Y= 4809389.80096441,
                            X= 1543742.52786379
                        },
                        new Point2D{
                            Y= 4902262.258834,
                            X= 1597042.34358779
                        },
                        new Point2D{
                            Y= 4895476.54413311,
                            X= 1842559.94348831
                        },
                        new Point2D{
                            Y= 4599996.60493905,
                            X= 932329.791285148
                        },
                        new Point2D{
                            Y= 4634929.70981753,
                            X= 1130375.65588701
                        },
                        new Point2D{
                            Y= 4663722.19893533,
                            X= 1271390.10186989
                        },
                        new Point2D{
                            Y= 4697792.92263294,
                            X= 1416741.02877937
                        },
                        new Point2D{
                            Y= 4699147.54747854,
                            X= 1613992.86446047
                        },
                        new Point2D{
                            Y= 4827335.18909809,
                            X= 1738675.30026215
                        },
                        new Point2D{
                            Y= 4866923.04130859,
                            X= 1954423.57165034
                        },
                        new Point2D{
                            Y= 4493384.04876875,
                            X= 958554.984492026
                        },
                        new Point2D{
                            Y= 4584793.26261634,
                            X= 1039735.87537795
                        },
                        new Point2D{
                            Y= 4573368.71974201,
                            X= 1264987.12544988
                        },
                        new Point2D{
                            Y= 4535682.89893446,
                            X= 1326693.92599423
                        },
                        new Point2D{
                            Y= 4635574.88352796,
                            X= 1504197.34322534
                        },
                        new Point2D{
                            Y= 4700294.42098123,
                            X= 1780949.13158124
                        },
                        new Point2D{
                            Y= 4609648.65600361,
                            X= 1736296.85726129
                        },
                        new Point2D{
                            Y= 4681629.34942827,
                            X= 1892050.86697712
                        },
                        new Point2D{
                            Y= 4429115.4946153,
                            X= 819736.784093866
                        },
                        new Point2D{
                            Y= 4391346.67602316,
                            X= 875265.46321163
                        },
                        new Point2D{
                            Y= 4480718.86504277,
                            X= 1069883.28758263
                        },
                        new Point2D{
                            Y= 4426733.6720174,
                            X= 1161193.55835407
                        },
                        new Point2D{
                            Y= 4499741.2075763,
                            X= 1429545.72668076
                        },
                        new Point2D{
                            Y= 4544119.35480714,
                            X= 1634202.9161613
                        },
                        new Point2D{
                            Y= 4462966.02017487,
                            X= 1612068.91063451
                        },
                        new Point2D{
                            Y= 4332966.0782395,
                            X= 963309.010178802
                        },
                        new Point2D{
                            Y= 4261654.54843649,
                            X= 1032901.83534834
                        },
                        new Point2D{
                            Y= 4335564.77584577,
                            X= 1105939.22516913
                        },
                        new Point2D{
                            Y= 4317860.35764764,
                            X= 1170895.84077513
                        },
                        new Point2D{
                            Y= 4217404.24906042,
                            X= 895159.921777312
                        },
                        new Point2D{
                            Y= 4172037.28506849,
                            X= 1013674.17742552
                        },
                        new Point2D{
                            Y= 4294064.32320943,
                            X= 1410882.22311226
                        },
                        new Point2D{
                            Y= 4065800.48286813,
                            X= 981182.910627941
                        },
                        new Point2D{
                            Y= 4085762.36636633,
                            X= 1083753.04706252
                        },
                        new Point2D{
                            Y= 4132090.75205249,
                            X= 1318132.26920098
                        },
                        new Point2D{
                            Y= 4142586.73480868,
                            X= 1529070.00567553
                        },
                        new Point2D{
                            Y= 3901432.30854768,
                            X= 934688.365342644
                        },
                        new Point2D{
                            Y= 3979771.31620484,
                            X= 1055867.46884087
                        },
                        new Point2D{
                            Y= 3941036.30757412,
                            X= 1065036.66719561
                        },
                        new Point2D{
                            Y= 3945317.28258398,
                            X= 1157932.98860132
                        },
                        new Point2D{
                            Y= 4021239.52247673,
                            X= 1239207.37028499
                        },
                        new Point2D{
                            Y= 3959852.55882132,
                            X= 1350795.41035969
                        },
                        new Point2D{
                            Y= 4049571.66639741,
                            X= 1410747.90968575
                        },
                        new Point2D{
                            Y= 3812004.0719844,
                            X= 930774.762858088
                        },
                        new Point2D{
                            Y= 3861707.05045625,
                            X= 1052333.00528697
                        },
                        new Point2D{
                            Y= 3820762.94886215,
                            X= 1192606.77848034
                        },
                        new Point2D{
                            Y= 3878446.39774496,
                            X= 1291276.59054776
                        },
                        new Point2D{
                            Y= 3740457.13221034,
                            X= -2280711.28933375
                        },
                        new Point2D{
                            Y= 3610835.64657684,
                            X= -1898494.89479583
                        },
                        new Point2D{
                            Y= 3432858.02097836,
                            X= -1399626.09108375
                        },
                        new Point2D{
                            Y= 3525877.24680728,
                            X= -1283933.48926119
                        },
                        new Point2D{
                            Y= 3417074.9256984,
                            X= -1207841.10592021
                        },
                        new Point2D{
                            Y= 3472059.71094418,
                            X= -2235091.75387382
                        },
                        new Point2D{
                            Y= 3405191.58105513,
                            X= -1535719.26165889
                        },
                        new Point2D{
                            Y= 3317974.06375021,
                            X= -1313233.1022214
                        },
                        new Point2D{
                            Y= 3215456.02366254,
                            X= -1668231.07906761
                        },
                        new Point2D{
                            Y= 3213100.7340777,
                            X= -1543300.50386288
                        },
                        new Point2D{
                            Y= 3226788.4638176,
                            X= -1322861.96368399
                        },
                        new Point2D{
                            Y= 3174717.87547459,
                            X= -1726275.53533564
                        },
                        new Point2D{
                            Y= 3071026.63935862,
                            X= -1213998.10397285
                        },
                        new Point2D{
                            Y= 3042582.07956807,
                            X= -1551388.82660322
                        },
                        new Point2D{
                            Y= 3718278.01215987,
                            X= -1134893.92611012
                        },
                        new Point2D{
                            Y= 3541408.55446127,
                            X= -891441.225776796
                        },
                        new Point2D{
                            Y= 3675701.19714148,
                            X= -834256.439798467
                        },
                        new Point2D{
                            Y= 3540464.43959992,
                            X= -732998.03015512
                        },
                        new Point2D{
                            Y= 3746189.56692848,
                            X= -608366.655726914
                        },
                        new Point2D{
                            Y= 3606814.82794058,
                            X= -487052.674114973
                        },
                        new Point2D{
                            Y= 3709929.54664069,
                            X= -305817.396472208
                        },
                        new Point2D{
                            Y= 3576785.13185038,
                            X= -185572.123301419
                        },
                        new Point2D{
                            Y= 3736802.08201411,
                            X= -188273.034228444
                        },
                        new Point2D{
                            Y= 3671256.44640523,
                            X= -88798.5526841957
                        },
                        new Point2D{
                            Y= 3554207.61323663,
                            X= -7623.30418290906
                        },
                        new Point2D{
                            Y= 3442294.00160218,
                            X= -1043182.71934844
                        },
                        new Point2D{
                            Y= 3372470.13339255,
                            X= -879746.005973445
                        },
                        new Point2D{
                            Y= 3329846.46586159,
                            X= -735820.26243473
                        },
                        new Point2D{
                            Y= 3385774.17225212,
                            X= -598714.291633867
                        },
                        new Point2D{
                            Y= 3365061.37108834,
                            X= -467319.138920955
                        },
                        new Point2D{
                            Y= 3438511.67240308,
                            X= -432661.89096875
                        },
                        new Point2D{
                            Y= 3289222.03255561,
                            X= -365797.290833479
                        },
                        new Point2D{
                            Y= 3388770.17906844,
                            X= -257770.633540852
                        },
                        new Point2D{
                            Y= 3489340.01488981,
                            X= -225771.692979256
                        },
                        new Point2D{
                            Y= 3470604.75468223,
                            X= -132336.985354525
                        },
                        new Point2D{
                            Y= 3443468.92293677,
                            X= -44754.6223657837
                        },
                        new Point2D{
                            Y= 3336462.95672917,
                            X= -29663.1701911858
                        },
                        new Point2D{
                            Y= 3188348.36033678,
                            X= -562133.802732499
                        },
                        new Point2D{
                            Y= 3182395.10255378,
                            X= -451072.029089209
                        },
                        new Point2D{
                            Y= 3171691.27767082,
                            X= -190690.808347991
                        },
                        new Point2D{
                            Y= 3246944.60898054,
                            X= -93001.3198132454
                        },
                        new Point2D{
                            Y= 3189044.90691625,
                            X= -1018939.24320695
                        },
                        new Point2D{
                            Y= 3075883.66985843,
                            X= -452936.775008653
                        },
                        new Point2D{
                            Y= 3181664.96098998,
                            X= -288960.46159696
                        },
                        new Point2D{
                            Y= 3118781.22393754,
                            X= -159790.744577716
                        },
                        new Point2D{
                            Y= 3048061.66104169,
                            X= -728952.008541062
                        },
                        new Point2D{
                            Y= 3016310.11607334,
                            X= -593441.216272313
                        },
                        new Point2D{
                            Y= 3065512.79171986,
                            X= -337546.791497588
                        },
                        new Point2D{
                            Y= 3037253.5443433,
                            X= -38674.8588136952
                        },
                        new Point2D{
                            Y= 2870958.91798656,
                            X= -562922.85739458
                        },
                        new Point2D{
                            Y= 2940239.589357,
                            X= -267015.166073815
                        },
                        new Point2D{
                            Y= 2876002.74884915,
                            X= -126172.573115854
                        },
                        new Point2D{
                            Y= 2832711.95458169,
                            X= -472704.963798652
                        },
                        new Point2D{
                            Y= 2800972.85034979,
                            X= -272502.708102723
                        },
                        new Point2D{
                            Y= 2772709.84050023,
                            X= -170569.940210372
                        },
                        new Point2D{
                            Y= 2821578.0335128,
                            X= -70850.184314894
                        },
                        new Point2D{
                            Y= 2637288.51064395,
                            X= -655586.120477028
                        },
                        new Point2D{
                            Y= 2644172.18392656,
                            X= -587777.726745159
                        },
                        new Point2D{
                            Y= 2703298.89946865,
                            X= -482287.720412992
                        },
                        new Point2D{
                            Y= 2700248.29722294,
                            X= -313691.583453497
                        },
                        new Point2D{
                            Y= 2621928.10377776,
                            X= -349824.747787611
                        },
                        new Point2D{
                            Y= 2618547.89012006,
                            X= -233803.408535259
                        },
                        new Point2D{
                            Y= 2679316.75973283,
                            X= -115340.872386075
                        },
                        new Point2D{
                            Y= 2531477.86970731,
                            X= -728995.100717991
                        },
                        new Point2D{
                            Y= 2563094.64881326,
                            X= -125143.603172223
                        },
                        new Point2D{
                            Y= 2502934.899521,
                            X= -502247.561521754
                        },
                        new Point2D{
                            Y= 2358890.88240176,
                            X= -524954.107223773
                        },
                        new Point2D{
                            Y= 2292630.51177167,
                            X= -439585.374990592
                        },
                        new Point2D{
                            Y= 2377779.72650678,
                            X= -416990.30480708
                        },
                        new Point2D{
                            Y= 2445570.16048078,
                            X= -309745.082737913
                        },
                        new Point2D{
                            Y= 2232920.85836036,
                            X= -359916.399294494
                        },
                        new Point2D{
                            Y= 2352502.54855071,
                            X= -326398.332104555
                        },
                        new Point2D{
                            Y= 2436706.39270386,
                            X= -166103.912027098
                        },
                        new Point2D{
                            Y= 3688008.89570777,
                            X= 67600.2272072867
                        },
                        new Point2D{
                            Y= 3663480.15988555,
                            X= 192838.184529764
                        },
                        new Point2D{
                            Y= 3662810.59694284,
                            X= 355703.330753522
                        },
                        new Point2D{
                            Y= 3688204.18776202,
                            X= 458561.395295938
                        },
                        new Point2D{
                            Y= 3644173.48283529,
                            X= 547133.521977902
                        },
                        new Point2D{
                            Y= 3718679.28398731,
                            X= 671945.729762831
                        },
                        new Point2D{
                            Y= 3736696.43296549,
                            X= 777367.148298923
                        },
                        new Point2D{
                            Y= 3518527.47714298,
                            X= 186765.444956859
                        },
                        new Point2D{
                            Y= 3539458.56983606,
                            X= 696201.573074832
                        },
                        new Point2D{
                            Y= 3638944.59590014,
                            X= 865109.855108802
                        },
                        new Point2D{
                            Y= 3408250.65462685,
                            X= 282035.410131644
                        },
                        new Point2D{
                            Y= 3484719.53651323,
                            X= 371995.501741616
                        },
                        new Point2D{
                            Y= 3415764.87662477,
                            X= 536177.020615121
                        },
                        new Point2D{
                            Y= 3460331.70655059,
                            X= 617095.718836684
                        },
                        new Point2D{
                            Y= 3441411.77217682,
                            X= 719233.636417891
                        },
                        new Point2D{
                            Y= 3546714.9420924,
                            X= 827799.023170084
                        },
                        new Point2D{
                            Y= 3449748.54867299,
                            X= 839739.868053981
                        },
                        new Point2D{
                            Y= 3349980.98134904,
                            X= 90421.6594242863
                        },
                        new Point2D{
                            Y= 3351011.0721427,
                            X= 165247.267444322
                        },
                        new Point2D{
                            Y= 3309382.62069191,
                            X= 234913.696065381
                        },
                        new Point2D{
                            Y= 3299160.4264898,
                            X= 423508.672359737
                        },
                        new Point2D{
                            Y= 3329772.07689117,
                            X= 710677.759931878
                        },
                        new Point2D{
                            Y= 3352006.07650959,
                            X= 939912.922450867
                        },
                        new Point2D{
                            Y= 3260166.78454166,
                            X= 103889.72619136
                        },
                        new Point2D{
                            Y= 3252089.69762263,
                            X= 264733.745155264
                        },
                        new Point2D{
                            Y= 3212982.41711427,
                            X= 424247.109423719
                        },
                        new Point2D{
                            Y= 3269120.01253464,
                            X= 595211.439003019
                        },
                        new Point2D{
                            Y= 3233715.91139639,
                            X= 681507.113912423
                        },
                        new Point2D{
                            Y= 3280596.97028057,
                            X= 863121.036452202
                        },
                        new Point2D{
                            Y= 3124913.88382641,
                            X= 4790.18386941863
                        },
                        new Point2D{
                            Y= 3125950.54618129,
                            X= 140506.955285668
                        },
                        new Point2D{
                            Y= 3117269.89290298,
                            X= 495831.120641366
                        },
                        new Point2D{
                            Y= 3134080.45553659,
                            X= 775380.764486573
                        },
                        new Point2D{
                            Y= 3107858.7627186,
                            X= 922632.139487556
                        },
                        new Point2D{
                            Y= 3046593.54436373,
                            X= 41857.0362429994
                        },
                        new Point2D{
                            Y= 3056211.55619788,
                            X= 207498.128916585
                        },
                        new Point2D{
                            Y= 3047811.12765708,
                            X= 363960.379351961
                        },
                        new Point2D{
                            Y= 3014137.90043046,
                            X= 523881.777939532
                        },
                        new Point2D{
                            Y= 3086839.04362806,
                            X= 643823.784250282
                        },
                        new Point2D{
                            Y= 3002649.08852154,
                            X= 769827.483357505
                        },
                        new Point2D{
                            Y= 2869651.62869318,
                            X= 27872.9317484134
                        },
                        new Point2D{
                            Y= 2915984.37123388,
                            X= 184415.713127814
                        },
                        new Point2D{
                            Y= 2947341.33027515,
                            X= 317288.190737034
                        },
                        new Point2D{
                            Y= 2897186.39990223,
                            X= 459764.109022809
                        },
                        new Point2D{
                            Y= 2882905.72963603,
                            X= 636193.600186931
                        },
                        new Point2D{
                            Y= 2898971.75316899,
                            X= 756729.755392647
                        },
                        new Point2D{
                            Y= 2968738.55616601,
                            X= 916448.816089683
                        },
                        new Point2D{
                            Y= 2890565.81626988,
                            X= 976711.338986039
                        },
                        new Point2D{
                            Y= 2791279.37379748,
                            X= 171902.057106717
                        },
                        new Point2D{
                            Y= 2839146.61113946,
                            X= 361998.773794132
                        },
                        new Point2D{
                            Y= 2754915.98957811,
                            X= 476456.06826857
                        },
                        new Point2D{
                            Y= 2822295.133214,
                            X= 557463.43710115
                        },
                        new Point2D{
                            Y= 2772793.28116736,
                            X= 658343.404961133
                        },
                        new Point2D{
                            Y= 2844691.45420129,
                            X= 886760.652637347
                        },
                        new Point2D{
                            Y= 2807138.33514438,
                            X= 943423.425118805
                        },
                        new Point2D{
                            Y= 2662024.11856233,
                            X= 18418.8067302062
                        },
                        new Point2D{
                            Y= 2663586.26199006,
                            X= 177481.053361353
                        },
                        new Point2D{
                            Y= 2709676.47335934,
                            X= 255020.70915319
                        },
                        new Point2D{
                            Y= 2727489.45631578,
                            X= 352799.332235946
                        },
                        new Point2D{
                            Y= 2663296.58097299,
                            X= 532908.75690764
                        },
                        new Point2D{
                            Y= 2735201.31001107,
                            X= 802909.606638045
                        },
                        new Point2D{
                            Y= 2760413.64414772,
                            X= 998128.571597744
                        },
                        new Point2D{
                            Y= 3721063.11897967,
                            X= 1096537.39352312
                        },
                        new Point2D{
                            Y= 3806009.76636672,
                            X= 1264114.54213764
                        },
                        new Point2D{
                            Y= 3659897.89147288,
                            X= 977303.165355995
                        },
                        new Point2D{
                            Y= 3667119.12693735,
                            X= 1276315.61125214
                        },
                        new Point2D{
                            Y= 3702016.02329532,
                            X= 1383256.45824885
                        },
                        new Point2D{
                            Y= 3553877.77738901,
                            X= 993474.608174142
                        },
                        new Point2D{
                            Y= 3468372.02258786,
                            X= 988812.470727333
                        },
                        new Point2D{
                            Y= 3574413.54323796,
                            X= 1136200.78538739
                        },
                        new Point2D{
                            Y= 3485554.36380323,
                            X= 1280217.3139514
                        },
                        new Point2D{
                            Y= 3602651.62845188,
                            X= 1404852.66645533
                        },
                        new Point2D{
                            Y= 3532437.89455206,
                            X= 1534975.70049492
                        },
                        new Point2D{
                            Y= 3389316.83744639,
                            X= 1058628.67769239
                        },
                        new Point2D{
                            Y= 3451596.58641825,
                            X= 1137507.28023503
                        },
                        new Point2D{
                            Y= 3388043.07507577,
                            X= 1273287.33287609
                        },
                        new Point2D{
                            Y= 3431490.1846496,
                            X= 1352440.04337153
                        },
                        new Point2D{
                            Y= 3404118.13654751,
                            X= 1446840.44101627
                        },
                        new Point2D{
                            Y= 3430248.27034911,
                            X= 1537904.45771578
                        },
                        new Point2D{
                            Y= 3300696.23828829,
                            X= 1138696.00897506
                        },
                        new Point2D{
                            Y= 3269372.40205119,
                            X= 1247991.0128512
                        },
                        new Point2D{
                            Y= 3310808.88154252,
                            X= 1368572.04179806
                        },
                        new Point2D{
                            Y= 3307745.87523641,
                            X= 1436282.30060329
                        },
                        new Point2D{
                            Y= 3183057.72978458,
                            X= 1050098.71440737
                        },
                        new Point2D{
                            Y= 3165123.62391299,
                            X= 1169757.52866603
                        },
                        new Point2D{
                            Y= 3247205.08430535,
                            X= 1508596.80461907
                        },
                        new Point2D{
                            Y= 3220867.3050062,
                            X= 1623292.93924163
                        },
                        new Point2D{
                            Y= 3072906.12802057,
                            X= 1055834.05376887
                        },
                        new Point2D{
                            Y= 3054259.10311613,
                            X= 1185083.1453954
                        },
                        new Point2D{
                            Y= 3153271.28341821,
                            X= 1336444.52037402
                        },
                        new Point2D{
                            Y= 3106608.29535023,
                            X= 1442694.06935961
                        },
                        new Point2D{
                            Y= 3162114.31669025,
                            X= 1532059.54674761
                        },
                        new Point2D{
                            Y= 3068112.28164598,
                            X= 1522546.41646987
                        },
                        new Point2D{
                            Y= 2968163.18255175,
                            X= 1139742.59881217
                        },
                        new Point2D{
                            Y= 2950198.46071246,
                            X= 1222724.73515578
                        },
                        new Point2D{
                            Y= 3023025.03384796,
                            X= 1249618.60194525
                        },
                        new Point2D{
                            Y= 3028386.49699655,
                            X= 1317873.81676608
                        },
                        new Point2D{
                            Y= 2987528.20626134,
                            X= 1488901.19639095
                        },
                        new Point2D{
                            Y= 2883359.91153971,
                            X= 1118210.83117724
                        },
                        new Point2D{
                            Y= 2883598.58787324,
                            X= 1301055.07382683
                        },
                        new Point2D{
                            Y= 2836475.40569418,
                            X= 1419701.42834733
                        },
                        new Point2D{
                            Y= 2958458.5860529,
                            X= 1543326.83079009
                        },
                        new Point2D{
                            Y= 2773249.97485878,
                            X= 1134190.44287698
                        },
                        new Point2D{
                            Y= 2797825.85798144,
                            X= 1230215.39264762
                        },
                        new Point2D{
                            Y= 2698499.26537307,
                            X= 1210454.4310213
                        },
                        new Point2D{
                            Y= 2779877.97637571,
                            X= 1308155.99447455
                        },
                        new Point2D{
                            Y= 2781523.46641634,
                            X= 1478372.53112989
                        },
                        new Point2D{
                            Y= 2780955.25640357,
                            X= 1695729.51000478
                        },
                        new Point2D{
                            Y= 2510740.12762428,
                            X= 6799.32802877793
                        },
                        new Point2D{
                            Y= 2585502.07350235,
                            X= 308869.62466418
                        },
                        new Point2D{
                            Y= 2551956.45893858,
                            X= 447221.767184735
                        },
                        new Point2D{
                            Y= 2541072.16723061,
                            X= 561536.925382477
                        },
                        new Point2D{
                            Y= 2617749.62086522,
                            X= 746443.966797166
                        },
                        new Point2D{
                            Y= 2616622.64409527,
                            X= 870118.731926645
                        },
                        new Point2D{
                            Y= 2526117.35551937,
                            X= 871138.446539101
                        },
                        new Point2D{
                            Y= 2589894.81075705,
                            X= 962572.713231698
                        },
                        new Point2D{
                            Y= 2666076.85000705,
                            X= 1073650.66836698
                        },
                        new Point2D{
                            Y= 2595916.05090011,
                            X= 1127265.84789501
                        },
                        new Point2D{
                            Y= 2643960.95299855,
                            X= 1322781.05891515
                        },
                        new Point2D{
                            Y= 2690579.35813788,
                            X= 1625448.05035969
                        },
                        new Point2D{
                            Y= 2439386.41098449,
                            X= 85592.5201350268
                        },
                        new Point2D{
                            Y= 2493652.93430166,
                            X= 163473.376908244
                        },
                        new Point2D{
                            Y= 2450556.72116492,
                            X= 521986.400406256
                        },
                        new Point2D{
                            Y= 2466884.14656935,
                            X= 646186.918019999
                        },
                        new Point2D{
                            Y= 2425626.31896125,
                            X= 767653.131214962
                        },
                        new Point2D{
                            Y= 2447486.46961931,
                            X= 857239.359467968
                        },
                        new Point2D{
                            Y= 2522243.18899328,
                            X= 989620.395439375
                        },
                        new Point2D{
                            Y= 2507730.91636747,
                            X= 1197469.02378431
                        },
                        new Point2D{
                            Y= 2515065.5362656,
                            X= 1575725.75755317
                        },
                        new Point2D{
                            Y= 2711088.22388368,
                            X= 1724911.11615994
                        },
                        new Point2D{
                            Y= 2321657.01922523,
                            X= 192226.955334629
                        },
                        new Point2D{
                            Y= 2358215.7463019,
                            X= 333125.849076423
                        },
                        new Point2D{
                            Y= 2340110.43037654,
                            X= 616071.11968716
                        },
                        new Point2D{
                            Y= 2386868.04824598,
                            X= 942101.979933059
                        },
                        new Point2D{
                            Y= 2427051.46053508,
                            X= 1069925.25071151
                        },
                        new Point2D{
                            Y= 2552373.86167019,
                            X= 1691516.60435959
                        },
                        new Point2D{
                            Y= 2284684.51680657,
                            X= 377265.300760663
                        },
                        new Point2D{
                            Y= 2232072.61508466,
                            X= 433410.421562829
                        },
                        new Point2D{
                            Y= 2212966.14726872,
                            X= 567501.528117173
                        },
                        new Point2D{
                            Y= 2294192.7665261,
                            X= 726918.621760921
                        },
                        new Point2D{
                            Y= 2285821.69523238,
                            X= 811372.65703444
                        },
                        new Point2D{
                            Y= 2083802.1431006,
                            X= 569203.736194104
                        },
                        new Point2D{
                            Y= 1974178.47564884,
                            X= 388581.639089637
                        },
                        new Point2D{
                            Y= 2023621.24785655,
                            X= 490290.131491742
                        },
                        new Point2D{
                            Y= 1997704.05015512,
                            X= 586386.72889879
                        },
                        new Point2D{
                            Y= 1884474.06621417,
                            X= 489483.436576429
                        }
                   },
                ZValues = new double[] { -3, -2, 0, -1, -3, 0, 1, 0, 1, 1, 1, 2, 0, 3, -2, 5, 3, 2, 4, 6, 5, 5, 5, 2, 7, 5, 3, 4, 5, 4, 6, 5, 4, 8, 5, 9, 4, 4, 9, 6, 10, 8, 8, 11, -4, 15, 12, 12, 12, 7, 13, 11, 12, 12, 12, 13, 13, 11, 4, 12, 5, 10, 10, 5, 10, 10, 8, 8, 8, 10, 3, 8, 9, 5, 2, 5, 0, 1, 6, 4, 6, 11, -5, 1, 4, 5, 2, 6, 2, 6, 6, 6, 5, 4, 8, 5, 9, 10, 9, 8, 7, 9, -1, 8, 10, 9, 10, 14, 10, 9, 9, 10, 12, 10, 15, 11, 11, 13, 15, 9, 9, 11, 15, 12, 15, 3, 8, 6, 7, 5, 4, 4, 6, 8, 8, 7, 4, 3, 8, 8, 8, 7, 6, 6, 8, 6, 10, 10, 9, 6, 8, 3, 10, 11, 10, 10, 10, 8, 10, 13, 13, 12, 11, 14, 13, 12, 14, 13, 13, 12, 14, 15, 6, 13, 13, 13, 13, 14, 14, 14, 14, 1, 1, 0, -3, 0, 4, 0, 2, 7, 7, 9, 3, 6, 0, -4, 1, -2, 4, -3, -1, 0, 2, 3, 6, 15, 2, 4, 8, 7, 6, 1, 8, 9, 2, 6, 15, 17, 13, 4, 17, 17, 9, 5, 7, 3, 12, 6, 9, 18, 12, 17, 12, 13, 15, 13, 11, 15, 17, 15, 21, 17, 16, 15, 21, 15, 18, 20, 23, 19, 24, 22, 19, 19, 12, 14, 15, 7, 13, 12, 15, 15, 15, 15, 15, 16, 15, 16, 16, 15, 16, 17, 17, 17, 17, 17, 17, 17, 17, 16, 17, 17, 17, 18, 18, 17, 18, 17, 18, 8, 15, 17, 18, 17, 13, 16, 18, 17, 17, 12, 18, 19, 15, 15, 17, 17, 18, 17, 19, 15, 20, 15, 18, 19, 19, 20, 15, 14, 15, 15, 15, 16, 16, 16, 16, 15, 16, 16, 17, 16, 16, 17, 17, 17, 8, 9, 17, 12, 18, 17, 17, 18, 19, 18, 18, 15, 18, 18, 18, 12, 18, 19, 19, 20, 20, 18, 19, 20, 20, 13, 20, 26, 17, 21, 21, 20, 20, 20, 21, 20, 19, 22, 21, 25.2, 19, 22, 22, 21, 23, 22, 22, 22, 23, 24, 23, 22, 23, 23, 23, 25, 23, 23, 24, 23, 23, 25, 25, 24, 25, 26 },
                //ResultSetting = new DataReturnOption
                //{
                //    MaxRecordCount = 1000,
                //    //DatasetName = "iso2@Interpolation",
                //    //IsOverwrite = true
                //},
                MaxReturnRecordCount = 100,
                ParametersSetting = new SurfaceAnalystParametersSetting
                {
                    DatumValue = -5,
                    Interval = 1,
                    ResampleTolerance = 0.7,
                    SmoothMethod = SmoothMethod.BSPLINE,
                    Smoothness = 3,
                }
            };

            var geometryIsolineService = new SurfaceAnalystService(url);
            geometryIsolineService.ProcessAsync(parameters);
            geometryIsolineService.ProcessCompleted += new EventHandler<SurfaceAnalystEventArgs>(geometryIsolineService_ProcessCompleted);
            geometryIsolineService.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(geometryIsolineService_Failed);
        }

        private void geometryIsolineService_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
        }

        private void geometryIsolineService_ProcessCompleted(object sender, SurfaceAnalystEventArgs e)
        {
            this.resultLayer.AddFeatureSet(e.Result.Recordset.Features);
        }

        //geometry提取等值面
        private void geometryIsoRegion_Click(object sender, RoutedEventArgs e)
        {
            var parameters = new GeometrySurfaceAnalystParameters
            {
                SurfaceAnalystMethod = SurfaceAnalystMethod.ISOREGION,
                Resolution = 3000,
                Points = new Point2DCollection
                   {
                        new Point2D{
                            Y= 5846399.01175416,
                            X= 1210581.3465131
                        },
                        new Point2D{
                            Y= 5806144.68366852,
                            X= 1374568.19688557
                        },
                        new Point2D{
                            Y= 5770737.83129165,
                            X= 1521370.85300054
                        },
                        new Point2D{
                            Y= 5528199.92958311,
                            X= 1095631.45977217
                        },
                        new Point2D{
                            Y= 5570741.49064607,
                            X= 1198626.21785984
                        },
                        new Point2D{
                            Y= 5593290.50255359,
                            X= 1373169.38077361
                        },
                        new Point2D{
                            Y= 5627352.64228498,
                            X= 1612502.82819101
                        },
                        new Point2D{
                            Y= 5411257.38832255,
                            X= 1081393.24523065
                        },
                        new Point2D{
                            Y= 5458415.29448267,
                            X= 1369469.88462763
                        },
                        new Point2D{
                            Y= 5476577.89402985,
                            X= 1479703.15553903
                        },
                        new Point2D{
                            Y= 5538359.53266899,
                            X= 1625450.59309043
                        },
                        new Point2D{
                            Y= 5322253.55595858,
                            X= 874680.756098034
                        },
                        new Point2D{
                            Y= 5387691.35135605,
                            X= 1247212.60467642
                        },
                        new Point2D{
                            Y= 5365829.12376669,
                            X= 1552293.42507508
                        },
                        new Point2D{
                            Y= 5189236.97383116,
                            X= 1129966.26952038
                        },
                        new Point2D{
                            Y= 5263941.13976024,
                            X= 1421971.51732685
                        },
                        new Point2D{
                            Y= 5316511.81462062,
                            X= 1646535.17129977
                        },
                        new Point2D{
                            Y= 5380482.7734873,
                            X= 1781263.73730004
                        },
                        new Point2D{
                            Y= 5387628.37304574,
                            X= 2020110.74786557
                        },
                        new Point2D{
                            Y= 5149389.37335088,
                            X= 1404994.6955034
                        },
                        new Point2D{
                            Y= 5175828.87663573,
                            X= 1548389.26860757
                        },
                        new Point2D{
                            Y= 5309279.1724014,
                            X= 1907367.43149643
                        },
                        new Point2D{
                            Y= 5293521.85741038,
                            X= 2062286.6638636
                        },
                        new Point2D{
                            Y= 4976125.83982239,
                            X= 927814.758846577
                        },
                        new Point2D{
                            Y= 5026632.56869923,
                            X= 1543409.98087479
                        },
                        new Point2D{
                            Y= 5130288.82705181,
                            X= 1672545.4937333
                        },
                        new Point2D{
                            Y= 5188386.30774339,
                            X= 1815170.61581664
                        },
                        new Point2D{
                            Y= 5093043.21216217,
                            X= 1776810.62037654
                        },
                        new Point2D{
                            Y= 5157034.50256423,
                            X= 2000720.93926822
                        },
                        new Point2D{
                            Y= 5251089.30125404,
                            X= 2137211.95629998
                        },
                        new Point2D{
                            Y= 5332054.20434767,
                            X= -1384684.21976224
                        },
                        new Point2D{
                            Y= 5275056.3083835,
                            X= -1266592.67195803
                        },
                        new Point2D{
                            Y= 5175286.30171108,
                            X= -1173120.37547033
                        },
                        new Point2D{
                            Y= 5241172.93384647,
                            X= -1666090.72204382
                        },
                        new Point2D{
                            Y= 5203815.90203324,
                            X= -1462060.04834705
                        },
                        new Point2D{
                            Y= 5089465.30512548,
                            X= -1553103.06288549
                        },
                        new Point2D{
                            Y= 4985329.13408486,
                            X= -1122827.89723113
                        },
                        new Point2D{
                            Y= 5084477.14390444,
                            X= -1860653.07373091
                        },
                        new Point2D{
                            Y= 5012320.00449773,
                            X= -1725720.73925144
                        },
                        new Point2D{
                            Y= 4847254.4566494,
                            X= -1220678.02426651
                        },
                        new Point2D{
                            Y= 4967869.57917987,
                            X= -1863459.09401226
                        },
                        new Point2D{
                            Y= 4846715.02903969,
                            X= -1376674.68719707
                        },
                        new Point2D{
                            Y= 4749993.79908471,
                            X= -1503256.39597018
                        },
                        new Point2D{
                            Y= 4733437.65622082,
                            X= -1062382.73304631
                        },
                        new Point2D{
                            Y= 4817393.24951001,
                            X= -1666642.31853787
                        },
                        new Point2D{
                            Y= 4731665.18199693,
                            X= -1268721.06517955
                        },
                        new Point2D{
                            Y= 4691431.37946197,
                            X= -1784339.07456816
                        },
                        new Point2D{
                            Y= 4643802.53247814,
                            X= -1537407.40647844
                        },
                        new Point2D{
                            Y= 4595707.88157582,
                            X= -2419318.16518532
                        },
                        new Point2D{
                            Y= 4697780.17446741,
                            X= -2175064.94799138
                        },
                        new Point2D{
                            Y= 4571808.9805013,
                            X= -2199394.38191528
                        },
                        new Point2D{
                            Y= 4596066.82343988,
                            X= -1978125.62843053
                        },
                        new Point2D{
                            Y= 4496517.04647146,
                            X= -1432929.65674657
                        },
                        new Point2D{
                            Y= 4311774.01178654,
                            X= -1425142.47552866
                        },
                        new Point2D{
                            Y= 4452868.31749155,
                            X= -2347377.8808384
                        },
                        new Point2D{
                            Y= 4340268.27463545,
                            X= -2287997.73283675
                        },
                        new Point2D{
                            Y= 4250483.4258309,
                            X= -2163460.02757897
                        },
                        new Point2D{
                            Y= 4263739.81754219,
                            X= -1828785.52128088
                        },
                        new Point2D{
                            Y= 4188669.31509868,
                            X= -1212229.28247201
                        },
                        new Point2D{
                            Y= 4183182.83873892,
                            X= -2025606.92718856
                        },
                        new Point2D{
                            Y= 4710813.79158798,
                            X= -825209.394421958
                        },
                        new Point2D{
                            Y= 4670956.24413595,
                            X= -925452.706876877
                        },
                        new Point2D{
                            Y= 4526184.57635418,
                            X= -321480.003370163
                        },
                        new Point2D{
                            Y= 4529087.78970059,
                            X= -651959.49175178
                        },
                        new Point2D{
                            Y= 4456951.66969839,
                            X= -217002.751793607
                        },
                        new Point2D{
                            Y= 4361674.17388149,
                            X= -862966.032372303
                        },
                        new Point2D{
                            Y= 4356696.39135096,
                            X= -665822.612619922
                        },
                        new Point2D{
                            Y= 4318695.18826926,
                            X= -16755.8954939763
                        },
                        new Point2D{
                            Y= 4291518.68150134,
                            X= -548534.027519153
                        },
                        new Point2D{
                            Y= 4214198.76083203,
                            X= -281463.84777592
                        },
                        new Point2D{
                            Y= 4217114.78997084,
                            X= -993942.17692933
                        },
                        new Point2D{
                            Y= 4188310.29690308,
                            X= -388864.100827363
                        },
                        new Point2D{
                            Y= 4147044.70045506,
                            X= -163911.282778357
                        },
                        new Point2D{
                            Y= 3994669.65229173,
                            X= -989326.169926525
                        },
                        new Point2D{
                            Y= 4097277.46806724,
                            X= -831169.132434991
                        },
                        new Point2D{
                            Y= 4027790.2582361,
                            X= -663189.775811282
                        },
                        new Point2D{
                            Y= 4008890.15249491,
                            X= -423251.717948863
                        },
                        new Point2D{
                            Y= 3985439.0098199,
                            X= -185918.900601112
                        },
                        new Point2D{
                            Y= 3940087.84301485,
                            X= -887752.642321781
                        },
                        new Point2D{
                            Y= 3902840.70822795,
                            X= -607958.642974107
                        },
                        new Point2D{
                            Y= 3933468.62385761,
                            X= -284995.314627413
                        },
                        new Point2D{
                            Y= 3854005.31553064,
                            X= -98784.4216291984
                        },
                        new Point2D{
                            Y= 3823156.34762407,
                            X= -1062959.98443942
                        },
                        new Point2D{
                            Y= 3808807.65507124,
                            X= -378120.522440156
                        },
                        new Point2D{
                            Y= 3778115.4734166,
                            X= 0
                        },
                        new Point2D{
                            Y= 4730534.72017257,
                            X= 555675.893973636
                        },
                        new Point2D{
                            Y= 4852624.17143594,
                            X= 719410.765951757
                        },
                        new Point2D{
                            Y= 4596256.33005592,
                            X= 416086.892674244
                        },
                        new Point2D{
                            Y= 4791746.94877328,
                            X= 788837.859700444
                        },
                        new Point2D{
                            Y= 4453008.15232032,
                            X= 115404.754046287
                        },
                        new Point2D{
                            Y= 4596011.98746132,
                            X= 641143.983066076
                        },
                        new Point2D{
                            Y= 4481738.3587981,
                            X= 288969.735496225
                        },
                        new Point2D{
                            Y= 4503841.15536681,
                            X= 445513.596625804
                        },
                        new Point2D{
                            Y= 4547533.1139069,
                            X= 735284.890774623
                        },
                        new Point2D{
                            Y= 4410713.69270361,
                            X= 554568.91407516
                        },
                        new Point2D{
                            Y= 4443600.75131925,
                            X= 667140.230636665
                        },
                        new Point2D{
                            Y= 4340461.47812997,
                            X= 697974.317012174
                        },
                        new Point2D{
                            Y= 4275630.55404934,
                            X= 63161.0494544051
                        },
                        new Point2D{
                            Y= 4386963.12826204,
                            X= 200844.704710507
                        },
                        new Point2D{
                            Y= 4202007.50817714,
                            X= 253524.487125277
                        },
                        new Point2D{
                            Y= 4291562.13846185,
                            X= 419214.550976308
                        },
                        new Point2D{
                            Y= 4246332.74161325,
                            X= 520391.418470442
                        },
                        new Point2D{
                            Y= 4212339.56506825,
                            X= 724422.341977581
                        },
                        new Point2D{
                            Y= 4319799.30743599,
                            X= 803872.440458256
                        },
                        new Point2D{
                            Y= 4129148.23944623,
                            X= 104259.642156941
                        },
                        new Point2D{
                            Y= 4109815.76603993,
                            X= 403953.288135979
                        },
                        new Point2D{
                            Y= 4182364.9195765,
                            X= 658420.237399424
                        },
                        new Point2D{
                            Y= 4116114.81460488,
                            X= 810569.047871445
                        },
                        new Point2D{
                            Y= 4015665.70312797,
                            X= 57888.6128501599
                        },
                        new Point2D{
                            Y= 4051893.79873069,
                            X= 207560.532887203
                        },
                        new Point2D{
                            Y= 3955829.39507851,
                            X= 276969.255214574
                        },
                        new Point2D{
                            Y= 4033619.21146623,
                            X= 529235.890586695
                        },
                        new Point2D{
                            Y= 4074202.839648,
                            X= 652385.210996039
                        },
                        new Point2D{
                            Y= 3996377.3703958,
                            X= 696258.054838688
                        },
                        new Point2D{
                            Y= 4008021.5303752,
                            X= 828160.952774673
                        },
                        new Point2D{
                            Y= 3924572.25012626,
                            X= 395141.805401175
                        },
                        new Point2D{
                            Y= 3987617.53255572,
                            X= 604975.099446582
                        },
                        new Point2D{
                            Y= 3874146.39928132,
                            X= 574491.97093995
                        },
                        new Point2D{
                            Y= 3900010.46986606,
                            X= 826688.565478772
                        },
                        new Point2D{
                            Y= 3798192.21543255,
                            X= 148384.115113218
                        },
                        new Point2D{
                            Y= 3820764.88362328,
                            X= 233881.007408209
                        },
                        new Point2D{
                            Y= 3751682.66734236,
                            X= 364131.993837199
                        },
                        new Point2D{
                            Y= 3754910.35564437,
                            X= 538899.222473607
                        },
                        new Point2D{
                            Y= 3813977.85449175,
                            X= 658807.463567117
                        },
                        new Point2D{
                            Y= 3806050.10370921,
                            X= 792225.28442553
                        },
                        new Point2D{
                            Y= 4878613.66968738,
                            X= 989879.451014638
                        },
                        new Point2D{
                            Y= 4913952.11370657,
                            X= 1247443.67571332
                        },
                        new Point2D{
                            Y= 4831270.94477481,
                            X= 1140083.25107815
                        },
                        new Point2D{
                            Y= 4921315.05493549,
                            X= 1492086.6167465
                        },
                        new Point2D{
                            Y= 5052757.70771331,
                            X= 1918509.27530086
                        },
                        new Point2D{
                            Y= 5063796.81301938,
                            X= 2042017.29979237
                        },
                        new Point2D{
                            Y= 4793681.90422199,
                            X= 877851.099733359
                        },
                        new Point2D{
                            Y= 4774006.99534911,
                            X= 1040808.50860678
                        },
                        new Point2D{
                            Y= 4825058.94082724,
                            X= 1372389.78541081
                        },
                        new Point2D{
                            Y= 4809389.80096441,
                            X= 1543742.52786379
                        },
                        new Point2D{
                            Y= 4902262.258834,
                            X= 1597042.34358779
                        },
                        new Point2D{
                            Y= 4895476.54413311,
                            X= 1842559.94348831
                        },
                        new Point2D{
                            Y= 4599996.60493905,
                            X= 932329.791285148
                        },
                        new Point2D{
                            Y= 4634929.70981753,
                            X= 1130375.65588701
                        },
                        new Point2D{
                            Y= 4663722.19893533,
                            X= 1271390.10186989
                        },
                        new Point2D{
                            Y= 4697792.92263294,
                            X= 1416741.02877937
                        },
                        new Point2D{
                            Y= 4699147.54747854,
                            X= 1613992.86446047
                        },
                        new Point2D{
                            Y= 4827335.18909809,
                            X= 1738675.30026215
                        },
                        new Point2D{
                            Y= 4866923.04130859,
                            X= 1954423.57165034
                        },
                        new Point2D{
                            Y= 4493384.04876875,
                            X= 958554.984492026
                        },
                        new Point2D{
                            Y= 4584793.26261634,
                            X= 1039735.87537795
                        },
                        new Point2D{
                            Y= 4573368.71974201,
                            X= 1264987.12544988
                        },
                        new Point2D{
                            Y= 4535682.89893446,
                            X= 1326693.92599423
                        },
                        new Point2D{
                            Y= 4635574.88352796,
                            X= 1504197.34322534
                        },
                        new Point2D{
                            Y= 4700294.42098123,
                            X= 1780949.13158124
                        },
                        new Point2D{
                            Y= 4609648.65600361,
                            X= 1736296.85726129
                        },
                        new Point2D{
                            Y= 4681629.34942827,
                            X= 1892050.86697712
                        },
                        new Point2D{
                            Y= 4429115.4946153,
                            X= 819736.784093866
                        },
                        new Point2D{
                            Y= 4391346.67602316,
                            X= 875265.46321163
                        },
                        new Point2D{
                            Y= 4480718.86504277,
                            X= 1069883.28758263
                        },
                        new Point2D{
                            Y= 4426733.6720174,
                            X= 1161193.55835407
                        },
                        new Point2D{
                            Y= 4499741.2075763,
                            X= 1429545.72668076
                        },
                        new Point2D{
                            Y= 4544119.35480714,
                            X= 1634202.9161613
                        },
                        new Point2D{
                            Y= 4462966.02017487,
                            X= 1612068.91063451
                        },
                        new Point2D{
                            Y= 4332966.0782395,
                            X= 963309.010178802
                        },
                        new Point2D{
                            Y= 4261654.54843649,
                            X= 1032901.83534834
                        },
                        new Point2D{
                            Y= 4335564.77584577,
                            X= 1105939.22516913
                        },
                        new Point2D{
                            Y= 4317860.35764764,
                            X= 1170895.84077513
                        },
                        new Point2D{
                            Y= 4217404.24906042,
                            X= 895159.921777312
                        },
                        new Point2D{
                            Y= 4172037.28506849,
                            X= 1013674.17742552
                        },
                        new Point2D{
                            Y= 4294064.32320943,
                            X= 1410882.22311226
                        },
                        new Point2D{
                            Y= 4065800.48286813,
                            X= 981182.910627941
                        },
                        new Point2D{
                            Y= 4085762.36636633,
                            X= 1083753.04706252
                        },
                        new Point2D{
                            Y= 4132090.75205249,
                            X= 1318132.26920098
                        },
                        new Point2D{
                            Y= 4142586.73480868,
                            X= 1529070.00567553
                        },
                        new Point2D{
                            Y= 3901432.30854768,
                            X= 934688.365342644
                        },
                        new Point2D{
                            Y= 3979771.31620484,
                            X= 1055867.46884087
                        },
                        new Point2D{
                            Y= 3941036.30757412,
                            X= 1065036.66719561
                        },
                        new Point2D{
                            Y= 3945317.28258398,
                            X= 1157932.98860132
                        },
                        new Point2D{
                            Y= 4021239.52247673,
                            X= 1239207.37028499
                        },
                        new Point2D{
                            Y= 3959852.55882132,
                            X= 1350795.41035969
                        },
                        new Point2D{
                            Y= 4049571.66639741,
                            X= 1410747.90968575
                        },
                        new Point2D{
                            Y= 3812004.0719844,
                            X= 930774.762858088
                        },
                        new Point2D{
                            Y= 3861707.05045625,
                            X= 1052333.00528697
                        },
                        new Point2D{
                            Y= 3820762.94886215,
                            X= 1192606.77848034
                        },
                        new Point2D{
                            Y= 3878446.39774496,
                            X= 1291276.59054776
                        },
                        new Point2D{
                            Y= 3740457.13221034,
                            X= -2280711.28933375
                        },
                        new Point2D{
                            Y= 3610835.64657684,
                            X= -1898494.89479583
                        },
                        new Point2D{
                            Y= 3432858.02097836,
                            X= -1399626.09108375
                        },
                        new Point2D{
                            Y= 3525877.24680728,
                            X= -1283933.48926119
                        },
                        new Point2D{
                            Y= 3417074.9256984,
                            X= -1207841.10592021
                        },
                        new Point2D{
                            Y= 3472059.71094418,
                            X= -2235091.75387382
                        },
                        new Point2D{
                            Y= 3405191.58105513,
                            X= -1535719.26165889
                        },
                        new Point2D{
                            Y= 3317974.06375021,
                            X= -1313233.1022214
                        },
                        new Point2D{
                            Y= 3215456.02366254,
                            X= -1668231.07906761
                        },
                        new Point2D{
                            Y= 3213100.7340777,
                            X= -1543300.50386288
                        },
                        new Point2D{
                            Y= 3226788.4638176,
                            X= -1322861.96368399
                        },
                        new Point2D{
                            Y= 3174717.87547459,
                            X= -1726275.53533564
                        },
                        new Point2D{
                            Y= 3071026.63935862,
                            X= -1213998.10397285
                        },
                        new Point2D{
                            Y= 3042582.07956807,
                            X= -1551388.82660322
                        },
                        new Point2D{
                            Y= 3718278.01215987,
                            X= -1134893.92611012
                        },
                        new Point2D{
                            Y= 3541408.55446127,
                            X= -891441.225776796
                        },
                        new Point2D{
                            Y= 3675701.19714148,
                            X= -834256.439798467
                        },
                        new Point2D{
                            Y= 3540464.43959992,
                            X= -732998.03015512
                        },
                        new Point2D{
                            Y= 3746189.56692848,
                            X= -608366.655726914
                        },
                        new Point2D{
                            Y= 3606814.82794058,
                            X= -487052.674114973
                        },
                        new Point2D{
                            Y= 3709929.54664069,
                            X= -305817.396472208
                        },
                        new Point2D{
                            Y= 3576785.13185038,
                            X= -185572.123301419
                        },
                        new Point2D{
                            Y= 3736802.08201411,
                            X= -188273.034228444
                        },
                        new Point2D{
                            Y= 3671256.44640523,
                            X= -88798.5526841957
                        },
                        new Point2D{
                            Y= 3554207.61323663,
                            X= -7623.30418290906
                        },
                        new Point2D{
                            Y= 3442294.00160218,
                            X= -1043182.71934844
                        },
                        new Point2D{
                            Y= 3372470.13339255,
                            X= -879746.005973445
                        },
                        new Point2D{
                            Y= 3329846.46586159,
                            X= -735820.26243473
                        },
                        new Point2D{
                            Y= 3385774.17225212,
                            X= -598714.291633867
                        },
                        new Point2D{
                            Y= 3365061.37108834,
                            X= -467319.138920955
                        },
                        new Point2D{
                            Y= 3438511.67240308,
                            X= -432661.89096875
                        },
                        new Point2D{
                            Y= 3289222.03255561,
                            X= -365797.290833479
                        },
                        new Point2D{
                            Y= 3388770.17906844,
                            X= -257770.633540852
                        },
                        new Point2D{
                            Y= 3489340.01488981,
                            X= -225771.692979256
                        },
                        new Point2D{
                            Y= 3470604.75468223,
                            X= -132336.985354525
                        },
                        new Point2D{
                            Y= 3443468.92293677,
                            X= -44754.6223657837
                        },
                        new Point2D{
                            Y= 3336462.95672917,
                            X= -29663.1701911858
                        },
                        new Point2D{
                            Y= 3188348.36033678,
                            X= -562133.802732499
                        },
                        new Point2D{
                            Y= 3182395.10255378,
                            X= -451072.029089209
                        },
                        new Point2D{
                            Y= 3171691.27767082,
                            X= -190690.808347991
                        },
                        new Point2D{
                            Y= 3246944.60898054,
                            X= -93001.3198132454
                        },
                        new Point2D{
                            Y= 3189044.90691625,
                            X= -1018939.24320695
                        },
                        new Point2D{
                            Y= 3075883.66985843,
                            X= -452936.775008653
                        },
                        new Point2D{
                            Y= 3181664.96098998,
                            X= -288960.46159696
                        },
                        new Point2D{
                            Y= 3118781.22393754,
                            X= -159790.744577716
                        },
                        new Point2D{
                            Y= 3048061.66104169,
                            X= -728952.008541062
                        },
                        new Point2D{
                            Y= 3016310.11607334,
                            X= -593441.216272313
                        },
                        new Point2D{
                            Y= 3065512.79171986,
                            X= -337546.791497588
                        },
                        new Point2D{
                            Y= 3037253.5443433,
                            X= -38674.8588136952
                        },
                        new Point2D{
                            Y= 2870958.91798656,
                            X= -562922.85739458
                        },
                        new Point2D{
                            Y= 2940239.589357,
                            X= -267015.166073815
                        },
                        new Point2D{
                            Y= 2876002.74884915,
                            X= -126172.573115854
                        },
                        new Point2D{
                            Y= 2832711.95458169,
                            X= -472704.963798652
                        },
                        new Point2D{
                            Y= 2800972.85034979,
                            X= -272502.708102723
                        },
                        new Point2D{
                            Y= 2772709.84050023,
                            X= -170569.940210372
                        },
                        new Point2D{
                            Y= 2821578.0335128,
                            X= -70850.184314894
                        },
                        new Point2D{
                            Y= 2637288.51064395,
                            X= -655586.120477028
                        },
                        new Point2D{
                            Y= 2644172.18392656,
                            X= -587777.726745159
                        },
                        new Point2D{
                            Y= 2703298.89946865,
                            X= -482287.720412992
                        },
                        new Point2D{
                            Y= 2700248.29722294,
                            X= -313691.583453497
                        },
                        new Point2D{
                            Y= 2621928.10377776,
                            X= -349824.747787611
                        },
                        new Point2D{
                            Y= 2618547.89012006,
                            X= -233803.408535259
                        },
                        new Point2D{
                            Y= 2679316.75973283,
                            X= -115340.872386075
                        },
                        new Point2D{
                            Y= 2531477.86970731,
                            X= -728995.100717991
                        },
                        new Point2D{
                            Y= 2563094.64881326,
                            X= -125143.603172223
                        },
                        new Point2D{
                            Y= 2502934.899521,
                            X= -502247.561521754
                        },
                        new Point2D{
                            Y= 2358890.88240176,
                            X= -524954.107223773
                        },
                        new Point2D{
                            Y= 2292630.51177167,
                            X= -439585.374990592
                        },
                        new Point2D{
                            Y= 2377779.72650678,
                            X= -416990.30480708
                        },
                        new Point2D{
                            Y= 2445570.16048078,
                            X= -309745.082737913
                        },
                        new Point2D{
                            Y= 2232920.85836036,
                            X= -359916.399294494
                        },
                        new Point2D{
                            Y= 2352502.54855071,
                            X= -326398.332104555
                        },
                        new Point2D{
                            Y= 2436706.39270386,
                            X= -166103.912027098
                        },
                        new Point2D{
                            Y= 3688008.89570777,
                            X= 67600.2272072867
                        },
                        new Point2D{
                            Y= 3663480.15988555,
                            X= 192838.184529764
                        },
                        new Point2D{
                            Y= 3662810.59694284,
                            X= 355703.330753522
                        },
                        new Point2D{
                            Y= 3688204.18776202,
                            X= 458561.395295938
                        },
                        new Point2D{
                            Y= 3644173.48283529,
                            X= 547133.521977902
                        },
                        new Point2D{
                            Y= 3718679.28398731,
                            X= 671945.729762831
                        },
                        new Point2D{
                            Y= 3736696.43296549,
                            X= 777367.148298923
                        },
                        new Point2D{
                            Y= 3518527.47714298,
                            X= 186765.444956859
                        },
                        new Point2D{
                            Y= 3539458.56983606,
                            X= 696201.573074832
                        },
                        new Point2D{
                            Y= 3638944.59590014,
                            X= 865109.855108802
                        },
                        new Point2D{
                            Y= 3408250.65462685,
                            X= 282035.410131644
                        },
                        new Point2D{
                            Y= 3484719.53651323,
                            X= 371995.501741616
                        },
                        new Point2D{
                            Y= 3415764.87662477,
                            X= 536177.020615121
                        },
                        new Point2D{
                            Y= 3460331.70655059,
                            X= 617095.718836684
                        },
                        new Point2D{
                            Y= 3441411.77217682,
                            X= 719233.636417891
                        },
                        new Point2D{
                            Y= 3546714.9420924,
                            X= 827799.023170084
                        },
                        new Point2D{
                            Y= 3449748.54867299,
                            X= 839739.868053981
                        },
                        new Point2D{
                            Y= 3349980.98134904,
                            X= 90421.6594242863
                        },
                        new Point2D{
                            Y= 3351011.0721427,
                            X= 165247.267444322
                        },
                        new Point2D{
                            Y= 3309382.62069191,
                            X= 234913.696065381
                        },
                        new Point2D{
                            Y= 3299160.4264898,
                            X= 423508.672359737
                        },
                        new Point2D{
                            Y= 3329772.07689117,
                            X= 710677.759931878
                        },
                        new Point2D{
                            Y= 3352006.07650959,
                            X= 939912.922450867
                        },
                        new Point2D{
                            Y= 3260166.78454166,
                            X= 103889.72619136
                        },
                        new Point2D{
                            Y= 3252089.69762263,
                            X= 264733.745155264
                        },
                        new Point2D{
                            Y= 3212982.41711427,
                            X= 424247.109423719
                        },
                        new Point2D{
                            Y= 3269120.01253464,
                            X= 595211.439003019
                        },
                        new Point2D{
                            Y= 3233715.91139639,
                            X= 681507.113912423
                        },
                        new Point2D{
                            Y= 3280596.97028057,
                            X= 863121.036452202
                        },
                        new Point2D{
                            Y= 3124913.88382641,
                            X= 4790.18386941863
                        },
                        new Point2D{
                            Y= 3125950.54618129,
                            X= 140506.955285668
                        },
                        new Point2D{
                            Y= 3117269.89290298,
                            X= 495831.120641366
                        },
                        new Point2D{
                            Y= 3134080.45553659,
                            X= 775380.764486573
                        },
                        new Point2D{
                            Y= 3107858.7627186,
                            X= 922632.139487556
                        },
                        new Point2D{
                            Y= 3046593.54436373,
                            X= 41857.0362429994
                        },
                        new Point2D{
                            Y= 3056211.55619788,
                            X= 207498.128916585
                        },
                        new Point2D{
                            Y= 3047811.12765708,
                            X= 363960.379351961
                        },
                        new Point2D{
                            Y= 3014137.90043046,
                            X= 523881.777939532
                        },
                        new Point2D{
                            Y= 3086839.04362806,
                            X= 643823.784250282
                        },
                        new Point2D{
                            Y= 3002649.08852154,
                            X= 769827.483357505
                        },
                        new Point2D{
                            Y= 2869651.62869318,
                            X= 27872.9317484134
                        },
                        new Point2D{
                            Y= 2915984.37123388,
                            X= 184415.713127814
                        },
                        new Point2D{
                            Y= 2947341.33027515,
                            X= 317288.190737034
                        },
                        new Point2D{
                            Y= 2897186.39990223,
                            X= 459764.109022809
                        },
                        new Point2D{
                            Y= 2882905.72963603,
                            X= 636193.600186931
                        },
                        new Point2D{
                            Y= 2898971.75316899,
                            X= 756729.755392647
                        },
                        new Point2D{
                            Y= 2968738.55616601,
                            X= 916448.816089683
                        },
                        new Point2D{
                            Y= 2890565.81626988,
                            X= 976711.338986039
                        },
                        new Point2D{
                            Y= 2791279.37379748,
                            X= 171902.057106717
                        },
                        new Point2D{
                            Y= 2839146.61113946,
                            X= 361998.773794132
                        },
                        new Point2D{
                            Y= 2754915.98957811,
                            X= 476456.06826857
                        },
                        new Point2D{
                            Y= 2822295.133214,
                            X= 557463.43710115
                        },
                        new Point2D{
                            Y= 2772793.28116736,
                            X= 658343.404961133
                        },
                        new Point2D{
                            Y= 2844691.45420129,
                            X= 886760.652637347
                        },
                        new Point2D{
                            Y= 2807138.33514438,
                            X= 943423.425118805
                        },
                        new Point2D{
                            Y= 2662024.11856233,
                            X= 18418.8067302062
                        },
                        new Point2D{
                            Y= 2663586.26199006,
                            X= 177481.053361353
                        },
                        new Point2D{
                            Y= 2709676.47335934,
                            X= 255020.70915319
                        },
                        new Point2D{
                            Y= 2727489.45631578,
                            X= 352799.332235946
                        },
                        new Point2D{
                            Y= 2663296.58097299,
                            X= 532908.75690764
                        },
                        new Point2D{
                            Y= 2735201.31001107,
                            X= 802909.606638045
                        },
                        new Point2D{
                            Y= 2760413.64414772,
                            X= 998128.571597744
                        },
                        new Point2D{
                            Y= 3721063.11897967,
                            X= 1096537.39352312
                        },
                        new Point2D{
                            Y= 3806009.76636672,
                            X= 1264114.54213764
                        },
                        new Point2D{
                            Y= 3659897.89147288,
                            X= 977303.165355995
                        },
                        new Point2D{
                            Y= 3667119.12693735,
                            X= 1276315.61125214
                        },
                        new Point2D{
                            Y= 3702016.02329532,
                            X= 1383256.45824885
                        },
                        new Point2D{
                            Y= 3553877.77738901,
                            X= 993474.608174142
                        },
                        new Point2D{
                            Y= 3468372.02258786,
                            X= 988812.470727333
                        },
                        new Point2D{
                            Y= 3574413.54323796,
                            X= 1136200.78538739
                        },
                        new Point2D{
                            Y= 3485554.36380323,
                            X= 1280217.3139514
                        },
                        new Point2D{
                            Y= 3602651.62845188,
                            X= 1404852.66645533
                        },
                        new Point2D{
                            Y= 3532437.89455206,
                            X= 1534975.70049492
                        },
                        new Point2D{
                            Y= 3389316.83744639,
                            X= 1058628.67769239
                        },
                        new Point2D{
                            Y= 3451596.58641825,
                            X= 1137507.28023503
                        },
                        new Point2D{
                            Y= 3388043.07507577,
                            X= 1273287.33287609
                        },
                        new Point2D{
                            Y= 3431490.1846496,
                            X= 1352440.04337153
                        },
                        new Point2D{
                            Y= 3404118.13654751,
                            X= 1446840.44101627
                        },
                        new Point2D{
                            Y= 3430248.27034911,
                            X= 1537904.45771578
                        },
                        new Point2D{
                            Y= 3300696.23828829,
                            X= 1138696.00897506
                        },
                        new Point2D{
                            Y= 3269372.40205119,
                            X= 1247991.0128512
                        },
                        new Point2D{
                            Y= 3310808.88154252,
                            X= 1368572.04179806
                        },
                        new Point2D{
                            Y= 3307745.87523641,
                            X= 1436282.30060329
                        },
                        new Point2D{
                            Y= 3183057.72978458,
                            X= 1050098.71440737
                        },
                        new Point2D{
                            Y= 3165123.62391299,
                            X= 1169757.52866603
                        },
                        new Point2D{
                            Y= 3247205.08430535,
                            X= 1508596.80461907
                        },
                        new Point2D{
                            Y= 3220867.3050062,
                            X= 1623292.93924163
                        },
                        new Point2D{
                            Y= 3072906.12802057,
                            X= 1055834.05376887
                        },
                        new Point2D{
                            Y= 3054259.10311613,
                            X= 1185083.1453954
                        },
                        new Point2D{
                            Y= 3153271.28341821,
                            X= 1336444.52037402
                        },
                        new Point2D{
                            Y= 3106608.29535023,
                            X= 1442694.06935961
                        },
                        new Point2D{
                            Y= 3162114.31669025,
                            X= 1532059.54674761
                        },
                        new Point2D{
                            Y= 3068112.28164598,
                            X= 1522546.41646987
                        },
                        new Point2D{
                            Y= 2968163.18255175,
                            X= 1139742.59881217
                        },
                        new Point2D{
                            Y= 2950198.46071246,
                            X= 1222724.73515578
                        },
                        new Point2D{
                            Y= 3023025.03384796,
                            X= 1249618.60194525
                        },
                        new Point2D{
                            Y= 3028386.49699655,
                            X= 1317873.81676608
                        },
                        new Point2D{
                            Y= 2987528.20626134,
                            X= 1488901.19639095
                        },
                        new Point2D{
                            Y= 2883359.91153971,
                            X= 1118210.83117724
                        },
                        new Point2D{
                            Y= 2883598.58787324,
                            X= 1301055.07382683
                        },
                        new Point2D{
                            Y= 2836475.40569418,
                            X= 1419701.42834733
                        },
                        new Point2D{
                            Y= 2958458.5860529,
                            X= 1543326.83079009
                        },
                        new Point2D{
                            Y= 2773249.97485878,
                            X= 1134190.44287698
                        },
                        new Point2D{
                            Y= 2797825.85798144,
                            X= 1230215.39264762
                        },
                        new Point2D{
                            Y= 2698499.26537307,
                            X= 1210454.4310213
                        },
                        new Point2D{
                            Y= 2779877.97637571,
                            X= 1308155.99447455
                        },
                        new Point2D{
                            Y= 2781523.46641634,
                            X= 1478372.53112989
                        },
                        new Point2D{
                            Y= 2780955.25640357,
                            X= 1695729.51000478
                        },
                        new Point2D{
                            Y= 2510740.12762428,
                            X= 6799.32802877793
                        },
                        new Point2D{
                            Y= 2585502.07350235,
                            X= 308869.62466418
                        },
                        new Point2D{
                            Y= 2551956.45893858,
                            X= 447221.767184735
                        },
                        new Point2D{
                            Y= 2541072.16723061,
                            X= 561536.925382477
                        },
                        new Point2D{
                            Y= 2617749.62086522,
                            X= 746443.966797166
                        },
                        new Point2D{
                            Y= 2616622.64409527,
                            X= 870118.731926645
                        },
                        new Point2D{
                            Y= 2526117.35551937,
                            X= 871138.446539101
                        },
                        new Point2D{
                            Y= 2589894.81075705,
                            X= 962572.713231698
                        },
                        new Point2D{
                            Y= 2666076.85000705,
                            X= 1073650.66836698
                        },
                        new Point2D{
                            Y= 2595916.05090011,
                            X= 1127265.84789501
                        },
                        new Point2D{
                            Y= 2643960.95299855,
                            X= 1322781.05891515
                        },
                        new Point2D{
                            Y= 2690579.35813788,
                            X= 1625448.05035969
                        },
                        new Point2D{
                            Y= 2439386.41098449,
                            X= 85592.5201350268
                        },
                        new Point2D{
                            Y= 2493652.93430166,
                            X= 163473.376908244
                        },
                        new Point2D{
                            Y= 2450556.72116492,
                            X= 521986.400406256
                        },
                        new Point2D{
                            Y= 2466884.14656935,
                            X= 646186.918019999
                        },
                        new Point2D{
                            Y= 2425626.31896125,
                            X= 767653.131214962
                        },
                        new Point2D{
                            Y= 2447486.46961931,
                            X= 857239.359467968
                        },
                        new Point2D{
                            Y= 2522243.18899328,
                            X= 989620.395439375
                        },
                        new Point2D{
                            Y= 2507730.91636747,
                            X= 1197469.02378431
                        },
                        new Point2D{
                            Y= 2515065.5362656,
                            X= 1575725.75755317
                        },
                        new Point2D{
                            Y= 2711088.22388368,
                            X= 1724911.11615994
                        },
                        new Point2D{
                            Y= 2321657.01922523,
                            X= 192226.955334629
                        },
                        new Point2D{
                            Y= 2358215.7463019,
                            X= 333125.849076423
                        },
                        new Point2D{
                            Y= 2340110.43037654,
                            X= 616071.11968716
                        },
                        new Point2D{
                            Y= 2386868.04824598,
                            X= 942101.979933059
                        },
                        new Point2D{
                            Y= 2427051.46053508,
                            X= 1069925.25071151
                        },
                        new Point2D{
                            Y= 2552373.86167019,
                            X= 1691516.60435959
                        },
                        new Point2D{
                            Y= 2284684.51680657,
                            X= 377265.300760663
                        },
                        new Point2D{
                            Y= 2232072.61508466,
                            X= 433410.421562829
                        },
                        new Point2D{
                            Y= 2212966.14726872,
                            X= 567501.528117173
                        },
                        new Point2D{
                            Y= 2294192.7665261,
                            X= 726918.621760921
                        },
                        new Point2D{
                            Y= 2285821.69523238,
                            X= 811372.65703444
                        },
                        new Point2D{
                            Y= 2083802.1431006,
                            X= 569203.736194104
                        },
                        new Point2D{
                            Y= 1974178.47564884,
                            X= 388581.639089637
                        },
                        new Point2D{
                            Y= 2023621.24785655,
                            X= 490290.131491742
                        },
                        new Point2D{
                            Y= 1997704.05015512,
                            X= 586386.72889879
                        },
                        new Point2D{
                            Y= 1884474.06621417,
                            X= 489483.436576429
                        }
                   },
                ZValues = new double[] { -3, -2, 0, -1, -3, 0, 1, 0, 1, 1, 1, 2, 0, 3, -2, 5, 3, 2, 4, 6, 5, 5, 5, 2, 7, 5, 3, 4, 5, 4, 6, 5, 4, 8, 5, 9, 4, 4, 9, 6, 10, 8, 8, 11, -4, 15, 12, 12, 12, 7, 13, 11, 12, 12, 12, 13, 13, 11, 4, 12, 5, 10, 10, 5, 10, 10, 8, 8, 8, 10, 3, 8, 9, 5, 2, 5, 0, 1, 6, 4, 6, 11, -5, 1, 4, 5, 2, 6, 2, 6, 6, 6, 5, 4, 8, 5, 9, 10, 9, 8, 7, 9, -1, 8, 10, 9, 10, 14, 10, 9, 9, 10, 12, 10, 15, 11, 11, 13, 15, 9, 9, 11, 15, 12, 15, 3, 8, 6, 7, 5, 4, 4, 6, 8, 8, 7, 4, 3, 8, 8, 8, 7, 6, 6, 8, 6, 10, 10, 9, 6, 8, 3, 10, 11, 10, 10, 10, 8, 10, 13, 13, 12, 11, 14, 13, 12, 14, 13, 13, 12, 14, 15, 6, 13, 13, 13, 13, 14, 14, 14, 14, 1, 1, 0, -3, 0, 4, 0, 2, 7, 7, 9, 3, 6, 0, -4, 1, -2, 4, -3, -1, 0, 2, 3, 6, 15, 2, 4, 8, 7, 6, 1, 8, 9, 2, 6, 15, 17, 13, 4, 17, 17, 9, 5, 7, 3, 12, 6, 9, 18, 12, 17, 12, 13, 15, 13, 11, 15, 17, 15, 21, 17, 16, 15, 21, 15, 18, 20, 23, 19, 24, 22, 19, 19, 12, 14, 15, 7, 13, 12, 15, 15, 15, 15, 15, 16, 15, 16, 16, 15, 16, 17, 17, 17, 17, 17, 17, 17, 17, 16, 17, 17, 17, 18, 18, 17, 18, 17, 18, 8, 15, 17, 18, 17, 13, 16, 18, 17, 17, 12, 18, 19, 15, 15, 17, 17, 18, 17, 19, 15, 20, 15, 18, 19, 19, 20, 15, 14, 15, 15, 15, 16, 16, 16, 16, 15, 16, 16, 17, 16, 16, 17, 17, 17, 8, 9, 17, 12, 18, 17, 17, 18, 19, 18, 18, 15, 18, 18, 18, 12, 18, 19, 19, 20, 20, 18, 19, 20, 20, 13, 20, 26, 17, 21, 21, 20, 20, 20, 21, 20, 19, 22, 21, 25.2, 19, 22, 22, 21, 23, 22, 22, 22, 23, 24, 23, 22, 23, 23, 23, 25, 23, 23, 24, 23, 23, 25, 25, 24, 25, 26 },
                //ResultSetting = new DataReturnOption
                //{
                //    MaxRecordCount = 1000,
                //    //DatasetName = "iso3@Interpolation",
                //    //IsOverwrite = true
                //},
                MaxReturnRecordCount = 100,
                ParametersSetting = new SurfaceAnalystParametersSetting
                {
                    DatumValue = -5,
                    Interval = 1,
                    ResampleTolerance = 0.7,
                    SmoothMethod = SmoothMethod.BSPLINE,
                    Smoothness = 3,
                }
            };

            var geometryIsoRegionService = new SurfaceAnalystService(url);
            geometryIsoRegionService.ProcessAsync(parameters);
            geometryIsoRegionService.ProcessCompleted += new EventHandler<SurfaceAnalystEventArgs>(geometryIsoRegionService_ProcessCompleted);
            geometryIsoRegionService.Failed += new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(geometryIsoRegionService_Failed);
        }

        private void geometryIsoRegionService_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
        }

        private void geometryIsoRegionService_ProcessCompleted(object sender, SurfaceAnalystEventArgs e)
        {
            this.resultLayer.AddFeatureSet(e.Result.Recordset.Features);
        }
    }
}
