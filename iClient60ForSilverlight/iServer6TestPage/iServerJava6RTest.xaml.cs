﻿

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Actions;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.iServerJava6R.NetworkAnalyst;
using SuperMap.Web.Mapping;
using SuperMap.Web.Service;

namespace iClient60ForSilverlight
{
    public partial class iServerJava6RTest : UserControl
    {
        //private const string url = "http://localhost:8090/iserver/services/map/rest/maps/SpatialQuery";

        private const string url = "http://localhost:8090/iserver/services/map-world/rest/maps/World Map";
        private const string url1 = "http://localhost:8090/iserver/services/map-changchun/rest/maps/长春市区图";
        private FeaturesLayer flayer;
        private DynamicRESTLayer TiledDynamicRESTLayer;
        private DynamicRESTLayer themeLayer;
        private HighlightLayer highlayer;

        public iServerJava6RTest()
        {
            InitializeComponent();

            TiledDynamicRESTLayer = new DynamicRESTLayer();
            TiledDynamicRESTLayer.Url = url;

            themeLayer = new DynamicRESTLayer();
            themeLayer.Url = url;
            themeLayer.IsSkipGetSMMapServiceInfo = true;
            //themeLayer.CRS = new CoordinateReferenceSystem(4326, Unit.Meter);
            themeLayer.Bounds = new Rectangle2D(48.39718933631297, -7668.245292074532, 8958.850874968857, -55.577652310411075);
            themeLayer.ReferViewBounds = new Rectangle2D(2264.1426896429275, -6101.3928147021725, 6743.105374662273, -1622.430129682827);
            themeLayer.ReferViewer = new Rect(0, 0, 256, 256);
            themeLayer.ReferScale = 2.016091813E-5;

            flayer = new FeaturesLayer();

            //MyMap.Layers.Add(TiledDynamicRESTLayer);
            MyMap.Layers.Add(themeLayer);
            MyMap.Layers.Add(flayer);

            MyMap.ViewBoundsChanged += new EventHandler<ViewBoundsEventArgs>(MyMap_ViewBoundsChanged);

            highlayer = new HighlightLayer(url);
        }

        void MyMap_ViewBoundsChanged(object sender, ViewBoundsEventArgs e)
        {
            mytb.Text = (1 / MyMap.Scale).ToString();
        }

        #region 量算
        private void mybtn_Click(object sender, RoutedEventArgs e)
        {
            DrawLine line = new DrawLine(MyMap);
            MyMap.Action = line;
            line.DrawCompleted += new EventHandler<DrawEventArgs>(line_DrawCompleted);
        }

        private void line_DrawCompleted(object sender, DrawEventArgs e)
        {
            if (e.Geometry == null)
            {
                return;
            }
            Measure(new GeoPoint(0, 0));
        }

        private void polygon_DrawCompleted(object sender, DrawEventArgs e)
        {
            if (e.Geometry == null)
            {
                return;
            }
            Measure(e.Geometry);
        }

        private void measureService_ProcessCompleted(object sender, MeasureEventArgs e)
        {
            if (e.Result.Distance == -1)
            {
                MessageBox.Show(e.Result.Area.ToString() + e.Result.Unit.ToString());
            }
            else if (e.Result.Area == -1)
            {
                MessageBox.Show(e.Result.Distance.ToString() + e.Result.Unit.ToString());
            }
            else
            {
                MessageBox.Show("算没有量结果！");
            }
        }

        private void mybtn2_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon polygon = new DrawPolygon(MyMap);
            MyMap.Action = polygon;
            polygon.DrawCompleted += new EventHandler<DrawEventArgs>(polygon_DrawCompleted);
        }

        private void Measure(SuperMap.Web.Core.Geometry geo)
        {
            MeasureParameters parameters = new MeasureParameters { Geometry = geo, Unit = Unit.Kilometer };
            MeasureService measureService = new MeasureService(url);
            measureService.ProcessAsync(parameters);
            measureService.ProcessCompleted += new EventHandler<MeasureEventArgs>(measureService_ProcessCompleted);
            measureService.Failed += new EventHandler<ServiceFailedEventArgs>(measureService_Failed);
        }

        private void measureService_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }
        #endregion

        #region 查询
        private void pan_Click(object sender, RoutedEventArgs e)
        {
            Pan pan = new Pan(MyMap);
            MyMap.Action = pan;
        }

        private void myQueryBySQL_Click(object sender, RoutedEventArgs e)
        {
            QueryParameters param = new QueryBySQLParameters()
            {
                FilterParameters = new List<FilterParameter>() 
                {
                    new FilterParameter()
                    {
                       Name = "World@world", 
                       AttributeFilter = "SmID=201",
                    }
                },
                //ReturnContent = false
            };
            QueryBySQLService service = new QueryBySQLService(url);
            service.ProcessAsync(param);
            service.ProcessCompleted += new EventHandler<QueryEventArgs>(service_ProcessCompleted);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
        }

        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        private bool isAdded;
        private void service_ProcessCompleted(object sender, QueryEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("查询结果为空！");
                return;
            }

            if (e.Result.ResourceInfo != null)
            {
                highlayer.QueryResultID = e.Result.ResourceInfo.NewResourceID;

                if (!isAdded)
                {
                    this.MyMap.Layers.Add(highlayer);
                    highlayer.Style = new ServerStyle { FillForeColor = Colors.Red, FillBackColor = Colors.Transparent };
                    isAdded = true;
                }

            }
            else
            {
                foreach (Recordset recordset in e.Result.Recordsets)
                {
                    foreach (Feature f in recordset.Features)
                    {
                        f.Style = new FillStyle { Fill = new SolidColorBrush(new Color { A = 10, R = 0, G = 100, B = 0 }), Stroke = new SolidColorBrush(Colors.Blue), StrokeThickness = 1 };
                        flayer.Features.Add(f);
                    }

                }
            }

        }

        private void sp_Click(object sender, RoutedEventArgs e)
        {
            QueryByGeometryParameters param = new QueryByGeometryParameters
            {
                FilterParameters = new List<FilterParameter>() 
                    {             
                        new FilterParameter()
                       {
                           Name = "World@World",
                       }
                    },
                ReturnContent = false,
                Geometry = new GeoRegion()
                {
                    Parts = new ObservableCollection<Point2DCollection>()
                { new Point2DCollection() 
                {
                    new Point2D(0, 0),
                    new Point2D(20, 0),
                    new Point2D(20, 20), 
                    new Point2D(0, 20) 
                } 
                }
                },
                SpatialQueryMode = SpatialQueryMode.INTERSECT
            };
            QueryByGeometryService sp = new QueryByGeometryService(url);
            sp.ProcessAsync(param);
            sp.ProcessCompleted += new EventHandler<QueryEventArgs>(service_ProcessCompleted);
            sp.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
        }

        private void distanceQuery_Click(object sender, RoutedEventArgs e)
        {
            QueryByDistanceParameters param = new QueryByDistanceParameters
            {
                Geometry = new GeoPoint(10, 10),
                Distance = 100,
                FilterParameters = new List<FilterParameter>() 
                { 
                    new FilterParameter(){
                        Name = "World@World",
                    },
                    
                },
                QueryOption = QueryOption.GEOMETRY
            };

            QueryByDistanceService dqs = new QueryByDistanceService(url);

            dqs.ProcessAsync(param);
            dqs.ProcessCompleted += new EventHandler<QueryEventArgs>(service_ProcessCompleted);
            dqs.Failed += service_Failed;
        }

        private void clear_Click(object sender, RoutedEventArgs e)
        {
            flayer.Features.Clear();
        }

        private void clear1_Click(object sender, RoutedEventArgs e)
        {
            if (MyMap.Layers.Count > 2)
            {
                MyMap.Layers.RemoveAt(2);
                isAdded = false;
            }
        }
        #endregion

        #region 网络分析
        private const string networkUrl = "http://localhost:8090/iserver/services/transportationanalyst-sample/rest/networkanalyst/RoadNet@Changchun";
        private void closestfacilities_Click(object sender, RoutedEventArgs e)
        {
            FindClosestFacilitiesParameters<Point2D> parameters1 = new FindClosestFacilitiesParameters<Point2D>
            {
                Event = new Point2D(32.754, 23.205),
                Facilities = new List<Point2D> { new Point2D(50.754, 50.205), new Point2D(80.754, 64.205) },
                FromEvent = true,
                MaxWeight = 0,
                ExpectFacilityCount = 2,
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        //ReturnImage = false,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    //MapParameter = new NAResultMapParameter
                    //{
                    //    Viewer = new Rect(0, 0, 256, 256),
                    //    Format = OutputFormat.PNG
                    //},
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"
                }
            };
            FindClosestFacilitiesParameters<int> p = new FindClosestFacilitiesParameters<int>()
            {
                Event = 2,
                Facilities = new List<int> { 1, 6, 21 },
                FromEvent = true,
                MaxWeight = 0,
                ExpectFacilityCount = 2,
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        //ReturnImage = false,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    //MapParameter = new NAResultMapParameter
                    //{
                    //    Viewer = new Rect(0, 0, 256, 256),
                    //    Format = OutputFormat.PNG
                    //},
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"
                }
            };
            FindClosestFacilitiesService closestFacilitiesService = new FindClosestFacilitiesService(networkUrl);
            closestFacilitiesService.ProcessAsync(p);
            //closestFacilitiesService.ProcessAsync(parameters1);
            closestFacilitiesService.Failed += Failed;
            closestFacilitiesService.ProcessCompleted += new EventHandler<FindClosestFacilitiesEventArgs>(closestFacilitiesService_ProcessCompleted);
        }

        void closestFacilitiesService_ProcessCompleted(object sender, FindClosestFacilitiesEventArgs e)
        {
            if (e.Result.FacilityPathList[0].Facility is Point2D)
            {
                Point2D pp = (Point2D)(e.Result.FacilityPathList[0].Facility);
            }
            else
            {
                int i = (int)(e.Result.FacilityPathList[0].Facility);
            }
        }

        void Failed(object sender, ServiceFailedEventArgs e)
        {

        }

        private void FindPath_Click(object sender, RoutedEventArgs e)
        {
            FindPathParameters<int> paramInt = new FindPathParameters<int>
            {
                Nodes = new List<int> { 2, 3 },
                HasLeastEdgeCount = true,
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        //ReturnImage = false,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    //MapParameter = new NAResultMapParameter
                    //{
                    //    Viewer = new Rect(0, 0, 256, 256),
                    //    Format = OutputFormat.PNG
                    //},
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"
                }
            };

            FindPathParameters<Point2D> paramPoint2D = new FindPathParameters<Point2D>
            {
                Nodes = new List<Point2D> { new Point2D(4737.65773280094, -3406.06007171244),
                    new Point2D(5645.28372661686, -3312.88173774655) },
                HasLeastEdgeCount = true,
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        //ReturnImage = false,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    //MapParameter = new NAResultMapParameter
                    //{
                    //    Viewer = new Rect(0, 0, 256, 256),
                    //    Format = OutputFormat.PNG
                    //},
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"
                }
            };
            FindPathService findPathService = new FindPathService("http://localhost:8090/iserver/services/components-rest/rest/networkanalyst/RoadNet@Changchun") { };
            //findPathService.ProcessAsync(paramInt);
            findPathService.ProcessAsync(paramPoint2D);
            findPathService.Failed += Failed;
            findPathService.ProcessCompleted += new EventHandler<FindPathEventArgs>(findPathService_ProcessCompleted);
        }

        void findPathService_ProcessCompleted(object sender, FindPathEventArgs e)
        {
            Feature f = e.Result.PathList[0].EdgeFeatures[0];
        }

        private void LocationAnalyst_Click(object sender, RoutedEventArgs e)
        {
            FindLocationAnalystParameters param = new FindLocationAnalystParameters
            {
                SupplyCenters = new List<SupplyCenter> 
                { 
                    new SupplyCenter
                    {
                        NodeID=7, 
                        MaxWeight=3,
                        ResourceValue=100,
                        Type= SupplyCenterType.OPTIONALCENTER
                    },
                     new SupplyCenter
                    {
                        NodeID=8, 
                        MaxWeight=3,
                        ResourceValue=100,
                        Type= SupplyCenterType.OPTIONALCENTER
                    }
                },
                ExpectedSupplyCenterCount = 1,
                WeightName = "length",
                TurnWeightField = "TurnCost",
                ReturnNodeFeature = true,
                ReturnEdgeGeometry = true,
                ReturnEdgeFeature = true,
                IsFromCenter = false,
            };

            FindLocationAnalystService locationAnalystService = new FindLocationAnalystService(networkUrl);
            locationAnalystService.ProcessAsync(param);
            locationAnalystService.Failed += Failed;
            locationAnalystService.ProcessCompleted += new EventHandler<FindLocationAnalystEventArgs>(locationAnalystService_ProcessCompleted);
        }

        void locationAnalystService_ProcessCompleted(object sender, FindLocationAnalystEventArgs e)
        {

        }

        private void ServiceAreas_Click(object sender, RoutedEventArgs e)
        {
            FindServiceAreasParameters<int> param = new FindServiceAreasParameters<int>
            {
                Centers = new List<int> { 2, 4 },
                Weights = new List<double> { 500, 1000 },
                IsCenterMutuallyExclusive = false,
                IsFromCenter = true,
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        //ReturnImage = false,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    //MapParameter = new NAResultMapParameter
                    //{
                    //    Viewer = new Rect(0, 0, 256, 256),
                    //    Format = OutputFormat.PNG
                    //},
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"
                }
            };
            FindServiceAreasService serviceAreasService = new FindServiceAreasService(networkUrl);
            serviceAreasService.ProcessAsync(param);
            serviceAreasService.ProcessCompleted += new EventHandler<FindServiceAreasEventArgs>(serviceAreasService_ProcessCompleted);
            serviceAreasService.Failed += Failed;
        }

        void serviceAreasService_ProcessCompleted(object sender, FindServiceAreasEventArgs e)
        {

        }

        private void TSPPaths_Click(object sender, RoutedEventArgs e)
        {
            FindTSPPathsParameters<int> param = new FindTSPPathsParameters<int>
            {
                Nodes = new List<int> { 2, 3 },
                EndNodeAssigned = true,
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        //ReturnImage = false,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    //MapParameter = new NAResultMapParameter
                    //{
                    //    Viewer = new Rect(0, 0, 256, 256),
                    //    Format = OutputFormat.PNG
                    //},
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"

                }
            };

            FindTSPPathsService tSPPathsService = new FindTSPPathsService(networkUrl);
            tSPPathsService.ProcessAsync(param);
            tSPPathsService.Failed += Failed;
            tSPPathsService.ProcessCompleted += new EventHandler<FindTSPPathsEventArgs>(tSPPathsService_ProcessCompleted);
        }

        void tSPPathsService_ProcessCompleted(object sender, FindTSPPathsEventArgs e)
        {

        }

        private void Mtsp_Click(object sender, RoutedEventArgs e)
        {
            FindMTSPPathsParameters<int> param = new FindMTSPPathsParameters<int>
            {
                Centers = new List<int> { 2, 5, 7 },
                Nodes = new List<int> { 1, 6, 21 },
                HasLeastTotalCost = false,

                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        //ReturnImage = false,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    //MapParameter = new NAResultMapParameter
                    //{
                    //    Viewer = new Rect(0, 0, 256, 256),
                    //    Format = OutputFormat.PNG
                    //},
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"
                }

            };
            FindMTSPPathsParameters<Point2D> paramPoint = new FindMTSPPathsParameters<Point2D>
            {
                Centers = new List<Point2D> { new Point2D(4737.65773280094, -3406.06007171244),
                    new Point2D(5645.28372661686, -3312.88173774655) },
                Nodes = new List<Point2D> { new Point2D(5737.65773280094, -3406.06007171244),
                    new Point2D(4645.28372661686, -3312.88173774655) },
                HasLeastTotalCost = false,

                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        //ReturnImage = false,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    //MapParameter = new NAResultMapParameter
                    //{
                    //    Viewer = new Rect(0, 0, 256, 256),
                    //    Format = OutputFormat.PNG
                    //},
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"
                }

            };
            FindMTSPPathsService mTSPPathsService = new FindMTSPPathsService(networkUrl);
            mTSPPathsService.ProcessAsync(paramPoint);
            mTSPPathsService.Failed += Failed;
            mTSPPathsService.ProcessCompleted += new EventHandler<FindMTSPPathsEventArgs>(mTSPPathsService_ProcessCompleted);

        }

        void mTSPPathsService_ProcessCompleted(object sender, FindMTSPPathsEventArgs e)
        {
            Point2D p = (Point2D)(e.Result.MTSPathList[0].Center);
        }



        private void WeightMatrix_Click(object sender, RoutedEventArgs e)
        {
            ComputeWeightMatrixParameters<int> param = new ComputeWeightMatrixParameters<int>
            {
                Nodes = new List<int> { 2, 3 },
                Parameter = new TransportationAnalystParameter
                {
                    ResultSetting = new TransportationAnalystResultSetting
                    {
                        ReturnEdgeFeatures = true,
                        ReturnEdgeGeometry = true,
                        ReturnEdgeIDs = true,
                        //ReturnImage = false,
                        ReturnNodeFeatures = true,
                        ReturnNodeGeometry = true,
                        ReturnNodeIDs = true,
                        ReturnPathGuides = true,
                        ReturnRoutes = true
                    },
                    //MapParameter = new NAResultMapParameter
                    //{
                    //    Viewer = new Rect(0, 0, 256, 256),
                    //    Format = OutputFormat.PNG
                    //},
                    WeightFieldName = "length",
                    TurnWeightField = "TurnCost"
                }
            };
            ComputeWeightMatrixService weightMatrixService = new ComputeWeightMatrixService(networkUrl);
            weightMatrixService.ProcessAsync(param);
            weightMatrixService.Failed += Failed;
            weightMatrixService.ProcessCompleted += new EventHandler<ComputeWeightMatrixEventArgs>(weightMatrixService_ProcessCompleted);
        }

        void weightMatrixService_ProcessCompleted(object sender, ComputeWeightMatrixEventArgs e)
        {

        }

        #endregion

        #region 编辑
        private void EditService_Click(object sender, RoutedEventArgs e)
        {
            Feature f = new Feature
            {
                Geometry = new GeoRegion
                  {
                      Parts = new ObservableCollection<Point2DCollection>
                    {
                        new Point2DCollection
                        {
                            new Point2D(-100,60),
                            new Point2D(-100,62),
                            new Point2D(-40,55)                                    
                        }
                    }
                  }
            };

            f.Attributes.Add("SMID", "1166");
            f.Attributes.Add("SMSDRIW", "-7.433472633361816");
            f.Attributes.Add("POP_1994", "123456789");

            EditFeaturesParameters param = new EditFeaturesParameters()
            {
                EditType = EditType.ADD,
                //IDs = new List<int> { 248 }
                Features = new FeatureCollection { f }
            };

            EditFeaturesService editFeaturesService = new EditFeaturesService("http://localhost:8090/iserver/services/data-world/rest/data/datasources/name/World/datasets/name/World/features");
            editFeaturesService.ProcessAsync(param);
            editFeaturesService.ProcessCompleted += new EventHandler<EditFeaturesEventArgs>(editFeaturesService_ProcessCompleted);
            editFeaturesService.Failed += Failed;
        }

        void editFeaturesService_ProcessCompleted(object sender, EditFeaturesEventArgs e)
        {

        }
        #endregion

        #region Data模块下的查询
        private const string dataqueryurl = "http://localhost:8090/iserver/services/data-world/rest/data/featureResults";
        private void QueryFeatures_Click(object sender, RoutedEventArgs e)
        {
            //GetFeaturesByIDsParameters param = new GetFeaturesByIDsParameters
            //{
            //    DatasetNames = new string[] { "World:Capital", "World:World" },
            //    IDs = new int[] { 2, 3, 5 },
            //    Fields = new string[] { "SmID" },
            //    FromIndex = 1,
            //    ToIndex = 10
            //};

            //GetFeaturesByIDsService queryFeaturesService = new GetFeaturesByIDsService(dataqueryurl);
            //queryFeaturesService.ProcessAsync(param);
            //queryFeaturesService.Failed += this.Failed;
            //queryFeaturesService.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(queryFeaturesService_ProcessCompleted);

            GetFeaturesByGeometryParameters geoParam = new GetFeaturesByGeometryParameters
            {
                Geometry = new GeoPoint(42, 21.5),
                DatasetNames = new string[] { "World:Capital", "World:World" },
                SpatialQueryMode = SpatialQueryMode.DISJOINT,
                FromIndex = 0,
                ToIndex = 10
            };
            GetFeaturesByGeometryService queryFeaturesService = new GetFeaturesByGeometryService(dataqueryurl);
            queryFeaturesService.ProcessAsync(geoParam);
            queryFeaturesService.Failed += this.Failed;
            queryFeaturesService.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(queryFeaturesService_ProcessCompleted);

            //GetFeaturesBySQLParameters sqlParam = new GetFeaturesBySQLParameters
            //{
            //    DatasetNames = new string[] { "World:Capital", "World:World" },
            //    FilterParameter = new FilterParameter
            //    {
            //        Name = "World@World",
            //        AttributeFilter = "SmID=42",
            //        Fields = new string[] { "SmID" },
            //    }
            //};
            //GetFeaturesBySQLService queryFeaturesService = new GetFeaturesBySQLService(dataqueryurl);
            //queryFeaturesService.ProcessAsync(sqlParam);
            //queryFeaturesService.Failed += this.Failed;
            //queryFeaturesService.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(queryFeaturesService_ProcessCompleted);

            //GetFeaturesByBufferParameters bufferParam = new GetFeaturesByBufferParameters
            //{
            //    DatasetNames = new string[] { "World:Capital", "World:World" },
            //    Geometry = new GeoPoint(42, 21.5),
            //    BufferDistance = 10,
            //    //AttributeFilter = "SmID<10"
            //};
            //GetFeaturesByBufferService queryFeaturesService = new GetFeaturesByBufferService(dataqueryurl);
            //queryFeaturesService.ProcessAsync(bufferParam);
            //queryFeaturesService.Failed += this.Failed;
            //queryFeaturesService.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(queryFeaturesService_ProcessCompleted);
        }

        void queryFeaturesService_ProcessCompleted(object sender, GetFeaturesEventArgs e)
        {
            //if (e.Result != null && e.Result.Features != null && e.Result.Features.Count > 0)
            //{
            //    flayer.AddFeatureSet(e.Result.Features);
            //}
        }
        #endregion

        #region 子图层控制
        private void SetLayerStatus_Click(object sender, RoutedEventArgs e)
        {
            SetLayerStatusService layerStatusService = new SetLayerStatusService(url);

            SetLayerStatusParameters parameters = new SetLayerStatusParameters
            {
                LayerStatusList = new List<LayerStatus>
                 {
                      new LayerStatus{
                           IsVisible=false,
                            LayerName="ContinentLabel@World"
                      },
                      new LayerStatus{
                           IsVisible=false,
                            LayerName="OceanLabel@World"
                      },
                      new LayerStatus
                      {
                          IsVisible=false,
                          LayerName="标签专题图#2"
                      },
                      new LayerStatus
                      {
                          IsVisible=false,
                          LayerName="Ocean@World"
                      },
                      new LayerStatus
                      {
                          IsVisible=false,
                          LayerName="Capitals@World"
                      },
                      new LayerStatus
                      {
                          IsVisible=false,
                          LayerName="Grids@World"
                      },
                      new LayerStatus
                      {
                          IsVisible=false,
                          LayerName="单值专题图"
                      },
                 },
                ResourceID = "3"
            };

            layerStatusService.ProcessAsync(parameters);
            layerStatusService.ProcessCompleted += new EventHandler<SetLayerEventArgs>(layerStatusService_ProcessCompleted);
            layerStatusService.Failed += new EventHandler<ServiceFailedEventArgs>(layerStatusService_Failed);
        }

        void layerStatusService_Failed(object sender, ServiceFailedEventArgs e)
        {
        }

        void layerStatusService_ProcessCompleted(object sender, SetLayerEventArgs e)
        {
            themeLayer.LayersID = e.Result.NewResourceID;
        }

        #endregion

        private void SetLayerStyle_Click(object sender, RoutedEventArgs e)
        {
            SetLayerStyleService setLayerStyleService = new SetLayerStyleService(url);

            SetLayerStyleParameters parameters = new SetLayerStyleParameters
            {
                Style = new ServerStyle
                {
                    FillForeColor = Colors.Green
                },
                LayerName = "Ocean@World",
                ResourceID = "7"
            };

            setLayerStyleService.ProcessAsync(parameters);
            setLayerStyleService.ProcessCompleted += new EventHandler<SetLayerEventArgs>(setLayerStyleService_ProcessCompleted);
            setLayerStyleService.Failed += new EventHandler<ServiceFailedEventArgs>(setLayerStyleService_Failed);
        }

        void setLayerStyleService_Failed(object sender, ServiceFailedEventArgs e)
        {
        }

        void setLayerStyleService_ProcessCompleted(object sender, SetLayerEventArgs e)
        {
            themeLayer.LayersID = e.Result.NewResourceID;
        }
    }
}
