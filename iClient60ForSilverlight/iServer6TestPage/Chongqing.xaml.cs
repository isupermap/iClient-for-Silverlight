﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.iServerJava6R.Data;
using SuperMap.Web.Service;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iClient60ForSilverlight
{
    public partial class Chongqing : UserControl
    {
        private FeaturesLayer featuresLayer = new FeaturesLayer();


        public Chongqing()
        {
            InitializeComponent();

            MyMap.Layers.Add(featuresLayer);

            MyMap.ViewBoundsChanged += new EventHandler<ViewBoundsEventArgs>(MyMap_ViewBoundsChanged);

        }

        void MyMap_ViewBoundsChanged(object sender, ViewBoundsEventArgs e)
        {
            btn.Content = 1.0 / MyMap.Scale;
        }

        void Chongqing_Loaded()
        {

            GetFeaturesBySQLParameters param = new GetFeaturesBySQLParameters
            {
                DatasetNames = new List<String>() { "A_重庆5万:roalkL" },
                FromIndex = 0,
                ToIndex = 20000
            };

            FilterParameter filterParameters = new FilterParameter();
            filterParameters.Name = "roalkL@A_重庆5万";
            filterParameters.AttributeFilter = "SMID=10";

            param.FilterParameter = filterParameters;

            GetFeaturesBySQLService getFeaturesByGeometryServiceInSelectEditTool = new GetFeaturesBySQLService("http://localhost:8090/iserver/services/data-world/rest/data/featureResults");
            getFeaturesByGeometryServiceInSelectEditTool.ProcessAsync(param);
            getFeaturesByGeometryServiceInSelectEditTool.ProcessCompleted += new EventHandler<GetFeaturesEventArgs>(getFeaturesByGeometryServiceInSelectEditTool_ProcessCompleted);
            getFeaturesByGeometryServiceInSelectEditTool.Failed += new EventHandler<ServiceFailedEventArgs>(query_Failed);

        }


        private void getFeaturesByGeometryServiceInSelectEditTool_ProcessCompleted(object sender, GetFeaturesEventArgs e)
        {
            if (e.Result == null || e.Result.FeatureCount < 1)
            {
                MessageBox.Show("很抱歉，没有查询到结果！\n" + "请重试！");
                return;
            }

            Feature addingFeature = e.Result.Features[0];

            this.MyMap.ZoomTo(addingFeature.Geometry.Bounds);

            featuresLayer.AddFeature(addingFeature);

        }

        private void query_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show("很抱歉，没有查询到结果！\n" + "请重试！");
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Chongqing_Loaded();
        }
    }
}
