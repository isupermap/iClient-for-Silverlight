﻿//参考：http://www.sharpgis.net/post/2009/02/27/Loading-pages-dynamically-in-Silverlight.aspx
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace iClient60ForSilverlight
{
    public class PageNavigator
    {
        private string currentHash;
        private Application application;
        private Grid pageGrid = new Grid();
        private string defaultPageName;

        public PageNavigator(Application app, UIElement defaultPage)
        {
            application = app;
            defaultPageName = defaultPage.GetType().FullName;
            CreateRootVisual();

            UIElement element = null;
            if (HtmlPage.IsEnabled)
            {
                DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(500) };
                timer.Tick += (sender, e) =>
                {
                    string hash = GetAnchor();
                    if (hash != currentHash &&
                        (pageGrid.Children.Count == 0 || pageGrid.Children[0].GetType().FullName != hash))
                    {
                        ShowElement(LoadElement(hash));
                    }
                    currentHash = hash;
                };
                currentHash = GetAnchor();
                element = LoadElement(currentHash);
                timer.Start();
            }

            ShowElement(element ?? defaultPage);
        }

        private static string GetAnchor()
        {
            if (HtmlPage.IsEnabled)
            {
                return HtmlPage.Window.CurrentBookmark;
            }
            return null;
        }

        private void ShowElement(UIElement element)
        {
            if (element == null)
            {
                return;
            }
            pageGrid.Children.Clear();
            pageGrid.Children.Add(element);
        }

        private UIElement LoadElement(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = defaultPageName;
            }
            try
            {
                return Assembly.GetExecutingAssembly().CreateInstance(name) as UIElement;
            }
            catch (Exception)
            {
                MessageBox.Show(string.Format("Failed to load page {0}. See output for stack trace", name));
            }
            return null;
        }


        private void CreateRootVisual()
        {
            Grid rootGrid = new Grid();
            rootGrid.Children.Add(pageGrid);
            ComboBox cb = new ComboBox
            {
                Width = 25,
                Height = 12,
                Opacity = 0.7,
                Margin = new Thickness(3),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
                Cursor = Cursors.Hand,
                ItemsSource = GetAllPages() //获取所有的Page:Usercontrol
            };
            cb.SelectionChanged += new SelectionChangedEventHandler(cb_SelectionChanged);
            rootGrid.Children.Add(cb);
            application.RootVisual = rootGrid;
        }

        private void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string value = (sender as ComboBox).SelectedItem as string;
            if (HtmlPage.IsEnabled)
            {
                HtmlPage.Window.Navigate(new Uri("#" + value, UriKind.Relative));
            }
            else
            {
                if (pageGrid.Children.Count == 0 || pageGrid.Children[0].GetType().FullName != value)
                {
                    ShowElement(LoadElement(value));
                }
            }
        }

        private List<string> GetAllPages()
        {
            List<string> pages = new List<string>();
            Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsSubclassOf(typeof(UserControl)) && type.GetConstructor(new Type[] { }) != null)
                {
                    pages.Add(type.FullName);
                }
            }
            return pages;
        }

    }
}
