﻿using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iClient60ForSilverlight
{
    public enum EMapType
    {
        Imagery,
        Street
    }

    public class TiledESRIOnlineMapsLayer : TiledCachedLayer
    {
        private const string ImageryUrl = "http://services.arcgisonline.com/ArcGIS/rest/services/ESRI_Imagery_World_2D/MapServer/tile/{0}/{1}/{2}";
        private const string StreetMapUrl = "http://services.arcgisonline.com/ArcGIS/rest/services/ESRI_StreetMap_World_2D/MapServer/tile/{0}/{1}/{2}";
        //private const string topoUrl = " http://services.arcgisonline.com/ArcGIS/rest/services/NGS_Topo_US_2D/MapServer/tile/{0}/{1}/{2}";
        private const int WKID = 4326;

        public override string GetTileUrl(int indexX, int indexY, int level)
        {
            return string.Format(this.Url, level, indexY, indexX);
        }

        public override void Initialize()
        {
            this.Bounds = new Rectangle2D(-179.999999, -89.999999, 179.999999, 89.999999);
            this.CRS = new CoordinateReferenceSystem(WKID, Unit.DecimalDegree);
            this.TileSize = 512;

            double res = 0.3515625;
            double[] resolutions = new double[16];
            for (int i = 0; i < resolutions.Length; i++)
            {
                resolutions[i] = res;
                res *= 0.5;
            }
            this.Resolutions = resolutions;

            switch (this.MapType)
            {
                case EMapType.Imagery:
                    this.Url = ImageryUrl;
                    break;
                case EMapType.Street:
                    this.Url = StreetMapUrl;
                    break;
            }
            base.Initialize();
        }
        public EMapType MapType
        {
            get { return (EMapType)GetValue(MapTypeProperty); }
            set { SetValue(MapTypeProperty, value); }
        }

        public static readonly DependencyProperty MapTypeProperty = DependencyProperty.Register("MapType", typeof(EMapType), typeof(TiledESRIOnlineMapsLayer), new PropertyMetadata(new PropertyChangedCallback(OnMapTypePropertyChanged)));
        private static void OnMapTypePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TiledESRIOnlineMapsLayer layer = d as TiledESRIOnlineMapsLayer;
            if (layer.IsInitialized)
            {
                layer.ChangedTileSource();
            }
        }

        private void ChangedTileSource()
        {
            switch (this.MapType)
            {
                case EMapType.Imagery:
                    this.Url = ImageryUrl;
                    break;
                case EMapType.Street:
                    this.Url = StreetMapUrl;
                    break;
            }
            if (!base.IsInitialized)
            {
                base.Initialize();
            }
            else
            {
                base.Refresh();
            }
        }

    }
}
