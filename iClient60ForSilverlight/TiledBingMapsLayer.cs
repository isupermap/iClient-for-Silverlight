using System;
using System.Text;
using System.Windows;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;

namespace iClient60ForSilverlight
{
    public enum BMapsType
    {
        Road,
        Aerial,
        AerialWithLabels
    }

    public class TiledBingMapsLayer : TiledCachedLayer
    {
        //http://ecn.t0.tiles.virtualearth.net/tiles/h13220.jpeg?g=266
        private const string RoadTileUri = "http://r{0}.ortho.tiles.virtualearth.net/tiles/r{1}.png?g=43&amp;shading=hill";
        private const string AerialTileUri = "http://a{0}.ortho.tiles.virtualearth.net/tiles/a{1}.jpg?g=43";
        private const string AerialWithLabelsTileUri = "http://h{0}.ortho.tiles.virtualearth.net/tiles/h{1}.jpg?g=43";
        //private const string RoadTileUri = "http://ecn.t{0}.tiles.virtualearth.net/tiles/h{1}.jpeg?g=266";

        private string[] subDomains = { "0", "1", "2", "3" };
        private const double CornerCoordinate = 20037508.3427892;
        //private const int WKID = 102113;//ESRI的Web MerCator投影编号102113，EPSG:900913

        private StringBuilder keyBuilder = new StringBuilder();


        public TiledBingMapsLayer()
        { }


        public override string GetTileUrl(int indexX, int indexY, int level)
        {
            //QuadKey key = new QuadKey(row, col, level);
            //char ch = key.Key[key.ZoomLevel - 1];
            this.keyBuilder.Length = 0;
            for (int i = level; i >= 0; i--)
            {
                int num2 = ((int)1) << i;
                int num = ((indexX & num2) >> i) | (((indexY & num2) != 0) ? 2 : 0);
                this.keyBuilder.Append(num);
            }
            int num3 = ((indexY & 1) << 1) | (indexX & 1);//没有多大意义？随机函数即可

            //string subDomain = subDomains[(level + row + col) % subDomains.Length];
            return string.Format(this.Url, num3, keyBuilder);
        }

        public override void Initialize()
        {
            this.Bounds = new Rectangle2D(-CornerCoordinate, -CornerCoordinate, CornerCoordinate, CornerCoordinate);
            this.TileSize = 256;

            double res = CornerCoordinate * 2 / 512;
            double[] resolutions = new double[18];
            for (int i = 0; i < resolutions.Length; i++)
            {
                resolutions[i] = res;
                res *= 0.5;
            }
            this.Resolutions = resolutions;

            switch (this.MapType)
            {
                case BMapsType.Road:
                    this.Url = RoadTileUri;
                    break;
                case BMapsType.Aerial:
                    this.Url = AerialTileUri;
                    break;
                case BMapsType.AerialWithLabels:
                    this.Url = AerialWithLabelsTileUri;
                    break;
            }
            //枚举直接默认第一个
            //为在xaml中设置时用的，后来变了才出发onChange事件

            base.Initialize();
        }


        public BMapsType MapType
        {
            get { return (BMapsType)GetValue(MapTypeProperty); }
            set { SetValue(MapTypeProperty, value); }
        }
        public static readonly DependencyProperty MapTypeProperty =
            DependencyProperty.Register("MapType", typeof(BMapsType), typeof(TiledBingMapsLayer), new PropertyMetadata(new PropertyChangedCallback(new PropertyChangedCallback(OnMapTypePropertyChanged))));

        private static void OnMapTypePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TiledBingMapsLayer layer = d as TiledBingMapsLayer;
            if (layer.IsInitialized)
            {
                layer.ChangeTileSource();
            }
        }

        private void ChangeTileSource()
        {
            switch (this.MapType)
            {
                case BMapsType.Road:
                    this.Url = RoadTileUri;
                    break;
                case BMapsType.Aerial:
                    this.Url = AerialTileUri;
                    break;
                case BMapsType.AerialWithLabels:
                    this.Url = AerialWithLabelsTileUri;
                    break;

            }
            if (!base.IsInitialized)
            {
                base.Initialize();
            }
            else
            {
                base.Refresh();
            }
        }

    }


    public struct QuadKey
    {
        public QuadKey(int x, int y, int zoomLevel)
        {
            this = new QuadKey();
            this.ZoomLevel = zoomLevel;
            this.X = x;
            this.Y = y;
        }

        public QuadKey(string quadKey)
        {
            int num;
            int num2;
            int num3;
            this = new QuadKey();
            QuadKeyToQuadPixel(quadKey, out num, out num2, out num3);
            this.ZoomLevel = num3;
            this.X = num;
            this.Y = num2;
        }

        public int ZoomLevel { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Key
        {
            get
            {
                return QuadPixelToQuadKey(this.X, this.Y, this.ZoomLevel);
            }
        }
        private static string QuadPixelToQuadKey(int x, int y, int zoomLevel)
        {
            int num2 = (int)Math.Pow(2.0, (double)zoomLevel);
            string str = "";
            if ((y >= 0) && (y < num2))
            {
                while (x < 0)
                {
                    x += num2;
                }
                while (x > num2)
                {
                    x -= num2;
                }
                for (int i = 1; i <= zoomLevel; i++)
                {
                    switch (((2 * (y % 2)) + (x % 2)))
                    {
                        case 0:
                            str = "0" + str;
                            break;

                        case 1:
                            str = "1" + str;
                            break;

                        case 2:
                            str = "2" + str;
                            break;

                        case 3:
                            str = "3" + str;
                            break;
                    }
                    x /= 2;
                    y /= 2;
                }
                return str;
            }
            return null;
        }

        private static void QuadKeyToQuadPixel(string quadKey, out int x, out int y, out int zoomLevel)
        {
            x = 0;
            y = 0;
            zoomLevel = 0;
            if (!string.IsNullOrEmpty(quadKey))
            {
                zoomLevel = quadKey.Length;
                for (int i = 0; i < quadKey.Length; i++)
                {
                    switch (quadKey[i])
                    {
                        case '0':
                            x *= 2;
                            y *= 2;
                            break;

                        case '1':
                            x = (x * 2) + 1;
                            y *= 2;
                            break;

                        case '2':
                            x *= 2;
                            y = (y * 2) + 1;
                            break;

                        case '3':
                            x = (x * 2) + 1;
                            y = (y * 2) + 1;
                            break;
                    }
                }
            }
        }

        public static bool operator ==(QuadKey tile1, QuadKey tile2)
        {
            return (((tile1.X == tile2.X) && (tile1.Y == tile2.Y)) && (tile1.ZoomLevel == tile2.ZoomLevel));
        }

        public static bool operator !=(QuadKey tile1, QuadKey tile2)
        {
            return !(tile1 == tile2);
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is QuadKey))
            {
                return false;
            }
            QuadKey key = (QuadKey)obj;
            return (this == key);
        }

        public override int GetHashCode()
        {
            return ((this.X.GetHashCode() ^ this.Y.GetHashCode()) ^ this.ZoomLevel.GetHashCode());
        }
    }
}