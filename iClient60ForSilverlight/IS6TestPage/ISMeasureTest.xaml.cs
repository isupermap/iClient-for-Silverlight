﻿
using SuperMap.Web.Actions;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;
using System;

namespace iClient60ForSilverlight
{
    public partial class ISMeasureTest : UserControl
    {
        public ISMeasureTest()
        {
            InitializeComponent();
            TiledCachedISLayer islayer = new TiledCachedISLayer();
            islayer.Url = "http://localhost/IS/AjaxDemo";
            islayer.MapName = "Changchun";

            islayer.Scales = new double[] { 1.25e-5, 2e-5, 5e-5, 1.25e-4, 2e-4, 5e-4 };

            islayer.IsSkipGetSMMapServiceInfo = true;
            islayer.Bounds = new Rectangle2D(48.397189336313, -7668.24529207453, 8958.85087496886, -77.4238846634398);
            islayer.ReferViewBounds = new Rectangle2D(345.73724048129, -6740.34272055611, 8661.51082382391, -1005.32645618189);
            islayer.ReferViewer = new Rect(0, 0, 580, 400);
            islayer.ReferScale = 0.00001743674218;

            MyMap.Layers.Add(islayer);
        }
    }
}
