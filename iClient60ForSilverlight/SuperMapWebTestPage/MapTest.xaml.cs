﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;

namespace iClient60ForSilverlight.SuperMapWebTestPage
{
    public partial class MapTest : UserControl
    {
        FeaturesLayer featureLayer;
        ElementsLayer elementsLayer;
        public MapTest()
        {
            InitializeComponent();
            featureLayer = MyMap.Layers["myf"] as FeaturesLayer;
            elementsLayer = MyMap.Layers["mye"] as ElementsLayer;
        }

        private void MyBtn_Click(object sender, RoutedEventArgs e)
        {
            featureLayer.ClearFeatures();
            mytb.Text = MyMap.Center.ToString();
        }

        private void MyBtn1_Click(object sender, RoutedEventArgs e)
        {
            Feature feature = new Feature();
            GeoRegion geoRegion = new GeoRegion();
            geoRegion.Parts.Add(new Point2DCollection() { new Point2D(110, 110), new Point2D(250, 350), new Point2D(400, 550), new Point2D(510, 510) });
            feature.Geometry = geoRegion;
            featureLayer.Features.Add(feature);

            mytb.Text = MyMap.Center.ToString();
        }

        private void MyBtn2_Click(object sender, RoutedEventArgs e)
        {
            Feature feature = new Feature();
            GeoRegion geoRegion = new GeoRegion();
            geoRegion.Parts.Add(new Point2DCollection() { new Point2D(50, 150), new Point2D(250, 250), new Point2D(400, 150), new Point2D(150, 150) });
            feature.Geometry = geoRegion;
            featureLayer.Features.Add(feature);

            mytb.Text = MyMap.Center.ToString();
        }

        private void MyBtn100_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Feature f = new Feature { Geometry = new GeoPoint(i, j) };
                    featureLayer.Features.Add(f);
                }
            }
        }

        private void MyBtn100e_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    Pushpin p = new Pushpin { Location = new Point2D(i, j), Content = new TextBlock { Text = j.ToString() + "-" + i.ToString(), FontSize = 9 } };
                    elementsLayer.AddChild(p);
                }
            }
        }

    }
}
