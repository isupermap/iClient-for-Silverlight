﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Mapping;
using SuperMap.Web.Core;

namespace iClient60ForSilverlight.SuperMapWebTestPage
{
    public partial class Different_CRS_Overlay : UserControl
    {
        public Different_CRS_Overlay()
        {
            InitializeComponent();

            TiledDynamicIServerLayer iserverChangchun = new TiledDynamicIServerLayer()
            {
                Url = "http://localhost:7080/demo",
                MapName = "Changchun",
                MapServiceAddress = "localhost",
                MapServicePort = 8600,
                CRS = new SuperMap.Web.Core.CoordinateReferenceSystem { Unit = Unit.Degree, WKID = 4326 }
            };
            TiledDynamicIServerLayer iserverWorld = new TiledDynamicIServerLayer()
            {
                Url = "http://localhost:7080/demo",
                MapName = "World",
                MapServiceAddress = "localhost",
                MapServicePort = 8600,
                CRS = new SuperMap.Web.Core.CoordinateReferenceSystem { Unit = Unit.Inch, WKID = 8600 }
            };
            MyMap.Layers.LayersInitialized+=new EventHandler(Layers_LayersInitialized);
            MyMap.Layers.Add(iserverChangchun);
            MyMap.Layers.Add(iserverWorld);
            MyMap.ViewBoundsChanged += new EventHandler<ViewBoundsEventArgs>(MyMap_ViewBoundsChanged);

        }

        void Layers_LayersInitialized(object sender, EventArgs e)
        {
        }
        void MyMap_ViewBoundsChanged(object sender, ViewBoundsEventArgs e)
        {
            //MessageBox.Show("Unit:" + (sender as Map).CRS.Unit.ToString() + "WKID:" + (sender as Map).CRS.WKID.ToString());
        }
    }
}
