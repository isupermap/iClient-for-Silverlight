﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Mapping;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.iServerJava6R;


namespace iClient60ForSilverlight
{
    public partial class ToolTipTest : UserControl
    {
        public string url = "http://localhost:8090/iserver/services/components-rest/rest/maps/";
        public FeaturesLayer featureLayer;
        public string tooltipText = "";

        public ToolTipTest()
        {
            InitializeComponent();
            featureLayer = myMap.Layers["myFeaturesLayer"] as FeaturesLayer;
            myMap.MouseLeftButtonDown += new MouseButtonEventHandler(myMap_MouseLeftButtonDown);
         
            myMap.Loaded += new RoutedEventHandler(myMap_Loaded);
            Query("世界地图", "World@world", "SmID=247", "WelCome To China!");

        }

        void myMap_Loaded(object sender, RoutedEventArgs e)
        {
            myMap.Focus();
            myMap.KeyDown+=new KeyEventHandler(myMap_KeyDown);
        }

        //进行SQL查询
        public void Query(string mapName, string layerName, string attributeFilter, string text)
        {
            tooltipText = text;
            QueryBySQLParameters param = new QueryBySQLParameters()
            {
                FilterParameters = new List<FilterParameter>() 
                {
                    new FilterParameter()
                    {
                        Name = layerName, 
                        AttributeFilter = attributeFilter,
                    }
                    
                },
                ReturnContent = true,
            };

            QueryBySQLService service = new QueryBySQLService(url + mapName);
            service.ProcessAsync(param);
            service.ProcessCompleted += new EventHandler<SuperMap.Web.iServerJava6R.QueryEventArgs>(service_ProcessCompleted);
            service.Failed +=new EventHandler<SuperMap.Web.Service.ServiceFailedEventArgs>(service_Failed);
        }
     
        void service_ProcessCompleted(object sender, SuperMap.Web.iServerJava6R.QueryEventArgs e)
        {
            featureLayer.ClearFeatures();
            if (e.Result != null)
            {
                if (e.Result.ResourceInfo == null)
                {
                    QueryResult queryResult = e.Result;

                    if (queryResult.Recordsets != null && queryResult.Recordsets.Count > 0)
                    {
                        foreach (Recordset recordSet in queryResult.Recordsets)
                        {
                            if (recordSet.Features != null)
                            {
                                featureLayer.AddFeatureSet(recordSet.Features);

                                var canvas = new Canvas()
                                {
                                    Width = 200,
                                    Height = 30,
                                    Background = new SolidColorBrush(Colors.Yellow),
                                };
                                var textblock = new TextBlock()
                                {
                                    Text = tooltipText,
                                    FontFamily = new FontFamily("微软雅黑"),
                                    FontSize = 20,
                                    Foreground = new SolidColorBrush(Colors.Purple),
                                };
                                canvas.Children.Add(textblock);
                                featureLayer.ToolTip = canvas;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Query Result is null");
                }
            }
        }

        //键盘操作时，先切换图层后进行查询操作，类似于新浪天气。
        void myMap_KeyDown(object sender, KeyEventArgs e)
        { 
            myMap.Layers.RemoveAt(0);
            TiledDynamicRESTLayer lay = new TiledDynamicRESTLayer()
            {
                Url = url + "世界地图_Day",
            };
            myMap.Layers.Insert(0, lay);
            Query("世界地图_Day", "World@world", "SmID=2", "俄罗斯");
        }


        //鼠标左键操作时，先清空FeaturesLayer后进行查询操作，看ToolTip的状态。
        void myMap_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Query("世界地图", "World@world", "SmID=2", "俄罗斯");
        }
   
        void service_Failed(object sender, SuperMap.Web.Service.ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

    }
}
