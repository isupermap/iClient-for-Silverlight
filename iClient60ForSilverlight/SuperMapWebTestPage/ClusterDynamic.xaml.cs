﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using SuperMap.Web.Clustering;

namespace iClient60ForSilverlight
{
    public partial class ClusterDynamic : UserControl
    {
        private FeaturesLayer featuresLayer;
        public ClusterDynamic()
        {
            InitializeComponent();
            featuresLayer = MyMap.Layers["MyFeaturesLayer"] as FeaturesLayer;

            LoadFeatures();
        }

        private void Cluster_Click(object sender, RoutedEventArgs e)
        {
            featuresLayer.Features.Clear();
            LoadFeatures();
        }

        private void ClusterNo_Click(object sender, RoutedEventArgs e)
        {
            featuresLayer.Clusterer = null;
        }
        private TextBlock tb = new TextBlock() { Text = "LionGG", Width = 100, Height = 100 };

        //大约的中国范围
        private const double ChinaLeft = 8257123.2296;
        private const double ChinaRight = 15036292.6772;
        private const double ChinaBottom = 2103938.3760;
        private const double ChinaTop = 7113315.4617;

        private Random random = new Random();//随机数生成s不够集中
       
        private void LoadFeatures()
        {
           var  flareClusterer = new ScatterClusterer()
            {
                Background = new SolidColorBrush(Color.FromArgb(99, 00, 255, 00)),
                Foreground = new SolidColorBrush(Color.FromArgb(99, 00, 00, 00)),
                //MaximumCount = 10,
                Radius = 40,
                Gradient = RedGradient,
                //EnableRotation = true
            };
            featuresLayer.Clusterer = flareClusterer;

            for (int i = 0; i < 1000; i++)
            {
                double x = random.NextDouble() * (ChinaRight - ChinaLeft) + ChinaLeft;
                double y = random.NextDouble() * (ChinaTop - ChinaBottom) + ChinaBottom;

                Feature feature = new Feature();
                feature.Geometry = new GeoPoint(x, y);
                feature.Style = MediumMarkerStyle;
                feature.Attributes.Add("id", i);
                feature.ToolTip = new TextBlock { Text = i.ToString() };
                featuresLayer.Features.Add(feature);
            }
        }

        private void FeaturesClear_Click(object sender, RoutedEventArgs e)
        {
            featuresLayer.ClearFeatures();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var sparkClusterer = new SparkClusterer()
            {
                Background = new SolidColorBrush(Color.FromArgb(99, 00, 255, 00)),
                Foreground = new SolidColorBrush(Color.FromArgb(99, 00, 00, 00)),
                Radius = 40,
                Gradient = RedGradient,
            };
            featuresLayer.Clusterer = sparkClusterer;
        }
    }
}
