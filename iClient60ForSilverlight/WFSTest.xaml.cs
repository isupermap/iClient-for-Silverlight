﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Mapping;
using SuperMap.Web.OGC;
using SuperMap.Web.Core;
using System.Xml.Linq;
using System.Collections.ObjectModel;

namespace iClient60ForSilverlight
{
    public partial class WFSTest : UserControl
    {
        FeaturesLayer fl;
        public WFSTest()
        {
            InitializeComponent();
            fl = this.MyMap.Layers["fl"] as FeaturesLayer;
        }

        #region GetWFSCapabilitiesService
        private void getCapa_Click(object sender, RoutedEventArgs e)
        {
            GetWFSCapabilities getCap = new GetWFSCapabilities("http://localhost:8090/iserver/services/data-world/wfs100");
            getCap.ProcessAsync();
            getCap.ProcessCompleted += new EventHandler<ResultEventArgs<List<WFSFeatureType>>>(getCap_ProcessCompleted);
            getCap.Failed += new EventHandler<FailedEventArgs>(getCap_Failed);
        }

        void getCap_ProcessCompleted(object sender, ResultEventArgs<List<WFSFeatureType>> e)
        {
        }

        private void getCap_Failed(object sender, FailedEventArgs e)
        {

        }
        #endregion

        #region GetWFSDescribeFeatureType
        private void getType_Click(object sender, RoutedEventArgs e)
        {
            GetWFSDescribeFeatureType getType = new GetWFSDescribeFeatureType("http://localhost:8090/iserver/services/data-world/wfs100");
            getType.TypeNames.Add("World:Ocean");
            getType.TypeNames.Add("World:Capitals");
            getType.ProcessAsync();
            getType.ProcessCompleted += new EventHandler<ResultEventArgs<Dictionary<string, List<WFSFeatureDescription>>>>(getType_ProcessCompleted);
            getType.Failed += new EventHandler<FailedEventArgs>(getType_Failed);
        }

        void getType_ProcessCompleted(object sender, ResultEventArgs<Dictionary<string, List<WFSFeatureDescription>>> e)
        {
        }

        private void getType_Failed(object sender, FailedEventArgs e)
        {

        }
        #endregion

        #region GetWMTSCapabilities
        private void GetWMTSCapabilities_Click(object sender, RoutedEventArgs e)
        {
            GetWMTSCapabilities gwc = new GetWMTSCapabilities("http://localhost:8090/iserver/services/map-world/wmts100");
            gwc.ProcessAsync();
            gwc.ProcessCompleted += new EventHandler<ResultEventArgs<GetWMTSCapabilitiesResult>>(gwc_ProcessComplated);
            gwc.Failed += new EventHandler<FailedEventArgs>(gwc_Failed);
        }

        private void gwc_Failed(object sender, FailedEventArgs e)
        {

        }

        private void gwc_ProcessComplated(object sender, ResultEventArgs<GetWMTSCapabilitiesResult> e)
        {

        }
        #endregion

        #region GetWFSFeatureByType
        private void getFeatureByID_Click(object sender, RoutedEventArgs e)
        {
            //GetWFSFeatureByIDs getserver = new GetWFSFeatureByIDs("http://localhost:8090/iserver/services/data-world/wfs100");
            //var type1 = new LayerType
            //{
            //    TypeName = "World:Capitals",
            //    SpacialProperty = "the_geom"
            //};
            //type1.IDs.Add("World.Capitals.90");
            //type1.Properties.Add("SMY");
            //type1.Properties.Add("SMX");
            ////type1.Properties.Add("the_geom");

            GetWFSFeature getserver = new GetWFSFeature("http://localhost:8090/iserver/services/data-world/wfs100")
            {
                //MaxFeatures = 10,
            };
            //getserver.FeatureIDs.Add("World.Capitals.90");
            //getserver.FeatureIDs.Add("World.Countries.196");
            //var type1 = new LayerType
            //{
            //    TypeName = "World:Capitals",
            //    SpacialProperty = "the_geom"
            //};
            //type1.Properties.Add("SMY");
            //type1.Properties.Add("SMX");

            var type2 = new WFSFeatureDescription
            {
                TypeName = "World:Countries",
                SpatialProperty = "the_geom"
            };
            type2.Properties.Add("SMY");
            type2.Properties.Add("SMX");


            //getserver.LayerType.Add(type1);
            getserver.FeatureDescriptions.Add(type2);
            getserver.FeatureNS = "http://www.supermap.com/World";
            getserver.ProcessAsync();
            getserver.ProcessCompleted += new EventHandler<ResultEventArgs<GetWFSFeatureResult>>(getserver_ProcessCompleted);
        }

        void getserver_ProcessCompleted(object sender, ResultEventArgs<GetWFSFeatureResult> e)
        {
            foreach (string key in e.Result.FeaturePair.Keys)
            {
                fl.AddFeatureSet(e.Result.FeaturePair[key]);
                foreach (Feature feature in e.Result.FeaturePair[key])
                {
                    feature.ToolTip = new TextBlock { Text = "X:" + feature.Attributes["SMX"] + "\n" + "Y:" + feature.Attributes["SMY"] };
                }
            }
        }
        #endregion
        private void Filter_Click(object sender, RoutedEventArgs e)
        {
            GetWFSFeature getFilter = new GetWFSFeature("http://localhost:8090/iserver/services/data-world/wfs100");
            getFilter.FeatureNS = "http://www.supermap.com/World";
            var type1 = new WFSFeatureDescription { TypeName = "World:Countries" };

            getFilter.FeatureDescriptions.Add(type1);

            Logical f1 = new Logical
            {
                Type = LogicalType.Not,
                Filters =
                {
                    new Logical
                    {
                        Type = LogicalType.And,
                        Filters = 
                        {
                            new Comparison
                            {
                                Type = ComparisonType.LessThan,                        
                                Expressions=
                                {
                                    new Arithmetic
                                    {
                                        PropertyNames={"SMSDRIW","SMSDRIN"},
                                        Type=ArithmeticType.Mul
                                    },
                                    new Arithmetic
                                    {
                                        PropertyNames={"SmID","COLOR_MAP"},
                                        Type=ArithmeticType.Div
                                    }                             
                                }
                            },
                            new Logical
                            {
                                Type = LogicalType.And,
                                Filters = 
                                {
                                    new Comparison
                                    {
                                        PropertyNames = { "SQKM" },
                                        Type = ComparisonType.GreaterThan,
                                        Value = "800000"
                                    },
                                    new Logical 
                                    {
                                        Type = LogicalType.Not ,
                                        Filters=
                                        {
                                            new Comparison 
                                            { 
                                                PropertyNames = { "Capital" }, 
                                                Type = ComparisonType.Null 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            Comparison f2 = new Comparison
                               {
                                   Type = ComparisonType.LessThan,
                                   Expressions = 
                                   {new Arithmetic{
                                           PropertyNames = { "SMSDRIW", "SMSDRIN" },
                                           Type = ArithmeticType.Mul
                                       },
                                       new Arithmetic
                                        {
                                            PropertyNames = { "SmID", "COLOR_MAP" },
                                            Type = ArithmeticType.Div
                                        }
                                   }
                               };

            Comparison f3 = new Comparison
            {
                Type = ComparisonType.GreaterThan,
                PropertyNames = { "SQKM" },
                Expressions = 
                {
                    new Arithmetic
                    {
                        Type= ArithmeticType.Sub,
                        Expressions={
                            new Arithmetic
                            {
                                Type=ArithmeticType.Mul,
                                PropertyNames= {"SMSDRIW","SMSDRIN"}
                            },
                            new Arithmetic
                            {
                                Type=ArithmeticType.Div,
                                PropertyNames={"SmID","COLOR_MAP"}
                            }
                        }
                    }
                }
            };

            Comparison f4 = new Comparison
            {
                Type = ComparisonType.Between,
                PropertyNames = { "SmID" },
                Expressions =
                {
                      new Arithmetic{ Value="100"},
                      new Arithmetic{ Value="110"},
                }
            };

            Comparison f5 = new Comparison
            {
                Type = ComparisonType.Between,
                Expressions =
                {
                    new Arithmetic
                    {
                        PropertyNames={"SmID"},
                         Type=ArithmeticType.Add,
                          Value="10"
                    },
                    new Arithmetic{ Value="100"},
                    new Arithmetic{ Value="110"},
                }
            };

            //(SQKM > 8000000) && (Capition!=null) && (SmArea/2 > 1000)
            Logical f6 = new Logical
            {
                Type = LogicalType.And,
                Filters =
                {
                    new Comparison
                    {
                         Type=ComparisonType.GreaterThan,
                         Value="8000000",
                         PropertyNames={"SQKM"}
                    },
                    new Logical
                    {
                         Type=LogicalType.And,
                         Filters=
                         {                              
                            new Comparison
                            {
                                Type=ComparisonType.Null,
                                PropertyNames={"Capition"}
                            },
                            new Comparison
                            {
                                 Type=ComparisonType.GreaterThan,
                                 Expressions=
                                 {
                                      new Arithmetic
                                      {
                                           PropertyNames={"SmArea"},
                                           Type=ArithmeticType.Div,
                                           Value="2"
                                      },                                      
                                 },
                                 Value="1000"
                            }
                         }
                    }
                }
            };

            // ((8000000 <= SQKM <= 10000000) && (Capital = "华盛顿")) || (Country != "加拿大")
            Logical f7 = new Logical
            {
                Type = LogicalType.Or,
                Filters = 
                { 
                    new Logical
                    {
                        Type =  LogicalType.And,
                        Filters=
                        {
                            new Comparison
                            {
                                 Type=ComparisonType.Between,
                                 PropertyNames={"SQKM"},
                                 Expressions=
                                 {
                                     new Arithmetic{ Value="8000000"},
                                     new Arithmetic{Value="10000000"}
                                 }
                            },
                            new Comparison
                            {
                                 PropertyNames = {"Capital"},
                                 Type = ComparisonType.EqualTo,
                                 Value="华盛顿"
                            }
                        }
                    },
                    new Comparison
                    {
                        PropertyNames = {"Country"},
                        Type = ComparisonType.NotEqualTo,
                        Value = "加拿大"
                    }
                }
            };

            //(SQKM < 10) && (Country Like "直布%") or ((SQKM * 2) > 10)
            Logical f8 = new Logical
            {
                Type = LogicalType.Or,
                Filters = 
                {
                    new Logical
                    {
                        Type = LogicalType.And,
                        Filters=
                        {
                            new Comparison
                            {
                                PropertyNames = {"SQKM"},
                                Type = ComparisonType.LessThan,
                                Value = "10"
                            },
                            new Comparison
                            {
                                Type = ComparisonType.Like,
                                PropertyNames = {"Country"},
                                Value = "直布%"
                            }
                        }
                    },
                    new Comparison
                    {
                        Type = ComparisonType.GreaterThan,
                        Expressions =
                        {
                            new Arithmetic
                            {
                                Type = ArithmeticType.Mul,
                                PropertyNames = {"SQKM"},
                                Value = "2"
                            }
                        },
                        Value = "10"
                    }
                }
            };

            //(SQKM <= 10) && ((SQKM + SQMI) >10) or((SQKM + SQMI)<1)
            Logical f9 = new Logical
            {
                Type = LogicalType.Or,
                Filters = 
                {
                    new Logical
                    {
                        Type = LogicalType.And,
                        Filters=
                        {
                            new Comparison
                            {
                                Type = ComparisonType.LessThanOrEqualTo,
                                PropertyNames = {"SQKM"},
                                Value = "10"
                            },
                            new Comparison
                            {
                                Type = ComparisonType.GreaterThan,
                                Expressions = 
                                {
                                    new Arithmetic
                                    {
                                        PropertyNames = {"SQKM","SQMI"},
                                        Type = ArithmeticType.Add
                                    }
                                },
                                Value = "10"
                            }
                        }
                    },
                    new Comparison
                    {
                        Type = ComparisonType.LessThan,
                        Expressions =
                        {
                            new Arithmetic
                            {
                                PropertyNames = {"SQKM","SQMI"},
                                Type = ArithmeticType.Add
                            }
                        },
                        Value = "1"
                    }
                }
            };

            Logical f10 = new Logical
            {
                Type = LogicalType.And,
                Filters =
                {
                    new Logical
                    {
                        Type = LogicalType.And,
                        Filters=
                        {
                            new Logical
                            {
                                Type = LogicalType.And,
                                Filters=
                                {
                                    new SpatialRect
                                    {
                                        Type = SpatialType.BBOX,
                                        PropertyName = "World",
                                        Value = new Rectangle2D(100,-10,180,30)
                                    },
                                    new SpatialGeometry
                                    {
                                        Type = SpatialType.Disjoint,
                                        PropertyName = "World",
                                        Value = new GeoRegion
                                        {
                                            Parts = 
                                            {
                                                new Point2DCollection
                                                {
                                                    new Point2D(100,-10),
                                                    new Point2D(140,0)
                                                }
                                            }
                                        }
                                    }
                                }
                            },
                            new SpatialGeometry
                            {
                                Type = SpatialType.Intersects,
                                PropertyName = "World",
                                Value = new GeoRegion
                                {
                                     Parts = 
                                     {
                                         new Point2DCollection
                                         {
                                             new Point2D(160,10),
                                             new Point2D(180,20)
                                         }
                                     }
                                }
                            }
                        }
                    },
                    new SpatialGeometry
                    {
                        Type = SpatialType.Contains,
                        PropertyName = "World",
                        Value = new GeoPoint(166.62,19.3)
                    }
                }
            };

            Logical f11 = new Logical
            {
                Type = LogicalType.And,
                Filters =
                {
                    new Logical
                    {
                        Type = LogicalType.And,
                        Filters =
                        {
                            new SpatialGeometry
                            {
                                Type = SpatialType.Within,
                                PropertyName = "Grids",
                                Value = new GeoLine
                                {
                                    Parts =
                                    {
                                        new Point2DCollection
                                        {
                                            new Point2D(-180,-90),
                                            new Point2D(-120,0)
                                        }
                                    }
                                }
                            },
                            new SpatialGeometry
                            {
                                Type = SpatialType.Crosses,
                                PropertyName = "Grids",
                                Value = new GeoLine
                                {
                                    Parts =
                                    {
                                        new Point2DCollection
                                        {
                                            new Point2D(-180,-60),
                                            new Point2D(-160,-30),
                                        }
                                    }
                                }
                            }
                        }
                    },
                    new SpatialGeometry
                    {
                        Type = SpatialType.Overlaps,
                        PropertyName = "Grids",
                        Value = new GeoLine
                        {
                            Parts =
                            {
                                new Point2DCollection
                                {
                                    new Point2D(-180,-30),
                                    new Point2D(-160,-45),
                                }
                            }
                        }
                    }
                }
            };

            SpatialGeometry f12 = new SpatialGeometry
            {
                Type = SpatialType.Touches,
                PropertyName = "Grids",
                Value = new GeoLine
                {
                    Parts =
                    {
                        new Point2DCollection
                        {
                            new Point2D(-180,-60),
                            new Point2D(-160,-30),
                        }
                    }
                }
            };

            SpatialGeometry f13 = new SpatialGeometry
            {
                Type = SpatialType.Equals,
                PropertyName = "Grids",
                Value = new GeoLine
                {
                    Parts =
                    {
                        new Point2DCollection
                        {
                            new Point2D(-180,-90),
                            new Point2D(-160,-90),
                        }
                    }
                }
            };
            getFilter.Filters.Add(f4);
            getFilter.Filters.Add(f5);
            getFilter.ProcessAsync();
            getFilter.ProcessCompleted += new EventHandler<ResultEventArgs<GetWFSFeatureResult>>(getFilter_ProcessCompleted);
            getFilter.Failed += new EventHandler<FailedEventArgs>(getFilter_Failed);

        }

        void getFilter_Failed(object sender, FailedEventArgs e)
        { }

        void getFilter_ProcessCompleted(object sender, ResultEventArgs<GetWFSFeatureResult> e)
        {
            foreach (var item in e.Result.FeaturePair)
            {
                fl.AddFeatureSet(item.Value);
            }
        }

        private void WFST_Click(object sender, RoutedEventArgs e)
        {
            WFSTDeleteParams delete = new WFSTDeleteParams();
            delete.TypeName = "feature:BusLine";
            //p.FeatureIDs = new string[] { "Changchun.BusLine.30" };
            delete.Filter = new Logical
            {
                Type = LogicalType.Not,
                Filters =
                {
                    new Logical
                    {
                        Type = LogicalType.And,
                        Filters = 
                        {
                            new Comparison
                            {
                                Type = ComparisonType.LessThan,
                                Expressions=
                                {
                                    new Arithmetic
                                    {
                                        PropertyNames={"SMSDRIW","SMSDRIN"},
                                        Type=ArithmeticType.Mul
                                    },
                                    new Arithmetic
                                    {
                                        PropertyNames={"SmID","COLOR_MAP"},
                                        Type=ArithmeticType.Div
                                    }                             
                                }
                            },
                            new Logical
                            {
                                Type = LogicalType.And,
                                Filters = 
                                {
                                    new Comparison
                                    {
                                        PropertyNames = { "SQKM" },
                                        Type = ComparisonType.GreaterThan,
                                        Value = "800000"
                                    },
                                    new Logical 
                                    {
                                        Type = LogicalType.Not ,
                                        Filters=
                                        {
                                            new Comparison 
                                            { 
                                                PropertyNames = { "Capital" }, 
                                                Type = ComparisonType.Null 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            Feature f = new Feature
            {
                Geometry = new GeoRegion
                {
                    Parts = new ObservableCollection<Point2DCollection> 
                        {
                            new Point2DCollection 
                            {
                                new Point2D(4325.0155662865,-4408.9315638131),
                                new Point2D(4368.5573809289,-4369.4974675333),
                                new Point2D(4419.493088623703,-4360.460487135835),
                                new Point2D(4411.2776518987,-4417.9685442106),
                                new Point2D(4325.0155662865,-4408.9315638131)
                            }
                        }
                }
            };
            f.Attributes.Add("SMID", 10);
            WFSTUpdateParams update = new WFSTUpdateParams()
            {
                TypeName = "Changchun:BusPoly",
                Feature = f,
                SpatialProperty = "the_geom",
                FeatureIDs = new string[] { "Changchun.BusLine.30" },
            };


            WFSTInsertParams insert = new WFSTInsertParams
            {
                TypeName = "feature:BusPoly",
                SpatialProperty = "the_geom",
            };

            Feature f1 = new Feature
            {
                Geometry = new GeoRegion
                {
                    Parts = new ObservableCollection<Point2DCollection>
                    {
                        new Point2DCollection
                        {
                            new Point2D(4325.0155662865,-4408.9315638131),
                            new Point2D(4368.5573809289,-4369.4974675333),
                            new Point2D(4445.7824861436,-4344.0296136859),
                            new Point2D(4411.2776518987,-4417.9685442106),
                            new Point2D(4325.0155662865,-4408.9315638131),
                        }
                    }
                },
            };
            //f.Attributes.Add("NAME", "用脚走的公交车");
            f1.Attributes.Add("feature:ClassID", "10");

            insert.Features.Add(f1);
            WFSTransaction wfst = new WFSTransaction("http://localhost:8090/iserver/services/data-changchun/wfs100") { FeatureNS = "http://www.supermap.com/Changchun" };
            wfst.ProcessAsync(new List<WFSTParameters> { delete, update, insert });
            wfst.ProcessCompleted += new EventHandler<ResultEventArgs<WFSTResult>>(wfst_ProcessCompleted);
        }

        void wfst_Failed(object sender, FailedEventArgs e)
        {

        }

        void wfst_ProcessCompleted(object sender, ResultEventArgs<WFSTResult> e)
        {

        }

        public static int GetMyProperty(DependencyObject obj)
        {
            return (int)obj.GetValue(MyPropertyProperty);
        }

        public static void SetMyProperty(DependencyObject obj, int value)
        {
            obj.SetValue(MyPropertyProperty, value);
        }

        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.RegisterAttached("MyProperty", typeof(int), typeof(WFSTest), null);
    }
}
