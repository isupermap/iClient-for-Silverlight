﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SuperMap.Web.Core;
using SuperMap.Web.iServerJava2;
using SuperMap.Web.Mapping;
using SuperMap.Web.Actions;
using System.Windows.Media;
using SuperMap.Web.Rendering;

namespace iClient60ForSilverlight
{
    public partial class iServer2Test : UserControl
    {
        private FeaturesLayer featuresLayer;
        public iServer2Test()
        {
            InitializeComponent();

            TiledCachedIServerLayer iserverlayer = new TiledCachedIServerLayer();
            iserverlayer.Url = "http://localhost:7080/demo";
            iserverlayer.MapServiceAddress = "localhost";
            iserverlayer.MapServicePort = 8600;
            iserverlayer.MapName = "World";

            iserverlayer.CachedUrl = "http://localhost:7080/output";
            iserverlayer.Scales = new double[] { 500000, 5000000, 50000000, 500000000 };

            iserverlayer.IsSkipGetSMMapServiceInfo = true;
            iserverlayer.CRS = new CoordinateReferenceSystem { Unit = Unit.Degree, WKID = 4326 };
            iserverlayer.Bounds = new Rectangle2D(-180, -90, 180, 90);
            iserverlayer.ReferViewBounds = new Rectangle2D(-66.6154069176247, -49.9740487003936, 66.6154069176247, 49.9740487003936);
            iserverlayer.ReferViewer = new Rect(-31984, -31976, 400, 300);
            iserverlayer.ReferScale = 0.000000008987817752221122;

            MyMap.Layers.Add(iserverlayer);

            featuresLayer = MyMap.Layers["myFeaturesLayer"] as FeaturesLayer;
            MyMap.ViewBoundsChanged += new System.EventHandler<ViewBoundsEventArgs>(MyMap_ViewBoundsChanged);
        }

        void MyMap_ViewBoundsChanged(object sender, ViewBoundsEventArgs e)
        {
            mytb.Text = (1 / MyMap.Scale).ToString();
        }

        private void query_Click(object sender, RoutedEventArgs e)
        {
            DrawPolygon polygon = new DrawPolygon(MyMap);
            polygon.DrawCompleted += new System.EventHandler<DrawEventArgs>(polygon_DrawCompleted);
            MyMap.Action = polygon;
        }

        private void polygon_DrawCompleted(object sender, DrawEventArgs e)
        {
            //QueryByGeometryParameters param = new QueryByGeometryParameters()
            //{
            //    MapName = "Changchun",
            //    Geometry = e.Geometry,
            //    QueryParam = new QueryParam
            //    {
            //        QueryLayerParams = new List<QueryLayerParam> 
            //        {
            //            new QueryLayerParam
            //            {
            //                 Name="Vegetable@changchun"
            //            }
            //        }
            //    }
            //};
            //QueryByGeometryService s = new QueryByGeometryService("http://localhost:7080/demo");
            //s.ProcessCompleted += new System.EventHandler<QueryEventArgs>(s_ProcessCompleted);
            //s.ProcessAsync(param);

            QueryBySqlParameters param = new QueryBySqlParameters
            {
                MapName = "World",
                QueryParam = new QueryParam
                {
                    QueryLayerParams = new List<QueryLayerParam> 
                    {
                        new QueryLayerParam
                        {
                             Name="World@world",
                              SqlParam=new SqlParam
                              {
                                   WhereClause="SmID=247"
                              }
                        }
                    }
                }
            };

            QueryBySqlService qss = new QueryBySqlService("http://localhost:7080/demo");
            qss.ProcessCompleted += new System.EventHandler<QueryEventArgs>(qss_ProcessCompleted);

            qss.ProcessAsync(param);
        }

        void qss_ProcessCompleted(object sender, QueryEventArgs e)
        {
            for (int i = 0; i < e.ResultSet.RecordSets.Count; i++)
            {
                foreach (Feature f in e.ResultSet.RecordSets[i].ToFeatureSet())
                {
                    f.Style = new FillStyle { Fill = new SolidColorBrush(new Color { A = 100, R = 0, G = 100, B = 0 }), Stroke = new SolidColorBrush(Colors.Blue), StrokeThickness = 1 };
                    featuresLayer.Features.Add(f);
                }
            }
        }

        void s_ProcessCompleted(object sender, QueryEventArgs e)
        {
            featuresLayer.Renderer = new UniformRenderer
            {
                MarkerStyle = new PredefinedMarkerStyle() { Color = new SolidColorBrush(Colors.Blue), Size = 20, Symbol = PredefinedMarkerStyle.MarkerSymbol.Diamond }
            };

            foreach (RecordSet item in e.ResultSet.RecordSets)
            {
                featuresLayer.AddFeatureSet(item.ToFeatureSet());
            }

        }

        private void clear_Click(object sender, RoutedEventArgs e)
        {
            Pan p = new Pan(MyMap);
            MyMap.Action = p;
        }
    }
}
