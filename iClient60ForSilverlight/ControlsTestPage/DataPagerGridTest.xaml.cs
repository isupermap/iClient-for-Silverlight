﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SuperMap.Web.Mapping;
using SuperMap.Web.iServerJava6R;
using SuperMap.Web.Service;
using SuperMap.Web.Core;

namespace iClient60ForSilverlight
{
    public partial class DataPagerGridTest : UserControl
    {
        private const string url = "http://localhost:8090/iserver/services/components-rest/rest/maps/世界地图";
        private FeaturesLayer flayer;
        private FeaturesLayer flayer1;

        public DataPagerGridTest()
        {
            InitializeComponent();
            flayer = MyMap.Layers["FeaturesLayer"] as FeaturesLayer;
            flayer1 = MyMap1.Layers["FeaturesLayer1"] as FeaturesLayer;
            flayer.MouseLeftButtonDown += new EventHandler<FeatureMouseButtonEventArgs>(flayer_MouseLeftButtonDown);
            flayer1.MouseLeftButtonDown += new EventHandler<FeatureMouseButtonEventArgs>(flayer1_MouseLeftButtonDown);           
        }

        void flayer_MouseLeftButtonDown(object sender, FeatureMouseButtonEventArgs e)
        {
            e.Feature.Selected = !e.Feature.Selected;
            //if (e.Feature.Selected)
            //{
            //    e.Feature.Style = SelectStyle;
            //}
            int i = 0;
            foreach (var item in flayer.Features)
            {
                if (item.Selected)
                { i++; }
            }

            System.Diagnostics.Debug.WriteLine("FeaturesLayer已经选中了： " + i + "个Feature");
        }

        void flayer1_MouseLeftButtonDown(object sender, FeatureMouseButtonEventArgs e)
        {
            e.Feature.Selected = !e.Feature.Selected;
            //if (e.Feature.Selected)
            //{
            //    e.Feature.Style = SelectStyle;
            //}
            int i = 0;
            foreach (var item in flayer1.Features)
            {
                if (item.Selected)
                { i++; }
            }

            System.Diagnostics.Debug.WriteLine("FeaturesLayer1已经选中了： " + i + "个Feature");
        }

        private static QueryBySQLParameters SetParameters()
        {
            QueryBySQLParameters param = new QueryBySQLParameters()
            {
                FilterParameters = new List<FilterParameter>() 
                {
                    new FilterParameter()
                    {
                        Name = "Countries@world", 
                        
                        AttributeFilter = "SmID<50 and SmID>10"
                    }
                },

                ReturnContent = true
            };
            return param;
        }

        private void service_ProcessCompleted(object sender, QueryEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("查询结果为空！");
                return;
            }

            //flayer.Features.Clear();

            foreach (Recordset recordset in e.Result.Recordsets)
            {
                flayer.AddFeatureSet(recordset.Features);
            }

            foreach (Feature item in flayer.Features)
            {
                item.Style = SelectStyle;
            }

            foreach (Feature item in flayer.Features)
            {
                item.Selected = true;
            }
        }

        private void service_Failed(object sender, ServiceFailedEventArgs e)
        {
            MessageBox.Show(e.Error.Message);
        }

        private static QueryBySQLParameters NewMethod1()
        {
            QueryBySQLParameters param = new QueryBySQLParameters()
            {
                FilterParameters = new List<FilterParameter>() 
                {
                    new FilterParameter()
                    {
                        Name = "Ocean@World", 
                        
                        AttributeFilter = "SmID<30 and SmID>10"
                    }
                },

                ReturnContent = true
            };
            return param;
        }

        private void service1_ProcessCompleted(object sender, QueryEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("查询结果为空！");
                return;
            }

            foreach (Recordset recordset in e.Result.Recordsets)
            {
                flayer1.AddFeatureSet(recordset.Features);
            }

            foreach (Feature item in flayer1.Features)
            {
                item.Style = SelectStyle;
                item.Selected = true;
            }
        }

        private void a1_Click(object sender, RoutedEventArgs e)
        {
            QueryBySQLParameters param = SetParameters();

            QueryBySQLService service = new QueryBySQLService(url);
            service.ProcessAsync(param);
            service.ProcessCompleted += new EventHandler<QueryEventArgs>(service_ProcessCompleted);
            service.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
        }

        private void a2_Click(object sender, RoutedEventArgs e)
        {
            QueryBySQLParameters param1 = NewMethod1();

            QueryBySQLService service1 = new QueryBySQLService(url);
            service1.ProcessAsync(param1);
            service1.ProcessCompleted += new EventHandler<QueryEventArgs>(service1_ProcessCompleted);
            service1.Failed += new EventHandler<ServiceFailedEventArgs>(service_Failed);
        }
    }
}
