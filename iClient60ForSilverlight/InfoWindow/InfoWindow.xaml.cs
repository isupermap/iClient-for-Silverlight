﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Animation;
using System.Windows.Browser;

namespace iClient60ForSilverlight
{
    public partial class InfoWindow : UserControl
    {
        public InfoWindow()
        {
            InitializeComponent();
            this.Open.Begin();
        }

        //关闭按钮
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close.Begin();
        }

        //OK按钮
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://www.supermap.com.cn"), "_blank");
        }

        #region 删除按钮
        public delegate void DeletedClickedHandler(object sender, RoutedEventArgs e);
        public event DeletedClickedHandler DeletedClicked;
        protected virtual void OnDeletedClicked(RoutedEventArgs e)
        {
            if (this.DeletedClicked != null)
            {
                this.DeletedClicked(this, e);
            }
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            this.OnDeletedClicked(e);
        }
        #endregion

        #region Title 可以是复杂控件，比如加图片等
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(object), typeof(InfoWindow), new PropertyMetadata(new PropertyChangedCallback(InfoWindow.TitleChanged)));
        public object Title
        {
            get
            {
                return base.GetValue(TitleProperty);
            }
            set
            {
                base.SetValue(TitleProperty, value);
            }
        }
        private static void TitleChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((InfoWindow)sender).TitleContent.Content = e.NewValue;

        }
        #endregion

        //可以根据需要自定义
        #region Description
        public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register("Description", typeof(string), typeof(InfoWindow), new PropertyMetadata(new PropertyChangedCallback(InfoWindow.DescriptionChanged)));
        public string Description
        {
            get
            {
                return (string)base.GetValue(DescriptionProperty);
            }
            set
            {
                base.SetValue(DescriptionProperty, value);
            }
        }
        private static void DescriptionChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((InfoWindow)sender).tbDescription.Text = (string)e.NewValue;
        }
        #endregion

    }
}
