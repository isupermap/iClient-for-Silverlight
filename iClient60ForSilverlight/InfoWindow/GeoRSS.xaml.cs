﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml.Linq;
using SuperMap.Web.Core;
using SuperMap.Web.Mapping;
using System.Windows.Media;
using iClientLab;

namespace iClient60ForSilverlight
{
    public partial class GeoRSS : UserControl
    {
        private ElementsLayer elemLayer;
        private Pushpin slectedPP;
        private const string url = "http://earthquake.usgs.gov/eqcenter/catalogs/eqs7day-M2.5.xml";

        private FeaturesLayer fLayer = new FeaturesLayer();


        public GeoRSS()
        {
            InitializeComponent();
            elemLayer = MyMap.Layers["MyArbitraryLayer"] as ElementsLayer;
            MyMap.Loaded += new RoutedEventHandler(MyMap_Loaded);

            var btn = new Button() { Width = 100, Height = 80, Content = "aaaa" };
            fLayer.AddFeature(new Feature()
            {
                Geometry = new GeoPoint(0, 0),
                Style = new PredefinedMarkerStyle() { Size = 30, Color = new SolidColorBrush(Color.FromArgb(99, 00, 0xff, 00)) },
                //ToolTip = new InfoBox() { Title="aaa", Description="BBBBBBBBBBb"}
                ToolTip = btn
            }
            );

            FeaturesLayer.SetToolTipHorizontalOffset(btn, 100);

            MyMap.Layers.Add(fLayer);

            MyMap.ViewBoundsChanged += new EventHandler<ViewBoundsEventArgs>(MyMap_ViewBoundsChanged);

            var pp = new Pushpin() { Location = new Point2D(0, 0) };
            pp.MouseLeftButtonDown += new MouseButtonEventHandler(pp_MouseLeftButtonDown);
            elemLayer.AddChild(pp);
        }

        void MyMap_ViewBoundsChanged(object sender, ViewBoundsEventArgs e)
        {
            if (e.OldViewBounds.IsEmpty)
            {
                var box = new Button() { Width = 100, Height = 80, Content = "ScreenLayer" };
                box.Click += new RoutedEventHandler(box_Click);
                MyMap.ScreenContainer.Children.Add(box);
                Canvas.SetZIndex(box, 20);
            }
        }

        void MyMap_Loaded(object sender, RoutedEventArgs e)
        {
            LoadRSS(url);
            DispatcherTimer updateTimer = new DispatcherTimer();
            updateTimer.Interval = new TimeSpan(0, 0, 0, 60000);
            updateTimer.Tick += (s, args) => { LoadRSS(url); };
            updateTimer.Start();
        }

        void box_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("呵呵呵呵哈哈哈");
        }
        private void LoadRSS(string uri)
        {
            WebClient wc = new WebClient();
            wc.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);
            Uri feedUri = new Uri(uri, UriKind.Absolute);
            wc.OpenReadAsync(feedUri);
        }

        void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                return;
            }
            elemLayer.Children.Clear();

            XDocument doc = XDocument.Load(e.Result);
            XNamespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";

            var temp = from tt in doc.Descendants("item")
                       select new RssClass
                       {
                           Lon = Convert.ToDouble(tt.Element(geo + "long").Value),
                           Lat = Convert.ToDouble(tt.Element(geo + "lat").Value),
                           Title = tt.Element("title").Value,
                           Desc = tt.Element("description").Value
                       };

            foreach (var item in temp)
            {
                Point2D lonlat = new Point2D(item.Lon, item.Lat);
                Point2D xy = MercatorUtility.LatLonToMeters(lonlat);
                Pushpin pp = new Pushpin() { Location = xy, Tag = item };
                pp.MouseLeftButtonDown += new MouseButtonEventHandler(pp_MouseLeftButtonDown);
                elemLayer.AddChild(pp);
            }

        }

        private void pp_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            slectedPP = sender as Pushpin;
            RssClass info = (RssClass)slectedPP.Tag;

            InfoWindow box = new InfoWindow
            {
                Title = new TextBlock() { Text = info.Title },
                Description = info.Desc
            };
            box.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Left);
            box.SetValue(FrameworkElement.VerticalAlignmentProperty, VerticalAlignment.Top);
            box.DeletedClicked += new InfoWindow.DeletedClickedHandler(box_DeletedClicked);

            //MyMap.OpenInfoWindow(slectedPP.Location, box);
            MyMap.OpenInfoWindow(slectedPP.Location, -10, -10, box);

        }

        private void box_DeletedClicked(object sender, RoutedEventArgs e)
        {
            MyMap.CloseInfoWindow();
            elemLayer.Children.Remove(slectedPP);
        }


        private class RssClass
        {
            public double Lon { get; set; }
            public double Lat { get; set; }
            public string Title { get; set; }
            public string Desc { get; set; }
        }

        TextBlock tb = new TextBlock() { Text = "Tb S...Layer" };
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //MyMap.ScreenLayer.Background = new SolidColorBrush(Colors.Red);

            //tb.SetValue(Canvas.LeftProperty,100);
            Canvas.SetLeft(tb, 300.0);
            MyMap.ScreenContainer.Children.Add(tb);
        }

        private void FeaturesClear_Click(object sender, RoutedEventArgs e)
        {
            Canvas.SetTop(tb, 300.0);
        }
    }
}
